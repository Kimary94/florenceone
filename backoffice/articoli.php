<?php
    include "sys_header.php";
    if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(2, "read")) {header("location: ".linkto("sys_dashboard")); die();}}
?>
<style>
.datatable-header {display:none;}
</style>
<body class="navbar-top<?php if ($settings_menu_closed == 1) { echo ' sidebar-xs'; } ?>">
    <div id="imagePreviewDiv" class="modal modal-xl fade imgpreview" style="z-index: 9999;"></div>
    <?php include "sys_navbar.php"; ?>
    <div class="page-content">
        <?php
        include "sys_sidebar.php";
        if ($_SESSION["user_username"] != "sysadmin") {
            if (!checkUserRight(2, "create")) {echo "<script>can_create = false;</script>";} else {echo "<script>can_create = true;</script>";}
            if (!checkUserRight(2, "edit")) {echo "<script>can_edit = false;</script>";} else {echo "<script>can_edit = true;</script>";}
        } else {
            echo "<script>can_create = true; can_edit = true;</script>";
        }
        ?>
        <div class="content-wrapper">

        
        <!-- Page header -->
        <div class="page-header page-header-light">
            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">

                <div class="d-flex">
                    <i class="icon-t-shirt mr-2 breadcrumb-item"></i><span style="margin-top: 8px;">Articoli</span>
                </div>

                <div class="header-elements d-none" style="display: flex !important;">
                    <div class="breadcrumb justify-content-center">
                        <?php
                        $show_button_new = true;
                        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(2, "create")) { $show_button_new = false; }}
                        if ($show_button_new) { ?>
                            <a href="articoli_edit.php" class="breadcrumb-elements-item">
                                <i class="icon-plus-squared"></i>
                                Nuovo articolo
                            </a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

<div class="content unselectable">
    <div class="card card-collapsed" style="background-color: #f9f9f9;">
        <div class="card-header header-elements-inline" onmouseup="$('#searchexpand').click();" style="font-variant: all-petite-caps;">
            <div><i class="icon-search-7"></i> Filtri di ricerca</div>
            <div data-action="collapse" id="searchexpand" style="display:none;"></div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-lg-10 col-md-9 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-2 col-md-2"><div class="form-group container_fields">
                            <label class="text-bold">Codice</label>
                            <input type="text" class="form-control" autocomplete="off" id="codice" value="<?php if (isset($_SESSION['articolo_filter_codice'])) { echo $_SESSION['articolo_filter_codice']; } ?>">
                        </div></div>
                        <div class="col-lg-5 col-md-8"><div class="form-group container_fields">
                            <label class="text-bold">Nome</label>
                            <input type="text" class="form-control" autocomplete="off" id="nome" value="<?php if (isset($_SESSION['articolo_filter_nome'])) { echo $_SESSION['articolo_filter_nome']; } ?>">
                        </div></div>
                        <div class="col-lg-1 col-md-2"><div class="form-group container_fields">
                            <label class="text-bold" style="display: inherit;">Online</label><br>
                            <input type="checkbox" class="form-control" id="online" value="1" <?php if (isset($_SESSION['articolo_filter_online'])) { if ($_SESSION['articolo_filter_online'] == 1) { echo " CHECKED "; } } ?> >
                        </div></div>
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="text-center">
                                <button onclick="event.preventDefault(); applyFilters();"
                                        style="width: 150px; padding: 10px; border-radius: 5px;"
                                        class="btn btn-sm btn-outline-info">
                                            <i class="icon-search-1" style="float: left; margin-top: 2px;"></i> Search
                                </button><br>
                                <button onclick="event.preventDefault(); resetFilters();"
                                        style="width: 150px; padding: 10px; border-radius: 5px; margin: 3px 0px 3px 0px;"
                                        class="btn btn-sm btn-outline-warning">
                                            <i class="icon-cancel-alt" style="float: left; margin-top: 2px;"></i> Reset
                                </button><br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="card">
    <table id="myDatatable" class="display table table-striped datatable-responsive" style="width:100%; overflow: hidden;">
        <thead>
            <tr>
                <th><b>Id</b></th>
                <th><b>Codice</b></th>
                <th><b>Nome</b></th>
                <th><b>72h</b></th>
                <th><b>Prezzo<br>attuale</b></th>
                <th><b>Prezzo<br>precedente</b></th>
                <th><b>Online</b></th>
                <th><b>Azioni</b></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th><b>Id</b></th>
                <th><b>Codice</b></th>
                <th><b>Nome</b></th>
                <th><b>72h</b></th>
                <th><b>Prezzo<br>attuale</b></th>
                <th><b>Prezzo<br>precedente</b></th>
                <th><b>Online</b></th>
                <th><b>Azioni</b></th>
            </tr>
        </tfoot>
    </table>
</div>

</div>
</div>
</div>

<script>

    function update_table()  {
        table = $('#myDatatable').DataTable();
        table.destroy();
        $("#myDatatable > tbody").html("");
        load_table();
        table = $('#myDatatable').DataTable();
        table.page(0).draw(false);
    }

    $(document).ready(function () {
        load_table();
        table = $('#myDatatable').DataTable();
        table.on('draw', function () {
            table.rows().every(function (rowIdx, tableLoop, rowLoop) {
                var values_in_row = this.data();
                $.ajax({
                    async: false,
                    type: "POST",
                    url: "controllers/articoli_controller.php",
                    data: {
                        "action": "button_record",
                        "id": values_in_row[0]
                    },
                    success: function (data, stato) {
                        values_in_row[7] = data;
                    }
                });
                this.data(values_in_row);
            });
        });
    });

    function delete_record(id) {
        swal({
            title: "Sei sicuro?",
            text: "I dati non potranno essere recuperati",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: "Si, elimina!",
            cancelButtonText: "No, annulla!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    async: true,
                    type: "POST",
                    url : "controllers/articoli_controller.php",
                    data: {
                        "action": "delete_record",
                        "id": id
                    },
                    success: function (data) {
                        if (data === "OK") {
                            swal({
                                title: "Eliminato!",
                                text: "Il record è stato cancellato",
                                confirmButtonColor: "#66BB6A",
                                type: "success"
                            }, function () {
                                document.location.reload();
                            });
                        } else {
                            swal({
                                title: "Operazione annullata",
                                text: "Qualcosa è andato storto",
                                confirmButtonColor: "#2196F3",
                                type: "error"
                            });
                        }
                    },
                    error: function (data) {
                        swal({
                            title: "Operazione annullata",
                            text: "Qualcosa è andato storto",
                            confirmButtonColor: "#2196F3",
                            type: "error"
                        });
                    }
                });
            }
            else {
                swal({
                    title: "Operazione annullata",
                    text: "",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
    }

    function load_table() {
        filtercodice = "";
        if (parseInt($("#codice").val().length) > 0) { filtercodice = $("#codice").val(); }
        filternome = "";
        if (parseInt($("#nome").val().length) > 0) { filternome = $("#nome").val(); }
        filteronline = "";
        if (ischecked("#online")) { filteronline = "1"; }
        $('#myDatatable').DataTable( {
            'iDisplayLength': 25,
            'processing': true,
            'serverSide': true,
            'stateSave': true,
            'searching': false,
            'responsive': true,
            dom: '<"datatable-header"flB><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            buttons: [],
            language: {
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: {'first': '|&larr;', 'last': '&rarr;|', 'next': '&rarr;', 'previous': '&larr;'},
                emptyTable:     "Nessun dato disponibile",
                info:           "Stai visualizzando il record dal n. <b style='font-size:14px;'>_START_</b> al n. <b style='font-size:14px;'>_END_</b> su un totale di <b style='font-size:14px;'>_TOTAL_</b> record",
                infoEmpty:      "",
            },
            ajax: {
                url: 'controllers/articoli_controller.php',
                type: 'POST',
                data: {
                    action: "load_table",
                    filtercodice: filtercodice,
                    filternome: filternome,
                    filteronline: filteronline,
                }
            },
            'columnDefs': [
                { 'targets': 0, 'visible': false },
                { "targets": 1,                          "width": "20%" },
                { "targets": 2,                          "width": "60%" },
                { "targets": 3, "className": "dt-center",  "width": "5%" },
                { "targets": 4, "className": "dt-right", "orderable": false, "width": "5%" },
                { "targets": 5, "className": "dt-right", "orderable": false, "width": "5%" },
                { "targets": 6, "className": "dt-center", "orderable": false, "width": "5%" },
                { "targets": 7, "className": "dt-center", "orderable": false, "width": "5%" },
            ]
        } );
    }

    function applyFilters() {
        table = $('#myDatatable').DataTable();
        table.destroy();
        $("#myDatatable > tbody").html("");
        load_table();
        table = $('#myDatatable').DataTable();
        table.page(0).draw(false);
    }

    function resetFilters() {
        $("#codice").val("");
        $("#nome").val("");
        if (ischecked("#online")) {
            $("#online").click();
        }
        applyFilters();
    }

    function func72h(id) {
        $.ajax({
            async: false,
            type: "POST",
            url: "controllers/articoli_controller.php",
            data: {
                "action": "72h",
                "id": id
            },
            success: function (data, stato) {
                applyFilters();
            }
        });
    }
</script>