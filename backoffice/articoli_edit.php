<?php
    include "sys_header.php";
    if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(2, "read")) {header("location: ".linkto("sys_dashboard")); die();}}

    $bordo1 = "";
    $bordo1b = "";
    $bordo2 = "";
    $bordo2b = "";
    $bordo3 = "";
    $tipoform = "";

    $id = "";
    $codice = "";
    $nome = "";
    $descrizione = "";
    $qtainput = 0;
    $qtaselect = 0;
    $qtamin = "";
    $qtamax = "";
    $coloreselect = 0;
    $grigliacoloretaglie = 0;
    $id_articolo_equivalente1 = 0;
    $id_articolo_equivalente2 = 0;
    $online = 0;
    $immagine1 = "";
    $immagine2 = "";
    if(isset($_GET['id'])) {
        $qry = "SELECT * FROM articolo WHERE id = ".$_GET['id'];
        $rows = fetch_rows($qry);
        foreach ( $rows as $row ) {
            $id = $row['id'];
            $codice = $row['codice'];
            $nome = $row['nome'];
            $descrizione = $row['descrizione'];
            $qtainput = $row['qtainput'];
            $qtaselect = $row['qtaselect'];
            $qtamin = $row['qtamin'];
            $qtamax = $row['qtamax'];
            $coloreselect = $row['coloreselect'];
            $grigliacoloretaglie = $row['grigliacoloretaglie'];
            $id_articolo_equivalente1 = $row['id_articolo_equivalente1'];
            $id_articolo_equivalente2 = $row['id_articolo_equivalente2'];
            $online = $row['online'];
            $immagine1 = $row['immagine1'];
            $immagine2 = $row['immagine2'];
        }
        if (($qtainput == 1) && ($qtaselect == 0) && ($coloreselect == 0) && ($grigliacoloretaglie == 0)) { $tipoform = "1";  $bordo1  = " border: solid 2px red; "; }
        if (($qtainput == 1) && ($qtaselect == 0) && ($coloreselect == 1) && ($grigliacoloretaglie == 0)) { $tipoform = "1b"; $bordo1b = " border: solid 2px red; "; }
        if (($qtainput == 0) && ($qtaselect == 1) && ($coloreselect == 0) && ($grigliacoloretaglie == 0)) { $tipoform = "2";  $bordo2  = " border: solid 2px red; "; }
        if (($qtainput == 0) && ($qtaselect == 1) && ($coloreselect == 1) && ($grigliacoloretaglie == 0)) { $tipoform = "2b"; $bordo2b = " border: solid 2px red; "; }
        if (($qtainput == 1) && ($qtaselect == 0) && ($coloreselect == 0) && ($grigliacoloretaglie == 1)) { $tipoform = "3";  $bordo3  = " border: solid 2px red; "; }
    }
?>

<body class="navbar-top<?php if ($settings_menu_closed == 1) { echo ' sidebar-xs'; } ?>">
    <div id="imagePreviewDiv" class="modal modal-xl fade imgpreview" style="z-index: 9999;"></div>
    <?php include "sys_navbar.php"; ?>
    <div class="page-content">
        <?php
        include "sys_sidebar.php";
        if (!isset($_GET['id'])) {
            if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(2, "create")) {die();}}
        } else {
            if (!checkUserRight(2, "edit")) {echo "<style>.newrow,.icon-trash-empty,.dropzone,.savebutton{display: none;}</style>";}
        }
        ?>
        <div class="content-wrapper">

        <div class="page-header page-header-light">
            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">

                <div class="d-flex">
                    <i class="icon-t-shirt mr-2 breadcrumb-item"></i>
                    <span style="margin-top: 8px;">
                    <?php
                        if (isset($_GET['id'])) {
                            if (checkUserRight(2, "edit")) {
                                echo "Modifica Articolo";
                            } else {
                                echo "Visualizza Articolo";
                            }
                        } else {
                            echo "Nuovo Articolo";
                        }
                    ?>
                    </span>
                </div>

                <div class="header-elements d-none" style="display: flex !important;">
                    <div class="breadcrumb justify-content-center">
                        
                    </div>
                </div>
            </div>
        </div>

<div class="content unselectable">
    <input type="hidden" id="id" <?php if(isset($_GET['id'])) echo 'value="'.$_GET['id'].'"'; ?> >

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                    Tipologia<br>di acquisto
                </div>
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                    <img id="tipo1"  onclick="selezionatipo('1');" src="assets/images/tipo1.png"  style="padding: 2px; <?=$bordo1?>" >
                </div>
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                    <img id="tipo1b" onclick="selezionatipo('1b');" src="assets/images/tipo1b.png" style="padding: 2px; <?=$bordo1b?>" >
                </div>
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                    <img id="tipo2"  onclick="selezionatipo('2');" src="assets/images/tipo2.png"  style="padding: 2px; <?=$bordo2?>" >
                </div>
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                    <img id="tipo2b" onclick="selezionatipo('2b');" src="assets/images/tipo2b.png" style="padding: 2px; <?=$bordo2b?>" >
                </div>
                <div class="col-lg-2 col-md-2 col-sm-12 col-xs-12">
                    <img id="tipo3"  onclick="selezionatipo('3');" src="assets/images/tipo3.png"  style="width: 100%; padding: 2px; <?=$bordo3?>">
                </div>
            </div>
        </div>
    </div>

    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                    <div class="form-group container_fields">
                        <label class="text-bold">Codice</label>
                        <input type="text" id="codice"  maxlength="50"  autocomplete="off" class="form-control" value="<?=$codice?>" >
                    </div>
                </div>

                <div class="col-lg-9 col-md-8 col-sm-8 col-xs-12">
                    <div class="form-group container_fields">
                        <label class="text-bold">Nome</label>
                        <input type="text" id="nome"  maxlength="255"  autocomplete="off" class="form-control" value="<?=$nome?>" >
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group container_fields">
                        <label class="text-bold">Descrizione</label>
                        <textarea id="descrizione" rows="8" maxlength="10000" autocomplete="off" class="form-control" style="border: none;" ><?=$descrizione?></textarea>
                    </div>
                </div>
            </div>

            <div class="row">
                <input type="hidden" id="qtainput"            value="<?=$qtainput?>">
                <input type="hidden" id="qtaselect"           value="<?=$qtaselect?>">
                <input type="hidden" id="coloreselect"        value="<?=$coloreselect?>">
                <input type="hidden" id="grigliacoloretaglie" value="<?=$grigliacoloretaglie?>">

                <div class="col-lg-1 col-md-2 col-sm-6 col-xs-6">
                    <div class="form-group container_fields">
                        <label class="text-bold">Qtamin</label>
                        <input type="number" style="text-align:center;" step="1" id="qtamin"    onkeypress="return checkInteger(event)" autocomplete="off" class="form-control" value="<?=$qtamin?>">
                    </div>
                </div>

                <div class="col-lg-1 col-md-2 col-sm-6 col-xs-6">
                    <div class="form-group container_fields">
                        <label class="text-bold">Qtamax</label>
                        <input type="number" style="text-align:center;" step="1" id="qtamax"    onkeypress="return checkInteger(event)" autocomplete="off" class="form-control" value="<?=$qtamax?>">
                    </div>
                </div>

                <div class="col-lg-5 col-md-4 col-sm-6 col-xs-6"><div class="form-group container_fields">
                    <label class="text-bold">Articolo equivalente 1</label>
                    <select class="form-control select" id="id_articolo_equivalente1"  >
                    <option value=""></option>
                    <?php
                        $query = $db->query("SELECT id, CONCAT(codice, ' - ', nome) as nome FROM articolo ORDER BY codice");
                        while($risultati = mysqli_fetch_array($query)) {
                            if ($risultati["id"] == $id_articolo_equivalente1) {
                                $opzione_selezionata = " SELECTED ";
                            } else {
                                $opzione_selezionata = " ";
                            }
                            echo '<option value="'.$risultati["id"].'" '.$opzione_selezionata.'>'.$risultati["nome"].'</option>';
                        }
                    ?>
                    </select>
                </div></div>

                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6"><div class="form-group container_fields">
                    <label class="text-bold">Articolo equivalente 2</label>
                    <select class="form-control select" id="id_articolo_equivalente2"  >
                    <option value=""></option>
                    <?php
                        $query = $db->query("SELECT id, CONCAT(codice, ' - ', nome) as nome FROM articolo ORDER BY codice");
                        while($risultati = mysqli_fetch_array($query)) {
                            if ($risultati["id"] == $id_articolo_equivalente2) {
                                $opzione_selezionata = " SELECTED ";
                            } else {
                                $opzione_selezionata = " ";
                            }
                            echo '<option value="'.$risultati["id"].'" '.$opzione_selezionata.'>'.$risultati["nome"].'</option>';
                        }
                    ?>
                    </select>
                </div></div>

                <div class="col-lg-1 col-md-2 col-sm-4 col-xs-4">
                    <div class="form-group container_fields">
                        <label class="text-bold">Online</label><br>
                        <?php
                        if ($online == 1) {
                            $opzione_selezionata = " CHECKED ";
                        } else {
                            $opzione_selezionata = " ";
                        }
                        ?>
                        <input type="checkbox" id="online" class="form-control" value="1" style="width: 28px; margin: 0;" <?=$opzione_selezionata?> >
                    </div>
                </div>

            </div>

            <?php if ($id != "") { ?>
            <div class="row" >
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group container_fields">
                        <label class="text-bold">Immagine 1</label><br>
                        <input style="display:inline-block;" type="file" id="immagine1"><br>
                        <input type="hidden" id="immagine1_old" <?php if (strlen($immagine1) > 0) { echo ' value="'.$immagine1.'"'; } ?>>
                        <img id="prima_immagine" <?php if (strlen($immagine1) > 0) { echo 'src="'.$immagine1.'"'; } ?> style="padding: 10px; max-height:300px; max-width:300px; height:auto; width:auto;"><br>
                        <?php if (strlen($immagine1) > 0) { ?>
                        <i id="elimina_immagine1" class="icon-trash" style="float: right; margin-top: -20px;" onclick="$('#elimina_immagine1').css('display', 'none'); $('#immagine1_old').val(''); $('#prima_immagine').css('display', 'none');"></i>
                        <?php } ?>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group container_fields">
                        <label class="text-bold">Immagine 2</label><br>
                        <input style="display:inline-block;" type="file" id="immagine2"><br>
                        <input type="hidden" id="immagine2_old" <?php if (strlen($immagine2) > 0) { echo ' value="'.$immagine2.'"'; } ?>>
                        <img id="seconda_immagine" <?php if (strlen($immagine2) > 0) { echo 'src="'.$immagine2.'"'; } ?> style="padding: 10px; max-height:300px; max-width:300px; height:auto; width:auto;"><br>
                        <?php if (strlen($immagine2) > 0) { ?>
                        <i id="elimina_immagine2" class="icon-trash" style="float: right; margin-top: -20px;" onclick="$('#elimina_immagine2').css('display', 'none'); $('#immagine2_old').val(''); $('#seconda_immagine').css('display', 'none');"></i>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <?php } ?>


            <div class="row">
                <div class="col-lg-12">
                    <div class="text-right">
                        <button onclick="event.preventDefault(); save_record();" class="btn btn-lg btn-success savebutton"><i class="icon-ok-3"></i> Salva</button>
                        <button onclick="event.preventDefault(); document.location.href='articoli.php'" class="btn btn-lg btn-warning"><i class="icon-undo"></i> Chiudi</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?php
    // DA QUI IN POI SOLO SE SIAMO IN MODIFICA ARTICOLO
    if ($id != "") { ?>

    <div class="row">
        <div class="col-lg-9 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <span style="padding: 10px; font-variant: small-caps;">Caratteristiche</span>
                <div class="card-body">
                    <div class="row">
                        <?php
                            $qry = "SELECT * FROM categoria ORDER BY ordine";
                            $rows = fetch_rows($qry);
                            foreach ( $rows as $row ) {
                                ?>
                                    <div class="col-lg-1 col-md-2 col-sm-3 col-xs-3">
                                        <div class="form-group">
                                            <span style="line-height: 33px;"><?=$row['descrizione']?></span>
                                        </div>
                                    </div>
                                    <div class="col-lg-5 col-md-4 col-sm-9 col-xs-9">
                                        <div class="form-group">
                                            
                                            <?php if (strlen($row['um']) == 0) { ?>
                                                <select class="form-control select valori_categoria" id="categoria_<?=$row['id']?>"  >
                                                <option value=""></option>
                                                <?php
                                                    $query = $db->query("SELECT id, descrizione FROM row_categoria WHERE id_categoria = ".$row['id']." ORDER BY ordine");
                                                    while($values = mysqli_fetch_array($query)) {
                                                        $opzione_selezionata = "";
                                                        $query2 = $db->query("SELECT * FROM categoriaxarticolo WHERE id_articolo = ".$id." AND id_row_categoria = ".$values['id']);
                                                        while($selezionato = mysqli_fetch_array($query2)) {
                                                            $opzione_selezionata = " SELECTED ";
                                                        }
                                                        echo '<option value="'.$values["id"].'" '.$opzione_selezionata.'>'.$values["descrizione"].'</option>';
                                                    }
                                                ?>
                                                </select>
                                            <?php } else { 
                                                $valore = "";
                                                $query2 = $db->query("SELECT valore FROM categoriaxarticolo WHERE id_articolo = ".$id." AND id_categoria = ".$row['id']);
                                                while($selezionato = mysqli_fetch_array($query2)) {
                                                    $valore = $selezionato['valore'];
                                                }
                                                ?>
                                                <input type="number" step="0.01" style="width: 100px; display: inline-block;" class="form-control valori_categoria" id="categoria_<?=$row['id']?>" value="<?=$valore?>" > <span><?=$row['um']?></span>
                                            <?php } ?>
                                        </div>
                                    </div>
                                <?php
                            }
                        ?>
                    </div>
                </div>
            </div>
        </div>


        <?php if ($tipoform == "3") { ?>
        <div class="col-lg-3 col-md-12 col-sm-12 col-xs-12">
            <div class="card">
                <span style="padding: 10px; font-variant: small-caps;">Taglie e dimensioni</span>
                <div class="card-body" style="padding-top: 0px; padding-bottom: 6px;">
                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="text-align:center;">Taglia</div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="text-align:center;">Larghezza</div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4" style="text-align:center;">Altezza</div>
                    </div>
                
                    <?php
                        $conta_dimensioni = 0;
                        $qry = "SELECT * FROM dimensioni WHERE id_articolo = ".$id." ORDER BY id";
                        $rows = fetch_rows($qry);
                        foreach ( $rows as $row ) {
                            $conta_dimensioni += 1;
                            ?>
                                <div class="row" style="height: 35px;">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <div class="form-group">
                                            <input type="hidden" id="id_dimensioni_<?=$conta_dimensioni?>" value="<?=$row['id']?>">
                                            <input type="text" class="form-control" style="width: 100%; height: 30px;" id="taglia_<?=$conta_dimensioni?>" value="<?=$row['taglia']?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <div class="form-group">
                                            <input type="number" class="form-control" style="width: 100%; height: 30px;" step="0.01" id="larghezza_<?=$conta_dimensioni?>" value="<?=($row['larghezza'] == 0 ? "" : $row['larghezza'])?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <div class="form-group">
                                            <input type="number" class="form-control" style="width: 100%; height: 30px;" step="0.01" id="altezza_<?=$conta_dimensioni?>" value="<?=($row['altezza'] == 0 ? "" : $row['altezza'])?>">
                                        </div>
                                    </div>
                                </div>
                            <?php
                        }

                        $conta_dimensioni += 1;
                        for ($i = $conta_dimensioni; $i <= 10; $i++) {
                            ?>
                                <div class="row" style="height: 35px;">
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <div class="form-group">
                                            <input type="hidden" id="id_dimensioni_<?=$conta_dimensioni?>">
                                            <input type="text" class="form-control" style="width: 100%; height: 30px;" id="taglia_<?=$conta_dimensioni?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <div class="form-group">
                                            <input type="number" class="form-control" style="width: 100%; height: 30px;" step="0.01" id="larghezza_<?=$conta_dimensioni?>">
                                        </div>
                                    </div>
                                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                                        <div class="form-group">
                                            <input type="number" class="form-control" style="width: 100%; height: 30px;" step="0.01" id="altezza_<?=$conta_dimensioni?>">
                                        </div>
                                    </div>
                                </div>
                            <?php
                            $conta_dimensioni += 1;
                        }
                    ?>
                </div>
            </div>
        </div>
        <?php } ?>




        <?php if (($tipoform == "1b") || ($tipoform == "2b") || ($tipoform == "3")) { ?>
        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
            <div class="card">
                <span style="padding: 10px; font-variant: small-caps;">Colori<button type="button" class="btn btn-success" style="float: right;" onclick="aggiungi_colore();">Aggiungi colore</button><br></span>
                <div class="card-body" style="padding-top: 0px; padding-bottom: 6px;">
                    <table id="tabella_colori"></table>
                </div>
            </div>
        </div>
        <?php } ?>



        <?php if (($tipoform == "2") || ($tipoform == "2b")) { ?>
        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
            <div class="card" style="padding-bottom: 12px;">
                <span style="padding: 10px; font-variant: small-caps;">Quantità possibili</span>
                <div class="row card-body" style="padding-top: 0px; padding-bottom: 6px;">
                    <?php
                        $i = 0;
                        $rows = fetch_rows("SELECT * FROM qtaselect WHERE id_articolo = ".$id." ORDER BY quantita");
                        foreach ( $rows as $row ) {
                            $i += 1;
                            ?>
                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4">
                                <input type="number" class="form-control" style="margin-bottom: 6px; width: 100%;" value="<?=$row['quantita']?>" id="qta<?=$i?>">
                            </div>
                        <?php
                        }
                        $i += 1;
                    ?>   
                    <?php for ($j = $i; $j <= 20; $j++) { ?>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4">
                        <input type="number" class="form-control" style="margin-bottom: 6px; width: 100%;" id="qta<?=$j?>">
                        </div>
                    <?php } ?>
                </div>
            </div>
        </div>
        <?php } ?>

        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
            <div class="card" style="padding-bottom: 12px;">
                <span style="padding: 10px; font-variant: small-caps;">Lavorazioni disponibili</span>
                <div class="card-body" style="padding-top: 0px; padding-bottom: 6px;">
                    <?php
                        $i = 0;
                        $rows = fetch_rows("SELECT * FROM servizio ORDER BY neutro, ordine");
                        foreach ( $rows as $row ) {
                            $i += 1;
                            ?>
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <input type="hidden" value="<?=$row['id']?>" id="servizio_id_<?=$i?>">
                                    <input type="checkbox" class="form-control servizi_id" style="" value="<?=$row['id']?>" <?php
                                        $selected = "";
                                        $rows_sel = fetch_rows("SELECT * FROM servizioxarticolo WHERE id_articolo = ".$id." AND id_servizio = ".$row['id']);
                                        foreach ( $rows_sel as $row_sel ) {
                                            $selected = " CHECKED ";
                                        }
                                        echo $selected;
                                    ?> id="servizio_selezionato_<?=$i?>" name="servizio_selezionato_<?=$i?>">
                                    <?php
                                        $neutro = "";
                                        if ($row['neutro'] == 1) { $neutro = "N"; }
                                    ?>
                                    <label style="display:inline-block; width:20px; margin-left:10px; font-size: 10px; font-weight:bold;" for="servizio_neutro_<?=$i?>"><?=$neutro?></label>
                                    <label for="servizio_selezionato_<?=$i?>"><?=$row['descrizione']?></label>
                                </div>
                            </div>
                        <?php
                        }
                        $i += 1;
                    ?>
                </div>
            </div>
        </div>



        <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
            <div class="card" style="padding-bottom: 12px;">
                <span style="padding: 10px; font-variant: small-caps;">Dimensioni di stampa</span>
                <div class="card-body" style="padding-top: 0px; padding-bottom: 6px;">
                    <?php
                        $rows = fetch_rows("SELECT prezzostampa.id_lato, lato.descrizione FROM prezzostampa LEFT JOIN lato ON lato.id = prezzostampa.id_lato WHERE prezzostampa.id_articolo = ".$id." GROUP BY prezzostampa.id_lato, lato.descrizione ORDER BY lato.ordine");
                        foreach ( $rows as $row ) {
                            ?>
                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                    <?php
                                    $testo_dimensioni = "";
                                    $rows_dim = fetch_rows("SELECT * FROM dimensione_stampa WHERE id_articolo = ".$id." AND id_lato = ".$row['id_lato']);
                                    foreach ( $rows_dim as $row_dim ) {
                                        $testo_dimensioni = $row_dim['descrizione'];
                                    }
                                    ?>
                                    <label style="width:150px; margin: 8px 0px 0px 0px;"><?=$row['descrizione']?></label><input type="text" maxlength="250" class="form-control dimensioni_stampa" style="" value="<?=str_replace('"', '&quot;', $testo_dimensioni)?>" id="dimensioni_lato_<?=$row['id_lato']?>" name="dimensioni_lato_<?=$row['id_lato']?>">
                                </div>
                            </div>
                        <?php
                        }
                    ?>
                </div>
            </div>
        </div>


    </div>

    <?php } ?>
</div>



<div id="modal_colore" class="modal fade" style="-webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none;" unselectable="on">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-primary">
                <h6 class="modal-title"><i class="icon-paint"></i> Colore</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
            <div class="modal-body">
                <div class="content">
                    <input type="hidden" id="colore_id" value="" >
                    
                    <div class="row form-group">
                        <div class="col-lg-4">
                            <label class="text-bold" style="margin: 9px 0px 0px 0px;">Descrizione</label>
                        </div>
                        <div class="col-lg-8" style="margin-top: 3px;">
                            <input style="width:100%;" type="text" id="colore_descrizione" value="" class="form-control" autocomplete="off" maxlength="255"  >
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-lg-4">
                            <label class="text-bold" style="margin: 9px 0px 0px 0px;">RGB</label>
                        </div>
                        <div class="col-lg-8" style="margin-top: 3px;">
                            <input style="width:100px;" type="text" id="colore_rgb" value="" class="form-control" autocomplete="off" maxlength="6"  >
                        </div>
                    </div>


                    <div class="row form-group">
                        <div class="col-lg-4">
                            <label class="text-bold" style="margin: 9px 0px 0px 0px;">Pittogramma</label>
                        </div>
                        <div class="col-lg-8" style="margin-top: 3px;">
                            <input type="file" id="colore_texture">
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-lg-4">
                            <label class="text-bold" style="margin: 9px 0px 0px 0px;">Immagine</label>
                        </div>
                        <div class="col-lg-8" style="margin-top: 3px;">
                            <input type="file" id="colore_immagine">
                        </div>
                    </div>



                </div>
            </div>
			<div class="modal-footer">
                <button onclick="salva_colore();" class="btn btn-lg btn-success savebutton"><i class="icon-ok-3"></i> Salva</button>
                <button onclick="$('#modal_colore').modal('toggle');" class="btn btn-lg btn-warning"><i class="icon-undo"></i> Chiudi</button>
			</div>
		</div>
	</div>
</div>

</div>
</div>
</body>


<script type="text/javascript">


    $(document).ready(function () {
        $('.select').select2({ width: '100%' });

        <?php if (($tipoform == "1b") || ($tipoform == "2b") || ($tipoform == "3")) { ?>
            aggiorna_tabella_colori();
        <?php } ?>

    });

    function aggiungi_colore() {
        $('#colore_id').val('');
        $('#colore_descrizione').val('');
        $('#colore_rgb').val('');
        $("#colore_texture").val(null);
        $("#colore_immagine").val(null);
        $('#modal_colore').modal({backdrop: 'static'});
    }

    function modifica_colore(id_colore) {
        $.ajax({
            async: false,
            type: "POST",
            url: "controllers/articoli_controller.php",
            data: {
                "action": "modifica_colore",
                "colore_id": id_colore
            },
            success: function (data, stato) {
                console.log(data);
                var data = $.parseJSON(data);
                $('#colore_id').val(data.id);
                $('#colore_descrizione').val(data.descrizione);
                $('#colore_rgb').val(data.rgb);
                $("#colore_texture").val(null);
                $("#colore_immagine").val(null);
                //$('#colore_preview_texture').val('');
                //$('#colore_preview_immagine').val('');
                $('#modal_colore').modal({backdrop: 'static'});
            }
        });
    }

    function salva_colore() {
        $.ajax({
            async: false,
            type: "POST",
            url: "controllers/articoli_controller.php",
            data: {
                "action": "salva_colore",
                "id_articolo": $("#id").val(), 
                "colore_id": $("#colore_id").val(), 
                "colore_descrizione": $("#colore_descrizione").val(), 
                "colore_rgb": $("#colore_rgb").val(), 
            },
            success: function (data, stato) {
                if (data.substr(0,2) != "OK") {
                    swal({
                        title: "Error",
                        text: "Qualcosa è andato storto. " + data.toString(),
                        confirmButtonColor: "#66BB6A",
                        type: "warning"
                    });
                } else {
                    var id_elaborato = data.substr(2);

                    if ($("#colore_texture")[0]['files']['length'] > 0) {
                        var fdata = new FormData();
                        fdata.append("file",     $("#colore_texture")[0]['files'][0]);
                        fdata.append("tipofile",  "texture");
                        fdata.append("colore_id", id_elaborato);
                        fdata.append("action",    "salva_immagine_colore");
                        $.ajax({
                            url: "controllers/articoli_controller.php",
                            type: "post",
                            data: fdata,
                            async: false,
                            processData: false,
                            contentType: false,
                            success: function (response, status, jqxhr) { },
                            error: function (jqxhr, status, errorMessage) { alert(errorMessage); }
                        });
                    }
                    
                    

                    if ($("#colore_immagine")[0]['files']['length'] > 0) {
                        var fdata = new FormData();
                        fdata.append("file",     $("#colore_immagine")[0]['files'][0]);
                        fdata.append("tipofile",  "immagine");
                        fdata.append("colore_id", id_elaborato);
                        fdata.append("action",    "salva_immagine_colore");
                        $.ajax({
                            url: "controllers/articoli_controller.php",
                            type: "post",
                            data: fdata,
                            async: false,
                            processData: false,
                            contentType: false,
                            success: function (response, status, jqxhr) { },
                            error: function (jqxhr, status, errorMessage) { alert(errorMessage); }
                        });
                    }
                    
                    $('#modal_colore').modal('toggle');                    
                    aggiorna_tabella_colori();                    
                }
            }
        });
    }

    function aggiorna_tabella_colori() {
        $.ajax({
            async: false,
            type: "POST",
            url: "controllers/articoli_controller.php",
            data: {
                "action": "aggiorna_tabella_colori",
                "id_articolo": $("#id").val(), 
            },
            success: function (data, stato) {
                $('#tabella_colori').html(data);
            }
        });
    }

    
    var id_colore_texture_da_cancellare = 0;
    function colore_elimina_texture(id_colore) {
        id_colore_texture_da_cancellare = id_colore;
        true_false_box("Rimuovi pittogramma", "Confermi di voler eliminare il pittogramma selezionato?", "Si, elimina", "No, annulla", "warning", colore_elimina_texture2)
    }
    function colore_elimina_texture2(risp) {
        if (risp) {
            if (id_colore_texture_da_cancellare > 0) {
                $.ajax({
                    async: false,
                    type: "POST",
                    url: "controllers/articoli_controller.php",
                    data: {
                        "action": "colore_elimina_texture",
                        "id_colore": id_colore_texture_da_cancellare, 
                    },
                    success: function (data, stato) {
                        aggiorna_tabella_colori();
                    }
                });
            }
        }
    }

    var id_colore_immagine_da_cancellare = 0;
    function colore_elimina_immagine(id_colore) {
        id_colore_immagine_da_cancellare = id_colore;
        true_false_box("Rimuovi immagine", "Confermi di voler eliminare l'immagine selezionata?", "Si, elimina", "No, annulla", "warning", colore_elimina_immagine2)
    }
    function colore_elimina_immagine2(risp) {
        if (risp) {
            if (id_colore_immagine_da_cancellare > 0) {
                $.ajax({
                    async: false,
                    type: "POST",
                    url: "controllers/articoli_controller.php",
                    data: {
                        "action": "colore_elimina_immagine",
                        "id_colore": id_colore_immagine_da_cancellare, 
                    },
                    success: function (data, stato) {
                        aggiorna_tabella_colori();
                    }
                });
            }
        }
    }

    var id_colore_da_cancellare = 0;
    function elimina_colore(id_colore) {
        id_colore_da_cancellare = id_colore;
        true_false_box("Rimuovi colore", "Confermi di voler eliminare il colore selezionato?", "Si, elimina", "No, annulla", "warning", elimina_colore2)
    }
    function elimina_colore2(risp) {
        if (risp) {
            if (id_colore_da_cancellare > 0) {
                $.ajax({
                    async: false,
                    type: "POST",
                    url: "controllers/articoli_controller.php",
                    data: {
                        "action": "elimina_colore",
                        "id_colore": id_colore_da_cancellare, 
                    },
                    success: function (data, stato) {
                        aggiorna_tabella_colori();
                    }
                });
            }
        }
    }

    var id_articolo = "<?php if (isset($_GET["id"])) { echo $_GET["id"]; } ?>";
    var tipoform = "<?=$tipoform?>";

    function selezionatipo(tiposelezionato) {
        if (id_articolo == "") {
            if (tipoform == "") {
                tipoform = tiposelezionato;
                if (tipoform == '1')  { $('#tipo1').css('border', 'solid 2px red');   $('#qtainput').val('1'); $('#qtaselect').val('0'); $('#coloreselect').val('0'); $('#grigliacoloretaglie').val('0'); }
                if (tipoform == '1b') { $('#tipo1b').css('border', 'solid 2px red');  $('#qtainput').val('1'); $('#qtaselect').val('0'); $('#coloreselect').val('1'); $('#grigliacoloretaglie').val('0'); }
                if (tipoform == '2')  { $('#tipo2').css('border', 'solid 2px red');   $('#qtainput').val('0'); $('#qtaselect').val('1'); $('#coloreselect').val('0'); $('#grigliacoloretaglie').val('0'); }
                if (tipoform == '2b') { $('#tipo2b').css('border', 'solid 2px red');  $('#qtainput').val('0'); $('#qtaselect').val('1'); $('#coloreselect').val('1'); $('#grigliacoloretaglie').val('0'); }
                if (tipoform == '3')  { $('#tipo3').css('border', 'solid 2px red');   $('#qtainput').val('1'); $('#qtaselect').val('0'); $('#coloreselect').val('0'); $('#grigliacoloretaglie').val('1'); }
            }
        }
    }

    function save_record() {

        if ( $("#id").val() != "") {

            var categorie = [];
            $('.valori_categoria').each(function(){ categorie.push( [ $(this).attr("id").replace("categoria_", "") , $(this).val() ] ); });

            var taglie = [];
            for (i = 1; i < 11; i++) {
                taglie.push([ $('#id_dimensioni_'+i).val(), $('#taglia_'+i).val(), $('#larghezza_'+i).val(), $('#altezza_'+i).val() ]);
            }
            
            var qta = [];
            for (i = 1; i < 21; i++) {
                if ($('#qta'+i).val() > 0) {
                    qta.push($('#qta'+i).val());
                }
            }

            var servizi = [];
            $('.servizi_id').each(function( index ) {
                if (ischecked( "#".concat($(this).attr('id')))) { servizi.push($(this).val()); }
            });


            var dimensioni_stampa = [];
            $('.dimensioni_stampa').each(function(){ dimensioni_stampa.push( [ $(this).attr("id").replace("dimensioni_lato_", "") , $(this).val() ] ); });


            $.ajax({
                async: false,
                type: "POST",
                url: "controllers/articoli_controller.php",
                data: {
                    "action": "save_record",
                    "id": $("#id").val(), 
                    "codice": $("#codice").val(), 
                    "nome": $("#nome").val(), 
                    "descrizione": $("#descrizione").val(), 
                    "qtainput": $("#qtainput").val(), 
                    "qtaselect": $("#qtaselect").val(), 
                    "qtamin": $("#qtamin").val(), 
                    "qtamax": $("#qtamax").val(), 
                    "coloreselect": $("#coloreselect").val(), 
                    "grigliacoloretaglie": $("#grigliacoloretaglie").val(), 
                    "id_articolo_equivalente1": $("#id_articolo_equivalente1").val(), 
                    "id_articolo_equivalente2": $("#id_articolo_equivalente2").val(), 
                    "online": ischecked("#online"), 
                    "categorie": categorie,
                    "taglie": taglie,
                    "immagine1": $("#immagine1_old").val(), 
                    "immagine2": $("#immagine2_old").val(), 
                    "qta": qta,
                    "servizi": servizi,
                    "dimensioni_stampa": dimensioni_stampa,
                },
                success: function (data, stato) {
                    if (data.substr(0,2) != "OK") {
                        swal({
                            title: "Error",
                            text: "Qualcosa è andato storto " + data.toString(),
                            confirmButtonColor: "#66BB6A",
                            type: "warning"
                        });
                    } else {

                        
                        if ($("#immagine1")[0]['files']['length'] > 0) {
                            var fdata = new FormData();
                            fdata.append("file",     $("#immagine1")[0]['files'][0]);
                            fdata.append("tipofile",  "immagine1");
                            fdata.append("id", $("#id").val());
                            fdata.append("action",    "salva_immagine");
                            $.ajax({
                                url: "controllers/articoli_controller.php",
                                type: "post",
                                data: fdata,
                                async: false,
                                processData: false,
                                contentType: false,
                                success: function (response, status, jqxhr) { },
                                error: function (jqxhr, status, errorMessage) { alert(errorMessage); }
                            });
                        }
                    
                        if ($("#immagine2")[0]['files']['length'] > 0) {
                            var fdata = new FormData();
                            fdata.append("file",     $("#immagine2")[0]['files'][0]);
                            fdata.append("tipofile",  "immagine2");
                            fdata.append("id", $("#id").val());
                            fdata.append("action",    "salva_immagine");
                            $.ajax({
                                url: "controllers/articoli_controller.php",
                                type: "post",
                                data: fdata,
                                async: false,
                                processData: false,
                                contentType: false,
                                success: function (response, status, jqxhr) { },
                                error: function (jqxhr, status, errorMessage) { alert(errorMessage); }
                            });
                        }
                    


                        location.href = 'articoli.php'; 
                    }
                }
            });

        } else {
            if (tipoform == "") {
                toast("Seleziona la Tipologia di acquisto", "error"); return;
            }

            $.ajax({
                async: false,
                type: "POST",
                url: "controllers/articoli_controller.php",
                data: {
                    "action": "save_record",
                    "id": $("#id").val(), 
                    "codice": $("#codice").val(), 
                    "nome": $("#nome").val(), 
                    "descrizione": $("#descrizione").val(), 
                    "qtainput": $("#qtainput").val(), 
                    "qtaselect": $("#qtaselect").val(), 
                    "qtamin": $("#qtamin").val(), 
                    "qtamax": $("#qtamax").val(), 
                    "coloreselect": $("#coloreselect").val(), 
                    "grigliacoloretaglie": $("#grigliacoloretaglie").val(), 
                    "id_articolo_equivalente1": $("#id_articolo_equivalente1").val(), 
                    "id_articolo_equivalente2": $("#id_articolo_equivalente2").val(), 
                    "online": ischecked("#online"), 
                },
                success: function (data, stato) {
                    if (data.substr(0,2) != "OK") {
                        swal({
                            title: "Error",
                            text: "Qualcosa è andato storto " + data.toString(),
                            confirmButtonColor: "#66BB6A",
                            type: "warning"
                        });
                    } else {
                        var id_elaborato = data.substr(2);
                        location.href = 'articoli_edit.php?id='.concat(id_elaborato); 
                        
                    }
                }
            });
        }
    }


</script>