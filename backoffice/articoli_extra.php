<?php
    include "sys_header.php";
    if ($_SESSION["user_username"] != "sysadmin") {header("location: ".linkto("sys_dashboard")); die();}
?>
<body class="navbar-top<?php if ($settings_menu_closed == 1) { echo ' sidebar-xs'; } ?>">
    <?php include "sys_navbar.php"; ?>
    <div class="page-content">
        <?php include "sys_sidebar.php"; ?>
        <div class="content-wrapper">
            <div class="page-header page-header-light">
				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					<div class="d-flex">
                        <i class="icon-money-2 mr-2 breadcrumb-item"></i><span style="margin-top: 8px;">Prezzi extra</span>
					</div>
				</div>
			</div>
            <div class="content">
                <?php
                    $id = $_GET['id'];
                    $rows = fetch_rows("SELECT * FROM articolo WHERE id = ".$id);
                    foreach ( $rows as $row ) {
                        echo "<span style='font-size:16px;'>".$row['codice']."</span><br>";
                        echo "<span style='font-size:16px;'>".$row['nome']."</span><br><br><br>";
                    } ?>
                <table>
                <tr style="font-weight:bold;">
                    <td style="font-size: 12px; width: 300px; line-height: 38px;">Titolo</td>
                    <td style="font-size: 12px; width: 300px; line-height: 38px;">Descrizione</td>
                    <td style="font-size: 12px; text-align:center; width: 100px; line-height: 38px;">Prezzo</td>
                    <td style="font-size: 12px; text-align:center; width: 100px; line-height: 38px;">Ordine</td>
                </tr>
                <?php
                    $rows = fetch_rows("SELECT * FROM serviziaggiuntivi WHERE id_articolo = ".$id." ORDER BY ordine");
                    $i = 0;
                    foreach ( $rows as $row ) {
                        echo "<tr style='height: 50px; border-bottom: 1px solid #c5c5c5; border-top: 1px solid #c5c5c5;'>";
                        echo '<td style="width:300px;"><input type="text" class="form-control" style="width:300px;font-size: 11px; margin-bottom: 6px;" value="'.$row['titolo'].'" id="titolo_'.$i.'"></td>';
                        echo '<td style="width:300px;"><input type="text" class="form-control" style="width:300px;font-size: 11px; margin-bottom: 6px;" value="'.$row['descrizione'].'" id="descrizione_'.$i.'"></td>';
                        echo '<td style="width:100px;"><input type="number" step="0.01" class="form-control" style="text-align:right; width:100px;font-size: 11px; margin-bottom: 6px;" value="'.$row['prezzounitario'].'" id="prezzo_'.$i.'"></td>';
                        echo '<td style="width:100px;"><input type="number" step="1"    class="form-control" style="text-align:center; width:100px;font-size: 11px; margin-bottom: 6px;" value="'.$row['ordine'].'" id="ordine_'.$i.'"></td>';
                        echo '</tr>';
                        $i += 1;
                    }
                    
                    for ($j = $i; $j < 10; $j++) {
                        echo "<tr style='height: 50px; border-bottom: 1px solid #c5c5c5; border-top: 1px solid #c5c5c5;'>";
                        echo '<td style="width:300px;"><input type="text" class="form-control" style="width:300px;font-size: 11px; margin-bottom: 6px;" value="" id="titolo_'.$j.'"></td>';
                        echo '<td style="width:300px;"><input type="text" class="form-control" style="width:300px;font-size: 11px; margin-bottom: 6px;" value="" id="descrizione_'.$j.'"></td>';
                        echo '<td style="width:100px;"><input type="number" step="0.01" class="form-control" style="text-align:right; width:100px;font-size: 11px; margin-bottom: 6px;" value="" id="prezzo_'.$j.'"></td>';
                        echo '<td style="width:100px;"><input type="number" step="1"    class="form-control" style="text-align:center; width:100px;font-size: 11px; margin-bottom: 6px;" value="" id="ordine_'.$j.'"></td>';
                        echo '</tr>';
                    }
                ?>
                </table><br>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text-right">
                            <button onclick="event.preventDefault(); save_record();" class="btn btn-lg btn-success savebutton"><i class="icon-ok-3"></i> Salva</button>
                            <button onclick="event.preventDefault(); document.location.href='articoli.php'" class="btn btn-lg btn-warning"><i class="icon-undo"></i> Chiudi</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
    function save_record() {
        var id_articolo = <?=$id?>;
        prezziextra = [];
        var i;
        
        for (i = 0; i < 10; i++) {
            if ($('#titolo_'.concat(i)).val() != '') {
                if ($('#descrizione_'.concat(i)).val() != '') {
                    prezziextra.push([ $('#titolo_'.concat(i)).val(), $('#descrizione_'.concat(i)).val(), $('#prezzo_'.concat(i)).val(), $('#ordine_'.concat(i)).val() ]);
                }
            }
        }

        $.ajax({
            async: false,
            type: "POST",
            url: "controllers/articoli_extra_controller.php",
            data: {
                "action": "salva",
                "id_articolo": id_articolo,
                "prezziextra": prezziextra,
            },
            success: function (data, stato) {
                if (data == "OK") {
                    document.location.href='articoli.php';
                } else {
                    swal({
                        title: "Ops !",
                        text: "Qualcosa è andato storto",
                        confirmButtonColor: "#2196F3",
                        type: "error"
                    });
                }
            }
        });

    }
    </script>
</body>
</html>