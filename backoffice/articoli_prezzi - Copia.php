<?php
    include "sys_header.php";
    if ($_SESSION["user_username"] != "sysadmin") {header("location: ".linkto("sys_dashboard")); die();}
?>
<body class="navbar-top<?php if ($settings_menu_closed == 1) { echo ' sidebar-xs'; } ?>">
    <?php include "sys_navbar.php"; ?>
    <div class="page-content">
        <?php include "sys_sidebar.php"; ?>
        <div class="content-wrapper">
            <div class="page-header page-header-light">
				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					<div class="d-flex">
                        <i class="icon-tag-1 mr-2 breadcrumb-item"></i><span style="margin-top: 8px;">Prezzi articolo</span>
					</div>
				</div>
			</div>


            <div class="content">

                <?php
                    $id = $_GET['id'];
                    $rows = fetch_rows("SELECT * FROM articolo WHERE id = ".$id);
                    foreach ( $rows as $row ) {
                        echo "<span style='font-size:16px;'>".$row['codice']."</span><br>";
                        echo "<span style='font-size:16px;'>".$row['nome']."</span><br><br><br>";
                        $qtaselect = $row['qtaselect'];
                        $coloreselect = $row['coloreselect'];
                        $grigliacoloretaglie = $row['grigliacoloretaglie'];
                    } ?>
                <table>
                <tr>
                    <th style="font-size: 12px; width: 300px; display: inline-block; line-height: 38px;">Colore / Scaglione (fino a)</th>
                    <?php
                        $scaglioni = array();
                        $i = 0;
                        $rows = fetch_rows("SELECT scaglione FROM prezzo WHERE id_articolo = ".$id." GROUP BY scaglione ORDER BY scaglione");
                        foreach ( $rows as $row ) {
                            $i += 1;
                            array_push($scaglioni, $row['scaglione']);
                            ?>
                            <th><input type="number" step="1" class="form-control" style="font-weight: bold; text-align:center; margin-bottom: 6px; width: 80px;" value="<?=$row['scaglione']?>" id="qta<?=$i?>"></th>
                        <?php
                        }

                        if (($i == 0) && ($qtaselect == 1)) {
                            // NESSUNO SCAGLIONE TROVATO NEL DB e ARTICOLO DI TIPO 2 / 2b (quantità da tendina fissa), ALLORA CARICO LE Q.TA' IMPOSTATE NELLA TABELLA qtaselect
                            $rows = fetch_rows("SELECT quantita FROM qtaselect WHERE id_articolo = ".$id." ORDER BY quantita");
                            foreach ( $rows as $row ) {
                                $i += 1;
                                array_push($scaglioni, $row['quantita']);
                                ?>
                                <th><input type="number" step="1" class="form-control" style="font-weight: bold; text-align:center; margin-bottom: 6px; width: 80px;" value="<?=$row['quantita']?>" id="qta<?=$i?>"></th>
                            <?php
                            }
                        }

                        $i += 1;

                        for ($j = $i; $j <= 20; $j++) { ?>
                            <th><input type="number" step="1" class="form-control" style="font-weight: bold; text-align:center; margin-bottom: 6px; width: 80px;" id="qta<?=$j?>"></th>
                        <?php
                            array_push($scaglioni, 0);
                        } ?>
                </tr>
                <?php
                echo "<script>var colori = [];</script>";

                if (($grigliacoloretaglie == 0) && ($coloreselect == 0)) {
                    // articolo di tipo 1 / 2
                    $rows = array();
                    $rows[0]['id'] = 0;
                    $rows[0]['rgb'] = "";
                    $rows[0]['texture'] = "";
                    $rows[0]['immagine'] = "";
                    $rows[0]['descrizione'] = "NEUTRO";
                } else {
                    // articolo di tipo 1b / 2b / 3
                    $rows = fetch_rows("SELECT * FROM colore WHERE id_articolo = ".$id." ORDER BY ordine");
                }
                
                $i = 0;
                foreach ( $rows as $row ) {
                    echo "<script>colori.push(".$row['id'].");</script>";
                    echo "<tr style='height: 50px; border-bottom: 1px solid #c5c5c5; border-top: 1px solid #c5c5c5;'><td>";
                    if ($row['rgb'] != "") {
                        echo "<img style='width:36px; height:36px; display: inline-block; background-color:#".$row['rgb'].";'>";
                    } else {
                        echo "<img style='width:36px; height:36px; display: inline-block; visibility: hidden;' >";
                    }
                    if ($row['texture'] != "") {
                        echo "<img style='width:36px; height:36px; display: inline-block;' src='".$row['texture']."'>";
                    } else {
                        echo "<img style='width:36px; height:36px; display: inline-block; visibility: hidden;' >";
                    }
                    if ($row['immagine'] != "") {
                        echo "<img style='width:36px; height:36px; display: inline-block;' src='".$row['immagine']."'>";
                    } else {
                        echo "<img style='width:36px; height:36px; display: inline-block; visibility: hidden;' >";
                    }
                    echo $row['descrizione'];

                    if (($grigliacoloretaglie == 1) || ($coloreselect == 1)) {
                        if ($i == 0) {
                            echo '<button class="btn btn-primary" style="padding: 1px; width: 57px; height: 18px; font-size: 8px; float: right; margin-top: -12px;" onclick="copiaTutto();">COPIA RIGA</button>';
                        }
                    }
                    echo "</td>";

                    /*
                    for ($j = 0; $j <= 19; $j++) {
                        $prezzi = fetch_rows("SELECT prezzounitario FROM prezzo WHERE id_articolo = ".$id." AND id_colore = ".$row['id']." AND scaglione = ".$scaglioni[$j]);
                        $prezzo_unitario = "";
                        foreach ( $prezzi as $prezzo) {
                            $prezzo_unitario = $prezzo['prezzounitario'];
                        }
                        echo '<td>';
                        if (($grigliacoloretaglie == 1) || ($coloreselect == 1)) {
                            if ($i == 0) {
                                echo '<button class="btn btn-primary" tabindex="-1" style="margin-bottom: 3px; padding: 1px; width: 100%; height: 18px; font-size: 8px;" onclick="copia('.$j.');">COPIA</button>';
                            }
                        }
                        echo '<input type="number" step="0.01" class="form-control colonna'.$j.' scaglioneprezzo" style="font-size: 11px; text-align:center; margin-bottom: 6px; width: 80px;" value="'.$prezzo_unitario.'" id="prezzo_'.$i.'_'.$j.'">';
                        echo '</td>';
                    }
                    */
                    
                    $prezzi = fetch_rows("SELECT scaglione, prezzounitario FROM prezzo WHERE id_articolo = ".$id." AND id_colore = ".$row['id']);
                    $prezzo_unitario = array();
                    foreach ( $prezzi as $prezzo) {
                        $prezzo_unitario[$prezzo['scaglione']] = $prezzo['prezzounitario'];
                    }
                    
                    for ($j = 0; $j <= 19; $j++) {
                        $pr_unit = "";
                        if (isset($prezzo_unitario[$scaglioni[$j]])) { $pr_unit = $prezzo_unitario[$scaglioni[$j]]; }

                        echo '<td>';
                        if (($grigliacoloretaglie == 1) || ($coloreselect == 1)) {
                            if ($i == 0) {
                                echo '<button class="btn btn-primary" tabindex="-1" style="margin-bottom: 3px; padding: 1px; width: 100%; height: 18px; font-size: 8px;" onclick="copia('.$j.');">COPIA</button>';
                            }
                        }
                        echo '<input type="number" step="0.01" class="form-control colonna'.$j.' scaglioneprezzo" style="font-size: 11px; text-align:center; margin-bottom: 6px; width: 80px;" value="'.$pr_unit.'" id="prezzo_'.$i.'_'.$j.'">';
                        echo '</td>';
                    }

                    

                    $i += 1;
                    ?>
                    </tr>
                <?php
                } ?>

                </table><br>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="text-right">
                            <button onclick="event.preventDefault(); save_record();" class="btn btn-lg btn-success savebutton"><i class="icon-ok-3"></i> Salva</button>
                            <button onclick="event.preventDefault(); document.location.href='articoli.php'" class="btn btn-lg btn-warning"><i class="icon-undo"></i> Chiudi</button>
                        </div>
                    </div>
                </div>


            </div>

        </div>
    </div>

    <script>
    
    function copia(indice) {
        if ($('.colonna'.concat(indice)).val() > 0) {
            $('.colonna'.concat(indice)).val($('.colonna'.concat(indice)).val());
        }
    }
    
    function copiaTutto() {
        for (i = 0; i < 20; i++) {
            if ($('.colonna'.concat(i)).val() > 0) {
                $('.colonna'.concat(i)).val($('.colonna'.concat(i)).val());
            }
        }
    }
    
    function save_record() {

        scaglioni = [];
        var id_articolo = <?=$id?>;

        $('.scaglioneprezzo').each(function() {
            if ($( this ).val() > 0) {
                id = $( this ).attr('id');
                prezzounit = $( this ).val();
                var start = id.indexOf("_");
                var stop = id.indexOf("_", id.indexOf("_")+1);
                var indicecolore = id.substring(start+1, stop);
                var idcolore = colori[indicecolore];
                var colonna = parseInt(id.substring(stop+1))+1;
                var qtascaglione = $('#qta'.concat(colonna)).val();
                if (qtascaglione > 0) {
                    scaglioni.push([id_articolo, idcolore, qtascaglione, prezzounit]);
                }
            }
        });

        $.ajax({
            async: false,
            type: "POST",
            url: "controllers/articoli_prezzi_controller.php",
            data: {
                "action": "salva",
                "id_articolo": id_articolo,
                "scaglioni": scaglioni,
            },
            success: function (data, stato) {
                if (data == "OK") {
                    document.location.href='articoli.php';
                } else {
                    swal({
                        title: "Ops !",
                        text: "Qualcosa è andato storto",
                        confirmButtonColor: "#2196F3",
                        type: "error"
                    });
                }
            }
        });
    }
    </script>

</body>
</html>