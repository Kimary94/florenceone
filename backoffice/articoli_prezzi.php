<?php
    include "sys_header.php";
    if ($_SESSION["user_username"] != "sysadmin") {header("location: ".linkto("sys_dashboard")); die();}
?>
<body class="navbar-top<?php if ($settings_menu_closed == 1) { echo ' sidebar-xs'; } ?>">
    <?php include "sys_navbar.php"; ?>
    <div class="page-content">
        <?php include "sys_sidebar.php"; ?>
        <div class="content-wrapper">
            <div class="page-header page-header-light">
				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					<div class="d-flex">
                        <i class="icon-tag-1 mr-2 breadcrumb-item"></i><span style="margin-top: 8px;">Prezzi articolo</span>
					</div>
				</div>
			</div>


            <div class="content">

                <?php
                    $id = $_GET['id'];
                    $rows = fetch_rows("SELECT * FROM articolo WHERE id = ".$id);
                    foreach ( $rows as $row ) {
                        echo "<span style='font-size:16px;'>".$row['codice']."</span><br>";
                        echo "<span style='font-size:16px;'>".$row['nome']."</span><br><br><br>";
                        $qtaselect = $row['qtaselect'];
                        $coloreselect = $row['coloreselect'];
                        $grigliacoloretaglie = $row['grigliacoloretaglie'];
                        $prezzo_precedente = $row['prezzo_precedente'];
                        $descrizione_ordine = $row['descrizione_ordine'];
                    }
                    
                    

                    // SE PENNE O MATITE PRENDERE SCAGLIONE 500
                    $id_row_categoria = 0;
                    $scaglione = 100;
                    $categoria_articolo = fetch_rows("select id_row_categoria from categoriaxarticolo where id_categoria = 1 and id_articolo = ".$id);
                    foreach ( $categoria_articolo as $cat_articolo) {
                        $id_row_categoria = $cat_articolo["id_row_categoria"];
                    }
                    if (($id_row_categoria == 66) || ($id_row_categoria == 75)) { $scaglione = 500; }
                    ?>
                <table>
                <tr>
                    <th style="font-size: 12px; width: 150px; display: inline-block; line-height: 38px;">Colore / Scaglione (fino a)</th>
                    <?php
                        $scaglioni = array();
                        $i = 0;
                        $id_input_100_pezzi = ""; // necessario per calcola_actual_price()
                        $rows = fetch_rows("SELECT scaglione FROM prezzo WHERE id_articolo = ".$id." GROUP BY scaglione ORDER BY scaglione");
                        foreach ( $rows as $row ) {
                            $i += 1;
                            array_push($scaglioni, $row['scaglione']);
                            if ($row['scaglione'] == $scaglione) $id_input_100_pezzi = 'prezzo_0_'.($i-1); // necessario per calcola_actual_price()
                            ?>
                            <th><input type="number" step="1" class="form-control" style="font-weight: bold; text-align:center; margin-bottom: 6px; padding: 4px 4px; width: 70px;" value="<?=$row['scaglione']?>" id="qta<?=$i?>"></th>
                        <?php
                        }

                        if (($i == 0) && ($qtaselect == 1)) {
                            // NESSUNO SCAGLIONE TROVATO NEL DB e ARTICOLO DI TIPO 2 / 2b (quantità da tendina fissa), ALLORA CARICO LE Q.TA' IMPOSTATE NELLA TABELLA qtaselect
                            $rows = fetch_rows("SELECT quantita FROM qtaselect WHERE id_articolo = ".$id." ORDER BY quantita");
                            foreach ( $rows as $row ) {
                                $i += 1;
                                array_push($scaglioni, $row['quantita']);
                                ?>
                                <th><input type="number" step="1" class="form-control" style="font-weight: bold; text-align:center; margin-bottom: 6px; padding: 4px 4px; width: 70px;" value="<?=$row['quantita']?>" id="qta<?=$i?>"></th>
                            <?php
                            }
                        }

                        $i += 1;

                        for ($j = $i; $j <= 23; $j++) { ?>
                            <th><input type="number" step="1" class="form-control" style="font-weight: bold; text-align:center; margin-bottom: 6px; padding: 4px 4px; width: 70px;" id="qta<?=$j?>"></th>
                        <?php
                            array_push($scaglioni, 0);
                        } ?>
                </tr>
                <?php
                echo "<script>var colori = [];</script>";

                if (($grigliacoloretaglie == 0) && ($coloreselect == 0)) {
                    // articolo di tipo 1 / 2
                    $rows = array();
                    $rows[0]['id'] = 0;
                    $rows[0]['rgb'] = "";
                    $rows[0]['texture'] = "";
                    $rows[0]['immagine'] = "";
                    $rows[0]['descrizione'] = "NEUTRO";
                } else {
                    // articolo di tipo 1b / 2b / 3
                    $rows = fetch_rows("SELECT * FROM colore WHERE id_articolo = ".$id." LIMIT 1");
                }
                
                $i = 0;
                foreach ( $rows as $row ) {
                    echo "<script>colori.push(".$row['id'].");</script>";
                    echo "<tr style='height: 50px; border-bottom: 1px solid #c5c5c5; border-top: 1px solid #c5c5c5;'><td>";
                    /*
                    if ($row['rgb'] != "") {
                        echo "<img style='width:36px; height:36px; display: inline-block; background-color:#".$row['rgb'].";'>";
                    } else {
                        echo "<img style='width:36px; height:36px; display: inline-block; visibility: hidden;' >";
                    }
                    if ($row['texture'] != "") {
                        echo "<img style='width:36px; height:36px; display: inline-block;' src='".$row['texture']."'>";
                    } else {
                        echo "<img style='width:36px; height:36px; display: inline-block; visibility: hidden;' >";
                    }
                    if ($row['immagine'] != "") {
                        echo "<img style='width:36px; height:36px; display: inline-block;' src='".$row['immagine']."'>";
                    } else {
                        echo "<img style='width:36px; height:36px; display: inline-block; visibility: hidden;' >";
                    }
                    echo $row['descrizione'];
                    */
                    if (($grigliacoloretaglie == 1) || ($coloreselect == 1)) {
                        if ($i == 0) {
                            echo '<button class="btn btn-primary" style="padding: 1px; width: 57px; height: 18px; font-size: 8px; float: right; margin-top: -12px;" onclick="copiaTutto();">COPIA RIGA</button>';
                        }
                    }
                    echo "</td>";

                    /*
                    for ($j = 0; $j <= 19; $j++) {
                        $prezzi = fetch_rows("SELECT prezzounitario FROM prezzo WHERE id_articolo = ".$id." AND id_colore = ".$row['id']." AND scaglione = ".$scaglioni[$j]);
                        $prezzo_unitario = "";
                        foreach ( $prezzi as $prezzo) {
                            $prezzo_unitario = $prezzo['prezzounitario'];
                        }
                        echo '<td>';
                        if (($grigliacoloretaglie == 1) || ($coloreselect == 1)) {
                            if ($i == 0) {
                                echo '<button class="btn btn-primary" tabindex="-1" style="margin-bottom: 3px; padding: 1px; width: 100%; height: 18px; font-size: 8px;" onclick="copia('.$j.');">COPIA</button>';
                            }
                        }
                        echo '<input type="number" step="0.01" class="form-control colonna'.$j.' scaglioneprezzo" style="font-size: 11px; text-align:center; margin-bottom: 6px; padding: 4px 4px; width: 70px;" value="'.$prezzo_unitario.'" id="prezzo_'.$i.'_'.$j.'">';
                        echo '</td>';
                    }
                    */
                    


                    
                    //$prezzi = fetch_rows("SELECT scaglione, prezzounitario FROM prezzo WHERE id_articolo = ".$id." AND id_colore = ".$row['id']);
                    $prezzi = fetch_rows("SELECT scaglione, prezzounitario FROM prezzo WHERE id_articolo = ".$id);
                    $prezzo_unitario = array();
                    $prezzo_effettivo = 0;  // necessario per #actual_price
                    foreach ( $prezzi as $prezzo) {
                        $prezzo_unitario[$prezzo['scaglione']] = $prezzo['prezzounitario'];

                        if ($prezzo['scaglione'] == $scaglione) {
                            $prezzo_effettivo = $prezzo['prezzounitario'];  // necessario per #actual_price
                        }
                    }

                    /* necessario per #actual_price */
                    $stampa_100_pezzi = 0;

                    if (($id_row_categoria == 66) || ($id_row_categoria == 75)) {
                        // SE PENNE O MATITE PRENDE IL VALORE PIU' BASSO
                        $prezzi_stampa = fetch_rows("SELECT prezzounitario FROM prezzostampa WHERE scaglione = ".$scaglione." AND id_articolo = ".$id." ORDER BY prezzounitario LIMIT 1");
                    } else {
                        $prezzi_stampa = fetch_rows("SELECT prezzounitario FROM prezzostampa WHERE id_lato = 1 AND id_tipostampa = 1 AND scaglione = ".$scaglione." AND id_articolo = ".$id);
                    }
                    
                    foreach ( $prezzi_stampa as $prezzo_stampa) {
                        $stampa_100_pezzi = $prezzo_stampa['prezzounitario'];
                    }
                    $prezzo_effettivo += $stampa_100_pezzi;


                    for ($j = 0; $j <= 22; $j++) {
                        $pr_unit = "";
                        if (isset($prezzo_unitario[$scaglioni[$j]])) { $pr_unit = $prezzo_unitario[$scaglioni[$j]]; }

                        echo '<td>';
                        if (($grigliacoloretaglie == 1) || ($coloreselect == 1)) {
                            if ($i == 0) {
                                echo '<button class="btn btn-primary" tabindex="-1" style="margin-bottom: 3px; padding: 1px; width: 100%; height: 18px; font-size: 8px;" onclick="copia('.$j.');">COPIA</button>';
                            }
                        }

                        echo '<input type="number" onkeyup="calcola_actual_price();" step="0.01" class="form-control colonna'.$j.' scaglioneprezzo" style="font-size: 11px; text-align:center; margin-bottom: 6px; padding: 4px 4px; width: 70px;" value="'.$pr_unit.'" id="prezzo_'.$i.'_'.$j.'">';
                        echo '</td>';
                    }

                    

                    $i += 1;
                    ?>
                    </tr>
                <?php
                } ?>

                </table><br>


                <div class="row">
                    <div class="col-lg-12">
                        <!--
                            Prezzo effettivo di vendita: <span id="actual_price" style="font-weight: 900;"><?=number_format($prezzo_effettivo,2,",",".")?></span> € <?=$descrizione_ordine?><br>
                        -->
                        Prezzo precedente: <input type="number" step="0.01" style="text-align:center; width:80px;" id="prezzo_precedente" value="<?=$prezzo_precedente?>"> € (questo prezzo apparirà così <b style="color:red; text-decoration: line-through;">10,00 €</b>)
                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-12">
                        <div class="text-right">
                            <button onclick="event.preventDefault(); save_record();" class="btn btn-lg btn-success savebutton"><i class="icon-ok-3"></i> Salva</button>
                            <button onclick="event.preventDefault(); document.location.href='articoli.php'" class="btn btn-lg btn-warning"><i class="icon-undo"></i> Chiudi</button>
                        </div>
                    </div>
                </div>


            </div>

        </div>
    </div>

    <script>

    function calcola_actual_price() {
        var stampa_100_pezzi = <?=$stampa_100_pezzi?>;
        var id_input_100_pezzi = "<?=$id_input_100_pezzi?>";
        if (id_input_100_pezzi != "") {
            var nuovo_prezzo = parseFloat($('#'.concat(id_input_100_pezzi)).val()) + parseFloat(stampa_100_pezzi);
            $('#actual_price').html( nuovo_prezzo.toFixed(2).replace(".", ",") );
        }
    }
    
    function copia(indice) {
        if ($('.colonna'.concat(indice)).val() > 0) {
            $('.colonna'.concat(indice)).val($('.colonna'.concat(indice)).val());
        }
    }
    
    function copiaTutto() {
        for (i = 0; i < 23; i++) {
            if ($('.colonna'.concat(i)).val() > 0) {
                $('.colonna'.concat(i)).val($('.colonna'.concat(i)).val());
            }
        }
    }
    
    function save_record() {

        scaglioni = [];
        var id_articolo = <?=$id?>;

        $('.scaglioneprezzo').each(function() {
            if ($( this ).val() > 0) {
                id = $( this ).attr('id');
                prezzounit = $( this ).val();
                var start = id.indexOf("_");
                var stop = id.indexOf("_", id.indexOf("_")+1);
                var indicecolore = id.substring(start+1, stop);
                var idcolore = colori[indicecolore];
                var colonna = parseInt(id.substring(stop+1))+1;
                var qtascaglione = $('#qta'.concat(colonna)).val();
                if (qtascaglione > 0) {
                    scaglioni.push([id_articolo, idcolore, qtascaglione, prezzounit]);
                }
            }
        });

        $.ajax({
            async: false,
            type: "POST",
            url: "controllers/articoli_prezzi_controller.php",
            data: {
                "action": "salva",
                "id_articolo": id_articolo,
                "scaglioni": scaglioni,
                "prezzo_precedente": $('#prezzo_precedente').val()
            },
            success: function (data, stato) {
                if (data == "OK") {
                    document.location.href='articoli.php';
                } else {
                    swal({
                        title: "Ops !",
                        text: "Qualcosa è andato storto",
                        confirmButtonColor: "#2196F3",
                        type: "error"
                    });
                }
            }
        });
    }
    </script>

</body>
</html>