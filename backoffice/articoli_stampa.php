<?php
    include "sys_header.php";
    if ($_SESSION["user_username"] != "sysadmin") {header("location: ".linkto("sys_dashboard")); die();}
?>
<body class="navbar-top<?php if ($settings_menu_closed == 1) { echo ' sidebar-xs'; } ?>">
    <?php include "sys_navbar.php"; ?>
    <div class="page-content">
        <?php include "sys_sidebar.php"; ?>
        <div class="content-wrapper">
            <div class="page-header page-header-light">
				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					<div class="d-flex">
                        <i class="icon-print-3 mr-2 breadcrumb-item"></i><span style="margin-top: 8px;">Prezzi stampa</span>
					</div>
				</div>
			</div>


            <div class="content">

                <?php
                    $id = $_GET['id'];
                    $rows = fetch_rows("SELECT * FROM articolo WHERE id = ".$id);
                    foreach ( $rows as $row ) {
                        echo "<span style='font-size:16px;'>".$row['codice']."</span><br>";
                        echo "<span style='font-size:16px;'>".$row['nome']."</span><br><br><br>";
                        $qtaselect = $row['qtaselect'];
                    } ?>
                <table>
                <tr>
                    <th style="font-size: 12px; width: 300px; display: inline-block; line-height: 38px;">Tipo stampa / Scaglione (fino a)</th>
                    <?php
                        $scaglioni = array();
                        $i = 0;
                        $rows = fetch_rows("SELECT scaglione FROM prezzostampa WHERE id_articolo = ".$id." GROUP BY scaglione ORDER BY scaglione");
                        foreach ( $rows as $row ) {
                            $i += 1;
                            array_push($scaglioni, $row['scaglione']);
                            ?>
                            <th style="border-bottom: 1px solid #c5c5c5;"><input type="number" step="1" class="form-control" style="font-weight: bold; text-align:center; margin-bottom: 6px; padding: 4px 4px; width: 70px;" value="<?=$row['scaglione']?>" id="qta<?=$i?>"></th>
                        <?php
                        }

                        if (($i == 0) && ($qtaselect == 1)) {
                            // NESSUNO SCAGLIONE TROVATO NEL DB e ARTICOLO DI TIPO 2 / 2b (quantità da tendina fissa), ALLORA CARICO LE Q.TA' IMPOSTATE NELLA TABELLA qtaselect
                            $rows = fetch_rows("SELECT quantita FROM qtaselect WHERE id_articolo = ".$id." ORDER BY quantita");
                            foreach ( $rows as $row ) {
                                $i += 1;
                                array_push($scaglioni, $row['quantita']);
                                ?>
                                <th style="border-bottom: 1px solid #c5c5c5;"><input type="number" step="1" class="form-control" style="font-weight: bold; text-align:center; margin-bottom: 6px; padding: 4px 4px; width: 70px;" value="<?=$row['quantita']?>" id="qta<?=$i?>"></th>
                            <?php
                            }
                        }

                        $i += 1;

                        for ($j = $i; $j <= 23; $j++) { ?>
                            <th style="border-bottom: 1px solid #c5c5c5;"><input type="number" step="1" class="form-control" style="font-weight: bold; text-align:center; margin-bottom: 6px; padding: 4px 4px; width: 70px;" id="qta<?=$j?>"></th>
                        <?php
                            array_push($scaglioni, 0);
                        } ?>
                </tr>
                <?php
                $lati = fetch_rows("SELECT * FROM lato ORDER BY ordine");
                foreach ( $lati as $lato ) {

                    $conteggio_tipologie_per_lato = 0;
                    $rows = fetch_rows("SELECT IFNULL(count(*), 0) as conteggio FROM prezzostampa where id_articolo = ".$id." AND id_lato = ".$lato['id']);
                    foreach ( $rows as $row ) {
                        $conteggio_tipologie_per_lato = $row['conteggio'];
                    }
                    
                    if ($conteggio_tipologie_per_lato > 0) {
                        echo "<tr style='height: 50px; border-bottom: 1px solid #c5c5c5; border-top: 1px solid #c5c5c5; font-weight:bold;'><td><center>".$lato['descrizione']."</center>";
                        if ($lato['descrizione'] != "Stampa fronte") {
                            if ($lato['descrizione'] == "Stampa destra") {
                                echo '<button class="btn btn-primary" tabindex="-1" style="padding: 1px; width: 53px; height: 30px; font-size: 9px; float: right; margin-top: -24px; font-variant: all-small-caps;" onclick="copia(3, '.$lato['id'].');">COPIA DA SINISTRA</button>';
                            } else {
                                echo '<button class="btn btn-primary" tabindex="-1" style="padding: 1px; width: 53px; height: 30px; font-size: 9px; float: right; margin-top: -24px; font-variant: all-small-caps;" onclick="copia(1, '.$lato['id'].');">COPIA DA FRONTE</button>';
                            }
                        }
                        echo "</td></tr>";

                        //$rows = fetch_rows("SELECT * FROM tipostampa ORDER BY ordine");
                        // CARICHIAMO SOLO LE TIPOLOGIE PRESENTI PER QUESTO ARTICOLO
                        $rows = fetch_rows("SELECT * FROM tipostampa WHERE id in (select id_tipostampa as id from prezzostampa where id_articolo = ".$id." and id_lato = ".$lato['id']." group by id_tipostampa) ORDER BY ordine");
                        $i = 0;
                        foreach ( $rows as $row ) {
                            echo "<tr style='height: 50px; border-bottom: 1px solid #c5c5c5; border-top: 1px solid #c5c5c5;'><td>".$row['descrizione'];
                            echo "</td>";

                            $prezzi = fetch_rows("SELECT scaglione, prezzounitario FROM prezzostampa WHERE id_articolo = ".$id." AND id_tipostampa = ".$row['id']." AND id_lato=".$lato['id']);
                            $prezzo_unitario = array();
                            foreach ( $prezzi as $prezzo) {
                                $prezzo_unitario[$prezzo['scaglione']] = $prezzo['prezzounitario'];
                            }

                            for ($j = 0; $j <= 22; $j++) {
                                $pr_unit = "";
                                if (isset($prezzo_unitario[$scaglioni[$j]])) { $pr_unit = $prezzo_unitario[$scaglioni[$j]]; }
                                echo '<td><input type="number" step="0.01" class="form-control colonna'.$j.' scaglioneprezzo scaglionelato'.$lato['id'].'" style="font-size: 11px; text-align:center; margin-bottom: 6px; padding: 4px 4px; width: 70px;" value="'.$pr_unit.'" id="prezzo_'.$row['id'].'_'.$j.'_'.$lato['id'].'"></td>';
                            }

                            $i += 1;
                            ?>
                            </tr>
                        <?php
                        }
                    }
                } ?>
                </table><br>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="text-right">
                            <button onclick="event.preventDefault(); save_record();" class="btn btn-lg btn-success savebutton"><i class="icon-ok-3"></i> Salva</button>
                            <button onclick="event.preventDefault(); document.location.href='articoli.php'" class="btn btn-lg btn-warning"><i class="icon-undo"></i> Chiudi</button>
                        </div>
                    </div>
                </div>


            </div>

        </div>
    </div>

    <script>
    
    function copia(da_lato, a_lato) {
        $('.scaglionelato'.concat(a_lato)).each(function() {
            id = $( this ).attr('id');
            from = id.substr(0, id.length - 1);
            if (from.substr(-1, 1) != "_") {
                from = from.substr(0, from.length - 1);
            }
            from = from.concat(da_lato);
            $( this ).val( $('#'.concat(from)).val() );
        });
    }
        
    function save_record() {
        scaglioni = [];
        var id_articolo = <?=$id?>;
        $('.scaglioneprezzo').each(function() {
            //if ($( this ).val() > 0) {
            if ($( this ).val() != "") {
                id = $( this ).attr('id');
                prezzounit = $( this ).val();
                var start = id.indexOf("_");
                var stop = id.indexOf("_", start + 1);
                var stop2 = id.indexOf("_", stop + 1);

                var idtipostampa = id.substring(start+1, stop);

                var colonna = parseInt(id.substring(stop+1, stop2)) + 1;
                var qtascaglione = $('#qta'.concat(colonna)).val();

                var id_lato = parseInt(id.substring(stop2+1));

                if (qtascaglione > 0) {
                    scaglioni.push([id_articolo, id_lato, idtipostampa, qtascaglione, prezzounit]);
                }
            }
        });
        $.ajax({
            async: false,
            type: "POST",
            url: "controllers/articoli_stampa_controller.php",
            data: {
                "action": "salva",
                "id_articolo": id_articolo,
                "scaglioni": scaglioni,
            },
            success: function (data, stato) {
                if (data == "OK") {
                    document.location.href='articoli.php';
                } else {
                    console.log(data);
                    swal({
                        title: "Ops !",
                        text: "Qualcosa è andato storto",
                        confirmButtonColor: "#2196F3",
                        type: "error"
                    });
                }
            }
        });
    }
    </script>

</body>
</html>