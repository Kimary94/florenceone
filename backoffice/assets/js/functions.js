function drawFileList(attachment_list, attachment_list_name, id_container) {
    var html_code = "";
    if (attachment_list !== null) {
        for( var i = 0; i < attachment_list.length; i++){ 
            var file_extension = attachment_list[i]['serverFileName'].substring( attachment_list[i]['serverFileName'].length - 4 ).toLowerCase();
            html_code = html_code.concat("<div class=\"row\" style=\"border: 1px solid #dddddd; border-radius:3px; margin:-1px; padding-top:3px;\"><div class=\"col-lg-12\">");
            if ((file_extension == ".jpg") || (file_extension == "jpeg") || (file_extension == ".bmp") || (file_extension == ".gif") || (file_extension == ".png") || (file_extension == "apng") || (file_extension == ".svg") || (file_extension == ".ico")) {
                html_code = html_code.concat("<i class=\"icon-eye\" style=\"color:#1a7909; padding: 4px;\" onclick=\"$('#imagePreviewDiv').html('<div class=\\\'modal-dialog modal-xl imgpreview-dlg\\\'><img class=\\\'imgpreview-img\\\' src=\\\'").concat(attachment_list[i]['serverFileName'].replace(/\\/g , "/")).concat("\\\' /></div>'); $('#imagePreviewDiv').modal('show');\"></i>");
            } else {
                html_code = html_code.concat("<a style=\"padding: 4px;\" href=\"").concat(attachment_list[i]['serverFileName']).concat("\" target=\"_blank\"><i class=\"icon-eye\"></i></a>");
            }            
            html_code = html_code.concat("<a style=\"padding: 4px;\" href=\"").concat(attachment_list[i]['serverFileName']).concat("\" download=\"").concat(attachment_list[i]['fileName']).concat("\" target=\"_blank\"><i class=\"icon-download\"></i></a>");
            html_code = html_code.concat("<i class=\"icon-trash-empty\" style=\"float:right; margin-top:5px; color:red;\" onclick=\"remove_attachment(").concat(attachment_list_name).concat(", '").concat(attachment_list_name).concat("', '").concat(attachment_list[i]["serverFileName"].replace(/[\\]/g, '\\\\')).concat("', '").concat(id_container).concat("');\"></i>");
            html_code = html_code.concat("<p style=\"padding:4px; margin:0px; display:inline-block;\" >").concat(attachment_list[i]["fileName"]).concat("</p>");
            html_code = html_code.concat("</div></div>");
        }
    }
    $('#'.concat(id_container)).html(html_code);
}

function remove_attachment(attachment_list, attachment_list_name, serverFileName, id_container) {
    swal({
        title: "Sei sicuro?",
        text: "",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#EF5350",
        confirmButtonText: "Si, elimina!",
        cancelButtonText: "No, annulla!",
        closeOnConfirm: true,
        closeOnCancel: true
    }, function (isConfirm) {
        if (isConfirm) {
            for( var i = 0; i < attachment_list.length; i++){ 
                console.log("###########");
                console.log(attachment_list[i]['serverFileName']);
                console.log(serverFileName);
                
                if (attachment_list[i]['serverFileName'] == serverFileName) {
                    attachment_list.splice(i, 1); 
                    i--;
                }
            }
            drawFileList(attachment_list, attachment_list_name, id_container);
        }
    });
}

function checkInteger(e){
    if(window.event) {
        var carattere = String.fromCharCode(e.keyCode);
    } else if(e.which){
        var carattere = String.fromCharCode(e.which);
    }
    if ((carattere == "+") || (carattere == ".") || (carattere == ",") || (carattere == "e") || (carattere == "E")) {
        return false;
    } else { return true; }
}

function checkFloat(e){
    if(window.event) {
        var carattere = String.fromCharCode(e.keyCode);
    } else if(e.which){
        var carattere = String.fromCharCode(e.which);
    }
    if ((carattere == "+") || (carattere == "e") || (carattere == "E")) {
        return false;
    } else { return true; }
}


// return "false" if user press Cancel button
function input_box(title_text, description_text, button_ok_text, button_cancel_text, next_function){
    if ((button_ok_text === undefined) || (button_ok_text == "")) { button_ok_text = "Ok"; }
    if ((button_cancel_text === undefined) || (button_cancel_text == "")) { button_cancel_text = "Annulla"; }
    swal({
        html:true,
        title: title_text,
        text: description_text,
        type: "input",
        showCancelButton: true,
        confirmButtonText: button_ok_text,
        cancelButtonText: button_cancel_text,
        closeOnConfirm: true,
        closeOnCancel: true
        }, function(textinputted){
            if (next_function !== undefined) {setTimeout(function () {next_function(textinputted);}, 500);}
        });
}

// icon_type can be: "success", "warning", "info", "error"
function true_false_box(title_text, description_text, button_true_text, button_false_text, icon_type, next_function){
    if ((icon_type === undefined) || (icon_type == "")) { icon_type = "warning"; }
    if ((button_true_text === undefined) || (button_true_text == "")) { button_true_text = "Si"; }
    if ((button_false_text === undefined) || (button_false_text == "")) { button_false_text = "No"; }
    swal({
        html:true,
        title: title_text,
        text: description_text,
        type: icon_type,
        showCancelButton: true,
        confirmButtonText: button_true_text,
        cancelButtonText: button_false_text,
        closeOnConfirm: true,
        closeOnCancel: true
        }, function(true_or_false){
            if (next_function !== undefined) {setTimeout(function () {next_function(true_or_false);}, 500);}
        });
}

// icon_type can be: "success", "warning", "info", "error"
function message_box(title_text, description_text, button_ok_text, icon_type, next_function){
    if ((icon_type === undefined) || (icon_type == "")) { icon_type = "info"; }
    if ((button_ok_text === undefined) || (button_ok_text == "")) { button_ok_text = "Ok"; }
    swal({
        html:true,
        title: title_text,
        text: description_text,
        type: icon_type,
        confirmButtonText: button_ok_text,
        closeOnConfirm: true,
        }, function(){
            if (next_function !== undefined) {setTimeout(function () {next_function();}, 500);}
        });
}

// icon_type can be: "success", "warning", "info", "error"
function toast(description_text, icon_type) {
    if ((icon_type === undefined) || (icon_type == "")) { icon_type = "info"; }
    nativeToast({
        message: description_text,
        type: icon_type,
        position: 'south',
        timeout: 5000
      });
}

function ischecked(control_selector){
    if ($(control_selector).prop('checked')) { 
        return 1;
    } else {
        return 0;
    }
}

function reload_page() {
    window.location.reload();
}

function showOverlay(){
    $("html").append('<div id="overlay" style="background-color:black;position:absolute;top:0;left:0;height:'.concat($(document).height()).concat('px;width:100%;z-index:9999; opacity: 0.2;"></div>'));
}

function hideOverlay(){
    $("#overlay").remove();
}

// ON DOCUMENT READY
$(function() {
    $('input:checkbox').each(function() { Switchery( this ); }); // Switchery all checkboxes
    $('.switchery').css('display', 'inline-block')               // Switchery inline
    $('.switchery').css('vertical-align', 'text-bottom')         // Switchery v-align
    
    // Enable multiple modals: when a modal is hiding if another modal is open it set .modal-open to the <body> (required for images preview on modal)
    $('body').on('hidden.bs.modal', function () {
        if($('.modal.show').length > 0) { $('body').addClass('modal-open'); }
    });
});
