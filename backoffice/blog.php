<?php
    include "sys_header.php";
    if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(5, "read")) {header("location: ".linkto("sys_dashboard")); die();}}
?>

<body class="navbar-top<?php if ($settings_menu_closed == 1) { echo ' sidebar-xs'; } ?>">
    <div id="imagePreviewDiv" class="modal modal-xl fade imgpreview" style="z-index: 9999;"></div>
    <?php include "sys_navbar.php"; ?>
    <div class="page-content">
        <?php
        include "sys_sidebar.php";
        if ($_SESSION["user_username"] != "sysadmin") {
            if (!checkUserRight(5, "create")) {echo "<script>can_create = false;</script>";} else {echo "<script>can_create = true;</script>";}
            if (!checkUserRight(5, "edit")) {echo "<script>can_edit = false;</script>";} else {echo "<script>can_edit = true;</script>";}
        } else {
            echo "<script>can_create = true; can_edit = true;</script>";
        }
        ?>
        <div class="content-wrapper">

        
        <!-- Page header -->
        <div class="page-header page-header-light">
            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">

                <div class="d-flex">
                    <i class="icon-doc-text mr-2 breadcrumb-item"></i><span style="margin-top: 8px;">Blog</span>
                </div>

                <div class="header-elements d-none" style="display: flex !important;">
                    <div class="breadcrumb justify-content-center">
                        <?php
                        $show_button_new = true;
                        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(5, "create")) { $show_button_new = false; }}
                        if ($show_button_new) { ?>
                            <a href="#" class="breadcrumb-elements-item" onclick="new_record();">
                                <i class="icon-plus-squared"></i>
                                New record
                            </a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>


<div class="content unselectable">
    <div class="card card-collapsed" style="background-color: #f9f9f9;">
        <div class="card-header header-elements-inline" onmouseup="$('#searchexpand').click();" style="font-variant: all-petite-caps;">
            <div><i class="icon-search-7"></i> Filtri di ricerca</div>
            <div data-action="collapse" id="searchexpand" style="display:none;"></div>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-lg-10 col-md-9 col-sm-12 col-xs-12">
                    <div class="row">

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Titolo</label>
                        <input type="text"   class="form-control" autocomplete="off" id="titolo" value="<?php if (isset($_SESSION['blog_filter_titolo'])) { echo $_SESSION['blog_filter_titolo']; } ?>">
                    </div></div>

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Testo</label>
                        <input type="text"   class="form-control" autocomplete="off" id="testo" value="<?php if (isset($_SESSION['blog_filter_testo'])) { echo $_SESSION['blog_filter_testo']; } ?>">
                    </div></div>

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Testo breve</label>
                        <input type="text"   class="form-control" autocomplete="off" id="testo_breve" value="<?php if (isset($_SESSION['blog_filter_testo_breve'])) { echo $_SESSION['blog_filter_testo_breve']; } ?>">
                    </div></div>

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Immagini</label>
                        <input type="text"   class="form-control" autocomplete="off" id="immagini" value="">
                    </div></div>


                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="text-center">
                                <button onclick="event.preventDefault(); applyFilters();"
                                        style="width: 150px; padding: 10px; border-radius: 5px;"
                                        class="btn btn-sm btn-outline-info">
                                            <i class="icon-search-1" style="float: left; margin-top: 2px;"></i> Search
                                </button><br>
                                <button onclick="event.preventDefault(); resetFilters();"
                                        style="width: 150px; padding: 10px; border-radius: 5px; margin: 3px 0px 3px 0px;"
                                        class="btn btn-sm btn-outline-warning">
                                            <i class="icon-cancel-alt" style="float: left; margin-top: 2px;"></i> Reset
                                </button><br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="card">
    <table id="myDatatable" class="display table table-striped datatable-responsive" style="width:100%; overflow: hidden;">
        <thead>
            <tr>
                <th><b>Id</b></th>
                <th><b>Titolo</b></th>
                <th><b>Testo</b></th>
                <th><b>Testo breve</b></th>

                <th><b>Actions</b></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th><b>Id</b></th>
                <th><b>Titolo</b></th>
                <th><b>Testo</b></th>
                <th><b>Testo breve</b></th>

                <th><b>Actions</b></th>
            </tr>
        </tfoot>
    </table>
</div>

<div id="modal_edit" class="modal fade" style="-webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none;" unselectable="on">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<h6 class="modal-title"><i class="icon-doc-text"></i> <span id="modal_mode"></span></h6>
			</div>
            <div class="modal-body">
                <div class="content">
                        
                    <input type="hidden" id="record_id">
            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">Titolo</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <input type="text" id="record_titolo" maxlength="255" autocomplete="off" class="form-control" >
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">Testo</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <input type="text" id="record_testo" maxlength="5000" autocomplete="off" class="form-control" >
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">Testo breve</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <input type="text" id="record_testo_breve" maxlength="500" autocomplete="off" class="form-control" >
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">Immagini</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <div class="row">
                        <div class="col-lg-12 dropzonerow">
                            <form class="dropzone" id="dropzone_immagini"></form>
                        </div>
                        <div class="col-lg-12" id="fileContainer_immagini"></div>    
                    </div>
                </div>
            </div>

                </div>
            </div>
			<div class="modal-footer">
                <button onclick="save_record();" class="btn btn-lg btn-outline-success savebutton"><i class="icon-ok-3"></i> Save</button>
                <button onclick="$('#modal_edit').modal('hide');" class="btn btn-lg btn-outline-warning"><i class="icon-undo"></i> Cancel</button>
			</div>
		</div>
	</div>
</div>


</div>
</div>
</div>


<script>
    var FILELIST_immagini = new Array;
    
    function new_record() {
        $('#modal_mode').html('New Blog');
        $("#record_id").val("");
        $("#record_titolo").val("");
        $("#record_testo").val("");
        $("#record_testo_breve").val("");

        document.querySelector("#dropzone_immagini").dropzone.removeAllFiles();
        FILELIST_immagini = new Array;
        $("#fileContainer_immagini").html("");

        setTimeout(function(){ $("#record_titolo").focus(); }, 300);
        if (can_create) { $(".savebutton").show(); $(".dropzone").show(); $(".icon-trash-empty").show(); } else { $(".savebutton").hide();  $(".dropzone").hide(); $(".icon-trash-empty").hide(); }
        $('#modal_edit').modal({backdrop: 'static'});
    }

    function edit_record(id) {
        if (can_edit) { $('#modal_mode').html('Edit Blog'); } else { $('#modal_mode').html('View Blog'); }
        $.ajax({
            async: false,
            type: "POST",
            url: "controllers/blog_controller.php",
            data: {
                "action": "read_record",
                "id": id
            },
            success: function (data, stato) {
                var data = $.parseJSON(data);
                if (data == "NOT AUTHORIZED") { return; }

                $("#record_id").val(data["record_id"]);
                $("#record_titolo").val(data["record_titolo"]);
                $("#record_testo").val(data["record_testo"]);
                $("#record_testo_breve").val(data["record_testo_breve"]);

                $("#fileContainer_immagini").html("");
                document.querySelector("#dropzone_immagini").dropzone.removeAllFiles();
                FILELIST_immagini = new Array;
                if (data.record_immagini !== null) {
                    if (data.record_immagini.length > 0) {
                        FILELIST_immagini = JSON.parse(data.record_immagini);
                        if (FILELIST_immagini === null) {
                            FILELIST_immagini = new Array;
                        } else {
                            drawFileList(FILELIST_immagini, "FILELIST_immagini", "fileContainer_immagini");
                        }
                    }
                }
                setTimeout(function(){ $("#record_titolo").focus(); }, 300);
                if (can_edit) { $(".savebutton").show(); $(".dropzone").show(); $(".icon-trash-empty").show(); } else { $(".savebutton").hide(); $(".dropzone").hide(); $(".icon-trash-empty").hide(); }
                $('#modal_edit').modal({backdrop: 'static'});
            }
        });
    }
    
    function save_record() {
        var requiredfields = "";
        $(".modal-body :required").each(function(){
            if (($(this).val() == "") || ($(this).val() === null)) {
                if (requiredfields != "") { requiredfields = requiredfields.concat(", "); }
                var tmp_req_field = $(this).attr("id").replace("record_", "").replace("_", " ");
                if (tmp_req_field.substr(0, 3) == "id ") { tmp_req_field = tmp_req_field.replace("id ", ""); }
                requiredfields = requiredfields.concat(tmp_req_field);
            }
        })
        if (requiredfields != "") { toast("Required fields: ".concat(requiredfields), "error"); return; }

        $.ajax({
            async: false,
            type: "POST",
            url: "controllers/blog_controller.php",
            data: {
                "action": "save_record",
                "id": $("#record_id").val(), 
                "titolo": $("#record_titolo").val(), 
                "testo": $("#record_testo").val(), 
                "testo_breve": $("#record_testo_breve").val(), 
                "immagini": JSON.stringify(FILELIST_immagini),

            },
            success: function (data, stato) {
                if (data != "OK") {
                    swal({
                        title: "Error",
                        text: "An error occurred during Save: " + data.toString(),
                        confirmButtonColor: "#66BB6A",
                        type: "warning"
                    });
                } else {
                    $('#modal_edit').modal('hide');
                    update_table();
                }
            }
        });
    }

    function update_table()  {
        table = $('#myDatatable').DataTable();
        table.destroy();
        $("#myDatatable > tbody").html("");
        load_table();
        table = $('#myDatatable').DataTable();
        table.page(0).draw(false);
    }

    $(document).ready(function () {

        $('.select').select2({ width: '100%' });

        new Dropzone("#dropzone_immagini", {
            init: function() {
                this.on("success", function(file, serverFileName) {
                    FILELIST_immagini.push({"serverFileName" : serverFileName, "fileName" : file.name});
                    drawFileList(FILELIST_immagini, "FILELIST_immagini", "fileContainer_immagini");
                });
                this.on("error", function(file, errorMessage) {
                    message_box("Error", errorMessage, "Ok", "error");
                });
            },
            url: "controllers/sys_file_uploader.php" });

        load_table();
        table = $('#myDatatable').DataTable();
        table.on('draw', function () {
            table.rows().every(function (rowIdx, tableLoop, rowLoop) {
                var values_in_row = this.data();
                $.ajax({
                    async: false,
                    type: "POST",
                    url: "controllers/blog_controller.php",
                    data: {
                        "action": "button_record",
                        "id": values_in_row[0]
                    },
                    success: function (data, stato) {
                        values_in_row[4] = data;
                    }
                });
                this.data(values_in_row);
            });
        });
    });


    function delete_record(id) {
        swal({
            title: "Sei sicuro?",
            text: "I dati non potranno essere recuperati",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: "Si, elimina!",
            cancelButtonText: "No, annulla!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    async: true,
                    type: "POST",
                    url : "controllers/blog_controller.php",
                    data: {
                        "action": "delete_record",
                        "id": id
                    },
                    success: function (data) {
                        if (data === "OK") {
                            swal({
                                title: "Eliminato!",
                                text: "Il record è stato cancellato",
                                confirmButtonColor: "#66BB6A",
                                type: "success"
                            }, function () {
                                document.location.reload();
                            });
                        } else {
                            swal({
                                title: "Operazione annullata",
                                text: "Qualcosa è andato storto",
                                confirmButtonColor: "#2196F3",
                                type: "error"
                            });
                        }
                    },
                    error: function (data) {
                        swal({
                            title: "Operazione annullata",
                            text: "Qualcosa è andato storto",
                            confirmButtonColor: "#2196F3",
                            type: "error"
                        });
                    }
                });
            }
            else {
                swal({
                    title: "Operazione annullata",
                    text: "",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
    }


    function load_table() {

        filtertitolo = "";
        if (parseInt($("#titolo").val().length) > 0) { filtertitolo = $("#titolo").val(); }

        filtertesto = "";
        if (parseInt($("#testo").val().length) > 0) { filtertesto = $("#testo").val(); }

        filtertesto_breve = "";
        if (parseInt($("#testo_breve").val().length) > 0) { filtertesto_breve = $("#testo_breve").val(); }

        filterimmagini = "";
        if (parseInt($("#immagini").val().length) > 0) { filterimmagini = $("#immagini").val(); }

        $('#myDatatable').DataTable( {
            'iDisplayLength': 25,
            'processing': true,
            'serverSide': true,
            'stateSave': true,
            'searching': false,
            'responsive': true,
            dom: '<"datatable-header"flB><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            buttons: [{
                        extend: 'pdf',
                        orientation: 'landscape',
                        text: '<i class="icon-file-pdf"></i> PDF',
                        title: '',
                        className: 'btn btn-outline-danger',
                        exportOptions: {columns: [1, 2, 3]}
                    },
                    {
                        extend: 'excel',
                        text: '<i class="icon-file-excel"></i> Excel',
                        title: '',
                        className: 'btn btn-outline-success',
                        exportOptions: {columns: [1, 2, 3]}
                    }
            ],
            language: {
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: {'first': '|&larr;', 'last': '&rarr;|', 'next': '&rarr;', 'previous': '&larr;'}
            },
            ajax: {
                url: 'controllers/blog_controller.php',
                type: 'POST',
                data: {
                    action: "load_table",
                    filtertitolo: filtertitolo,
                    filtertesto: filtertesto,
                    filtertesto_breve: filtertesto_breve,
                    filterimmagini: filterimmagini,

                }
            },
            'columnDefs': [
                { 'targets': 0, 'visible': false },
                { "targets": 1,                           "width": "25%" },
                { "targets": 2,                           "width": "25%" },
                { "targets": 3,                           "width": "25%" },

                { "targets": 4, "className": "dt-center", "orderable": false, "width": "5%" },
            ]
        } );
    }

    function applyFilters() {
        table = $('#myDatatable').DataTable();
        table.destroy();
        $("#myDatatable > tbody").html("");
        load_table();
        table = $('#myDatatable').DataTable();
        table.page(0).draw(false);
    }

    function resetFilters() {
        $("#titolo").val("");
        $("#testo").val("");
        $("#testo_breve").val("");
        $("#immagini").val("");

        applyFilters();
    }

    
</script>