<?php
    include "sys_header.php";
    if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(9, "read")) {header("location: ".linkto("sys_dashboard")); die();}}
?>

<body class="navbar-top<?php if ($settings_menu_closed == 1) { echo ' sidebar-xs'; } ?>">
    <div id="imagePreviewDiv" class="modal modal-xl fade imgpreview" style="z-index: 9999;"></div>
    <?php include "sys_navbar.php"; ?>
    <div class="page-content">
        <?php
        include "sys_sidebar.php";
        if ($_SESSION["user_username"] != "sysadmin") {
            if (!checkUserRight(9, "create")) {echo "<script>can_create = false;</script>";} else {echo "<script>can_create = true;</script>";}
            if (!checkUserRight(9, "edit")) {echo "<script>can_edit = false;</script>";} else {echo "<script>can_edit = true;</script>";}
        } else {
            echo "<script>can_create = true; can_edit = true;</script>";
        }
        ?>
        <div class="content-wrapper">

        
        <!-- Page header -->
        <div class="page-header page-header-light">
            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">

                <div class="d-flex">
                    <i class="icon-smile mr-2 breadcrumb-item"></i><span style="margin-top: 8px;">Casi di successo</span>
                </div>

                <div class="header-elements d-none" style="display: flex !important;">
                    <div class="breadcrumb justify-content-center">
                        <?php
                        $show_button_new = true;
                        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(9, "create")) { $show_button_new = false; }}
                        if ($show_button_new) { ?>
                            <a href="#" class="breadcrumb-elements-item" onclick="new_record();">
                                <i class="icon-plus-squared"></i>
                                New record
                            </a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>


<div class="content unselectable">
    <div class="card card-collapsed" style="background-color: #f9f9f9;">
        <div class="card-header header-elements-inline" onmouseup="$('#searchexpand').click();" style="font-variant: all-petite-caps;">
            <div><i class="icon-search-7"></i> Filtri di ricerca</div>
            <div data-action="collapse" id="searchexpand" style="display:none;"></div>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-lg-10 col-md-9 col-sm-12 col-xs-12">
                    <div class="row">

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Titolo</label>
                        <input type="text"   class="form-control" autocomplete="off" id="titolo" value="<?php if (isset($_SESSION['casi_di_successo_filter_titolo'])) { echo $_SESSION['casi_di_successo_filter_titolo']; } ?>">
                    </div></div>

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Sottotitolo</label>
                        <input type="text"   class="form-control" autocomplete="off" id="sottotitolo" value="<?php if (isset($_SESSION['casi_di_successo_filter_sottotitolo'])) { echo $_SESSION['casi_di_successo_filter_sottotitolo']; } ?>">
                    </div></div>

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Cliente</label>
                        <input type="text"   class="form-control" autocomplete="off" id="cliente" value="<?php if (isset($_SESSION['casi_di_successo_filter_cliente'])) { echo $_SESSION['casi_di_successo_filter_cliente']; } ?>">
                    </div></div>

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Urllogo</label>
                        <input type="text"   class="form-control" autocomplete="off" id="urllogo" value="<?php if (isset($_SESSION['casi_di_successo_filter_urllogo'])) { echo $_SESSION['casi_di_successo_filter_urllogo']; } ?>">
                    </div></div>

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">From data</label>
                        <input type="date"   class="form-control" autocomplete="off" id="data_from" style="text-align:center;" id="data_from"  value="<?php if (isset($_SESSION['casi_di_successo_filter_data_from'])) { echo $_SESSION['casi_di_successo_filter_data_from']; } ?>">
                    </div></div>

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">To data</label>
                        <input type="date"   class="form-control" autocomplete="off" id="data_to" style="text-align:center;" id="data_to"  value="<?php if (isset($_SESSION['casi_di_successo_filter_data_to'])) { echo $_SESSION['casi_di_successo_filter_data_to']; } ?>">
                    </div></div>

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Sito</label>
                        <input type="text"   class="form-control" autocomplete="off" id="sito" value="<?php if (isset($_SESSION['casi_di_successo_filter_sito'])) { echo $_SESSION['casi_di_successo_filter_sito']; } ?>">
                    </div></div>

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Foto</label>
                        <input type="text"   class="form-control" autocomplete="off" id="foto" value="<?php if (isset($_SESSION['casi_di_successo_filter_foto'])) { echo $_SESSION['casi_di_successo_filter_foto']; } ?>">
                    </div></div>

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Storia</label>
                        <input type="text"   class="form-control" autocomplete="off" id="storia" value="<?php if (isset($_SESSION['casi_di_successo_filter_storia'])) { echo $_SESSION['casi_di_successo_filter_storia']; } ?>">
                    </div></div>

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Sfida</label>
                        <input type="text"   class="form-control" autocomplete="off" id="sfida" value="<?php if (isset($_SESSION['casi_di_successo_filter_sfida'])) { echo $_SESSION['casi_di_successo_filter_sfida']; } ?>">
                    </div></div>

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Soluzione</label>
                        <input type="text"   class="form-control" autocomplete="off" id="soluzione" value="<?php if (isset($_SESSION['casi_di_successo_filter_soluzione'])) { echo $_SESSION['casi_di_successo_filter_soluzione']; } ?>">
                    </div></div>

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Extra</label>
                        <input type="text"   class="form-control" autocomplete="off" id="extra" value="<?php if (isset($_SESSION['casi_di_successo_filter_extra'])) { echo $_SESSION['casi_di_successo_filter_extra']; } ?>">
                    </div></div>


                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Settore</label>
                        <select class="form-control select" id="id_settore" >
                        <option value=""></option>
                        <?php
                            $selected_id = "";
                            if (isset($_SESSION["casi_di_successo_filter_id_settore"])) {
                                if (intval($_SESSION["casi_di_successo_filter_id_settore"]) > 0) {
                                    $selected_id = $_SESSION["casi_di_successo_filter_id_settore"];
                                }
                            }
                            $query = $db->query("SELECT id, titolo FROM settore ORDER BY titolo");
                            while($risultati = mysqli_fetch_array($query)) {
                                if ($selected_id == $risultati["id"]) { $selected = " SELECTED "; } else { $selected = " "; }
                                echo '<option value="'.$risultati["id"].'"'.$selected.'>'.$risultati["titolo"].'</option>';
                            }
                        ?>
                        </select>
                    </div></div>


                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Regione</label>
                        <select class="form-control select" id="id_regione" >
                        <option value=""></option>
                        <?php
                            $selected_id = "";
                            if (isset($_SESSION["casi_di_successo_filter_id_regione"])) {
                                if (intval($_SESSION["casi_di_successo_filter_id_regione"]) > 0) {
                                    $selected_id = $_SESSION["casi_di_successo_filter_id_regione"];
                                }
                            }
                            $query = $db->query("SELECT id, nome FROM regione ORDER BY nome");
                            while($risultati = mysqli_fetch_array($query)) {
                                if ($selected_id == $risultati["id"]) { $selected = " SELECTED "; } else { $selected = " "; }
                                echo '<option value="'.$risultati["id"].'"'.$selected.'>'.$risultati["nome"].'</option>';
                            }
                        ?>
                        </select>
                    </div></div>


                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Moduli</label>
                        <select class="form-control select" id="id_moduli" >
                        <option value=""></option>
                        <?php
                            $selected_id = "";
                            if (isset($_SESSION["casi_di_successo_filter_id_moduli"])) {
                                if (intval($_SESSION["casi_di_successo_filter_id_moduli"]) > 0) {
                                    $selected_id = $_SESSION["casi_di_successo_filter_id_moduli"];
                                }
                            }
                            $query = $db->query("SELECT id, nome FROM moduli ORDER BY nome");
                            while($risultati = mysqli_fetch_array($query)) {
                                if ($selected_id == $risultati["id"]) { $selected = " SELECTED "; } else { $selected = " "; }
                                echo '<option value="'.$risultati["id"].'"'.$selected.'>'.$risultati["nome"].'</option>';
                            }
                        ?>
                        </select>
                    </div></div>


                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Moduli1</label>
                        <select class="form-control select" id="id_moduli1" >
                        <option value=""></option>
                        <?php
                            $selected_id = "";
                            if (isset($_SESSION["casi_di_successo_filter_id_moduli1"])) {
                                if (intval($_SESSION["casi_di_successo_filter_id_moduli1"]) > 0) {
                                    $selected_id = $_SESSION["casi_di_successo_filter_id_moduli1"];
                                }
                            }
                            $query = $db->query("SELECT id, nome FROM moduli ORDER BY nome");
                            while($risultati = mysqli_fetch_array($query)) {
                                if ($selected_id == $risultati["id"]) { $selected = " SELECTED "; } else { $selected = " "; }
                                echo '<option value="'.$risultati["id"].'"'.$selected.'>'.$risultati["nome"].'</option>';
                            }
                        ?>
                        </select>
                    </div></div>


                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Moduli2</label>
                        <select class="form-control select" id="id_moduli2" >
                        <option value=""></option>
                        <?php
                            $selected_id = "";
                            if (isset($_SESSION["casi_di_successo_filter_id_moduli2"])) {
                                if (intval($_SESSION["casi_di_successo_filter_id_moduli2"]) > 0) {
                                    $selected_id = $_SESSION["casi_di_successo_filter_id_moduli2"];
                                }
                            }
                            $query = $db->query("SELECT id, nome FROM moduli ORDER BY nome");
                            while($risultati = mysqli_fetch_array($query)) {
                                if ($selected_id == $risultati["id"]) { $selected = " SELECTED "; } else { $selected = " "; }
                                echo '<option value="'.$risultati["id"].'"'.$selected.'>'.$risultati["nome"].'</option>';
                            }
                        ?>
                        </select>
                    </div></div>


                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Moduli3</label>
                        <select class="form-control select" id="id_moduli3" >
                        <option value=""></option>
                        <?php
                            $selected_id = "";
                            if (isset($_SESSION["casi_di_successo_filter_id_moduli3"])) {
                                if (intval($_SESSION["casi_di_successo_filter_id_moduli3"]) > 0) {
                                    $selected_id = $_SESSION["casi_di_successo_filter_id_moduli3"];
                                }
                            }
                            $query = $db->query("SELECT id, nome FROM moduli ORDER BY nome");
                            while($risultati = mysqli_fetch_array($query)) {
                                if ($selected_id == $risultati["id"]) { $selected = " SELECTED "; } else { $selected = " "; }
                                echo '<option value="'.$risultati["id"].'"'.$selected.'>'.$risultati["nome"].'</option>';
                            }
                        ?>
                        </select>
                    </div></div>


                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Moduli4</label>
                        <select class="form-control select" id="id_moduli4" >
                        <option value=""></option>
                        <?php
                            $selected_id = "";
                            if (isset($_SESSION["casi_di_successo_filter_id_moduli4"])) {
                                if (intval($_SESSION["casi_di_successo_filter_id_moduli4"]) > 0) {
                                    $selected_id = $_SESSION["casi_di_successo_filter_id_moduli4"];
                                }
                            }
                            $query = $db->query("SELECT id, nome FROM moduli ORDER BY nome");
                            while($risultati = mysqli_fetch_array($query)) {
                                if ($selected_id == $risultati["id"]) { $selected = " SELECTED "; } else { $selected = " "; }
                                echo '<option value="'.$risultati["id"].'"'.$selected.'>'.$risultati["nome"].'</option>';
                            }
                        ?>
                        </select>
                    </div></div>


                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="text-center">
                                <button onclick="event.preventDefault(); applyFilters();"
                                        style="width: 150px; padding: 10px; border-radius: 5px;"
                                        class="btn btn-sm btn-outline-info">
                                            <i class="icon-search-1" style="float: left; margin-top: 2px;"></i> Search
                                </button><br>
                                <button onclick="event.preventDefault(); resetFilters();"
                                        style="width: 150px; padding: 10px; border-radius: 5px; margin: 3px 0px 3px 0px;"
                                        class="btn btn-sm btn-outline-warning">
                                            <i class="icon-cancel-alt" style="float: left; margin-top: 2px;"></i> Reset
                                </button><br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="card">
    <table id="myDatatable" class="display table table-striped datatable-responsive" style="width:100%; overflow: hidden;">
        <thead>
            <tr>
                <th><b>Id</b></th>
                <th><b>Titolo</b></th>
                <th><b>Sottotitolo</b></th>
                <th><b>Cliente</b></th>
                <th><b>Data</b></th>

                <th><b>Actions</b></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th><b>Id</b></th>
                <th><b>Titolo</b></th>
                <th><b>Sottotitolo</b></th>
                <th><b>Cliente</b></th>
                <th><b>Data</b></th>

                <th><b>Actions</b></th>
            </tr>
        </tfoot>
    </table>
</div>

<div id="modal_edit" class="modal fade" style="-webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none;" unselectable="on">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<h6 class="modal-title"><i class="icon-smile"></i> <span id="modal_mode"></span></h6>
			</div>
            <div class="modal-body">
                <div class="content">
                        
                    <input type="hidden" id="record_id">
            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">Titolo</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <input type="text" id="record_titolo" maxlength="100" autocomplete="off" class="form-control" >
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">Sottotitolo</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <input type="text" id="record_sottotitolo" maxlength="500" autocomplete="off" class="form-control" >
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">Cliente</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <input type="text" id="record_cliente" maxlength="100" autocomplete="off" class="form-control" >
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">Urllogo</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <input type="text" id="record_urllogo" maxlength="500" autocomplete="off" class="form-control" >
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">Data</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <input type="date" style="text-align:center; width:170px;" id="record_data" autocomplete="off" class="form-control"  >
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">Sito</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <input type="text" id="record_sito" maxlength="100" autocomplete="off" class="form-control" >
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">Foto</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <div class="row">
                        <div class="col-lg-12 dropzonerow">
                            <form class="dropzone" id="dropzone_foto"></form>
                        </div>
                        <div class="col-lg-12" id="fileContainer_foto"></div>    
                    </div>
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">Storia</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <input type="text" id="record_storia" maxlength="65535" autocomplete="off" class="form-control" >
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">Sfida</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <input type="text" id="record_sfida" maxlength="65535" autocomplete="off" class="form-control" >
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">Soluzione</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <input type="text" id="record_soluzione" maxlength="65535" autocomplete="off" class="form-control" >
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">Extra</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <input type="text" id="record_extra" maxlength="65535" autocomplete="off" class="form-control" >
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">Settore</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <select class="form-control select" id="record_id_settore"  >
                    <option value=""></option>
                    <?php
                        $query = $db->query("SELECT id, titolo FROM settore ORDER BY titolo");
                        while($risultati = mysqli_fetch_array($query)) {
                            echo '<option value="'.$risultati["id"].'">'.$risultati["titolo"].'</option>';
                        }
                    ?>
                    </select>
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">Regione</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <select class="form-control select" id="record_id_regione"  >
                    <option value=""></option>
                    <?php
                        $query = $db->query("SELECT id, nome FROM regione ORDER BY nome");
                        while($risultati = mysqli_fetch_array($query)) {
                            echo '<option value="'.$risultati["id"].'">'.$risultati["nome"].'</option>';
                        }
                    ?>
                    </select>
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">Moduli</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <select class="form-control select" id="record_id_moduli"  >
                    <option value=""></option>
                    <?php
                        $query = $db->query("SELECT id, nome FROM moduli ORDER BY nome");
                        while($risultati = mysqli_fetch_array($query)) {
                            echo '<option value="'.$risultati["id"].'">'.$risultati["nome"].'</option>';
                        }
                    ?>
                    </select>
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">Moduli1</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <select class="form-control select" id="record_id_moduli1"  >
                    <option value=""></option>
                    <?php
                        $query = $db->query("SELECT id, nome FROM moduli ORDER BY nome");
                        while($risultati = mysqli_fetch_array($query)) {
                            echo '<option value="'.$risultati["id"].'">'.$risultati["nome"].'</option>';
                        }
                    ?>
                    </select>
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">Moduli2</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <select class="form-control select" id="record_id_moduli2"  >
                    <option value=""></option>
                    <?php
                        $query = $db->query("SELECT id, nome FROM moduli ORDER BY nome");
                        while($risultati = mysqli_fetch_array($query)) {
                            echo '<option value="'.$risultati["id"].'">'.$risultati["nome"].'</option>';
                        }
                    ?>
                    </select>
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">Moduli3</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <select class="form-control select" id="record_id_moduli3"  >
                    <option value=""></option>
                    <?php
                        $query = $db->query("SELECT id, nome FROM moduli ORDER BY nome");
                        while($risultati = mysqli_fetch_array($query)) {
                            echo '<option value="'.$risultati["id"].'">'.$risultati["nome"].'</option>';
                        }
                    ?>
                    </select>
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">Moduli4</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <select class="form-control select" id="record_id_moduli4"  >
                    <option value=""></option>
                    <?php
                        $query = $db->query("SELECT id, nome FROM moduli ORDER BY nome");
                        while($risultati = mysqli_fetch_array($query)) {
                            echo '<option value="'.$risultati["id"].'">'.$risultati["nome"].'</option>';
                        }
                    ?>
                    </select>
                </div>
            </div>

                </div>
            </div>
			<div class="modal-footer">
                <button onclick="save_record();" class="btn btn-lg btn-outline-success savebutton"><i class="icon-ok-3"></i> Save</button>
                <button onclick="$('#modal_edit').modal('hide');" class="btn btn-lg btn-outline-warning"><i class="icon-undo"></i> Cancel</button>
			</div>
		</div>
	</div>
</div>


</div>
</div>
</div>


<script>
    var FILELIST_foto = new Array;
    
    function new_record() {
        $('#modal_mode').html('New Casi_di_successo');
        $("#record_id").val("");
        $("#record_titolo").val("");
        $("#record_sottotitolo").val("");
        $("#record_cliente").val("");
        $("#record_urllogo").val("");
        $("#record_data").val("");
        $("#record_sito").val("");

        document.querySelector("#dropzone_foto").dropzone.removeAllFiles();
        FILELIST_foto = new Array;
        $("#fileContainer_foto").html("");

        $("#record_storia").val("");
        $("#record_sfida").val("");
        $("#record_soluzione").val("");
        $("#record_extra").val("");
        $("#record_id_settore").val(0).trigger("change");
        $("#record_id_regione").val(0).trigger("change");
        $("#record_id_moduli").val(0).trigger("change");
        $("#record_id_moduli1").val(0).trigger("change");
        $("#record_id_moduli2").val(0).trigger("change");
        $("#record_id_moduli3").val(0).trigger("change");
        $("#record_id_moduli4").val(0).trigger("change");
        setTimeout(function(){ $("#record_titolo").focus(); }, 300);
        if (can_create) { $(".savebutton").show(); $(".dropzone").show(); $(".icon-trash-empty").show(); } else { $(".savebutton").hide();  $(".dropzone").hide(); $(".icon-trash-empty").hide(); }
        $('#modal_edit').modal({backdrop: 'static'});
    }

    function edit_record(id) {
        if (can_edit) { $('#modal_mode').html('Edit Casi_di_successo'); } else { $('#modal_mode').html('View Casi_di_successo'); }
        $.ajax({
            async: false,
            type: "POST",
            url: "controllers/casi_di_successo_controller.php",
            data: {
                "action": "read_record",
                "id": id
            },
            success: function (data, stato) {
                var data = $.parseJSON(data);
                if (data == "NOT AUTHORIZED") { return; }

                $("#record_id").val(data["record_id"]);
                $("#record_titolo").val(data["record_titolo"]);
                $("#record_sottotitolo").val(data["record_sottotitolo"]);
                $("#record_cliente").val(data["record_cliente"]);
                $("#record_urllogo").val(data["record_urllogo"]);
                $("#record_data").val(data["record_data"]);
                $("#record_sito").val(data["record_sito"]);

                $("#fileContainer_foto").html("");
                document.querySelector("#dropzone_foto").dropzone.removeAllFiles();
                FILELIST_foto = new Array;
                if (data.record_foto !== null) {
                    if (data.record_foto.length > 0) {
                        FILELIST_foto = JSON.parse(data.record_foto);
                        if (FILELIST_foto === null) {
                            FILELIST_foto = new Array;
                        } else {
                            drawFileList(FILELIST_foto, "FILELIST_foto", "fileContainer_foto");
                        }
                    }
                }
                $("#record_storia").val(data["record_storia"]);
                $("#record_sfida").val(data["record_sfida"]);
                $("#record_soluzione").val(data["record_soluzione"]);
                $("#record_extra").val(data["record_extra"]);
                $("#record_id_settore").val(data["record_id_settore"]).trigger("change");
                $("#record_id_regione").val(data["record_id_regione"]).trigger("change");
                $("#record_id_moduli").val(data["record_id_moduli"]).trigger("change");
                $("#record_id_moduli1").val(data["record_id_moduli1"]).trigger("change");
                $("#record_id_moduli2").val(data["record_id_moduli2"]).trigger("change");
                $("#record_id_moduli3").val(data["record_id_moduli3"]).trigger("change");
                $("#record_id_moduli4").val(data["record_id_moduli4"]).trigger("change");
                setTimeout(function(){ $("#record_titolo").focus(); }, 300);
                if (can_edit) { $(".savebutton").show(); $(".dropzone").show(); $(".icon-trash-empty").show(); } else { $(".savebutton").hide(); $(".dropzone").hide(); $(".icon-trash-empty").hide(); }
                $('#modal_edit').modal({backdrop: 'static'});
            }
        });
    }
    
    function save_record() {
        var requiredfields = "";
        $(".modal-body :required").each(function(){
            if (($(this).val() == "") || ($(this).val() === null)) {
                if (requiredfields != "") { requiredfields = requiredfields.concat(", "); }
                var tmp_req_field = $(this).attr("id").replace("record_", "").replace("_", " ");
                if (tmp_req_field.substr(0, 3) == "id ") { tmp_req_field = tmp_req_field.replace("id ", ""); }
                requiredfields = requiredfields.concat(tmp_req_field);
            }
        })
        if (requiredfields != "") { toast("Required fields: ".concat(requiredfields), "error"); return; }

        $.ajax({
            async: false,
            type: "POST",
            url: "controllers/casi_di_successo_controller.php",
            data: {
                "action": "save_record",
                "id": $("#record_id").val(), 
                "titolo": $("#record_titolo").val(), 
                "sottotitolo": $("#record_sottotitolo").val(), 
                "cliente": $("#record_cliente").val(), 
                "urllogo": $("#record_urllogo").val(), 
                "data": $("#record_data").val(), 
                "sito": $("#record_sito").val(), 
                "foto": JSON.stringify(FILELIST_foto),
                "storia": $("#record_storia").val(), 
                "sfida": $("#record_sfida").val(), 
                "soluzione": $("#record_soluzione").val(), 
                "extra": $("#record_extra").val(), 
                "id_settore": $("#record_id_settore").val(), 
                "id_regione": $("#record_id_regione").val(), 
                "id_moduli": $("#record_id_moduli").val(), 
                "id_moduli1": $("#record_id_moduli1").val(), 
                "id_moduli2": $("#record_id_moduli2").val(), 
                "id_moduli3": $("#record_id_moduli3").val(), 
                "id_moduli4": $("#record_id_moduli4").val(), 

            },
            success: function (data, stato) {
                if (data != "OK") {
                    swal({
                        title: "Error",
                        text: "An error occurred during Save: " + data.toString(),
                        confirmButtonColor: "#66BB6A",
                        type: "warning"
                    });
                } else {
                    $('#modal_edit').modal('hide');
                    update_table();
                }
            }
        });
    }

    function update_table()  {
        table = $('#myDatatable').DataTable();
        table.destroy();
        $("#myDatatable > tbody").html("");
        load_table();
        table = $('#myDatatable').DataTable();
        table.page(0).draw(false);
    }

    $(document).ready(function () {

        $('.select').select2({ width: '100%' });

        new Dropzone("#dropzone_foto", {
            init: function() {
                this.on("success", function(file, serverFileName) {
                    FILELIST_foto.push({"serverFileName" : serverFileName, "fileName" : file.name});
                    drawFileList(FILELIST_foto, "FILELIST_foto", "fileContainer_foto");
                });
                this.on("error", function(file, errorMessage) {
                    message_box("Error", errorMessage, "Ok", "error");
                });
            },
            url: "controllers/sys_file_uploader.php" });

        load_table();
        table = $('#myDatatable').DataTable();
        table.on('draw', function () {
            table.rows().every(function (rowIdx, tableLoop, rowLoop) {
                var values_in_row = this.data();
                $.ajax({
                    async: false,
                    type: "POST",
                    url: "controllers/casi_di_successo_controller.php",
                    data: {
                        "action": "button_record",
                        "id": values_in_row[0]
                    },
                    success: function (data, stato) {
                        values_in_row[5] = data;
                    }
                });
                this.data(values_in_row);
            });
        });
    });


    function delete_record(id) {
        swal({
            title: "Sei sicuro?",
            text: "I dati non potranno essere recuperati",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: "Si, elimina!",
            cancelButtonText: "No, annulla!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    async: true,
                    type: "POST",
                    url : "controllers/casi_di_successo_controller.php",
                    data: {
                        "action": "delete_record",
                        "id": id
                    },
                    success: function (data) {
                        if (data === "OK") {
                            swal({
                                title: "Eliminato!",
                                text: "Il record è stato cancellato",
                                confirmButtonColor: "#66BB6A",
                                type: "success"
                            }, function () {
                                document.location.reload();
                            });
                        } else {
                            swal({
                                title: "Operazione annullata",
                                text: "Qualcosa è andato storto",
                                confirmButtonColor: "#2196F3",
                                type: "error"
                            });
                        }
                    },
                    error: function (data) {
                        swal({
                            title: "Operazione annullata",
                            text: "Qualcosa è andato storto",
                            confirmButtonColor: "#2196F3",
                            type: "error"
                        });
                    }
                });
            }
            else {
                swal({
                    title: "Operazione annullata",
                    text: "",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
    }


    function load_table() {

        filtertitolo = "";
        if (parseInt($("#titolo").val().length) > 0) { filtertitolo = $("#titolo").val(); }

        filtersottotitolo = "";
        if (parseInt($("#sottotitolo").val().length) > 0) { filtersottotitolo = $("#sottotitolo").val(); }

        filtercliente = "";
        if (parseInt($("#cliente").val().length) > 0) { filtercliente = $("#cliente").val(); }

        filterurllogo = "";
        if (parseInt($("#urllogo").val().length) > 0) { filterurllogo = $("#urllogo").val(); }

        filterdata_from = "";
        if (parseInt($("#data_from").val().length) > 0) { filterdata_from = $("#data_from").val(); }
        filterdata_to = "";
        if (parseInt($("#data_to").val().length) > 0) { filterdata_to = $("#data_to").val(); }

        filtersito = "";
        if (parseInt($("#sito").val().length) > 0) { filtersito = $("#sito").val(); }

        filterfoto = "";
        if (parseInt($("#foto").val().length) > 0) { filterfoto = $("#foto").val(); }

        filterstoria = "";
        if (parseInt($("#storia").val().length) > 0) { filterstoria = $("#storia").val(); }

        filtersfida = "";
        if (parseInt($("#sfida").val().length) > 0) { filtersfida = $("#sfida").val(); }

        filtersoluzione = "";
        if (parseInt($("#soluzione").val().length) > 0) { filtersoluzione = $("#soluzione").val(); }

        filterextra = "";
        if (parseInt($("#extra").val().length) > 0) { filterextra = $("#extra").val(); }

        filterid_settore = "";
        if ($("#id_settore").val() !== null) { filterid_settore = $("#id_settore").val(); }

        filterid_regione = "";
        if ($("#id_regione").val() !== null) { filterid_regione = $("#id_regione").val(); }

        filterid_moduli = "";
        if ($("#id_moduli").val() !== null) { filterid_moduli = $("#id_moduli").val(); }

        filterid_moduli1 = "";
        if ($("#id_moduli1").val() !== null) { filterid_moduli1 = $("#id_moduli1").val(); }

        filterid_moduli2 = "";
        if ($("#id_moduli2").val() !== null) { filterid_moduli2 = $("#id_moduli2").val(); }

        filterid_moduli3 = "";
        if ($("#id_moduli3").val() !== null) { filterid_moduli3 = $("#id_moduli3").val(); }

        filterid_moduli4 = "";
        if ($("#id_moduli4").val() !== null) { filterid_moduli4 = $("#id_moduli4").val(); }

        $('#myDatatable').DataTable( {
            'iDisplayLength': 25,
            'processing': true,
            'serverSide': true,
            'stateSave': true,
            'searching': false,
            'responsive': true,
            dom: '<"datatable-header"flB><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            buttons: [{
                        extend: 'pdf',
                        orientation: 'landscape',
                        text: '<i class="icon-file-pdf"></i> PDF',
                        title: '',
                        className: 'btn btn-outline-danger',
                        exportOptions: {columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17]}
                    },
                    {
                        extend: 'excel',
                        text: '<i class="icon-file-excel"></i> Excel',
                        title: '',
                        className: 'btn btn-outline-success',
                        exportOptions: {columns: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17]}
                    }
            ],
            language: {
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: {'first': '|&larr;', 'last': '&rarr;|', 'next': '&rarr;', 'previous': '&larr;'}
            },
            ajax: {
                url: 'controllers/casi_di_successo_controller.php',
                type: 'POST',
                data: {
                    action: "load_table",
                    filtertitolo: filtertitolo,
                    filtersottotitolo: filtersottotitolo,
                    filtercliente: filtercliente,
                    filterurllogo: filterurllogo,
                    filterdata_from: filterdata_from,
                    filterdata_to:   filterdata_to,
                    filtersito: filtersito,
                    filterfoto: filterfoto,
                    filterstoria: filterstoria,
                    filtersfida: filtersfida,
                    filtersoluzione: filtersoluzione,
                    filterextra: filterextra,
                    filterid_settore: filterid_settore,
                    filterid_regione: filterid_regione,
                    filterid_moduli: filterid_moduli,
                    filterid_moduli1: filterid_moduli1,
                    filterid_moduli2: filterid_moduli2,
                    filterid_moduli3: filterid_moduli3,
                    filterid_moduli4: filterid_moduli4,

                }
            },
            'columnDefs': [
                { 'targets': 0, 'visible': false },
                { "targets": 1,                           "width": "25%" },
                { "targets": 2,                           "width": "25%" },
                { "targets": 3,                           "width": "25%" },
                { "targets": 4,                           "width": "15%" },
                
                { "targets": 5, "className": "dt-center", "orderable": false, "width": "5%" },
            ]
        } );
    }

    function applyFilters() {
        table = $('#myDatatable').DataTable();
        table.destroy();
        $("#myDatatable > tbody").html("");
        load_table();
        table = $('#myDatatable').DataTable();
        table.page(0).draw(false);
    }

    function resetFilters() {
        $("#titolo").val("");
        $("#sottotitolo").val("");
        $("#cliente").val("");
        $("#urllogo").val("");
        $("#data_from").val("");
        $("#data_to").val("");
        $("#sito").val("");
        $("#foto").val("");
        $("#storia").val("");
        $("#sfida").val("");
        $("#soluzione").val("");
        $("#extra").val("");
        $("#id_settore").val("").trigger("change");
        $("#id_regione").val("").trigger("change");
        $("#id_moduli").val("").trigger("change");
        $("#id_moduli1").val("").trigger("change");
        $("#id_moduli2").val("").trigger("change");
        $("#id_moduli3").val("").trigger("change");
        $("#id_moduli4").val("").trigger("change");

        applyFilters();
    }

    
</script>