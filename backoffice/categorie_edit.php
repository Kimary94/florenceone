<?php
    include "sys_header.php";
    if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(1, "read")) {header("location: ".linkto("sys_dashboard")); die();}}
?>

<body class="navbar-top<?php if ($settings_menu_closed == 1) { echo ' sidebar-xs'; } ?>">
    <div id="imagePreviewDiv" class="modal modal-xl fade imgpreview" style="z-index: 9999;"></div>
    <?php include "sys_navbar.php"; ?>
    <div class="page-content">
        <?php
        include "sys_sidebar.php";
        if (!isset($_GET['id'])) {
            if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(1, "create")) {die();}}
        } else {
            if (!checkUserRight(1, "edit")) {echo "<style>.newrow,.icon-trash-empty,.dropzone,.savebutton{display: none;}</style>";}
        }
        ?>
        <div class="content-wrapper">

        <div class="page-header page-header-light">
            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">

                <div class="d-flex">
                    <i class="icon-book-open mr-2 breadcrumb-item"></i>
                    <span style="margin-top: 8px;">
                    <?php
                        if (isset($_GET['id'])) {
                            if (checkUserRight(1, "edit")) {
                                echo "Modifica Categoria";
                            } else {
                                echo "Visualizza Categoria";
                            }
                        } else {
                            echo "Nuova Categoria";
                        }
                    ?>
                    </span>
                </div>

                <div class="header-elements d-none" style="display: flex !important;">
                    <div class="breadcrumb justify-content-center">
                        
                    </div>
                </div>
            </div>
        </div>

<div class="content unselectable">
    <input type="hidden" id="id" <?php if(isset($_GET['id'])) echo 'value="'.$_GET['id'].'"'; ?> >
    <div class="card">
        <div class="card-body">
            <div class="row">

                <div class="col-lg-3 col-md-6">
                    <div class="form-group container_fields">
                        <label class="text-bold">Descrizione</label>
                        <input type="text" id="descrizione"  maxlength="255"  autocomplete="off" class="form-control" >
                    </div>
                </div>

                <div class="col-lg-3 col-md-6">
                    <div class="form-group container_fields">
                        <label class="text-bold">Ordine</label>
                        <input type="number" style="text-align:center;" step="1" id="ordine"    onkeypress="return checkInteger(event)" autocomplete="off" class="form-control"  >
                    </div>
                </div>

                <div class="col-lg-3 col-md-6" style="<?php
                if ($_GET['id'] != 10) { echo "visibility:hidden;"; }
                ?>">
                    <div class="form-group container_fields">
                        <label class="text-bold">Um (per la grammatura)</label>
                        <input type="text" id="um"  maxlength="255"  autocomplete="off" class="form-control" >
                    </div>
                </div>

            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="text-right">
                        <button onclick="event.preventDefault(); save_record();" class="btn btn-lg btn-outline-success savebutton"><i class="icon-ok-3"></i> Salva</button>
                        <button onclick="event.preventDefault(); document.location.href='categorie.php'" class="btn btn-lg btn-outline-warning"><i class="icon-undo"></i> Chiudi</button>
                    </div>
                </div>
            </div>
        </div>
    </div>


        <div class="card">
            <table id="myRowsDatatable" class="display table table-striped datatable-responsive" style="width:100%; overflow: hidden;">
                <thead>
                    <tr>
                        <th><b>Id&nbsp;</b></th>
                        <th><b>Descrizione&nbsp;</b></th>
                        <th><b>Ordine&nbsp;</b></th>

                        <th><b>Azioni</b></th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th><b>Id&nbsp;</b></th>
                        <th><b>Descrizione&nbsp;</b></th>
                        <th><b>Ordine&nbsp;</b></th>

                        <th><b>Azioni</b></th>
                    </tr>
                </tfoot>
            </table>
        </div>

</div>

<div id="modal_detail" class="modal fade" style="-webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none;" unselectable="on">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-primary">
                <h6 class="modal-title"><i class="icon-pencil"></i> Valore</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
            <div class="modal-body">
                <div class="content">
                    <input type="hidden" id="row_id" value="" >
                    <div class="row form-group">
                        <div class="col-lg-4">
                            <label class="text-bold" style="margin: 9px 0px 0px 0px;">Descrizione</label>
                        </div>
                        <div class="col-lg-8" style="margin-top: 3px;">
                            <input style="width:100%;" type="text" id="row_descrizione" value="" class="form-control" autocomplete="off" maxlength="255"  >
                        </div>
                    </div>
                    <input type="hidden" id="row_id_categoria" value="" >
                    <div class="row form-group">
                        <div class="col-lg-4">
                            <label class="text-bold" style="margin: 9px 0px 0px 0px;">Ordine</label>
                        </div>
                        <div class="col-lg-8" style="margin-top: 3px;">
                            <input style="width:170px; text-align:center;"  type="number"    id="row_ordine" value=""  class="form-control col-xs-6"  autocomplete="off" onkeypress="return checkInteger(event)"  >
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-lg-4">
                            <label class="text-bold" style="margin: 9px 0px 0px 0px;">Immagine scheda tecnica</label>
                        </div>
                        <div class="col-lg-8" style="margin-top: 3px;">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form class="dropzone" id="dropzone_row_immagine_scheda_tecnica"></form>
                                </div>
                                <div class="col-lg-12" id="fileContainer_row_immagine_scheda_tecnica"></div>    
                            </div>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-lg-4">
                            <label class="text-bold" style="margin: 9px 0px 0px 0px;">Immagine menu</label>
                        </div>
                        <div class="col-lg-8" style="margin-top: 3px;">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form class="dropzone" id="dropzone_row_immagine_menu"></form>
                                </div>
                                <div class="col-lg-12" id="fileContainer_row_immagine_menu"></div>    
                            </div>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-lg-4">
                            <label class="text-bold" style="margin: 9px 0px 0px 0px;">Immagine wizard</label>
                        </div>
                        <div class="col-lg-8" style="margin-top: 3px;">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form class="dropzone" id="dropzone_row_immagine_wizard"></form>
                                </div>
                                <div class="col-lg-12" id="fileContainer_row_immagine_wizard"></div>    
                            </div>
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-lg-4">
                            <label class="text-bold" style="margin: 9px 0px 0px 0px;">Immagine extra</label>
                        </div>
                        <div class="col-lg-8" style="margin-top: 3px;">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form class="dropzone" id="dropzone_row_immagine_extra"></form>
                                </div>
                                <div class="col-lg-12" id="fileContainer_row_immagine_extra"></div>    
                            </div>
                        </div>
                    </div>

                </div>
            </div>
			<div class="modal-footer">
                <button onclick="save_row();" class="btn btn-lg btn-outline-success savebutton"><i class="icon-ok-3"></i> Salva</button>
                <button onclick="$('#modal_detail').modal('hide');" class="btn btn-lg btn-outline-warning"><i class="icon-undo"></i> Chiudi</button>
			</div>
		</div>
	</div>
</div>

</div>
</div>
</body>


<script type="text/javascript">

    var FILELIST_row_immagine_scheda_tecnica = new Array;
    var FILELIST_row_immagine_menu = new Array;
    var FILELIST_row_immagine_wizard = new Array;
    var FILELIST_row_immagine_extra = new Array;


    $(document).ready(function () {
        $('.select').select2({ width: '100%' });


        new Dropzone("#dropzone_row_immagine_scheda_tecnica", {
            init: function() {
                this.on("success", function(file, serverFileName) {
                    FILELIST_row_immagine_scheda_tecnica.push({"serverFileName" : serverFileName, "fileName" : file.name});
                    drawFileList(FILELIST_row_immagine_scheda_tecnica, "FILELIST_row_immagine_scheda_tecnica", "fileContainer_row_immagine_scheda_tecnica");
                });
                this.on("error", function(file, errorMessage) {
                    message_box("Error", errorMessage, "Ok", "error");
                });
            },
            url: "controllers/sys_file_uploader.php" });


        new Dropzone("#dropzone_row_immagine_menu", {
            init: function() {
                this.on("success", function(file, serverFileName) {
                    FILELIST_row_immagine_menu.push({"serverFileName" : serverFileName, "fileName" : file.name});
                    drawFileList(FILELIST_row_immagine_menu, "FILELIST_row_immagine_menu", "fileContainer_row_immagine_menu");
                });
                this.on("error", function(file, errorMessage) {
                    message_box("Error", errorMessage, "Ok", "error");
                });
            },
            url: "controllers/sys_file_uploader.php" });


        new Dropzone("#dropzone_row_immagine_wizard", {
            init: function() {
                this.on("success", function(file, serverFileName) {
                    FILELIST_row_immagine_wizard.push({"serverFileName" : serverFileName, "fileName" : file.name});
                    drawFileList(FILELIST_row_immagine_wizard, "FILELIST_row_immagine_wizard", "fileContainer_row_immagine_wizard");
                });
                this.on("error", function(file, errorMessage) {
                    message_box("Error", errorMessage, "Ok", "error");
                });
            },
            url: "controllers/sys_file_uploader.php" });


        new Dropzone("#dropzone_row_immagine_extra", {
            init: function() {
                this.on("success", function(file, serverFileName) {
                    FILELIST_row_immagine_extra.push({"serverFileName" : serverFileName, "fileName" : file.name});
                    drawFileList(FILELIST_row_immagine_extra, "FILELIST_row_immagine_extra", "fileContainer_row_immagine_extra");
                });
                this.on("error", function(file, errorMessage) {
                    message_box("Error", errorMessage, "Ok", "error");
                });
            },
            url: "controllers/sys_file_uploader.php" });


        <?php 
        if (isset($_GET["id"])) {
            echo "edit_record(".$_GET["id"].");";
        }
        ?> 

        setTimeout(function(){ $("#descrizione").focus(); }, 300);

        load_table();
        table = $("#myRowsDatatable").DataTable();
        table.on("draw", function () {
            table.rows().every(function (rowIdx, tableLoop, rowLoop) {
                var values_in_row = this.data();
                $.ajax({
                    async: false,
                    type: "POST",
                    url: "controllers/categorie_controller.php",
                    data: {
                        "action": "button_record_row",
                        "id": values_in_row[0]
                    },
                    success: function (data, stato) {
                        values_in_row[3] = data;
                    }
                });
                this.data(values_in_row);
            });
        });

    });


    function edit_record(id) {
        $.ajax({
            async: false,
            type: "POST",
            url: "controllers/categorie_controller.php",
            data: {
                "action": "read_record",
                "id": id
            },
            success: function (data, stato) {
                var data = $.parseJSON(data);
                if ((data.length == 0) || (data == "NOT AUTHORIZED")) {
                    location.href = 'categorie.php';
                } else {
                    $("#id").val(data["record_id"]);
                    $("#descrizione").val(data["record_descrizione"]);
                    $("#ordine").val(data["record_ordine"]);
                    $("#um").val(data["record_um"]);

                }
            }
        });
    }


    function save_record() {
        var requiredfields = "";
        $(".card-body :required").each(function(){
            if (($(this).val() == "") || ($(this).val() === null)) {
                if (requiredfields != "") { requiredfields = requiredfields.concat(", "); }
                var tmp_req_field = $(this).attr("id").replace("_", " ");
                if (tmp_req_field.substr(0, 3) == "id ") { tmp_req_field = tmp_req_field.replace("id ", ""); }
                requiredfields = requiredfields.concat(tmp_req_field);
            }
        })
        if (requiredfields != "") { toast("Required fields: ".concat(requiredfields), "error"); return; }

        $.ajax({
            async: false,
            type: "POST",
            url: "controllers/categorie_controller.php",
            data: {
                "action": "save_record",
                "id": $("#id").val(), 
                "descrizione": $("#descrizione").val(), 
                "ordine": $("#ordine").val(), 
                "um": $("#um").val(), 

            },
            success: function (data, stato) {
                if (data != "OK") {
                    swal({
                        title: "Error",
                        text: "Qualcosa è andato storto " + data.toString(),
                        confirmButtonColor: "#66BB6A",
                        type: "warning"
                    });
                } else {
                    location.href = 'categorie.php'; 
                }
            }
        });
    }


    function add_row() {
        $("#row_id").val("");
        $("#row_descrizione").val("");
        <?php if (!isset($_GET['id'])) { echo '$("#row_id_categoria").val("-'.$_SESSION['user_id'].'");'; } else { echo '$("#row_id_categoria").val("' . $_GET['id'] . '");'; } ?>
        $("#row_ordine").val("");


        document.querySelector("#dropzone_row_immagine_scheda_tecnica").dropzone.removeAllFiles();
        FILELIST_row_immagine_scheda_tecnica = new Array;
        $("#fileContainer_row_immagine_scheda_tecnica").html("");


        document.querySelector("#dropzone_row_immagine_menu").dropzone.removeAllFiles();
        FILELIST_row_immagine_menu = new Array;
        $("#fileContainer_row_immagine_menu").html("");


        document.querySelector("#dropzone_row_immagine_wizard").dropzone.removeAllFiles();
        FILELIST_row_immagine_wizard = new Array;
        $("#fileContainer_row_immagine_wizard").html("");


        document.querySelector("#dropzone_row_immagine_extra").dropzone.removeAllFiles();
        FILELIST_row_immagine_extra = new Array;
        $("#fileContainer_row_immagine_extra").html("");


        setTimeout(function(){ $("#descrizione").focus(); }, 300);
        $("#modal_detail").modal({backdrop: 'static'});
    }
    
    function edit_row(id) {
        $.ajax({
            async: false,
            type: "POST",
            url : "controllers/categorie_controller.php",
            data: {
                "action": "read_record_row",
                "id": id
            },
            success : function (data) {
                if (data.length > 8) {
                    var data = $.parseJSON(data);
                    if (data == "NOT AUTHORIZED") { return; }
                    $("#row_id").val(data.id);
                    $("#row_descrizione").val(data.descrizione);
                    $("#row_id_categoria").val(data.id_categoria).trigger("change");
                    $("#row_ordine").val(data.ordine);


                    $("#fileContainer_row_immagine_scheda_tecnica").html("");
                    document.querySelector("#dropzone_row_immagine_scheda_tecnica").dropzone.removeAllFiles();
                    FILELIST_row_immagine_scheda_tecnica = new Array;
                    if (data.immagine_scheda_tecnica !== null) {
                        if (data.immagine_scheda_tecnica.length > 0) {
                            FILELIST_row_immagine_scheda_tecnica = JSON.parse(data.immagine_scheda_tecnica);
                            if (FILELIST_row_immagine_scheda_tecnica === null) {
                                FILELIST_row_immagine_scheda_tecnica = new Array;
                            } else {
                                drawFileList(FILELIST_row_immagine_scheda_tecnica, "FILELIST_row_immagine_scheda_tecnica", "fileContainer_row_immagine_scheda_tecnica");
                            }
                        }
                    }

                    $("#fileContainer_row_immagine_menu").html("");
                    document.querySelector("#dropzone_row_immagine_menu").dropzone.removeAllFiles();
                    FILELIST_row_immagine_menu = new Array;
                    if (data.immagine_menu !== null) {
                        if (data.immagine_menu.length > 0) {
                            FILELIST_row_immagine_menu = JSON.parse(data.immagine_menu);
                            if (FILELIST_row_immagine_menu === null) {
                                FILELIST_row_immagine_menu = new Array;
                            } else {
                                drawFileList(FILELIST_row_immagine_menu, "FILELIST_row_immagine_menu", "fileContainer_row_immagine_menu");
                            }
                        }
                    }

                    $("#fileContainer_row_immagine_wizard").html("");
                    document.querySelector("#dropzone_row_immagine_wizard").dropzone.removeAllFiles();
                    FILELIST_row_immagine_wizard = new Array;
                    if (data.immagine_wizard !== null) {
                        if (data.immagine_wizard.length > 0) {
                            FILELIST_row_immagine_wizard = JSON.parse(data.immagine_wizard);
                            if (FILELIST_row_immagine_wizard === null) {
                                FILELIST_row_immagine_wizard = new Array;
                            } else {
                                drawFileList(FILELIST_row_immagine_wizard, "FILELIST_row_immagine_wizard", "fileContainer_row_immagine_wizard");
                            }
                        }
                    }

                    $("#fileContainer_row_immagine_extra").html("");
                    document.querySelector("#dropzone_row_immagine_extra").dropzone.removeAllFiles();
                    FILELIST_row_immagine_extra = new Array;
                    if (data.immagine_extra !== null) {
                        if (data.immagine_extra.length > 0) {
                            FILELIST_row_immagine_extra = JSON.parse(data.immagine_extra);
                            if (FILELIST_row_immagine_extra === null) {
                                FILELIST_row_immagine_extra = new Array;
                            } else {
                                drawFileList(FILELIST_row_immagine_extra, "FILELIST_row_immagine_extra", "fileContainer_row_immagine_extra");
                            }
                        }
                    }

                    setTimeout(function(){ $("#row_descrizione").focus(); }, 300);
                    $("#modal_detail").modal({backdrop: 'static'});
                }
            }
        });
    }


    function save_row() {
        var requiredfields = "";
        $(".modal-body :required").each(function(){
            if (($(this).val() == "") || ($(this).val() === null)) {
                if (requiredfields != "") { requiredfields = requiredfields.concat(", "); }
                var tmp_req_field = $(this).attr("id").replace("row_", "").replace("_", " ");
                if (tmp_req_field.substr(0, 3) == "id ") { tmp_req_field = tmp_req_field.replace("id ", ""); }
                requiredfields = requiredfields.concat(tmp_req_field);
            }
        })
        if (requiredfields != "") { toast("Required fields: ".concat(requiredfields), "error"); return; }

        $.ajax({
            async: false,
            type: "POST",
            url: "controllers/categorie_controller.php",
            data: {
                "action": "save_record_row",
                "id": $('#row_id').val(),
                "descrizione": $('#row_descrizione').val(),
                "id_categoria": $('#row_id_categoria').val(),
                "ordine": $('#row_ordine').val(),
                "immagine_scheda_tecnica": JSON.stringify(FILELIST_row_immagine_scheda_tecnica),
                "immagine_menu": JSON.stringify(FILELIST_row_immagine_menu),
                "immagine_wizard": JSON.stringify(FILELIST_row_immagine_wizard),
                "immagine_extra": JSON.stringify(FILELIST_row_immagine_extra),

            },
            success: function (data, stato) {
                if (data != "OK") {
                    swal({
                        title: "Error",
                        text: "An error occurred during Save: " + data.toString(),
                        confirmButtonColor: "#66BB6A",
                        type: "warning"
                    });
                } else {
                    $("#modal_detail").modal('hide');
                    update_table();
                    
                    if ($('#row_id').val() == '') {
                        setTimeout(function(){
                            add_row();
                        }, 700);
                    }
                    
                }
            }
        });
    }


    function update_table()  {
        table = $('#myRowsDatatable').DataTable();
        table.destroy();
        $("#myRowsDatatable > tbody").html("");
        load_table();
        table = $('#myRowsDatatable').DataTable();
        table.page(0).draw(false);
    }

    
    function load_table() {
        $('#myRowsDatatable').DataTable( {
            'processing': true,
            'serverSide': true,
            'stateSave': true,
            'searching': false,
            'responsive': true,
            dom: '<"datatable-header"flB><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            buttons: [{
                        text: '<i class="icon-plus-squared"></i> Nuovo valore',
                        className: 'btn btn-outline-info newrow',
                        action: function ( e, dt, node, config ) {
                            add_row();
                        }
                    },
                    {
                        extend: 'pdf',
                        orientation: 'landscape',
                        text: '<i class="icon-file-pdf"></i> PDF',
                        title: '',
                        className: 'btn btn-outline-danger',
                        exportOptions: {columns: [1, 2]}
                    },
                    {
                        extend: 'excel',
                        text: '<i class="icon-file-excel"></i> Excel',
                        title: '',
                        className: 'btn btn-outline-success',
                        exportOptions: {columns: [1, 2]}
                    }
            ],
            language: {
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: {'first': '|&larr;', 'last': '&rarr;|', 'next': '&rarr;', 'previous': '&larr;'}
            },
            ajax: {
                url: 'controllers/categorie_controller.php',
                type: 'POST',
                data: {
                    action: 'load_table_rows',
                    filter_id: <?php if (!isset($_GET['id'])) { echo "-".$_SESSION["user_id"]; } else { echo $_GET['id']; } ?>,
                }
            },
            'columnDefs': [
                { 'targets': 0, 'visible': false },
                { "targets": 1,                           "width": "25%" },
                { "targets": 2, "className": "dt-center", "width": "10%" },

                { "targets": 3, "className": "dt-center", "orderable": false, "width": "5%" },
            ]
        } );
    }


    function delete_row(id) {
        swal({
            title: "Sei sicuro?",
            text: "I dati non potranno essere recuperati",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: "Si, elimina!",
            cancelButtonText: "No, annulla!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    async: true,
                    type: "POST",
                    url : "controllers/categorie_controller.php",
                    data: {
                        "action": "delete_record_row",
                        "id": id
                    },
                    success: function (data) {
                        if (data === "OK") {
                            swal({
                                title: "Eliminato!",
                                text: "Il record è stato cancellato",
                                confirmButtonColor: "#66BB6A",
                                type: "success"
                            }, function () {
                                update_table();
                            });
                        } else {
                            swal({
                                title: "Operazione annullata",
                                text: "Qualcosa è andato storto",
                                confirmButtonColor: "#2196F3",
                                type: "error"
                            });
                        update_table();
                        }
                    },
                    error: function (data) {
                        swal({
                            title: "Operazione annullata",
                            text: "Qualcosa è andato storto",
                            confirmButtonColor: "#2196F3",
                            type: "error"
                        });
                        update_table();
                    }
                });
            }
            else {
                swal({
                    title: "Operazione annullata",
                    text: "",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
    }
</script>