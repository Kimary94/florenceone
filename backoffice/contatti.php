<?php
    include "sys_header.php";
    if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(15, "read")) {header("location: ".linkto("sys_dashboard")); die();}}
?>

<body class="navbar-top<?php if ($settings_menu_closed == 1) { echo ' sidebar-xs'; } ?>">
    <div id="imagePreviewDiv" class="modal modal-xl fade imgpreview" style="z-index: 9999;"></div>
    <?php include "sys_navbar.php"; ?>
    <div class="page-content">
        <?php
        include "sys_sidebar.php";
        if ($_SESSION["user_username"] != "sysadmin") {
            if (!checkUserRight(15, "create")) {echo "<script>can_create = false;</script>";} else {echo "<script>can_create = true;</script>";}
            if (!checkUserRight(15, "edit")) {echo "<script>can_edit = false;</script>";} else {echo "<script>can_edit = true;</script>";}
        } else {
            echo "<script>can_create = true; can_edit = true;</script>";
        }
        ?>
        <div class="content-wrapper">

        
        <!-- Page header -->
        <div class="page-header page-header-light">
            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">

                <div class="d-flex">
                    <i class="icon-users mr-2 breadcrumb-item"></i><span style="margin-top: 8px;">Contatti</span>
                </div>

                <div class="header-elements d-none" style="display: flex !important;">
                    <div class="breadcrumb justify-content-center">
                        <?php
                        $show_button_new = true;
                        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(15, "create")) { $show_button_new = false; }}
                        if ($show_button_new) { ?>
                            <a href="#" class="breadcrumb-elements-item" onclick="new_record();">
                                <i class="icon-plus-squared"></i>
                                New record
                            </a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>


<div class="content unselectable">
    <div class="card card-collapsed" style="background-color: #f9f9f9;">
        <div class="card-header header-elements-inline" onmouseup="$('#searchexpand').click();" style="font-variant: all-petite-caps;">
            <div><i class="icon-search-7"></i> Filtri di ricerca</div>
            <div data-action="collapse" id="searchexpand" style="display:none;"></div>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-lg-10 col-md-9 col-sm-12 col-xs-12">
                    <div class="row">

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Nome</label>
                        <input type="text"   class="form-control" autocomplete="off" id="nome" value="<?php if (isset($_SESSION['contatti_filter_nome'])) { echo $_SESSION['contatti_filter_nome']; } ?>">
                    </div></div>

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Azienda</label>
                        <input type="text"   class="form-control" autocomplete="off" id="azienda" value="<?php if (isset($_SESSION['contatti_filter_azienda'])) { echo $_SESSION['contatti_filter_azienda']; } ?>">
                    </div></div>

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Email</label>
                        <input type="text"   class="form-control" autocomplete="off" id="email" value="<?php if (isset($_SESSION['contatti_filter_email'])) { echo $_SESSION['contatti_filter_email']; } ?>">
                    </div></div>

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Telefono</label>
                        <input type="text"   class="form-control" autocomplete="off" id="telefono" value="<?php if (isset($_SESSION['contatti_filter_telefono'])) { echo $_SESSION['contatti_filter_telefono']; } ?>">
                    </div></div>

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Pagina</label>
                        <input type="text"   class="form-control" autocomplete="off" id="pagina" value="<?php if (isset($_SESSION['contatti_filter_pagina'])) { echo $_SESSION['contatti_filter_pagina']; } ?>">
                    </div></div>

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">From data</label>
                        <input type="date"   class="form-control" autocomplete="off" id="data_from" style="text-align:center;" id="data_from"  value="<?php if (isset($_SESSION['contatti_filter_data_from'])) { echo $_SESSION['contatti_filter_data_from']; } ?>">
                    </div></div>

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">To data</label>
                        <input type="date"   class="form-control" autocomplete="off" id="data_to" style="text-align:center;" id="data_to"  value="<?php if (isset($_SESSION['contatti_filter_data_to'])) { echo $_SESSION['contatti_filter_data_to']; } ?>">
                    </div></div>


                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="text-center">
                                <button onclick="event.preventDefault(); applyFilters();"
                                        style="width: 150px; padding: 10px; border-radius: 5px;"
                                        class="btn btn-sm btn-outline-info">
                                            <i class="icon-search-1" style="float: left; margin-top: 2px;"></i> Search
                                </button><br>
                                <button onclick="event.preventDefault(); resetFilters();"
                                        style="width: 150px; padding: 10px; border-radius: 5px; margin: 3px 0px 3px 0px;"
                                        class="btn btn-sm btn-outline-warning">
                                            <i class="icon-cancel-alt" style="float: left; margin-top: 2px;"></i> Reset
                                </button><br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="card">
    <table id="myDatatable" class="display table table-striped datatable-responsive" style="width:100%; overflow: hidden;">
        <thead>
            <tr>
                <th><b>Id</b></th>
                <th><b>Nome</b></th>
                <th><b>Azienda</b></th>
                <th><b>Email</b></th>
                <th><b>Telefono</b></th>
                <th><b>Pagina</b></th>
                <th><b>Data</b></th>

                <th><b>Actions</b></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th><b>Id</b></th>
                <th><b>Nome</b></th>
                <th><b>Azienda</b></th>
                <th><b>Email</b></th>
                <th><b>Telefono</b></th>
                <th><b>Pagina</b></th>
                <th><b>Data</b></th>

                <th><b>Actions</b></th>
            </tr>
        </tfoot>
    </table>
</div>

<div id="modal_edit" class="modal fade" style="-webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none;" unselectable="on">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<h6 class="modal-title"><i class="icon-users"></i> <span id="modal_mode"></span></h6>
			</div>
            <div class="modal-body">
                <div class="content">
                        
                    <input type="hidden" id="record_id">
            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">Nome</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <input type="text" id="record_nome" maxlength="255" autocomplete="off" class="form-control" >
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">Azienda</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <input type="text" id="record_azienda" maxlength="255" autocomplete="off" class="form-control" >
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">Email</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <input type="text" id="record_email" maxlength="255" autocomplete="off" class="form-control" >
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">Telefono</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <input type="text" id="record_telefono" maxlength="255" autocomplete="off" class="form-control" >
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">Pagina</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <input type="text" id="record_pagina" maxlength="255" autocomplete="off" class="form-control" >
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">Data</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <input type="date" style="text-align:center; width:170px;" id="record_data" autocomplete="off" class="form-control"  >
                </div>
            </div>

                </div>
            </div>
			<div class="modal-footer">
                <button onclick="save_record();" class="btn btn-lg btn-outline-success savebutton"><i class="icon-ok-3"></i> Save</button>
                <button onclick="$('#modal_edit').modal('hide');" class="btn btn-lg btn-outline-warning"><i class="icon-undo"></i> Cancel</button>
			</div>
		</div>
	</div>
</div>


</div>
</div>
</div>


<script>
    
    function new_record() {
        $('#modal_mode').html('New Contatti');
        $("#record_id").val("");
        $("#record_nome").val("");
        $("#record_azienda").val("");
        $("#record_email").val("");
        $("#record_telefono").val("");
        $("#record_pagina").val("");
        $("#record_data").val("");
        setTimeout(function(){ $("#record_nome").focus(); }, 300);
        if (can_create) { $(".savebutton").show(); $(".dropzone").show(); $(".icon-trash-empty").show(); } else { $(".savebutton").hide();  $(".dropzone").hide(); $(".icon-trash-empty").hide(); }
        $('#modal_edit').modal({backdrop: 'static'});
    }

    function edit_record(id) {
        if (can_edit) { $('#modal_mode').html('Edit Contatti'); } else { $('#modal_mode').html('View Contatti'); }
        $.ajax({
            async: false,
            type: "POST",
            url: "controllers/contatti_controller.php",
            data: {
                "action": "read_record",
                "id": id
            },
            success: function (data, stato) {
                var data = $.parseJSON(data);
                if (data == "NOT AUTHORIZED") { return; }

                $("#record_id").val(data["record_id"]);
                $("#record_nome").val(data["record_nome"]);
                $("#record_azienda").val(data["record_azienda"]);
                $("#record_email").val(data["record_email"]);
                $("#record_telefono").val(data["record_telefono"]);
                $("#record_pagina").val(data["record_pagina"]);
                $("#record_data").val(data["record_data"]);
                setTimeout(function(){ $("#record_nome").focus(); }, 300);
                if (can_edit) { $(".savebutton").show(); $(".dropzone").show(); $(".icon-trash-empty").show(); } else { $(".savebutton").hide(); $(".dropzone").hide(); $(".icon-trash-empty").hide(); }
                $('#modal_edit').modal({backdrop: 'static'});
            }
        });
    }
    
    function save_record() {
        var requiredfields = "";
        $(".modal-body :required").each(function(){
            if (($(this).val() == "") || ($(this).val() === null)) {
                if (requiredfields != "") { requiredfields = requiredfields.concat(", "); }
                var tmp_req_field = $(this).attr("id").replace("record_", "").replace("_", " ");
                if (tmp_req_field.substr(0, 3) == "id ") { tmp_req_field = tmp_req_field.replace("id ", ""); }
                requiredfields = requiredfields.concat(tmp_req_field);
            }
        })
        if (requiredfields != "") { toast("Required fields: ".concat(requiredfields), "error"); return; }

        $.ajax({
            async: false,
            type: "POST",
            url: "controllers/contatti_controller.php",
            data: {
                "action": "save_record",
                "id": $("#record_id").val(), 
                "nome": $("#record_nome").val(), 
                "azienda": $("#record_azienda").val(), 
                "email": $("#record_email").val(), 
                "telefono": $("#record_telefono").val(), 
                "pagina": $("#record_pagina").val(), 
                "data": $("#record_data").val(), 

            },
            success: function (data, stato) {
                if (data != "OK") {
                    swal({
                        title: "Error",
                        text: "An error occurred during Save: " + data.toString(),
                        confirmButtonColor: "#66BB6A",
                        type: "warning"
                    });
                } else {
                    $('#modal_edit').modal('hide');
                    update_table();
                }
            }
        });
    }

    function update_table()  {
        table = $('#myDatatable').DataTable();
        table.destroy();
        $("#myDatatable > tbody").html("");
        load_table();
        table = $('#myDatatable').DataTable();
        table.page(0).draw(false);
    }

    $(document).ready(function () {

        $('.select').select2({ width: '100%' });

        load_table();
        table = $('#myDatatable').DataTable();
        table.on('draw', function () {
            table.rows().every(function (rowIdx, tableLoop, rowLoop) {
                var values_in_row = this.data();
                $.ajax({
                    async: false,
                    type: "POST",
                    url: "controllers/contatti_controller.php",
                    data: {
                        "action": "button_record",
                        "id": values_in_row[0]
                    },
                    success: function (data, stato) {
                        values_in_row[7] = data;
                    }
                });
                this.data(values_in_row);
            });
        });
    });


    function delete_record(id) {
        swal({
            title: "Sei sicuro?",
            text: "I dati non potranno essere recuperati",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: "Si, elimina!",
            cancelButtonText: "No, annulla!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    async: true,
                    type: "POST",
                    url : "controllers/contatti_controller.php",
                    data: {
                        "action": "delete_record",
                        "id": id
                    },
                    success: function (data) {
                        if (data === "OK") {
                            swal({
                                title: "Eliminato!",
                                text: "Il record è stato cancellato",
                                confirmButtonColor: "#66BB6A",
                                type: "success"
                            }, function () {
                                document.location.reload();
                            });
                        } else {
                            swal({
                                title: "Operazione annullata",
                                text: "Qualcosa è andato storto",
                                confirmButtonColor: "#2196F3",
                                type: "error"
                            });
                        }
                    },
                    error: function (data) {
                        swal({
                            title: "Operazione annullata",
                            text: "Qualcosa è andato storto",
                            confirmButtonColor: "#2196F3",
                            type: "error"
                        });
                    }
                });
            }
            else {
                swal({
                    title: "Operazione annullata",
                    text: "",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
    }


    function load_table() {

        filternome = "";
        if (parseInt($("#nome").val().length) > 0) { filternome = $("#nome").val(); }

        filterazienda = "";
        if (parseInt($("#azienda").val().length) > 0) { filterazienda = $("#azienda").val(); }

        filteremail = "";
        if (parseInt($("#email").val().length) > 0) { filteremail = $("#email").val(); }

        filtertelefono = "";
        if (parseInt($("#telefono").val().length) > 0) { filtertelefono = $("#telefono").val(); }

        filterpagina = "";
        if (parseInt($("#pagina").val().length) > 0) { filterpagina = $("#pagina").val(); }

        filterdata_from = "";
        if (parseInt($("#data_from").val().length) > 0) { filterdata_from = $("#data_from").val(); }
        filterdata_to = "";
        if (parseInt($("#data_to").val().length) > 0) { filterdata_to = $("#data_to").val(); }

        $('#myDatatable').DataTable( {
            'iDisplayLength': 25,
            'processing': true,
            'serverSide': true,
            'stateSave': true,
            'searching': false,
            'responsive': true,
            dom: '<"datatable-header"flB><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            buttons: [{
                        extend: 'pdf',
                        orientation: 'landscape',
                        text: '<i class="icon-file-pdf"></i> PDF',
                        title: '',
                        className: 'btn btn-outline-danger',
                        exportOptions: {columns: [1, 2, 3, 4, 5, 6]}
                    },
                    {
                        extend: 'excel',
                        text: '<i class="icon-file-excel"></i> Excel',
                        title: '',
                        className: 'btn btn-outline-success',
                        exportOptions: {columns: [1, 2, 3, 4, 5, 6]}
                    }
            ],
            language: {
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: {'first': '|&larr;', 'last': '&rarr;|', 'next': '&rarr;', 'previous': '&larr;'}
            },
            ajax: {
                url: 'controllers/contatti_controller.php',
                type: 'POST',
                data: {
                    action: "load_table",
                    filternome: filternome,
                    filterazienda: filterazienda,
                    filteremail: filteremail,
                    filtertelefono: filtertelefono,
                    filterpagina: filterpagina,
                    filterdata_from: filterdata_from,
                    filterdata_to:   filterdata_to,

                }
            },
            'columnDefs': [
                { 'targets': 0, 'visible': false },
                { "targets": 1,                           "width": "25%" },
                { "targets": 2,                           "width": "25%" },
                { "targets": 3,                           "width": "25%" },
                { "targets": 4,                           "width": "25%" },
                { "targets": 5,                           "width": "25%" },
                { "targets": 6,                           "width": "15%" },

                { "targets": 7, "className": "dt-center", "orderable": false, "width": "5%" },
            ]
        } );
    }

    function applyFilters() {
        table = $('#myDatatable').DataTable();
        table.destroy();
        $("#myDatatable > tbody").html("");
        load_table();
        table = $('#myDatatable').DataTable();
        table.page(0).draw(false);
    }

    function resetFilters() {
        $("#nome").val("");
        $("#azienda").val("");
        $("#email").val("");
        $("#telefono").val("");
        $("#pagina").val("");
        $("#data_from").val("");
        $("#data_to").val("");

        applyFilters();
    }

    
</script>