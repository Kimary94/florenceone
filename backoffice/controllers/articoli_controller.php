<?php

if (!isset($_SESSION)) { session_start(); }
include "sys_database.php";
include "../sys_functions.php";
include "sys_ssp.class.php";

// action: load_table         LOAD TABLE articolo
// action: button_record      RETURN BUTTON (to put inside datatable) FOR SINGLE RECORD ON articolo
// action: delete_record      DELETE RECORD ON articolo
// action: save_record        SAVE RECORD ON articolo

if(isset($_POST['action'])) {

    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'load_table') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(2, "read")) {echo json_encode("NOT AUTHORIZED"); die();}}
        $table = "articolo";
        $primaryKey = 'articolo.id';
        $columns = array(
        array( "db" => "articolo.id", "dt" => 0, "field" => "id" ), 
        array( "db" => "CONVERT(articolo.codice USING binary) as codice", "dt" => 1, "field" => "codice" ), 
        array( "db" => "CONVERT(articolo.nome USING binary) as nome", "dt" => 2, "field" => "nome" ), 
        array( "db" => "IF(articolo.72ore = 1, CONCAT(\"<i class='icon-check' onclick='func72h(\", articolo.id, \");'></i>\"), CONCAT(\"<i class='icon-check-empty' onclick='func72h(\", articolo.id, \");'></i>\")) as 72ore", "dt" => 3, "field" => "72ore" ), 
        array( "db" => "articolo.ordine as prezzo_attuale", "dt" => 4, "field" => "prezzo_attuale" ), 
        array( "db" => "if (articolo.prezzo_precedente = 0, '', articolo.prezzo_precedente) as prezzo_precedente", "dt" => 5, "field" => "prezzo_precedente" ), 
        array( "db" => "IF(articolo.online = 1, \"<i class='icon-check'></i>\", \"<i class='icon-check-empty'></i>\") as online", "dt" => 6, "field" => "online" ), 
        array( "db" => "emptycolumn", "dt" => 7, "field" => "" ), 
        );

        $sql_details = array(
            "user" => $db_username,
            "pass" => $db_password,
            "db"   => $db_database,
            "host" => $db_host,
        );

        $filtercodice = filter_input(INPUT_POST, "filtercodice", FILTER_SANITIZE_STRING);
        $filternome = filter_input(INPUT_POST, "filternome", FILTER_SANITIZE_STRING);
        $filteronline = filter_input(INPUT_POST, "filteronline", FILTER_SANITIZE_STRING);

        $where = " articolo.id >= 0 ";
        if ($filtercodice != "") { $where .= " and articolo.codice like '%" . $filtercodice . "%' "; $_SESSION["articolo_filter_codice"] = $filtercodice; } else { unset($_SESSION["articolo_filter_codice"]); } 
        if ($filternome != "") { $where .= " and articolo.nome like '%" . $filternome . "%' "; $_SESSION["articolo_filter_nome"] = $filternome; } else { unset($_SESSION["articolo_filter_nome"]); } 
        if ($filteronline == "1") { $where .= " and articolo.online = 1; "; $_SESSION["articolo_filter_online"] = $filteronline; } else { unset($_SESSION["articolo_filter_online"]); } 

        
        $fixedOrder = "";
        echo json_encode(
            SSP::complex( $_POST, $sql_details, $table, $primaryKey, $columns, $where, $fixedOrder )
        );
    }

    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'button_record') {
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $html_button = '<div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown" aria-expanded="false"><i class="icon-th-list-1"></i></a>
                                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(22px, 19px, 0px);">';
        
        $button_edit = true;
        $button_delete = true;
        if ($_SESSION["user_username"] != "sysadmin") {
            if (!checkUserRight(2, "edit")) { $button_edit = false; }
            if (!checkUserRight(2, "delete")) { $button_delete = false; }
        }
        if ($button_edit) { $html_button .= '<a href="articoli_edit.php?id='.$id.'" class="dropdown-item"><i class="icon-pencil"></i> Modifica</a>'; }        else { $html_button .= '<a href="articoli_edit.php?id='.$id.'" class="dropdown-item"><i class="icon-eye"></i> Visualizza</a>'; }
        if ($button_edit) { $html_button .= '<a href="articoli_prezzi.php?id='.$id.'" class="dropdown-item"><i class="icon-tag-1"></i> Prezzi articolo</a>'; }
        if ($button_edit) { $html_button .= '<a href="articoli_stampa.php?id='.$id.'" class="dropdown-item"><i class="icon-print-3"></i> Prezzi stampa</a>'; }
        if ($button_edit) { $html_button .= '<a href="articoli_extra.php?id='.$id.'"  class="dropdown-item"><i class="icon-money-2"></i> Prezzi extra</a>'; }

        // CHECK IF RECORD CAN BE DELETED
        $external_usage = 0;

        $query = $db->query("SELECT count(*) as conteggio FROM colore WHERE id_articolo = ".$id);
        while ($risultati = mysqli_fetch_array($query)) {
            if ($risultati["conteggio"] <> "") { if (intval($risultati["conteggio"]) > 0) { $external_usage += 1; }}
        }

        $query = $db->query("SELECT count(*) as conteggio FROM downloadtemplate WHERE id_articolo = ".$id);
        while ($risultati = mysqli_fetch_array($query)) {
            if ($risultati["conteggio"] <> "") { if (intval($risultati["conteggio"]) > 0) { $external_usage += 1; }}
        }

        $query = $db->query("SELECT count(*) as conteggio FROM prezzo WHERE id_articolo = ".$id);
        while ($risultati = mysqli_fetch_array($query)) {
            if ($risultati["conteggio"] <> "") { if (intval($risultati["conteggio"]) > 0) { $external_usage += 1; }}
        }

        $query = $db->query("SELECT count(*) as conteggio FROM prezzostampa WHERE id_articolo = ".$id);
        while ($risultati = mysqli_fetch_array($query)) {
            if ($risultati["conteggio"] <> "") { if (intval($risultati["conteggio"]) > 0) { $external_usage += 1; }}
        }

        $query = $db->query("SELECT count(*) as conteggio FROM qtaselect WHERE id_articolo = ".$id);
        while ($risultati = mysqli_fetch_array($query)) {
            if ($risultati["conteggio"] <> "") { if (intval($risultati["conteggio"]) > 0) { $external_usage += 1; }}
        }

        $query = $db->query("SELECT count(*) as conteggio FROM serviziaggiuntivi WHERE id_articolo = ".$id);
        while ($risultati = mysqli_fetch_array($query)) {
            if ($risultati["conteggio"] <> "") { if (intval($risultati["conteggio"]) > 0) { $external_usage += 1; }}
        }

        if ($button_delete) {
            if ($external_usage == 0) {
                $html_button .= '<a href="#" class="dropdown-item" onclick="delete_record('.filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING).');"><i class="icon-trash"></i> Elimina</a>';
            } else {
                $html_button .= '<a href="#" class="dropdown-item" style="color:#dddddd;"><i class="icon-trash"></i> Elimina</a>';
            }
        }

        $html_button .= '</div></div></div>';
        echo $html_button;
    }
    
    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'delete_record') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(2, "delete")) {echo json_encode("NOT AUTHORIZED"); die();}}
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $db->query("DELETE FROM articolo WHERE id = ".$id);
        echo "OK";
    }

    
    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'aggiorna_tabella_colori') {
        $id_articolo = filter_input(INPUT_POST, 'id_articolo', FILTER_SANITIZE_STRING);
        $query = $db->query("SELECT * FROM colore WHERE id_articolo = ".$id_articolo." ORDER BY ordine");
        $risp = "";
        while ($risultati = mysqli_fetch_array($query)) {
            $risp .= "<tr style='height: 38px;'>";
            $risp .= "<td><i class='icon-trash-8 mr-2' onclick='elimina_colore(".$risultati['id'].");'></i></td>";
            $risp .= "<td><i class='icon-pencil mr-2' onclick='modifica_colore(".$risultati['id'].");'></i></td>";
            $risp .= "<td><span style='width:20px; height:20px; display: inline-block; background-color: #".$risultati['rgb'].";'></span></td>";
            $risp .= "<td style='width:50px;'>";
            if (strlen($risultati['texture']) > 0) { $risp .= "<img onclick='colore_elimina_texture(".$risultati['id'].");' src='".$risultati['texture']."' style='margin-left: 8px; width:30px; height:20px;'>"; }
            $risp .= "</td>";
            $risp .= "<td style='width:50px;'>";
            if (strlen($risultati['immagine']) > 0) { $risp .= "<img onclick='colore_elimina_immagine(".$risultati['id'].");' src='".$risultati['immagine']."' style='height:20px;'>"; }
            $risp .= "</td>";
            $risp .= "<td>".$risultati['descrizione']."</td>";
            $risp .= "</tr>";
        }
        echo $risp;
    }


    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'salva_immagine') {
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $tipofile = filter_input(INPUT_POST, 'tipofile', FILTER_SANITIZE_STRING);
        $uploadsFolder = 'uploads';
        if (!empty($_FILES)) {
            $targetPath_pre = dirname( __FILE__ ) . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR;
            $targetPath_post = $uploadsFolder . DIRECTORY_SEPARATOR . date("Y") . DIRECTORY_SEPARATOR . date("m") . DIRECTORY_SEPARATOR;
            $targetPath = $targetPath_pre . $targetPath_post;
            if (!file_exists($targetPath)) {
                if (!mkdir($targetPath, 0777, true)) {
                    http_response_code (500);
                    header( 'Content-Type: application/json; charset=utf-8' );
                    echo json_encode("Failed to create folders structure: ".$targetPath_post);
                    die();
                }
            }
            $tempFile = $_FILES['file']['tmp_name'];
            $dot_position = strrpos($_FILES['file']['name'], ".");
            if ($dot_position === false) {
                $new_file_name =  $_FILES['file']['name'] . "_" . time() . "_"; 
            } else {
                $new_file_name =  substr($_FILES['file']['name'], 0, $dot_position) . "_" . time() . "_" . substr($_FILES['file']['name'], $dot_position);
            }
            $targetFile = $targetPath . $new_file_name;
            move_uploaded_file($tempFile, $targetFile);
            $file_full_path = $targetPath_post . $new_file_name;

            $data = [
                "s", $file_full_path,
                "i", $id,
            ];

            $sql = "UPDATE articolo SET ".$tipofile." = ? WHERE id = ?";
            $db->prepare_and_execute($sql, $data);

        }
        echo "OK";
    }



    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'salva_immagine_colore') {
        $colore_id = filter_input(INPUT_POST, 'colore_id', FILTER_SANITIZE_STRING);
        $tipofile = filter_input(INPUT_POST, 'tipofile', FILTER_SANITIZE_STRING);
        $uploadsFolder = 'uploads';
        if (!empty($_FILES)) {
            $targetPath_pre = dirname( __FILE__ ) . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR;
            $targetPath_post = $uploadsFolder . DIRECTORY_SEPARATOR . date("Y") . DIRECTORY_SEPARATOR . date("m") . DIRECTORY_SEPARATOR;
            $targetPath = $targetPath_pre . $targetPath_post;
            if (!file_exists($targetPath)) {
                if (!mkdir($targetPath, 0777, true)) {
                    http_response_code (500);
                    header( 'Content-Type: application/json; charset=utf-8' );
                    echo json_encode("Failed to create folders structure: ".$targetPath_post);
                    die();
                }
            }
            $tempFile = $_FILES['file']['tmp_name'];
            $dot_position = strrpos($_FILES['file']['name'], ".");
            if ($dot_position === false) {
                $new_file_name =  $_FILES['file']['name'] . "_" . time() . "_"; 
            } else {
                $new_file_name =  substr($_FILES['file']['name'], 0, $dot_position) . "_" . time() . "_" . substr($_FILES['file']['name'], $dot_position);
            }
            $targetFile = $targetPath . $new_file_name;
            move_uploaded_file($tempFile, $targetFile);
            $file_full_path = $targetPath_post . $new_file_name;

            $data = [
                "s", $file_full_path,
                "i", $colore_id,
            ];

            if ($tipofile == "texture") {
                $sql = "UPDATE colore SET texture = ? WHERE id = ?";
            } else {
                $sql = "UPDATE colore SET immagine = ? WHERE id = ?";
            }

            $db->prepare_and_execute($sql, $data);

        }
        echo "OK";
    }

    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'modifica_colore') {
        $colore_id = filter_input(INPUT_POST, 'colore_id', FILTER_SANITIZE_STRING);
        $query = $db->query("SELECT * FROM colore WHERE id = ".$colore_id);
        $risp = array();
        while ($risultati = mysqli_fetch_array($query)) {
            $risp = $risultati;
        }
        echo json_encode($risp);
    }

    

    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'elimina_colore') {
        $id_colore = filter_input(INPUT_POST, 'id_colore', FILTER_SANITIZE_STRING);
        $query = $db->query("DELETE FROM colore WHERE id = ".$id_colore);
        echo "OK";
    }

    
    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'colore_elimina_texture') {
        $id_colore = filter_input(INPUT_POST, 'id_colore', FILTER_SANITIZE_STRING);
        $query = $db->query("UPDATE colore SET texture = '' WHERE id = ".$id_colore);
        echo "OK";
    }

    
    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'colore_elimina_immagine') {
        $id_colore = filter_input(INPUT_POST, 'id_colore', FILTER_SANITIZE_STRING);
        $query = $db->query("UPDATE colore SET immagine = '' WHERE id = ".$id_colore);
        echo "OK";
    }


    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == '72h') {
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $query = $db->query("SELECT 72ore FROM articolo WHERE id = ".$id);
        while ($risultati = mysqli_fetch_array($query)) {
            $value72ore = intval($risultati["72ore"]);
        }
        if ($value72ore == 0) {
            $value72ore = 1;
        } else {
            $value72ore = 0;
        }
        $db->query("UPDATE articolo SET 72ore = ".$value72ore." WHERE id = ".$id);
        echo "OK";
    }

    

    

    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'salva_colore') {
        $id_articolo = filter_input(INPUT_POST, 'id_articolo', FILTER_SANITIZE_STRING);
        $colore_id = filter_input(INPUT_POST, 'colore_id', FILTER_SANITIZE_STRING);
        $colore_descrizione = filter_input(INPUT_POST, 'colore_descrizione', FILTER_SANITIZE_STRING);
        $colore_rgb = filter_input(INPUT_POST, 'colore_rgb', FILTER_SANITIZE_STRING);
        if ($colore_id == "") {
            $data = [
                "i", $id_articolo,
                "s", $colore_descrizione,
                "s", $colore_rgb,
            ];
            $sql = "INSERT INTO colore (id_articolo, descrizione, rgb) VALUES (?, ?, ?)";
            $retv = $db->prepare_and_execute($sql, $data);
            if ($retv == 1) {
                $query = $db->query("SELECT id FROM colore ORDER BY id DESC LIMIT 1");
                while ($risultati = mysqli_fetch_array($query)) {
                    $colore_id = $risultati["id"];
                }
                echo "OK".$colore_id;
            } else {
                echo "Errore: ".$retv;
            }
        } else {
            $data = [
                "s", $colore_descrizione,
                "s", $colore_rgb,
                "i", $colore_id,
            ];
            $sql = "UPDATE colore SET descrizione=?, rgb=? WHERE id=?";
            $retv = $db->prepare_and_execute($sql, $data);
            if ($retv == 1) {
                echo "OK".$colore_id;
            } else {
                echo "Errore: ".$retv;
            }
        }
    }

    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'save_record') {
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $input_codice = filter_input(INPUT_POST, "codice", FILTER_SANITIZE_STRING);
        $input_nome = filter_input(INPUT_POST, "nome", FILTER_SANITIZE_STRING);
        $input_descrizione = filter_input(INPUT_POST, "descrizione", FILTER_SANITIZE_STRING);
        $input_qtainput = filter_input(INPUT_POST, "qtainput", FILTER_SANITIZE_STRING);
        $input_qtaselect = filter_input(INPUT_POST, "qtaselect", FILTER_SANITIZE_STRING);
        $input_qtamin = filter_input(INPUT_POST, "qtamin", FILTER_SANITIZE_STRING);
        if ($input_qtamin == "") { $input_qtamin = null; }
        $input_qtamax = filter_input(INPUT_POST, "qtamax", FILTER_SANITIZE_STRING);
        if ($input_qtamax == "") { $input_qtamax = null; }
        $input_coloreselect = filter_input(INPUT_POST, "coloreselect", FILTER_SANITIZE_STRING);
        $input_grigliacoloretaglie = filter_input(INPUT_POST, "grigliacoloretaglie", FILTER_SANITIZE_STRING);
        $input_id_articolo_equivalente1 = filter_input(INPUT_POST, "id_articolo_equivalente1", FILTER_SANITIZE_STRING);
        if ($input_id_articolo_equivalente1 == 0) { $input_id_articolo_equivalente1 = null; }
        if ($input_id_articolo_equivalente1 == "") { $input_id_articolo_equivalente1 = null; }
        $input_id_articolo_equivalente2 = filter_input(INPUT_POST, "id_articolo_equivalente2", FILTER_SANITIZE_STRING);
        if ($input_id_articolo_equivalente2 == 0) { $input_id_articolo_equivalente2 = null; }
        if ($input_id_articolo_equivalente2 == "") { $input_id_articolo_equivalente2 = null; }
        $input_online = filter_input(INPUT_POST, "online", FILTER_SANITIZE_STRING);

        
        // (Types: "s" = string, "i" = integer, "d" = double, "b" = blob)
        if ($id == "") {
            $data = [
                "s", $input_codice,
                "s", $input_nome,
                "s", $input_descrizione,
                "i", $input_qtainput,
                "i", $input_qtaselect,
                "i", $input_qtamin,
                "i", $input_qtamax,
                "i", $input_coloreselect,
                "i", $input_grigliacoloretaglie,
                "i", $input_id_articolo_equivalente1,
                "i", $input_id_articolo_equivalente2,
                "i", $input_online,
            ];

            $sql = "INSERT INTO articolo (codice, nome, descrizione, qtainput, qtaselect, qtamin, qtamax, coloreselect, grigliacoloretaglie, id_articolo_equivalente1, id_articolo_equivalente2, online) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            $retv = $db->prepare_and_execute($sql, $data);

            $query = $db->query("SELECT id FROM articolo ORDER BY id DESC LIMIT 1");
            while ($risultati = mysqli_fetch_array($query)) {
                $id = $risultati["id"];
            }

            $data = [
                "s", "Piega",
                "s", "Piega e imbustamento",
                "d", 0,
                "i", 1,
                "i", $id
            ];
            $sql = "INSERT INTO serviziaggiuntivi (titolo, descrizione, prezzounitario, ordine, id_articolo) VALUES (?, ?, ?, ?, ?)";
            $retv = $db->prepare_and_execute($sql, $data);

        } else {
            $immagine1 = filter_input(INPUT_POST, "immagine1", FILTER_SANITIZE_STRING);
            $immagine2 = filter_input(INPUT_POST, "immagine2", FILTER_SANITIZE_STRING);
            $id_articolo = $id;

            $data = [
                "s", $input_codice,
                "s", $input_nome,
                "s", $input_descrizione,
                "i", $input_qtainput,
                "i", $input_qtaselect,
                "i", $input_qtamin,
                "i", $input_qtamax,
                "i", $input_coloreselect,
                "i", $input_grigliacoloretaglie,
                "i", $input_id_articolo_equivalente1,
                "i", $input_id_articolo_equivalente2,
                "i", $input_online,
                "s", $immagine1,
                "s", $immagine2,
                "i", $id_articolo,
            ];

            $sql = "UPDATE articolo SET codice=?, nome=?, descrizione=?, qtainput=?, qtaselect=?, qtamin=?, qtamax=?, coloreselect=?, grigliacoloretaglie=?, id_articolo_equivalente1=?, id_articolo_equivalente2=?, online=?, immagine1=?, immagine2=? WHERE id=?";
            $retv = $db->prepare_and_execute($sql, $data);

            if (isset($_POST['categorie'])) {
                $post = $_POST['categorie'];
                // CICLO SU TUTTI I VALORI DI CATEGORIA
                for ($i = 0; $i <= count($post)-1; $i++) {
                    $id_categoria = $post[$i][0];
                    $id_row_categoria = $post[$i][1];
                    // VERIFICO SE PRESENTE UNITA DI MISURA SULLA CATEGORIA
                    $um = "";
                    $query = $db->query("SELECT um FROM categoria WHERE id = ".$id_categoria);
                    while ($risultati = mysqli_fetch_array($query)) {
                        $um = $risultati['um'];
                    }
                    if ($id_row_categoria == "") {
                        // elimina eventuale record già presente
                        $sql = "DELETE FROM categoriaxarticolo WHERE id_categoria = ".$id_categoria." AND id_articolo=".$id_articolo;
                        $db->query($sql);
                    } else {
                        // inserisci o aggiorna eventuale record
                        $categoria_presente = 0;
                        $query = $db->query("SELECT id FROM categoriaxarticolo WHERE id_categoria = ".$id_categoria." AND id_articolo=".$id_articolo);
                        while ($risultati = mysqli_fetch_array($query)) {
                            $categoria_presente = 1;
                        }
                        if ($categoria_presente == 0) {
                            // INSERT
                            if (strlen($um) > 0) {
                                $sql = "INSERT INTO categoriaxarticolo (id_articolo, id_categoria, valore) VALUES (".$id_articolo.", ".$id_categoria.", ".$id_row_categoria.")";
                            } else {
                                $sql = "INSERT INTO categoriaxarticolo (id_articolo, id_categoria, id_row_categoria) VALUES (".$id_articolo.", ".$id_categoria.", ".$id_row_categoria.")";
                            }
                            $db->query($sql);
                        } else {
                            // UPDATE
                            if (strlen($um) > 0) {
                                $sql = "UPDATE categoriaxarticolo SET valore = ".$id_row_categoria." WHERE id_articolo = ".$id_articolo." AND id_categoria = ".$id_categoria;
                            } else {
                                $sql = "UPDATE categoriaxarticolo SET id_row_categoria = ".$id_row_categoria." WHERE id_articolo = ".$id_articolo." AND id_categoria = ".$id_categoria;
                            }
                            $db->query($sql);
                        }
                    }
                }
            }

            if (isset($_POST['taglie'])) {
                $post = $_POST['taglie'];
                // CICLO SU TUTTI I VALORI DI CATEGORIA
                for ($i = 0; $i <= count($post)-1; $i++) {
                    $id_dimensioni = $post[$i][0];
                    $taglia = $post[$i][1];
                    $larghezza = $post[$i][2];
                    $altezza = $post[$i][3];
                    if ($taglia == "") {
                        // elimina record già presente
                        if ($id_dimensioni != "") {
                            $sql = "DELETE FROM dimensioni WHERE id = ".$id_dimensioni;
                            $db->query($sql);
                        }
                    } else {
                        if ($id_dimensioni == "") {
                            // INSERT
                            if ($larghezza == "") { $larghezza = "0"; }
                            if ($altezza == "") { $altezza = "0"; }
                            $sql = "INSERT INTO dimensioni (taglia, id_articolo, larghezza, altezza) VALUES ('".$taglia."', ".$id_articolo.", ".$larghezza.", ".$altezza.")";
                            $db->query($sql);
                        } else {
                            // UPDATE
                            if ($larghezza == "") { $larghezza = "0"; }
                            if ($altezza == "") { $altezza = "0"; }
                            $sql = "UPDATE dimensioni SET taglia = '".$taglia."', larghezza = ".$larghezza.", altezza = ".$altezza." WHERE id = ".$id_dimensioni;
                            $db->query($sql);
                        }
                    }
                }
            } else {
                $sql = "DELETE FROM dimensioni WHERE id_articolo = ".$id_articolo;
                $db->query($sql);
            }


            if (isset($_POST['qta'])) {
                $db->query("DELETE FROM qtaselect WHERE id_articolo = ".$id_articolo);
                $post = $_POST['qta'];
                for ($i = 0; $i <= count($post)-1; $i++) {
                    $qta = $post[$i];
                    $sql = "INSERT INTO qtaselect (id_articolo, quantita) VALUES (".$id_articolo.", ".$qta.")";
                    $db->query($sql);
                }
            }

            
            if (isset($_POST['servizi'])) {
                $db->query("DELETE FROM servizioxarticolo WHERE id_articolo = ".$id_articolo);
                $post = $_POST['servizi'];
                for ($i = 0; $i <= count($post)-1; $i++) {
                    $id_servizio = $post[$i];
                    $sql = "INSERT INTO servizioxarticolo (id_articolo, id_servizio) VALUES (".$id_articolo.", ".$id_servizio.")";
                    $db->query($sql);
                }
            }


            
            if (isset($_POST['dimensioni_stampa'])) {
                $post = $_POST['dimensioni_stampa'];
                // CICLO SU TUTTI I VALORI DI CATEGORIA
                for ($i = 0; $i <= count($post)-1; $i++) {
                    $id_lato = $post[$i][0];
                    $descrizione = $post[$i][1];
                    // elimina eventuale record già presente
                    $sql = "DELETE FROM dimensione_stampa WHERE id_lato = ".$id_lato." AND id_articolo=".$id_articolo;
                    $db->query($sql);
                    // inserisci il record se dimensione popolata
                    if (strlen($descrizione) > 0) {
                        $data = [
                            "i", $id_articolo,
                            "i", $id_lato,
                            "s", $descrizione,
                        ];
                        $sql = "INSERT INTO dimensione_stampa (id_articolo, id_lato, descrizione) VALUES (?, ?, ?)";
                        $retv = $db->prepare_and_execute($sql, $data);
                    }
                }
            }

        }
        if ($retv == 1) {
            echo "OK".$id;
        } else {
            echo "Error: ".$retv;
        }
    }
    
}