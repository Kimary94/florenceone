<?php
if (!isset($_SESSION)) { session_start(); }
include "sys_database.php";
include "../sys_functions.php";
include "sys_ssp.class.php";
if ($_SESSION["user_username"] != "sysadmin") {echo json_encode("NOT AUTHORIZED"); die();}

if(isset($_POST['action'])) {

    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'salva') {
        $prezziextra = $_POST['prezziextra'];
        $id_articolo = filter_input(INPUT_POST, 'id_articolo', FILTER_SANITIZE_STRING);
        $query = $db->query("DELETE FROM serviziaggiuntivi WHERE id_articolo = ".$id_articolo);

        for ($i = 0; $i <= count($prezziextra)-1; $i++) {
            $data = [
                "s", $prezziextra[$i][0],
                "s", $prezziextra[$i][1],
                "d", $prezziextra[$i][2],
                "i", $prezziextra[$i][3],
                "i", $id_articolo
            ];
            $sql = "INSERT INTO serviziaggiuntivi (titolo, descrizione, prezzounitario, ordine, id_articolo) VALUES (?, ?, ?, ?, ?)";
            $retv = $db->prepare_and_execute($sql, $data);
        }
        echo "OK";
    }

}
?>
