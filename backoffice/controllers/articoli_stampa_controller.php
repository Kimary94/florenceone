<?php
if (!isset($_SESSION)) { session_start(); }
include "sys_database.php";
include "../sys_functions.php";
include "sys_ssp.class.php";
if ($_SESSION["user_username"] != "sysadmin") {echo json_encode("NOT AUTHORIZED"); die();}

if(isset($_POST['action'])) {
    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'salva') {
        $prezzi = $_POST['scaglioni'];
        $id_articolo = filter_input(INPUT_POST, 'id_articolo', FILTER_SANITIZE_STRING);
        $query = $db->query("DELETE FROM prezzostampa WHERE id_articolo = ".$id_articolo);
        for ($i = 0; $i <= count($prezzi)-1; $i++) {
            $data = [
                "i", $prezzi[$i][0],
                "i", $prezzi[$i][1],
                "i", $prezzi[$i][2],
                "i", $prezzi[$i][3],
                "d", $prezzi[$i][4],
            ];
            $sql = "INSERT INTO prezzostampa (id_articolo, id_lato, id_tipostampa, scaglione, prezzounitario) VALUES (?, ?, ?, ?, ?)";
            $retv = $db->prepare_and_execute($sql, $data);
        }



        
        $prezzounitario = 0;
        $scaglione = "";
        $prezzostampa = 0;
        $tipostampa = "";
        $descrizione_ordine = "";


        // SE PENNE O MATITE PRENDERE SCAGLIONE 500 COME PRIMO TENTATIVO
        $id_row_categoria = 0;
        $query = $db->query("select id_row_categoria from categoriaxarticolo where id_categoria = 1 and id_articolo = ".$id_articolo);
        while ($risultati = mysqli_fetch_array($query)) { $id_row_categoria = $risultati["id_row_categoria"]; }
        if (($id_row_categoria == 66) || ($id_row_categoria == 75)) {
            $query = $db->query("SELECT prezzounitario FROM prezzo WHERE id_articolo = ".$id_articolo." AND scaglione = 500 ORDER BY prezzounitario LIMIT 1");
            while ($risultati = mysqli_fetch_array($query)) { $prezzounitario = $risultati["prezzounitario"]; $scaglione = "500"; }            
        }

        if ($prezzounitario == 0) {
            $query = $db->query("SELECT prezzounitario FROM prezzo WHERE id_articolo = ".$id_articolo." AND scaglione = 100 ORDER BY prezzounitario LIMIT 1");
            while ($risultati = mysqli_fetch_array($query)) { $prezzounitario = $risultati["prezzounitario"]; $scaglione = "100"; }
        }
        if ($prezzounitario == 0) {
            $query = $db->query("SELECT prezzounitario FROM prezzo WHERE id_articolo = ".$id_articolo." AND scaglione = 50 ORDER BY prezzounitario LIMIT 1");
            while ($risultati = mysqli_fetch_array($query)) { $prezzounitario = $risultati["prezzounitario"]; $scaglione = "50"; }
        }
        if ($prezzounitario == 0) {
            $query = $db->query("SELECT prezzounitario FROM prezzo WHERE id_articolo = ".$id_articolo." AND scaglione = 200 ORDER BY prezzounitario LIMIT 1");
            while ($risultati = mysqli_fetch_array($query)) { $prezzounitario = $risultati["prezzounitario"]; $scaglione = "200"; }
        }



        if ($scaglione != "") {
            $descrizione_ordine = "(".$scaglione . " pezzi";

            
            // SE PENNE O MATITE PRENDERE IL PREZZO DI STAMPA PIU' BASSO CHE TROVA
            if (($id_row_categoria == 66) || ($id_row_categoria == 75)) {
                $query = $db->query("SELECT prezzostampa.prezzounitario, tipostampa.descrizione as tipostampa FROM prezzostampa LEFT JOIN tipostampa ON tipostampa.id = prezzostampa.id_tipostampa WHERE prezzostampa.id_articolo = ".$id_articolo." AND prezzostampa.scaglione = ".$scaglione." ORDER BY prezzostampa.prezzounitario, prezzostampa.id_tipostampa LIMIT 1");
                while ($risultati = mysqli_fetch_array($query)) { $prezzostampa = $risultati["prezzounitario"]; $tipostampa = " / ".$risultati["tipostampa"]; }
            }

            if ($prezzostampa == 0) {
                $query = $db->query("SELECT prezzounitario FROM prezzostampa WHERE id_tipostampa = 1 AND id_articolo = ".$id_articolo." AND scaglione = ".$scaglione." ORDER BY prezzounitario LIMIT 1");
                while ($risultati = mysqli_fetch_array($query)) { $prezzostampa = $risultati["prezzounitario"]; $tipostampa = " / stampa 1 colore"; }
            }
            if ($prezzostampa == 0) {
                $query = $db->query("SELECT prezzounitario FROM prezzostampa WHERE id_tipostampa = 10 AND id_articolo = ".$id_articolo." AND scaglione = ".$scaglione." ORDER BY prezzounitario LIMIT 1");
                while ($risultati = mysqli_fetch_array($query)) { $prezzostampa = $risultati["prezzounitario"]; $tipostampa = " / stampa digitale"; }
            }
            if ($prezzostampa == 0) {
                $query = $db->query("SELECT prezzounitario FROM prezzostampa WHERE id_articolo = ".$id_articolo." AND scaglione = ".$scaglione." ORDER BY prezzounitario LIMIT 1");
                while ($risultati = mysqli_fetch_array($query)) { $prezzostampa = $risultati["prezzounitario"]; $tipostampa = " stampati"; }
            }
            if ($prezzostampa > 0) {
                $descrizione_ordine .= $tipostampa.")";
            } else {
                $descrizione_ordine .= ")";
            }
        }
        $prezzounitario = $prezzounitario + $prezzostampa;

        $data = [
            "d", $prezzounitario,
            "s", $descrizione_ordine,
            "i", $id_articolo
        ];
        $sql = "UPDATE articolo SET ordine = ?, descrizione_ordine = ? WHERE id = ?";
        $retv = $db->prepare_and_execute($sql, $data);




        echo "OK";
    }
}
?>
