<?php

if (!isset($_SESSION)) { session_start(); }
include "sys_database.php";
include "../sys_functions.php";
include "sys_ssp.class.php";

// action: read_record        LOAD SINGLE RECORD FROM blog FOR MODAL EDIT
// action: load_table         LOAD TABLE blog
// action: button_record      RETURN BUTTON (to put inside datatable) FOR SINGLE RECORD ON blog
// action: delete_record      DELETE RECORD ON blog
// action: save_record        SAVE RECORD ON blog


if(isset($_POST['action'])) {

    
    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'read_record') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(5, "read")) {echo json_encode("NOT AUTHORIZED"); die();}}
        $id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT);
        $risultato = array();
        $qry = "SELECT * FROM blog WHERE blog.id = ".$id;
        $query = $db->query($qry);
        while ($risultati = mysqli_fetch_array($query))
        {
            $risultato["record_id"] = $risultati["id"];
            $risultato["record_titolo"] = $risultati["titolo"];
            $risultato["record_testo"] = $risultati["testo"];
            $risultato["record_testo_breve"] = $risultati["testo_breve"];
            $risultato["record_immagini"] = $risultati["immagini"];

        }
        echo json_encode($risultato);
    }


    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'load_table') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(5, "read")) {echo json_encode("NOT AUTHORIZED"); die();}}
        $table = "blog";
        $primaryKey = 'blog.id';
        $columns = array(
        array( "db" => "blog.id", "dt" => 0, "field" => "id" ), 
        array( "db" => "blog.titolo", "dt" => 1, "field" => "titolo" ), 
        array( "db" => "blog.testo", "dt" => 2, "field" => "testo" ), 
        array( "db" => "blog.testo_breve", "dt" => 3, "field" => "testo_breve" ), 
        array( "db" => "emptycolumn", "dt" => 4, "field" => "" ), 
        );

        $sql_details = array(
            "user" => $db_username,
            "pass" => $db_password,
            "db"   => $db_database,
            "host" => $db_host,
        );
        $filtertitolo = filter_input(INPUT_POST, "filtertitolo", FILTER_SANITIZE_STRING);
        $filtertesto = filter_input(INPUT_POST, "filtertesto", FILTER_SANITIZE_STRING);
        $filtertesto_breve = filter_input(INPUT_POST, "filtertesto_breve", FILTER_SANITIZE_STRING);
        $filterimmagini = filter_input(INPUT_POST, "filterimmagini", FILTER_SANITIZE_STRING);

        $where = " blog.id >= 0 ";
        if ($filtertitolo != "") { $where .= " and blog.titolo like '%" . $filtertitolo . "%' "; $_SESSION["blog_filter_titolo"] = $filtertitolo; } else { unset($_SESSION["blog_filter_titolo"]); } 
        if ($filtertesto != "") { $where .= " and blog.testo like '%" . $filtertesto . "%' "; $_SESSION["blog_filter_testo"] = $filtertesto; } else { unset($_SESSION["blog_filter_testo"]); } 
        if ($filtertesto_breve != "") { $where .= " and blog.testo_breve like '%" . $filtertesto_breve . "%' "; $_SESSION["blog_filter_testo_breve"] = $filtertesto_breve; } else { unset($_SESSION["blog_filter_testo_breve"]); } 
        if ($filterimmagini != "") { $where .= " and blog.immagini like '%" . $filterimmagini . "%' "; $_SESSION["blog_filter_immagini"] = $filterimmagini; } else { unset($_SESSION["blog_filter_immagini"]); } 

        $fixedOrder = "";
        echo json_encode(
            SSP::complex( $_POST, $sql_details, $table, $primaryKey, $columns, $where, $fixedOrder )
        );
    }


    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'button_record') {
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $html_button = '<div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown" aria-expanded="false"><i class="icon-th-list-1"></i></a>
                                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(22px, 19px, 0px);">';
        
        $button_edit = true;
        $button_delete = true;
        if ($_SESSION["user_username"] != "sysadmin") {
            if (!checkUserRight(5, "edit")) { $button_edit = false; }
            if (!checkUserRight(5, "delete")) { $button_delete = false; }
        }
        if ($button_edit) { $html_button .= '<a href="#" onclick="edit_record('.$id.');" class="dropdown-item"><i class="icon-pencil"></i> Edit</a>'; }        else { $html_button .= '<a href="#" onclick="edit_record('.$id.');" class="dropdown-item"><i class="icon-eye"></i> View</a>'; }

        // CHECK IF RECORD CAN BE DELETED
        $external_usage = 0;

        if ($button_delete) {
            if ($external_usage == 0) {
                $html_button .= '<a href="#" class="dropdown-item" onclick="delete_record('.filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING).');"><i class="icon-trash"></i> Delete</a>';
            } else {
                $html_button .= '<a href="#" class="dropdown-item" style="color:#dddddd;"><i class="icon-trash"></i> Delete</a>';
            }
        }

        $html_button .= '</div></div></div>';
        echo $html_button;
    }


    
    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'delete_record') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(5, "delete")) {echo json_encode("NOT AUTHORIZED"); die();}}
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);

        $db->query("DELETE FROM blog WHERE id = ".$id);
        echo "OK";
    }

    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'save_record') {
        
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $input_titolo = filter_input(INPUT_POST, "titolo", FILTER_SANITIZE_STRING);
        $input_testo = filter_input(INPUT_POST, "testo", FILTER_SANITIZE_STRING);
        $input_testo_breve = filter_input(INPUT_POST, "testo_breve", FILTER_SANITIZE_STRING);
        $input_FILELIST_immagini = html_entity_decode(filter_input(INPUT_POST, "immagini", FILTER_SANITIZE_STRING));
        $data = [
            "s", $input_titolo,
            "s", $input_testo,
            "s", $input_testo_breve,
            "s", $input_FILELIST_immagini,
        ];

        // (Types: "s" = string, "i" = integer, "d" = double, "b" = blob)
        if ($id == "") {
            if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(5, "create")) {echo json_encode("NOT AUTHORIZED"); die();}}
            $sql = "INSERT INTO blog (titolo, testo, testo_breve, immagini) VALUES (?, ?, ?, ?)";
            $retv = $db->prepare_and_execute($sql, $data);
        } else {
            if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(5, "edit")) {echo json_encode("NOT AUTHORIZED"); die();}}
            array_push($data, "i", $id);
            $sql = "UPDATE blog SET titolo=?, testo=?, testo_breve=?, immagini=? WHERE id=?";
            $retv = $db->prepare_and_execute($sql, $data);
        }
        if ($retv == 1) {

            echo "OK";
        } else {
            echo "Error: ".$retv;
        }
    }
    
    if (filter_input(INPUT_POST, "action", FILTER_SANITIZE_STRING) == "save_record_row") {
        $id = filter_input(INPUT_POST, "id", FILTER_SANITIZE_STRING);
        $data = [
        ];

        // (Types: "s" = string, "i" = integer, "d" = double, "b" = blob)
        if ($id == "") {
            if ($_SESSION["user_username"] != "sysadmin") {
                if ((!checkUserRight(5, "create")) && (!checkUserRight(5, "edit"))) {
                    echo json_encode("NOT AUTHORIZED");
                    die();
                }
            }
            $sql = "INSERT INTO ) VALUES)";
            $retv = $db->prepare_and_execute($sql, $data);
        } else {
            if ($_SESSION["user_username"] != "sysadmin") {
                if ((!checkUserRight(5, "create")) && (!checkUserRight(5, "edit"))) {
                    echo json_encode("NOT AUTHORIZED");
                    die();
                }
            }
            array_push($data, "i", $id);
            $sql = "UPDATE  SE WHERE id=?";
            $retv = $db->prepare_and_execute($sql, $data);
        }
        if ($retv == 1) {
            echo "OK";
        } else {
            echo "Error: ".$retv;
        }
    }
}