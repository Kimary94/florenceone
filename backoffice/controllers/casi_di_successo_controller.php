<?php

if (!isset($_SESSION)) { session_start(); }
include "sys_database.php";
include "../sys_functions.php";
include "sys_ssp.class.php";

// action: read_record        LOAD SINGLE RECORD FROM casi_di_successo FOR MODAL EDIT
// action: load_table         LOAD TABLE casi_di_successo
// action: button_record      RETURN BUTTON (to put inside datatable) FOR SINGLE RECORD ON casi_di_successo
// action: delete_record      DELETE RECORD ON casi_di_successo
// action: save_record        SAVE RECORD ON casi_di_successo


if(isset($_POST['action'])) {

    
    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'read_record') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(9, "read")) {echo json_encode("NOT AUTHORIZED"); die();}}
        $id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT);
        $risultato = array();
        $qry = "SELECT * FROM casi_di_successo WHERE casi_di_successo.id = ".$id;
        $query = $db->query($qry);
        while ($risultati = mysqli_fetch_array($query))
        {
            $risultato["record_id"] = $risultati["id"];
            $risultato["record_titolo"] = $risultati["titolo"];
            $risultato["record_sottotitolo"] = $risultati["sottotitolo"];
            $risultato["record_cliente"] = $risultati["cliente"];
            $risultato["record_urllogo"] = $risultati["urllogo"];
            $risultato["record_data"] = $risultati["data"];
            $risultato["record_sito"] = $risultati["sito"];
            $risultato["record_foto"] = $risultati["foto"];
            $risultato["record_storia"] = $risultati["storia"];
            $risultato["record_sfida"] = $risultati["sfida"];
            $risultato["record_soluzione"] = $risultati["soluzione"];
            $risultato["record_extra"] = $risultati["extra"];
            $risultato["record_id_settore"] = $risultati["id_settore"];
            $risultato["record_id_regione"] = $risultati["id_regione"];
            $risultato["record_id_moduli"] = $risultati["id_moduli"];
            $risultato["record_id_moduli1"] = $risultati["id_moduli1"];
            $risultato["record_id_moduli2"] = $risultati["id_moduli2"];
            $risultato["record_id_moduli3"] = $risultati["id_moduli3"];
            $risultato["record_id_moduli4"] = $risultati["id_moduli4"];

        }
        echo json_encode($risultato);
    }


    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'load_table') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(9, "read")) {echo json_encode("NOT AUTHORIZED"); die();}}
        $table = "casi_di_successo                 LEFT JOIN settore ON settore.id = casi_di_successo.id_settore                 LEFT JOIN regione ON regione.id = casi_di_successo.id_regione                 LEFT JOIN moduli ON moduli.id = casi_di_successo.id_moduli                 LEFT JOIN moduli as m1 ON m1.id = casi_di_successo.id_moduli1                 LEFT JOIN moduli as m2 ON m2.id = casi_di_successo.id_moduli2                 LEFT JOIN moduli as m3 ON m3.id = casi_di_successo.id_moduli3                 LEFT JOIN moduli as m4 ON m4.id = casi_di_successo.id_moduli4";
        $primaryKey = 'casi_di_successo.id';
        $columns = array(
        array( "db" => "casi_di_successo.id", "dt" => 0, "field" => "id" ), 
        array( "db" => "casi_di_successo.titolo", "dt" => 1, "field" => "titolo" ), 
        array( "db" => "casi_di_successo.sottotitolo", "dt" => 2, "field" => "sottotitolo" ), 
        array( "db" => "casi_di_successo.cliente", "dt" => 3, "field" => "cliente" ), 
        array( "db" => "DATE_FORMAT(casi_di_successo.data, '%Y-%m-%d') as data", "dt" => 4, "field" => "data" ), 
        array( "db" => "emptycolumn", "dt" => 5, "field" => "" ), 
        );

        $sql_details = array(
            "user" => $db_username,
            "pass" => $db_password,
            "db"   => $db_database,
            "host" => $db_host,
        );
        $filtertitolo = filter_input(INPUT_POST, "filtertitolo", FILTER_SANITIZE_STRING);
        $filtersottotitolo = filter_input(INPUT_POST, "filtersottotitolo", FILTER_SANITIZE_STRING);
        $filtercliente = filter_input(INPUT_POST, "filtercliente", FILTER_SANITIZE_STRING);
        $filterurllogo = filter_input(INPUT_POST, "filterurllogo", FILTER_SANITIZE_STRING);
        $filterdata_from = filter_input(INPUT_POST, "filterdata_from", FILTER_SANITIZE_STRING);
        $filterdata_to = filter_input(INPUT_POST, "filterdata_to", FILTER_SANITIZE_STRING);
        $filtersito = filter_input(INPUT_POST, "filtersito", FILTER_SANITIZE_STRING);
        $filterfoto = filter_input(INPUT_POST, "filterfoto", FILTER_SANITIZE_STRING);
        $filterstoria = filter_input(INPUT_POST, "filterstoria", FILTER_SANITIZE_STRING);
        $filtersfida = filter_input(INPUT_POST, "filtersfida", FILTER_SANITIZE_STRING);
        $filtersoluzione = filter_input(INPUT_POST, "filtersoluzione", FILTER_SANITIZE_STRING);
        $filterextra = filter_input(INPUT_POST, "filterextra", FILTER_SANITIZE_STRING);
        $filterid_settore = filter_input(INPUT_POST, "filterid_settore", FILTER_SANITIZE_STRING);
        $filterid_regione = filter_input(INPUT_POST, "filterid_regione", FILTER_SANITIZE_STRING);
        if ($filterid_moduli  != "") {$where .= " and (casi_di_successo.id_moduli  = " . $filterid_moduli . " OR casi_di_successo.id_moduli1 = " . $filterid_moduli . " OR casi_di_successo.id_moduli2 = " . $filterid_moduli . " OR casi_di_successo.id_moduli3 = " . $filterid_moduli . " OR casi_di_successo.id_moduli4 = " . $filterid_moduli . " ) ";$_SESSION["casi_di_successo_filter_id_moduli"] = $filterid_moduli;} else {unset($_SESSION["casi_di_successo_filter_id_moduli"]);        } 

        $where = " casi_di_successo.id >= 0 ";
        if ($filtertitolo != "") { $where .= " and casi_di_successo.titolo like '%" . $filtertitolo . "%' "; $_SESSION["casi_di_successo_filter_titolo"] = $filtertitolo; } else { unset($_SESSION["casi_di_successo_filter_titolo"]); } 
        if ($filtersottotitolo != "") { $where .= " and casi_di_successo.sottotitolo like '%" . $filtersottotitolo . "%' "; $_SESSION["casi_di_successo_filter_sottotitolo"] = $filtersottotitolo; } else { unset($_SESSION["casi_di_successo_filter_sottotitolo"]); } 
        if ($filtercliente != "") { $where .= " and casi_di_successo.cliente like '%" . $filtercliente . "%' "; $_SESSION["casi_di_successo_filter_cliente"] = $filtercliente; } else { unset($_SESSION["casi_di_successo_filter_cliente"]); } 
        if ($filterurllogo != "") { $where .= " and casi_di_successo.urllogo like '%" . $filterurllogo . "%' "; $_SESSION["casi_di_successo_filter_urllogo"] = $filterurllogo; } else { unset($_SESSION["casi_di_successo_filter_urllogo"]); } 
        if ($filterdata_from != "") { $where .= " and casi_di_successo.data >= '" . $filterdata_from . "' "; $_SESSION["casi_di_successo_filter_data_from"] = $filterdata_from; } else {unset($_SESSION["casi_di_successo_filter_data_from"]); } 
        if ($filterdata_to   != "") { $where .= " and casi_di_successo.data <= '" . $filterdata_to . "' ";   $_SESSION["casi_di_successo_filter_data_to"] =   $filterdata_to; }   else {unset($_SESSION["casi_di_successo_filter_data_to"]); } 
        if ($filtersito != "") { $where .= " and casi_di_successo.sito like '%" . $filtersito . "%' "; $_SESSION["casi_di_successo_filter_sito"] = $filtersito; } else { unset($_SESSION["casi_di_successo_filter_sito"]); } 
        if ($filterfoto != "") { $where .= " and casi_di_successo.foto like '%" . $filterfoto . "%' "; $_SESSION["casi_di_successo_filter_foto"] = $filterfoto; } else { unset($_SESSION["casi_di_successo_filter_foto"]); } 
        if ($filterstoria != "") { $where .= " and casi_di_successo.storia like '%" . $filterstoria . "%' "; $_SESSION["casi_di_successo_filter_storia"] = $filterstoria; } else { unset($_SESSION["casi_di_successo_filter_storia"]); } 
        if ($filtersfida != "") { $where .= " and casi_di_successo.sfida like '%" . $filtersfida . "%' "; $_SESSION["casi_di_successo_filter_sfida"] = $filtersfida; } else { unset($_SESSION["casi_di_successo_filter_sfida"]); } 
        if ($filtersoluzione != "") { $where .= " and casi_di_successo.soluzione like '%" . $filtersoluzione . "%' "; $_SESSION["casi_di_successo_filter_soluzione"] = $filtersoluzione; } else { unset($_SESSION["casi_di_successo_filter_soluzione"]); } 
        if ($filterextra != "") { $where .= " and casi_di_successo.extra like '%" . $filterextra . "%' "; $_SESSION["casi_di_successo_filter_extra"] = $filterextra; } else { unset($_SESSION["casi_di_successo_filter_extra"]); } 
        if ($filterid_settore  != "") { $where .= " and casi_di_successo.id_settore = " . $filterid_settore . " "; $_SESSION["casi_di_successo_filter_id_settore"] = $filterid_settore; } else { unset($_SESSION["casi_di_successo_filter_id_settore"]); } 
        if ($filterid_regione  != "") { $where .= " and casi_di_successo.id_regione = " . $filterid_regione . " "; $_SESSION["casi_di_successo_filter_id_regione"] = $filterid_regione; } else { unset($_SESSION["casi_di_successo_filter_id_regione"]); } 
        if ($filterid_moduli  != "") { $where .= " and casi_di_successo.id_moduli = " . $filterid_moduli . " "; $_SESSION["casi_di_successo_filter_id_moduli"] = $filterid_moduli; } else { unset($_SESSION["casi_di_successo_filter_id_moduli"]); } 
        if ($filterid_moduli1  != "") { $where .= " and casi_di_successo.id_moduli1 = " . $filterid_moduli1 . " "; $_SESSION["casi_di_successo_filter_id_moduli1"] = $filterid_moduli1; } else { unset($_SESSION["casi_di_successo_filter_id_moduli1"]); } 
        if ($filterid_moduli2  != "") { $where .= " and casi_di_successo.id_moduli2 = " . $filterid_moduli2 . " "; $_SESSION["casi_di_successo_filter_id_moduli2"] = $filterid_moduli2; } else { unset($_SESSION["casi_di_successo_filter_id_moduli2"]); } 
        if ($filterid_moduli3  != "") { $where .= " and casi_di_successo.id_moduli3 = " . $filterid_moduli3 . " "; $_SESSION["casi_di_successo_filter_id_moduli3"] = $filterid_moduli3; } else { unset($_SESSION["casi_di_successo_filter_id_moduli3"]); } 
        if ($filterid_moduli4  != "") { $where .= " and casi_di_successo.id_moduli4 = " . $filterid_moduli4 . " "; $_SESSION["casi_di_successo_filter_id_moduli4"] = $filterid_moduli4; } else { unset($_SESSION["casi_di_successo_filter_id_moduli4"]); } 

        $fixedOrder = "";
        echo json_encode(
            SSP::complex( $_POST, $sql_details, $table, $primaryKey, $columns, $where, $fixedOrder )
        );
    }


    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'button_record') {
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $html_button = '<div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown" aria-expanded="false"><i class="icon-th-list-1"></i></a>
                                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(22px, 19px, 0px);">';
        
        $button_edit = true;
        $button_delete = true;
        if ($_SESSION["user_username"] != "sysadmin") {
            if (!checkUserRight(9, "edit")) { $button_edit = false; }
            if (!checkUserRight(9, "delete")) { $button_delete = false; }
        }
        if ($button_edit) { $html_button .= '<a href="#" onclick="edit_record('.$id.');" class="dropdown-item"><i class="icon-pencil"></i> Edit</a>'; }        else { $html_button .= '<a href="#" onclick="edit_record('.$id.');" class="dropdown-item"><i class="icon-eye"></i> View</a>'; }

        // CHECK IF RECORD CAN BE DELETED
        $external_usage = 0;

        if ($button_delete) {
            if ($external_usage == 0) {
                $html_button .= '<a href="#" class="dropdown-item" onclick="delete_record('.filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING).');"><i class="icon-trash"></i> Delete</a>';
            } else {
                $html_button .= '<a href="#" class="dropdown-item" style="color:#dddddd;"><i class="icon-trash"></i> Delete</a>';
            }
        }

        $html_button .= '</div></div></div>';
        echo $html_button;
    }


    
    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'delete_record') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(9, "delete")) {echo json_encode("NOT AUTHORIZED"); die();}}
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);

        $db->query("DELETE FROM casi_di_successo WHERE id = ".$id);
        echo "OK";
    }

    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'save_record') {
        
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $input_titolo = filter_input(INPUT_POST, "titolo", FILTER_SANITIZE_STRING);
        $input_sottotitolo = filter_input(INPUT_POST, "sottotitolo", FILTER_SANITIZE_STRING);
        $input_cliente = filter_input(INPUT_POST, "cliente", FILTER_SANITIZE_STRING);
        $input_urllogo = filter_input(INPUT_POST, "urllogo", FILTER_SANITIZE_STRING);
        $input_data = filter_input(INPUT_POST, "data", FILTER_SANITIZE_STRING);
        if ($input_data == "") { $input_data = null; }
        $input_sito = filter_input(INPUT_POST, "sito", FILTER_SANITIZE_STRING);
        $input_FILELIST_foto = html_entity_decode(filter_input(INPUT_POST, "foto", FILTER_SANITIZE_STRING));
        $input_storia = filter_input(INPUT_POST, "storia", FILTER_SANITIZE_STRING);
        $input_sfida = filter_input(INPUT_POST, "sfida", FILTER_SANITIZE_STRING);
        $input_soluzione = filter_input(INPUT_POST, "soluzione", FILTER_SANITIZE_STRING);
        $input_extra = filter_input(INPUT_POST, "extra", FILTER_SANITIZE_STRING);
        $input_id_settore = filter_input(INPUT_POST, "id_settore", FILTER_SANITIZE_STRING);
        if ($input_id_settore == 0) { $input_id_settore = null; }
        if ($input_id_settore == "") { $input_id_settore = null; }
        $input_id_regione = filter_input(INPUT_POST, "id_regione", FILTER_SANITIZE_STRING);
        if ($input_id_regione == 0) { $input_id_regione = null; }
        if ($input_id_regione == "") { $input_id_regione = null; }
        $input_id_moduli = filter_input(INPUT_POST, "id_moduli", FILTER_SANITIZE_STRING);
        if ($input_id_moduli == 0) { $input_id_moduli = null; }
        if ($input_id_moduli == "") { $input_id_moduli = null; }
        $input_id_moduli1 = filter_input(INPUT_POST, "id_moduli1", FILTER_SANITIZE_STRING);
        if ($input_id_moduli1 == 0) { $input_id_moduli1 = null; }
        if ($input_id_moduli1 == "") { $input_id_moduli1 = null; }
        $input_id_moduli2 = filter_input(INPUT_POST, "id_moduli2", FILTER_SANITIZE_STRING);
        if ($input_id_moduli2 == 0) { $input_id_moduli2 = null; }
        if ($input_id_moduli2 == "") { $input_id_moduli2 = null; }
        $input_id_moduli3 = filter_input(INPUT_POST, "id_moduli3", FILTER_SANITIZE_STRING);
        if ($input_id_moduli3 == 0) { $input_id_moduli3 = null; }
        if ($input_id_moduli3 == "") { $input_id_moduli3 = null; }
        $input_id_moduli4 = filter_input(INPUT_POST, "id_moduli4", FILTER_SANITIZE_STRING);
        if ($input_id_moduli4 == 0) { $input_id_moduli4 = null; }
        if ($input_id_moduli4 == "") { $input_id_moduli4 = null; }
        $data = [
            "s", $input_titolo,
            "s", $input_sottotitolo,
            "s", $input_cliente,
            "s", $input_urllogo,
            "s", $input_data,
            "s", $input_sito,
            "s", $input_FILELIST_foto,
            "s", $input_storia,
            "s", $input_sfida,
            "s", $input_soluzione,
            "s", $input_extra,
            "i", $input_id_settore,
            "i", $input_id_regione,
            "i", $input_id_moduli,
            "i", $input_id_moduli1,
            "i", $input_id_moduli2,
            "i", $input_id_moduli3,
            "i", $input_id_moduli4,
        ];

        // (Types: "s" = string, "i" = integer, "d" = double, "b" = blob)
        if ($id == "") {
            if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(9, "create")) {echo json_encode("NOT AUTHORIZED"); die();}}
            $sql = "INSERT INTO casi_di_successo (titolo, sottotitolo, cliente, urllogo, data, sito, foto, storia, sfida, soluzione, extra, id_settore, id_regione, id_moduli, id_moduli1, id_moduli2, id_moduli3, id_moduli4) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            $retv = $db->prepare_and_execute($sql, $data);
        } else {
            if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(9, "edit")) {echo json_encode("NOT AUTHORIZED"); die();}}
            array_push($data, "i", $id);
            $sql = "UPDATE casi_di_successo SET titolo=?, sottotitolo=?, cliente=?, urllogo=?, data=?, sito=?, foto=?, storia=?, sfida=?, soluzione=?, extra=?, id_settore=?, id_regione=?, id_moduli=?, id_moduli1=?, id_moduli2=?, id_moduli3=?, id_moduli4=? WHERE id=?";
            $retv = $db->prepare_and_execute($sql, $data);
        }
        if ($retv == 1) {

            echo "OK";
        } else {
            echo "Error: ".$retv;
        }
    }
    
    if (filter_input(INPUT_POST, "action", FILTER_SANITIZE_STRING) == "save_record_row") {
        $id = filter_input(INPUT_POST, "id", FILTER_SANITIZE_STRING);
        $data = [
        ];

        // (Types: "s" = string, "i" = integer, "d" = double, "b" = blob)
        if ($id == "") {
            if ($_SESSION["user_username"] != "sysadmin") {
                if ((!checkUserRight(9, "create")) && (!checkUserRight(9, "edit"))) {
                    echo json_encode("NOT AUTHORIZED");
                    die();
                }
            }
            $sql = "INSERT INTO ) VALUES)";
            $retv = $db->prepare_and_execute($sql, $data);
        } else {
            if ($_SESSION["user_username"] != "sysadmin") {
                if ((!checkUserRight(9, "create")) && (!checkUserRight(9, "edit"))) {
                    echo json_encode("NOT AUTHORIZED");
                    die();
                }
            }
            array_push($data, "i", $id);
            $sql = "UPDATE  SE WHERE id=?";
            $retv = $db->prepare_and_execute($sql, $data);
        }
        if ($retv == 1) {
            echo "OK";
        } else {
            echo "Error: ".$retv;
        }
    }
}