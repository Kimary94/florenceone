<?php

if (!isset($_SESSION)) { session_start(); }
include "sys_database.php";
include "../sys_functions.php";
include "sys_ssp.class.php";

// action: read_record        LOAD SINGLE RECORD FROM categoria FOR MODAL EDIT
// action: read_record_row    LOAD SINGLE ROWS RECORD FROM row_categoria FOR MODAL EDIT
// action: load_table         LOAD TABLE categoria
// action: load_table_rows    LOAD TABLE row_categoria
// action: button_record      RETURN BUTTON (to put inside datatable) FOR SINGLE RECORD ON categoria
// action: button_record_row  RETURN BUTTON (to put inside datatable) FOR SINGLE RECORD ON row_categoria
// action: delete_record      DELETE RECORD ON categoria
// action: delete_record_row  DELETE RECORD ON row_categoria
// action: save_record        SAVE RECORD ON categoria
// action: save_record_row    SAVE RECORD ON row_categoria


if(isset($_POST['action'])) {

    
    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'read_record') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(1, "read")) {echo json_encode("NOT AUTHORIZED"); die();}}
        $id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT);
        $risultato = array();
        $qry = "SELECT * FROM categoria WHERE categoria.id = ".$id;
        $query = $db->query($qry);
        while ($risultati = mysqli_fetch_array($query))
        {
            $risultato["record_id"] = $risultati["id"];
            $risultato["record_descrizione"] = $risultati["descrizione"];
            $risultato["record_ordine"] = $risultati["ordine"];
            $risultato["record_um"] = $risultati["um"];

        }
        echo json_encode($risultato);
    }

 
    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'read_record_row') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(1, "read")) {echo json_encode("NOT AUTHORIZED"); die();}}
        $qry = "SELECT * FROM row_categoria WHERE id = " . filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $query = $db->query($qry);
        $risultati = mysqli_fetch_array($query);
        echo json_encode($risultati);
    }

    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'load_table') {
        

        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(1, "read")) {echo json_encode("NOT AUTHORIZED"); die();}}

        
        
        //array( "db" => "CONVERT(categoria.descrizione USING ascii) as descrizione", "dt" => 1, "field" => "descrizione" ), 

        $table = "categoria";
        $primaryKey = 'categoria.id';
        $columns = array(
        array( "db" => "categoria.id", "dt" => 0, "field" => "id" ), 
        array( "db" => "CONVERT(categoria.descrizione USING binary) as descrizione", "dt" => 1, "field" => "descrizione" ), 
        array( "db" => "categoria.ordine", "dt" => 2, "field" => "ordine" ), 
        array( "db" => "CONVERT(categoria.um USING binary) as um", "dt" => 3, "field" => "um" ), 
        array( "db" => "emptycolumn", "dt" => 4, "field" => "" ), 
        );
        $sql_details = array(
            "user" => $db_username,
            "pass" => $db_password,
            "db"   => $db_database,
            "host" => $db_host,
        );
        $filterdescrizione = filter_input(INPUT_POST, "filterdescrizione", FILTER_SANITIZE_STRING);
        $filterum = filter_input(INPUT_POST, "filterum", FILTER_SANITIZE_STRING);
        $where = " categoria.id >= 0 ";
        if ($filterdescrizione != "") { $where .= " and categoria.descrizione like '%" . $filterdescrizione . "%' "; $_SESSION["categoria_filter_descrizione"] = $filterdescrizione; } else { unset($_SESSION["categoria_filter_descrizione"]); } 
        if ($filterum != "") { $where .= " and categoria.um like '%" . $filterum . "%' "; $_SESSION["categoria_filter_um"] = $filterum; } else { unset($_SESSION["categoria_filter_um"]); } 
        

        $fixedOrder = "";
        echo json_encode(
            SSP::complex( $_POST, $sql_details, $table, $primaryKey, $columns, $where, $fixedOrder )
        );
    }

    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'load_table_rows') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(1, "read")) {die();}}
        $table = "row_categoria LEFT JOIN categoria ON categoria.id = row_categoria.id_categoria";
        $primaryKey = "row_categoria.id";
        $columns = array(
            array( "db" => "row_categoria.id", "dt" => 0, "field" => "id" ), 
            array( "db" => "row_categoria.descrizione", "dt" => 1, "field" => "descrizione" ), 
            array( "db" => "row_categoria.ordine", "dt" => 2, "field" => "ordine" ), 
            array( "db" => "emptycolumn", "dt" => 3, "field" => "" ),
        );

        $sql_details = array(
            "user" => $db_username,
            "pass" => $db_password,
            "db"   => $db_database,
            "host" => $db_host,
        );

        $filter_id = filter_input(INPUT_POST, "filter_id", FILTER_SANITIZE_STRING);
        $where = " row_categoria.id >= 0 ";
        if ($filter_id != "") { $where .= " and row_categoria.id_categoria = " . $filter_id . " "; } 
        
        $fixedOrder = "";
        echo json_encode(
            SSP::complex( $_POST, $sql_details, $table, $primaryKey, $columns, $where, $fixedOrder )
        );
    }


    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'button_record') {
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $html_button = '<div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown" aria-expanded="false"><i class="icon-th-list-1"></i></a>
                                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(22px, 19px, 0px);">';
        
        $button_edit = true;
        $button_delete = true;
        if ($_SESSION["user_username"] != "sysadmin") {
            if (!checkUserRight(1, "edit")) { $button_edit = false; }
            if (!checkUserRight(1, "delete")) { $button_delete = false; }
        }
        if ($button_edit) { $html_button .= '<a href="categorie_edit.php?id='.$id.'" class="dropdown-item"><i class="icon-pencil"></i> Edit</a>'; }        else { $html_button .= '<a href="categorie_edit.php?id='.$id.'" class="dropdown-item"><i class="icon-eye"></i> View</a>'; }

        // CHECK IF RECORD CAN BE DELETED
        $external_usage = 0;

        $query = $db->query("SELECT count(*) as conteggio FROM categoriaxarticolo WHERE id_categoria = ".$id);
        while ($risultati = mysqli_fetch_array($query)) {
            if ($risultati["conteggio"] <> "") { if (intval($risultati["conteggio"]) > 0) { $external_usage += 1; }}
        }

        if ($button_delete) {
            if ($external_usage == 0) {
                $html_button .= '<a href="#" class="dropdown-item" onclick="delete_record('.filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING).');"><i class="icon-trash"></i> Delete</a>';
            } else {
                $html_button .= '<a href="#" class="dropdown-item" style="color:#dddddd;"><i class="icon-trash"></i> Delete</a>';
            }
        }

        $html_button .= '</div></div></div>';
        echo $html_button;
    }


    
    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'button_record_row') {
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $html_button = '<div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown" aria-expanded="false"><i class="icon-th-list-1"></i></a>
                                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(22px, 19px, 0px);">';
        
        $button_edit = true;
        $button_delete = true;
        if ($_SESSION["user_username"] != "sysadmin") {
            if (!checkUserRight(1, "edit")) { $button_edit = false; }
            if (!checkUserRight(1, "delete")) { $button_delete = false; }
        }
        
        if ($button_edit) {
            $html_button .= '<a href="#" onclick="edit_row('.$id.');" class="dropdown-item"><i class="icon-pencil"></i> Edit</a>';
        } else {
            $html_button .= '<a href="#" onclick="edit_row('.$id.');" class="dropdown-item"><i class="icon-eye"></i> View</a>';
        }

        // CHECK IF RECORD CAN BE DELETED
        $external_usage = 0;
        $query = $db->query("SELECT count(*) as conteggio FROM categoriaxarticolo WHERE id_row_categoria = ".$id);
        while ($risultati = mysqli_fetch_array($query)) {
            if ($risultati["conteggio"] <> "") { if (intval($risultati["conteggio"]) > 0) { $external_usage += 1; }}
        }

        $query = $db->query("SELECT count(*) as conteggio FROM immaginexlato WHERE id_row_categoria = ".$id);
        while ($risultati = mysqli_fetch_array($query)) {
            if ($risultati["conteggio"] <> "") { if (intval($risultati["conteggio"]) > 0) { $external_usage += 1; }}
        }

        if ($button_delete) {
            if ($external_usage == 0) {
                $html_button .= '<a href="#" class="dropdown-item" onclick="delete_row('.filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING).');"><i class="icon-trash"></i>Delete</a>';
            } else {
                $html_button .= '<a href="#" class="dropdown-item" style="color:#dddddd;"><i class="icon-trash"></i>Delete</a>';
            }
        }
        
        $html_button .= "</div></div></div>";
        echo $html_button;
    }


    
    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'delete_record') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(1, "delete")) {echo json_encode("NOT AUTHORIZED"); die();}}
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $db->query("DELETE FROM row_categoria WHERE id_categoria = ".$id);

        $db->query("DELETE FROM categoria WHERE id = ".$id);
        echo "OK";
    }

    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'delete_record_row') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(1, "delete")) {echo json_encode("NOT AUTHORIZED"); die();}}
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $db->query("DELETE FROM row_categoria WHERE id = ".$id);
        echo "OK";
    }

    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'save_record') {
        
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $input_descrizione = filter_input(INPUT_POST, "descrizione", FILTER_SANITIZE_STRING);
        $input_ordine = filter_input(INPUT_POST, "ordine", FILTER_SANITIZE_STRING);
        if ($input_ordine == "") { $input_ordine = null; }
        $input_um = filter_input(INPUT_POST, "um", FILTER_SANITIZE_STRING);
        $data = [
            "s", $input_descrizione,
            "i", $input_ordine,
            "s", $input_um,
        ];

        // (Types: "s" = string, "i" = integer, "d" = double, "b" = blob)
        if ($id == "") {
            if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(1, "create")) {echo json_encode("NOT AUTHORIZED"); die();}}
            $sql = "INSERT INTO categoria (descrizione, ordine, um) VALUES (?, ?, ?)";
            $retv = $db->prepare_and_execute($sql, $data);
        } else {
            if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(1, "edit")) {echo json_encode("NOT AUTHORIZED"); die();}}
            array_push($data, "i", $id);
            $sql = "UPDATE categoria SET descrizione=?, ordine=?, um=? WHERE id=?";
            $retv = $db->prepare_and_execute($sql, $data);
        }
        if ($retv == 1) {

            if ($id == "") {
                $query = $db->query("SELECT id FROM categoria ORDER BY id DESC LIMIT 1");
                while ($risultati = mysqli_fetch_array($query)) { $nuovo_id = $risultati["id"]; }
                $db->query("UPDATE row_categoria SET id_categoria = ".$nuovo_id." WHERE id_categoria = -".$_SESSION["user_id"]);
            }

            echo "OK";
        } else {
            echo "Error: ".$retv;
        }
    }
    
    if (filter_input(INPUT_POST, "action", FILTER_SANITIZE_STRING) == "save_record_row") {
        $id = filter_input(INPUT_POST, "id", FILTER_SANITIZE_STRING);
        $input_descrizione = filter_input(INPUT_POST, "descrizione", FILTER_SANITIZE_STRING);
        $input_id_categoria = filter_input(INPUT_POST, "id_categoria", FILTER_SANITIZE_STRING);
        if ($input_id_categoria == 0) { $input_id_categoria = null; }
        if ($input_id_categoria == "") { $input_id_categoria = null; }
        $input_ordine = filter_input(INPUT_POST, "ordine", FILTER_SANITIZE_STRING);
        if ($input_ordine == "") { $input_ordine = null; }
        $input_FILELIST_immagine_scheda_tecnica = html_entity_decode(filter_input(INPUT_POST, "immagine_scheda_tecnica", FILTER_SANITIZE_STRING));
        $input_FILELIST_immagine_menu = html_entity_decode(filter_input(INPUT_POST, "immagine_menu", FILTER_SANITIZE_STRING));
        $input_FILELIST_immagine_wizard = html_entity_decode(filter_input(INPUT_POST, "immagine_wizard", FILTER_SANITIZE_STRING));
        $input_FILELIST_immagine_extra = html_entity_decode(filter_input(INPUT_POST, "immagine_extra", FILTER_SANITIZE_STRING));
        $data = [
            "s", $input_descrizione,
            "i", $input_id_categoria,
            "i", $input_ordine,
            "s", $input_FILELIST_immagine_scheda_tecnica,
            "s", $input_FILELIST_immagine_menu,
            "s", $input_FILELIST_immagine_wizard,
            "s", $input_FILELIST_immagine_extra,
        ];

        // (Types: "s" = string, "i" = integer, "d" = double, "b" = blob)
        if ($id == "") {
            if ($_SESSION["user_username"] != "sysadmin") {
                if ((!checkUserRight(1, "create")) && (!checkUserRight(1, "edit"))) {
                    echo json_encode("NOT AUTHORIZED");
                    die();
                }
            }
            $sql = "INSERT INTO row_categoria (descrizione, id_categoria, ordine, immagine_scheda_tecnica, immagine_menu, immagine_wizard, immagine_extra) VALUES (?, ?, ?, ?, ?, ?, ?)";
            $retv = $db->prepare_and_execute($sql, $data);
        } else {
            if ($_SESSION["user_username"] != "sysadmin") {
                if ((!checkUserRight(1, "create")) && (!checkUserRight(1, "edit"))) {
                    echo json_encode("NOT AUTHORIZED");
                    die();
                }
            }
            array_push($data, "i", $id);
            $sql = "UPDATE row_categoria SET descrizione=?, id_categoria=?, ordine=?, immagine_scheda_tecnica=?, immagine_menu=?, immagine_wizard=?, immagine_extra=? WHERE id=?";
            $retv = $db->prepare_and_execute($sql, $data);
        }
        if ($retv == 1) {
            echo "OK";
        } else {
            echo "Error: ".$retv;
        }
    }
}