<?php

if (!isset($_SESSION)) { session_start(); }
include "sys_database.php";
include "../sys_functions.php";
include "sys_ssp.class.php";

// action: read_record        LOAD SINGLE RECORD FROM cliente FOR MODAL EDIT
// action: read_record_row    LOAD SINGLE ROWS RECORD FROM row_cliente FOR MODAL EDIT
// action: load_table         LOAD TABLE cliente
// action: load_table_rows    LOAD TABLE row_cliente
// action: button_record      RETURN BUTTON (to put inside datatable) FOR SINGLE RECORD ON cliente
// action: button_record_row  RETURN BUTTON (to put inside datatable) FOR SINGLE RECORD ON row_cliente
// action: delete_record      DELETE RECORD ON cliente
// action: delete_record_row  DELETE RECORD ON row_cliente
// action: save_record        SAVE RECORD ON cliente
// action: save_record_row    SAVE RECORD ON row_cliente


if(isset($_POST['action'])) {

    
    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'read_record') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(1, "read")) {echo json_encode("NOT AUTHORIZED"); die();}}
        $id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT);
        $risultato = array();
        $qry = "SELECT * FROM cliente WHERE cliente.id = ".$id;
        $query = $db->query($qry);
        while ($risultati = mysqli_fetch_array($query))
        {
            $risultato["record_id"] = $risultati["id"];
            $risultato["record_ragione_sociale"] = $risultati["ragione_sociale"];

        }
        echo json_encode($risultato);
    }

 
    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'read_record_row') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(1, "read")) {echo json_encode("NOT AUTHORIZED"); die();}}
        $qry = "SELECT * FROM row_cliente WHERE id = " . filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $query = $db->query($qry);
        $risultati = mysqli_fetch_array($query);
        echo json_encode($risultati);
    }

    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'load_table') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(1, "read")) {echo json_encode("NOT AUTHORIZED"); die();}}
        $table = "cliente";
        $primaryKey = 'cliente.id';
        $columns = array(
        array( "db" => "cliente.id", "dt" => 0, "field" => "id" ), 
        array( "db" => "cliente.ragione_sociale", "dt" => 1, "field" => "ragione_sociale" ), 
        array( "db" => "emptycolumn", "dt" => 2, "field" => "" ), 
        );

        $sql_details = array(
            "user" => $db_username,
            "pass" => $db_password,
            "db"   => $db_database,
            "host" => $db_host,
        );
        $filterragione_sociale = filter_input(INPUT_POST, "filterragione_sociale", FILTER_SANITIZE_STRING);

        $where = " cliente.id >= 0 ";
        if ($filterragione_sociale != "") { $where .= " and cliente.ragione_sociale like '%" . $filterragione_sociale . "%' "; $_SESSION["cliente_filter_ragione_sociale"] = $filterragione_sociale; } else { unset($_SESSION["cliente_filter_ragione_sociale"]); } 

        $fixedOrder = "";
        echo json_encode(
            SSP::complex( $_POST, $sql_details, $table, $primaryKey, $columns, $where, $fixedOrder )
        );
    }

    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'load_table_rows') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(1, "read")) {die();}}
        $table = "row_cliente LEFT JOIN cliente ON cliente.id = row_cliente.id_cliente";
        $primaryKey = "row_cliente.id";
        $columns = array(
            array( "db" => "row_cliente.id", "dt" => 0, "field" => "id" ), 
            array( "db" => "row_cliente.progetto", "dt" => 1, "field" => "progetto" ), 
            array( "db" => "emptycolumn", "dt" => 2, "field" => "" ),
        );

        $sql_details = array(
            "user" => $db_username,
            "pass" => $db_password,
            "db"   => $db_database,
            "host" => $db_host,
        );

        $filter_id = filter_input(INPUT_POST, "filter_id", FILTER_SANITIZE_STRING);
        $where = " row_cliente.id >= 0 ";
        if ($filter_id != "") { $where .= " and row_cliente.id_cliente = " . $filter_id . " "; } 
        
        $fixedOrder = "";
        echo json_encode(
            SSP::complex( $_POST, $sql_details, $table, $primaryKey, $columns, $where, $fixedOrder )
        );
    }


    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'button_record') {
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $html_button = '<div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown" aria-expanded="false"><i class="icon-th-list-1"></i></a>
                                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(22px, 19px, 0px);">';
        
        $button_edit = true;
        $button_delete = true;
        if ($_SESSION["user_username"] != "sysadmin") {
            if (!checkUserRight(1, "edit")) { $button_edit = false; }
            if (!checkUserRight(1, "delete")) { $button_delete = false; }
        }
        if ($button_edit) { $html_button .= '<a href="clienti_edit.php?id='.$id.'" class="dropdown-item"><i class="icon-pencil"></i> Modifica</a>'; }        else { $html_button .= '<a href="clienti_edit.php?id='.$id.'" class="dropdown-item"><i class="icon-eye"></i> View</a>'; }

        // CHECK IF RECORD CAN BE DELETED
        $external_usage = 0;

        $query = $db->query("SELECT count(*) as conteggio FROM ore WHERE id_cliente = ".$id);
        while ($risultati = mysqli_fetch_array($query)) {
            if ($risultati["conteggio"] <> "") { if (intval($risultati["conteggio"]) > 0) { $external_usage += 1; }}
        }

        if ($button_delete) {
            if ($external_usage == 0) {
                $html_button .= '<a href="#" class="dropdown-item" onclick="delete_record('.filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING).');"><i class="icon-trash"></i> Elimina</a>';
            } else {
                $html_button .= '<a href="#" class="dropdown-item" style="color:#dddddd;"><i class="icon-trash"></i> Elimina</a>';
            }
        }

        $html_button .= '</div></div></div>';
        echo $html_button;
    }


    
    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'button_record_row') {
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $html_button = '<div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown" aria-expanded="false"><i class="icon-th-list-1"></i></a>
                                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(22px, 19px, 0px);">';
        
        $button_edit = true;
        $button_delete = true;
        if ($_SESSION["user_username"] != "sysadmin") {
            if (!checkUserRight(1, "edit")) { $button_edit = false; }
            if (!checkUserRight(1, "delete")) { $button_delete = false; }
        }
        
        if ($button_edit) {
            $html_button .= '<a href="#" onclick="edit_row('.$id.');" class="dropdown-item"><i class="icon-pencil"></i> Edit</a>';
        } else {
            $html_button .= '<a href="#" onclick="edit_row('.$id.');" class="dropdown-item"><i class="icon-eye"></i> View</a>';
        }

        // CHECK IF RECORD CAN BE DELETED
        $external_usage = 0;
        $query = $db->query("SELECT count(*) as conteggio FROM ore WHERE id_row_cliente = ".$id);
        while ($risultati = mysqli_fetch_array($query)) {
            if ($risultati["conteggio"] <> "") { if (intval($risultati["conteggio"]) > 0) { $external_usage += 1; }}
        }

        if ($button_delete) {
            if ($external_usage == 0) {
                $html_button .= '<a href="#" class="dropdown-item" onclick="delete_row('.filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING).');"><i class="icon-trash"></i>Delete</a>';
            } else {
                $html_button .= '<a href="#" class="dropdown-item" style="color:#dddddd;"><i class="icon-trash"></i>Delete</a>';
            }
        }
        
        $html_button .= "</div></div></div>";
        echo $html_button;
    }


    
    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'delete_record') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(1, "delete")) {echo json_encode("NOT AUTHORIZED"); die();}}
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $db->query("DELETE FROM row_cliente WHERE id_cliente = ".$id);

        $db->query("DELETE FROM cliente WHERE id = ".$id);
        echo "OK";
    }

    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'delete_record_row') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(1, "delete")) {echo json_encode("NOT AUTHORIZED"); die();}}
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $db->query("DELETE FROM row_cliente WHERE id = ".$id);
        echo "OK";
    }

    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'save_record') {
        
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $input_ragione_sociale = filter_input(INPUT_POST, "ragione_sociale", FILTER_SANITIZE_STRING);
        $data = [
            "s", $input_ragione_sociale,
        ];

        // (Types: "s" = string, "i" = integer, "d" = double, "b" = blob)
        if ($id == "") {
            if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(1, "create")) {echo json_encode("NOT AUTHORIZED"); die();}}
            $sql = "INSERT INTO cliente (ragione_sociale) VALUES (?)";
            $retv = $db->prepare_and_execute($sql, $data);
        } else {
            if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(1, "edit")) {echo json_encode("NOT AUTHORIZED"); die();}}
            array_push($data, "i", $id);
            $sql = "UPDATE cliente SET ragione_sociale=? WHERE id=?";
            $retv = $db->prepare_and_execute($sql, $data);
        }
        if ($retv == 1) {

            if ($id == "") {
                $query = $db->query("SELECT id FROM cliente ORDER BY id DESC LIMIT 1");
                while ($risultati = mysqli_fetch_array($query)) { $nuovo_id = $risultati["id"]; }
                $db->query("UPDATE row_cliente SET id_cliente = ".$nuovo_id." WHERE id_cliente = -".$_SESSION["user_id"]);
            }

            echo "OK";
        } else {
            echo "Error: ".$retv;
        }
    }
    
    if (filter_input(INPUT_POST, "action", FILTER_SANITIZE_STRING) == "save_record_row") {
        $id = filter_input(INPUT_POST, "id", FILTER_SANITIZE_STRING);
        $input_progetto = filter_input(INPUT_POST, "progetto", FILTER_SANITIZE_STRING);
        $input_id_cliente = filter_input(INPUT_POST, "id_cliente", FILTER_SANITIZE_STRING);
        if ($input_id_cliente == 0) { $input_id_cliente = null; }
        if ($input_id_cliente == "") { $input_id_cliente = null; }
        $data = [
            "s", $input_progetto,
            "i", $input_id_cliente,
        ];

        // (Types: "s" = string, "i" = integer, "d" = double, "b" = blob)
        if ($id == "") {
            if ($_SESSION["user_username"] != "sysadmin") {
                if ((!checkUserRight(1, "create")) && (!checkUserRight(1, "edit"))) {
                    echo json_encode("NOT AUTHORIZED");
                    die();
                }
            }
            $sql = "INSERT INTO row_cliente (progetto, id_cliente) VALUES (?, ?)";
            $retv = $db->prepare_and_execute($sql, $data);
        } else {
            if ($_SESSION["user_username"] != "sysadmin") {
                if ((!checkUserRight(1, "create")) && (!checkUserRight(1, "edit"))) {
                    echo json_encode("NOT AUTHORIZED");
                    die();
                }
            }
            array_push($data, "i", $id);
            $sql = "UPDATE row_cliente SET progetto=?, id_cliente=? WHERE id=?";
            $retv = $db->prepare_and_execute($sql, $data);
        }
        if ($retv == 1) {
            echo "OK";
        } else {
            echo "Error: ".$retv;
        }
    }
}