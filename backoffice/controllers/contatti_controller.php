<?php

if (!isset($_SESSION)) { session_start(); }
include "sys_database.php";
include "../sys_functions.php";
include "sys_ssp.class.php";

// action: read_record        LOAD SINGLE RECORD FROM contatti FOR MODAL EDIT
// action: load_table         LOAD TABLE contatti
// action: button_record      RETURN BUTTON (to put inside datatable) FOR SINGLE RECORD ON contatti
// action: delete_record      DELETE RECORD ON contatti
// action: save_record        SAVE RECORD ON contatti


if(isset($_POST['action'])) {

    
    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'read_record') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(15, "read")) {echo json_encode("NOT AUTHORIZED"); die();}}
        $id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT);
        $risultato = array();
        $qry = "SELECT * FROM contatti WHERE contatti.id = ".$id;
        $query = $db->query($qry);
        while ($risultati = mysqli_fetch_array($query))
        {
            $risultato["record_id"] = $risultati["id"];
            $risultato["record_nome"] = $risultati["nome"];
            $risultato["record_azienda"] = $risultati["azienda"];
            $risultato["record_email"] = $risultati["email"];
            $risultato["record_telefono"] = $risultati["telefono"];
            $risultato["record_pagina"] = $risultati["pagina"];
            $risultato["record_data"] = $risultati["data"];

        }
        echo json_encode($risultato);
    }


    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'load_table') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(15, "read")) {echo json_encode("NOT AUTHORIZED"); die();}}
        $table = "contatti";
        $primaryKey = 'contatti.id';
        $columns = array(
        array( "db" => "contatti.id", "dt" => 0, "field" => "id" ), 
        array( "db" => "contatti.nome", "dt" => 1, "field" => "nome" ), 
        array( "db" => "contatti.azienda", "dt" => 2, "field" => "azienda" ), 
        array( "db" => "contatti.email", "dt" => 3, "field" => "email" ), 
        array( "db" => "contatti.telefono", "dt" => 4, "field" => "telefono" ), 
        array( "db" => "contatti.pagina", "dt" => 5, "field" => "pagina" ), 
        array( "db" => "DATE_FORMAT(contatti.data, '%Y-%m-%d') as data", "dt" => 6, "field" => "data" ), 
        array( "db" => "emptycolumn", "dt" => 7, "field" => "" ), 
        );

        $sql_details = array(
            "user" => $db_username,
            "pass" => $db_password,
            "db"   => $db_database,
            "host" => $db_host,
        );
        $filternome = filter_input(INPUT_POST, "filternome", FILTER_SANITIZE_STRING);
        $filterazienda = filter_input(INPUT_POST, "filterazienda", FILTER_SANITIZE_STRING);
        $filteremail = filter_input(INPUT_POST, "filteremail", FILTER_SANITIZE_STRING);
        $filtertelefono = filter_input(INPUT_POST, "filtertelefono", FILTER_SANITIZE_STRING);
        $filterpagina = filter_input(INPUT_POST, "filterpagina", FILTER_SANITIZE_STRING);
        $filterdata_from = filter_input(INPUT_POST, "filterdata_from", FILTER_SANITIZE_STRING);
        $filterdata_to = filter_input(INPUT_POST, "filterdata_to", FILTER_SANITIZE_STRING);

        $where = " contatti.id >= 0 ";
        if ($filternome != "") { $where .= " and contatti.nome like '%" . $filternome . "%' "; $_SESSION["contatti_filter_nome"] = $filternome; } else { unset($_SESSION["contatti_filter_nome"]); } 
        if ($filterazienda != "") { $where .= " and contatti.azienda like '%" . $filterazienda . "%' "; $_SESSION["contatti_filter_azienda"] = $filterazienda; } else { unset($_SESSION["contatti_filter_azienda"]); } 
        if ($filteremail != "") { $where .= " and contatti.email like '%" . $filteremail . "%' "; $_SESSION["contatti_filter_email"] = $filteremail; } else { unset($_SESSION["contatti_filter_email"]); } 
        if ($filtertelefono != "") { $where .= " and contatti.telefono like '%" . $filtertelefono . "%' "; $_SESSION["contatti_filter_telefono"] = $filtertelefono; } else { unset($_SESSION["contatti_filter_telefono"]); } 
        if ($filterpagina != "") { $where .= " and contatti.pagina like '%" . $filterpagina . "%' "; $_SESSION["contatti_filter_pagina"] = $filterpagina; } else { unset($_SESSION["contatti_filter_pagina"]); } 
        if ($filterdata_from != "") { $where .= " and contatti.data >= '" . $filterdata_from . "' "; $_SESSION["contatti_filter_data_from"] = $filterdata_from; } else {unset($_SESSION["contatti_filter_data_from"]); } 
        if ($filterdata_to   != "") { $where .= " and contatti.data <= '" . $filterdata_to . "' ";   $_SESSION["contatti_filter_data_to"] =   $filterdata_to; }   else {unset($_SESSION["contatti_filter_data_to"]); } 

        $fixedOrder = "";
        echo json_encode(
            SSP::complex( $_POST, $sql_details, $table, $primaryKey, $columns, $where, $fixedOrder )
        );
    }


    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'button_record') {
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $html_button = '<div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown" aria-expanded="false"><i class="icon-th-list-1"></i></a>
                                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(22px, 19px, 0px);">';
        
        $button_edit = true;
        $button_delete = true;
        if ($_SESSION["user_username"] != "sysadmin") {
            if (!checkUserRight(15, "edit")) { $button_edit = false; }
            if (!checkUserRight(15, "delete")) { $button_delete = false; }
        }
        if ($button_edit) { $html_button .= '<a href="#" onclick="edit_record('.$id.');" class="dropdown-item"><i class="icon-pencil"></i> Edit</a>'; }        else { $html_button .= '<a href="#" onclick="edit_record('.$id.');" class="dropdown-item"><i class="icon-eye"></i> View</a>'; }

        // CHECK IF RECORD CAN BE DELETED
        $external_usage = 0;

        if ($button_delete) {
            if ($external_usage == 0) {
                $html_button .= '<a href="#" class="dropdown-item" onclick="delete_record('.filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING).');"><i class="icon-trash"></i> Delete</a>';
            } else {
                $html_button .= '<a href="#" class="dropdown-item" style="color:#dddddd;"><i class="icon-trash"></i> Delete</a>';
            }
        }

        $html_button .= '</div></div></div>';
        echo $html_button;
    }


    
    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'delete_record') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(15, "delete")) {echo json_encode("NOT AUTHORIZED"); die();}}
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);

        $db->query("DELETE FROM contatti WHERE id = ".$id);
        echo "OK";
    }

    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'save_record') {
        
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $input_nome = filter_input(INPUT_POST, "nome", FILTER_SANITIZE_STRING);
        $input_azienda = filter_input(INPUT_POST, "azienda", FILTER_SANITIZE_STRING);
        $input_email = filter_input(INPUT_POST, "email", FILTER_SANITIZE_STRING);
        $input_telefono = filter_input(INPUT_POST, "telefono", FILTER_SANITIZE_STRING);
        $input_pagina = filter_input(INPUT_POST, "pagina", FILTER_SANITIZE_STRING);
        $input_data = filter_input(INPUT_POST, "data", FILTER_SANITIZE_STRING);
        if ($input_data == "") { $input_data = null; }
        $data = [
            "s", $input_nome,
            "s", $input_azienda,
            "s", $input_email,
            "s", $input_telefono,
            "s", $input_pagina,
            "s", $input_data,
        ];

        // (Types: "s" = string, "i" = integer, "d" = double, "b" = blob)
        if ($id == "") {
            if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(15, "create")) {echo json_encode("NOT AUTHORIZED"); die();}}
            $sql = "INSERT INTO contatti (nome, azienda, email, telefono, pagina, data) VALUES (?, ?, ?, ?, ?, ?)";
            $retv = $db->prepare_and_execute($sql, $data);
        } else {
            if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(15, "edit")) {echo json_encode("NOT AUTHORIZED"); die();}}
            array_push($data, "i", $id);
            $sql = "UPDATE contatti SET nome=?, azienda=?, email=?, telefono=?, pagina=?, data=? WHERE id=?";
            $retv = $db->prepare_and_execute($sql, $data);
        }
        if ($retv == 1) {

            echo "OK";
        } else {
            echo "Error: ".$retv;
        }
    }
    
    if (filter_input(INPUT_POST, "action", FILTER_SANITIZE_STRING) == "save_record_row") {
        $id = filter_input(INPUT_POST, "id", FILTER_SANITIZE_STRING);
        $data = [
        ];

        // (Types: "s" = string, "i" = integer, "d" = double, "b" = blob)
        if ($id == "") {
            if ($_SESSION["user_username"] != "sysadmin") {
                if ((!checkUserRight(15, "create")) && (!checkUserRight(15, "edit"))) {
                    echo json_encode("NOT AUTHORIZED");
                    die();
                }
            }
            $sql = "INSERT INTO ) VALUES)";
            $retv = $db->prepare_and_execute($sql, $data);
        } else {
            if ($_SESSION["user_username"] != "sysadmin") {
                if ((!checkUserRight(15, "create")) && (!checkUserRight(15, "edit"))) {
                    echo json_encode("NOT AUTHORIZED");
                    die();
                }
            }
            array_push($data, "i", $id);
            $sql = "UPDATE  SE WHERE id=?";
            $retv = $db->prepare_and_execute($sql, $data);
        }
        if ($retv == 1) {
            echo "OK";
        } else {
            echo "Error: ".$retv;
        }
    }
}