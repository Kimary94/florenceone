<?php

if (!isset($_SESSION)) { session_start(); }
include "sys_database.php";
include "../sys_functions.php";
include "sys_ssp.class.php";

// action: read_record        LOAD SINGLE RECORD FROM news FOR MODAL EDIT
// action: load_table         LOAD TABLE news
// action: button_record      RETURN BUTTON (to put inside datatable) FOR SINGLE RECORD ON news
// action: delete_record      DELETE RECORD ON news
// action: save_record        SAVE RECORD ON news


if(isset($_POST['action'])) {

    
    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'read_record') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(12, "read")) {echo json_encode("NOT AUTHORIZED"); die();}}
        $id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT);
        $risultato = array();
        $qry = "SELECT * FROM news WHERE news.id = ".$id;
        $query = $db->query($qry);
        while ($risultati = mysqli_fetch_array($query))
        {
            $risultato["record_id"] = $risultati["id"];
            $risultato["record_titolo"] = $risultati["titolo"];
            $risultato["record_testo"] = $risultati["testo"];
            $risultato["record_img"] = $risultati["img"];
            $risultato["record_data"] = $risultati["data"];
            $risultato["record_attivo"] = $risultati["attivo"];

        }
        echo json_encode($risultato);
    }


    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'load_table') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(12, "read")) {echo json_encode("NOT AUTHORIZED"); die();}}
        $table = "news";
        $primaryKey = 'news.id';
        $columns = array(
        array( "db" => "news.id", "dt" => 0, "field" => "id" ), 
        array( "db" => "CONVERT(news.titolo USING binary) as titolo", "dt" => 1, "field" => "titolo" ), 
        array( "db" => "DATE_FORMAT(news.data, '%d/%m/%Y') as data", "dt" => 2, "field" => "data" ),  
        array( "db" => "IF(news.attivo = 1, \"<i class='icon-check'></i>\", \"<i class='icon-check-empty'></i>\") as attivo", "dt" => 3, "field" => "attivo" ), 
        array( "db" => "emptycolumn", "dt" => 4, "field" => "" ), 
        );

        $sql_details = array(
            "user" => $db_username,
            "pass" => $db_password,
            "db"   => $db_database,
            "host" => $db_host,
        );
        $filtertitolo = filter_input(INPUT_POST, "filtertitolo", FILTER_SANITIZE_STRING);
        $filtertesto = filter_input(INPUT_POST, "filtertesto", FILTER_SANITIZE_STRING);
        $filterdata_from = filter_input(INPUT_POST, "filterdata_from", FILTER_SANITIZE_STRING);
        $filterdata_to = filter_input(INPUT_POST, "filterdata_to", FILTER_SANITIZE_STRING);
        $filterattivo = filter_input(INPUT_POST, "filterattivo", FILTER_SANITIZE_STRING);

        $where = " news.id >= 0 ";
        if ($filtertitolo != "") { $where .= " and news.titolo like '%" . $filtertitolo . "%' "; $_SESSION["news_filter_titolo"] = $filtertitolo; } else { unset($_SESSION["news_filter_titolo"]); } 
        if ($filtertesto != "") { $where .= " and news.testo like '%" . $filtertesto . "%' "; $_SESSION["news_filter_testo"] = $filtertesto; } else { unset($_SESSION["news_filter_testo"]); } 
        if ($filterdata_from != "") { $where .= " and news.data >= '" . $filterdata_from . "' "; $_SESSION["news_filter_data_from"] = $filterdata_from; } else {unset($_SESSION["news_filter_data_from"]); } 
        if ($filterdata_to   != "") { $where .= " and news.data <= '" . $filterdata_to . "' ";   $_SESSION["news_filter_data_to"] =   $filterdata_to; }   else {unset($_SESSION["news_filter_data_to"]); } 
        if ($filterattivo == "1") { $where .= " and news.attivo = 1; "; $_SESSION["news_filter_attivo"] = $filterattivo; } else { unset($_SESSION["news_filter_attivo"]); } 

        $fixedOrder = "ORDER BY news.data DESC";
        echo json_encode(
            SSP::complex( $_POST, $sql_details, $table, $primaryKey, $columns, $where, $fixedOrder )
        );
    }


    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'button_record') {
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $html_button = '<div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown" aria-expanded="false"><i class="icon-th-list-1"></i></a>
                                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(22px, 19px, 0px);">';
        
        $button_edit = true;
        $button_delete = true;
        if ($_SESSION["user_username"] != "sysadmin") {
            if (!checkUserRight(12, "edit")) { $button_edit = false; }
            if (!checkUserRight(12, "delete")) { $button_delete = false; }
        }
        if ($button_edit) { $html_button .= '<a href="#" onclick="edit_record('.$id.');" class="dropdown-item"><i class="icon-pencil"></i> Edit</a>'; }        else { $html_button .= '<a href="#" onclick="edit_record('.$id.');" class="dropdown-item"><i class="icon-eye"></i> View</a>'; }

        // CHECK IF RECORD CAN BE DELETED
        $external_usage = 0;

        if ($button_delete) {
            if ($external_usage == 0) {
                $html_button .= '<a href="#" class="dropdown-item" onclick="delete_record('.filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING).');"><i class="icon-trash"></i> Delete</a>';
            } else {
                $html_button .= '<a href="#" class="dropdown-item" style="color:#dddddd;"><i class="icon-trash"></i> Delete</a>';
            }
        }

        $html_button .= '</div></div></div>';
        echo $html_button;
    }


    
    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'delete_record') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(12, "delete")) {echo json_encode("NOT AUTHORIZED"); die();}}
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);

        $db->query("DELETE FROM news WHERE id = ".$id);
        echo "OK";
    }

    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'save_record') {
        
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $input_titolo = filter_input(INPUT_POST, "titolo", FILTER_SANITIZE_STRING);
        //$input_testo = filter_input(INPUT_POST, "testo", FILTER_SANITIZE_STRING);
        $input_testo = $_POST["testo"];
        $input_FILELIST_img = html_entity_decode(filter_input(INPUT_POST, "img", FILTER_SANITIZE_STRING));
        $input_data = filter_input(INPUT_POST, "data", FILTER_SANITIZE_STRING);
        if ($input_data == "") { $input_data = null; }
        $input_attivo = filter_input(INPUT_POST, "attivo", FILTER_SANITIZE_STRING);
        $data = [
            "s", $input_titolo,
            "s", $input_testo,
            "s", $input_FILELIST_img,
            "s", $input_data,
            "i", $input_attivo,
        ];

        // (Types: "s" = string, "i" = integer, "d" = double, "b" = blob)
        if ($id == "") {
            if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(12, "create")) {echo json_encode("NOT AUTHORIZED"); die();}}
            $sql = "INSERT INTO news (titolo, testo, img, data, attivo) VALUES (?, ?, ?, ?, ?)";
            $retv = $db->prepare_and_execute($sql, $data);
        } else {
            if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(12, "edit")) {echo json_encode("NOT AUTHORIZED"); die();}}
            array_push($data, "i", $id);
            $sql = "UPDATE news SET titolo=?, testo=?, img=?, data=?, attivo=? WHERE id=?";
            $retv = $db->prepare_and_execute($sql, $data);
        }
        if ($retv == 1) {

            echo "OK";
        } else {
            echo "Error: ".$retv;
        }
    }
    
    if (filter_input(INPUT_POST, "action", FILTER_SANITIZE_STRING) == "save_record_row") {
        $id = filter_input(INPUT_POST, "id", FILTER_SANITIZE_STRING);
        $data = [
        ];

        // (Types: "s" = string, "i" = integer, "d" = double, "b" = blob)
        if ($id == "") {
            if ($_SESSION["user_username"] != "sysadmin") {
                if ((!checkUserRight(12, "create")) && (!checkUserRight(12, "edit"))) {
                    echo json_encode("NOT AUTHORIZED");
                    die();
                }
            }
            $sql = "INSERT INTO ) VALUES)";
            $retv = $db->prepare_and_execute($sql, $data);
        } else {
            if ($_SESSION["user_username"] != "sysadmin") {
                if ((!checkUserRight(12, "create")) && (!checkUserRight(12, "edit"))) {
                    echo json_encode("NOT AUTHORIZED");
                    die();
                }
            }
            array_push($data, "i", $id);
            $sql = "UPDATE  SE WHERE id=?";
            $retv = $db->prepare_and_execute($sql, $data);
        }
        if ($retv == 1) {
            echo "OK";
        } else {
            echo "Error: ".$retv;
        }
    }
}