<?php

if (!isset($_SESSION)) { session_start(); }
include "sys_database.php";
include "../sys_functions.php";
include "sys_ssp.class.php";

// action: read_record        LOAD SINGLE RECORD FROM ordine FOR MODAL EDIT
// action: load_table         LOAD TABLE ordine
// action: button_record      RETURN BUTTON (to put inside datatable) FOR SINGLE RECORD ON ordine
// action: delete_record      DELETE RECORD ON ordine
// action: save_record        SAVE RECORD ON ordine


if(isset($_POST['action'])) {

    /*
    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'read_record') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(4, "read")) {echo json_encode("NOT AUTHORIZED"); die();}}
        $id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT);
        $risultato = array();
        $qry = "SELECT * FROM ordine WHERE ordine.id = ".$id;
        $query = $db->query($qry);
        while ($risultati = mysqli_fetch_array($query))
        {
            $risultato["record_id"] = $risultati["id"];
            $risultato["record_giorno"] = $risultati["giorno"];
            $risultato["record_nome"] = $risultati["nome"];
            $risultato["record_cognome"] = $risultati["cognome"];
            $risultato["record_ragionesociale"] = $risultati["ragionesociale"];
            $risultato["record_id_status_cliente"] = $risultati["id_status_cliente"];
            $risultato["record_id_status_sede"] = $risultati["id_status_sede"];
            $risultato["record_totale_netto"] = $risultati["totale_netto"];
            $risultato["record_totale_iva"] = $risultati["totale_iva"];
            $risultato["record_totale_ordine"] = $risultati["totale_ordine"];
            $risultato["record_transazione_rc"] = $risultati["transazione_rc"];
            $risultato["record_transazione_tranID"] = $risultati["transazione_tranID"];
            $risultato["record_transazione_enrStatus"] = $risultati["transazione_enrStatus"];
            $risultato["record_transazione_authStatus"] = $risultati["transazione_authStatus"];
            $risultato["record_transazione_payInstrToken"] = $risultati["transazione_payInstrToken"];

        }
        echo json_encode($risultato);
    }
    */

    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'load_table') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(4, "read")) {echo json_encode("NOT AUTHORIZED"); die();}}
        $table = "ordine LEFT JOIN status_cliente ON status_cliente.id = ordine.id_status_cliente LEFT JOIN status_sede ON status_sede.id = ordine.id_status_sede
        LEFT JOIN modalita_pagamento ON modalita_pagamento.id = ordine.id_modalita_pagamento ";
        $primaryKey = 'ordine.id';
        $columns = array(
        array( "db" => "ordine.id", "dt" => 0, "field" => "id" ), 
        array( "db" => "ordine.id", "dt" => 1, "field" => "id" ), 
        array( "db" => "ordine.giorno", "dt" => 2, "field" => "giorno" ), 
        array( "db" => "CONVERT(CONCAT(ordine.nome, ' ', ordine.cognome, '<br>', ordine.ragionesociale) USING binary) as nominativo", "dt" => 3, "field" => "nominativo" ), 
        array( "db" => "IF ( CONVERT(status_sede.descrizione USING binary) = 'Annullato', '', CONVERT(status_cliente.descrizione USING binary) ) as desc_status_cliente", "dt" => 4, "field" => "desc_status_cliente" ), 
        array( "db" => "CONVERT(status_sede.descrizione USING binary) as desc_status_sede", "dt" => 5, "field" => "desc_status_sede" ), 
        array( "db" => "CONVERT(modalita_pagamento.descrizione USING binary) as modalita_pagamento", "dt" => 6, "field" => "modalita_pagamento" ), 
        array( "db" => "REPLACE(ordine.totale_finale, '.', ',') as totale_finale", "dt" => 7, "field" => "totale_finale" ), 
        array( "db" => "CONVERT(CONCAT( 'RC: ', ordine.transazione_rc, '<br>Tran.ID: ', ordine.transazione_tranID, '<br>', ordine.transazione_enrStatus, '<br>', 
        ordine.transazione_authStatus, '<br>', ordine.transazione_payInstrToken) USING binary) AS transazione", "dt" => 8, "field" => "transazione" ), 
        array( "db" => "emptycolumn", "dt" => 9, "field" => "" ), 
        );

        $sql_details = array(
            "user" => $db_username,
            "pass" => $db_password,
            "db"   => $db_database,
            "host" => $db_host,
        );
        $filtergiorno = filter_input(INPUT_POST, "filtergiorno", FILTER_SANITIZE_STRING);
        $filternome = filter_input(INPUT_POST, "filternome", FILTER_SANITIZE_STRING);
        $filtercognome = filter_input(INPUT_POST, "filtercognome", FILTER_SANITIZE_STRING);
        $filterragionesociale = filter_input(INPUT_POST, "filterragionesociale", FILTER_SANITIZE_STRING);
        $filterid_status_cliente = filter_input(INPUT_POST, "filterid_status_cliente", FILTER_SANITIZE_STRING);
        $filterid_status_sede = filter_input(INPUT_POST, "filterid_status_sede", FILTER_SANITIZE_STRING);
        
        $where = " ordine.id >= 0 ";
        if ($filtergiorno != "") { $where .= " and ordine.giorno like '%" . $filtergiorno . "%' "; $_SESSION["ordine_filter_giorno"] = $filtergiorno; } else { unset($_SESSION["ordine_filter_giorno"]); } 
        if ($filternome != "") { $where .= " and ordine.nome like '%" . $filternome . "%' "; $_SESSION["ordine_filter_nome"] = $filternome; } else { unset($_SESSION["ordine_filter_nome"]); } 
        if ($filtercognome != "") { $where .= " and ordine.cognome like '%" . $filtercognome . "%' "; $_SESSION["ordine_filter_cognome"] = $filtercognome; } else { unset($_SESSION["ordine_filter_cognome"]); } 
        if ($filterragionesociale != "") { $where .= " and ordine.ragionesociale like '%" . $filterragionesociale . "%' "; $_SESSION["ordine_filter_ragionesociale"] = $filterragionesociale; } else { unset($_SESSION["ordine_filter_ragionesociale"]); } 
        if ($filterid_status_cliente  != "") { $where .= " and ordine.id_status_cliente = " . $filterid_status_cliente . " "; $_SESSION["ordine_filter_id_status_cliente"] = $filterid_status_cliente; } else { unset($_SESSION["ordine_filter_id_status_cliente"]); } 
        if ($filterid_status_sede  != "") { $where .= " and ordine.id_status_sede = " . $filterid_status_sede . " "; $_SESSION["ordine_filter_id_status_sede"] = $filterid_status_sede; } else { unset($_SESSION["ordine_filter_id_status_sede"]); } 
        
        $fixedOrder = " ORDER BY ordine.id DESC ";
        echo json_encode(
            SSP::complex( $_POST, $sql_details, $table, $primaryKey, $columns, $where, $fixedOrder )
        );
    }


    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'button_record') {
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $html_button = '<div class="list-icons">
                        <a class="btn btn-success" target="_blank" href="dettaglio.php?id='.$id.'"><i class="icon-eye"></i> Dettaglio ordine</a>
                        </div>';
        echo $html_button;
    }


}