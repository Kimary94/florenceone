<?php

if (!isset($_SESSION)) { session_start(); }
include "sys_database.php";
include "../sys_functions.php";
include "sys_ssp.class.php";

// action: read_record        LOAD SINGLE RECORD FROM ore FOR MODAL EDIT
// action: load_table         LOAD TABLE ore
// action: button_record      RETURN BUTTON (to put inside datatable) FOR SINGLE RECORD ON ore
// action: delete_record      DELETE RECORD ON ore
// action: save_record        SAVE RECORD ON ore


if(isset($_POST['action'])) {

    
    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'read_record') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(2, "read")) {echo json_encode("NOT AUTHORIZED"); die();}}
        $id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT);
        $risultato = array();
        $qry = "SELECT * FROM ore WHERE ore.id = ".$id;
        $query = $db->query($qry);
        while ($risultati = mysqli_fetch_array($query))
        {
            $risultato["record_id"] = $risultati["id"];
            $risultato["record_giorno"] = $risultati["giorno"];
            $risultato["record_ore"] = $risultati["ore"];
            $risultato["record_minuti"] = $risultati["minuti"];
            $risultato["record_id_cliente"] = $risultati["id_cliente"];
            $risultato["record_id_row_cliente"] = $risultati["id_row_cliente"];
            $risultato["record_fuori_progetto"] = $risultati["fuori_progetto"];
            $risultato["record_id_sys_users"] = $risultati["id_sys_users"];

        }
        echo json_encode($risultato);
    }


    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'load_table') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(2, "read")) {echo json_encode("NOT AUTHORIZED"); die();}}
        $table = "ore LEFT JOIN cliente ON cliente.id = ore.id_cliente LEFT JOIN row_cliente ON row_cliente.id = ore.id_row_cliente LEFT JOIN sys_users ON sys_users.id = ore.id_sys_users";
        $primaryKey = 'ore.id';
        $columns = array(
        array( "db" => "ore.id", "dt" => 0, "field" => "id" ), 
        array( "db" => "DATE_FORMAT(ore.giorno, '%d/%m/%Y') as giorno", "dt" => 1, "field" => "giorno" ), 
        array( "db" => "ore.ore", "dt" => 2, "field" => "ore" ), 
        array( "db" => "ore.minuti", "dt" => 3, "field" => "minuti" ), 
        array( "db" => "cliente.ragione_sociale as desc_cliente", "dt" => 4, "field" => "desc_cliente" ), 
        array( "db" => "row_cliente.progetto as desc_row_cliente", "dt" => 5, "field" => "desc_row_cliente" ), 
        array( "db" => "ore.fuori_progetto", "dt" => 6, "field" => "fuori_progetto" ), 
        array( "db" => "sys_users.firstname as desc_sys_users", "dt" => 7, "field" => "desc_sys_users" ), 
        array( "db" => "emptycolumn", "dt" => 8, "field" => "" ), 
        );

        $sql_details = array(
            "user" => $db_username,
            "pass" => $db_password,
            "db"   => $db_database,
            "host" => $db_host,
        );
        $filtergiorno_from = filter_input(INPUT_POST, "filtergiorno_from", FILTER_SANITIZE_STRING);
        $filtergiorno_to = filter_input(INPUT_POST, "filtergiorno_to", FILTER_SANITIZE_STRING);
        $filterore_from = filter_input(INPUT_POST, "filterore_from", FILTER_SANITIZE_STRING);
        $filterore_to = filter_input(INPUT_POST, "filterore_to", FILTER_SANITIZE_STRING);
        $filterminuti_from = filter_input(INPUT_POST, "filterminuti_from", FILTER_SANITIZE_STRING);
        $filterminuti_to = filter_input(INPUT_POST, "filterminuti_to", FILTER_SANITIZE_STRING);
        $filterid_cliente = filter_input(INPUT_POST, "filterid_cliente", FILTER_SANITIZE_STRING);
        $filterid_row_cliente = filter_input(INPUT_POST, "filterid_row_cliente", FILTER_SANITIZE_STRING);
        $filterfuori_progetto = filter_input(INPUT_POST, "filterfuori_progetto", FILTER_SANITIZE_STRING);
        $filterid_sys_users = filter_input(INPUT_POST, "filterid_sys_users", FILTER_SANITIZE_STRING);

        $where = " ore.id >= 0 ";
        if ($filtergiorno_from != "") { $where .= " and ore.giorno >= '" . $filtergiorno_from . "' "; $_SESSION["ore_filter_giorno_from"] = $filtergiorno_from; } else {unset($_SESSION["ore_filter_giorno_from"]); } 
        if ($filtergiorno_to   != "") { $where .= " and ore.giorno <= '" . $filtergiorno_to . "' ";   $_SESSION["ore_filter_giorno_to"] =   $filtergiorno_to; }   else {unset($_SESSION["ore_filter_giorno_to"]); } 
        if ($filterore_from  != "") { $where .= " and ore.ore >= " . str_replace(",", ".", $filterore_from) . " "; $_SESSION["ore_filter_ore_from"] = $filterore_from; } else { unset($_SESSION["ore_filter_ore_from"]); } 
        if ($filterore_to  != "") { $where .= " and ore.ore <= " . str_replace(",", ".", $filterore_to) . " "; $_SESSION["ore_filter_ore_to"] =   $filterore_to; } else {   unset($_SESSION["ore_filter_ore_to"]); } 
        if ($filterminuti_from  != "") { $where .= " and ore.minuti >= " . str_replace(",", ".", $filterminuti_from) . " "; $_SESSION["ore_filter_minuti_from"] = $filterminuti_from; } else { unset($_SESSION["ore_filter_minuti_from"]); } 
        if ($filterminuti_to  != "") { $where .= " and ore.minuti <= " . str_replace(",", ".", $filterminuti_to) . " "; $_SESSION["ore_filter_minuti_to"] =   $filterminuti_to; } else {   unset($_SESSION["ore_filter_minuti_to"]); } 
        if ($filterid_cliente  != "") { $where .= " and ore.id_cliente = " . $filterid_cliente . " "; $_SESSION["ore_filter_id_cliente"] = $filterid_cliente; } else { unset($_SESSION["ore_filter_id_cliente"]); } 
        if ($filterid_row_cliente  != "") { $where .= " and ore.id_row_cliente = " . $filterid_row_cliente . " "; $_SESSION["ore_filter_id_row_cliente"] = $filterid_row_cliente; } else { unset($_SESSION["ore_filter_id_row_cliente"]); } 
        if ($filterfuori_progetto != "") { $where .= " and ore.fuori_progetto like '%" . $filterfuori_progetto . "%' "; $_SESSION["ore_filter_fuori_progetto"] = $filterfuori_progetto; } else { unset($_SESSION["ore_filter_fuori_progetto"]); } 
        if ($filterid_sys_users  != "") { $where .= " and ore.id_sys_users = " . $filterid_sys_users . " "; $_SESSION["ore_filter_id_sys_users"] = $filterid_sys_users; } else { unset($_SESSION["ore_filter_id_sys_users"]); } 

        $fixedOrder = "";
        echo json_encode(
            SSP::complex( $_POST, $sql_details, $table, $primaryKey, $columns, $where, $fixedOrder )
        );
    }


    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'button_record') {
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $html_button = '<div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown" aria-expanded="false"><i class="icon-th-list-1"></i></a>
                                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(22px, 19px, 0px);">';
        
        $button_edit = true;
        $button_delete = true;
        if ($_SESSION["user_username"] != "sysadmin") {
            if (!checkUserRight(2, "edit")) { $button_edit = false; }
            if (!checkUserRight(2, "delete")) { $button_delete = false; }
        }
        if ($button_edit) { $html_button .= '<a href="#" onclick="edit_record('.$id.');" class="dropdown-item"><i class="icon-pencil"></i> Modifica</a>'; }        else { $html_button .= '<a href="#" onclick="edit_record('.$id.');" class="dropdown-item"><i class="icon-eye"></i> View</a>'; }

        // CHECK IF RECORD CAN BE DELETED
        $external_usage = 0;

        if ($button_delete) {
            if ($external_usage == 0) {
                $html_button .= '<a href="#" class="dropdown-item" onclick="delete_record('.filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING).');"><i class="icon-trash"></i> Elimina</a>';
            } else {
                $html_button .= '<a href="#" class="dropdown-item" style="color:#dddddd;"><i class="icon-trash"></i> Elimina</a>';
            }
        }

        $html_button .= '</div></div></div>';
        echo $html_button;
    }


    
    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'delete_record') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(2, "delete")) {echo json_encode("NOT AUTHORIZED"); die();}}
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);

        $db->query("DELETE FROM ore WHERE id = ".$id);
        echo "OK";
    }

    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'save_record') {
        
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $input_giorno = filter_input(INPUT_POST, "giorno", FILTER_SANITIZE_STRING);
        if ($input_giorno == "") { $input_giorno = null; }
        $input_ore = filter_input(INPUT_POST, "ore", FILTER_SANITIZE_STRING);
        if ($input_ore == "") { $input_ore = null; }
        $input_minuti = filter_input(INPUT_POST, "minuti", FILTER_SANITIZE_STRING);
        if ($input_minuti == "") { $input_minuti = null; }
        $input_id_cliente = filter_input(INPUT_POST, "id_cliente", FILTER_SANITIZE_STRING);
        if ($input_id_cliente == 0) { $input_id_cliente = null; }
        if ($input_id_cliente == "") { $input_id_cliente = null; }
        $input_id_row_cliente = filter_input(INPUT_POST, "id_row_cliente", FILTER_SANITIZE_STRING);
        if ($input_id_row_cliente == 0) { $input_id_row_cliente = null; }
        if ($input_id_row_cliente == "") { $input_id_row_cliente = null; }
        $input_fuori_progetto = filter_input(INPUT_POST, "fuori_progetto", FILTER_SANITIZE_STRING);
        $input_id_sys_users = $_SESSION['user_id'];
        $data = [
            "s", $input_giorno,
            "i", $input_ore,
            "i", $input_minuti,
            "i", $input_id_cliente,
            "i", $input_id_row_cliente,
            "s", $input_fuori_progetto,
            "i", $input_id_sys_users,
        ];

        // (Types: "s" = string, "i" = integer, "d" = double, "b" = blob)
        if ($id == "") {
            if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(2, "create")) {echo json_encode("NOT AUTHORIZED"); die();}}
            $sql = "INSERT INTO ore (giorno, ore, minuti, id_cliente, id_row_cliente, fuori_progetto, id_sys_users) VALUES (?, ?, ?, ?, ?, ?, ?)";
            $retv = $db->prepare_and_execute($sql, $data);
        } else {
            if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(2, "edit")) {echo json_encode("NOT AUTHORIZED"); die();}}
            array_push($data, "i", $id);
            $sql = "UPDATE ore SET giorno=?, ore=?, minuti=?, id_cliente=?, id_row_cliente=?, fuori_progetto=?, id_sys_users=? WHERE id=?";
            $retv = $db->prepare_and_execute($sql, $data);
        }
        if ($retv == 1) {

            echo "OK";
        } else {
            echo "Error: ".$retv;
        }
    }
    
    if (filter_input(INPUT_POST, "action", FILTER_SANITIZE_STRING) == "save_record_row") {
        $id = filter_input(INPUT_POST, "id", FILTER_SANITIZE_STRING);
        $data = [
        ];

        // (Types: "s" = string, "i" = integer, "d" = double, "b" = blob)
        if ($id == "") {
            if ($_SESSION["user_username"] != "sysadmin") {
                if ((!checkUserRight(2, "create")) && (!checkUserRight(2, "edit"))) {
                    echo json_encode("NOT AUTHORIZED");
                    die();
                }
            }
            $sql = "INSERT INTO ) VALUES)";
            $retv = $db->prepare_and_execute($sql, $data);
        } else {
            if ($_SESSION["user_username"] != "sysadmin") {
                if ((!checkUserRight(2, "create")) && (!checkUserRight(2, "edit"))) {
                    echo json_encode("NOT AUTHORIZED");
                    die();
                }
            }
            array_push($data, "i", $id);
            $sql = "UPDATE  SE WHERE id=?";
            $retv = $db->prepare_and_execute($sql, $data);
        }
        if ($retv == 1) {
            echo "OK";
        } else {
            echo "Error: ".$retv;
        }
    }
    if (filter_input(INPUT_POST, "action", FILTER_SANITIZE_STRING) == "progetti_di_un_cliente") {
        $id = filter_input(INPUT_POST, "id", FILTER_SANITIZE_STRING);

        $risultato = array();
        $qry = "SELECT id, progetto FROM row_cliente WHERE id_cliente = ".$id;
        $query = $db->query($qry);
        while ($risultati = mysqli_fetch_array($query))
        {
            array_push($risultato, [$risultati["id"], $risultati["progetto"]]);
        }
        echo json_encode($risultato);
    
    }
}