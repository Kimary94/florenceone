<?php

if (!isset($_SESSION)) { session_start(); }
include "sys_database.php";
include "../sys_functions.php";
include "sys_ssp.class.php";

// action: read_record        LOAD SINGLE RECORD FROM servizio FOR MODAL EDIT
// action: read_record_row    LOAD SINGLE ROWS RECORD FROM prezzoservizio FOR MODAL EDIT
// action: load_table_rows    LOAD TABLE prezzoservizio
// action: button_record      RETURN BUTTON (to put inside datatable) FOR SINGLE RECORD ON servizio
// action: button_record_row  RETURN BUTTON (to put inside datatable) FOR SINGLE RECORD ON prezzoservizio
// action: delete_record      DELETE RECORD ON servizio
// action: delete_record_row  DELETE RECORD ON prezzoservizio
// action: save_record        SAVE RECORD ON servizio
// action: save_record_row    SAVE RECORD ON prezzoservizio


if(isset($_POST['action'])) {

    
    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'read_record') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(3, "read")) {echo json_encode("NOT AUTHORIZED"); die();}}
        $id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT);
        $risultato = array();
        $qry = "SELECT * FROM servizio WHERE servizio.id = ".$id;
        $query = $db->query($qry);
        while ($risultati = mysqli_fetch_array($query))
        {
            $risultato["record_id"] = $risultati["id"];
            $risultato["record_descrizione"] = $risultati["descrizione"];
            $risultato["record_neutro"] = $risultati["neutro"];
            $risultato["record_ordine"] = $risultati["ordine"];
            $risultato["record_testo_pubblico"] = $risultati["testo_pubblico"];
        }
        echo json_encode($risultato);
    }

    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'read_record_row') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(3, "read")) {echo json_encode("NOT AUTHORIZED"); die();}}
        $qry = "SELECT * FROM prezzoservizio WHERE id = " . filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $query = $db->query($qry);
        $risultati = mysqli_fetch_array($query);
        echo json_encode($risultati);
    }

    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'load_table') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(3, "read")) {echo json_encode("NOT AUTHORIZED"); die();}}
        $table = "servizio";
        $primaryKey = 'servizio.id';
        $columns = array(
        array( "db" => "servizio.id", "dt" => 0, "field" => "id" ), 
        array( "db" => "CONVERT(servizio.descrizione USING binary) as descrizione", "dt" => 1, "field" => "descrizione" ), 
        array( "db" => "CONVERT(servizio.testo_pubblico USING binary) as testo_pubblico", "dt" => 2, "field" => "testo_pubblico" ), 
        array( "db" => "IF(servizio.neutro = 1, \"<i class='icon-check'></i>\", \"<i class='icon-check-empty'></i>\") as neutro", "dt" => 3, "field" => "neutro" ), 
        array( "db" => "servizio.ordine", "dt" => 4, "field" => "ordine" ), 
        array( "db" => "emptycolumn", "dt" => 5, "field" => "" ), 
        );
        $sql_details = array(
            "user" => $db_username,
            "pass" => $db_password,
            "db"   => $db_database,
            "host" => $db_host,
        );
        $where = " servizio.id >= 0 ";
        $fixedOrder = "";
        echo json_encode(
            SSP::complex( $_POST, $sql_details, $table, $primaryKey, $columns, $where, $fixedOrder )
        );
    }

    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'load_table_rows') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(3, "read")) {die();}}
        $table = "prezzoservizio LEFT JOIN servizio ON servizio.id = prezzoservizio.id_servizio";
        $primaryKey = "prezzoservizio.id";
        $columns = array(
            array( "db" => "prezzoservizio.id", "dt" => 0, "field" => "id" ), 
            array( "db" => "prezzoservizio.scaglione", "dt" => 1, "field" => "scaglione" ), 
            array( "db" => "REPLACE(prezzoservizio.prezzo, '.', ',') as prezzo", "dt" => 2, "field" => "prezzo" ), 
            array( "db" => "prezzoservizio.giorni", "dt" => 3, "field" => "giorni" ), 
            array( "db" => "emptycolumn", "dt" => 4, "field" => "" ),
        );

        $sql_details = array(
            "user" => $db_username,
            "pass" => $db_password,
            "db"   => $db_database,
            "host" => $db_host,
        );

        $filter_id = filter_input(INPUT_POST, "filter_id", FILTER_SANITIZE_STRING);
        $where = " prezzoservizio.id >= 0 ";
        if ($filter_id != "") { $where .= " and prezzoservizio.id_servizio = " . $filter_id . " "; } 
        
        $fixedOrder = " ORDER BY prezzoservizio.scaglione";
        echo json_encode(
            SSP::complex( $_POST, $sql_details, $table, $primaryKey, $columns, $where, $fixedOrder )
        );
    }


    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'button_record') {
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $html_button = '<div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown" aria-expanded="false"><i class="icon-th-list-1"></i></a>
                                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(22px, 19px, 0px);">';
        
        $button_edit = true;
        $button_delete = true;
        if ($_SESSION["user_username"] != "sysadmin") {
            if (!checkUserRight(3, "edit")) { $button_edit = false; }
            if (!checkUserRight(3, "delete")) { $button_delete = false; }
        }
        if ($button_edit) { $html_button .= '<a href="servizi_lavorazione_edit.php?id='.$id.'" class="dropdown-item"><i class="icon-pencil"></i> Edit</a>'; }        else { $html_button .= '<a href="servizi_lavorazione_edit.php?id='.$id.'" class="dropdown-item"><i class="icon-eye"></i> View</a>'; }

        // CHECK IF RECORD CAN BE DELETED
        $external_usage = 0;

        $query = $db->query("SELECT count(*) as conteggio FROM servizioxarticolo WHERE id_servizio = ".$id);
        while ($risultati = mysqli_fetch_array($query)) {
            if ($risultati["conteggio"] <> "") { if (intval($risultati["conteggio"]) > 0) { $external_usage += 1; }}
        }

        if ($button_delete) {
            if ($external_usage == 0) {
                $html_button .= '<a href="#" class="dropdown-item" onclick="delete_record('.filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING).');"><i class="icon-trash"></i> Delete</a>';
            } else {
                $html_button .= '<a href="#" class="dropdown-item" style="color:#dddddd;"><i class="icon-trash"></i> Delete</a>';
            }
        }

        $html_button .= '</div></div></div>';
        echo $html_button;
    }


    
    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'button_record_row') {
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $html_button = '<div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown" aria-expanded="false"><i class="icon-th-list-1"></i></a>
                                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(22px, 19px, 0px);">';
        
        $button_edit = true;
        $button_delete = true;
        if ($_SESSION["user_username"] != "sysadmin") {
            if (!checkUserRight(3, "edit")) { $button_edit = false; }
            if (!checkUserRight(3, "delete")) { $button_delete = false; }
        }
        
        if ($button_edit) {
            $html_button .= '<a href="#" onclick="edit_row('.$id.');" class="dropdown-item"><i class="icon-pencil"></i> Edit</a>';
        } else {
            $html_button .= '<a href="#" onclick="edit_row('.$id.');" class="dropdown-item"><i class="icon-eye"></i> View</a>';
        }

        // CHECK IF RECORD CAN BE DELETED
        $external_usage = 0;
        if ($button_delete) {
            if ($external_usage == 0) {
                $html_button .= '<a href="#" class="dropdown-item" onclick="delete_row('.filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING).');"><i class="icon-trash"></i>Delete</a>';
            } else {
                $html_button .= '<a href="#" class="dropdown-item" style="color:#dddddd;"><i class="icon-trash"></i>Delete</a>';
            }
        }
        
        $html_button .= "</div></div></div>";
        echo $html_button;
    }


    
    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'delete_record') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(3, "delete")) {echo json_encode("NOT AUTHORIZED"); die();}}
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $db->query("DELETE FROM prezzoservizio WHERE id_servizio = ".$id);

        $db->query("DELETE FROM servizio WHERE id = ".$id);
        echo "OK";
    }

    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'delete_record_row') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(3, "delete")) {echo json_encode("NOT AUTHORIZED"); die();}}
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $db->query("DELETE FROM prezzoservizio WHERE id = ".$id);
        echo "OK";
    }

    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'save_record') {
        
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $input_descrizione = filter_input(INPUT_POST, "descrizione", FILTER_SANITIZE_STRING);
        $input_neutro = filter_input(INPUT_POST, "neutro", FILTER_SANITIZE_STRING);
        $input_ordine = filter_input(INPUT_POST, "ordine", FILTER_SANITIZE_STRING);
        if ($input_ordine == "") { $input_ordine = null; }
        $input_testo_pubblico = filter_input(INPUT_POST, "testo_pubblico", FILTER_SANITIZE_STRING);
        $data = [
            "s", $input_descrizione,
            "i", $input_neutro,
            "i", $input_ordine,
            "s", $input_testo_pubblico,
        ];

        // (Types: "s" = string, "i" = integer, "d" = double, "b" = blob)
        if ($id == "") {
            if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(3, "create")) {echo json_encode("NOT AUTHORIZED"); die();}}
            $sql = "INSERT INTO servizio (descrizione, neutro, ordine, testo_pubblico) VALUES (?, ?, ?, ?)";
            $retv = $db->prepare_and_execute($sql, $data);
        } else {
            if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(3, "edit")) {echo json_encode("NOT AUTHORIZED"); die();}}
            array_push($data, "i", $id);
            $sql = "UPDATE servizio SET descrizione=?, neutro=?, ordine=?, testo_pubblico=? WHERE id=?";
            $retv = $db->prepare_and_execute($sql, $data);
        }
        if ($retv == 1) {

            if ($id == "") {
                $query = $db->query("SELECT id FROM servizio ORDER BY id DESC LIMIT 1");
                while ($risultati = mysqli_fetch_array($query)) { $nuovo_id = $risultati["id"]; }
                $db->query("UPDATE prezzoservizio SET id_servizio = ".$nuovo_id." WHERE id_servizio = -".$_SESSION["user_id"]);
            }

            echo "OK";
        } else {
            echo "Error: ".$retv;
        }
    }
    
    if (filter_input(INPUT_POST, "action", FILTER_SANITIZE_STRING) == "save_record_row") {
        $id = filter_input(INPUT_POST, "id", FILTER_SANITIZE_STRING);
        $input_id_servizio = filter_input(INPUT_POST, "id_servizio", FILTER_SANITIZE_STRING);
        if ($input_id_servizio == 0) { $input_id_servizio = null; }
        if ($input_id_servizio == "") { $input_id_servizio = null; }
        $input_scaglione = filter_input(INPUT_POST, "scaglione", FILTER_SANITIZE_STRING);
        if ($input_scaglione == "") { $input_scaglione = null; }
        $input_prezzo = filter_input(INPUT_POST, "prezzo", FILTER_SANITIZE_STRING);
        $input_giorni = filter_input(INPUT_POST, "giorni", FILTER_SANITIZE_STRING);
        if ($input_giorni == "") { $input_giorni = null; }
        $data = [
            "i", $input_id_servizio,
            "i", $input_scaglione,
            "d", $input_prezzo,
            "i", $input_giorni,
        ];

        // (Types: "s" = string, "i" = integer, "d" = double, "b" = blob)
        if ($id == "") {
            if ($_SESSION["user_username"] != "sysadmin") {
                if ((!checkUserRight(3, "create")) && (!checkUserRight(3, "edit"))) {
                    echo json_encode("NOT AUTHORIZED");
                    die();
                }
            }
            $sql = "INSERT INTO prezzoservizio (id_servizio, scaglione, prezzo, giorni) VALUES (?, ?, ?, ?)";
            $retv = $db->prepare_and_execute($sql, $data);
        } else {
            if ($_SESSION["user_username"] != "sysadmin") {
                if ((!checkUserRight(3, "create")) && (!checkUserRight(3, "edit"))) {
                    echo json_encode("NOT AUTHORIZED");
                    die();
                }
            }
            array_push($data, "i", $id);
            $sql = "UPDATE prezzoservizio SET id_servizio=?, scaglione=?, prezzo=?, giorni=? WHERE id=?";
            $retv = $db->prepare_and_execute($sql, $data);
        }
        if ($retv == 1) {
            echo "OK";
        } else {
            echo "Error: ".$retv;
        }
    }
}