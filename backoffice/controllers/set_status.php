<?php

if (!isset($_SESSION)) { session_start(); }
include "sys_database.php";
include "../sys_functions.php";
include "../../email_functions.php";

$campo = filter_input(INPUT_POST, 'campo', FILTER_SANITIZE_STRING);
$stato = filter_input(INPUT_POST, 'stato', FILTER_VALIDATE_INT);
$idordine = filter_input(INPUT_POST, 'idordine', FILTER_VALIDATE_INT);

$rows = fetch_rows( "select cliente.email from cliente left join ordine on ordine.id_cliente = cliente.id where ordine.id = ".$idordine );
foreach ( $rows as $row ) {
    $email_cliente = $row['email'];
}

$qry = "UPDATE ordine SET ".$campo." = ".$stato." WHERE id = ".$idordine;
$db->query($qry);

if ($stato == 5) {
    $qry = "UPDATE ordine SET tracking = '".filter_input(INPUT_POST, 'tracking', FILTER_SANITIZE_STRING)."' WHERE id = ".$idordine;
    $db->query($qry);

    $testo = "Gentile cliente,<br>vi informiamo che il vostro ordine su noistampiamo.it è stato contrassegnato come spedito.";
    if (strlen(filter_input(INPUT_POST, 'tracking', FILTER_SANITIZE_STRING)) > 3) {
        $testo .= "<br>Il codice di tracking spedizione è: ".filter_input(INPUT_POST, 'tracking', FILTER_SANITIZE_STRING);
    }
    $testo .= "<br>Grazie";

    invia_email($email_cliente, "Ordine spedito", $testo);
}

