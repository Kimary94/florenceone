<?php

if (!isset($_SESSION)) { session_start(); }
include "sys_database.php";
include "../sys_functions.php";
include "sys_ssp.class.php";

// action: read_record        LOAD SINGLE RECORD FROM slides FOR MODAL EDIT
// action: load_table         LOAD TABLE slides
// action: button_record      RETURN BUTTON (to put inside datatable) FOR SINGLE RECORD ON slides
// action: delete_record      DELETE RECORD ON slides
// action: save_record        SAVE RECORD ON slides


if(isset($_POST['action'])) {

    
    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'read_record') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(14, "read")) {echo json_encode("NOT AUTHORIZED"); die();}}
        $id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT);
        $risultato = array();
        $qry = "SELECT * FROM slides WHERE slides.id = ".$id;
        $query = $db->query($qry);
        while ($risultati = mysqli_fetch_array($query))
        {
            $risultato["record_id"] = $risultati["id"];
            $risultato["record_img_sfondo"] = $risultati["img_sfondo"];
            $risultato["record_logo"] = $risultati["logo"];
            $risultato["record_titolo"] = $risultati["titolo"];
            $risultato["record_sottotitolo"] = $risultati["sottotitolo"];
            $risultato["record_p_1_testo"] = $risultati["p_1_testo"];
            $risultato["record_p_1_link"] = $risultati["p_1_link"];
            $risultato["record_p_2_testo"] = $risultati["p_2_testo"];
            $risultato["record_p_2_link"] = $risultati["p_2_link"];
            $risultato["record_attiva"] = $risultati["attiva"];
            $risultato["record_ordine"] = $risultati["ordine"];

        }
        echo json_encode($risultato);
    }


    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'load_table') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(14, "read")) {echo json_encode("NOT AUTHORIZED"); die();}}
        $table = "slides";
        $primaryKey = 'slides.id';
        $columns = array(
        array( "db" => "slides.id", "dt" => 0, "field" => "id" ), 
        array( "db" => "CONVERT(slides.titolo USING binary) as titolo", "dt" => 1, "field" => "titolo" ), 
        array( "db" => "CONVERT(slides.sottotitolo USING binary) as sottotitolo", "dt" => 2, "field" => "sottotitolo" ), 
        array( "db" => "IF(slides.attiva = 1, \"<i class='icon-check'></i>\", \"<i class='icon-check-empty'></i>\") as attiva", "dt" => 3, "field" => "attiva" ), 
        array( "db" => "slides.ordine", "dt" => 4, "field" => "ordine" ), 
        array( "db" => "emptycolumn", "dt" => 5, "field" => "" ), 
        );

        $sql_details = array(
            "user" => $db_username,
            "pass" => $db_password,
            "db"   => $db_database,
            "host" => $db_host,
        );
        $filterimg_sfondo = filter_input(INPUT_POST, "filterimg_sfondo", FILTER_SANITIZE_STRING);
        $filterlogo = filter_input(INPUT_POST, "filterlogo", FILTER_SANITIZE_STRING);
        $filtertitolo = filter_input(INPUT_POST, "filtertitolo", FILTER_SANITIZE_STRING);
        $filtersottotitolo = filter_input(INPUT_POST, "filtersottotitolo", FILTER_SANITIZE_STRING);
        $filterp_1_testo = filter_input(INPUT_POST, "filterp_1_testo", FILTER_SANITIZE_STRING);
        $filterp_1_link = filter_input(INPUT_POST, "filterp_1_link", FILTER_SANITIZE_STRING);
        $filterp_2_testo = filter_input(INPUT_POST, "filterp_2_testo", FILTER_SANITIZE_STRING);
        $filterp_2_link = filter_input(INPUT_POST, "filterp_2_link", FILTER_SANITIZE_STRING);
        $filterattiva = filter_input(INPUT_POST, "filterattiva", FILTER_SANITIZE_STRING);
        $filterordine_from = filter_input(INPUT_POST, "filterordine_from", FILTER_SANITIZE_STRING);
        $filterordine_to = filter_input(INPUT_POST, "filterordine_to", FILTER_SANITIZE_STRING);

        $where = " slides.id >= 0 ";
        if ($filterimg_sfondo != "") { $where .= " and slides.img_sfondo like '%" . $filterimg_sfondo . "%' "; $_SESSION["slides_filter_img_sfondo"] = $filterimg_sfondo; } else { unset($_SESSION["slides_filter_img_sfondo"]); } 
        if ($filterlogo != "") { $where .= " and slides.logo like '%" . $filterlogo . "%' "; $_SESSION["slides_filter_logo"] = $filterlogo; } else { unset($_SESSION["slides_filter_logo"]); } 
        if ($filtertitolo != "") { $where .= " and slides.titolo like '%" . $filtertitolo . "%' "; $_SESSION["slides_filter_titolo"] = $filtertitolo; } else { unset($_SESSION["slides_filter_titolo"]); } 
        if ($filtersottotitolo != "") { $where .= " and slides.sottotitolo like '%" . $filtersottotitolo . "%' "; $_SESSION["slides_filter_sottotitolo"] = $filtersottotitolo; } else { unset($_SESSION["slides_filter_sottotitolo"]); } 
        if ($filterp_1_testo != "") { $where .= " and slides.p_1_testo like '%" . $filterp_1_testo . "%' "; $_SESSION["slides_filter_p_1_testo"] = $filterp_1_testo; } else { unset($_SESSION["slides_filter_p_1_testo"]); } 
        if ($filterp_1_link != "") { $where .= " and slides.p_1_link like '%" . $filterp_1_link . "%' "; $_SESSION["slides_filter_p_1_link"] = $filterp_1_link; } else { unset($_SESSION["slides_filter_p_1_link"]); } 
        if ($filterp_2_testo != "") { $where .= " and slides.p_2_testo like '%" . $filterp_2_testo . "%' "; $_SESSION["slides_filter_p_2_testo"] = $filterp_2_testo; } else { unset($_SESSION["slides_filter_p_2_testo"]); } 
        if ($filterp_2_link != "") { $where .= " and slides.p_2_link like '%" . $filterp_2_link . "%' "; $_SESSION["slides_filter_p_2_link"] = $filterp_2_link; } else { unset($_SESSION["slides_filter_p_2_link"]); } 
        if ($filterattiva == "1") { $where .= " and slides.attiva = 1; "; $_SESSION["slides_filter_attiva"] = $filterattiva; } else { unset($_SESSION["slides_filter_attiva"]); } 
        if ($filterordine_from  != "") { $where .= " and slides.ordine >= " . str_replace(",", ".", $filterordine_from) . " "; $_SESSION["slides_filter_ordine_from"] = $filterordine_from; } else { unset($_SESSION["slides_filter_ordine_from"]); } 
        if ($filterordine_to  != "") { $where .= " and slides.ordine <= " . str_replace(",", ".", $filterordine_to) . " "; $_SESSION["slides_filter_ordine_to"] =   $filterordine_to; } else {   unset($_SESSION["slides_filter_ordine_to"]); } 

        $fixedOrder = "";
        echo json_encode(
            SSP::complex( $_POST, $sql_details, $table, $primaryKey, $columns, $where, $fixedOrder )
        );
    }


    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'button_record') {
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $html_button = '<div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown" aria-expanded="false"><i class="icon-th-list-1"></i></a>
                                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(22px, 19px, 0px);">';
        
        $button_edit = true;
        $button_delete = true;
        if ($_SESSION["user_username"] != "sysadmin") {
            if (!checkUserRight(14, "edit")) { $button_edit = false; }
            if (!checkUserRight(14, "delete")) { $button_delete = false; }
        }
        if ($button_edit) { $html_button .= '<a href="#" onclick="edit_record('.$id.');" class="dropdown-item"><i class="icon-pencil"></i> Edit</a>'; }        else { $html_button .= '<a href="#" onclick="edit_record('.$id.');" class="dropdown-item"><i class="icon-eye"></i> View</a>'; }

        // CHECK IF RECORD CAN BE DELETED
        $external_usage = 0;

        if ($button_delete) {
            if ($external_usage == 0) {
                $html_button .= '<a href="#" class="dropdown-item" onclick="delete_record('.filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING).');"><i class="icon-trash"></i> Delete</a>';
            } else {
                $html_button .= '<a href="#" class="dropdown-item" style="color:#dddddd;"><i class="icon-trash"></i> Delete</a>';
            }
        }

        $html_button .= '</div></div></div>';
        echo $html_button;
    }


    
    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'delete_record') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(14, "delete")) {echo json_encode("NOT AUTHORIZED"); die();}}
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);

        $db->query("DELETE FROM slides WHERE id = ".$id);
        echo "OK";
    }

    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'save_record') {
        
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $input_FILELIST_img_sfondo = html_entity_decode(filter_input(INPUT_POST, "img_sfondo", FILTER_SANITIZE_STRING));
        $input_FILELIST_logo = html_entity_decode(filter_input(INPUT_POST, "logo", FILTER_SANITIZE_STRING));
        $input_titolo = filter_input(INPUT_POST, "titolo", FILTER_SANITIZE_STRING);
        $input_sottotitolo = filter_input(INPUT_POST, "sottotitolo", FILTER_SANITIZE_STRING);
        $input_p_1_testo = filter_input(INPUT_POST, "p_1_testo", FILTER_SANITIZE_STRING);
        $input_p_1_link = filter_input(INPUT_POST, "p_1_link", FILTER_SANITIZE_STRING);
        $input_p_2_testo = filter_input(INPUT_POST, "p_2_testo", FILTER_SANITIZE_STRING);
        $input_p_2_link = filter_input(INPUT_POST, "p_2_link", FILTER_SANITIZE_STRING);
        $input_attiva = filter_input(INPUT_POST, "attiva", FILTER_SANITIZE_STRING);
        $input_ordine = filter_input(INPUT_POST, "ordine", FILTER_SANITIZE_STRING);
        if ($input_ordine == "") { $input_ordine = null; }
        $data = [
            "s", $input_FILELIST_img_sfondo,
            "s", $input_FILELIST_logo,
            "s", $input_titolo,
            "s", $input_sottotitolo,
            "s", $input_p_1_testo,
            "s", $input_p_1_link,
            "s", $input_p_2_testo,
            "s", $input_p_2_link,
            "i", $input_attiva,
            "i", $input_ordine,
        ];

        // (Types: "s" = string, "i" = integer, "d" = double, "b" = blob)
        if ($id == "") {
            if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(14, "create")) {echo json_encode("NOT AUTHORIZED"); die();}}
            $sql = "INSERT INTO slides (img_sfondo, logo, titolo, sottotitolo, p_1_testo, p_1_link, p_2_testo, p_2_link, attiva, ordine) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            $retv = $db->prepare_and_execute($sql, $data);
        } else {
            if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(14, "edit")) {echo json_encode("NOT AUTHORIZED"); die();}}
            array_push($data, "i", $id);
            $sql = "UPDATE slides SET img_sfondo=?, logo=?, titolo=?, sottotitolo=?, p_1_testo=?, p_1_link=?, p_2_testo=?, p_2_link=?, attiva=?, ordine=? WHERE id=?";
            $retv = $db->prepare_and_execute($sql, $data);
        }
        if ($retv == 1) {

            echo "OK";
        } else {
            echo "Error: ".$retv;
        }
    }
    
    if (filter_input(INPUT_POST, "action", FILTER_SANITIZE_STRING) == "save_record_row") {
        $id = filter_input(INPUT_POST, "id", FILTER_SANITIZE_STRING);
        $data = [
        ];

        // (Types: "s" = string, "i" = integer, "d" = double, "b" = blob)
        if ($id == "") {
            if ($_SESSION["user_username"] != "sysadmin") {
                if ((!checkUserRight(14, "create")) && (!checkUserRight(14, "edit"))) {
                    echo json_encode("NOT AUTHORIZED");
                    die();
                }
            }
            $sql = "INSERT INTO ) VALUES)";
            $retv = $db->prepare_and_execute($sql, $data);
        } else {
            if ($_SESSION["user_username"] != "sysadmin") {
                if ((!checkUserRight(14, "create")) && (!checkUserRight(14, "edit"))) {
                    echo json_encode("NOT AUTHORIZED");
                    die();
                }
            }
            array_push($data, "i", $id);
            $sql = "UPDATE  SE WHERE id=?";
            $retv = $db->prepare_and_execute($sql, $data);
        }
        if ($retv == 1) {
            echo "OK";
        } else {
            echo "Error: ".$retv;
        }
    }
}