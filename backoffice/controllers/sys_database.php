<?php
if (!isset($_SESSION)) { session_start(); } 
ini_set("max_execution_time",180);
ini_set("set_time_limit",180);
$db_host = "";
$db_username = "";
$db_password = "";
$db_database = "";
if (file_exists("../sys_config.php")) { include "../sys_config.php"; }
if (file_exists("sys_config.php")) { include "sys_config.php"; }
if (file_exists("backoffice/sys_config.php")) { include "backoffice/sys_config.php"; }
if (file_exists("/backoffice/sys_config.php")) { include "/backoffice/sys_config.php"; }

	
class database {
	var $conn;

	function __construct() {
		global $db_host;
		global $db_username;
		global $db_password;
		global $db_database;
		
		$this->conn = @mysqli_connect($db_host, $db_username, $db_password);
		if ($this->conn) {
			$db_selection = mysqli_select_db( $this->conn, $db_database);
			if ($db_selection) {
				mysqli_set_charset( $this->conn, 'utf8' );

				mysqli_query($this->conn, "SET character_set_client=utf8");
				mysqli_query($this->conn, "SET character_set_connection=utf8");
				mysqli_query($this->conn, "SET character_set_results=utf8");
				mysqli_query($this->conn, "SET names utf8");

				return $this->conn;
			}
		}
	}

	public function set_charset($charset)	{
		mysqli_set_charset($this->conn, $charset);
	}

	public function query($sql) {
		$result = mysqli_query($this->conn, $sql) or die(mysqli_error($this->conn));
		return $result;
	}

	public function prepare_and_execute($sql, $data) {
		$stmt = mysqli_prepare($this->conn, $sql) or die(mysqli_error($this->conn));
		$a_params = array();
		$n = count($data) / 2;
		$param_type = "";
		for($i = 0; $i < $n; $i++) {
			$param_type .= $data[($i * 2)];
		}
		$a_params[] = & $param_type;
		for($i = 0; $i < $n; $i++) {
			$a_params[] = & $data[($i * 2) + 1];
		}
		call_user_func_array(array($stmt, 'bind_param'), $a_params);
		$result = mysqli_stmt_execute($stmt);
		return $result;
	}

	public function rows($result) {
		$rows = mysqli_num_rows($result);
		return $rows;
	}

	public function close() {
		mysqli_close($this->conn);
	}

	public function status(){
		if (!$this->conn) return false;
		return mysqli_ping($this->conn);
	}

	public function fetchArray ($sql, $data) {

		$stmt = mysqli_prepare($this->conn, $sql) or die(mysqli_error($this->conn));
		$a_params = array();
		$n = count($data) / 2;
		$param_type = "";
		for($i = 0; $i < $n; $i++) {
			$param_type .= $data[($i * 2)];
		}
		$a_params[] = & $param_type;
		for($i = 0; $i < $n; $i++) {
			$a_params[] = & $data[($i * 2) + 1];
		}
		call_user_func_array(array($stmt, 'bind_param'), $a_params);
		mysqli_stmt_execute($stmt);
		

		$data = mysqli_stmt_result_metadata($stmt);
		$fields = array();
		$out = array();

		$fields[0] = &$stmt;
		$count = 1;

		while($field = mysqli_fetch_field($data)) {
			$fields[$count] = &$out[$field->name];
			$count++;
		}
	
		call_user_func_array('mysqli_stmt_bind_result', $fields);
		//mysqli_stmt_fetch($stmt);

		$rows = array();
		while( mysqli_stmt_fetch($stmt)) {
			$rows[] = $out;
		}
		return $rows;

		//return (count($out) == 0) ? false : $out;
	
	}
	 

}

if (($db_host != "") && ($db_username != "") && ($db_password != "") && ($db_database != "")) {
	$db = New database();
}

// Return array results from mysql query
function fetch_rows($sql) {
	global $db_host;
	global $db_username;
	global $db_password;
	global $db_database;
	global $db;
	$result = mysqli_query($db->conn, $sql) or die(mysqli_error($db));
	return mysqli_fetch_all( $result, MYSQLI_BOTH );
}
?>