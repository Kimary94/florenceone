<?php
    $uploadsFolder = 'uploads';
    
    if (!empty($_FILES)) {
        $targetPath_pre = dirname( __FILE__ ) . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR;
        $targetPath_post = $uploadsFolder . DIRECTORY_SEPARATOR;
        $targetPath = $targetPath_pre . $targetPath_post;
        if (!file_exists($targetPath)) {
            if (!mkdir($targetPath, 0777, true)) {
                http_response_code (500);
                header( 'Content-Type: application/json; charset=utf-8' );
                echo json_encode("Failed to create folders structure: ".$targetPath_post);
                die();
            }
        }
        $tempFile = $_FILES['file']['tmp_name'];
        $dot_position = strrpos($_FILES['file']['name'], ".");
        if ($dot_position === false) {
            $new_file_name =  $_FILES['file']['name'] . "_" . time() . "_"; 
        } else {
            $new_file_name =  substr($_FILES['file']['name'], 0, $dot_position) . "_" . time() . "_" . substr($_FILES['file']['name'], $dot_position);
        }
        $targetFile = $targetPath . $new_file_name;
        move_uploaded_file($tempFile, $targetFile);
        echo $targetPath_post . $new_file_name;
        //echo $targetPath;
    }
?>