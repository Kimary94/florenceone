<?php

if (!isset($_SESSION)) { session_start(); }
include "sys_database.php";
include "../sys_functions.php";
include "sys_ssp.class.php";

// action: load_table         LOAD TABLE sys_groups
// action: button_record      RETURN BUTTON (to put inside datatable) FOR SINGLE RECORD ON sys_groups
// action: delete_record      DELETE RECORD ON sys_groups
// action: save_record        SAVE RECORD ON sys_groups
// action: save_right         SAVE RECORD ON sys_rights

// CONTROLLER RESTRICTED TO sysadmin
if (!isset($_SESSION['user_username'])) { die(); }
if (strtolower($_SESSION['user_username']) != 'sysadmin') { die(); }


if(isset($_POST['action'])) {

    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'load_table') {
        $table = 'sys_groups';
        $primaryKey = 'sys_groups.id';
        $columns = array(
        array( 'db' => 'sys_groups.id', 'dt' => 0, 'field' => 'id' ), 
        array( 'db' => 'sys_groups.group_name', 'dt' => 1, 'field' => 'group_name' ), 
        array( 'db' => 'emptycolumn', 'dt' => 2, 'field' => '' ), 
        );
        $sql_details = array(
            'user' => $db_username,
            'pass' => $db_password,
            'db'   => $db_database,
            'host' => $db_host,
        );
        $where = " sys_groups.id >= 0 ";
        $fixedOrder = "";
        echo json_encode(
            SSP::complex( $_POST, $sql_details, $table, $primaryKey, $columns, $where, $fixedOrder )
        );
    }



    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'button_record') {
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $html_button  = '<div class="list-icons">
                                    <div class="dropdown">
                                        <a href="#" class="list-icons-item" data-toggle="dropdown" aria-expanded="false">
                                            <i class="icon-th-list-1"></i>
                                        </a>

                                        <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(22px, 19px, 0px);">
                                            <a href="sys_groups_edit.php?id='.$id.'" class="dropdown-item"><i class="icon-pencil"></i> Edit</a>';
        
        $external_usage = 0;
        // QUERY TO CHECK IF RECORD CAN BE DELETED
        $query = $db->query("SELECT count(*) as conteggio FROM sys_users WHERE id_sys_groups = ".$id);
        while ($risultati = mysqli_fetch_array($query)) {
            if ($risultati["conteggio"] <> "") { if (intval($risultati["conteggio"]) > 0) { $external_usage += 1; }}
        }

        if ($external_usage == 0) {
            $html_button .= '<a href="#" class="dropdown-item" onclick="delete_record('.filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING).');"><i class="icon-trash"></i> Delete</a>';
        } else {
            $html_button .= '<a href="#" class="dropdown-item" style="color:#dddddd;"><i class="icon-trash"></i> Delete</a>';
        }

        $html_button .= '</div></div></div>';
        echo $html_button;
    }


    
    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'delete_record') {
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $db->query("DELETE FROM sys_rights WHERE id_sys_groups = ".$id);
        $db->query("DELETE FROM sys_groups WHERE id = ".$id);
        echo "OK";
    }



    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'save_record') {
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $input_group_name = filter_input(INPUT_POST, 'group_name', FILTER_SANITIZE_STRING);
        $data = ['s', $input_group_name];
        if ($id == '') {
            $sql = 'INSERT INTO sys_groups (group_name) VALUES (?)';
            $retv = $db->prepare_and_execute($sql, $data);
        } else {
            array_push($data, 'i', $id);
            $sql = 'UPDATE sys_groups SET group_name=? WHERE id=?';
            $retv = $db->prepare_and_execute($sql, $data);
        }
        if ($retv == 1) {
                if ($id == "") {
                    $query = $db->query("SELECT id FROM sys_groups ORDER BY id DESC LIMIT 1");
                    while ($risultati = mysqli_fetch_array($query)) { $id = $risultati["id"]; }
                }
            echo $id;
        } else {
            echo "KO";
        }
    }
    
    

    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'save_right') {
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $group_id = filter_input(INPUT_POST, 'group_id', FILTER_SANITIZE_STRING);
        $group_right = filter_input(INPUT_POST, 'group_right', FILTER_SANITIZE_STRING); // can_read can_edit....
        $id_page = filter_input(INPUT_POST, 'id_page', FILTER_SANITIZE_STRING);
        $id_sys_rights = filter_input(INPUT_POST, 'id_sys_rights', FILTER_SANITIZE_STRING); //id tabella sys_rights (0 se nuovo record)
        $checked = filter_input(INPUT_POST, 'checked', FILTER_SANITIZE_STRING);
        if ($checked == 'true') { $checked = 1; } else { $checked = 0; }

        if (intval($id_sys_rights) == 0) {
            $query = $db->query("SELECT id FROM sys_rights WHERE id_sys_pages = ".$id_page." AND id_sys_groups = ".$group_id);
            $rows = array();
            while($risultati = mysqli_fetch_array($query)) {
                $id_sys_rights = $risultati['id'];
            }
            if (intval($id_sys_rights) == 0) {
                $data = [
                    'i', $group_id,
                    'i', $id_page
                ];
                $sql = 'INSERT INTO sys_rights (id_sys_groups, id_sys_pages) VALUES (?, ?)';
                $retv = $db->prepare_and_execute($sql, $data);
                $query = $db->query("SELECT id FROM sys_rights WHERE id_sys_pages = ".$id_page." AND id_sys_groups = ".$group_id);
                $rows = array();
                while($risultati = mysqli_fetch_array($query)) {
                    $id_sys_rights = $risultati['id'];
                }
            }
        }

        if (intval($id_sys_rights) == 0) {
            echo "Error";
        } else {
            $data = [
                'i', $checked,
                'i', $id_sys_rights
            ];
            $sql = 'UPDATE sys_rights SET '.$group_right.'=? WHERE id=?';
            $retv = $db->prepare_and_execute($sql, $data);
        }

        if ($retv == 1) {
            echo "OK";
        } else {
            echo "Error: ".$retv;
        }
    }

}
?>
