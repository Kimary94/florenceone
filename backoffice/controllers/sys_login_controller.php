<?php
include "sys_database.php";

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
require '../libs/PHPMailer/src/Exception.php';
require '../libs/PHPMailer/src/PHPMailer.php';
require '../libs/PHPMailer/src/SMTP.php';

function generateRandomString($length) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
  }

  

if(isset($_POST['action'])) {
    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'login') {

        if (!$db->status()) {
            echo json_encode("Error: No database connection");
            die();
        }
        
        $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
        $password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);

        $ret = 0;
        $qry = "SELECT * FROM sys_users WHERE username = '" . $username . "' and password = '" . md5($password) . "' ORDER BY id desc LIMIT 1";
        $rows = fetch_rows($qry);
        foreach ( $rows as $row ) {
            $ret = 1;
            $_SESSION['user_id']        = $row['id'];
            $_SESSION['user_firstname'] = $row['firstname'];
            $_SESSION['user_lastname']  = $row['lastname'];
            $_SESSION['user_username']  = $row['username'];
            $_SESSION['user_email']     = $row['email'];
            $_SESSION['user_id_group']  = $row['id_sys_groups'];
            $_SESSION['user_ip']        = $_SERVER['REMOTE_ADDR'];
        }
        echo json_encode($ret);
    }

    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'logout') {
        session_destroy();
        echo json_encode("OK");
    }
    
    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'send_reset_link') {
        $ret = 'OK';
        echo json_encode($ret);
    }
    
    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'save_password') {
        if (!$db->status()) {
            echo json_encode("Error: No database connection");
            die();
        }
        $reset_token = filter_input(INPUT_POST, 'reset_token', FILTER_SANITIZE_STRING);
        $new_password = filter_input(INPUT_POST, 'new_password', FILTER_SANITIZE_STRING);
        if (strlen($new_password) <= 7) {
            echo json_encode("Error: Password too short");
            die();
        }
        // SECURITY ADVICE: do not check the validity of the reset token
        $db->query("UPDATE sys_users SET password = '".md5($new_password)."', reset_token = null WHERE reset_token = '".$reset_token."'");
        echo json_encode("Your password has been successfully changed");
    }

}
$db->close();
?>