<?php
if (!isset($_SESSION)) { session_start(); }
include "sys_database.php";
include "../sys_functions.php";
$crlf = chr(10);


// CONTROLLER RESTRICTED TO sysadmin
if (!isset($_SESSION['user_username'])) { die(); }
if (strtolower($_SESSION['user_username']) != 'sysadmin') { die(); }

?>

<?php if (isset($_GET['page_id'])) { ?>
    <div class="modal-content">
        <div class="modal-header bg-primary">
            <h6 class="modal-title"><span style="font-variant: small-caps; font-size: 15px;"><i class="icon-table mr-2"></i>Table manager</span></h6>
        </div>
        <div class="modal-body" id="step1">
            <div class="content" style="-webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none;" unselectable="on">
                <h3 style="font-variant: small-caps;">Select the table to manage:</h3>
                
                <?php
                $tablename = "";
                if ($db->status()) {
                    $qry = "SELECT obj_value FROM sys_module_settings WHERE id_page = ".$_GET['page_id']." AND obj_name = 'table'";
                    $rows = fetch_rows($qry);
                    foreach ( $rows as $row ) {
                        $tablename = $row['obj_value'];
                    }
                }
                ?>

                <select id="select_table" class="form-control">
                <?php
                    if ($db->status()) {
                        $qry = "show tables";
                        $rows = fetch_rows($qry);
                        $file_name = "";
                        foreach ( $rows as $row ) {
                            if (substr(strtolower($row[0]), 0, 4) != "sys_") {
                                $selected = "";
                                if ($row[0] == $tablename) $selected = "SELECTED";
                                echo "<option value='".$row[0]."' ".$selected.">".$row[0]."</option>";
                            }
                        }
                    }
                ?>
                </select>
            </div>
        </div>
        <div class="modal-body" id="step2" style="display:none;">
            <div class="content" style="-webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none;" unselectable="on">
                <h3 style="font-variant: small-caps;">Table rows</h3>
                <div id="tableRowsYes" style="font-size: 14px;">
                    <select id="table_rows" class="form-control">
                    <option value="TEST">RIGA DI TEST</option>
                    </select>
                </div>
                <div id="tableRowsNo" style="font-size: 14px;">
                    No table rows found.<br><br>
                    <i class="icon-info-1"></i>
                    <i>Table rows must contain a column named <b>id_<label class="name_table_selected"></label></b> to manage records connected with a record of <b><label class="name_table_selected"></label></b>.</i>
                </div>
            </div>
        </div>
        <div class="modal-body" id="step3" style="display:none;">
            <div class="content" style="-webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none;" unselectable="on">
                <h3 style="font-variant: small-caps;">Table check</h3>
                <div id="tableCheck" style="font-size: 14px;">
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button onclick="next1();"                          class="btn btn-lg btn-outline-success" id="button_step1"><i class="icon-angle-right mr-2"></i>Next</button>
            <button onclick="next2();"    style="display:none;" class="btn btn-lg btn-outline-success" id="button_step2"><i class="icon-angle-right mr-2"></i>Next</button>
            <button onclick="generate();" style="display:none;" class="btn btn-lg btn-outline-success" id="button_step3"><i class="icon-magic mr-2"></i>Generate</button>
            <button onclick="$('#modal_module').modal('hide');" class="btn btn-lg btn-outline-warning"><i class="icon-undo mr-2"></i>Cancel</button>
        </div>
    </div>
    <script>

        function next1(){
            if ($('#select_table').val().length > 0) {
                showOverlay();
                $.ajax({
                    url: "<?=linkto(getPageName())?>",
                    type: "POST",
                    data: {
                        'action': 'save_table',
                        'id_page': <?=$_GET['page_id']?>,
                        'table': $('#select_table').val()
                    },
                    success: function (data, stato) {
                        hideOverlay();
                        var data = $.parseJSON(data);
                        if (data[0] == "KO") {
                            message_box("Error", data[1], "Ok", "error");
                        } else {
                            $('.name_table_selected').html( $('#select_table').val() );
                            
                            $("#table_rows").empty();

                            var o = new Option("No table rows connected", "");
                            $(o).html("No table rows connected");
                            $("#table_rows").append(o);

                            if (data[1].length > 0) {

                                data[1].forEach(function(element) {
                                    var o = new Option(element, element);
                                    $(o).html(element);
                                    $("#table_rows").append(o);
                                });

                                if (data[2].length > 0) {
                                    $("#table_rows").val(data[2]);
                                }

                                $('#tableRowsNo').hide()
                                $('#tableRowsYes').show()
                            } else {
                                $('#tableRowsYes').hide()
                                $('#tableRowsNo').show()
                            }

                            $('#step1').hide()
                            $('#step2').show()
                            $('#button_step1').hide()
                            $('#button_step2').show()
                        }
                    },
                    error: function (data) {
                        hideOverlay();
                        message_box("Error", "Oops! Qualcosa è andato storto.", "Ok", "error");
                    }
                });
            }
        }

        function next2(){
            showOverlay();
            $.ajax({
                url: "<?=linkto(getPageName())?>",
                type: "POST",
                data: {
                    'action': 'save_table_rows',
                    'id_page': <?=$_GET['page_id']?>,
                    'table': $('#table_rows').val()
                },
                success: function (data, stato) {
                    hideOverlay();
                    var data = $.parseJSON(data);
                    if (data[0] == "KO") {
                        message_box("Error", data[1], "Ok", "error");
                    } else {
                        $('#step2').hide()
                        $('#step3').show()
                        $('#button_step2').hide()
                        $('#tableCheck').html("<center><i class='icon-warning-empty' style='font-size: 24px;'></i></center><br><br>".concat(data[1]));
                        if (data[1] == "") {
                            $('#tableCheck').html("<center><i class='icon-thumbs-up-5' style='font-size: 24px;'></i><br><br>Everything is OK</center>");
                            $('#button_step3').show()
                        }
                    }
                },
                error: function (data) {
                    hideOverlay();
                    message_box("Error", "Oops! Qualcosa è andato storto.", "Ok", "error");
                }
            });
        }

        function generate(){
            showOverlay();
            $.ajax({
                url: "<?=linkto(getPageName())?>",
                type: "POST",
                data: {
                    'action': 'generate',
                    'id_page': <?=$_GET['page_id']?>
                },
                success: function (data, stato) {
                    hideOverlay();
                    var data = $.parseJSON(data);
                    if (data[0] == "KO") {
                        message_box("Error", data[1], "Ok", "error");
                    } else {
                        message_box("Completed", data[1], "Ok", "success", function_hide_modal);
                    }
                },
                error: function (data) {
                    hideOverlay();
                    message_box("Error", "Oops! Qualcosa è andato storto.", "Ok", "error");
                }
            });
        }
        function function_hide_modal(){
            $('#modal_module').modal('hide');
        }
    </script>
<?php
}

if (isset($_POST['action'])) {

    if ($_POST['action'] == "save_table") {
        if ($db->status()) {
            global $db_database;
            $id_page = $_POST['id_page'];
            $table_selected = $_POST['table'];
            // SAVE SELECTED TABLE IN sys_module_settings
            $id_sys_module_settings = 0;
            $qry = "SELECT id FROM sys_module_settings WHERE id_page = ".$id_page." AND obj_name = 'table'";
            $rows = fetch_rows($qry);
            $file_name = "";
            foreach ( $rows as $row ) {
                $id_sys_module_settings = $row['id'];
            }
            if ($id_sys_module_settings == 0) {
                $db->query( "INSERT INTO sys_module_settings(id_page, obj_name, obj_value) VALUES (".$id_page.", 'table', '".$table_selected."')" );
            } else {
                $db->query( "UPDATE sys_module_settings SET obj_value = '".$table_selected."' WHERE id = ".$id_sys_module_settings );
            }

            // RETRIEVE ALL TABLES WITH COLUMN id_SELECTEDTABLE
            $available_table_rows = array();
            $query_available_table_rows = "SELECT table_name FROM information_schema.columns WHERE column_name = 'id_".$table_selected."' and table_schema = '".$db_database."' ORDER BY ordinal_position";
            $rows = fetch_rows($query_available_table_rows);
            foreach ( $rows as $row ) {
                array_push($available_table_rows, $row['table_name']);
            }

            // RETRIEVE PREVIOUS SELECTED TABLE ROWS
            $preselected_table_rows = "";
            $qry = "SELECT obj_value FROM sys_module_settings WHERE id_page = ".$id_page." AND obj_name = 'table_rows'";
            $rows = fetch_rows($qry);
            foreach ( $rows as $row ) {
                $preselected_table_rows = $row['obj_value'];
            }

            $ret = ["OK", $available_table_rows, $preselected_table_rows];
        } else {
            $ret = ["KO", "No connection to Database"];
        }
        echo json_encode($ret);
    }


    if ($_POST['action'] == "save_table_rows") {
        if ($db->status()) {
            global $db_database;
            $id_page = $_POST['id_page'];
            
            // RETRIEVE PREVIOUS SELECTED MAIN TABLE
            $table_selected = "";
            $qry = "SELECT obj_value FROM sys_module_settings WHERE id_page = ".$id_page." AND obj_name = 'table'";
            $rows = fetch_rows($qry);
            foreach ( $rows as $row ) {
                $table_selected = $row['obj_value'];
            }

            // SAVE SELECTED TABLE ROWS IN sys_module_settings
            $table_rows_selected = $_POST['table'];
            $id_sys_module_settings = 0;
            $qry = "SELECT id FROM sys_module_settings WHERE id_page = ".$id_page." AND obj_name = 'table_rows'";
            $rows = fetch_rows($qry);
            $file_name = "";
            foreach ( $rows as $row ) {
                $id_sys_module_settings = $row['id'];
            }
            if ($id_sys_module_settings == 0) {
                $db->query( "INSERT INTO sys_module_settings(id_page, obj_name, obj_value) VALUES (".$id_page.", 'table_rows', '".$table_rows_selected."')" );
            } else {
                $db->query( "UPDATE sys_module_settings SET obj_value = '".$table_rows_selected."' WHERE id = ".$id_sys_module_settings );
            }


            /********************************
                CHECK TABLE & TABLE ROWS
            ********************************/
            
            // MAIN TABLE INFORMATION SCHEMA
            $query_fields = "SELECT column_name, data_type, column_key, extra FROM information_schema.columns WHERE table_name = '".$table_selected."' and table_schema = '".$db_database."' ORDER BY ordinal_position";
            
            // TABLE ROWS INFORMATION SCHEMA (if selected)
            if ($table_rows_selected != "") {
                $query_fields_rows = "SELECT column_name, data_type, column_key, extra FROM information_schema.columns WHERE table_name = '".$table_rows_selected."' and table_schema = '".$db_database."' ORDER BY ordinal_position";
            } else {
                $query_fields_rows = "";
            }
            
            $table_check = "";

            // SPACES IN MAIN TABLE NAME
            if (strpos($table_selected, " ") !== false) {
                $table_check = 'Table <b>'.$table_selected.'</b>: table name must not contain spaces. Please rename to <b>'.str_replace(" ", "_", $table_selected).'</b> or <b>'.str_replace(" ", "", $table_selected).'</b>.';
            }
            
            // COLUMN id IN MAIN TABLE
            if ($table_check == "") {
                $query = $db->query($query_fields." LIMIT 1");
                while ($risultati = mysqli_fetch_array($query)) {
                    if ($risultati['column_name'] != "id") {
                        $table_check = 'Table <b>'.$table_selected.'</b>: first column must be <b>id</id> (Integer, Primary key, Auto-increment).';
                    } else {
                        if ($risultati['data_type'] != "int") {
                            $table_check = 'Table <b>'.$table_selected.'</b>: first column must be <b>id</id> (Integer, Primary key, Auto-increment).';
                        } else {
                            if ($risultati['column_key'] != "PRI") {
                                $table_check = 'Table <b>'.$table_selected.'</b>: first column must be <b>id</id> (Integer, Primary key, Auto-increment).';
                            } else {
                                if ($risultati['extra'] != "auto_increment") {
                                    $table_check = 'Table <b>'.$table_selected.'</b>: first column must be <b>id</id> (Integer, Primary key, Auto-increment).';
                                }
                            }
                        }
                    }
                }
            }

            // SPACES IN COLUMNS OF MAIN TABLE
            // TABLE CHECK FOR EVERY id_ COLUMNS OF MAIN TABLE
            if ($table_check == "") {
                $query = $db->query($query_fields);
                while ($risultati = mysqli_fetch_array($query)) {
                    if (strpos($risultati['column_name'], " ") !== false) {
                        $table_check .= 'Table <b>'.$table_selected.'</b>, column <b>'.$risultati['column_name'].'</b>: column name must not contain spaces. Please rename to <b>'.str_replace(" ", "_", $risultati['column_name']).'</b> or <b>'.str_replace(" ", "", $risultati['column_name']).'</b>.<br>';
                    } else {
                        if (substr($risultati['column_name'], 0, 3) == "id_") {
                            if ($risultati['data_type'] != "int") {
                                $table_check .= 'Table <b>'.$table_selected.'</b>, column <b>'.$risultati['column_name'].'</b>: this column must be an Integer.<br>';
                            } else {
                                $connected_table_found = false;
                                $connected_table_id_found = false;
                                $connected_table_description_found = false;
                                $query2 = $db->query("SELECT ordinal_position, column_name, data_type FROM information_schema.columns WHERE table_name = '".substr($risultati["column_name"], 3)."' and table_schema = '".$db_database."' ORDER BY ordinal_position LIMIT 2");
                                while ($risultati2 = mysqli_fetch_array($query2)) {
                                    $connected_table_found = true;
                                    if ($risultati2['ordinal_position'] == 1) {
                                        $connected_table_id_found = true;
                                        if ($risultati2['column_name'] != "id") {
                                            $table_check .= 'Table <b>'.substr($risultati['column_name'],3).'</b>: first column must be <b>id</b>.<br>';
                                        } else {
                                            if ($risultati2['data_type'] != "int") {
                                                $table_check .= 'Table <b>'.substr($risultati['column_name'],3).'</b>, column <b>id</b>: this column must be an Integer.<br>';
                                            }
                                        }
                                    }
                                    if ($risultati2['ordinal_position'] == 2) {
                                        $connected_table_description_found = true;
                                        if (strpos($risultati2['column_name'], " ") !== false) {
                                            $table_check .= 'Table <b>'.substr($risultati["column_name"], 3).'</b>, column <b>'.$risultati2['column_name'].'</b>: column name must not contain spaces. Please rename to <b>'.str_replace(" ", "_", $risultati2['column_name']).'</b> or <b>'.str_replace(" ", "", $risultati2['column_name']).'</b>.<br>';
                                        }
                                    }
                                }
                                if (!$connected_table_found) {
                                    $table_check .= 'Table <b>'.substr($risultati['column_name'],3).'</b>, column <b>'.$risultati['column_name'].'</b>: there must be a table named <b>'.substr($risultati['column_name'],3).'</b> but is not present in your database.<br>';
                                } else {
                                    if (!$connected_table_id_found) { $table_check .= 'Table <b>'.substr($risultati['column_name'],3).'</b>: first column must be <b>id</b>.<br>'; }
                                    if (!$connected_table_description_found) { $table_check .= 'Table <b>'.substr($risultati['column_name'],3).'</b>: second column must be a description field.<br>'; }
                                }
                            }
                        } else {
                            if (($risultati['data_type'] == "linestring") || ($risultati['data_type'] == "multilinestring") || ($risultati['data_type'] == "binary") || ($risultati['data_type'] == "varbinary") || ($risultati['data_type'] == "tinyblob") || ($risultati['data_type'] == "point") || ($risultati['data_type'] == "json") || ($risultati['data_type'] == "year") || ($risultati['data_type'] == "timestamp") || ($risultati['data_type'] == "datetime") || ($risultati['data_type'] == "set") || ($risultati['data_type'] == "enum") || ($risultati['data_type'] == "polygon") || ($risultati['data_type'] == "multipoint") || ($risultati['data_type'] == "multipolygon") || ($risultati['data_type'] == "geometry") || ($risultati['data_type'] == "geometrycollection")) {
                                $table_check .= 'Table <b>'.$table_selected.'</b>, column <b>'.$risultati['column_name'].'</b>: data type <b>'.$risultati['data_type'].'</b> can\'t be managed by this module.<br>';
                            }
                            if (($risultati['data_type'] == "float") || ($risultati['data_type'] == "double") || ($risultati['data_type'] == "real")) {
                                $table_check .= 'Table <b>'.$table_selected.'</b>, column <b>'.$risultati['column_name'].'</b>: data type <b>'.$risultati['data_type'].'</b> represent approximate numeric data values and can\'t be managed by this module. Please convert to <b>decimal</b> data type.<br>';
                            }
                        }
                    }
                }
            }



            if ($query_fields_rows != "") {
                // COLUMN id IN TABLE ROWS
                $table_rows_found = false;
                if ($table_check == "") {
                    $query = $db->query($query_fields_rows." LIMIT 1");
                    while ($risultati = mysqli_fetch_array($query)) {
                        $table_rows_found = true;
                        while ($risultati = mysqli_fetch_array($query)) {
                            if ($risultati['column_name'] != "id") {
                                $table_check = 'Table <b>'.$table_rows_selected.'</b>: first column must be <b>id</id> (Integer, Primary key, Auto-increment).';
                            } else {
                                if ($risultati['data_type'] != "int") {
                                    $table_check = 'Table <b>'.$table_rows_selected.'</b>: first column must be <b>id</id> (Integer, Primary key, Auto-increment).';
                                } else {
                                    if ($risultati['column_key'] != "PRI") {
                                        $table_check = 'Table <b>'.$table_rows_selected.'</b>: first column must be <b>id</id> (Integer, Primary key, Auto-increment).';
                                    } else {
                                        if ($risultati['extra'] != "auto_increment") {
                                            $table_check = 'Table <b>'.$table_rows_selected.'</b>: first column must be <b>id</id> (Integer, Primary key, Auto-increment).';
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                // SPACES IN COLUMNS OF TABLE ROWS
                // TABLE CHECK FOR EVERY id_ COLUMNS OF TABLE ROWS
                if (($table_check == "") && ($table_rows_found)) {
                    $query = $db->query($query_fields_rows);
                    while ($risultati = mysqli_fetch_array($query)) {
                        if (strpos($risultati['column_name'], " ") !== false) {
                            $table_check .= 'Table <b>'.$table_rows_selected.'</b>, column <b>'.$risultati['column_name'].'</b>: column name must not contain spaces. Please rename to <b>'.str_replace(" ", "_", $risultati['column_name']).'</b> or <b>'.str_replace(" ", "", $risultati['column_name']).'</b>.<br>';
                        } else {
                            if (substr($risultati['column_name'], 0, 3) == "id_") {
                                if ($risultati['data_type'] != "int") {
                                    $table_check .= 'Table <b>'.$table_rows_selected.'</b>, column <b>'.$risultati['column_name'].'</b>: this column must be an Integer.<br>';
                                } else {

                                    $connected_table_found = false;
                                    $connected_table_id_found = false;
                                    $connected_table_description_found = false;
                                    $query2 = $db->query("SELECT ordinal_position, column_name, data_type FROM information_schema.columns WHERE table_name = '".substr($risultati["column_name"], 3)."' and table_schema = '".$db_database."' ORDER BY ordinal_position LIMIT 2");
                                    while ($risultati2 = mysqli_fetch_array($query2)) {
                                        $connected_table_found = true;
                                        if ($risultati2['ordinal_position'] == 1) {
                                            $connected_table_id_found = true;
                                            if ($risultati2['column_name'] != "id") {
                                                $table_check .= 'Table <b>'.substr($risultati['column_name'],3).'</b>: first column must be <b>id</b>.<br>';
                                            } else {
                                                if ($risultati2['data_type'] != "int") {
                                                    $table_check .= 'Table <b>'.substr($risultati['column_name'],3).'</b>, column <b>id</b>: this column must be an Integer.<br>';
                                                }
                                            }
                                        }
                                        if ($risultati2['ordinal_position'] == 2) {
                                            $connected_table_description_found = true;
                                            if (strpos($risultati2['column_name'], " ") !== false) {
                                                $table_check .= 'Table <b>'.substr($risultati["column_name"], 3).'</b>, column <b>'.$risultati2['column_name'].'</b>: column name must not contain spaces. Please rename to <b>'.str_replace(" ", "_", $risultati2['column_name']).'</b> or <b>'.str_replace(" ", "", $risultati2['column_name']).'</b>.<br>';
                                            }
                                        }
                                    }
                                    if (!$connected_table_found) {
                                        $table_check .= 'Table <b>'.substr($risultati['column_name'],3).'</b>, column <b>'.$risultati['column_name'].'</b>: there must be a table named <b>'.substr($risultati['column_name'],3).'</b> but is not present in your database.<br>';
                                    } else {
                                        if (!$connected_table_id_found) { $table_check .= 'Table <b>'.substr($risultati['column_name'],3).'</b>: first column must be <b>id</b>.<br>'; }
                                        if (!$connected_table_description_found) { $table_check .= 'Table <b>'.substr($risultati['column_name'],3).'</b>: second column must be a description field.<br>'; }
                                    }
                                }
                            } else {
                                if (($risultati['data_type'] == "linestring") || ($risultati['data_type'] == "multilinestring") || ($risultati['data_type'] == "binary") || ($risultati['data_type'] == "varbinary") || ($risultati['data_type'] == "tinyblob") || ($risultati['data_type'] == "point") || ($risultati['data_type'] == "json") || ($risultati['data_type'] == "year") || ($risultati['data_type'] == "timestamp") || ($risultati['data_type'] == "datetime") || ($risultati['data_type'] == "set") || ($risultati['data_type'] == "enum") || ($risultati['data_type'] == "polygon") || ($risultati['data_type'] == "multipoint") || ($risultati['data_type'] == "multipolygon") || ($risultati['data_type'] == "geometry") || ($risultati['data_type'] == "geometrycollection")) {
                                    $table_check .= 'Table <b>'.$table_rows_selected.'</b>, column <b>'.$risultati['column_name'].'</b>: data type <b>'.$risultati['data_type'].'</b> can\'t be managed by this module.<br>';
                                }
                                if (($risultati['data_type'] == "float") || ($risultati['data_type'] == "double") || ($risultati['data_type'] == "real")) {
                                    $table_check .= 'Table <b>'.$table_rows_selected.'</b>, column <b>'.$risultati['column_name'].'</b>: data type <b>'.$risultati['data_type'].'</b> represent approximate numeric data values and can\'t be managed by this module. Please convert to <b>decimal</b> data type.<br>';
                                }
                            }
                        }
                    }
                }
            }


            $ret = ["OK", $table_check];
            
        } else {
            $ret = ["KO", "No connection to Database"];
        }
        echo json_encode($ret);
    }


    if ($_POST['action'] == "generate") {
        if ($db->status()) {
            $id_page = $_POST['id_page'];
            $tablename = "";
            $tablename_rows = "";
            $page_name = "";
            $icon = "";
            $file_name = "";
            $info_execution = "";
            $info_execution_prefix = "";
            $error_execution = "";

            $qry = "SELECT obj_value FROM sys_module_settings WHERE id_page = ".$id_page." AND obj_name = 'table'";
            $rows = fetch_rows($qry);
            foreach ( $rows as $row ) {
                $tablename = $row['obj_value'];
            }
            
            $qry = "SELECT obj_value FROM sys_module_settings WHERE id_page = ".$id_page." AND obj_name = 'table_rows'";
            $rows = fetch_rows($qry);
            foreach ( $rows as $row ) {
                $tablename_rows = $row['obj_value'];
            }

            $qry = "SELECT * FROM sys_pages WHERE id = ".$id_page;
            $rows = fetch_rows($qry);
            foreach ( $rows as $row ) {
                $page_name = $row['page_name'];
                $icon = $row['icon'];
                $file_name = $row['file_name'].".php";
            }
            
            if (($file_name == "") || ($tablename == "")) {
                $ret = [0, "Record not found or filename not set"];
            } else {
            
                $file_name_edit = str_replace(".php", "_edit.php", $file_name);
                $file_name_controller = str_replace(".php", "_controller.php", $file_name);

                $suffix_name_backup = ".BACKUP_".time();
                $previous_files_exist = 0;
                if (file_exists("../".$file_name))      { $previous_files_exist += 1; rename("../".$file_name,      "../backups/".$file_name.$suffix_name_backup); }
                if (file_exists("../".$file_name_edit)) { $previous_files_exist += 1; rename("../".$file_name_edit, "../backups/".$file_name_edit.$suffix_name_backup); }
                if (file_exists($file_name_controller)) { $previous_files_exist += 1; rename($file_name_controller, "../backups/".$file_name_controller.$suffix_name_backup); }
                if ($previous_files_exist > 0) { $info_execution_prefix .= '<div>Previous files have now suffix <span style="font-weight: bold;">'.$suffix_name_backup.'</span></div><div>&nbsp;</div>'; }

                global $db_database;

                $query_fields = "SELECT column_name, data_type, numeric_scale, CASE WHEN is_nullable = 'YES' THEN 1 ELSE 0 END as nullable, character_maximum_length FROM information_schema.columns WHERE table_name = '".$tablename."' and table_schema = '".$db_database."' ORDER BY ordinal_position";
                $query_fields_rows = "SELECT column_name, data_type, numeric_scale, CASE WHEN is_nullable = 'YES' THEN 1 ELSE 0 END as nullable, character_maximum_length FROM information_schema.columns WHERE table_name = '".$tablename_rows."' and table_schema = '".$db_database."' ORDER BY ordinal_position";

                
                $settings_decimal_separator = 0;
                $settings_date_format = 0;
                if (isset($db)) {
                    if ($db->status()) {
                        $qry = "SELECT * FROM sys_settings WHERE id = 1";
                        $rows = fetch_rows($qry);
                        foreach ( $rows as $row ) {
                            $settings_decimal_separator = $row['decimal_separator'];
                            $settings_date_format = $row['date_format'];
                        }
                    }
                }
                $date_format = "%Y-%m-%d";
                if ($settings_date_format == 1) { $date_format = "%d/%m/%Y"; }
                if ($settings_date_format == 2) { $date_format = "%m/%d/%Y"; }


/******************************************************************************
*******************************************************************************
*******************************************************************************
*******************************************************************************
GENERATING FIRST FILE: view
*******************************************************************************
*******************************************************************************
*******************************************************************************
******************************************************************************/

$data = '<?php
    include "sys_header.php";
    if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight('.$id_page.', "read")) {header("location: ".linkto("sys_dashboard")); die();}}
?>

<body class="navbar-top<?php if ($settings_menu_closed == 1) { echo \' sidebar-xs\'; } ?>">
    <div id="imagePreviewDiv" class="modal modal-xl fade imgpreview" style="z-index: 9999;"></div>
    <?php include "sys_navbar.php"; ?>
    <div class="page-content">
        <?php
        include "sys_sidebar.php";
        if ($_SESSION["user_username"] != "sysadmin") {
            if (!checkUserRight('.$id_page.', "create")) {echo "<script>can_create = false;</script>";} else {echo "<script>can_create = true;</script>";}
            if (!checkUserRight('.$id_page.', "edit")) {echo "<script>can_edit = false;</script>";} else {echo "<script>can_edit = true;</script>";}
        } else {
            echo "<script>can_create = true; can_edit = true;</script>";
        }
        ?>
        <div class="content-wrapper">

        
        <!-- Page header -->
        <div class="page-header page-header-light">
            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">

                <div class="d-flex">
                    <i class="'.$icon.' mr-2 breadcrumb-item"></i><span style="margin-top: 8px;">'.$page_name.'</span>
                </div>

                <div class="header-elements d-none" style="display: flex !important;">
                    <div class="breadcrumb justify-content-center">
                        <?php
                        $show_button_new = true;
                        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight('.$id_page.', "create")) { $show_button_new = false; }}
                        if ($show_button_new) { ?>'.$crlf;
                        if ($tablename_rows == "") {
                            $data .= '                            <a href="#" class="breadcrumb-elements-item" onclick="new_record();">';
                        } else {
                            $data .= '                            <a href="'.$file_name_edit.'" class="breadcrumb-elements-item">';
                        }
                        $data .='
                                <i class="icon-plus-squared"></i>
                                New record
                            </a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>


<div class="content unselectable">
    <div class="card card-collapsed" style="background-color: #f9f9f9;">
        <div class="card-header header-elements-inline" onmouseup="$(\'#searchexpand\').click();" style="font-variant: all-petite-caps;">
            <div><i class="icon-search-7"></i> Filtri di ricerca</div>
            <div data-action="collapse" id="searchexpand" style="display:none;"></div>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-lg-10 col-md-9 col-sm-12 col-xs-12">
                    <div class="row">'.$crlf.$crlf;



$query2 = $db->query($query_fields);
while ($risultati2 = mysqli_fetch_array($query2))
{
    if ($risultati2['column_name'] != 'id') {
        if (substr($risultati2['column_name'], 0, 3) == "id_") {

            $description_field = "";
            $query_campo_descrittivo = "SELECT column_name FROM information_schema.columns WHERE ordinal_position > 1 AND table_name = '".substr($risultati2["column_name"], 3)."' AND table_schema = '".$db_database."' ORDER BY ordinal_position ASC LIMIT 1 ";
            $query4 = $db->query($query_campo_descrittivo);
            while ($risultati4 = mysqli_fetch_array($query4)) {
                $description_field = $risultati4['column_name'];
            }

            $data .= '
                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">'.str_replace("_", " ", ucfirst(substr($risultati2['column_name'], 3))).'</label>
                        <select class="form-control select" id="'.$risultati2['column_name'].'" >
                        <option value=""></option>
                        <?php
                            $selected_id = "";
                            if (isset($_SESSION["'.$tablename.'_filter_'.$risultati2["column_name"].'"])) {
                                if (intval($_SESSION["'.$tablename.'_filter_'.$risultati2["column_name"].'"]) > 0) {
                                    $selected_id = $_SESSION["'.$tablename.'_filter_'.$risultati2["column_name"].'"];
                                }
                            }
                            $query = $db->query("SELECT id, '.$description_field.' FROM '.substr($risultati2["column_name"], 3).' ORDER BY '.$description_field.'");
                            while($risultati = mysqli_fetch_array($query)) {
                                if ($selected_id == $risultati["id"]) { $selected = " SELECTED "; } else { $selected = " "; }
                                echo \'<option value="\'.$risultati["id"].\'"\'.$selected.\'>\'.$risultati["'.$description_field.'"].\'</option>\';
                            }
                        ?>
                        </select>
                    </div></div>'.$crlf.$crlf;
            
        } else {
            
            $data .= '                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">'.$crlf;
        
            if ($risultati2['data_type'] == 'date') {
                $data .= '                        <label class="text-bold">From '.str_replace("_", " ", $risultati2['column_name']).'</label>'.$crlf;
                $data .= '                        <input type="date"   class="form-control" autocomplete="off" id="'.$risultati2['column_name'].'_from" style="text-align:center;" id="'.$risultati2['column_name'].'_from"  value="<?php if (isset($_SESSION[\''.$tablename.'_filter_'.$risultati2['column_name'].'_from\'])) { echo $_SESSION[\''.$tablename.'_filter_'.$risultati2['column_name'].'_from\']; } ?>">'.$crlf;
            } elseif (($risultati2['data_type'] == 'int') || ($risultati2['data_type'] == 'smallint') || ($risultati2['data_type'] == 'mediumint') || ($risultati2['data_type'] == 'bigint')) {
                $data .= '                        <label class="text-bold">From '.str_replace("_", " ", $risultati2['column_name']).'</label>'.$crlf;
                $data .= '                        <input type="number" class="form-control" autocomplete="off" id="'.$risultati2['column_name'].'_from" onkeypress="return checkInteger(event)" style="text-align:right;"  step="1"  id="'.$risultati2['column_name'].'_from"  value="<?php if (isset($_SESSION[\''.$tablename.'_filter_'.$risultati2['column_name'].'_from\'])) { echo $_SESSION[\''.$tablename.'_filter_'.$risultati2['column_name'].'_from\']; } ?>">'.$crlf;
            } elseif ($risultati2['data_type'] == 'decimal') {
                if ($risultati2['numeric_scale'] == 0) { $step = "1"; } else { $step = "0.".str_pad("1",$risultati2['numeric_scale'],"0",STR_PAD_LEFT); }
                $data .= '                        <label class="text-bold">From '.str_replace("_", " ", $risultati2['column_name']).'</label>'.$crlf;
                $data .= '                        <input type="number" class="form-control" autocomplete="off" id="'.$risultati2['column_name'].'_from" onkeypress="return checkFloat(event)" style="text-align:right;"  step="'.$step.'"  id="'.$risultati2['column_name'].'_from"  value="<?php if (isset($_SESSION[\''.$tablename.'_filter_'.$risultati2['column_name'].'_from\'])) { echo $_SESSION[\''.$tablename.'_filter_'.$risultati2['column_name'].'_from\']; } ?>">'.$crlf;
            } elseif (($risultati2['data_type'] == 'varchar') || ($risultati2['data_type'] == 'char') || ($risultati2['data_type'] == 'text') || ($risultati2['data_type'] == 'tinytext') || ($risultati2['data_type'] == 'mediumtext') || ($risultati2['data_type'] == 'longtext') || ($risultati2['data_type'] == 'blob') || ($risultati2['data_type'] == 'mediumblob') || ($risultati2['data_type'] == 'longblob')) {
                $data .= '                        <label class="text-bold">'.str_replace("_", " ", ucfirst($risultati2['column_name'])).'</label>'.$crlf;
                $data .= '                        <input type="text"   class="form-control" autocomplete="off" id="'.$risultati2['column_name'].'" value="<?php if (isset($_SESSION[\''.$tablename.'_filter_'.$risultati2['column_name'].'\'])) { echo $_SESSION[\''.$tablename.'_filter_'.$risultati2['column_name'].'\']; } ?>">'.$crlf;
            } elseif (($risultati2['data_type'] == 'tinyint') || ($risultati2['data_type'] == 'bit')) {
                $data .= '                        <label class="text-bold" style="display: inherit;">'.str_replace("_", " ", ucfirst($risultati2['column_name'])).'</label><br>'.$crlf;
                $data .= '                        <input type="checkbox"  class="form-control" id="'.$risultati2['column_name'].'" value="1" <?php if (isset($_SESSION[\''.$tablename.'_filter_'.$risultati2['column_name'].'\'])) { if ($_SESSION[\''.$tablename.'_filter_'.$risultati2['column_name'].'\'] == 1) { echo " CHECKED "; } } ?> >'.$crlf;
            } elseif ($risultati2['data_type'] == 'time') {
                $data .= '                        <label class="text-bold" style="display: inherit;">From '.str_replace("_", " ", ucfirst($risultati2['column_name'])).'</label>'.$crlf;
                $data .= '                        <input type="time"   class="form-control" autocomplete="off" id="'.$risultati2['column_name'].'_from" style="text-align:center;" id="'.$risultati2['column_name'].'_from"  value="<?php if (isset($_SESSION[\''.$tablename.'_filter_'.$risultati2['column_name'].'_from\'])) { echo $_SESSION[\''.$tablename.'_filter_'.$risultati2['column_name'].'_from\']; } ?>">'.$crlf;
            }
            $data .= '                    </div></div>'.$crlf.$crlf;

            if (($risultati2['data_type'] == 'date') || ($risultati2['data_type'] == 'int') || ($risultati2['data_type'] == 'smallint') || ($risultati2['data_type'] == 'mediumint') || ($risultati2['data_type'] == 'bigint') || ($risultati2['data_type'] == 'decimal') || ($risultati2['data_type'] == 'time')) {
                $data .= '                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">'.$crlf;
                if ($risultati2['data_type'] == 'date') {
                    $data .= '                        <label class="text-bold">To '.str_replace("_", " ", $risultati2['column_name']).'</label>'.$crlf;
                    $data .= '                        <input type="date"   class="form-control" autocomplete="off" id="'.$risultati2['column_name'].'_to" style="text-align:center;" id="'.$risultati2['column_name'].'_to"  value="<?php if (isset($_SESSION[\''.$tablename.'_filter_'.$risultati2['column_name'].'_to\'])) { echo $_SESSION[\''.$tablename.'_filter_'.$risultati2['column_name'].'_to\']; } ?>">'.$crlf;
                } elseif (($risultati2['data_type'] == 'int') || ($risultati2['data_type'] == 'smallint') || ($risultati2['data_type'] == 'mediumint') || ($risultati2['data_type'] == 'bigint')) {
                    $data .= '                        <label class="text-bold">To '.str_replace("_", " ", $risultati2['column_name']).'</label>'.$crlf;
                    $data .= '                        <input type="number" class="form-control" autocomplete="off" id="'.$risultati2['column_name'].'_to" onkeypress="return checkInteger(event)" style="text-align:right;"  step="1"  id="'.$risultati2['column_name'].'_to"  value="<?php if (isset($_SESSION[\''.$tablename.'_filter_'.$risultati2['column_name'].'_to\'])) { echo $_SESSION[\''.$tablename.'_filter_'.$risultati2['column_name'].'_to\']; } ?>">'.$crlf;
                } elseif ($risultati2['data_type'] == 'decimal') {
                    if ($risultati2['numeric_scale'] == 0) { $step = "1"; } else { $step = "0.".str_pad("1",$risultati2['numeric_scale'],"0",STR_PAD_LEFT); }
                    $data .= '                        <label class="text-bold">To '.str_replace("_", " ", $risultati2['column_name']).'</label>'.$crlf;
                    $data .= '                        <input type="number" class="form-control" autocomplete="off" id="'.$risultati2['column_name'].'_to" onkeypress="return checkFloat(event)" style="text-align:right;"  step="'.$step.'"  id="'.$risultati2['column_name'].'_to"  value="<?php if (isset($_SESSION[\''.$tablename.'_filter_'.$risultati2['column_name'].'_to\'])) { echo $_SESSION[\''.$tablename.'_filter_'.$risultati2['column_name'].'_to\']; } ?>">'.$crlf;
                } elseif ($risultati2['data_type'] == 'time') {
                    $data .= '                        <label class="text-bold">To '.str_replace("_", " ", $risultati2['column_name']).'</label>'.$crlf;
                    $data .= '                        <input type="time"   class="form-control" autocomplete="off" id="'.$risultati2['column_name'].'_to" style="text-align:center;" id="'.$risultati2['column_name'].'_to"  value="<?php if (isset($_SESSION[\''.$tablename.'_filter_'.$risultati2['column_name'].'_to\'])) { echo $_SESSION[\''.$tablename.'_filter_'.$risultati2['column_name'].'_to\']; } ?>">'.$crlf;
                }
                $data .= '                    </div></div>'.$crlf.$crlf;
            }
            
        }
    }
}


$data .= '
                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="text-center">
                                <button onclick="event.preventDefault(); applyFilters();"
                                        style="width: 150px; padding: 10px; border-radius: 5px;"
                                        class="btn btn-sm btn-outline-info">
                                            <i class="icon-search-1" style="float: left; margin-top: 2px;"></i> Search
                                </button><br>
                                <button onclick="event.preventDefault(); resetFilters();"
                                        style="width: 150px; padding: 10px; border-radius: 5px; margin: 3px 0px 3px 0px;"
                                        class="btn btn-sm btn-outline-warning">
                                            <i class="icon-cancel-alt" style="float: left; margin-top: 2px;"></i> Reset
                                </button><br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="card">
    <table id="myDatatable" class="display table table-striped datatable-responsive" style="width:100%; overflow: hidden;">
        <thead>
            <tr>'.$crlf;



$nr_column_action = 0;
$query2 = $db->query($query_fields);
while ($risultati2 = mysqli_fetch_array($query2)) {
    if (($risultati2['data_type'] != "blob") && ($risultati2['data_type'] != "mediumblob") && ($risultati2['data_type'] != "longblob")) {
        if (substr($risultati2['column_name'], 0, 3) == "id_") {
            $data .= '                <th><b>'.str_replace("_", " ", ucfirst(substr($risultati2['column_name'], 3))).'</b></th>'.$crlf;
        } else {
            $data .= '                <th><b>'.str_replace("_", " ", ucfirst($risultati2['column_name'])).'</b></th>'.$crlf;
        }
        $nr_column_action += 1;
    }
}

$data .= '
                <th><b>Actions</b></th>
            </tr>
        </thead>
        <tfoot>
            <tr>'.$crlf;



$query2 = $db->query($query_fields);
while ($risultati2 = mysqli_fetch_array($query2)) {
    if (($risultati2['data_type'] != "blob") && ($risultati2['data_type'] != "mediumblob") && ($risultati2['data_type'] != "longblob")) {
        if (substr($risultati2['column_name'], 0, 3) == "id_") {
            $data .= '                <th><b>'.str_replace("_", " ", ucfirst(substr($risultati2['column_name'], 3))).'</b></th>'.$crlf;
        } else {
            $data .= '                <th><b>'.str_replace("_", " ", ucfirst($risultati2['column_name'])).'</b></th>'.$crlf;
        }
    }
}


$data .= '
                <th><b>Actions</b></th>
            </tr>
        </tfoot>
    </table>
</div>'.$crlf;



if ($tablename_rows == "") {
$data .= '
<div id="modal_edit" class="modal fade" style="-webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none;" unselectable="on">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<h6 class="modal-title"><i class="'.$icon.'"></i> <span id="modal_mode"></span></h6>
			</div>
            <div class="modal-body">
                <div class="content">
                        
                    <input type="hidden" id="record_id">';


$query2 = $db->query($query_fields);
while ($risultati2 = mysqli_fetch_array($query2))
{
    if ($risultati2['column_name'] != 'id') {
        if (substr($risultati2['column_name'], 0, 3) == "id_") {
            
            $description_field = "";
            $query_campo_descrittivo = "SELECT column_name FROM information_schema.columns WHERE ordinal_position > 1 AND table_name = '".substr($risultati2["column_name"], 3)."' AND table_schema = '".$db_database."' ORDER BY ordinal_position ASC LIMIT 1 ";
            $query4 = $db->query($query_campo_descrittivo);
            while ($risultati4 = mysqli_fetch_array($query4)) {
                $description_field = $risultati4['column_name'];
            }

            $data .= '
            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">'.str_replace("_", " ", ucfirst(substr($risultati2['column_name'], 3))).'</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <select class="form-control select" id="record_'.$risultati2['column_name'].'" '.(($risultati2['nullable'] == 0) ? "required" : "").' >
                    <option value=""></option>
                    <?php
                        $query = $db->query("SELECT id, '.$description_field.' FROM '.substr($risultati2["column_name"], 3).' ORDER BY '.$description_field.'");
                        while($risultati = mysqli_fetch_array($query)) {
                            echo \'<option value="\'.$risultati["id"].\'">\'.$risultati["'.$description_field.'"].\'</option>\';
                        }
                    ?>
                    </select>
                </div>
            </div>'.$crlf;
        } elseif ($risultati2['data_type'] == 'date') {
            $data .= '
            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">'.str_replace("_", " ", ucfirst($risultati2['column_name'])).'</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <input type="date" style="text-align:center; width:170px;" id="record_'.$risultati2['column_name'].'" autocomplete="off" class="form-control" '.(($risultati2['nullable'] == 0) ? "required" : "").' >
                </div>
            </div>'.$crlf;
        } elseif ($risultati2['data_type'] == 'time') {
            $data .= '
            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">'.str_replace("_", " ", ucfirst($risultati2['column_name'])).'</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <input type="time" style="text-align:center; width:95px;" id="record_'.$risultati2['column_name'].'" autocomplete="off" class="form-control" '.(($risultati2['nullable'] == 0) ? "required" : "").' >
                </div>
            </div>'.$crlf;
        } elseif (($risultati2['data_type'] == 'int') || ($risultati2['data_type'] == 'smallint') || ($risultati2['data_type'] == 'mediumint') || ($risultati2['data_type'] == 'bigint')) {
            $data .= '
            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">'.str_replace("_", " ", ucfirst($risultati2['column_name'])).'</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <input type="number" style="text-align:center; width:170px;" step="1" id="record_'.$risultati2['column_name'].'" onkeypress="return checkInteger(event)" autocomplete="off" class="form-control" '.(($risultati2['nullable'] == 0) ? "required" : "").' >
                </div>
            </div>'.$crlf;
        } elseif ($risultati2['data_type'] == 'decimal') {
            if ($risultati2['numeric_scale'] == 0) { $step = "1"; } else { $step = "0.".str_pad("1",$risultati2['numeric_scale'],"0",STR_PAD_LEFT); }
            $data .= '
            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">'.str_replace("_", " ", ucfirst($risultati2['column_name'])).'</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <input type="number" style="text-align:right; width:170px;" step="'.$step.'" id="record_'.$risultati2['column_name'].'"  onkeypress="return checkFloat(event)" autocomplete="off" class="form-control" '.(($risultati2['nullable'] == 0) ? "required" : "").' >
                </div>
            </div>'.$crlf;
        } elseif (($risultati2['data_type'] == 'tinyint') || ($risultati2['data_type'] == 'bit')) {
            $data .= '
            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 0px 0px 0px 0px;">'.str_replace("_", " ", ucfirst($risultati2['column_name'])).'</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <input type="checkbox" id="record_'.$risultati2['column_name'].'" class="form-control" value="1"  style="width: 28px; margin: 0;" >
                </div>
            </div>'.$crlf;
        } elseif (($risultati2['data_type'] == 'blob') || ($risultati2['data_type'] == "mediumblob") || ($risultati2['data_type'] == "longblob")) {
            $data .= '
            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">'.str_replace("_", " ", ucfirst($risultati2['column_name'])).'</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <div class="row">
                        <div class="col-lg-12 dropzonerow">
                            <form class="dropzone" id="dropzone_'.$risultati2['column_name'].'"></form>
                        </div>
                        <div class="col-lg-12" id="fileContainer_'.$risultati2['column_name'].'"></div>    
                    </div>
                </div>
            </div>'.$crlf;

        } else {
            $data .= '
            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">'.str_replace("_", " ", ucfirst($risultati2['column_name'])).'</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <input type="text" id="record_'.$risultati2['column_name'].'" maxlength="'.$risultati2['character_maximum_length'].'" autocomplete="off" class="form-control" >
                </div>
            </div>'.$crlf;
        }
        
    }
}

$data .= '
                </div>
            </div>
			<div class="modal-footer">
                <button onclick="save_record();" class="btn btn-lg btn-outline-success savebutton"><i class="icon-ok-3"></i> Save</button>
                <button onclick="$(\'#modal_edit\').modal(\'hide\');" class="btn btn-lg btn-outline-warning"><i class="icon-undo"></i> Cancel</button>
			</div>
		</div>
	</div>
</div>'.$crlf;
}


$data .= '

</div>
</div>
</div>


<script>'.$crlf;

if ($tablename_rows == "") {

$query2 = $db->query($query_fields);
while ($risultati2 = mysqli_fetch_array($query2)) {
    if (($risultati2['data_type'] == 'blob') || ($risultati2['data_type'] == "mediumblob") || ($risultati2['data_type'] == "longblob")) {
        $data .= '    var FILELIST_'.$risultati2['column_name'].' = new Array;'.$crlf;
    }
}    


$data .= '    
    function new_record() {
        $(\'#modal_mode\').html(\'New ' . ucfirst($tablename) . '\');'.$crlf;

$set_focus_first_field = "";
$query2 = $db->query($query_fields);
while ($risultati2 = mysqli_fetch_array($query2))
{
    if (substr($risultati2['column_name'], 0, 3) == "id_") {
        $data .= '        $("#record_'.$risultati2['column_name'].'").val(0).trigger("change");'.$crlf;
        if ($set_focus_first_field == "") {
            $set_focus_first_field = '        setTimeout(function(){ $("#record_'.$risultati2['column_name'].'").focus(); }, 300);'.$crlf;
        }
    } elseif (($risultati2['data_type'] == 'tinyint') || ($risultati2['data_type'] == 'bit')) {
        $data .= '        if (ischecked("#record_'.$risultati2['column_name'].'")) {'.$crlf;
        $data .= '            $("#record_'.$risultati2['column_name'].'").click();'.$crlf;
        $data .= '        }'.$crlf;
    } elseif (($risultati2['data_type'] == 'blob') || ($risultati2['data_type'] == "mediumblob") || ($risultati2['data_type'] == "longblob")) {
        $data .= $crlf;
        $data .= '        document.querySelector("#dropzone_'.$risultati2['column_name'].'").dropzone.removeAllFiles();'.$crlf;
        $data .= '        FILELIST_'.$risultati2['column_name'].' = new Array;'.$crlf;
        $data .= '        $("#fileContainer_'.$risultati2['column_name'].'").html("");'.$crlf.$crlf;
    } else {
        $data .= '        $("#record_'.$risultati2['column_name'].'").val("");'.$crlf;
        if ($set_focus_first_field == "") {
            if ($risultati2['column_name'] != 'id') {
                $set_focus_first_field = '        setTimeout(function(){ $("#record_'.$risultati2['column_name'].'").focus(); }, 300);'.$crlf;
            }
        }
    }
}
$data .= $set_focus_first_field;
$data .= '        if (can_create) { $(".savebutton").show(); $(".dropzone").show(); $(".icon-trash-empty").show(); } else { $(".savebutton").hide();  $(".dropzone").hide(); $(".icon-trash-empty").hide(); }';

$data .= '
        $(\'#modal_edit\').modal({backdrop: \'static\'});
    }

    function edit_record(id) {
        if (can_edit) { $(\'#modal_mode\').html(\'Edit '.ucfirst($tablename).'\'); } else { $(\'#modal_mode\').html(\'View '.ucfirst($tablename).'\'); }
        $.ajax({
            async: false,
            type: "POST",
            url: "controllers/' . $file_name_controller . '",
            data: {
                "action": "read_record",
                "id": id
            },
            success: function (data, stato) {
                var data = $.parseJSON(data);
                if (data == "NOT AUTHORIZED") { return; }'.$crlf;


$data .= $crlf;
$query2 = $db->query($query_fields);
while ($risultati2 = mysqli_fetch_array($query2))
{
    if (substr($risultati2['column_name'], 0, 3) == "id_") {
        $data .= '                $("#record_'.$risultati2['column_name'].'").val(data["record_'.$risultati2['column_name'].'"]).trigger("change");'.$crlf;
    } elseif (($risultati2['data_type'] == 'tinyint') || ($risultati2['data_type'] == 'bit')) { 
        $data .= '
            if (data["record_'.$risultati2['column_name'].'"] == 1) {
                if (!ischecked("#record_'.$risultati2['column_name'].'")) {
                    $("#record_'.$risultati2['column_name'].'").click();
                }
            } else {
                if (ischecked("#record_'.$risultati2['column_name'].'")) {
                    $("#record_'.$risultati2['column_name'].'").click();
                }
            }'.$crlf;
    } elseif (($risultati2['data_type'] == 'blob') || ($risultati2['data_type'] == "mediumblob") || ($risultati2['data_type'] == "longblob")) { 
        $data .= '
                $("#fileContainer_'.$risultati2['column_name'].'").html("");
                document.querySelector("#dropzone_'.$risultati2['column_name'].'").dropzone.removeAllFiles();
                FILELIST_'.$risultati2['column_name'].' = new Array;
                if (data.record_'.$risultati2['column_name'].' !== null) {
                    if (data.record_'.$risultati2['column_name'].'.length > 0) {
                        FILELIST_'.$risultati2['column_name'].' = JSON.parse(data.record_'.$risultati2['column_name'].');
                        if (FILELIST_'.$risultati2['column_name'].' === null) {
                            FILELIST_'.$risultati2['column_name'].' = new Array;
                        } else {
                            drawFileList(FILELIST_'.$risultati2['column_name'].', "FILELIST_'.$risultati2['column_name'].'", "fileContainer_'.$risultati2['column_name'].'");
                        }
                    }
                }'.$crlf;
    } else {
        $data .= '                $("#record_'.$risultati2['column_name'].'").val(data["record_'.$risultati2['column_name'].'"]);'.$crlf;
    }
    
}
$data .= '        '.$set_focus_first_field;
$data .= '                if (can_edit) { $(".savebutton").show(); $(".dropzone").show(); $(".icon-trash-empty").show(); } else { $(".savebutton").hide(); $(".dropzone").hide(); $(".icon-trash-empty").hide(); }';


$data .= '
                $(\'#modal_edit\').modal({backdrop: \'static\'});
            }
        });
    }
    
    function save_record() {
        var requiredfields = "";
        $(".modal-body :required").each(function(){
            if (($(this).val() == "") || ($(this).val() === null)) {
                if (requiredfields != "") { requiredfields = requiredfields.concat(", "); }
                var tmp_req_field = $(this).attr("id").replace("record_", "").replace("_", " ");
                if (tmp_req_field.substr(0, 3) == "id ") { tmp_req_field = tmp_req_field.replace("id ", ""); }
                requiredfields = requiredfields.concat(tmp_req_field);
            }
        })
        if (requiredfields != "") { toast("Required fields: ".concat(requiredfields), "error"); return; }

        $.ajax({
            async: false,
            type: "POST",
            url: "controllers/' . $file_name_controller . '",
            data: {
                "action": "save_record",'.$crlf;



$query2 = $db->query($query_fields);
while ($risultati2 = mysqli_fetch_array($query2))
{
    if (($risultati2['data_type'] == 'tinyint') || ($risultati2['data_type'] == 'bit')) {
        $data .= '                "'.$risultati2['column_name'].'": ischecked("#record_'.$risultati2['column_name'].'"), '.$crlf;
    } elseif (($risultati2['data_type'] == 'blob') || ($risultati2['data_type'] == "mediumblob") || ($risultati2['data_type'] == "longblob")) {
        $data .= '                "'.$risultati2['column_name'].'": JSON.stringify(FILELIST_'.$risultati2['column_name'].'),'.$crlf;
    } else {
        $data .= '                "'.$risultati2['column_name'].'": $("#record_'.$risultati2['column_name'].'").val(), '.$crlf;
    }
}


$data .= '
            },
            success: function (data, stato) {
                if (data != "OK") {
                    swal({
                        title: "Error",
                        text: "An error occurred during Save: " + data.toString(),
                        confirmButtonColor: "#66BB6A",
                        type: "warning"
                    });
                } else {
                    $(\'#modal_edit\').modal(\'hide\');
                    update_table();
                }
            }
        });
    }'.$crlf;
}


$data .= '
    function update_table()  {
        table = $(\'#myDatatable\').DataTable();
        table.destroy();
        $("#myDatatable > tbody").html("");
        load_table();
        table = $(\'#myDatatable\').DataTable();
        table.page(0).draw(false);
    }

    $(document).ready(function () {

        $(\'.select\').select2({ width: \'100%\' });'.$crlf;



if ($tablename_rows == "") {
$query2 = $db->query($query_fields);
while ($risultati2 = mysqli_fetch_array($query2)) {
    if (($risultati2['data_type'] == 'blob') || ($risultati2['data_type'] == "mediumblob") || ($risultati2['data_type'] == "longblob")) {
        $data .= '
        new Dropzone("#dropzone_'.$risultati2['column_name'].'", {
            init: function() {
                this.on("success", function(file, serverFileName) {
                    FILELIST_'.$risultati2['column_name'].'.push({"serverFileName" : serverFileName, "fileName" : file.name});
                    drawFileList(FILELIST_'.$risultati2['column_name'].', "FILELIST_'.$risultati2['column_name'].'", "fileContainer_'.$risultati2['column_name'].'");
                });
                this.on("error", function(file, errorMessage) {
                    message_box("Error", errorMessage, "Ok", "error");
                });
            },
            url: "controllers/sys_file_uploader.php" });'.$crlf;
    }
}
}


$data .= '
        load_table();
        table = $(\'#myDatatable\').DataTable();
        table.on(\'draw\', function () {
            table.rows().every(function (rowIdx, tableLoop, rowLoop) {
                var values_in_row = this.data();
                $.ajax({
                    async: false,
                    type: "POST",
                    url: "controllers/' . $file_name_controller . '",
                    data: {
                        "action": "button_record",
                        "id": values_in_row[0]
                    },
                    success: function (data, stato) {
                        values_in_row[' . $nr_column_action . '] = data;
                    }
                });
                this.data(values_in_row);
            });
        });
    });


    function delete_record(id) {
        swal({
            title: "Sei sicuro?",
            text: "I dati non potranno essere recuperati",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: "Si, elimina!",
            cancelButtonText: "No, annulla!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    async: true,
                    type: "POST",
                    url : "controllers/' . $file_name_controller . '",
                    data: {
                        "action": "delete_record",
                        "id": id
                    },
                    success: function (data) {
                        if (data === "OK") {
                            swal({
                                title: "Eliminato!",
                                text: "Il record è stato cancellato",
                                confirmButtonColor: "#66BB6A",
                                type: "success"
                            }, function () {
                                document.location.reload();
                            });
                        } else {
                            swal({
                                title: "Operazione annullata",
                                text: "Qualcosa è andato storto",
                                confirmButtonColor: "#2196F3",
                                type: "error"
                            });
                        }
                    },
                    error: function (data) {
                        swal({
                            title: "Operazione annullata",
                            text: "Qualcosa è andato storto",
                            confirmButtonColor: "#2196F3",
                            type: "error"
                        });
                    }
                });
            }
            else {
                swal({
                    title: "Operazione annullata",
                    text: "",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
    }


    function load_table() {'.$crlf;

$query2 = $db->query($query_fields);
while ($risultati2 = mysqli_fetch_array($query2)) {
    if ($risultati2['column_name'] != 'id') {

        if (substr($risultati2['column_name'], 0, 3) == 'id_') {

            $data .= '
        filter'.$risultati2['column_name'].' = "";
        if ($("#'.$risultati2['column_name'].'").val() !== null) { filter'.$risultati2['column_name'].' = $("#'.$risultati2['column_name'].'").val(); }'.$crlf;

        } elseif (($risultati2['data_type'] == 'date') || ($risultati2['data_type'] == 'int') || ($risultati2['data_type'] == 'smallint') || ($risultati2['data_type'] == 'mediumint') || ($risultati2['data_type'] == 'bigint') || ($risultati2['data_type'] == 'decimal') || ($risultati2['data_type'] == 'time')) {

            $data .= '
        filter'.$risultati2['column_name'].'_from = "";
        if (parseInt($("#'.$risultati2['column_name'].'_from").val().length) > 0) { filter'.$risultati2['column_name'].'_from = $("#'.$risultati2['column_name'].'_from").val(); }
        filter'.$risultati2['column_name'].'_to = "";
        if (parseInt($("#'.$risultati2['column_name'].'_to").val().length) > 0) { filter'.$risultati2['column_name'].'_to = $("#'.$risultati2['column_name'].'_to").val(); }'.$crlf;

        } elseif (($risultati2['data_type'] == 'tinyint') || ($risultati2['data_type'] == 'bit')) {
            
            $data .= '
        filter'.$risultati2['column_name'].' = "";
        if (ischecked("#'.$risultati2['column_name'].'")) { filter'.$risultati2['column_name'].' = "1"; }'.$crlf;

        } else {

            $data .= '
        filter'.$risultati2['column_name'].' = "";
        if (parseInt($("#'.$risultati2['column_name'].'").val().length) > 0) { filter'.$risultati2['column_name'].' = $("#'.$risultati2['column_name'].'").val(); }'.$crlf;
        }
    }
}


$columns_to_export = "";
$counter = 1;
$query2 = $db->query($query_fields);
while ($risultati2 = mysqli_fetch_array($query2))
{
    if (($risultati2['column_name'] != 'id') && ($risultati2['data_type'] != 'blob') && ($risultati2['data_type'] != "mediumblob") && ($risultati2['data_type'] != "longblob")) {
        $columns_to_export .= $counter.", ";
        $counter += 1;
    }
}
$columns_to_export = substr($columns_to_export, 0, strlen($columns_to_export)-2);


$data .= '
        $(\'#myDatatable\').DataTable( {
            \'iDisplayLength\': 25,
            \'processing\': true,
            \'serverSide\': true,
            \'stateSave\': true,
            \'searching\': false,
            \'responsive\': true,
            dom: \'<"datatable-header"flB><"datatable-scroll-wrap"t><"datatable-footer"ip>\',
            buttons: [{
                        extend: \'pdf\',
                        orientation: \'landscape\',
                        text: \'<i class="icon-file-pdf"></i> PDF\',
                        title: \'\',
                        className: \'btn btn-outline-danger\',
                        exportOptions: {columns: [' . $columns_to_export . ']}
                    },
                    {
                        extend: \'excel\',
                        text: \'<i class="icon-file-excel"></i> Excel\',
                        title: \'\',
                        className: \'btn btn-outline-success\',
                        exportOptions: {columns: [' . $columns_to_export . ']}
                    }
            ],
            language: {
                lengthMenu: \'<span>Show:</span> _MENU_\',
                paginate: {\'first\': \'|&larr;\', \'last\': \'&rarr;|\', \'next\': \'&rarr;\', \'previous\': \'&larr;\'}
            },
            ajax: {
                url: \'controllers/' . $file_name_controller . '\',
                type: \'POST\',
                data: {
                    action: "load_table",'.$crlf;



$query2 = $db->query($query_fields);
while ($risultati2 = mysqli_fetch_array($query2))
{
    if ($risultati2['column_name'] != 'id') {
        if (substr($risultati2['column_name'], 0, 3) == 'id_') {
            $data .= '                    filter'.$risultati2['column_name'].': filter'.$risultati2['column_name'].','.$crlf;
        } elseif (($risultati2['data_type'] == 'date') || ($risultati2['data_type'] == 'int') || ($risultati2['data_type'] == 'smallint') || ($risultati2['data_type'] == 'mediumint') || ($risultati2['data_type'] == 'bigint') || ($risultati2['data_type'] == 'decimal') || ($risultati2['data_type'] == 'time')) {
            $data .= '                    filter'.$risultati2['column_name'].'_from: filter'.$risultati2['column_name'].'_from,'.$crlf;
            $data .= '                    filter'.$risultati2['column_name'].'_to:   filter'.$risultati2['column_name'].'_to,'.$crlf;
        } else {
            $data .= '                    filter'.$risultati2['column_name'].': filter'.$risultati2['column_name'].','.$crlf;
        }
    }
}


$data .= '
                }
            },
            \'columnDefs\': [
                { \'targets\': 0, \'visible\': false },'.$crlf;



$counter = 1;
$query2 = $db->query($query_fields);
while ($risultati2 = mysqli_fetch_array($query2))
{
    if (($risultati2['column_name'] != 'id') && ($risultati2['data_type'] != 'blob') && ($risultati2['data_type'] != "mediumblob") && ($risultati2['data_type'] != "longblob")) {

        if (substr($risultati2['column_name'], 0, 3) == "id_") {
            $data .= '                { "targets": '.$counter.',                           "width": "25%" },'.$crlf;
        } elseif (($risultati2['data_type'] == 'int') || ($risultati2['data_type'] == 'smallint') || ($risultati2['data_type'] == 'mediumint') || ($risultati2['data_type'] == 'bigint')) {
            $data .= '                { "targets": '.$counter.', "className": "dt-center", "width": "10%" },'.$crlf;
        } elseif ($risultati2['data_type'] == 'decimal') {
            $data .= '                { "targets": '.$counter.', "className": "dt-right",  "width": "10%" },'.$crlf;
        } elseif ($risultati2['data_type'] == 'date') {
            $data .= '                { "targets": '.$counter.',                           "width": "15%" },'.$crlf;
        } elseif ($risultati2['data_type'] == 'time') {
            $data .= '                { "targets": '.$counter.',                           "width": "15%" },'.$crlf;
        } else {
            $data .= '                { "targets": '.$counter.',                           "width": "25%" },'.$crlf;
        }
        $counter += 1;
    }
}
$data .= $crlf.'                { "targets": '.$counter.', "className": "dt-center", "orderable": false, "width": "5%" },';


$data .= '
            ]
        } );
    }

    function applyFilters() {
        table = $(\'#myDatatable\').DataTable();
        table.destroy();
        $("#myDatatable > tbody").html("");
        load_table();
        table = $(\'#myDatatable\').DataTable();
        table.page(0).draw(false);
    }

    function resetFilters() {'.$crlf;




$query2 = $db->query($query_fields);
while ($risultati2 = mysqli_fetch_array($query2)) {
    if ($risultati2['column_name'] != 'id') {
        if (substr($risultati2['column_name'], 0, 3) == "id_") {
            $data .= '        $("#'.$risultati2['column_name'].'").val("").trigger("change");'.$crlf;
        } elseif (($risultati2['data_type'] == 'tinyint') || ($risultati2['data_type'] == 'bit')) {
            $data .= '        if (ischecked("#'.$risultati2['column_name'].'")) {'.$crlf;
            $data .= '            $("#'.$risultati2['column_name'].'").click();'.$crlf;
            $data .= '        }'.$crlf;
        } elseif (($risultati2['data_type'] == 'date') || ($risultati2['data_type'] == 'int') || ($risultati2['data_type'] == 'smallint') || ($risultati2['data_type'] == 'mediumint') || ($risultati2['data_type'] == 'bigint') || ($risultati2['data_type'] == 'decimal') || ($risultati2['data_type'] == 'time')) {
            $data .= '        $("#'.$risultati2['column_name'].'_from").val("");'.$crlf;
            $data .= '        $("#'.$risultati2['column_name'].'_to").val("");'.$crlf;
        } else {
            $data .= '        $("#'.$risultati2['column_name'].'").val("");'.$crlf;
        }
    }
}


$data .= '
        applyFilters();
    }

    
</script>';





                    file_put_contents ("../".$file_name, $data);
                    if (file_exists("../".$file_name)) {
                        $info_execution .= '<div><span style="font-weight: bold;">'.$file_name.'</span></div>';
                    } else {
                        $error_execution .= '<div>Errors creating '.$file_name.'</div>';
                    }


/******************************************************************************
*******************************************************************************
*******************************************************************************
*******************************************************************************
GENERATING SECOND FILE: view rows
*******************************************************************************
*******************************************************************************
*******************************************************************************
******************************************************************************/
if ($tablename_rows != "") {

$data = '<?php
    include "sys_header.php";
    if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight('.$id_page.', "read")) {header("location: ".linkto("sys_dashboard")); die();}}
?>

<body class="navbar-top<?php if ($settings_menu_closed == 1) { echo \' sidebar-xs\'; } ?>">
    <div id="imagePreviewDiv" class="modal modal-xl fade imgpreview" style="z-index: 9999;"></div>
    <?php include "sys_navbar.php"; ?>
    <div class="page-content">
        <?php
        include "sys_sidebar.php";
        if (!isset($_GET[\'id\'])) {
            if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight('.$id_page.', "create")) {die();}}
        } else {
            if (!checkUserRight('.$id_page.', "edit")) {echo "<style>.newrow,.icon-trash-empty,.dropzone,.savebutton{display: none;}</style>";}
        }
        ?>
        <div class="content-wrapper">

        <div class="page-header page-header-light">
            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">

                <div class="d-flex">
                    <i class="'.$icon.' mr-2 breadcrumb-item"></i>
                    <span style="margin-top: 8px;">
                    <?php
                        if (isset($_GET[\'id\'])) {
                            if (checkUserRight('.$id_page.', "edit")) {
                                echo "Edit '.$page_name.'";
                            } else {
                                echo "View '.$page_name.'";
                            }
                        } else {
                            echo "New '.$page_name.'";
                        }
                    ?>
                    </span>
                </div>

                <div class="header-elements d-none" style="display: flex !important;">
                    <div class="breadcrumb justify-content-center">
                        
                    </div>
                </div>
            </div>
        </div>'.$crlf;



$data .= '
<div class="content unselectable">
    <input type="hidden" id="id" <?php if(isset($_GET[\'id\'])) echo \'value="\'.$_GET[\'id\'].\'"\'; ?> >
    <div class="card">
        <div class="card-body">
            <div class="row">'.$crlf;



$query2 = $db->query($query_fields);
while ($risultati2 = mysqli_fetch_array($query2))
{
    if ($risultati2['column_name'] != 'id') {
        if (substr($risultati2['column_name'], 0, 3) == "id_") {
            
            $description_field = "";
            $query_campo_descrittivo = "SELECT column_name FROM information_schema.columns WHERE ordinal_position > 1 AND table_name = '".substr($risultati2["column_name"], 3)."' AND table_schema = '".$db_database."' ORDER BY ordinal_position ASC LIMIT 1 ";
            $query4 = $db->query($query_campo_descrittivo);
            while ($risultati4 = mysqli_fetch_array($query4)) {
                $description_field = $risultati4['column_name'];
            }

            $data .= '
                <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                    <label class="text-bold">'.str_replace("_", " ", ucfirst(substr($risultati2['column_name'], 3))).'</label>
                    <select class="form-control select" id="'.$risultati2['column_name'].'" '.(($risultati2['nullable'] == 0) ? "required" : "").' >
                    <option value=""></option>
                    <?php
                        $query = $db->query("SELECT id, '.$description_field.' FROM '.substr($risultati2["column_name"], 3).' ORDER BY '.$description_field.'");
                        while($risultati = mysqli_fetch_array($query)) {
                            echo \'<option value="\'.$risultati["id"].\'">\'.$risultati["'.$description_field.'"].\'</option>\';
                        }
                    ?>
                    </select>
                </div></div>'.$crlf;
        } else {
            if ($risultati2['data_type'] == 'date') {
                $data .= '
                <div class="col-lg-3 col-md-6">
                    <div class="form-group container_fields">
                        <label class="text-bold">'.str_replace("_", " ", ucfirst($risultati2['column_name'])).'</label>
                        <input type="date" style="text-align:center;" id="'.$risultati2['column_name'].'"    autocomplete="off" class="form-control" '.(($risultati2['nullable'] == 0) ? "required" : "").' >
                    </div>
                </div>'.$crlf;
            } elseif (($risultati2['data_type'] == 'int') || ($risultati2['data_type'] == 'smallint') || ($risultati2['data_type'] == 'mediumint') || ($risultati2['data_type'] == 'bigint')) {
                $data .= '
                <div class="col-lg-3 col-md-6">
                    <div class="form-group container_fields">
                        <label class="text-bold">'.str_replace("_", " ", ucfirst($risultati2['column_name'])).'</label>
                        <input type="number" style="text-align:center;" step="1" id="'.$risultati2['column_name'].'"    onkeypress="return checkInteger(event)" autocomplete="off" class="form-control" '.(($risultati2['nullable'] == 0) ? "required" : "").' >
                    </div>
                </div>'.$crlf;
            } elseif ($risultati2['data_type'] == 'decimal') {
                if ($risultati2['numeric_scale'] == 0) { $step = "1"; } else { $step = "0.".str_pad("1",$risultati2['numeric_scale'],"0",STR_PAD_LEFT); }
                $data .= '
                <div class="col-lg-3 col-md-6">
                    <div class="form-group container_fields">
                        <label class="text-bold">'.str_replace("_", " ", ucfirst($risultati2['column_name'])).'</label>
                        <input type="number" style="text-align:right;" step="'.$step.'" id="'.$risultati2['column_name'].'"  onkeypress="return checkFloat(event)"   autocomplete="off" class="form-control" '.(($risultati2['nullable'] == 0) ? "required" : "").' >
                    </div>
                </div>'.$crlf;
            } elseif (($risultati2['data_type'] == 'tinyint') || ($risultati2['data_type'] == 'bit')) {
                $data .= '
                <div class="col-lg-3 col-md-6">
                    <div class="form-group container_fields">
                        <label class="text-bold">'.str_replace("_", " ", ucfirst($risultati2['column_name'])).'</label><br>
                        <input type="checkbox" id="'.$risultati2['column_name'].'" class="form-control" value="1"  style="width: 28px; margin: 0;" >
                    </div>
                </div>'.$crlf;
            } elseif ($risultati2['data_type'] == 'time') {
                $data .= '
                <div class="col-lg-3 col-md-6">
                    <div class="form-group container_fields">
                        <label class="text-bold">'.str_replace("_", " ", ucfirst($risultati2['column_name'])).'</label>
                        <input type="time" style="text-align:center;" id="'.$risultati2['column_name'].'"    autocomplete="off" class="form-control" '.(($risultati2['nullable'] == 0) ? "required" : "").' >
                    </div>
                </div>'.$crlf;
            } elseif (($risultati2['data_type'] == 'blob') || ($risultati2['data_type'] == "mediumblob") || ($risultati2['data_type'] == "longblob")) {
                $data .= '
                <div class="col-lg-6 col-md-12">
                    <div class="form-group container_fields">
                        <label class="text-bold">'.str_replace("_", " ", ucfirst($risultati2['column_name'])).'</label><br>
                        <div class="row" style="text-align: left; margin: unset;">
                            <div class="col-lg-6 col-md-6">
                                <form class="dropzone" id="dropzone_'.$risultati2['column_name'].'"></form>
                            </div>
                            <div class="col-lg-6 col-md-6" id="fileContainer_'.$risultati2['column_name'].'"></div>
                        </div>
                    </div>
                </div>
                '.$crlf;
            } else {
                $data .= '
                <div class="col-lg-3 col-md-6">
                    <div class="form-group container_fields">
                        <label class="text-bold">'.str_replace("_", " ", ucfirst($risultati2['column_name'])).'</label>
                        <input type="text" id="'.$risultati2['column_name'].'"  maxlength="'.$risultati2['character_maximum_length'].'"  autocomplete="off" class="form-control" >
                    </div>
                </div>'.$crlf;
            }
        }
    }
}


$data .= '
            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="text-right">
                        <button onclick="event.preventDefault(); save_record();" class="btn btn-lg btn-outline-success savebutton"><i class="icon-ok-3"></i> Save</button>
                        <button onclick="event.preventDefault(); document.location.href=\'' . $file_name . '\'" class="btn btn-lg btn-outline-warning"><i class="icon-undo"></i> Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>'.$crlf;




$tmp_table_rows = "";
$export_table = "";
$last_column = 0;
$query3 = $db->query($query_fields_rows);
while ($risultati3 = mysqli_fetch_array($query3)) {
    if (($risultati3['column_name'] != "id_".$tablename) && ($risultati3['data_type'] != "blob") && ($risultati3['data_type'] != "mediumblob") && ($risultati3['data_type'] != "longblob")) {
        if (substr($risultati3['column_name'], 0, 3) == "id_") {
            $tmp_table_rows .= '                        <th><b>'.str_replace("_", " ", ucfirst(substr($risultati3['column_name'], 3))).'&nbsp;</b></th>'.$crlf;
        } else {
            $tmp_table_rows .= '                        <th><b>'.str_replace("_", " ", ucfirst($risultati3['column_name'])).'&nbsp;</b></th>'.$crlf;
        }

        if ($risultati3['column_name'] != "id") {
            if ($export_table != "") { $export_table .= ', '; }
            $export_table .= $last_column;
        }
        $last_column += 1;
    }
}


if ($tmp_table_rows != "") {
    $data .= '

        <div class="card">
            <table id="myRowsDatatable" class="display table table-striped datatable-responsive" style="width:100%; overflow: hidden;">
                <thead>
                    <tr>'.$crlf.
$tmp_table_rows.'
                        <th><b>Actions</b></th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>'.$crlf.
$tmp_table_rows.'
                        <th><b>Actions</b></th>
                    </tr>
                </tfoot>
            </table>
        </div>'.$crlf;
}



$data .= '
</div>

<div id="modal_detail" class="modal fade" style="-webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none;" unselectable="on">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-primary">
                <h6 class="modal-title"><i class="icon-pencil"></i> Row detail</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
            <div class="modal-body">
                <div class="content">'.$crlf;




$query3 = $db->query($query_fields_rows);
while ($risultati3 = mysqli_fetch_array($query3))
{
    if ($risultati3['column_name'] == "id") {
        $data .= '                    <input type="hidden" id="row_id" value="" >'.$crlf;

    } elseif ($risultati3['column_name'] == "id_".$tablename) {
        $data .= '                    <input type="hidden" id="row_id_'.$tablename.'" value="" >'.$crlf;

    } elseif (substr($risultati3['column_name'], 0, 3) == "id_") {

        $description_field = "";
        $query_campo_descrittivo = "SELECT column_name FROM information_schema.columns WHERE ordinal_position > 1 AND table_name = '".substr($risultati3["column_name"], 3)."' AND table_schema = '".$db_database."' ORDER BY ordinal_position ASC LIMIT 1 ";
        $query4 = $db->query($query_campo_descrittivo);
        while ($risultati4 = mysqli_fetch_array($query4)) {
            $description_field = $risultati4['column_name'];
        }

        $data .= '                    <div class="row form-group">
                        <div class="col-lg-4">
                            <label class="text-bold" style="margin: 9px 0px 0px 0px;">'.str_replace("_", " ", ucfirst(substr($risultati3['column_name'], 3))).'</label>
                        </div>
                        <div class="col-lg-8" style="margin-top: 3px;">
                            <select style="width:100%" id="row_'.$risultati3['column_name'].'" class="form-control select" '.(($risultati3['nullable'] == 0) ? "required" : "").' >
                            <?php
                                $query = $db->query("SELECT id, '.$description_field.' FROM '.substr($risultati3["column_name"], 3).' ORDER BY '.$description_field.'");
                                while($risultati = mysqli_fetch_array($query)) {
                                    echo \'<option value="\'.$risultati["id"].\'">\'.$risultati["'.$description_field.'"].\'</option>\';
                                }
                            ?>
                            </select>
                        </div>
                    </div>'.$crlf;

    } elseif ($risultati3['data_type'] == 'date') {
        $data .= '                    <div class="row form-group">
                        <div class="col-lg-4">
                            <label class="text-bold" style="margin: 9px 0px 0px 0px;">'.str_replace("_", " ", ucfirst($risultati3['column_name'])).'</label>
                        </div>
                        <div class="col-lg-8" style="margin-top: 3px;">
                            <input style="width:170px;" type="date" id="row_'.$risultati3['column_name'].'" value=""  class="form-control col-xs-6"  autocomplete="off" '.(($risultati3['nullable'] == 0) ? "required" : "").' >
                        </div>
                    </div>'.$crlf;

    } elseif ($risultati3['data_type'] == 'time') {
        $data .= '                    <div class="row form-group">
                        <div class="col-lg-4">
                            <label class="text-bold" style="margin: 9px 0px 0px 0px;">'.str_replace("_", " ", ucfirst($risultati3['column_name'])).'</label>
                        </div>
                        <div class="col-lg-8" style="margin-top: 3px;">
                            <input style="width:95px;" type="time" id="row_'.$risultati3['column_name'].'" value=""  class="form-control col-xs-6"  autocomplete="off" '.(($risultati3['nullable'] == 0) ? "required" : "").' >
                        </div>
                    </div>'.$crlf;

    } elseif (($risultati3['data_type'] == 'int') || ($risultati3['data_type'] == 'smallint') || ($risultati3['data_type'] == 'mediumint') || ($risultati3['data_type'] == 'bigint')) {
        $data .= '                    <div class="row form-group">
                        <div class="col-lg-4">
                            <label class="text-bold" style="margin: 9px 0px 0px 0px;">'.str_replace("_", " ", ucfirst($risultati3['column_name'])).'</label>
                        </div>
                        <div class="col-lg-8" style="margin-top: 3px;">
                            <input style="width:170px; text-align:center;"  type="number"    id="row_'.$risultati3['column_name'].'" value=""  class="form-control col-xs-6"  autocomplete="off" onkeypress="return checkInteger(event)" '.(($risultati3['nullable'] == 0) ? "required" : "").' >
                        </div>
                    </div>'.$crlf;

    } elseif ($risultati3['data_type'] == 'decimal') {
        if ($risultati3['numeric_scale'] == 0) { $step = "1"; } else { $step = "0.".str_pad("1",$risultati3['numeric_scale'],"0",STR_PAD_LEFT); }
        $data .= '                    <div class="row form-group">
                        <div class="col-lg-4">
                            <label class="text-bold" style="margin: 9px 0px 0px 0px;">'.str_replace("_", " ", ucfirst($risultati3['column_name'])).'</label>
                        </div>
                        <div class="col-lg-8" style="margin-top: 3px;">
                            <input style="width:170px; text-align:right;" type="number" id="row_'.$risultati3['column_name'].'" value=""  class="form-control col-xs-6"  autocomplete="off" onkeypress="return checkFloat(event)" step="'.$step.'" '.(($risultati3['nullable'] == 0) ? "required" : "").' >
                        </div>
                    </div>'.$crlf;

    } elseif (($risultati3['data_type'] == 'tinyint') || ($risultati3['data_type'] == 'bit')) {
        $data .= '                    <div class="row form-group">
                        <div class="col-lg-4">
                            <label class="text-bold" style="margin: 0px 0px 0px 0px;">'.str_replace("_", " ", ucfirst($risultati3['column_name'])).'</label>
                        </div>
                        <div class="col-lg-8" style="margin-top: 3px;">
                            <input style="width:22px; height:22px;" type="checkbox" id="row_'.$risultati3['column_name'].'" value="1" class="form-control col-xs-6"  >
                        </div>
                    </div>'.$crlf;

    } elseif (($risultati3['data_type'] == 'blob') || ($risultati3['data_type'] == "mediumblob") || ($risultati3['data_type'] == "longblob")) {
        $data .= '
                    <div class="row form-group">
                        <div class="col-lg-4">
                            <label class="text-bold" style="margin: 9px 0px 0px 0px;">'.str_replace("_", " ", ucfirst($risultati3['column_name'])).'</label>
                        </div>
                        <div class="col-lg-8" style="margin-top: 3px;">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form class="dropzone" id="dropzone_row_'.$risultati3['column_name'].'"></form>
                                </div>
                                <div class="col-lg-12" id="fileContainer_row_'.$risultati3['column_name'].'"></div>    
                            </div>
                        </div>
                    </div>'.$crlf;

    } else {
        $data .= '                    <div class="row form-group">
                        <div class="col-lg-4">
                            <label class="text-bold" style="margin: 9px 0px 0px 0px;">'.str_replace("_", " ", ucfirst($risultati3['column_name'])).'</label>
                        </div>
                        <div class="col-lg-8" style="margin-top: 3px;">
                            <input style="width:100%;" type="text" id="row_'.$risultati3['column_name'].'" value="" class="form-control" autocomplete="off" maxlength="'.$risultati3['character_maximum_length'].'" '.(($risultati3['nullable'] == 0) ? "required" : "").' >
                        </div>
                    </div>'.$crlf;
    }
}



$data .= '
                </div>
            </div>
			<div class="modal-footer">
                <button onclick="save_row();" class="btn btn-lg btn-outline-success savebutton"><i class="icon-ok-3"></i> Save</button>
                <button onclick="$(\'#modal_detail\').modal(\'hide\');" class="btn btn-lg btn-outline-warning"><i class="icon-undo"></i> Cancel</button>
			</div>
		</div>
	</div>
</div>

</div>
</div>
</body>


<script type="text/javascript">'.$crlf.$crlf;



$query2 = $db->query($query_fields);
while ($risultati2 = mysqli_fetch_array($query2)) {
    if (($risultati2['data_type'] == 'blob') || ($risultati2['data_type'] == "mediumblob") || ($risultati2['data_type'] == "longblob")) {
        $data .= '    var FILELIST_'.$risultati2['column_name'].' = new Array;'.$crlf;
    }
}
if ($tmp_table_rows != "") {
$query3 = $db->query($query_fields_rows);
while ($risultati3 = mysqli_fetch_array($query3)) {
    if (($risultati3['data_type'] == 'blob') || ($risultati3['data_type'] == "mediumblob") || ($risultati3['data_type'] == "longblob")) {
        $data .= '    var FILELIST_row_'.$risultati3['column_name'].' = new Array;'.$crlf;
    }
}
}


$data .= '
    $(document).ready(function () {
        $(\'.select\').select2({ width: \'100%\' });'.$crlf;



$query2 = $db->query($query_fields);
while ($risultati2 = mysqli_fetch_array($query2)) {
    if (($risultati2['data_type'] == 'blob') || ($risultati2['data_type'] == "mediumblob") || ($risultati2['data_type'] == "longblob")) {
        $data .= '

        new Dropzone("#dropzone_'.$risultati2['column_name'].'", {
            init: function() {
                this.on("success", function(file, serverFileName) {
                    FILELIST_'.$risultati2['column_name'].'.push({"serverFileName" : serverFileName, "fileName" : file.name});
                    drawFileList(FILELIST_'.$risultati2['column_name'].', "FILELIST_'.$risultati2['column_name'].'", "fileContainer_'.$risultati2['column_name'].'");
                });
                this.on("error", function(file, errorMessage) {
                    message_box("Error", errorMessage, "Ok", "error");
                });
            },
            url: "controllers/sys_file_uploader.php" });'.$crlf;
    }
}


if ($tmp_table_rows != "") {
$query3 = $db->query($query_fields_rows);
while ($risultati3 = mysqli_fetch_array($query3)) {
    if (($risultati3['data_type'] == 'blob') || ($risultati3['data_type'] == "mediumblob") || ($risultati3['data_type'] == "longblob")) {
        $data .= '

        new Dropzone("#dropzone_row_'.$risultati3['column_name'].'", {
            init: function() {
                this.on("success", function(file, serverFileName) {
                    FILELIST_row_'.$risultati3['column_name'].'.push({"serverFileName" : serverFileName, "fileName" : file.name});
                    drawFileList(FILELIST_row_'.$risultati3['column_name'].', "FILELIST_row_'.$risultati3['column_name'].'", "fileContainer_row_'.$risultati3['column_name'].'");
                });
                this.on("error", function(file, errorMessage) {
                    message_box("Error", errorMessage, "Ok", "error");
                });
            },
            url: "controllers/sys_file_uploader.php" });'.$crlf;
    }
}
}


$data .= $crlf.'
        <?php 
        if (isset($_GET["id"])) {
            echo "edit_record(".$_GET["id"].");";
        }
        ?> '.$crlf.$crlf;


$first_input = "";
$query2 = $db->query($query_fields);
while ($risultati2 = mysqli_fetch_array($query2))
{
    if (($risultati2['column_name'] != 'id') && (substr($risultati2['column_name'], 0, 3) != 'id_') && ($first_input == "")) { $first_input = "#".$risultati2['column_name']; }
}
$data .= '        setTimeout(function(){ $("'.$first_input.'").focus(); }, 300);'.$crlf;


if ($tmp_table_rows != "") {
    $data .= '
        load_table();
        table = $("#myRowsDatatable").DataTable();
        table.on("draw", function () {
            table.rows().every(function (rowIdx, tableLoop, rowLoop) {
                var values_in_row = this.data();
                $.ajax({
                    async: false,
                    type: "POST",
                    url: "controllers/'.$file_name_controller.'",
                    data: {
                        "action": "button_record_row",
                        "id": values_in_row[0]
                    },
                    success: function (data, stato) {
                        values_in_row['.$last_column.'] = data;
                    }
                });
                this.data(values_in_row);
            });
        });'.$crlf;
}    



$data .= '
    });


    function edit_record(id) {
        $.ajax({
            async: false,
            type: "POST",
            url: "controllers/' . $file_name_controller . '",
            data: {
                "action": "read_record",
                "id": id
            },
            success: function (data, stato) {
                var data = $.parseJSON(data);
                if ((data.length == 0) || (data == "NOT AUTHORIZED")) {
                    location.href = \''.$file_name.'\';
                } else {'.$crlf;

$query2 = $db->query($query_fields);
while ($risultati2 = mysqli_fetch_array($query2))
{
    if (substr($risultati2['column_name'], 0, 3) == "id_") {
        $data .= '                    $("#'.$risultati2['column_name'].'").val(data["record_'.$risultati2['column_name'].'"]).trigger("change");'.$crlf;
    } else {
        if (($risultati2['data_type'] == 'tinyint') || ($risultati2['data_type'] == 'bit')) { 
            $data .= '
                    if (data["record_'.$risultati2['column_name'].'"] == 1) {
                        if (!ischecked("#'.$risultati2['column_name'].'")) {
                            $("#'.$risultati2['column_name'].'").click();
                        }
                    } else {
                        if (ischecked("#'.$risultati2['column_name'].'")) {
                            $("#'.$risultati2['column_name'].'").click();
                        }
                    }'.$crlf;
        } elseif (($risultati2['data_type'] == 'blob') || ($risultati2['data_type'] == "mediumblob") || ($risultati2['data_type'] == "longblob")) {
            $data .= '
                    if (data["record_'.$risultati2['column_name'].'"] !== null) {
                        if (data["record_'.$risultati2['column_name'].'"].length > 0) {
                            FILELIST_'.$risultati2['column_name'].' = JSON.parse(data["record_'.$risultati2['column_name'].'"]);
                            if (FILELIST_'.$risultati2['column_name'].' === null) {
                                FILELIST_'.$risultati2['column_name'].' = new Array;
                            } else {
                                drawFileList(FILELIST_'.$risultati2['column_name'].', "FILELIST_'.$risultati2['column_name'].'", "fileContainer_'.$risultati2['column_name'].'");
                            }
                        }
                    }
            '.$crlf;
        } else {
            $data .= '                    $("#'.$risultati2['column_name'].'").val(data["record_'.$risultati2['column_name'].'"]);'.$crlf;
        }
    }
}
$data .= '
                }
            }
        });
    }


    function save_record() {
        var requiredfields = "";
        $(".card-body :required").each(function(){
            if (($(this).val() == "") || ($(this).val() === null)) {
                if (requiredfields != "") { requiredfields = requiredfields.concat(", "); }
                var tmp_req_field = $(this).attr("id").replace("_", " ");
                if (tmp_req_field.substr(0, 3) == "id ") { tmp_req_field = tmp_req_field.replace("id ", ""); }
                requiredfields = requiredfields.concat(tmp_req_field);
            }
        })
        if (requiredfields != "") { toast("Required fields: ".concat(requiredfields), "error"); return; }

        $.ajax({
            async: false,
            type: "POST",
            url: "controllers/' . $file_name_controller . '",
            data: {
                "action": "save_record",'.$crlf;



$query2 = $db->query($query_fields);
while ($risultati2 = mysqli_fetch_array($query2)) {
    if (($risultati2['data_type'] == 'blob') || ($risultati2['data_type'] == "mediumblob") || ($risultati2['data_type'] == "longblob")) {
        $data .= '                "'.$risultati2['column_name'].'": JSON.stringify(FILELIST_'.$risultati2['column_name'].'), '.$crlf;
    } else {
        if (($risultati2['data_type'] == 'tinyint') || ($risultati2['data_type'] == 'bit')) {
            $data .= '                "'.$risultati2['column_name'].'": ischecked("#'.$risultati2['column_name'].'"), '.$crlf;
        } else {
            $data .= '                "'.$risultati2['column_name'].'": $("#'.$risultati2['column_name'].'").val(), '.$crlf;
        }
    }
}


$data .= '
            },
            success: function (data, stato) {
                if (data != "OK") {
                    swal({
                        title: "Error",
                        text: "Qualcosa è andato storto " + data.toString(),
                        confirmButtonColor: "#66BB6A",
                        type: "warning"
                    });
                } else {
                    location.href = \'' . $file_name . '\'; 
                }
            }
        });
    }'.$crlf;


if ($tmp_table_rows != "") {
    $data .= '

    function add_row() {'.$crlf;


    $query3 = $db->query($query_fields_rows);
    while ($risultati3 = mysqli_fetch_array($query3)) {
        if ($risultati3['column_name'] == "id_".$tablename) {
            $data .= '        <?php if (!isset($_GET[\'id\'])) { echo \'$("#row_id_'.$tablename.'").val("-\'.$_SESSION[\'user_id\'].\'");\'; } else { echo \'$("#row_id_'.$tablename.'").val("\' . $_GET[\'id\'] . \'");\'; } ?>'.$crlf;
        } elseif (($risultati3['data_type'] == 'tinyint') || ($risultati3['data_type'] == 'bit')) {
            $data .= '        if (ischecked("#row_'.$risultati3['column_name'].'")) {'.$crlf;
            $data .= '            $("#row_'.$risultati3['column_name'].'").click();'.$crlf;
            $data .= '        }'.$crlf;
        } elseif (substr($risultati3['column_name'], 0, 3) == "id_") {
            $data .= '        $("#row_'.$risultati3['column_name'].'").val(0).trigger("change");'.$crlf;
        } elseif (($risultati3['data_type'] == 'blob') || ($risultati3['data_type'] == "mediumblob") || ($risultati3['data_type'] == "longblob")) {
            $data .= $crlf;
            $data .= '        document.querySelector("#dropzone_row_'.$risultati3['column_name'].'").dropzone.removeAllFiles();'.$crlf;
            $data .= '        FILELIST_row_'.$risultati3['column_name'].' = new Array;'.$crlf;
            $data .= '        $("#fileContainer_row_'.$risultati3['column_name'].'").html("");'.$crlf.$crlf;
        } else {
            $data .= '        $("#row_'.$risultati3['column_name'].'").val("");'.$crlf;
        }
    }
    $data .= '        setTimeout(function(){ $("'.$first_input.'").focus(); }, 300);
        $("#modal_detail").modal({backdrop: \'static\'});
    }
    
    function edit_row(id) {
        $.ajax({
            async: false,
            type: "POST",
            url : "controllers/' . $file_name_controller . '",
            data: {
                "action": "read_record_row",
                "id": id
            },
            success : function (data) {
                if (data.length > 8) {
                    var data = $.parseJSON(data);
                    if (data == "NOT AUTHORIZED") { return; }'.$crlf;



    $first_input = "";
    $query3 = $db->query($query_fields_rows);
    while ($risultati3 = mysqli_fetch_array($query3)) {
        if (($risultati3['column_name'] != "id_".$tablename) && ($risultati3['data_type'] != "blob") && ($risultati3['data_type'] != "mediumblob") && ($risultati3['data_type'] != "longblob")) {
            if (($risultati3['column_name'] != 'id') && ($risultati3['data_type'] != 'bit') && ($risultati3['data_type'] != 'tinyint') && (substr($risultati3['column_name'], 0, 3) != 'id_') && ($first_input == "")) { $first_input = "#row_".$risultati3['column_name']; }
        }
        if (($risultati3['data_type'] == 'tinyint') || ($risultati3['data_type'] == 'bit')) {
            $data .= '
                    if (data.'.$risultati3['column_name'].' == 1) {
                        if (!ischecked("#row_'.$risultati3['column_name'].'")) {
                            $("#row_'.$risultati3['column_name'].'").click();
                        }
                    } else {
                        if (ischecked("#row_'.$risultati3['column_name'].'")) {
                            $("#row_'.$risultati3['column_name'].'").click();
                        }
                    }'.$crlf;
        } elseif (substr($risultati3['column_name'], 0, 3) == "id_") {
            $data .= '                    $("#row_'.$risultati3['column_name'].'").val(data.'.$risultati3['column_name'].').trigger("change");'.$crlf;
        } elseif (($risultati3['data_type'] == 'blob') || ($risultati3['data_type'] == "mediumblob") || ($risultati3['data_type'] == "longblob")) {
            $data .= '
                    $("#fileContainer_row_'.$risultati3['column_name'].'").html("");
                    document.querySelector("#dropzone_row_'.$risultati3['column_name'].'").dropzone.removeAllFiles();
                    FILELIST_row_'.$risultati3['column_name'].' = new Array;
                    if (data.'.$risultati3['column_name'].' !== null) {
                        if (data.'.$risultati3['column_name'].'.length > 0) {
                            FILELIST_row_'.$risultati3['column_name'].' = JSON.parse(data.'.$risultati3['column_name'].');
                            if (FILELIST_row_'.$risultati3['column_name'].' === null) {
                                FILELIST_row_'.$risultati3['column_name'].' = new Array;
                            } else {
                                drawFileList(FILELIST_row_'.$risultati3['column_name'].', "FILELIST_row_'.$risultati3['column_name'].'", "fileContainer_row_'.$risultati3['column_name'].'");
                            }
                        }
                    }'.$crlf;
        } else {
            $data .= '                    $("#row_'.$risultati3['column_name'].'").val(data.'.$risultati3['column_name'].');'.$crlf;
        }
    }
    $data .= '                    setTimeout(function(){ $("'.$first_input.'").focus(); }, 300);
                    $("#modal_detail").modal({backdrop: \'static\'});
                }
            }
        });
    }


    function save_row() {
        var requiredfields = "";
        $(".modal-body :required").each(function(){
            if (($(this).val() == "") || ($(this).val() === null)) {
                if (requiredfields != "") { requiredfields = requiredfields.concat(", "); }
                var tmp_req_field = $(this).attr("id").replace("row_", "").replace("_", " ");
                if (tmp_req_field.substr(0, 3) == "id ") { tmp_req_field = tmp_req_field.replace("id ", ""); }
                requiredfields = requiredfields.concat(tmp_req_field);
            }
        })
        if (requiredfields != "") { toast("Required fields: ".concat(requiredfields), "error"); return; }

        $.ajax({
            async: false,
            type: "POST",
            url: "controllers/' . $file_name_controller . '",
            data: {
                "action": "save_record_row",'.$crlf;


$query3 = $db->query($query_fields_rows);
while ($risultati3 = mysqli_fetch_array($query3))
{
    if (($risultati3['data_type'] == 'tinyint') || ($risultati3['data_type'] == 'bit')) {
        $data .= '                "'.$risultati3['column_name'].'": ischecked(\'#row_'.$risultati3['column_name'].'\'),'.$crlf;
    } elseif (($risultati3['data_type'] == 'blob') || ($risultati3['data_type'] == "mediumblob") || ($risultati3['data_type'] == "longblob")) {
        $data .= '                "'.$risultati3['column_name'].'": JSON.stringify(FILELIST_row_'.$risultati3['column_name'].'),'.$crlf;
    } else {
        $data .= '                "'.$risultati3['column_name'].'": $(\'#row_'.$risultati3['column_name'].'\').val(),'.$crlf;
    }
}

$data .= '
            },
            success: function (data, stato) {
                if (data != "OK") {
                    swal({
                        title: "Error",
                        text: "An error occurred during Save: " + data.toString(),
                        confirmButtonColor: "#66BB6A",
                        type: "warning"
                    });
                } else {
                    $("#modal_detail").modal(\'hide\');
                    update_table();
                    
                    if ($(\'#row_id\').val() == \'\') {
                        setTimeout(function(){
                            add_row();
                        }, 700);
                    }
                    
                }
            }
        });
    }


    function update_table()  {
        table = $(\'#myRowsDatatable\').DataTable();
        table.destroy();
        $("#myRowsDatatable > tbody").html("");
        load_table();
        table = $(\'#myRowsDatatable\').DataTable();
        table.page(0).draw(false);
    }

    
    function load_table() {
        $(\'#myRowsDatatable\').DataTable( {
            \'processing\': true,
            \'serverSide\': true,
            \'stateSave\': true,
            \'searching\': false,
            \'responsive\': true,
            dom: \'<"datatable-header"flB><"datatable-scroll-wrap"t><"datatable-footer"ip>\',
            buttons: [{
                        text: \'<i class="icon-plus-squared"></i> New row\',
                        className: \'btn btn-outline-info newrow\',
                        action: function ( e, dt, node, config ) {
                            add_row();
                        }
                    },
                    {
                        extend: \'pdf\',
                        orientation: \'landscape\',
                        text: \'<i class="icon-file-pdf"></i> PDF\',
                        title: \'\',
                        className: \'btn btn-outline-danger\',
                        exportOptions: {columns: ['.$export_table.']}
                    },
                    {
                        extend: \'excel\',
                        text: \'<i class="icon-file-excel"></i> Excel\',
                        title: \'\',
                        className: \'btn btn-outline-success\',
                        exportOptions: {columns: ['.$export_table.']}
                    }
            ],
            language: {
                lengthMenu: \'<span>Show:</span> _MENU_\',
                paginate: {\'first\': \'|&larr;\', \'last\': \'&rarr;|\', \'next\': \'&rarr;\', \'previous\': \'&larr;\'}
            },
            ajax: {
                url: \'controllers/'.$file_name_controller.'\',
                type: \'POST\',
                data: {
                    action: \'load_table_rows\',
                    filter_id: <?php if (!isset($_GET[\'id\'])) { echo "-".$_SESSION["user_id"]; } else { echo $_GET[\'id\']; } ?>,
                }
            },
            \'columnDefs\': [
                { \'targets\': 0, \'visible\': false },'.$crlf;

    $contatore = 1;
    $query3 = $db->query($query_fields_rows);
    while ($risultati3 = mysqli_fetch_array($query3)) {
        if (($risultati3['column_name'] != "id") && ($risultati3['column_name'] != "id_".$tablename) && ($risultati3['data_type'] != "blob") && ($risultati3['data_type'] != "mediumblob") && ($risultati3['data_type'] != "longblob")) {
            if (substr($risultati3['column_name'], 0, 3) == "id_") {
                $data .= '                { "targets": '.$contatore.',                           "width": "25%" },'.$crlf;
            } elseif (($risultati3['data_type'] == 'int') || ($risultati3['data_type'] == 'smallint') || ($risultati3['data_type'] == 'mediumint') || ($risultati3['data_type'] == 'bigint')) {
                $data .= '                { "targets": '.$contatore.', "className": "dt-center", "width": "10%" },'.$crlf;
            } elseif ($risultati3['data_type'] == 'decimal') {
                $data .= '                { "targets": '.$contatore.', "className": "dt-right",  "width": "10%" },'.$crlf;
            } elseif ($risultati3['data_type'] == 'date') {
                $data .= '                { "targets": '.$contatore.',                           "width": "15%" },'.$crlf;
            } elseif ($risultati3['data_type'] == 'time') {
                $data .= '                { "targets": '.$contatore.',                           "width": "15%" },'.$crlf;
            } else {
                $data .= '                { "targets": '.$contatore.',                           "width": "25%" },'.$crlf;
            }
            $contatore += 1;
        }
    }

    $data .= '
                { "targets": '.$contatore.', "className": "dt-center", "orderable": false, "width": "5%" },
            ]
        } );
    }


    function delete_row(id) {
        swal({
            title: "Sei sicuro?",
            text: "I dati non potranno essere recuperati",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: "Si, elimina!",
            cancelButtonText: "No, annulla!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    async: true,
                    type: "POST",
                    url : "controllers/'.$file_name_controller.'",
                    data: {
                        "action": "delete_record_row",
                        "id": id
                    },
                    success: function (data) {
                        if (data === "OK") {
                            swal({
                                title: "Eliminato!",
                                text: "Il record è stato cancellato",
                                confirmButtonColor: "#66BB6A",
                                type: "success"
                            }, function () {
                                update_table();
                            });
                        } else {
                            swal({
                                title: "Operazione annullata",
                                text: "Qualcosa è andato storto",
                                confirmButtonColor: "#2196F3",
                                type: "error"
                            });
                        update_table();
                        }
                    },
                    error: function (data) {
                        swal({
                            title: "Operazione annullata",
                            text: "Qualcosa è andato storto",
                            confirmButtonColor: "#2196F3",
                            type: "error"
                        });
                        update_table();
                    }
                });
            }
            else {
                swal({
                    title: "Operazione annullata",
                    text: "",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
    }'.$crlf;
}

$data .= '</script>';


                    
    file_put_contents ("../".$file_name_edit, $data);
    if (file_exists("../".$file_name_edit)) {
        $info_execution .= '<div><span style="font-weight: bold;">'.$file_name_edit.'</span></div>';
    } else {
        $error_execution .= '<div>Errors creating '.$file_name_edit.'</div>';
    }
}


/******************************************************************************
*******************************************************************************
*******************************************************************************
*******************************************************************************
GENERATING THIRD FILE: controller
*******************************************************************************
*******************************************************************************
*******************************************************************************
******************************************************************************/


$data = '<?php

if (!isset($_SESSION)) { session_start(); }
include "sys_database.php";
include "../sys_functions.php";
include "sys_ssp.class.php";'.$crlf.$crlf;

                             $data .= '// action: read_record        LOAD SINGLE RECORD FROM '.$tablename.' FOR MODAL EDIT'.$crlf;
if ($tablename_rows != "") { $data .= '// action: read_record_row    LOAD SINGLE ROWS RECORD FROM '.$tablename_rows.' FOR MODAL EDIT'.$crlf; }
                             $data .= '// action: load_table         LOAD TABLE '.$tablename.$crlf;
if ($tablename_rows != "") { $data .= '// action: load_table_rows    LOAD TABLE '.$tablename_rows.$crlf; }
                             $data .= '// action: button_record      RETURN BUTTON (to put inside datatable) FOR SINGLE RECORD ON '.$tablename.$crlf;
if ($tablename_rows != "") { $data .= '// action: button_record_row  RETURN BUTTON (to put inside datatable) FOR SINGLE RECORD ON '.$tablename_rows.$crlf; }
                             $data .= '// action: delete_record      DELETE RECORD ON '.$tablename.$crlf;
if ($tablename_rows != "") { $data .= '// action: delete_record_row  DELETE RECORD ON '.$tablename_rows.$crlf; }
                             $data .= '// action: save_record        SAVE RECORD ON '.$tablename.$crlf;
if ($tablename_rows != "") { $data .= '// action: save_record_row    SAVE RECORD ON '.$tablename_rows.$crlf; }


$data .= '

if(isset($_POST[\'action\'])) {

    
    if (filter_input(INPUT_POST, \'action\', FILTER_SANITIZE_STRING) == \'read_record\') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight('.$id_page.', "read")) {echo json_encode("NOT AUTHORIZED"); die();}}
        $id = filter_input(INPUT_POST, \'id\', FILTER_VALIDATE_INT);
        $risultato = array();
        $qry = "SELECT * FROM '.$tablename.' WHERE '.$tablename.'.id = ".$id;
        $query = $db->query($qry);
        while ($risultati = mysqli_fetch_array($query))
        {'.$crlf;


$query2 = $db->query($query_fields);
while ($risultati2 = mysqli_fetch_array($query2))
{
    if ($risultati2['data_type'] == 'time') {
        $data .= '            if (is_null($risultati["'.$risultati2['column_name'].'"])) { $risultato["record_'.$risultati2['column_name'].'"] = ""; } else { $risultato["record_'.$risultati2['column_name'].'"] = date_format( date_create($risultati["'.$risultati2['column_name'].'"]), \'H:i\'); }'.$crlf;
    } else {
        $data .= '            $risultato["record_'.$risultati2['column_name'].'"] = $risultati["'.$risultati2['column_name'].'"];'.$crlf;
    }
}


$data .= '
        }
        echo json_encode($risultato);
    }'.$crlf.$crlf;



if ($tablename_rows != "") {
$data .= ' 
    if (filter_input(INPUT_POST, \'action\', FILTER_SANITIZE_STRING) == \'read_record_row\') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight('.$id_page.', "read")) {echo json_encode("NOT AUTHORIZED"); die();}}
        $qry = "SELECT * FROM '.$tablename_rows.' WHERE id = " . filter_input(INPUT_POST, \'id\', FILTER_SANITIZE_STRING);
        $query = $db->query($qry);
        $risultati = mysqli_fetch_array($query);
        echo json_encode($risultati);
    }'.$crlf;
}

    
$data .= '
    if (filter_input(INPUT_POST, \'action\', FILTER_SANITIZE_STRING) == \'load_table\') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight('.$id_page.', "read")) {echo json_encode("NOT AUTHORIZED"); die();}}
        $table = "'.$tablename;

$query2 = $db->query($query_fields);
while ($risultati2 = mysqli_fetch_array($query2))
{
    if (substr($risultati2['column_name'], 0, 3) == "id_") {
        $data .= ' LEFT JOIN '.substr($risultati2['column_name'], 3).' ON '.substr($risultati2['column_name'], 3).'.id = '.$tablename.'.'.$risultati2['column_name'];
    }
}
$data .='";
        $primaryKey = \'' . $tablename . '.id\';
        $columns = array('.$crlf;


$contatore = 0;
$query2 = $db->query($query_fields);
while ($risultati2 = mysqli_fetch_array($query2))
{
    if (($risultati2['data_type'] != 'blob') && ($risultati2['data_type'] != "mediumblob") && ($risultati2['data_type'] != "longblob")) {
        if (substr($risultati2['column_name'], 0, 3) == "id_") {
            $nome_campo_descrittivo = "";
            $query_campo_descrittivo = "SELECT column_name FROM information_schema.columns WHERE ordinal_position > 1 AND table_name = '".substr($risultati2["column_name"], 3)."' and table_schema = '".$db_database."'  and (data_type = 'varchar' OR data_type = 'char' OR data_type = 'text' OR data_type = 'tinytext' OR data_type = 'mediumtext' OR data_type = 'longtext') ORDER BY ordinal_position ASC LIMIT 1 ";
            $query3 = $db->query($query_campo_descrittivo);
            while ($risultati3 = mysqli_fetch_array($query3))
            {
                $nome_campo_descrittivo = $risultati3['column_name'];
            }
            $data .= '        array( "db" => "'.substr($risultati2['column_name'], 3).'.'.$nome_campo_descrittivo.' as desc_'.substr($risultati2['column_name'], 3).'", "dt" => '.$contatore.', "field" => "desc_'.substr($risultati2['column_name'], 3).'" ), '.$crlf;
        } elseif ($risultati2['data_type'] == 'date') {
            $data .= '        array( "db" => "DATE_FORMAT('.$tablename.'.'.$risultati2['column_name'].', \''.$date_format.'\') as '.$risultati2['column_name'].'", "dt" => '.$contatore.', "field" => "'.$risultati2['column_name'].'" ), '.$crlf;
        } elseif ($risultati2['data_type'] == 'time') {
            $data .= '        array( "db" => "TIME_FORMAT('.$tablename.'.'.$risultati2['column_name'].', \'%H:%i\') as '.$risultati2['column_name'].'", "dt" => '.$contatore.', "field" => "'.$risultati2['column_name'].'" ), '.$crlf;
        } elseif (($risultati2['data_type'] == 'tinyint') || ($risultati2['data_type'] == 'bit')) {
            $data .= '        array( "db" => "IF('.$tablename.'.'.$risultati2['column_name'].' = 1, \"<i class=\'icon-check\'></i>\", \"<i class=\'icon-check-empty\'></i>\") as '.$risultati2['column_name'].'", "dt" => '.$contatore.', "field" => "'.$risultati2['column_name'].'" ), '.$crlf;
        } elseif ($risultati2['data_type'] == 'decimal') {
            if ($settings_decimal_separator == 0) {
                $data .= '        array( "db" => "REPLACE('.$tablename.'.'.$risultati2['column_name'].', \'.\', \',\') as '.$risultati2['column_name'].'", "dt" => '.$contatore.', "field" => "'.$risultati2['column_name'].'" ), '.$crlf;
            } else {
                $data .= '        array( "db" => "'.$tablename.'.'.$risultati2['column_name'].'", "dt" => '.$contatore.', "field" => "'.$risultati2['column_name'].'" ), '.$crlf;
            }
        } else {
            $data .= '        array( "db" => "'.$tablename.'.'.$risultati2['column_name'].'", "dt" => '.$contatore.', "field" => "'.$risultati2['column_name'].'" ), '.$crlf;
        }
        $contatore += 1;
    }
}
$data .= '        array( "db" => "emptycolumn", "dt" => '.$contatore.', "field" => "" ), 
        );

        $sql_details = array(
            "user" => $db_username,
            "pass" => $db_password,
            "db"   => $db_database,
            "host" => $db_host,
        );'.$crlf;


$query2 = $db->query($query_fields);
while ($risultati2 = mysqli_fetch_array($query2))
{
    if ($risultati2['column_name'] != 'id') {
        if (substr($risultati2['column_name'], 0, 3) == 'id_') {
            $data .= '        $filter'.$risultati2['column_name'].' = filter_input(INPUT_POST, "filter'.$risultati2['column_name'].'", FILTER_SANITIZE_STRING);'.$crlf;
        } elseif (($risultati2['data_type'] == 'date') || ($risultati2['data_type'] == 'int') || ($risultati2['data_type'] == 'smallint') || ($risultati2['data_type'] == 'mediumint') || ($risultati2['data_type'] == 'bigint') || ($risultati2['data_type'] == 'decimal') || ($risultati2['data_type'] == 'time')) {
            $data .= '        $filter'.$risultati2['column_name'].'_from = filter_input(INPUT_POST, "filter'.$risultati2['column_name'].'_from", FILTER_SANITIZE_STRING);'.$crlf;
            $data .= '        $filter'.$risultati2['column_name'].'_to = filter_input(INPUT_POST, "filter'.$risultati2['column_name'].'_to", FILTER_SANITIZE_STRING);'.$crlf;
        } else {
            $data .= '        $filter'.$risultati2['column_name'].' = filter_input(INPUT_POST, "filter'.$risultati2['column_name'].'", FILTER_SANITIZE_STRING);'.$crlf;
        }
    }
}


$data .= '
        $where = " '.$tablename.'.id >= 0 ";'.$crlf;


$query2 = $db->query($query_fields);
while ($risultati2 = mysqli_fetch_array($query2))
{
    if ($risultati2['column_name'] != 'id') {
        if (substr($risultati2['column_name'], 0, 3) == "id_") {
            $data .= '        if ($filter'.$risultati2["column_name"].'  != "") { $where .= " and '.$tablename.'.'.$risultati2["column_name"].' = " . $filter'.$risultati2["column_name"].' . " "; $_SESSION["'.$tablename.'_filter_'.$risultati2["column_name"].'"] = $filter'.$risultati2["column_name"].'; } else { unset($_SESSION["'.$tablename.'_filter_'.$risultati2["column_name"].'"]); } '.$crlf;
        } elseif ($risultati2['data_type'] == 'date') {
            $data .= '        if ($filter'.$risultati2["column_name"].'_from != "") { $where .= " and '.$tablename.'.'.$risultati2["column_name"].' >= \'" . $filter'.$risultati2["column_name"].'_from . "\' "; $_SESSION["'.$tablename.'_filter_'.$risultati2["column_name"].'_from"] = $filter'.$risultati2["column_name"].'_from; } else {unset($_SESSION["'.$tablename.'_filter_'.$risultati2["column_name"].'_from"]); } '.$crlf;
            $data .= '        if ($filter'.$risultati2["column_name"].'_to   != "") { $where .= " and '.$tablename.'.'.$risultati2["column_name"].' <= \'" . $filter'.$risultati2["column_name"].'_to . "\' ";   $_SESSION["'.$tablename.'_filter_'.$risultati2["column_name"].'_to"] =   $filter'.$risultati2["column_name"].'_to; }   else {unset($_SESSION["'.$tablename.'_filter_'.$risultati2["column_name"].'_to"]); } '.$crlf;
        } elseif ($risultati2['data_type'] == 'time') {
            $data .= '        if ($filter'.$risultati2["column_name"].'_from  != "") { $where .= " and '.$tablename.'.'.$risultati2["column_name"].' >= \'" . $filter'.$risultati2["column_name"].'_from . "\' "; $_SESSION["'.$tablename.'_filter_'.$risultati2["column_name"].'_from"] = $filter'.$risultati2["column_name"].'_from; } else { unset($_SESSION["'.$tablename.'_filter_'.$risultati2["column_name"].'_from"]); } '.$crlf;
            $data .= '        if ($filter'.$risultati2["column_name"].'_to  != "") { $where .= " and '.$tablename.'.'.$risultati2["column_name"].' <= \'" . $filter'.$risultati2["column_name"].'_to . "\' "; $_SESSION["'.$tablename.'_filter_'.$risultati2["column_name"].'_to"] = $filter'.$risultati2["column_name"].'_to; } else { unset($_SESSION["'.$tablename.'_filter_'.$risultati2["column_name"].'_to"]); } '.$crlf;
        } elseif (($risultati2['data_type'] == 'int') || ($risultati2['data_type'] == 'smallint') || ($risultati2['data_type'] == 'mediumint') || ($risultati2['data_type'] == 'bigint') || ($risultati2['data_type'] == 'decimal')) {
            $data .= '        if ($filter'.$risultati2["column_name"].'_from  != "") { $where .= " and '.$tablename.'.'.$risultati2["column_name"].' >= " . str_replace(",", ".", $filter'.$risultati2["column_name"].'_from) . " "; $_SESSION["'.$tablename.'_filter_'.$risultati2["column_name"].'_from"] = $filter'.$risultati2["column_name"].'_from; } else { unset($_SESSION["'.$tablename.'_filter_'.$risultati2["column_name"].'_from"]); } '.$crlf;
            $data .= '        if ($filter'.$risultati2["column_name"].'_to  != "") { $where .= " and '.$tablename.'.'.$risultati2["column_name"].' <= " . str_replace(",", ".", $filter'.$risultati2["column_name"].'_to) . " "; $_SESSION["'.$tablename.'_filter_'.$risultati2["column_name"].'_to"] =   $filter'.$risultati2["column_name"].'_to; } else {   unset($_SESSION["'.$tablename.'_filter_'.$risultati2["column_name"].'_to"]); } '.$crlf;
        } elseif (($risultati2['data_type'] == 'tinyint') || ($risultati2['data_type'] == 'bit')) {
            $data .= '        if ($filter'.$risultati2["column_name"].' == "1") { $where .= " and '.$tablename.'.'.$risultati2["column_name"].' = 1; "; $_SESSION["'.$tablename.'_filter_'.$risultati2["column_name"].'"] = $filter'.$risultati2["column_name"].'; } else { unset($_SESSION["'.$tablename.'_filter_'.$risultati2["column_name"].'"]); } '.$crlf;
        } else {
            $data .= '        if ($filter'.$risultati2["column_name"].' != "") { $where .= " and '.$tablename.'.'.$risultati2["column_name"].' like \'%" . $filter'.$risultati2["column_name"].' . "%\' "; $_SESSION["'.$tablename.'_filter_'.$risultati2["column_name"].'"] = $filter'.$risultati2["column_name"].'; } else { unset($_SESSION["'.$tablename.'_filter_'.$risultati2["column_name"].'"]); } '.$crlf;
        }
    }
}


$data .= '
        $fixedOrder = "";
        echo json_encode(
            SSP::complex( $_POST, $sql_details, $table, $primaryKey, $columns, $where, $fixedOrder )
        );
    }'.$crlf;


if ($tablename_rows != "") {

    $data .= '
    if (filter_input(INPUT_POST, \'action\', FILTER_SANITIZE_STRING) == \'load_table_rows\') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight('.$id_page.', "read")) {die();}}'.$crlf;

    $join_query = "";
    $query3 = $db->query($query_fields_rows);
    while ($risultati3 = mysqli_fetch_array($query3))
    {
        if ($join_query == "") {
            $join_query = '        $table = "'.$tablename_rows;
        }
        if (substr($risultati3['column_name'], 0, 3) == "id_") {
            $join_query .= ' LEFT JOIN '.substr($risultati3['column_name'], 3).' ON '.substr($risultati3['column_name'], 3).'.id = '.$tablename_rows.'.'.$risultati3['column_name'];
        }
        
    }
    $data .= $join_query.'";
        $primaryKey = "'.$tablename_rows.'.id";
        $columns = array('.$crlf;


    $contatore = 0;
    $query3 = $db->query($query_fields_rows);
    while ($risultati3 = mysqli_fetch_array($query3)) {
        if (($risultati3['column_name'] != "id_".$tablename) && ($risultati3['data_type'] != "blob") && ($risultati3['data_type'] != "mediumblob") && ($risultati3['data_type'] != "longblob")) {
            if (substr($risultati3['column_name'], 0, 3) == "id_") {
                $nome_campo_descrittivo = "";
                $query_campo_descrittivo = "SELECT column_name FROM information_schema.columns WHERE ordinal_position > 1 AND table_name = '".substr($risultati3["column_name"], 3)."' and table_schema = '".$db_database."' and (data_type = 'varchar' OR data_type = 'char' OR data_type = 'text' OR data_type = 'tinytext' OR data_type = 'mediumtext' OR data_type = 'longtext')  ORDER BY ordinal_position ASC LIMIT 1 ";
                $query4 = $db->query($query_campo_descrittivo);
                while ($risultati4 = mysqli_fetch_array($query4)) {
                    $nome_campo_descrittivo = $risultati4['column_name'];
                }
                $data .= '            array( "db" => "'.substr($risultati3['column_name'], 3).'.'.$nome_campo_descrittivo.'", "dt" => '.$contatore.', "field" => "'.$nome_campo_descrittivo.'" ), '.$crlf;
            } elseif ($risultati3['data_type'] == 'date') {
                $data .= '            array( "db" => "DATE_FORMAT('.$tablename_rows.'.'.$risultati3['column_name'].', \''.$date_format.'\') as '.$risultati3['column_name'].'", "dt" => '.$contatore.', "field" => "'.$risultati3['column_name'].'" ), '.$crlf;
            } elseif ($risultati3['data_type'] == 'time') {
                $data .= '            array( "db" => "TIME_FORMAT('.$tablename_rows.'.'.$risultati3['column_name'].', \'%H:%i\') as '.$risultati3['column_name'].'", "dt" => '.$contatore.', "field" => "'.$risultati3['column_name'].'" ), '.$crlf;
            } elseif (($risultati3['data_type'] == 'tinyint') || ($risultati3['data_type'] == 'bit')) {
                $data .= '            array( "db" => "IF('.$tablename_rows.'.'.$risultati3['column_name'].' = 1, \"<i class=\'icon-check\'></i>\", \"<i class=\'icon-check-empty\'></i>\") as '.$risultati3['column_name'].'", "dt" => '.$contatore.', "field" => "'.$risultati3['column_name'].'" ), '.$crlf;
            } elseif ($risultati3['data_type'] == 'decimal') {
                if ($settings_decimal_separator == 0) {
                    $data .= '            array( "db" => "REPLACE('.$tablename_rows.'.'.$risultati3['column_name'].', \'.\', \',\') as '.$risultati3['column_name'].'", "dt" => '.$contatore.', "field" => "'.$risultati3['column_name'].'" ), '.$crlf;
                } else {
                    $data .= '            array( "db" => "'.$tablename_rows.'.'.$risultati3['column_name'].'", "dt" => '.$contatore.', "field" => "'.$risultati3['column_name'].'" ), '.$crlf;
                }
            } else {
                $data .= '            array( "db" => "'.$tablename_rows.'.'.$risultati3['column_name'].'", "dt" => '.$contatore.', "field" => "'.$risultati3['column_name'].'" ), '.$crlf;
            }
            $contatore += 1;
        }
    }
    $data .= '            array( "db" => "emptycolumn", "dt" => '.$contatore.', "field" => "" ),
        );

        $sql_details = array(
            "user" => $db_username,
            "pass" => $db_password,
            "db"   => $db_database,
            "host" => $db_host,
        );

        $filter_id = filter_input(INPUT_POST, "filter_id", FILTER_SANITIZE_STRING);
        $where = " '.$tablename_rows.'.id >= 0 ";
        if ($filter_id != "") { $where .= " and '.$tablename_rows.'.id_'.$tablename.' = " . $filter_id . " "; } 
        
        $fixedOrder = "";
        echo json_encode(
            SSP::complex( $_POST, $sql_details, $table, $primaryKey, $columns, $where, $fixedOrder )
        );
    }'.$crlf;

}


$data .= '

    if (filter_input(INPUT_POST, \'action\', FILTER_SANITIZE_STRING) == \'button_record\') {
        $id = filter_input(INPUT_POST, \'id\', FILTER_SANITIZE_STRING);
        $html_button = \'<div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown" aria-expanded="false"><i class="icon-th-list-1"></i></a>
                                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(22px, 19px, 0px);">\';
        
        $button_edit = true;
        $button_delete = true;
        if ($_SESSION["user_username"] != "sysadmin") {
            if (!checkUserRight('.$id_page.', "edit")) { $button_edit = false; }
            if (!checkUserRight('.$id_page.', "delete")) { $button_delete = false; }
        }'.$crlf;

if ($tablename_rows == "") {
    $data .= '        if ($button_edit) { $html_button .= \'<a href="#" onclick="edit_record(\'.$id.\');" class="dropdown-item"><i class="icon-pencil"></i> Edit</a>\'; }';
    $data .= '        else { $html_button .= \'<a href="#" onclick="edit_record(\'.$id.\');" class="dropdown-item"><i class="icon-eye"></i> View</a>\'; }';
} else {
    $data .= '        if ($button_edit) { $html_button .= \'<a href="'.$file_name_edit.'?id=\'.$id.\'" class="dropdown-item"><i class="icon-pencil"></i> Edit</a>\'; }';
    $data .= '        else { $html_button .= \'<a href="'.$file_name_edit.'?id=\'.$id.\'" class="dropdown-item"><i class="icon-eye"></i> View</a>\'; }';
}

$data .= '

        // CHECK IF RECORD CAN BE DELETED
        $external_usage = 0;'.$crlf;


$query_ricerca_campi_collegati = "SELECT table_name FROM information_schema.columns WHERE column_name = 'id_".$tablename."' and table_name <> '".$tablename_rows."' and table_schema = '".$db_database."'";
$query2 = $db->query($query_ricerca_campi_collegati);
while ($risultati2 = mysqli_fetch_array($query2))
{
    $data .= '
        $query = $db->query("SELECT count(*) as conteggio FROM '.$risultati2['table_name'].' WHERE id_'.$tablename.' = ".$id);
        while ($risultati = mysqli_fetch_array($query)) {
            if ($risultati["conteggio"] <> "") { if (intval($risultati["conteggio"]) > 0) { $external_usage += 1; }}
        }'.$crlf;
}


$data .= '
        if ($button_delete) {
            if ($external_usage == 0) {
                $html_button .= \'<a href="#" class="dropdown-item" onclick="delete_record(\'.filter_input(INPUT_POST, \'id\', FILTER_SANITIZE_STRING).\');"><i class="icon-trash"></i> Delete</a>\';
            } else {
                $html_button .= \'<a href="#" class="dropdown-item" style="color:#dddddd;"><i class="icon-trash"></i> Delete</a>\';
            }
        }

        $html_button .= \'</div></div></div>\';
        echo $html_button;
    }'.$crlf;


if ($tablename_rows != "") {
$data .= '

    
    if (filter_input(INPUT_POST, \'action\', FILTER_SANITIZE_STRING) == \'button_record_row\') {
        $id = filter_input(INPUT_POST, \'id\', FILTER_SANITIZE_STRING);
        $html_button = \'<div class="list-icons">
                            <div class="dropdown">
                                <a href="#" class="list-icons-item" data-toggle="dropdown" aria-expanded="false"><i class="icon-th-list-1"></i></a>
                                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(22px, 19px, 0px);">\';
        
        $button_edit = true;
        $button_delete = true;
        if ($_SESSION["user_username"] != "sysadmin") {
            if (!checkUserRight('.$id_page.', "edit")) { $button_edit = false; }
            if (!checkUserRight('.$id_page.', "delete")) { $button_delete = false; }
        }
        
        if ($button_edit) {
            $html_button .= \'<a href="#" onclick="edit_row(\'.$id.\');" class="dropdown-item"><i class="icon-pencil"></i> Edit</a>\';
        } else {
            $html_button .= \'<a href="#" onclick="edit_row(\'.$id.\');" class="dropdown-item"><i class="icon-eye"></i> View</a>\';
        }

        // CHECK IF RECORD CAN BE DELETED
        $external_usage = 0;';

    $query_search_connected_fields = "SELECT table_name FROM information_schema.columns WHERE column_name = 'id_".$tablename_rows."' and table_schema = '".$db_database."'";
    $query2 = $db->query($query_search_connected_fields);
    while ($risultati2 = mysqli_fetch_array($query2))
    {
        $data .= '
        $query = $db->query("SELECT count(*) as conteggio FROM '.$risultati2['table_name'].' WHERE id_'.$tablename_rows.' = ".$id);
        while ($risultati = mysqli_fetch_array($query)) {
            if ($risultati["conteggio"] <> "") { if (intval($risultati["conteggio"]) > 0) { $external_usage += 1; }}
        }'.$crlf;
    }

    $data .= '
        if ($button_delete) {
            if ($external_usage == 0) {
                $html_button .= \'<a href="#" class="dropdown-item" onclick="delete_row(\'.filter_input(INPUT_POST, \'id\', FILTER_SANITIZE_STRING).\');"><i class="icon-trash"></i>Delete</a>\';
            } else {
                $html_button .= \'<a href="#" class="dropdown-item" style="color:#dddddd;"><i class="icon-trash"></i>Delete</a>\';
            }
        }
        
        $html_button .= "</div></div></div>";
        echo $html_button;
    }'.$crlf;
}


$data .= '

    
    if (filter_input(INPUT_POST, \'action\', FILTER_SANITIZE_STRING) == \'delete_record\') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight('.$id_page.', "delete")) {echo json_encode("NOT AUTHORIZED"); die();}}
        $id = filter_input(INPUT_POST, \'id\', FILTER_SANITIZE_STRING);'.$crlf;


if ($tablename_rows != "") {
    $data .= '        $db->query("DELETE FROM '.$tablename_rows.' WHERE id_'.$tablename.' = ".$id);'.$crlf;
}


$data .= '
        $db->query("DELETE FROM '.$tablename.' WHERE id = ".$id);
        echo "OK";
    }'.$crlf;


if ($tablename_rows != "") {
$data .= '
    if (filter_input(INPUT_POST, \'action\', FILTER_SANITIZE_STRING) == \'delete_record_row\') {
        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight('.$id_page.', "delete")) {echo json_encode("NOT AUTHORIZED"); die();}}
        $id = filter_input(INPUT_POST, \'id\', FILTER_SANITIZE_STRING);
        $db->query("DELETE FROM '.$tablename_rows.' WHERE id = ".$id);
        echo "OK";
    }'.$crlf;
}

$data .= '
    if (filter_input(INPUT_POST, \'action\', FILTER_SANITIZE_STRING) == \'save_record\') {
        
        $id = filter_input(INPUT_POST, \'id\', FILTER_SANITIZE_STRING);'.$crlf;


$query2 = $db->query($query_fields);
while ($risultati2 = mysqli_fetch_array($query2))
{
    if (($risultati2['column_name'] != 'id') && ($risultati2['data_type'] != 'blob') && ($risultati2['data_type'] != "mediumblob") && ($risultati2['data_type'] != "longblob")) {
        $data .= '        $input_'.$risultati2["column_name"].' = filter_input(INPUT_POST, "'.$risultati2['column_name'].'", FILTER_SANITIZE_STRING);'.$crlf;
        if (substr($risultati2['column_name'], 0, 3) == "id_") {
            $data .= '        if ($input_'.$risultati2["column_name"].' == 0) { $input_'.$risultati2['column_name'].' = null; }'.$crlf;
        }
        if (($risultati2['data_type'] == 'date') || ($risultati2['data_type'] == 'time') || ($risultati2['data_type'] == 'int') || ($risultati2['data_type'] == 'smallint') || ($risultati2['data_type'] == 'mediumint') || ($risultati2['data_type'] == 'bigint')) {
            $data .= '        if ($input_'.$risultati2["column_name"].' == "") { $input_'.$risultati2["column_name"].' = null; }'.$crlf;
        }
    } else {
        if (($risultati2['data_type'] == 'blob') || ($risultati2['data_type'] == "mediumblob") || ($risultati2['data_type'] == "longblob")) {
            $data .= '        $input_FILELIST_'.$risultati2['column_name'].' = html_entity_decode(filter_input(INPUT_POST, "'.$risultati2['column_name'].'", FILTER_SANITIZE_STRING));'.$crlf;
        }
    }
}




$data .= '        $data = ['.$crlf;
$query2 = $db->query($query_fields);
while ($risultati2 = mysqli_fetch_array($query2))
{
    if (($risultati2['column_name'] != 'id') && ($risultati2['data_type'] != 'blob') && ($risultati2['data_type'] != "mediumblob") && ($risultati2['data_type'] != "longblob")) {
        $dataType = "s";
        if (($risultati2['data_type'] == "int") || ($risultati2['data_type'] == "smallint") || ($risultati2['data_type'] == "mediumint") || ($risultati2['data_type'] == "bigint")) { $dataType = "i"; }
        if (($risultati2['data_type'] == "tinyint") || ($risultati2['data_type'] == 'bit')) { $dataType = "i"; }
        if ($risultati2['data_type'] == "decimal") { $dataType = "d"; }
        $data .= '            "'.$dataType.'", $input_'.$risultati2["column_name"].','.$crlf;
    } else {
        if (($risultati2['data_type'] == 'blob') || ($risultati2['data_type'] == "mediumblob") || ($risultati2['data_type'] == "longblob")) {
            $data .= '            "s", $input_FILELIST_'.$risultati2["column_name"].','.$crlf;
        }
    }
}
$data .= '        ];

        // (Types: "s" = string, "i" = integer, "d" = double, "b" = blob)
        if ($id == "") {
            if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight('.$id_page.', "create")) {echo json_encode("NOT AUTHORIZED"); die();}}
            $sql = "INSERT INTO '.$tablename.' (';
$query2 = $db->query($query_fields);
while ($risultati2 = mysqli_fetch_array($query2))
{
    if ($risultati2['column_name'] != 'id') {
        $data .= $risultati2['column_name'].', ';
    }
}
$data = substr($data, 0, strlen($data)-2);
$data .= ') VALUES (';
$query2 = $db->query($query_fields);
while ($risultati2 = mysqli_fetch_array($query2))
{
    if ($risultati2['column_name'] != 'id') {
        $data .= '?, ';
    }
}
$data = substr($data, 0, strlen($data)-2);
$data .= ')";
            $retv = $db->prepare_and_execute($sql, $data);
        } else {
            if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight('.$id_page.', "edit")) {echo json_encode("NOT AUTHORIZED"); die();}}
            array_push($data, "i", $id);
            $sql = "UPDATE '.$tablename.' SET ';
$query2 = $db->query($query_fields);
while ($risultati2 = mysqli_fetch_array($query2))
{
    if ($risultati2['column_name'] != 'id') {
        $data .= $risultati2['column_name'].'=?, ';
    }
}
$data = substr($data, 0, strlen($data)-2);
$data .= ' WHERE id=?";
            $retv = $db->prepare_and_execute($sql, $data);
        }
        if ($retv == 1) {'.$crlf;


if ($tablename_rows != "") {
        $data .= '
            if ($id == "") {
                $query = $db->query("SELECT id FROM '.$tablename.' ORDER BY id DESC LIMIT 1");
                while ($risultati = mysqli_fetch_array($query)) { $nuovo_id = $risultati["id"]; }
                $db->query("UPDATE '.$tablename_rows.' SET id_'.$tablename.' = ".$nuovo_id." WHERE id_'.$tablename.' = -".$_SESSION["user_id"]);
            }'.$crlf;
}

$data .= '
            echo "OK";
        } else {
            echo "Error: ".$retv;
        }
    }
    
    if (filter_input(INPUT_POST, "action", FILTER_SANITIZE_STRING) == "save_record_row") {
        $id = filter_input(INPUT_POST, "id", FILTER_SANITIZE_STRING);'.$crlf;


$query2 = $db->query($query_fields_rows);
while ($risultati2 = mysqli_fetch_array($query2))
{
    if (($risultati2['column_name'] != 'id') && ($risultati2['data_type'] != 'blob') && ($risultati2['data_type'] != "mediumblob") && ($risultati2['data_type'] != "longblob")) {
        $data .= '        $input_'.$risultati2['column_name'].' = filter_input(INPUT_POST, "'.$risultati2['column_name'].'", FILTER_SANITIZE_STRING);'.$crlf;
        if (substr($risultati2['column_name'], 0, 3) == "id_") {
            $data .= '        if ($input_'.$risultati2['column_name'].' == 0) { $input_'.$risultati2['column_name'].' = null; }'.$crlf;
        }
        if (($risultati2['data_type'] == 'date') || ($risultati2['data_type'] == 'time') || ($risultati2['data_type'] == 'int') || ($risultati2['data_type'] == 'smallint') || ($risultati2['data_type'] == 'mediumint') || ($risultati2['data_type'] == 'bigint')) {
            $data .= '        if ($input_'.$risultati2['column_name'].' == "") { $input_'.$risultati2['column_name'].' = null; }'.$crlf;
        }
    }
    if (($risultati2['data_type'] == 'blob') || ($risultati2['data_type'] == "mediumblob") || ($risultati2['data_type'] == "longblob")) {
        $data .= '        $input_FILELIST_'.$risultati2['column_name'].' = html_entity_decode(filter_input(INPUT_POST, "'.$risultati2['column_name'].'", FILTER_SANITIZE_STRING));'.$crlf;
    }
}



$data .= '        $data = ['.$crlf;
$query2 = $db->query($query_fields_rows);
while ($risultati2 = mysqli_fetch_array($query2))
{
    if (($risultati2['column_name'] != 'id') && ($risultati2['data_type'] != 'blob') && ($risultati2['data_type'] != "mediumblob") && ($risultati2['data_type'] != "longblob")) {
        $dataType = "s";
        if (($risultati2['data_type'] == "int") || ($risultati2['data_type'] == "smallint") || ($risultati2['data_type'] == "mediumint") || ($risultati2['data_type'] == "bigint")) { $dataType = "i"; }
        if (($risultati2['data_type'] == "tinyint") || ($risultati2['data_type'] == "bit")) { $dataType = "i"; }
        if ($risultati2['data_type'] == "decimal") { $dataType = "d"; }
        $data .= '            "'.$dataType.'", $input_'.$risultati2['column_name'].','.$crlf;
    }
    if (($risultati2['data_type'] == 'blob') || ($risultati2['data_type'] == "mediumblob") || ($risultati2['data_type'] == "longblob")) {
        $data .= '            "s", $input_FILELIST_'.$risultati2['column_name'].','.$crlf;
    }
}
$data .= '        ];

        // (Types: "s" = string, "i" = integer, "d" = double, "b" = blob)
        if ($id == "") {
            if ($_SESSION["user_username"] != "sysadmin") {
                if ((!checkUserRight('.$id_page.', "create")) && (!checkUserRight('.$id_page.', "edit"))) {
                    echo json_encode("NOT AUTHORIZED");
                    die();
                }
            }
            $sql = "INSERT INTO '.$tablename_rows.' (';
$query2 = $db->query($query_fields_rows);
while ($risultati2 = mysqli_fetch_array($query2))
{
    if ($risultati2['column_name'] != 'id') {
        $data .= $risultati2['column_name'].', ';
    }
}
$data = substr($data, 0, strlen($data)-2);
$data .= ') VALUES (';
$query2 = $db->query($query_fields_rows);
while ($risultati2 = mysqli_fetch_array($query2))
{
    if ($risultati2['column_name'] != 'id') {
        $data .= '?, ';
    }
}
$data = substr($data, 0, strlen($data)-2);
$data .= ')";';
$data .= '
            $retv = $db->prepare_and_execute($sql, $data);
        } else {
            if ($_SESSION["user_username"] != "sysadmin") {
                if ((!checkUserRight('.$id_page.', "create")) && (!checkUserRight('.$id_page.', "edit"))) {
                    echo json_encode("NOT AUTHORIZED");
                    die();
                }
            }';

$data .= '
            array_push($data, "i", $id);
            $sql = "UPDATE '.$tablename_rows.' SET ';

$query2 = $db->query($query_fields_rows);
while ($risultati2 = mysqli_fetch_array($query2))
{
    if ($risultati2['column_name'] != 'id') {
        $data .= $risultati2['column_name'].'=?, ';
    }
}
$data = substr($data, 0, strlen($data)-2);
$data .= ' WHERE id=?";
            $retv = $db->prepare_and_execute($sql, $data);
        }
        if ($retv == 1) {
            echo "OK";
        } else {
            echo "Error: ".$retv;
        }
    }
}';


                    file_put_contents ($file_name_controller, $data);
                    if (file_exists($file_name_controller)) {
                        $info_execution .= '<div><span style="font-weight: bold;">'.$file_name_controller.'</span></div>';
                    } else {
                        $error_execution .= '<div>Errors creating '.$file_name_controller.'</div>';
                    }

                    $return_execution = "";
                    if (strlen($info_execution_prefix)>0) $return_execution = $info_execution_prefix;
                    if (strlen($info_execution)>0) $return_execution .= '<div>New files created:</div>'.$info_execution;
                    if (strlen($error_execution)>0) $return_execution .= '<div>&nbsp;</div>'.$error_execution;
                    

                    $ret = ["OK", $return_execution];
                }
            
        } else {
            $ret = ["KO", "No connection to Database"];
        }
        echo json_encode($ret);
    }
}

?>