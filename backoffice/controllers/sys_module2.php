<?php
if (!isset($_SESSION)) { session_start(); }
include "sys_database.php";
include "../sys_functions.php";

// CONTROLLER RESTRICTED TO sysadmin
if (!isset($_SESSION['user_username'])) { die(); }
if (strtolower($_SESSION['user_username']) != 'sysadmin') { die(); }

?>

<?php if (isset($_GET['page_id'])) { ?>
    <div class="modal-content">
        <div class="modal-header bg-primary">
            <h6 class="modal-title"><span style="font-variant: small-caps; font-size: 15px;"><i class="icon-file-empty mr-2"></i>Blank page</span></h6>
        </div>
        <div class="modal-body">
            <div class="content" style="-webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none;" unselectable="on">

                <h3 style="font-variant: small-caps;">This module will generate a blank page</h3>
                
            </div>
        </div>
        <div class="modal-footer">
            <button onclick="generate();" class="btn btn-lg btn-outline-success"><i class="icon-magic mr-2"></i>Generate</button>
            <button onclick="$('#modal_module').modal('hide');" class="btn btn-lg btn-outline-warning"><i class="icon-undo mr-2"></i>Cancel</button>
        </div>
    </div>
    <script>
        function generate(){
            showOverlay();
            $.ajax({
                url: "<?=linkto(getPageName())?>",
                type: "POST",
                data: {
                    'action': 'generate',
                    'id': <?=$_GET['page_id']?>
                },
                success: function (data, stato) {
                    hideOverlay();
                    var data = $.parseJSON(data);
                    if (data[0] == 0) {
                        message_box("Error", data[1], "Ok", "error");
                    } else {
                        message_box("Completed", data[1], "Ok", "success", function_hide_modal);
                    }
                },
                error: function (data) {
                    hideOverlay();
                    message_box("Error", "Oops! Qualcosa è andato storto.", "Ok", "error");
                }
            });
        }
        function function_hide_modal(){
            $('#modal_module').modal('hide');
        }
    </script>
<?php
} 


if (isset($_POST['action'])) {
    if ($_POST['action'] == "generate") {
        $id_page = $_POST['id'];
        if ($db->status()) {
            $qry = "SELECT * FROM sys_pages WHERE id = ".$id_page;
            $rows = fetch_rows($qry);
            $file_name = "";
            foreach ( $rows as $row ) {
                $page_name = $row['page_name'];
                $icon = $row['icon'];
                $file_name = $row['file_name'].".php";
            }
            if ($file_name == "") {
                $ret = [0, "Record not found or filename not set"];
            } else {
                $file_name_controller = str_replace(".php", "_controller.php", $file_name);
                $suffix_name_backup = ".BACKUP_".time();
                $info_execution = "";
                $error_execution = "";
                $info_execution_prefix = "";
                $previous_files_exist = 0;
                if (file_exists("../".$file_name))      { $previous_files_exist += 1; rename("../".$file_name,      "../backups/".$file_name.$suffix_name_backup); }
                if (file_exists($file_name_controller)) { $previous_files_exist += 1; rename($file_name_controller, "../backups/".$file_name_controller.$suffix_name_backup); }
                if ($previous_files_exist > 0) { $info_execution_prefix .= "<div>Previous files have now suffix <span style='font-weight: bold;'>".$suffix_name_backup."</span></div><div>&nbsp;</div>"; }

                
                
                $data = '<?php
    include "sys_header.php";
    if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight('.$id_page.', "read")) {header("location: ".linkto("sys_dashboard")); die();}}
?>
<body class="navbar-top<?php if ($settings_menu_closed == 1) { echo \' sidebar-xs\'; } ?>">
    <?php include "sys_navbar.php"; ?>
    <!-- Page content -->
    <div class="page-content">
        <?php include "sys_sidebar.php"; ?>
        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
            <div class="page-header page-header-light">
				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">

					<div class="d-flex">
                        <i class="'.$icon.' mr-2 breadcrumb-item"></i><span style="margin-top: 8px;">'.$page_name.'</span>
					</div>

					<div class="header-elements d-none" style="display: flex !important;">
						<div class="breadcrumb justify-content-center">
							
						</div>
					</div>
				</div>
			</div>


            <!-- Content area -->
            <div class="content">
            </div>

        </div>	<!-- /Main content -->
    </div>	<!-- /Page content -->
</body>
</html>';

                file_put_contents ("../".$file_name, $data);
                if (file_exists("../".$file_name)) {
                    $info_execution .= "<div><span style='font-weight: bold;'>".$file_name."</span></div>";
                } else {
                    $error_execution .= "<div>Errors creating ".$file_name."</div>";
                }
                
                $data = '<?php
if (!isset($_SESSION)) { session_start(); }
include "sys_database.php";
include "../sys_functions.php";
include "sys_ssp.class.php";
if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(1, "read")) {echo json_encode("NOT AUTHORIZED"); die();}}


?>
';

                file_put_contents ($file_name_controller, $data);
                if (file_exists($file_name_controller)) {
                    $info_execution .= "<div><span style='font-weight: bold;'>".$file_name_controller."</span></div>";
                } else {
                    $error_execution .= "<div>Errors creating ".$file_name_controller."</div>";
                }

                $return_execution = "";
                if (strlen($info_execution_prefix)>0) $return_execution = $info_execution_prefix;
                if (strlen($info_execution)>0) $return_execution .= "<div>New files created:</div>".$info_execution;
                if (strlen($error_execution)>0) $return_execution .= "<div>&nbsp;</div>".$error_execution;
                
                $ret = [1, $return_execution];
                
            }
        } else {
            $ret = [0, "No connection to Database"];
        }
        echo json_encode($ret);
    }
}


 ?>