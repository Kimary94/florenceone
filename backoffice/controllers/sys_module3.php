<?php
if (!isset($_SESSION)) { session_start(); }
include "sys_database.php";
include "../sys_functions.php";

// CONTROLLER RESTRICTED TO sysadmin
if (!isset($_SESSION['user_username'])) { die(); }
if (strtolower($_SESSION['user_username']) != 'sysadmin') { die(); }

?>

<?php if (isset($_GET['page_id'])) { ?>
    <div class="modal-content">
        <div class="modal-header bg-primary">
            <h6 class="modal-title"><span style="font-variant: small-caps; font-size: 15px;"><i class="icon-globe-3 mr-2"></i>External page</span></h6>
        </div>
        <div class="modal-body">
            <div class="content" style="-webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none;" unselectable="on">

                <h3 style="font-variant: small-caps;">Insert the web-page to link</h3>
                <input type="text" class="form-control" autocomplete="off" id="webpage" value="<?=get_setting($_GET['page_id'], 'webpage');?>">
                <br>
                <label style="width:100px; font-variant: small-caps;">Method</label><label for="get_post">Get&nbsp;</label><input type="checkbox" class="form-control" autocomplete="off" id="get_post" value="1" <?php if (get_setting($_GET['page_id'], 'get_post') == 1) echo " checked "; ?>> <label for="get_post">Post</label>
                <br>
                <br>
                <label style="width:180px; font-variant: small-caps;">User values ​​to be passed</label><label style="width:100px; font-variant: small-caps;">Name</label>
                <br>
                <input type="checkbox" onchange="if (ischecked(this) && ($('#var_user_id').val() == ''))         { $('#var_user_id').val('user_id');                 } else { $('#var_user_id').val(''); }"         class="form-control" autocomplete="off" id="pass_user_id"         name="pass_user_id"         <?php if (get_setting($_GET['page_id'], 'pass_user_id') == 1)         echo " checked "; ?> ><label for="pass_user_id" style="margin-left: 6px; width:130px;"        >ID</label        ><input style="width:150px; display: inline-block;" type="text" class="form-control" autocomplete="off" id="var_user_id"         value="<?=get_setting($_GET['page_id'], 'var_user_id');?>">
                <br>
                <input type="checkbox" onchange="if (ischecked(this) && ($('#var_user_firstname').val() == ''))  { $('#var_user_firstname').val('user_firstname');   } else { $('#var_user_firstname').val(''); }"  class="form-control" autocomplete="off" id="pass_user_firstname"  name="pass_user_firstname"  <?php if (get_setting($_GET['page_id'], 'pass_user_firstname') == 1)  echo " checked "; ?> ><label for="pass_user_firstname" style="margin-left: 6px; width:130px;" >Firstname</label ><input style="width:150px; display: inline-block;" type="text" class="form-control" autocomplete="off" id="var_user_firstname"  value="<?=get_setting($_GET['page_id'], 'var_user_firstname');?>">
                <br>
                <input type="checkbox" onchange="if (ischecked(this) && ($('#var_user_lastname').val() == ''))   { $('#var_user_lastname').val('user_lastname');     } else { $('#var_user_lastname').val(''); }"   class="form-control" autocomplete="off" id="pass_user_lastname"   name="pass_user_lastname"   <?php if (get_setting($_GET['page_id'], 'pass_user_lastname') == 1)   echo " checked "; ?> ><label for="pass_user_lastname" style="margin-left: 6px; width:130px;"  >Lastname</label  ><input style="width:150px; display: inline-block;" type="text" class="form-control" autocomplete="off" id="var_user_lastname"   value="<?=get_setting($_GET['page_id'], 'var_user_lastname');?>">
                <br>
                <input type="checkbox" onchange="if (ischecked(this) && ($('#var_user_username').val() == ''))   { $('#var_user_username').val('user_username');     } else { $('#var_user_username').val(''); }"   class="form-control" autocomplete="off" id="pass_user_username"   name="pass_user_username"   <?php if (get_setting($_GET['page_id'], 'pass_user_username') == 1)   echo " checked "; ?> ><label for="pass_user_username" style="margin-left: 6px; width:130px;"  >Username</label  ><input style="width:150px; display: inline-block;" type="text" class="form-control" autocomplete="off" id="var_user_username"   value="<?=get_setting($_GET['page_id'], 'var_user_username');?>">
                <br>
                <input type="checkbox" onchange="if (ischecked(this) && ($('#var_user_email').val() == ''))      { $('#var_user_email').val('user_email');           } else { $('#var_user_email').val(''); }"      class="form-control" autocomplete="off" id="pass_user_email"      name="pass_user_email"      <?php if (get_setting($_GET['page_id'], 'pass_user_email') == 1)      echo " checked "; ?> ><label for="pass_user_email" style="margin-left: 6px; width:130px;"     >Email</label     ><input style="width:150px; display: inline-block;" type="text" class="form-control" autocomplete="off" id="var_user_email"      value="<?=get_setting($_GET['page_id'], 'var_user_email');?>">
                <br>
                <input type="checkbox" onchange="if (ischecked(this) && ($('#var_user_id_group').val() == ''))   { $('#var_user_id_group').val('user_id_group');     } else { $('#var_user_id_group').val(''); }"   class="form-control" autocomplete="off" id="pass_user_id_group"   name="pass_user_id_group"   <?php if (get_setting($_GET['page_id'], 'pass_user_id_group') == 1)   echo " checked "; ?> ><label for="pass_user_id_group" style="margin-left: 6px; width:130px;"  >Group ID</label  ><input style="width:150px; display: inline-block;" type="text" class="form-control" autocomplete="off" id="var_user_id_group"   value="<?=get_setting($_GET['page_id'], 'var_user_id_group');?>">
                <br>
                <input type="checkbox" onchange="if (ischecked(this) && ($('#var_user_can_read').val() == ''))   { $('#var_user_can_read').val('user_can_read');     } else { $('#var_user_can_read').val(''); }"   class="form-control" autocomplete="off" id="pass_user_can_read"   name="pass_user_can_read"   <?php if (get_setting($_GET['page_id'], 'pass_user_can_read') == 1)   echo " checked "; ?> ><label for="pass_user_can_read" style="margin-left: 6px; width:130px;"  >Can read</label  ><input style="width:150px; display: inline-block;" type="text" class="form-control" autocomplete="off" id="var_user_can_read"   value="<?=get_setting($_GET['page_id'], 'var_user_can_read');?>">
                <br>
                <input type="checkbox" onchange="if (ischecked(this) && ($('#var_user_can_create').val() == '')) { $('#var_user_can_create').val('user_can_create'); } else { $('#var_user_can_create').val(''); }" class="form-control" autocomplete="off" id="pass_user_can_create" name="pass_user_can_create" <?php if (get_setting($_GET['page_id'], 'pass_user_can_create') == 1) echo " checked "; ?> ><label for="pass_user_can_create" style="margin-left: 6px; width:130px;">Can create</label><input style="width:150px; display: inline-block;" type="text" class="form-control" autocomplete="off" id="var_user_can_create" value="<?=get_setting($_GET['page_id'], 'var_user_can_create');?>">
                <br>
                <input type="checkbox" onchange="if (ischecked(this) && ($('#var_user_can_edit').val() == ''))   { $('#var_user_can_edit').val('user_can_edit');     } else { $('#var_user_can_edit').val(''); }"   class="form-control" autocomplete="off" id="pass_user_can_edit"   name="pass_user_can_edit"   <?php if (get_setting($_GET['page_id'], 'pass_user_can_edit') == 1)   echo " checked "; ?> ><label for="pass_user_can_edit" style="margin-left: 6px; width:130px;"  >Can edit</label  ><input style="width:150px; display: inline-block;" type="text" class="form-control" autocomplete="off" id="var_user_can_edit"   value="<?=get_setting($_GET['page_id'], 'var_user_can_edit');?>">
                <br>
                <input type="checkbox" onchange="if (ischecked(this) && ($('#var_user_can_delete').val() == '')) { $('#var_user_can_delete').val('user_can_delete'); } else { $('#var_user_can_delete').val(''); }" class="form-control" autocomplete="off" id="pass_user_can_delete" name="pass_user_can_delete" <?php if (get_setting($_GET['page_id'], 'pass_user_can_delete') == 1) echo " checked "; ?> ><label for="pass_user_can_delete" style="margin-left: 6px; width:130px;">Can delete</label><input style="width:150px; display: inline-block;" type="text" class="form-control" autocomplete="off" id="var_user_can_delete" value="<?=get_setting($_GET['page_id'], 'var_user_can_delete');?>">
                <br>
                
            </div>
        </div>
        <div class="modal-footer">
            <button onclick="generate();" class="btn btn-lg btn-outline-success"><i class="icon-magic mr-2"></i>Generate</button>
            <button onclick="$('#modal_module').modal('hide');" class="btn btn-lg btn-outline-warning"><i class="icon-undo mr-2"></i>Cancel</button>
        </div>
    </div>
    <script>
    
        // ON DOCUMENT READY
        $(function() {
            $('input:checkbox').each(function() { Switchery( this ); }); // Switchery all checkboxes
            $('.switchery').css('display', 'inline-block')               // Switchery inline
            $('.switchery').css('vertical-align', 'text-bottom')         // Switchery v-align
            
        });

        function generate(){
            showOverlay();
            $.ajax({
                url: "<?=linkto(getPageName())?>",
                type: "POST",
                data: {
                    'action': 'generate',
                    'id_page': <?=$_GET['page_id']?>,
                    'webpage': $('#webpage').val(),
                    'get_post': ischecked($('#get_post')),
                    'pass_user_id': ischecked($('#pass_user_id')),
                    'pass_user_firstname': ischecked($('#pass_user_firstname')),
                    'pass_user_lastname': ischecked($('#pass_user_lastname')),
                    'pass_user_username': ischecked($('#pass_user_username')),
                    'pass_user_email': ischecked($('#pass_user_email')),
                    'pass_user_id_group': ischecked($('#pass_user_id_group')),
                    'pass_user_can_read': ischecked($('#pass_user_can_read')),
                    'pass_user_can_create': ischecked($('#pass_user_can_create')),
                    'pass_user_can_edit': ischecked($('#pass_user_can_edit')),
                    'pass_user_can_delete': ischecked($('#pass_user_can_delete')),
                    'var_user_id': $('#var_user_id').val(),
                    'var_user_firstname': $('#var_user_firstname').val(),
                    'var_user_lastname': $('#var_user_lastname').val(),
                    'var_user_username': $('#var_user_username').val(),
                    'var_user_email': $('#var_user_email').val(),
                    'var_user_id_group': $('#var_user_id_group').val(),
                    'var_user_can_read': $('#var_user_can_read').val(),
                    'var_user_can_create': $('#var_user_can_create').val(),
                    'var_user_can_edit': $('#var_user_can_edit').val(),
                    'var_user_can_delete': $('#var_user_can_delete').val()
                },
                success: function (data, stato) {
                    console.log(data);
                    hideOverlay();
                    var data = $.parseJSON(data);
                    if (data[0] == 0) {
                        message_box("Error", data[1], "Ok", "error");
                    } else {
                        message_box("Completed", data[1], "Ok", "success", function_hide_modal);
                    }
                },
                error: function (data) {
                    console.log(data);
                    hideOverlay();
                    message_box("Error", "Oops! Qualcosa è andato storto.", "Ok", "error");
                }
            });
        }
        function function_hide_modal(){
            $('#modal_module').modal('hide');
        }
    </script>
<?php
}

function save_setting($id_page, $obj_name, $obj_value) {
    global $db;
    $id_sys_module_settings = 0;
    $qry = "SELECT id FROM sys_module_settings WHERE id_page = ".$id_page." AND obj_name = '".$obj_name."'";
    $rows = fetch_rows($qry);
    foreach ( $rows as $row ) {
        $id_sys_module_settings = $row['id'];
    }
    if ($id_sys_module_settings == 0) {
        $db->query( "INSERT INTO sys_module_settings(id_page, obj_name, obj_value) VALUES (".$id_page.", '".$obj_name."', '".$obj_value."')" );
    } else {
        $db->query( "UPDATE sys_module_settings SET obj_value  = '".$obj_value."'  WHERE id = ".$id_sys_module_settings );
    }
}

function get_setting($id_page, $obj_name) {
    $obj_value = '';
    $qry = "SELECT obj_value FROM sys_module_settings WHERE id_page = ".$id_page." AND obj_name = '".$obj_name."'";
    $rows = fetch_rows($qry);
    foreach ( $rows as $row ) {
        $obj_value = $row['obj_value'];
    }
    return $obj_value;
}


if (isset($_POST['action'])) {
    if ($_POST['action'] == "generate") {
        if ($db->status()) {
            $id_page = $_POST['id_page'];
            
            save_setting($id_page, "webpage", $_POST['webpage']);
            save_setting($id_page, "get_post", $_POST['get_post']);
            save_setting($id_page, "pass_user_id", $_POST['pass_user_id']);
            save_setting($id_page, "pass_user_firstname", $_POST['pass_user_firstname']);
            save_setting($id_page, "pass_user_lastname", $_POST['pass_user_lastname']);
            save_setting($id_page, "pass_user_username", $_POST['pass_user_username']);
            save_setting($id_page, "pass_user_email", $_POST['pass_user_email']);
            save_setting($id_page, "pass_user_id_group", $_POST['pass_user_id_group']);
            save_setting($id_page, "pass_user_can_read", $_POST['pass_user_can_read']);
            save_setting($id_page, "pass_user_can_create", $_POST['pass_user_can_create']);
            save_setting($id_page, "pass_user_can_edit", $_POST['pass_user_can_edit']);
            save_setting($id_page, "pass_user_can_delete", $_POST['pass_user_can_delete']);
            save_setting($id_page, "var_user_id", $_POST['var_user_id']);
            save_setting($id_page, "var_user_firstname", $_POST['var_user_firstname']);
            save_setting($id_page, "var_user_lastname", $_POST['var_user_lastname']);
            save_setting($id_page, "var_user_username", $_POST['var_user_username']);
            save_setting($id_page, "var_user_email", $_POST['var_user_email']);
            save_setting($id_page, "var_user_id_group", $_POST['var_user_id_group']);
            save_setting($id_page, "var_user_can_read", $_POST['var_user_can_read']);
            save_setting($id_page, "var_user_can_create", $_POST['var_user_can_create']);
            save_setting($id_page, "var_user_can_edit", $_POST['var_user_can_edit']);
            save_setting($id_page, "var_user_can_delete", $_POST['var_user_can_delete']);
            
            $qry = "SELECT * FROM sys_pages WHERE id = ".$id_page;
            $rows = fetch_rows($qry);
            $file_name = "";
            foreach ( $rows as $row ) {
                $page_name = $row['page_name'];
                $icon = $row['icon'];
                $file_name = $row['file_name'].".php";
            }
            if ($file_name == "") {
                $ret = [0, "Record not found or filename not set"];
            } else {
                //$file_name_controller = str_replace(".php", "_controller.php", $file_name);
                $suffix_name_backup = ".BACKUP_".time();
                $info_execution = "";
                $error_execution = "";
                $info_execution_prefix = "";
                $previous_files_exist = 0;
                if (file_exists("../".$file_name))      { $previous_files_exist += 1; rename("../".$file_name,      "../backups/".$file_name.$suffix_name_backup); }
                //if (file_exists($file_name_controller)) { $previous_files_exist += 1; rename($file_name_controller, "../backups/".$file_name_controller.$suffix_name_backup); }
                if ($previous_files_exist > 0) { $info_execution_prefix .= "<div>Previous files have now suffix <span style='font-weight: bold;'>".$suffix_name_backup."</span></div><div>&nbsp;</div>"; }

                

                
                $data = '<?php
    include "sys_header.php";
    if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight('.$id_page.', "read")) {header("location: ".linkto("sys_dashboard")); die();}}
?>
<body class="navbar-top<?php if ($settings_menu_closed == 1) { echo \' sidebar-xs\'; } ?>">
    <?php include "sys_navbar.php"; ?>
    <!-- Page content -->
    <div class="page-content">
        <?php include "sys_sidebar.php"; ?>
        <!-- Main content -->
        <div class="content-wrapper">

            <!-- Page header -->
            <div class="page-header page-header-light">
				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">

					<div class="d-flex">
                        <i class="'.$icon.' mr-2 breadcrumb-item"></i><span style="margin-top: 8px;">'.$page_name.'</span>
					</div>

					<div class="header-elements d-none" style="display: flex !important;">
						<div class="breadcrumb justify-content-center">
							
						</div>
					</div>
				</div>
			</div>


            <!-- Content area -->
            <div class="content" style="display: flex; flex-flow: column; padding: unset;">';

                $webpage = $_POST['webpage'];

                if (intval($_POST['get_post']) == 1) {

                    // POST
                    $data .= '
                    <iframe name="myFrame" id="myFrame" style="border:none; flex: 1 1 auto;" height="1%"></iframe>
                    <form action="'.$webpage.'" method="post" target="myFrame" id="iframeLoader">';
                    
                    if ((intval($_POST['pass_user_id']) == 1) && (strlen($_POST['var_user_id']) > 0)) {
                        $data .= '
                        <input type="hidden" name="'.$_POST['var_user_id'].'" value="<?=$'.'_SESSION["user_id"]?>" />';
                    }
                    if ((intval($_POST['pass_user_firstname']) == 1) && (strlen($_POST['var_user_firstname']) > 0)) {
                        $data .= '
                        <input type="hidden" name="'.$_POST['var_user_firstname'].'" value="<?=$'.'_SESSION["user_firstname"]?>" />';
                    }
                    if ((intval($_POST['pass_user_lastname']) == 1) && (strlen($_POST['var_user_lastname']) > 0)) {
                        $data .= '
                        <input type="hidden" name="'.$_POST['var_user_lastname'].'" value="<?=$'.'_SESSION["user_lastname"]?>" />';
                    }
                    if ((intval($_POST['pass_user_username']) == 1) && (strlen($_POST['var_user_username']) > 0)) {
                        $data .= '
                        <input type="hidden" name="'.$_POST['var_user_username'].'" value="<?=$'.'_SESSION["user_username"]?>" />';
                    }
                    if ((intval($_POST['pass_user_email']) == 1) && (strlen($_POST['var_user_email']) > 0)) {
                        $data .= '
                        <input type="hidden" name="'.$_POST['var_user_email'].'" value="<?=$'.'_SESSION["user_email"]?>" />';
                    }
                    if ((intval($_POST['pass_user_id_group']) == 1) && (strlen($_POST['var_user_id_group']) > 0)) {
                        $data .= '
                        <input type="hidden" name="'.$_POST['var_user_id_group'].'" value="<?=$'.'_SESSION["user_id_group"]?>" />';
                    }
                    if ((intval($_POST['pass_user_can_read']) == 1) && (strlen($_POST['var_user_can_read']) > 0)) {
                        $data .= '
                        <input type="hidden" name="'.$_POST['var_user_can_read'].'" value="<?php if (checkUserRight('.$id_page.',\'read\')) {echo "1";} else {echo "0";}?>" />';
                    }
                    if ((intval($_POST['pass_user_can_create']) == 1) && (strlen($_POST['var_user_can_create']) > 0)) {
                        $data .= '
                        <input type="hidden" name="'.$_POST['var_user_can_create'].'" value="<?php if (checkUserRight('.$id_page.',\'create\')) {echo "1";} else {echo "0";}?>" />';
                    }
                    if ((intval($_POST['pass_user_can_edit']) == 1) && (strlen($_POST['var_user_can_edit']) > 0)) {
                        $data .= '
                        <input type="hidden" name="'.$_POST['var_user_can_edit'].'" value="<?php if (checkUserRight('.$id_page.',\'edit\')) {echo "1";} else {echo "0";}?>" />';
                    }
                    if ((intval($_POST['pass_user_can_delete']) == 1) && (strlen($_POST['var_user_can_delete']) > 0)) {
                        $data .= '
                        <input type="hidden" name="'.$_POST['var_user_can_delete'].'" value="<?php if (checkUserRight('.$id_page.',\'delete\')) {echo "1";} else {echo "0";}?>" />';
                    }
                    
                    $data .= '
                    </form>
                    <script>
                        $(document).ready(function () {
                            $("#iframeLoader").submit();
                        });
                    </script>';

                } else {

                    //GET
                    if ((intval($_POST['pass_user_id']) == 1) && (strlen($_POST['var_user_id']) > 0)) {
                        if (strpos($webpage, "?") === false) { $webpage .= "?"; } else { $webpage .= "&"; }
                        $webpage .= $_POST['var_user_id']."=<?=$"."_SESSION['user_id']?>";
                    }
                    if ((intval($_POST['pass_user_firstname']) == 1) && (strlen($_POST['var_user_firstname']) > 0)) {
                        if (strpos($webpage, "?") === false) { $webpage .= "?"; } else { $webpage .= "&"; }
                        $webpage .= $_POST['var_user_firstname']."=<?=$"."_SESSION['user_firstname']?>";
                    }
                    if ((intval($_POST['pass_user_lastname']) == 1) && (strlen($_POST['var_user_lastname']) > 0)) {
                        if (strpos($webpage, "?") === false) { $webpage .= "?"; } else { $webpage .= "&"; }
                        $webpage .= $_POST['var_user_lastname']."=<?=$"."_SESSION['user_lastname']?>";
                    }
                    if ((intval($_POST['pass_user_username']) == 1) && (strlen($_POST['var_user_username']) > 0)) {
                        if (strpos($webpage, "?") === false) { $webpage .= "?"; } else { $webpage .= "&"; }
                        $webpage .= $_POST['var_user_username']."=<?=$"."_SESSION['user_username']?>";
                    }
                    if ((intval($_POST['pass_user_email']) == 1) && (strlen($_POST['var_user_email']) > 0)) {
                        if (strpos($webpage, "?") === false) { $webpage .= "?"; } else { $webpage .= "&"; }
                        $webpage .= $_POST['var_user_email']."=<?=$"."_SESSION['user_email']?>";
                    }
                    if ((intval($_POST['pass_user_id_group']) == 1) && (strlen($_POST['var_user_id_group']) > 0)) {
                        if (strpos($webpage, "?") === false) { $webpage .= "?"; } else { $webpage .= "&"; }
                        $webpage .= $_POST['var_user_id_group']."=<?=$"."_SESSION['user_id_group']?>";
                    }
                    if ((intval($_POST['pass_user_can_read']) == 1) && (strlen($_POST['var_user_can_read']) > 0)) {
                        if (strpos($webpage, "?") === false) { $webpage .= "?"; } else { $webpage .= "&"; }
                        $webpage .= $_POST['var_user_can_read']."=<?php if (checkUserRight(".$id_page.",'read')) {echo '1';} else {echo '0';}?>";
                    }
                    if ((intval($_POST['pass_user_can_create']) == 1) && (strlen($_POST['var_user_can_create']) > 0)) {
                        if (strpos($webpage, "?") === false) { $webpage .= "?"; } else { $webpage .= "&"; }
                        $webpage .= $_POST['var_user_can_create']."=<?php if (checkUserRight(".$id_page.",'create')) {echo '1';} else {echo '0';}?>";
                    }
                    if ((intval($_POST['pass_user_can_edit']) == 1) && (strlen($_POST['var_user_can_edit']) > 0)) {
                        if (strpos($webpage, "?") === false) { $webpage .= "?"; } else { $webpage .= "&"; }
                        $webpage .= $_POST['var_user_can_edit']."=<?php if (checkUserRight(".$id_page.",'edit')) {echo '1';} else {echo '0';}?>";
                    }
                    if ((intval($_POST['pass_user_can_delete']) == 1) && (strlen($_POST['var_user_can_delete']) > 0)) {
                        if (strpos($webpage, "?") === false) { $webpage .= "?"; } else { $webpage .= "&"; }
                        $webpage .= $_POST['var_user_can_delete']."=<?php if (checkUserRight(".$id_page.",'delete')) {echo '1';} else {echo '0';}?>";
                    }
                    $data .= '
                <iframe name="myFrame" id="myFrame" style="border:none; flex: 1 1 auto;" height="1%" src="'.$webpage.'"></iframe>';
                }

                $data .= '
            </div>

        </div>	<!-- /Main content -->
    </div>	<!-- /Page content -->
</body>
</html>';

                file_put_contents ("../".$file_name, $data);
                if (file_exists("../".$file_name)) {
                    $info_execution .= "<div><span style='font-weight: bold;'>".$file_name."</span></div>";
                } else {
                    $error_execution .= "<div>Errors creating ".$file_name."</div>";
                }
/*
                $data = '<?php
if (!isset($_SESSION)) { session_start(); }
include "sys_database.php";
include "../sys_functions.php";
include "sys_ssp.class.php";
if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(1, "read")) {echo json_encode("NOT AUTHORIZED"); die();}}


?>
';

                file_put_contents ($file_name_controller, $data);
                if (file_exists($file_name_controller)) {
                    $info_execution .= "<div><span style='font-weight: bold;'>".$file_name_controller."</span></div>";
                } else {
                    $error_execution .= "<div>Errors creating ".$file_name_controller."</div>";
                }
*/
                $return_execution = "";
                if (strlen($info_execution_prefix)>0) $return_execution = $info_execution_prefix;
                if (strlen($info_execution)>0) $return_execution .= "<div>New files created:</div>".$info_execution;
                if (strlen($error_execution)>0) $return_execution .= "<div>&nbsp;</div>".$error_execution;
                
                $ret = [1, $return_execution];
                
            }
        } else {
            $ret = [0, "No connection to Database"];
        }
        echo json_encode($ret);
    }
}


 ?>