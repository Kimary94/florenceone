<?php
include "sys_database.php";


// CONTROLLER RESTRICTED TO sysadmin
if (!isset($_SESSION['user_username'])) { die(); }
if (strtolower($_SESSION['user_username']) != 'sysadmin') { die(); }


if(isset($_POST['action'])) {

    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'remove') {
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        if (intval($id) > 0) $db->query("DELETE FROM sys_pages WHERE id = ".$id);

        $qry = "SELECT id FROM sys_pages ORDER BY sorting"; 
        $rows = fetch_rows($qry);
        $counter = 10;
        foreach ( $rows as $row ) {
            $db->query("UPDATE sys_pages SET sorting = ".$counter." WHERE id = ".$row['id']);
            $counter += 10;
        }
    }

    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'read') {
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        if (intval($id) > 0) {
            if ($db->status()) { 
                $qry = "SELECT * FROM sys_pages WHERE id = ".$id; 
                $rows = fetch_rows($qry); 
                $ret = [0, 'Record not found'];
                foreach ( $rows as $row ) {
                    $ret = [1, $row];
                }
                echo json_encode($ret);
            } else {
                $ret = [0, 'No connection to Database'];
                echo json_encode($ret);
            }
        } else {
            $ret = [0, 'Invalid ID'];
            echo json_encode($ret);
        }
    }

    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'save') {
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $page_name = filter_input(INPUT_POST, 'page_name', FILTER_SANITIZE_STRING);
        $icon = filter_input(INPUT_POST, 'icon', FILTER_SANITIZE_STRING);
        $file_name = filter_input(INPUT_POST, 'file_name', FILTER_SANITIZE_STRING);
        $module = filter_input(INPUT_POST, 'module', FILTER_SANITIZE_STRING);
        if (!$db->status()) { 
            $ret = [0, 'No connection to Database'];
        } else {
            if (intval($id) > 0) {
                $qry = "UPDATE sys_pages SET page_name = '".$page_name."', icon = '".$icon."', file_name = '".strtolower($file_name)."', module = '".$module."' WHERE id = ".$id; 
                $db->query($qry);
                $ret = [1, ''];
            } else {
                $sorting = 1;
                $qry = "SELECT sorting FROM sys_pages ORDER BY sorting DESC LIMIT 1"; 
                $rows = fetch_rows($qry); 
                foreach ( $rows as $row ) {
                    $sorting = intval($row["sorting"]) + 10;
                }
                $qry = "INSERT INTO sys_pages(page_name, icon, file_name, module, sorting) VALUES ('".$page_name."', '".$icon."', '".$file_name."', '".$module."', '".$sorting."')";
                $db->query($qry);
                $ret = [1, ''];
            }
        }
        echo json_encode($ret);
    }
    
    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'move') {
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $step = filter_input(INPUT_POST, 'step', FILTER_SANITIZE_STRING);
        if (!$db->status()) { 
            $ret = [0, 'No connection to Database'];
        } else {
            $qry = "UPDATE sys_pages SET sorting = sorting " . $step . " WHERE id = ".$id; 
            $db->query($qry);

            $qry = "SELECT id FROM sys_pages ORDER BY sorting"; 
            $rows = fetch_rows($qry);
            $counter = 10;
            foreach ( $rows as $row ) {
                $db->query("UPDATE sys_pages SET sorting = ".$counter." WHERE id = ".$row['id']);
                $counter += 10;
            }
            $ret = [1, ''];
        }
        echo json_encode($ret);
    }
}

$db->close();
?>