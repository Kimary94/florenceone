<?php
class SSP {
	static function data_output($columns, $data) {
		$out = array();
		for ($i = 0, $ien = count($data); $i < $ien; $i++) {
			$row = array();
			for ($j = 0, $jen = count($columns); $j < $jen; $j++) {
				$column = $columns[$j];
				if (isset($column['formatter'])) {
					if ($column['field'] == '') {
						$row[$column['dt']] = $column['formatter']($data[$i][$column['db']], $data[$i]);
					} else {
						$row[$column['dt']] = $column['formatter']($data[$i][$column['field']], $data[$i]);
					}
				} else {
					if ($columns[$j]['field'] == '') {
							$row[$column['dt']] = $data[$i][$columns[$j]['db']];
					} else {
						$row[$column['dt']] = $data[$i][$columns[$j]['field']];
					}
				}
			}
			$out[] = $row;
		}
		return $out;
	}

	static function db($conn) {
		if (is_array($conn)) { return self::sql_connect($conn); }
		return $conn;
	}

	static function limit($request, $columns) {
		$limit = '';
		if (isset($request['start']) && $request['length'] != -1) {
			$limit = "LIMIT ".intval($request['start']).", ".intval($request['length']);
		}
		return $limit;
	}

	static function order($request, $columns) {
		$order = '';
		if (isset($request['order']) && count($request['order'])) {
			$orderBy	 = array();
			$dtColumns	 = self::pluck($columns, 'dt');
			for ($i = 0, $ien = count($request['order']); $i < $ien; $i++) {
				$columnIdx		= intval($request['order'][$i]['column']);
				$requestColumn	= $request['columns'][$columnIdx];
				$columnIdx	    = array_search($requestColumn['data'], $dtColumns);
				$column		    = $columns[$columnIdx];
				if ($requestColumn['orderable'] == 'true') {
					$dir		= $request['order'][$i]['dir'] === 'asc' ? 'ASC' : 'DESC';
					if (strlen($column['field']) > 1) { $orderBy[]	= ' `'.$column['field'].'` '.$dir; }
				} else {
					if ( $requestColumn['orderable'] != 'false' ) {
						$dir = $request['order'][$i]['dir'] === 'asc' ? 'ASC' : 'DESC';
						$orderBy[] = ''.$requestColumn['orderable'].' '.$dir;
					}
				}
			}
			if (count($orderBy) > 0) {
				$order = 'ORDER BY '.implode(', ', $orderBy);
			}
		}
		return $order;
	}

	static function complex($request, $conn, $table, $primaryKey, $columns, $whereResult = null, $fixedOrder = null, $customSelect = null, $group = null) {
		$bindings		  = array();
		$db				  = self::db($conn);
		$limit			  = self::limit($request, $columns);
		$order		 	  = self::order($request, $columns);
		$whereResult 	  = self::_flatten($whereResult);
		if ($whereResult) { $where = 'WHERE '.$whereResult; } else { $where = ""; }
		if ($customSelect != '') {
			$query_select = $customSelect." ".$where." ".$group." ".$order." ".$limit;
		} else {
			$query_select = "SELECT ".implode(", ", self::pluck($columns, 'db'))." FROM $table $where $group $order $limit";
		}
		$query_select = str_replace("emptycolumn", "'' as emptycolumn", $query_select);
		
		// ORDINAMENTO FISSO INZIALE, SUPPORTA ANCHE UNA ULTERIORE COLONNA CLICCATA MANUALMENTE
		if ($fixedOrder != '') {
			if (strpos($query_select, "ORDER BY") > 0) {

				if (strpos($query_select, "ORDER BY \n") > 0) {
					$query_select = str_replace("ORDER BY \n", " ".$fixedOrder." ", $query_select);
				} else {
					$query_select = str_replace("ORDER BY ", " ".$fixedOrder." , ", $query_select);
				}
				
			} else {
				$query_select = str_replace(" LIMIT ", " ".$fixedOrder." LIMIT ", $query_select);
			}
		}

		
		$data = self::sql_exec($db, $bindings, $query_select);
		// Data set length after filtering
		$resFilterLength = self::sql_exec($db, $bindings, "SELECT COUNT({$primaryKey}) FROM $table $where ");
		$recordsFiltered = $resFilterLength[0][0];
		// Total data set length
		$resTotalLength	 = self::sql_exec($db, $bindings, "SELECT COUNT({$primaryKey}) FROM $table ");
		$recordsTotal	 = $resTotalLength[0][0];
		return array(
			"draw"				 => isset($request['draw']) ? intval($request['draw']) : 0,
			"recordsTotal"		 => intval($recordsTotal),
			"recordsFiltered"	 => intval($recordsFiltered),
			"data"				 => self::data_output($columns, $data),
		);
	}

	static function sql_connect($sql_details) {
		try {
			$db = @new PDO( "mysql:host={$sql_details['host']};dbname={$sql_details['db']}", $sql_details['user'], $sql_details['pass'], array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION)	);
		} catch (PDOException $e) { self::fatal("Error connecting to database: ".$e->getMessage()); }
		return $db;
	}

	static function sql_exec($db, $bindings, $sql = null) {
		if ($sql === null) { $sql = $bindings; }
		$stmt = $db->prepare($sql);
		if (is_array($bindings)) {
			for ($i = 0, $ien = count($bindings); $i < $ien; $i++) {
				$binding = $bindings[$i];
				$stmt->bindValue($binding['key'], $binding['val'], $binding['type']);
			}
		}
		try {
			$stmt->execute();
		} catch (PDOException $e) { self::fatal("An SQL error occurred: ".$e->getMessage()); }
		return $stmt->fetchAll(PDO::FETCH_BOTH);
	}

	static function fatal($msg) {
		echo json_encode(array("error" => $msg));
		exit(0);
	}

	static function bind(&$a, $val, $type) {
		$key = ':binding_'.count($a);
		$a[] = array(
			'key'	 => $key,
			'val'	 => $val,
			'type'	 => $type
		);
		return $key;
	}

	static function pluck($a, $prop) {
		$out = array();
		for ($i = 0, $len = count($a); $i < $len; $i++) {
			$out[] = $a[$i][$prop];
		}
		return $out;
	}

	static function _flatten($a, $join = ' AND ') {
		if (!$a) {
			return '';
		} else {
			if ($a && is_array($a))	{ return implode($join, $a); }
		}
		return $a;
	}
}