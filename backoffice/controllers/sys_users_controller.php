<?php

if (!isset($_SESSION)) { session_start(); }
include "sys_database.php";
include "../sys_functions.php";
include "sys_ssp.class.php";

// action: read_record        LOAD SINGLE RECORD FROM sys_users FOR MODAL EDIT
// action: load_table         LOAD TABLE sys_users
// action: button_record      RETURN BUTTON (to put inside datatable) FOR SINGLE RECORD ON sys_users
// action: delete_record      DELETE RECORD ON sys_users
// action: save_record        SAVE RECORD ON sys_users


// CONTROLLER RESTRICTED TO sysadmin
if (!isset($_SESSION['user_username'])) { die(); }
if (strtolower($_SESSION['user_username']) != 'sysadmin') { die(); }


if(isset($_POST['action'])) {

    
    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'read_record') {
        $id = filter_input(INPUT_POST, 'id', FILTER_VALIDATE_INT);
        if (intval($id) != 1) {
            $risultato = array();
            $qry = "SELECT * FROM sys_users WHERE sys_users.id = ".$id;
            $query = $db->query($qry);
            while ($risultati = mysqli_fetch_array($query))
            {
                $risultato["record_id"] = $risultati["id"];
                $risultato["record_firstname"] = $risultati["firstname"];
                $risultato["record_lastname"] = $risultati["lastname"];
                $risultato["record_username"] = $risultati["username"];
                $risultato["record_email"] = $risultati["email"];
                $risultato["record_password"] = $risultati["password"];
                $risultato["record_id_sys_groups"] = $risultati["id_sys_groups"];
                $risultato["record_login_disabled"] = $risultati["login_disabled"];

            }
            echo json_encode($risultato);
        } else {
            echo json_encode("NOT AUTHORIZED");
        }
    }


    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'load_table') {
        $table = 'sys_users LEFT JOIN sys_groups ON sys_groups.id = sys_users.id_sys_groups';
        $primaryKey = 'sys_users.id';
        $columns = array(
        array( 'db' => 'sys_users.id', 'dt' => 0, 'field' => 'id' ), 
        array( 'db' => 'sys_users.firstname', 'dt' => 1, 'field' => 'firstname' ), 
        array( 'db' => 'sys_users.lastname', 'dt' => 2, 'field' => 'lastname' ), 
        array( 'db' => 'sys_users.username', 'dt' => 3, 'field' => 'username' ), 
        array( 'db' => 'sys_users.email', 'dt' => 4, 'field' => 'email' ),
        array( 'db' => 'sys_groups.group_name as desc_sys_groups', 'dt' => 5, 'field' => 'desc_sys_groups' ), 
        array( 'db' => 'IF(sys_users.login_disabled = 1, "<i class=\'icon-check\'></i>", "<i class=\'icon-check-empty\'></i>") as login_disabled', 'dt' => 6, 'field' => 'login_disabled' ), 
        array( 'db' => 'emptycolumn', 'dt' => 7, 'field' => '' ), 
        );

        $sql_details = array(
            'user' => $db_username,
            'pass' => $db_password,
            'db'   => $db_database,
            'host' => $db_host,
        );
        $filterfirstname = filter_input(INPUT_POST, 'filterfirstname', FILTER_SANITIZE_STRING);
        $filterlastname = filter_input(INPUT_POST, 'filterlastname', FILTER_SANITIZE_STRING);
        $filterusername = filter_input(INPUT_POST, 'filterusername', FILTER_SANITIZE_STRING);
        $filteremail = filter_input(INPUT_POST, 'filteremail', FILTER_SANITIZE_STRING);
        $filterid_sys_groups = filter_input(INPUT_POST, 'filterid_sys_groups', FILTER_SANITIZE_STRING);
        $filterlogin_disabled = filter_input(INPUT_POST, 'filterlogin_disabled', FILTER_SANITIZE_STRING);

        $where = " sys_users.id > 1 ";
        if ($filterfirstname  != '') { $where .= " and sys_users.firstname like '%" . $filterfirstname . "%' "; $_SESSION['sys_users_filter_firstname'] = $filterfirstname; } else { unset($_SESSION['sys_users_filter_firstname']); } 
        if ($filterlastname  != '') { $where .= " and sys_users.lastname like '%" . $filterlastname . "%' "; $_SESSION['sys_users_filter_lastname'] = $filterlastname; } else { unset($_SESSION['sys_users_filter_lastname']); } 
        if ($filterusername  != '') { $where .= " and sys_users.username like '%" . $filterusername . "%' "; $_SESSION['sys_users_filter_username'] = $filterusername; } else { unset($_SESSION['sys_users_filter_username']); } 
        if ($filteremail  != '') { $where .= " and sys_users.email like '%" . $filteremail . "%' "; $_SESSION['sys_users_filter_email'] = $filteremail; } else { unset($_SESSION['sys_users_filter_email']); } 
        if ($filterid_sys_groups  != '') { $where .= " and sys_users.id_sys_groups = " . $filterid_sys_groups . " "; $_SESSION['sys_users_filter_id_sys_groups'] = $filterid_sys_groups; } else { unset($_SESSION['sys_users_filter_id_sys_groups']); } 
        if ($filterlogin_disabled == '1') { $where .= " and sys_users.login_disabled = 1; "; $_SESSION['sys_users_filter_login_disabled'] = $filterlogin_disabled; } else { unset($_SESSION['sys_users_filter_login_disabled']); } 

        $fixedOrder = "";
        echo json_encode(
            SSP::complex( $_POST, $sql_details, $table, $primaryKey, $columns, $where, $fixedOrder )
        );
    }


    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'button_record') {
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $html_button  = '<div class="list-icons">
                                    <div class="dropdown">
                                        <a href="#" class="list-icons-item" data-toggle="dropdown" aria-expanded="false">
                                            <i class="icon-th-list-1"></i>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(22px, 19px, 0px);">
                                            <a href="#" onclick="edit_record('.$id.');" class="dropdown-item"><i class="icon-pencil"></i> Modifica</a>';
        $external_usage = 0;
        // QUERY TO CHECK IF RECORD CAN BE DELETED
        if ($external_usage == 0) {
            $html_button .= '<a href="#" class="dropdown-item" onclick="delete_record('.filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING).');"><i class="icon-trash"></i> Elimina</a>';
        } else {
            $html_button .= '<a href="#" class="dropdown-item" style="color:#dddddd;"><i class="icon-trash"></i> Elimina</a>';
        }
        $html_button .= '</div></div></div>';
        echo $html_button;
    }


    
    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'delete_record') {
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        if (intval($id) != 1) {
            $db->query("DELETE FROM sys_users WHERE id = ".$id);
            echo "OK";
        } else {
            echo "NOT AUTHORIZED";
        }
    }

    if (filter_input(INPUT_POST, 'action', FILTER_SANITIZE_STRING) == 'save_record') {
        $id = filter_input(INPUT_POST, 'id', FILTER_SANITIZE_STRING);
        $input_firstname = filter_input(INPUT_POST, 'firstname', FILTER_SANITIZE_STRING);
        $input_lastname = filter_input(INPUT_POST, 'lastname', FILTER_SANITIZE_STRING);
        $input_username = trim(filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING));
        $input_email = trim(filter_input(INPUT_POST, 'email', FILTER_SANITIZE_STRING));
        $input_password = filter_input(INPUT_POST, 'password', FILTER_SANITIZE_STRING);
        $input_id_sys_groups = filter_input(INPUT_POST, 'id_sys_groups', FILTER_SANITIZE_STRING);
        if ($input_id_sys_groups == 0) { $input_id_sys_groups = null; }
        if ($input_id_sys_groups == '') { $input_id_sys_groups = null; }
        $input_login_disabled = filter_input(INPUT_POST, 'login_disabled', FILTER_SANITIZE_STRING);
        $data = [
            's', $input_firstname,
            's', $input_lastname,
            's', $input_username,
            's', $input_email,
            's', $input_password,
            'i', $input_id_sys_groups,
            'i', $input_login_disabled
        ];
        if ((intval($id) != 1) && (strtolower($input_username) != "sysadmin")) {
            
            if ($id == '') {

                $query = $db->query("SELECT * FROM sys_users WHERE username like '".$input_username."'");
                while ($risultati = mysqli_fetch_array($query)) { echo "USERNAME ALREADY EXIST"; die(); }

                $sql = 'INSERT INTO sys_users (firstname, lastname, username, email, password, id_sys_groups, login_disabled) VALUES (?, ?, ?, ?, ?, ?, ?)';
                $retv = $db->prepare_and_execute($sql, $data);
            } else {

                $query = $db->query("SELECT * FROM sys_users WHERE id <> '".$id."' AND username like '".$input_username."'");
                while ($risultati = mysqli_fetch_array($query)) { echo "USERNAME ALREADY EXIST"; die(); }

                array_push($data, 'i', $id);
                $sql = 'UPDATE sys_users SET firstname=?, lastname=?, username=?, email=?, password=?, id_sys_groups=?, login_disabled=? WHERE id=?';
                $retv = $db->prepare_and_execute($sql, $data);
            }
            if ($retv == 1) {
                echo "OK";
            } else {
                echo "UNKNOWN ERROR";
            }
        } else {
            echo "NOT AUTHORIZED";
        }
    }
}
?>
