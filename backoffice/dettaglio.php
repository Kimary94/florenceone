<?php include "sys_header.php"; ?>
<body class="navbar-top<?php if ($settings_menu_closed == 1) { echo ' sidebar-xs'; } ?>">
	<?php include "sys_navbar.php"; ?>
	<!-- Page content -->
	<div class="page-content">
		<?php include "sys_sidebar.php"; ?>
		<!-- Main content -->
		<div class="content-wrapper">
			<!-- Page header -->
			<div class="page-header page-header-light">
				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					<div class="d-flex">
                        <i class="icon-user mr-2 breadcrumb-item"></i><span style="margin-top: 8px;">Dettaglio ordine</span>
					</div>
				</div>
			</div>
			<!-- Content area -->
			<div class="content unselectable">
				<div class="card card-collapsed" style="background-color: #f9f9f9;">
					<div class="card-body">
						<div class="row">
						
								
<?php
    $id_ordine = 0;
    $id_ordine_tmp = filter_input(INPUT_GET, 'id', FILTER_SANITIZE_STRING);
    if (intval($id_ordine_tmp) <= 0) {
        // HACK, ID PASSATO NELL'URL NON E' UN NUMERO
        echo "<script>window.location.replace('index.php');</script>";
        die();
    } else {
        $qry = "SELECT * FROM ordine WHERE id = ".$id_ordine_tmp;
        $rows = fetch_rows($qry);
        foreach ( $rows as $row ) {
            $id_ordine = $row['id'];
            $id_cliente = $row['id_cliente'];
            $nr_ordine = $row['id'];
            $giorno_ordine = $row['giorno'];
            $ora_ordine = $row['ora'];
        }
        $row = $rows[0];

        $qry = "SELECT descrizione FROM modalita_pagamento WHERE id = ".$row['id_modalita_pagamento'];
        $rowsmp = fetch_rows($qry);
        foreach ( $rowsmp as $rowmp ) {
            $modalita_pagamento = $rowmp['descrizione'];
        }

        
        $qry = "SELECT descrizione FROM status WHERE id = ".$row['id_status_cliente'];
        $rowsmp = fetch_rows($qry);
        foreach ( $rowsmp as $rowmp ) {
            $status_cliente = $rowmp['descrizione'];
        }
        
        $qry = "SELECT descrizione FROM status WHERE id = ".$row['id_status_sede'];
        $rowsmp = fetch_rows($qry);
        foreach ( $rowsmp as $rowmp ) {
            $status_sede = $rowmp['descrizione'];
        }

    }
?>



    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="text-align:center;">
    <h2>ORDINE &nbsp; nr. <?=$nr_ordine?> &nbsp; del &nbsp; <?=$giorno_ordine?></h2><br><br>
    </div>

</div>

<style>
.labeldettaglio {
    font-size: 16px;
    font-weight: 400;
}
.titolodettaglio {
    font-variant: all-small-caps;
    width:120px;
}
</style>

<div class="row">

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
Stato ordine: <b><?=$status_cliente?></b>
<br>
<?php if ($status_sede != 'Annullato') { ?>
<button style="margin-right: 6px;" onclick="setstatus('id_status_cliente', 1, <?=$id_ordine?>);">Confermato</button>
<button style="margin-right: 6px;" onclick="setstatus('id_status_cliente', 2, <?=$id_ordine?>);">Pagato</button>
<?php } ?>
<br>
<br>
Stato lavorazione: <b><?=$status_sede?></b>
<br>
<button style="margin-right: 6px;" onclick="setstatus('id_status_sede', 3, <?=$id_ordine?>);">In attesa del pagamento
<button style="margin-right: 6px;" onclick="setstatus('id_status_sede', 4, <?=$id_ordine?>);">In elaborazione
<button style="margin-right: 6px;" onclick="setstatus('id_status_sede', 5, <?=$id_ordine?>);">Spedito</button>
<button style="margin-right: 6px;" onclick="setstatus('id_status_sede', 6, <?=$id_ordine?>);">Annullato</button>
<button style="margin-right: 6px;" onclick="setstatus('id_status_sede', 7, <?=$id_ordine?>);">Annullato e rimborsato</button>
<br>
<br>
<br>
</div>

    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <h4 style="font-variant: all-small-caps;">Dati fiscali</h4><br>
    <?php

    echo "<label class='titolodettaglio'>Nome</label><label class='labeldettaglio'>".$row['nome']."</label><br>";
    echo "<label class='titolodettaglio'>Cognome</label><label class='labeldettaglio'>".$row['cognome']."</label><br>";
    echo "<label class='titolodettaglio'>Ragione sociale</label><label class='labeldettaglio'>".$row['ragionesociale']."</label><br>";
    echo "<label class='titolodettaglio'>P.iva</label><label class='labeldettaglio'>".$row['partitaiva']."</label><br>";
    echo "<label class='titolodettaglio'>Cod. fisc.</label><label class='labeldettaglio'>".$row['codicefiscale']."</label><br>";
    echo "<label class='titolodettaglio'>Email</label><label class='labeldettaglio'>".$row['email']."</label><br>";
    echo "<label class='titolodettaglio'>Telefono</label><label class='labeldettaglio'>".$row['telefono']."</label><br>";
    echo "<label class='titolodettaglio'>PEC</label><label class='labeldettaglio'>".$row['pec']."</label><br>";
    echo "<label class='titolodettaglio'>Codice univoco</label><label class='labeldettaglio'>".$row['codiceunivoco']."</label><br>";
    echo "<label class='titolodettaglio'>Indirizzo</label><label class='labeldettaglio'>".$row['fattura_indirizzo']."</label><br>";
    echo "<label class='titolodettaglio'>CAP</label><label class='labeldettaglio'>".$row['fattura_cap']."</label><br>";
    echo "<label class='titolodettaglio'>Città</label><label class='labeldettaglio'>".$row['fattura_citta']."</label><br>";
    echo "<label class='titolodettaglio'>Provincia</label><label class='labeldettaglio'>".$row['fattura_provincia']."</label><br>";
    ?>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <h4 style="font-variant: all-small-caps;">Dati di spedizione</h4><br>
    <?php

        if ($row['spedizione_tipo'] == 1) {
            echo "I dati di spedizione coincidono con quelli di fatturazione";
        } else {
            echo "<label class='titolodettaglio'>Nominativo</label><label class='labeldettaglio'>".$row['spedizione_nominativo']."</label><br>";
            echo "<label class='titolodettaglio'>Indirizzo</label><label class='labeldettaglio'>".$row['spedizione_indirizzo']."</label><br>";
            echo "<label class='titolodettaglio'>CAP</label><label class='labeldettaglio'>".$row['spedizione_cap']."</label><br>";
            echo "<label class='titolodettaglio'>Città</label><label class='labeldettaglio'>".$row['spedizione_citta']."</label><br>";
            echo "<label class='titolodettaglio'>Provincia</label><label class='labeldettaglio'>".$row['spedizione_provincia']."</label><br>";
            echo "<label class='titolodettaglio'>Riferimento</label><label class='labeldettaglio'>".$row['spedizione_riferimento']."</label><br>";
        }

    ?>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    <h4 style="font-variant: all-small-caps;">Pagamento / Transazione</h4><br>
    <?php
        echo "<label class='titolodettaglio'>Tot. Netto</label><label class='labeldettaglio'>".$row['totale_netto']."</label><br>";
        echo "<label class='titolodettaglio'>Tot. IVA</label><label class='labeldettaglio'>".$row['totale_iva']."</label><br>";
        echo "<label class='titolodettaglio'><b>Totale</label><label class='labeldettaglio'>".$row['totale_ordine']."</b><br><br>";    
        echo "<label class='titolodettaglio'><b>Commissioni</label><label class='labeldettaglio'>".$row['commissioni']."</b><br><br>";    
        echo "<label class='titolodettaglio'><b>Tot. Ordine</label><label class='labeldettaglio'>".$row['totale_finale']."</b><br><br>";    
        
        if ($row['sconto_euro'] > 0) {
            echo "<b style='font-variant: all-small-caps;'>Totale scontato per utilizzo coupon</b><br>";    
            echo "<b style='font-variant: all-small-caps; font-size:16px;'>".$row['sconto_dicitura']."</b><br><br>";    
        }

        echo "<label class='titolodettaglio'>Mod. pagamento</label><label class='labeldettaglio'>".$modalita_pagamento."</label><br><br>";

        if ($row['id_modalita_pagamento'] != 2) {
            echo "<label class='titolodettaglio'>RC</label><label class='labeldettaglio'>".$row['transazione_rc']."</label><br>";
            echo "<label class='titolodettaglio'>tranID</label><label class='labeldettaglio'>".$row['transazione_tranID']."</label><br>";
            echo "<label class='titolodettaglio'>enrStatus</label><label class='labeldettaglio'>".$row['transazione_enrStatus']."</label><br>";
            echo "<label class='titolodettaglio'>authStatus</label><label class='labeldettaglio'>".$row['transazione_authStatus']."</label><br>";
            echo "<label class='titolodettaglio'>payInstrToken</label><label class='labeldettaglio'>".$row['transazione_payInstrToken']."</label><br>";
            echo "<label class='titolodettaglio'>expireMonth</label><label class='labeldettaglio'>".$row['transazione_expireMonth']."</label><br>";
            echo "<label class='titolodettaglio'>expireYear</label><label class='labeldettaglio'>".$row['transazione_expireYear']."</label><br>";
        }
    ?>
    </div>

</div>

<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
<br><br>
    <table class="table cart-table table-responsive-xs">
    <thead>
    <tr class="table-head">
        <th scope="col">&nbsp;</th>
        <th scope="col">Articolo</th>
        <th scope="col">Quantità</th>
        <th scope="col">Dettagli</th>
        <th scope="col">Totale<br>Iva Compresa</th>
    </tr>
    </thead>
    <tbody>

    <?php
    $qry = "SELECT * FROM carrello WHERE id_ordine = ".$id_ordine;
    if ($id_cliente > 0) {
        $qry .= " AND id_cliente = ".$id_cliente;
    } else {
        $qry .= " AND sessionid = '".$id_session."'";
    }
    $totale_carrello = 0;
    $rows = fetch_rows($qry);
    foreach ( $rows as $row ) {
        $totale_carrello += $row['importo_totale'];
    ?>

    <tr>
        <td><img style="width:100px;" src="<?=$row['articolo_immagine1']?>" alt="cart"  class=" "></td>
        <td><h2><?php echo $row['articolo_codice']."<br>".$row['articolo_nome']; ?></h2></td>
        <td><h2><?=$row['quantita']?></h2></td>
        <td><div class="qty-box" style="text-align:left; font-size: 18px;">
            <?php
                $descrizione = "";
                $rows2 = fetch_rows("SELECT * FROM dettaglio WHERE id_carrello = ".$row['id']." ORDER BY ordine ");
                foreach ( $rows2 as $row2 ) {
                    if ($descrizione != "") { $descrizione .= "<br>"; }
                    if ($row2['rgb'] != "") { $descrizione .= "<span style='border: 1px solid black; width:30px; height:22px; background-color: #".$row2['rgb']."; display:inline-block; vertical-align: sub;'></span> "; }
                    if ($row2['texture'] != "") { $descrizione .= "<img style='border: 1px solid black; width:30px; height:22px; vertical-align: sub;' src='".$row2['texture']."'> "; }
                    if ($row2['immagine'] != "") { $descrizione .= "<img style='border: 1px solid black; width:30px; height:22px; vertical-align: sub;' src='".$row2['immagine']."'> "; }

                    $descrizione .= $row2['descrizione1'];
                    $descrizione = str_replace("nella taglia Colore", "", $descrizione);


                    if (strlen($row2['uploaded_file']) > 6) {
                        $descrizione .= ' <a href="../'.$row2['uploaded_file'].'" target="_blank">DOWNLOAD</a>';
                    }

                }

                
                if (strpos($descrizione, "Lavorazione:") === false) {
                    $descrizione .= "<br>Lavorazione: ".$row['servizio_nome'];
                }
                
                echo $descrizione;
            ?>
            </div>
        </td>
        <td><h2><?=$row['importo_totale']?> €</h2></td>
    </tr>
    <?php } ?>

    <tr>
        <td colspan=3>&nbsp;</td>
        <td><div class="qty-box">TOTALE CARRELLO<br>IVA COMPRESA</div></td>
        <td><h2><?=$totale_carrello?> €</h2></td>
    </tr>

    </tbody>
</table>


<script>
var tmp_campo = "";
var tmp_stato = 0;
var tmp_idordine = 0;
function setstatus(campo, stato, idordine) {
    if (stato == 5) {
        tmp_campo = campo;
        tmp_stato = stato;
        tmp_idordine = idordine;
        input_box("Tracking", "Inserisci il codice di tracking spedizione", "OK", "Annulla", setstatus5);
    } else {
        $.ajax({
            async: false,
            type: "POST",
            url: "controllers/set_status.php",
            data: {
                "campo": campo,
                "stato": stato,
                "idordine": idordine
            },
            success: function (data, stato) {
                window.location.reload();
            }
        });
    }
}

function setstatus5(tracking) {
    if (tracking) {
        $.ajax({
            async: false,
            type: "POST",
            url: "controllers/set_status.php",
            data: {
                "campo": tmp_campo,
                "stato": tmp_stato,
                "idordine": tmp_idordine,
                "tracking": tracking
            },
            success: function (data, stato) {
                window.location.reload();
            }
        });
    }
}

</script>
                            

							</div>
						</div>
					</div>
				</div>
			</div>	<!-- /Content area -->
		</div>	<!-- /Main content -->
	</div>	<!-- /Page content -->
</body>
</html>
