<?php
    include "sys_functions.php";

    if (!isset($_SESSION)) { session_start(); }

    // CHECK IF DATABASE CONFIGURATION FILE EXIST
    $wizard_config = false;
    $file_config = 'sys_config.php';
    if (file_exists($file_config)) {
        include $file_config;
        if ( !isset($db_host) || !isset($db_username) || !isset($db_password) || !isset($db_database) ) {
            $wizard_config = true;
        }
    } else {
        $wizard_config = true;
    }

    // START WIZARD IF MISSING CONFIG FILE OR MISSING VALUES
    if ($wizard_config) {
        header("location: ".linkto("sys_wizard"));
        die();
    }

    $logged_in = false;
    if (isset($_SESSION['user_id']))
        if ($_SESSION['user_id'] != "")
            $logged_in = true;
    
    if ($logged_in) {
        if (strtolower($_SESSION['user_username']) == 'sysadmin') {
            header("location: ".linkto("news"));
        } else {
            header("location: ".linkto("sys_dashboard"));
        }
    } else {
        header("location: ".linkto("sys_login"));
    }
?>