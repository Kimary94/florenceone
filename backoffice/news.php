<?php
    include "sys_header.php";
    if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(12, "read")) {header("location: ".linkto("sys_dashboard")); die();}}
?>

<body class="navbar-top<?php if ($settings_menu_closed == 1) { echo ' sidebar-xs'; } ?>">
    <div id="imagePreviewDiv" class="modal modal-xl fade imgpreview" style="z-index: 9999;"></div>
    <?php include "sys_navbar.php"; ?>
    <div class="page-content">
        <?php
        include "sys_sidebar.php";
        if ($_SESSION["user_username"] != "sysadmin") {
            if (!checkUserRight(12, "create")) {echo "<script>can_create = false;</script>";} else {echo "<script>can_create = true;</script>";}
            if (!checkUserRight(12, "edit")) {echo "<script>can_edit = false;</script>";} else {echo "<script>can_edit = true;</script>";}
        } else {
            echo "<script>can_create = true; can_edit = true;</script>";
        }
        ?>
        <div class="content-wrapper">

        
        <!-- Page header -->
        <div class="page-header page-header-light">
            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">

                <div class="d-flex">
                    <i class="icon-newspaper mr-2 breadcrumb-item"></i><span style="margin-top: 8px;">news</span>
                </div>

                <div class="header-elements d-none" style="display: flex !important;">
                    <div class="breadcrumb justify-content-center">
                        <?php
                        $show_button_new = true;
                        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(12, "create")) { $show_button_new = false; }}
                        if ($show_button_new) { ?>
                            <a href="#" class="breadcrumb-elements-item" onclick="new_record();">
                                <i class="icon-plus-squared"></i>
                                New record
                            </a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>


<div class="content unselectable">
    <div class="card card-collapsed" style="background-color: #f9f9f9;">
        <div class="card-header header-elements-inline" onmouseup="$('#searchexpand').click();" style="font-variant: all-petite-caps;">
            <div><i class="icon-search-7"></i> Filtri di ricerca</div>
            <div data-action="collapse" id="searchexpand" style="display:none;"></div>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-lg-10 col-md-9 col-sm-12 col-xs-12">
                    <div class="row">

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Titolo</label>
                        <input type="text"   class="form-control" autocomplete="off" id="titolo" value="<?php if (isset($_SESSION['news_filter_titolo'])) { echo $_SESSION['news_filter_titolo']; } ?>">
                    </div></div>

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Testo</label>
                        <input type="text"   class="form-control" autocomplete="off" id="testo" value="<?php if (isset($_SESSION['news_filter_testo'])) { echo $_SESSION['news_filter_testo']; } ?>">
                    </div></div>

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Dalla data</label>
                        <input type="date"   class="form-control" autocomplete="off" id="data_from" style="text-align:center;" id="data_from"  value="<?php if (isset($_SESSION['news_filter_data_from'])) { echo $_SESSION['news_filter_data_from']; } ?>">
                    </div></div>

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Alla data</label>
                        <input type="date"   class="form-control" autocomplete="off" id="data_to" style="text-align:center;" id="data_to"  value="<?php if (isset($_SESSION['news_filter_data_to'])) { echo $_SESSION['news_filter_data_to']; } ?>">
                    </div></div>

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold" style="display: inherit;">Attivo</label><br>
                        <input type="checkbox"  class="form-control" id="attivo" value="1" <?php if (isset($_SESSION['news_filter_attivo'])) { if ($_SESSION['news_filter_attivo'] == 1) { echo " CHECKED "; } } ?> >
                    </div></div>


                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="text-center">
                                <button onclick="event.preventDefault(); applyFilters();"
                                        style="width: 150px; padding: 10px; border-radius: 5px;"
                                        class="btn btn-sm btn-outline-info">
                                            <i class="icon-search-1" style="float: left; margin-top: 2px;"></i> Search
                                </button><br>
                                <button onclick="event.preventDefault(); resetFilters();"
                                        style="width: 150px; padding: 10px; border-radius: 5px; margin: 3px 0px 3px 0px;"
                                        class="btn btn-sm btn-outline-warning">
                                            <i class="icon-cancel-alt" style="float: left; margin-top: 2px;"></i> Reset
                                </button><br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="card">
    <table id="myDatatable" class="display table table-striped datatable-responsive" style="width:100%; overflow: hidden;">
        <thead>
            <tr>
                <th><b>Id</b></th>
                <th><b>Titolo</b></th>
                
                <th><b>Data</b></th>
                <th><b>Attivo</b></th>

                <th><b>Actions</b></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th><b>Id</b></th>
                <th><b>Titolo</b></th>
                
                <th><b>Data</b></th>
                <th><b>Attivo</b></th>

                <th><b>Actions</b></th>
            </tr>
        </tfoot>
    </table>
</div>

<div id="modal_edit" class="modal fade" style="-webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none;" unselectable="on">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<h6 class="modal-title"><i class="icon-newspaper"></i> <span id="modal_mode"></span></h6>
			</div>
            <div class="modal-body">
                <div class="content">
                        
                    <input type="hidden" id="record_id">
            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">Titolo</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <input type="text" id="record_titolo" maxlength="255" autocomplete="off" class="form-control" >
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">Testo</label>
                </div>            </div>            <div class="row form-group">
                <div class="col-lg-12" style="margin-top: 3px;">                    <div class="editor" style="height:400px;"></div>
                    <!-- <input type="text" id="record_testo" maxlength="65535" autocomplete="off" class="form-control" > -->
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">Img</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <div class="row">
                        <div class="col-lg-12 dropzonerow">
                            <form class="dropzone" id="dropzone_img"></form>
                        </div>
                        <div class="col-lg-12" id="fileContainer_img"></div>    
                    </div>
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">Data</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <input type="date" style="text-align:center; width:170px;" id="record_data" autocomplete="off" class="form-control" required >
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 0px 0px 0px 0px;">Attivo</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <input type="checkbox" id="record_attivo" class="form-control" value="1"  style="width: 28px; margin: 0;" >
                </div>
            </div>

                </div>
            </div>
			<div class="modal-footer">
                <button onclick="save_record();" class="btn btn-lg btn-outline-success savebutton"><i class="icon-ok-3"></i> Save</button>
                <button onclick="$('#modal_edit').modal('hide');" class="btn btn-lg btn-outline-warning"><i class="icon-undo"></i> Cancel</button>
			</div>
		</div>
	</div>
</div>


</div>
</div>
</div>


<script>
    var FILELIST_img = new Array;
    
    function new_record() {
        $('#modal_mode').html('New News');
        $("#record_id").val("");
        $("#record_titolo").val("");
        //$("#record_testo").val("");        window.editor.setData("");

        document.querySelector("#dropzone_img").dropzone.removeAllFiles();
        FILELIST_img = new Array;
        $("#fileContainer_img").html("");

        $("#record_data").val("");
        if (ischecked("#record_attivo")) {
            $("#record_attivo").click();
        }
        setTimeout(function(){ $("#record_titolo").focus(); }, 300);
        if (can_create) { $(".savebutton").show(); $(".dropzone").show(); $(".icon-trash-empty").show(); } else { $(".savebutton").hide();  $(".dropzone").hide(); $(".icon-trash-empty").hide(); }
        $('#modal_edit').modal({backdrop: 'static'});
    }

    function edit_record(id) {
        if (can_edit) { $('#modal_mode').html('Edit News'); } else { $('#modal_mode').html('View News'); }
        $.ajax({
            async: false,
            type: "POST",
            url: "controllers/news_controller.php",
            data: {
                "action": "read_record",
                "id": id
            },
            success: function (data, stato) {
                var data = $.parseJSON(data);
                if (data == "NOT AUTHORIZED") { return; }

                $("#record_id").val(data["record_id"]);
                $("#record_titolo").val(data["record_titolo"]);
                //$("#record_testo").val(data["record_testo"]);
                window.editor.setData(data["record_testo"]);
                $("#fileContainer_img").html("");
                document.querySelector("#dropzone_img").dropzone.removeAllFiles();
                FILELIST_img = new Array;
                if (data.record_img !== null) {
                    if (data.record_img.length > 0) {
                        FILELIST_img = JSON.parse(data.record_img);
                        if (FILELIST_img === null) {
                            FILELIST_img = new Array;
                        } else {
                            drawFileList(FILELIST_img, "FILELIST_img", "fileContainer_img");
                        }
                    }
                }
                $("#record_data").val(data["record_data"]);

            if (data["record_attivo"] == 1) {
                if (!ischecked("#record_attivo")) {
                    $("#record_attivo").click();
                }
            } else {
                if (ischecked("#record_attivo")) {
                    $("#record_attivo").click();
                }
            }
                setTimeout(function(){ $("#record_titolo").focus(); }, 300);
                if (can_edit) { $(".savebutton").show(); $(".dropzone").show(); $(".icon-trash-empty").show(); } else { $(".savebutton").hide(); $(".dropzone").hide(); $(".icon-trash-empty").hide(); }
                $('#modal_edit').modal({backdrop: 'static'});
            }
        });
    }
    
    function save_record() {
        var requiredfields = "";
        $(".modal-body :required").each(function(){
            if (($(this).val() == "") || ($(this).val() === null)) {
                if (requiredfields != "") { requiredfields = requiredfields.concat(", "); }
                var tmp_req_field = $(this).attr("id").replace("record_", "").replace("_", " ");
                if (tmp_req_field.substr(0, 3) == "id ") { tmp_req_field = tmp_req_field.replace("id ", ""); }
                requiredfields = requiredfields.concat(tmp_req_field);
            }
        })
        if (requiredfields != "") { toast("Required fields: ".concat(requiredfields), "error"); return; }

        $.ajax({
            async: false,
            type: "POST",
            url: "controllers/news_controller.php",
            data: {
                "action": "save_record",
                "id": $("#record_id").val(), 
                "titolo": $("#record_titolo").val(), 
                "testo": window.editor.getData(),
                "img": JSON.stringify(FILELIST_img),
                "data": $("#record_data").val(), 
                "attivo": ischecked("#record_attivo"), 

            },
            success: function (data, stato) {
                if (data != "OK") {
                    swal({
                        title: "Error",
                        text: "An error occurred during Save: " + data.toString(),
                        confirmButtonColor: "#66BB6A",
                        type: "warning"
                    });
                } else {
                    $('#modal_edit').modal('hide');
                    update_table();
                }
            }
        });
    }

    function update_table()  {
        table = $('#myDatatable').DataTable();
        table.destroy();
        $("#myDatatable > tbody").html("");
        load_table();
        table = $('#myDatatable').DataTable();
        table.page(0).draw(false);
    }

    $(document).ready(function () {

        $('.select').select2({ width: '100%' });

        new Dropzone("#dropzone_img", {
            init: function() {
                this.on("success", function(file, serverFileName) {
                    FILELIST_img.push({"serverFileName" : serverFileName, "fileName" : file.name});
                    drawFileList(FILELIST_img, "FILELIST_img", "fileContainer_img");
                });
                this.on("error", function(file, errorMessage) {
                    message_box("Error", errorMessage, "Ok", "error");
                });
            },
            url: "controllers/sys_file_uploader.php" });

        load_table();
        table = $('#myDatatable').DataTable();
        table.on('draw', function () {
            table.rows().every(function (rowIdx, tableLoop, rowLoop) {
                var values_in_row = this.data();
                $.ajax({
                    async: false,
                    type: "POST",
                    url: "controllers/news_controller.php",
                    data: {
                        "action": "button_record",
                        "id": values_in_row[0]
                    },
                    success: function (data, stato) {
                        values_in_row[4] = data;
                    }
                });
                this.data(values_in_row);
            });
        });
    });


    function delete_record(id) {
        swal({
            title: "Sei sicuro?",
            text: "I dati non potranno essere recuperati",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: "Si, elimina!",
            cancelButtonText: "No, annulla!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    async: true,
                    type: "POST",
                    url : "controllers/news_controller.php",
                    data: {
                        "action": "delete_record",
                        "id": id
                    },
                    success: function (data) {
                        if (data === "OK") {
                            swal({
                                title: "Eliminato!",
                                text: "Il record è stato cancellato",
                                confirmButtonColor: "#66BB6A",
                                type: "success"
                            }, function () {
                                document.location.reload();
                            });
                        } else {
                            swal({
                                title: "Operazione annullata",
                                text: "Qualcosa è andato storto",
                                confirmButtonColor: "#2196F3",
                                type: "error"
                            });
                        }
                    },
                    error: function (data) {
                        swal({
                            title: "Operazione annullata",
                            text: "Qualcosa è andato storto",
                            confirmButtonColor: "#2196F3",
                            type: "error"
                        });
                    }
                });
            }
            else {
                swal({
                    title: "Operazione annullata",
                    text: "",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
    }


    function load_table() {

        filtertitolo = "";
        if (parseInt($("#titolo").val().length) > 0) { filtertitolo = $("#titolo").val(); }

        filtertesto = "";
        if (parseInt($("#testo").val().length) > 0) { filtertesto = $("#testo").val(); }

        filterdata_from = "";
        if (parseInt($("#data_from").val().length) > 0) { filterdata_from = $("#data_from").val(); }
        filterdata_to = "";
        if (parseInt($("#data_to").val().length) > 0) { filterdata_to = $("#data_to").val(); }

        filterattivo = "";
        if (ischecked("#attivo")) { filterattivo = "1"; }

        $('#myDatatable').DataTable( {
            'iDisplayLength': 25,
            'processing': true,
            'serverSide': true,
            'stateSave': true,
            'searching': false,
            'responsive': true,
            dom: '<"datatable-header"flB><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            buttons: [{
                        extend: 'pdf',
                        orientation: 'landscape',
                        text: '<i class="icon-file-pdf"></i> PDF',
                        title: '',
                        className: 'btn btn-outline-danger',
                        exportOptions: {columns: [1, 2, 3]}
                    },
                    {
                        extend: 'excel',
                        text: '<i class="icon-file-excel"></i> Excel',
                        title: '',
                        className: 'btn btn-outline-success',
                        exportOptions: {columns: [1, 2, 3]}
                    }
            ],
            language: {
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: {'first': '|&larr;', 'last': '&rarr;|', 'next': '&rarr;', 'previous': '&larr;'}
            },
            ajax: {
                url: 'controllers/news_controller.php',
                type: 'POST',
                data: {
                    action: "load_table",
                    filtertitolo: filtertitolo,
                    filtertesto: filtertesto,
                    filterdata_from: filterdata_from,
                    filterdata_to:   filterdata_to,
                    filterattivo: filterattivo,
                },
             error: function (xhr, ajaxOptions, thrownError) {
                alert(xhr.status);
                alert(thrownError);
              }
            },
            'columnDefs': [
                { 'targets': 0, 'visible': false },
                { "targets": 1,                           "width": "50%" },
                { "targets": 2,                           "width": "30%" },
                { "targets": 3, "className": "dt-center", "orderable": false, "width": "20%" },
            ]
        } );
    }

    function applyFilters() {
        table = $('#myDatatable').DataTable();
        table.destroy();
        $("#myDatatable > tbody").html("");
        load_table();
        table = $('#myDatatable').DataTable();
        table.page(0).draw(false);
    }

    function resetFilters() {
        $("#titolo").val("");
        $("#testo").val("");
        $("#data_from").val("");
        $("#data_to").val("");
        if (ischecked("#attivo")) {
            $("#attivo").click();
        }

        applyFilters();
    }
</script>
<style>.ck-editor__editable_inline {    min-height: 400px;}</style><script src="ckeditor.js"></script><script>        ClassicEditor        .create( document.querySelector( '.editor' ), {                        toolbar: {                items: [                    'heading',                    '|',                    'bold',                    'italic',                    'link',                    'bulletedList',                    'numberedList',                    '|',                    'indent',                    'outdent',                    '|',                    'imageUpload',                    'blockQuote',                    'insertTable',                    'mediaEmbed',                    'undo',                    'redo'                ]            },            language: 'it',            image: {                toolbar: [                    'imageTextAlternative',                    'imageStyle:full',                    'imageStyle:side'                ]            },            table: {                contentToolbar: [                    'tableColumn',                    'tableRow',                    'mergeTableCells'                ]            },            licenseKey: '',                    } )        .then( editor => {            window.editor = editor;                                                                                        } )        .catch( error => {            console.error( 'Oops, something went wrong!' );            console.error( 'Please, report the following error on https://github.com/ckeditor/ckeditor5/issues with the build id and the error stack trace:' );            console.warn( 'Build id: 21b1dizg8ejr-8o65j7c6blw0' );            console.error( error );        } );</script>
