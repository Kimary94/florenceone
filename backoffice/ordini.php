<?php
    include "sys_header.php";
    if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(4, "read")) {header("location: ".linkto("sys_dashboard")); die();}}
?>

<body class="navbar-top<?php if ($settings_menu_closed == 1) { echo ' sidebar-xs'; } ?>">
    <div id="imagePreviewDiv" class="modal modal-xl fade imgpreview" style="z-index: 9999;"></div>
    <?php include "sys_navbar.php"; ?>
    <div class="page-content">
        <?php
        include "sys_sidebar.php";
        if ($_SESSION["user_username"] != "sysadmin") {
            if (!checkUserRight(4, "create")) {echo "<script>can_create = false;</script>";} else {echo "<script>can_create = true;</script>";}
            if (!checkUserRight(4, "edit")) {echo "<script>can_edit = false;</script>";} else {echo "<script>can_edit = true;</script>";}
        } else {
            echo "<script>can_create = true; can_edit = true;</script>";
        }
        ?>
        <div class="content-wrapper">

        <div class="page-header page-header-light">
            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">

                <div class="d-flex">
                    <i class="icon-money-2 mr-2 breadcrumb-item"></i><span style="margin-top: 8px;">Ordini</span>
                </div>

            </div>
        </div>

<div class="content unselectable">
    <div class="card card-collapsed" style="background-color: #f9f9f9;">
        <div class="card-header header-elements-inline" onmouseup="$('#searchexpand').click();" style="font-variant: all-petite-caps;">
            <div><i class="icon-search-7"></i> Filtri di ricerca</div>
            <div data-action="collapse" id="searchexpand" style="display:none;"></div>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-lg-10 col-md-9 col-sm-12 col-xs-12">
                    <div class="row">

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Giorno</label>
                        <input type="text"   class="form-control" autocomplete="off" id="giorno" value="<?php if (isset($_SESSION['ordine_filter_giorno'])) { echo $_SESSION['ordine_filter_giorno']; } ?>">
                    </div></div>

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Nome</label>
                        <input type="text"   class="form-control" autocomplete="off" id="nome" value="<?php if (isset($_SESSION['ordine_filter_nome'])) { echo $_SESSION['ordine_filter_nome']; } ?>">
                    </div></div>

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Cognome</label>
                        <input type="text"   class="form-control" autocomplete="off" id="cognome" value="<?php if (isset($_SESSION['ordine_filter_cognome'])) { echo $_SESSION['ordine_filter_cognome']; } ?>">
                    </div></div>

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Ragionesociale</label>
                        <input type="text"   class="form-control" autocomplete="off" id="ragionesociale" value="<?php if (isset($_SESSION['ordine_filter_ragionesociale'])) { echo $_SESSION['ordine_filter_ragionesociale']; } ?>">
                    </div></div>


                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Status cliente</label>
                        <select class="form-control select" id="id_status_cliente" >
                        <option value=""></option>
                        <?php
                            $selected_id = "";
                            if (isset($_SESSION["ordine_filter_id_status_cliente"])) {
                                if (intval($_SESSION["ordine_filter_id_status_cliente"]) > 0) {
                                    $selected_id = $_SESSION["ordine_filter_id_status_cliente"];
                                }
                            }
                            $query = $db->query("SELECT id, descrizione FROM status_cliente ORDER BY descrizione");
                            while($risultati = mysqli_fetch_array($query)) {
                                if ($selected_id == $risultati["id"]) { $selected = " SELECTED "; } else { $selected = " "; }
                                echo '<option value="'.$risultati["id"].'"'.$selected.'>'.$risultati["descrizione"].'</option>';
                            }
                        ?>
                        </select>
                    </div></div>


                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Status venditore</label>
                        <select class="form-control select" id="id_status_sede" >
                        <option value=""></option>
                        <?php
                            $selected_id = "";
                            if (isset($_SESSION["ordine_filter_id_status_sede"])) {
                                if (intval($_SESSION["ordine_filter_id_status_sede"]) > 0) {
                                    $selected_id = $_SESSION["ordine_filter_id_status_sede"];
                                }
                            }
                            $query = $db->query("SELECT id, descrizione FROM status_sede ORDER BY descrizione");
                            while($risultati = mysqli_fetch_array($query)) {
                                if ($selected_id == $risultati["id"]) { $selected = " SELECTED "; } else { $selected = " "; }
                                echo '<option value="'.$risultati["id"].'"'.$selected.'>'.$risultati["descrizione"].'</option>';
                            }
                        ?>
                        </select>
                    </div></div>


                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="text-center">
                                <button onclick="event.preventDefault(); applyFilters();"
                                        style="width: 150px; padding: 10px; border-radius: 5px;"
                                        class="btn btn-sm btn-outline-info">
                                            <i class="icon-search-1" style="float: left; margin-top: 2px;"></i> Cerca
                                </button><br>
                                <button onclick="event.preventDefault(); resetFilters();"
                                        style="width: 150px; padding: 10px; border-radius: 5px; margin: 3px 0px 3px 0px;"
                                        class="btn btn-sm btn-outline-warning">
                                            <i class="icon-cancel-alt" style="float: left; margin-top: 2px;"></i> Reset
                                </button><br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="card">
    <table id="myDatatable" class="display table table-striped datatable-responsive" style="width:100%; overflow: hidden;">
        <thead>
            <tr>
                <th><b>Nr.</b></th>
                <th><b>Nr.</b></th>
                <th><b>Giorno</b></th>
                <th><b>Cliente</b></th>
                <th><b>Stato ordine</b></th>
                <th><b>Stato lavorazione</b></th>
                <th><b>Modalità pagamento</b></th>
                <th><b>Totale ordine</b></th>
                <th><b>Transazione</b></th>
                <th><b>&nbsp;</b></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th><b>Nr.</b></th>
                <th><b>Nr.</b></th>
                <th><b>Giorno</b></th>
                <th><b>Cliente</b></th>
                <th><b>Stato ordine</b></th>
                <th><b>Stato lavorazione</b></th>
                <th><b>Modalità pagamento</b></th>
                <th><b>Totale ordine</b></th>
                <th><b>Transazione</b></th>
                <th><b>&nbsp;</b></th>
            </tr>
        </tfoot>
    </table>
</div>

</div>
</div>
</div>

<script>
    
    $(document).ready(function () {

        $('.select').select2({ width: '100%' });

        load_table();
        table = $('#myDatatable').DataTable();
        table.on('draw', function () {
            table.rows().every(function (rowIdx, tableLoop, rowLoop) {
                var values_in_row = this.data();
                $.ajax({
                    async: false,
                    type: "POST",
                    url: "controllers/ordini_controller.php",
                    data: {
                        "action": "button_record",
                        "id": values_in_row[0]
                    },
                    success: function (data, stato) {
                        values_in_row[9] = data;
                    }
                });
                this.data(values_in_row);
            });
        });
    });

    function load_table() {

        filtergiorno = "";
        if (parseInt($("#giorno").val().length) > 0) { filtergiorno = $("#giorno").val(); }

        filternome = "";
        if (parseInt($("#nome").val().length) > 0) { filternome = $("#nome").val(); }

        filtercognome = "";
        if (parseInt($("#cognome").val().length) > 0) { filtercognome = $("#cognome").val(); }

        filterragionesociale = "";
        if (parseInt($("#ragionesociale").val().length) > 0) { filterragionesociale = $("#ragionesociale").val(); }

        filterid_status_cliente = "";
        if ($("#id_status_cliente").val() !== null) { filterid_status_cliente = $("#id_status_cliente").val(); }

        filterid_status_sede = "";
        if ($("#id_status_sede").val() !== null) { filterid_status_sede = $("#id_status_sede").val(); }

        $('#myDatatable').DataTable( {
            'iDisplayLength': 25,
            'processing': true,
            'serverSide': true,
            'stateSave': true,
            'searching': false,
            'responsive': true,
            dom: '<"datatable-header"flB><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            buttons: [
            ],
            language: {
                lengthMenu: '<span>Mostra:</span> _MENU_',
                paginate: {'first': '|&larr;', 'last': '&rarr;|', 'next': '&rarr;', 'previous': '&larr;'},
                info:           "Stai visualizzando il record dal n. <b style='font-size:14px;'>_START_</b> al n. <b style='font-size:14px;'>_END_</b> su un totale di <b style='font-size:14px;'>_TOTAL_</b> record",
            },
            ajax: {
                url: 'controllers/ordini_controller.php',
                type: 'POST',
                data: {
                    action: "load_table",
                    filtergiorno: filtergiorno,
                    filternome: filternome,
                    filtercognome: filtercognome,
                    filterragionesociale: filterragionesociale,
                    filterid_status_cliente: filterid_status_cliente,
                    filterid_status_sede: filterid_status_sede
                    
                }
            },
            'columnDefs': [
                { 'targets': 0, "orderable": false, 'visible': false },
                { "targets": 1, "orderable": false, "width": "5%" },
                { "targets": 2, "orderable": false, "width": "5%" },
                { "targets": 3, "orderable": false, "width": "40%" },
                { "targets": 4, "orderable": false, "width": "15%" },
                { "targets": 5, "orderable": false, "width": "15%" },
                { "targets": 6, "orderable": false, "width": "15%" },
                { "targets": 7, "orderable": false, "className": "dt-right", "width": "15%" },
                { "targets": 8, "orderable": false, "width": "40%" },
                { "targets": 9, "orderable": false, "className": "dt-center", "width": "5%" },
            ]
        } );
    }

    function applyFilters() {
        table = $('#myDatatable').DataTable();
        table.destroy();
        $("#myDatatable > tbody").html("");
        load_table();
        table = $('#myDatatable').DataTable();
        table.page(0).draw(false);
    }

    function resetFilters() {
        $("#giorno").val("");
        $("#nome").val("");
        $("#cognome").val("");
        $("#ragionesociale").val("");
        $("#id_status_cliente").val("").trigger("change");
        $("#id_status_sede").val("").trigger("change");

        applyFilters();
    }

    
</script>