<?php
    include "sys_header.php";
    if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(2, "read")) {header("location: ".linkto("sys_dashboard")); die();}}
?>

<body class="navbar-top<?php if ($settings_menu_closed == 1) { echo ' sidebar-xs'; } ?>">
    <div id="imagePreviewDiv" class="modal modal-xl fade imgpreview" style="z-index: 9999;"></div>
    <?php include "sys_navbar.php"; ?>
    <div class="page-content">
        <?php
        include "sys_sidebar.php";
        if ($_SESSION["user_username"] != "sysadmin") {
            if (!checkUserRight(2, "create")) {echo "<script>can_create = false;</script>";} else {echo "<script>can_create = true;</script>";}
            if (!checkUserRight(2, "edit")) {echo "<script>can_edit = false;</script>";} else {echo "<script>can_edit = true;</script>";}
        } else {
            echo "<script>can_create = true; can_edit = true;</script>";
        }
        ?>
        <div class="content-wrapper">

        
        <!-- Page header -->
        <div class="page-header page-header-light">
            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">

                <div class="d-flex">
                    <i class="icon-clock-4 mr-2 breadcrumb-item"></i><span style="margin-top: 8px;">Ore di lavoro</span>
                </div>

                <div class="header-elements d-none" style="display: flex !important;">
                    <div class="breadcrumb justify-content-center">
                        <?php
                        $show_button_new = true;
                        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(2, "create")) { $show_button_new = false; }}
                        if ($show_button_new) { ?>
                            <a href="#" class="breadcrumb-elements-item" onclick="new_record();">
                                <i class="icon-plus-squared"></i>
                                Nuova registrazione
                            </a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>


<div class="content unselectable">
    <div class="card card-collapsed" style="background-color: #f9f9f9;">
        <div class="card-header header-elements-inline" onmouseup="$('#searchexpand').click();" style="font-variant: all-petite-caps;">
            <div><i class="icon-search-7"></i> Filtri di ricerca</div>
            <div data-action="collapse" id="searchexpand" style="display:none;"></div>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-lg-10 col-md-9 col-sm-12 col-xs-12">
                    <div class="row">

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Dal giorno</label>
                        <input type="date"   class="form-control" autocomplete="off" id="giorno_from" style="text-align:center;" id="giorno_from"  value="<?php if (isset($_SESSION['ore_filter_giorno_from'])) { echo $_SESSION['ore_filter_giorno_from']; } ?>">
                    </div></div>

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Al giorno</label>
                        <input type="date"   class="form-control" autocomplete="off" id="giorno_to" style="text-align:center;" id="giorno_to"  value="<?php if (isset($_SESSION['ore_filter_giorno_to'])) { echo $_SESSION['ore_filter_giorno_to']; } ?>">
                    </div></div>

                    <div class="col-lg-3 col-md-6" style="display:none;"><div class="form-group container_fields">
                        <label class="text-bold">Dalle ore</label>
                        <input type="number" class="form-control" autocomplete="off" id="ore_from" onkeypress="return checkInteger(event)" style="text-align:right;"  step="1"  id="ore_from"  value="<?php if (isset($_SESSION['ore_filter_ore_from'])) { echo $_SESSION['ore_filter_ore_from']; } ?>">
                    </div></div>

                    <div class="col-lg-3 col-md-6" style="display:none;"><div class="form-group container_fields">
                        <label class="text-bold">Alle ore</label>
                        <input type="number" class="form-control" autocomplete="off" id="ore_to" onkeypress="return checkInteger(event)" style="text-align:right;"  step="1"  id="ore_to"  value="<?php if (isset($_SESSION['ore_filter_ore_to'])) { echo $_SESSION['ore_filter_ore_to']; } ?>">
                    </div></div>

                    <div class="col-lg-3 col-md-6" style="display:none;"><div class="form-group container_fields">
                        <label class="text-bold">Da minuti</label>
                        <input type="number" class="form-control" autocomplete="off" id="minuti_from" onkeypress="return checkInteger(event)" style="text-align:right;"  step="1"  id="minuti_from"  value="<?php if (isset($_SESSION['ore_filter_minuti_from'])) { echo $_SESSION['ore_filter_minuti_from']; } ?>">
                    </div></div>

                    <div class="col-lg-3 col-md-6" style="display:none;"><div class="form-group container_fields">
                        <label class="text-bold">A minuti</label>
                        <input type="number" class="form-control" autocomplete="off" id="minuti_to" onkeypress="return checkInteger(event)" style="text-align:right;"  step="1"  id="minuti_to"  value="<?php if (isset($_SESSION['ore_filter_minuti_to'])) { echo $_SESSION['ore_filter_minuti_to']; } ?>">
                    </div></div>


                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Cliente</label>
                        <select class="form-control select" id="id_cliente" onchange="carica_progetti_ricerca();">
                        <option value=""></option>
                        <?php
                            $selected_id = "";
                            if (isset($_SESSION["ore_filter_id_cliente"])) {
                                if (intval($_SESSION["ore_filter_id_cliente"]) > 0) {
                                    $selected_id = $_SESSION["ore_filter_id_cliente"];
                                }
                            }
                            $query = $db->query("SELECT id, ragione_sociale FROM cliente ORDER BY ragione_sociale");
                            while($risultati = mysqli_fetch_array($query)) {
                                if ($selected_id == $risultati["id"]) { $selected = " SELECTED "; } else { $selected = " "; }
                                echo '<option value="'.$risultati["id"].'"'.$selected.'>'.$risultati["ragione_sociale"].'</option>';
                            }
                        ?>
                        </select>
                    </div></div>


                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Progetto</label>
                        <select class="form-control select" id="id_row_cliente" >
                        <option value=""></option>
                        <?php
                            $selected_id = "";
                            if (isset($_SESSION["ore_filter_id_row_cliente"])) {
                                if (intval($_SESSION["ore_filter_id_row_cliente"]) > 0) {
                                    $selected_id = $_SESSION["ore_filter_id_row_cliente"];
                                }
                            }
                            $query = $db->query("SELECT row_cliente.id, row_cliente.progetto, cliente.ragione_sociale FROM row_cliente LEFT JOIN cliente on cliente.id = row_cliente.id_cliente ORDER BY row_cliente.progetto");
                            while($risultati = mysqli_fetch_array($query)) {
                                if ($selected_id == $risultati["id"]) { $selected = " SELECTED "; } else { $selected = " "; }
                                echo '<option value="'.$risultati["id"].'"'.$selected.'>'.$risultati["progetto"].' ('.$risultati["ragione_sociale"].')'.'</option>';
                            }
                        ?>
                        </select>
                    </div></div>

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Fuori progetto</label>
                        <input type="text"   class="form-control" autocomplete="off" id="fuori_progetto" value="<?php if (isset($_SESSION['ore_filter_fuori_progetto'])) { echo $_SESSION['ore_filter_fuori_progetto']; } ?>">
                    </div></div>


                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Operatore</label>
                        <select class="form-control select" id="id_sys_users" >
                        <option value=""></option>
                        <?php
                            $selected_id = "";
                            if (isset($_SESSION["ore_filter_id_sys_users"])) {
                                if (intval($_SESSION["ore_filter_id_sys_users"]) > 0) {
                                    $selected_id = $_SESSION["ore_filter_id_sys_users"];
                                }
                            }
                            $query = $db->query("SELECT id, firstname FROM sys_users ORDER BY firstname");
                            while($risultati = mysqli_fetch_array($query)) {
                                if ($selected_id == $risultati["id"]) { $selected = " SELECTED "; } else { $selected = " "; }
                                echo '<option value="'.$risultati["id"].'"'.$selected.'>'.$risultati["firstname"].'</option>';
                            }
                        ?>
                        </select>
                    </div></div>


                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="text-center">
                                <button onclick="event.preventDefault(); applyFilters();"
                                        style="width: 150px; padding: 10px; border-radius: 5px;"
                                        class="btn btn-sm btn-outline-info">
                                            <i class="icon-search-1" style="float: left; margin-top: 2px;"></i> Search
                                </button><br>
                                <button onclick="event.preventDefault(); resetFilters();"
                                        style="width: 150px; padding: 10px; border-radius: 5px; margin: 3px 0px 3px 0px;"
                                        class="btn btn-sm btn-outline-warning">
                                            <i class="icon-cancel-alt" style="float: left; margin-top: 2px;"></i> Reset
                                </button><br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="card">
    <table id="myDatatable" class="display table table-striped datatable-responsive" style="width:100%; overflow: hidden;">
        <thead>
            <tr>
                <th><b>Id</b></th>
                <th><b>Giorno</b></th>
                <th><b>Ore</b></th>
                <th><b>Minuti</b></th>
                <th><b>Cliente</b></th>
                <th><b>Progetto</b></th>
                <th><b>Fuori progetto</b></th>
                <th><b>Operatore</b></th>

                <th><b></b></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th><b>Id</b></th>
                <th><b>Giorno</b></th>
                <th><b>Ore</b></th>
                <th><b>Minuti</b></th>
                <th><b>Cliente</b></th>
                <th><b>Progetto</b></th>
                <th><b>Fuori progetto</b></th>
                <th><b>Operatore</b></th>

                <th><b></b></th>
            </tr>
        </tfoot>
    </table>
</div>

<div id="modal_edit" class="modal fade" style="-webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none;" unselectable="on">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<h6 class="modal-title"><i class="icon-clock-4"></i> <span id="modal_mode"></span></h6>
			</div>
            <div class="modal-body">
                <div class="content">
                        
                    <input type="hidden" id="record_id">
            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">Giorno</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <input type="date" style="text-align:center; width:170px;" id="record_giorno" autocomplete="off" class="form-control"  >
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">Ore</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <input type="number" style="text-align:center; width:170px;" step="1" id="record_ore" onkeypress="return checkInteger(event)" autocomplete="off" class="form-control"  >
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">Minuti</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <input type="number" style="text-align:center; width:170px;" step="1" id="record_minuti" onkeypress="return checkInteger(event)" autocomplete="off" class="form-control"  >
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">Cliente</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <select class="form-control select" id="record_id_cliente" onchange="carica_progetti();"  >
                    <option value=""></option>
                    <?php
                        $query = $db->query("SELECT id, ragione_sociale FROM cliente ORDER BY ragione_sociale");
                        while($risultati = mysqli_fetch_array($query)) {
                            echo '<option value="'.$risultati["id"].'">'.$risultati["ragione_sociale"].'</option>';
                        }
                    ?>
                    </select>
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">Progetto</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <select class="form-control select" id="record_id_row_cliente"  >
                    <option value=""></option>
                    <?php
                        /*
                        $query = $db->query("SELECT id, progetto FROM row_cliente ORDER BY progetto");
                        while($risultati = mysqli_fetch_array($query)) {
                            echo '<option value="'.$risultati["id"].'">'.$risultati["progetto"].'</option>';
                        }
                        */
                    ?>
                    </select>
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">Fuori progetto</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <input type="text" id="record_fuori_progetto" maxlength="255" autocomplete="off" class="form-control" >
                </div>
            </div>

                </div>
            </div>
			<div class="modal-footer">
                <button onclick="save_record();" class="btn btn-lg btn-outline-success savebutton"><i class="icon-ok-3"></i> Save</button>
                <button onclick="$('#modal_edit').modal('hide');" class="btn btn-lg btn-outline-warning"><i class="icon-undo"></i> Cancel</button>
			</div>
		</div>
	</div>
</div>


</div>
</div>
</div>


<script>
    
    function new_record() {
        $('#modal_mode').html('Nuova registrazione');
        $("#record_id").val("");
        $("#record_giorno").val("");
        $("#record_ore").val("");
        $("#record_minuti").val("");
        $("#record_id_cliente").val(0).trigger("change");
        $("#record_id_row_cliente").val(0).trigger("change");
        $("#record_fuori_progetto").val("");
        $("#record_id_sys_users").val(0).trigger("change");
        setTimeout(function(){ $("#record_giorno").focus(); }, 300);
        if (can_create) { $(".savebutton").show(); $(".dropzone").show(); $(".icon-trash-empty").show(); } else { $(".savebutton").hide();  $(".dropzone").hide(); $(".icon-trash-empty").hide(); }
        $('#modal_edit').modal({backdrop: 'static'});
    }

    function edit_record(id) {
        if (can_edit) { $('#modal_mode').html('Modifica registrazione'); } else { $('#modal_mode').html('Visualizza registrazione'); }
        $.ajax({
            async: false,
            type: "POST",
            url: "controllers/ore_di_lavoro_controller.php",
            data: {
                "action": "read_record",
                "id": id
            },
            success: function (data, stato) {
                var data = $.parseJSON(data);
                if (data == "NOT AUTHORIZED") { return; }

                $("#record_id").val(data["record_id"]);
                $("#record_giorno").val(data["record_giorno"]);
                $("#record_ore").val(data["record_ore"]);
                $("#record_minuti").val(data["record_minuti"]);
                $("#record_id_cliente").val(data["record_id_cliente"]).trigger("change");
                $("#record_id_row_cliente").val(data["record_id_row_cliente"]).trigger("change");
                $("#record_fuori_progetto").val(data["record_fuori_progetto"]);
                $("#record_id_sys_users").val(data["record_id_sys_users"]).trigger("change");
                setTimeout(function(){ $("#record_giorno").focus(); }, 300);
                if (can_edit) { $(".savebutton").show(); $(".dropzone").show(); $(".icon-trash-empty").show(); } else { $(".savebutton").hide(); $(".dropzone").hide(); $(".icon-trash-empty").hide(); }
                $('#modal_edit').modal({backdrop: 'static'});
            }
        });
    }
    
    function save_record() {
        var requiredfields = "";
        $(".modal-body :required").each(function(){
            if (($(this).val() == "") || ($(this).val() === null)) {
                if (requiredfields != "") { requiredfields = requiredfields.concat(", "); }
                var tmp_req_field = $(this).attr("id").replace("record_", "").replace("_", " ");
                if (tmp_req_field.substr(0, 3) == "id ") { tmp_req_field = tmp_req_field.replace("id ", ""); }
                requiredfields = requiredfields.concat(tmp_req_field);
            }
        })
        if (requiredfields != "") { toast("Required fields: ".concat(requiredfields), "error"); return; }

        $.ajax({
            async: false,
            type: "POST",
            url: "controllers/ore_di_lavoro_controller.php",
            data: {
                "action": "save_record",
                "id": $("#record_id").val(), 
                "giorno": $("#record_giorno").val(), 
                "ore": $("#record_ore").val(), 
                "minuti": $("#record_minuti").val(), 
                "id_cliente": $("#record_id_cliente").val(), 
                "id_row_cliente": $("#record_id_row_cliente").val(), 
                "fuori_progetto": $("#record_fuori_progetto").val(),
            },
            success: function (data, stato) {
                if (data != "OK") {
                    swal({
                        title: "Error",
                        text: "An error occurred during Save: " + data.toString(),
                        confirmButtonColor: "#66BB6A",
                        type: "warning"
                    });
                } else {
                    $('#modal_edit').modal('hide');
                    update_table();
                }
            }
        });
    }

    function update_table()  {
        table = $('#myDatatable').DataTable();
        table.destroy();
        $("#myDatatable > tbody").html("");
        load_table();
        table = $('#myDatatable').DataTable();
        table.page(0).draw(false);
    }

    $(document).ready(function () {

        $('.select').select2({ width: '100%' });

        load_table();
        table = $('#myDatatable').DataTable();
        table.on('draw', function () {
            table.rows().every(function (rowIdx, tableLoop, rowLoop) {
                var values_in_row = this.data();
                $.ajax({
                    async: false,
                    type: "POST",
                    url: "controllers/ore_di_lavoro_controller.php",
                    data: {
                        "action": "button_record",
                        "id": values_in_row[0]
                    },
                    success: function (data, stato) {
                        values_in_row[8] = data;
                    }
                });
                this.data(values_in_row);
            });
        });
    });


    function delete_record(id) {
        swal({
            title: "Sei sicuro?",
            text: "I dati non potranno essere recuperati",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: "Si, elimina!",
            cancelButtonText: "No, annulla!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    async: true,
                    type: "POST",
                    url : "controllers/ore_di_lavoro_controller.php",
                    data: {
                        "action": "delete_record",
                        "id": id
                    },
                    success: function (data) {
                        if (data === "OK") {
                            swal({
                                title: "Eliminato!",
                                text: "Il record è stato cancellato",
                                confirmButtonColor: "#66BB6A",
                                type: "success"
                            }, function () {
                                document.location.reload();
                            });
                        } else {
                            swal({
                                title: "Operazione annullata",
                                text: "Qualcosa è andato storto",
                                confirmButtonColor: "#2196F3",
                                type: "error"
                            });
                        }
                    },
                    error: function (data) {
                        swal({
                            title: "Operazione annullata",
                            text: "Qualcosa è andato storto",
                            confirmButtonColor: "#2196F3",
                            type: "error"
                        });
                    }
                });
            }
            else {
                swal({
                    title: "Operazione annullata",
                    text: "",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
    }


    function load_table() {

        filtergiorno_from = "";
        if (parseInt($("#giorno_from").val().length) > 0) { filtergiorno_from = $("#giorno_from").val(); }
        filtergiorno_to = "";
        if (parseInt($("#giorno_to").val().length) > 0) { filtergiorno_to = $("#giorno_to").val(); }

        filterore_from = "";
        if (parseInt($("#ore_from").val().length) > 0) { filterore_from = $("#ore_from").val(); }
        filterore_to = "";
        if (parseInt($("#ore_to").val().length) > 0) { filterore_to = $("#ore_to").val(); }

        filterminuti_from = "";
        if (parseInt($("#minuti_from").val().length) > 0) { filterminuti_from = $("#minuti_from").val(); }
        filterminuti_to = "";
        if (parseInt($("#minuti_to").val().length) > 0) { filterminuti_to = $("#minuti_to").val(); }

        filterid_cliente = "";
        if ($("#id_cliente").val() !== null) { filterid_cliente = $("#id_cliente").val(); }

        filterid_row_cliente = "";
        if ($("#id_row_cliente").val() !== null) { filterid_row_cliente = $("#id_row_cliente").val(); }

        filterfuori_progetto = "";
        if (parseInt($("#fuori_progetto").val().length) > 0) { filterfuori_progetto = $("#fuori_progetto").val(); }

        filterid_sys_users = "";
        if ($("#id_sys_users").val() !== null) { filterid_sys_users = $("#id_sys_users").val(); }

        $('#myDatatable').DataTable( {
            'iDisplayLength': 25,
            'processing': true,
            'serverSide': true,
            'stateSave': true,
            'searching': false,
            'responsive': true,
            dom: '<"datatable-header"flB><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            buttons: [{
                        extend: 'pdf',
                        orientation: 'landscape',
                        text: '<i class="icon-file-pdf"></i> PDF',
                        title: '',
                        className: 'btn btn-outline-danger',
                        exportOptions: {columns: [1, 2, 3, 4, 5, 6, 7]}
                    },
                    {
                        extend: 'excel',
                        text: '<i class="icon-file-excel"></i> Excel',
                        title: '',
                        className: 'btn btn-outline-success',
                        exportOptions: {columns: [1, 2, 3, 4, 5, 6, 7]}
                    }
            ],
            language: {
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: {'first': '|&larr;', 'last': '&rarr;|', 'next': '&rarr;', 'previous': '&larr;'}
            },
            ajax: {
                url: 'controllers/ore_di_lavoro_controller.php',
                type: 'POST',
                data: {
                    action: "load_table",
                    filtergiorno_from: filtergiorno_from,
                    filtergiorno_to:   filtergiorno_to,
                    filterore_from: filterore_from,
                    filterore_to:   filterore_to,
                    filterminuti_from: filterminuti_from,
                    filterminuti_to:   filterminuti_to,
                    filterid_cliente: filterid_cliente,
                    filterid_row_cliente: filterid_row_cliente,
                    filterfuori_progetto: filterfuori_progetto,
                    filterid_sys_users: filterid_sys_users,

                }
            },
            'columnDefs': [
                { 'targets': 0, 'visible': false },
                { "targets": 1,                           "width": "15%" },
                { "targets": 2, "className": "dt-center", "width": "10%" },
                { "targets": 3, "className": "dt-center", "width": "10%" },
                { "targets": 4,                           "width": "25%" },
                { "targets": 5,                           "width": "25%" },
                { "targets": 6,                           "width": "25%" },
                { "targets": 7,                           "width": "25%" },

                { "targets": 8, "className": "dt-center", "orderable": false, "width": "5%" },
            ]
        } );
    }

    function applyFilters() {
        table = $('#myDatatable').DataTable();
        table.destroy();
        $("#myDatatable > tbody").html("");
        load_table();
        table = $('#myDatatable').DataTable();
        table.page(0).draw(false);
    }

    function resetFilters() {
        $("#giorno_from").val("");
        $("#giorno_to").val("");
        $("#ore_from").val("");
        $("#ore_to").val("");
        $("#minuti_from").val("");
        $("#minuti_to").val("");
        $("#id_cliente").val("").trigger("change");
        $("#id_row_cliente").val("").trigger("change");
        $("#fuori_progetto").val("");
        $("#id_sys_users").val("").trigger("change");

        applyFilters();
    }

    
    function carica_progetti_ricerca() {
        $('#id_row_cliente').empty();

        if ( $('#id_cliente').val() > 0) {    
            $.ajax({
                async: false,
                type: "POST",
                url: "controllers/ore_di_lavoro_controller.php",
                data: {
                    "action": "progetti_di_un_cliente",
                    "id": $('#id_cliente').val()
                },
                success: function (data, stato) {
                    var data = $.parseJSON(data);
                    $("#id_row_cliente").append('<option value=""></option>');
                    data.forEach(function(element) {
                        $("#id_row_cliente").append('<option value="'.concat(element[0]).concat('">').concat(element[1]).concat('</option>'));
                    });
                }
            });
        } 
    }


    function carica_progetti() {
        $('#record_id_row_cliente').empty();

        if ( $('#record_id_cliente').val() > 0) {    
            $.ajax({
                async: false,
                type: "POST",
                url: "controllers/ore_di_lavoro_controller.php",
                data: {
                    "action": "progetti_di_un_cliente",
                    "id": $('#record_id_cliente').val()
                },
                success: function (data, stato) {
                    var data = $.parseJSON(data);
                    $("#record_id_row_cliente").append('<option value=""></option>');
                    data.forEach(function(element) {
                        $("#record_id_row_cliente").append('<option value="'.concat(element[0]).concat('">').concat(element[1]).concat('</option>'));
                    });
                }
            });
        } 
    }

</script>