<?php
    include "sys_header.php";
    if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(3, "read")) {header("location: ".linkto("sys_dashboard")); die();}}
?>
<style>
.datatable-header {display:none;}
</style>
<body class="navbar-top<?php if ($settings_menu_closed == 1) { echo ' sidebar-xs'; } ?>">
    <div id="imagePreviewDiv" class="modal modal-xl fade imgpreview" style="z-index: 9999;"></div>
    <?php include "sys_navbar.php"; ?>
    <div class="page-content">
        <?php
        include "sys_sidebar.php";
        if ($_SESSION["user_username"] != "sysadmin") {
            if (!checkUserRight(3, "create")) {echo "<script>can_create = false;</script>";} else {echo "<script>can_create = true;</script>";}
            if (!checkUserRight(3, "edit")) {echo "<script>can_edit = false;</script>";} else {echo "<script>can_edit = true;</script>";}
        } else {
            echo "<script>can_create = true; can_edit = true;</script>";
        }
        ?>
        <div class="content-wrapper">

        
        <!-- Page header -->
        <div class="page-header page-header-light">
            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">

                <div class="d-flex">
                    <i class="icon-calendar-times-o mr-2 breadcrumb-item"></i><span style="margin-top: 8px;">Servizi lavorazione</span>
                </div>

                <div class="header-elements d-none" style="display: flex !important;">
                    <div class="breadcrumb justify-content-center">
                        <?php
                        $show_button_new = true;
                        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(3, "create")) { $show_button_new = false; }}
                        if ($show_button_new) { ?>
                            <a href="servizi_lavorazione_edit.php" class="breadcrumb-elements-item">
                                <i class="icon-plus-squared"></i>
                                Nuovo servizio
                            </a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>


<div class="content unselectable">
    
<div class="card">
    <table id="myDatatable" class="display table table-striped datatable-responsive" style="width:100%; overflow: hidden;">
        <thead>
            <tr>
                <th><b>Id</b></th>
                <th><b>Descrizione</b></th>
                <th><b>Testo pubblico</b></th>
                <th><b>Neutro</b></th>
                <th><b>Ordine</b></th>
                <th><b>Azioni</b></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th><b>Id</b></th>
                <th><b>Descrizione</b></th>
                <th><b>Testo pubblico</b></th>
                <th><b>Neutro</b></th>
                <th><b>Ordine</b></th>
                <th><b>Azioni</b></th>
            </tr>
        </tfoot>
    </table>
</div>
</div>
</div>
</div>

<script>
    function update_table()  {
        table = $('#myDatatable').DataTable();
        table.destroy();
        $("#myDatatable > tbody").html("");
        load_table();
        table = $('#myDatatable').DataTable();
        table.page(0).draw(false);
    }

    $(document).ready(function () {

        $('.select').select2({ width: '100%' });

        load_table();
        table = $('#myDatatable').DataTable();
        table.on('draw', function () {
            table.rows().every(function (rowIdx, tableLoop, rowLoop) {
                var values_in_row = this.data();
                $.ajax({
                    async: false,
                    type: "POST",
                    url: "controllers/servizi_lavorazione_controller.php",
                    data: {
                        "action": "button_record",
                        "id": values_in_row[0]
                    },
                    success: function (data, stato) {
                        values_in_row[5] = data;
                    }
                });
                this.data(values_in_row);
            });
        });
    });


    function delete_record(id) {
        swal({
            title: "Sei sicuro?",
            text: "I dati non potranno essere recuperati",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: "Si, elimina!",
            cancelButtonText: "No, annulla!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    async: true,
                    type: "POST",
                    url : "controllers/servizi_lavorazione_controller.php",
                    data: {
                        "action": "delete_record",
                        "id": id
                    },
                    success: function (data) {
                        if (data === "OK") {
                            swal({
                                title: "Eliminato!",
                                text: "Il record è stato cancellato",
                                confirmButtonColor: "#66BB6A",
                                type: "success"
                            }, function () {
                                document.location.reload();
                            });
                        } else {
                            swal({
                                title: "Operazione annullata",
                                text: "Qualcosa è andato storto",
                                confirmButtonColor: "#2196F3",
                                type: "error"
                            });
                        }
                    },
                    error: function (data) {
                        swal({
                            title: "Operazione annullata",
                            text: "Qualcosa è andato storto",
                            confirmButtonColor: "#2196F3",
                            type: "error"
                        });
                    }
                });
            }
            else {
                swal({
                    title: "Operazione annullata",
                    text: "",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
    }


    function load_table() {

        $('#myDatatable').DataTable( {
            'iDisplayLength': 25,
            'processing': true,
            'serverSide': true,
            'stateSave': true,
            'searching': false,
            'responsive': true,
            dom: '<"datatable-header"flB><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            buttons: [],
            language: {
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: {'first': '|&larr;', 'last': '&rarr;|', 'next': '&rarr;', 'previous': '&larr;'},
                emptyTable:     "Nessun dato disponibile",
                info:           "Stai visualizzando il record dal n. <b style='font-size:14px;'>_START_</b> al n. <b style='font-size:14px;'>_END_</b> su un totale di <b style='font-size:14px;'>_TOTAL_</b> record",
                infoEmpty:      "",                
            },
            ajax: {
                url: 'controllers/servizi_lavorazione_controller.php',
                type: 'POST',
                data: {
                    action: "load_table",
                }
            },
            'columnDefs': [
                { 'targets': 0, 'visible': false },
                { "targets": 1,                           "width": "30%" },
                { "targets": 2,                           "width": "30%" },
                { "targets": 3, "className": "dt-center", "width": "15%" },
                { "targets": 4,                           "width": "15%" },

                { "targets": 5, "className": "dt-center", "orderable": false, "width": "10%" },
            ]
        } );
    }


    
</script>