<?php
    include "sys_header.php";
    if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(3, "read")) {header("location: ".linkto("sys_dashboard")); die();}}
?>

<body class="navbar-top<?php if ($settings_menu_closed == 1) { echo ' sidebar-xs'; } ?>">
    <div id="imagePreviewDiv" class="modal modal-xl fade imgpreview" style="z-index: 9999;"></div>
    <?php include "sys_navbar.php"; ?>
    <div class="page-content">
        <?php
        include "sys_sidebar.php";
        if (!isset($_GET['id'])) {
            if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(3, "create")) {die();}}
        } else {
            if (!checkUserRight(3, "edit")) {echo "<style>.newrow,.icon-trash-empty,.dropzone,.savebutton{display: none;}</style>";}
        }
        ?>
        <div class="content-wrapper">

        <div class="page-header page-header-light">
            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">

                <div class="d-flex">
                    <i class="icon-calendar-times-o mr-2 breadcrumb-item"></i>
                    <span style="margin-top: 8px;">
                    <?php
                        if (isset($_GET['id'])) {
                            if (checkUserRight(3, "edit")) {
                                echo "Modifica Servizio di lavorazione";
                            } else {
                                echo "Visualizza Servizio di lavorazione";
                            }
                        } else {
                            echo "Nuovo Servizio di lavorazione";
                        }
                    ?>
                    </span>
                </div>

                <div class="header-elements d-none" style="display: flex !important;">
                    <div class="breadcrumb justify-content-center">
                        
                    </div>
                </div>
            </div>
        </div>

<div class="content unselectable">
    <input type="hidden" id="id" <?php if(isset($_GET['id'])) echo 'value="'.$_GET['id'].'"'; ?> >
    <div class="card">
        <div class="card-body">
            <div class="row">

                <div class="col-lg-4 col-md-6">
                    <div class="form-group container_fields">
                        <label class="text-bold">Descrizione</label>
                        <input type="text" id="descrizione"  maxlength="255"  autocomplete="off" class="form-control" >
                    </div>
                </div>
                
                <div class="col-lg-4 col-md-6">
                    <div class="form-group container_fields">
                        <label class="text-bold">Testo pubblico</label>
                        <input type="text" id="testo_pubblico"  maxlength="255"  autocomplete="off" class="form-control" >
                    </div>
                </div>

                <div class="col-lg-2 col-md-6">
                    <div class="form-group container_fields">
                        <label class="text-bold">Neutro</label><br>
                        <input type="checkbox" id="neutro" class="form-control" value="1"  style="width: 28px; margin: 0;" >
                    </div>
                </div>

                <div class="col-lg-2 col-md-6">
                    <div class="form-group container_fields">
                        <label class="text-bold">Ordine</label>
                        <input type="number" style="text-align:center;" step="1" id="ordine"    onkeypress="return checkInteger(event)" autocomplete="off" class="form-control"  >
                    </div>
                </div>


            </div>

            <div class="row">
                <div class="col-lg-12">
                    <div class="text-right">
                        <button onclick="event.preventDefault(); save_record();" class="btn btn-lg btn-success savebutton"><i class="icon-ok-3"></i> Salva</button>
                        <button onclick="event.preventDefault(); document.location.href='servizi_lavorazione.php'" class="btn btn-lg btn-warning"><i class="icon-undo"></i> Chiudi</button>
                    </div>
                </div>
            </div>
        </div>
    </div>


        <div class="card">
            <table id="myRowsDatatable" class="display table table-striped datatable-responsive" style="width:100%; overflow: hidden;">
                <thead>
                    <tr>
                        <th><b>Id&nbsp;</b></th>
                        <th><b>Scaglione (fino a)&nbsp;</b></th>
                        <th><b>Prezzo&nbsp;</b></th>
                        <th><b>Giorni lav.&nbsp;</b></th>
                        <th><b>Azioni</b></th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th><b>Id&nbsp;</b></th>
                        <th><b>Scaglione (fino a)&nbsp;</b></th>
                        <th><b>Prezzo&nbsp;</b></th>
                        <th><b>Giorni lav.&nbsp;</b></th>
                        <th><b>Azioni</b></th>
                    </tr>
                </tfoot>
            </table>
        </div>

</div>

<div id="modal_detail" class="modal fade" style="-webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none;" unselectable="on">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-primary">
                <h6 class="modal-title"><i class="icon-pencil"></i> Scaglione</h6>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
			</div>
            <div class="modal-body">
                <div class="content">
                    <input type="hidden" id="row_id" value="" >
                    <input type="hidden" id="row_id_servizio" value="" >
                    <div class="row form-group">
                        <div class="col-lg-4">
                            <label class="text-bold" style="margin: 9px 0px 0px 0px;">Scaglione (fino a)</label>
                        </div>
                        <div class="col-lg-8" style="margin-top: 3px;">
                            <input style="width:170px; text-align:center;"  type="number"    id="row_scaglione" value=""  class="form-control col-xs-6"  autocomplete="off" onkeypress="return checkInteger(event)"  >
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-lg-4">
                            <label class="text-bold" style="margin: 9px 0px 0px 0px;">Prezzo</label>
                        </div>
                        <div class="col-lg-8" style="margin-top: 3px;">
                            <input style="width:170px; text-align:right;" type="number" id="row_prezzo" value=""  class="form-control col-xs-6"  autocomplete="off" onkeypress="return checkFloat(event)" step="0.01"  >
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-lg-4">
                            <label class="text-bold" style="margin: 9px 0px 0px 0px;">Giorni lavorazione</label>
                        </div>
                        <div class="col-lg-8" style="margin-top: 3px;">
                            <input style="width:170px; text-align:center;"  type="number"    id="row_giorni" value=""  class="form-control col-xs-6"  autocomplete="off" onkeypress="return checkInteger(event)"  >
                        </div>
                    </div>

                </div>
            </div>
			<div class="modal-footer">
                <button onclick="save_row();" class="btn btn-lg btn-success savebutton"><i class="icon-ok-3"></i> Salva</button>
                <button onclick="$('#modal_detail').modal('hide');" class="btn btn-lg btn-warning"><i class="icon-undo"></i> Chiudi</button>
			</div>
		</div>
	</div>
</div>

</div>
</div>
</body>


<script type="text/javascript">


    $(document).ready(function () {
        $('.select').select2({ width: '100%' });


        <?php 
        if (isset($_GET["id"])) {
            echo "edit_record(".$_GET["id"].");";
        }
        ?> 

        setTimeout(function(){ $("#descrizione").focus(); }, 300);

        load_table();
        table = $("#myRowsDatatable").DataTable();
        table.on("draw", function () {
            table.rows().every(function (rowIdx, tableLoop, rowLoop) {
                var values_in_row = this.data();
                $.ajax({
                    async: false,
                    type: "POST",
                    url: "controllers/servizi_lavorazione_controller.php",
                    data: {
                        "action": "button_record_row",
                        "id": values_in_row[0]
                    },
                    success: function (data, stato) {
                        values_in_row[4] = data;
                    }
                });
                this.data(values_in_row);
            });
        });

    });


    function edit_record(id) {
        $.ajax({
            async: false,
            type: "POST",
            url: "controllers/servizi_lavorazione_controller.php",
            data: {
                "action": "read_record",
                "id": id
            },
            success: function (data, stato) {
                var data = $.parseJSON(data);
                if ((data.length == 0) || (data == "NOT AUTHORIZED")) {
                    location.href = 'servizi_lavorazione.php';
                } else {
                    $("#id").val(data["record_id"]);
                    $("#descrizione").val(data["record_descrizione"]);

                    if (data["record_neutro"] == 1) {
                        if (!ischecked("#neutro")) {
                            $("#neutro").click();
                        }
                    } else {
                        if (ischecked("#neutro")) {
                            $("#neutro").click();
                        }
                    }
                    $("#ordine").val(data["record_ordine"]);
                    $("#testo_pubblico").val(data["record_testo_pubblico"]);

                }
            }
        });
    }


    function save_record() {
        var requiredfields = "";
        $(".card-body :required").each(function(){
            if (($(this).val() == "") || ($(this).val() === null)) {
                if (requiredfields != "") { requiredfields = requiredfields.concat(", "); }
                var tmp_req_field = $(this).attr("id").replace("_", " ");
                if (tmp_req_field.substr(0, 3) == "id ") { tmp_req_field = tmp_req_field.replace("id ", ""); }
                requiredfields = requiredfields.concat(tmp_req_field);
            }
        })
        if (requiredfields != "") { toast("Required fields: ".concat(requiredfields), "error"); return; }

        $.ajax({
            async: false,
            type: "POST",
            url: "controllers/servizi_lavorazione_controller.php",
            data: {
                "action": "save_record",
                "id": $("#id").val(), 
                "descrizione": $("#descrizione").val(), 
                "neutro": ischecked("#neutro"), 
                "ordine": $("#ordine").val(), 
                "testo_pubblico": $("#testo_pubblico").val(), 

            },
            success: function (data, stato) {
                if (data != "OK") {
                    swal({
                        title: "Error",
                        text: "Qualcosa è andato storto " + data.toString(),
                        confirmButtonColor: "#66BB6A",
                        type: "warning"
                    });
                } else {
                    location.href = 'servizi_lavorazione.php'; 
                }
            }
        });
    }


    function add_row() {
        $("#row_id").val("");
        <?php if (!isset($_GET['id'])) { echo '$("#row_id_servizio").val("-'.$_SESSION['user_id'].'");'; } else { echo '$("#row_id_servizio").val("' . $_GET['id'] . '");'; } ?>
        $("#row_scaglione").val("");
        $("#row_prezzo").val("");
        $("#row_giorni").val("");
        setTimeout(function(){ $("#descrizione").focus(); }, 300);
        $("#modal_detail").modal({backdrop: 'static'});
    }
    
    function edit_row(id) {
        $.ajax({
            async: false,
            type: "POST",
            url : "controllers/servizi_lavorazione_controller.php",
            data: {
                "action": "read_record_row",
                "id": id
            },
            success : function (data) {
                if (data.length > 8) {
                    var data = $.parseJSON(data);
                    if (data == "NOT AUTHORIZED") { return; }
                    $("#row_id").val(data.id);
                    $("#row_id_servizio").val(data.id_servizio).trigger("change");
                    $("#row_scaglione").val(data.scaglione);
                    $("#row_prezzo").val(data.prezzo);
                    $("#row_giorni").val(data.giorni);
                    setTimeout(function(){ $("#row_scaglione").focus(); }, 300);
                    $("#modal_detail").modal({backdrop: 'static'});
                }
            }
        });
    }


    function save_row() {
        var requiredfields = "";
        $(".modal-body :required").each(function(){
            if (($(this).val() == "") || ($(this).val() === null)) {
                if (requiredfields != "") { requiredfields = requiredfields.concat(", "); }
                var tmp_req_field = $(this).attr("id").replace("row_", "").replace("_", " ");
                if (tmp_req_field.substr(0, 3) == "id ") { tmp_req_field = tmp_req_field.replace("id ", ""); }
                requiredfields = requiredfields.concat(tmp_req_field);
            }
        })
        if (requiredfields != "") { toast("Required fields: ".concat(requiredfields), "error"); return; }

        $.ajax({
            async: false,
            type: "POST",
            url: "controllers/servizi_lavorazione_controller.php",
            data: {
                "action": "save_record_row",
                "id": $('#row_id').val(),
                "id_servizio": $('#row_id_servizio').val(),
                "scaglione": $('#row_scaglione').val(),
                "prezzo": $('#row_prezzo').val(),
                "giorni": $('#row_giorni').val(),

            },
            success: function (data, stato) {
                if (data != "OK") {
                    swal({
                        title: "Error",
                        text: "An error occurred during Save: " + data.toString(),
                        confirmButtonColor: "#66BB6A",
                        type: "warning"
                    });
                } else {
                    $("#modal_detail").modal('hide');
                    update_table();
                    
                    if ($('#row_id').val() == '') {
                        setTimeout(function(){
                            add_row();
                        }, 700);
                    }
                    
                }
            }
        });
    }


    function update_table()  {
        table = $('#myRowsDatatable').DataTable();
        table.destroy();
        $("#myRowsDatatable > tbody").html("");
        load_table();
        table = $('#myRowsDatatable').DataTable();
        table.page(0).draw(false);
    }

    
    function load_table() {
        $('#myRowsDatatable').DataTable( {
            'processing': true,
            'serverSide': true,
            'stateSave': true,
            'searching': false,
            'responsive': true,
            dom: '<"datatable-header"flB><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            buttons: [{
                        text: '<i class="icon-plus-squared"></i> Nuovo scaglione',
                        className: 'btn btn-info newrow',
                        action: function ( e, dt, node, config ) {
                            add_row();
                        }
                    }
            ],
            language: {
                lengthMenu: '',
                paginate: {'first': '|&larr;', 'last': '&rarr;|', 'next': '&rarr;', 'previous': '&larr;'},
                emptyTable:     "Nessun dato disponibile",
                info:           "Stai visualizzando il record dal n. <b style='font-size:14px;'>_START_</b> al n. <b style='font-size:14px;'>_END_</b> su un totale di <b style='font-size:14px;'>_TOTAL_</b> record",
                infoEmpty:      "",    
            },
            ajax: {
                url: 'controllers/servizi_lavorazione_controller.php',
                type: 'POST',
                data: {
                    action: 'load_table_rows',
                    filter_id: <?php if (!isset($_GET['id'])) { echo "-".$_SESSION["user_id"]; } else { echo $_GET['id']; } ?>,
                }
            },
            'columnDefs': [
                { 'targets': 0, "orderable": false, 'visible': false },
                { "targets": 1, "className": "dt-center", "orderable": false, "width": "10%" },
                { "targets": 2, "className": "dt-right",  "orderable": false, "width": "10%" },
                { "targets": 3, "className": "dt-center", "orderable": false, "width": "10%" },
                { "targets": 4, "className": "dt-center", "orderable": false, "width": "5%" },
            ]
        } );
    }


    function delete_row(id) {
        swal({
            title: "Sei sicuro?",
            text: "I dati non potranno essere recuperati",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: "Si, elimina!",
            cancelButtonText: "No, annulla!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    async: true,
                    type: "POST",
                    url : "controllers/servizi_lavorazione_controller.php",
                    data: {
                        "action": "delete_record_row",
                        "id": id
                    },
                    success: function (data) {
                        if (data === "OK") {
                            swal({
                                title: "Eliminato!",
                                text: "Il record è stato cancellato",
                                confirmButtonColor: "#66BB6A",
                                type: "success"
                            }, function () {
                                update_table();
                            });
                        } else {
                            swal({
                                title: "Operazione annullata",
                                text: "Qualcosa è andato storto",
                                confirmButtonColor: "#2196F3",
                                type: "error"
                            });
                        update_table();
                        }
                    },
                    error: function (data) {
                        swal({
                            title: "Operazione annullata",
                            text: "Qualcosa è andato storto",
                            confirmButtonColor: "#2196F3",
                            type: "error"
                        });
                        update_table();
                    }
                });
            }
            else {
                swal({
                    title: "Operazione annullata",
                    text: "",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
    }
</script>