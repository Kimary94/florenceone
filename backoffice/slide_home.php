<?php
    include "sys_header.php";
    if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(14, "read")) {header("location: ".linkto("sys_dashboard")); die();}}
?>

<body class="navbar-top<?php if ($settings_menu_closed == 1) { echo ' sidebar-xs'; } ?>">
    <div id="imagePreviewDiv" class="modal modal-xl fade imgpreview" style="z-index: 9999;"></div>
    <?php include "sys_navbar.php"; ?>
    <div class="page-content">
        <?php
        include "sys_sidebar.php";
        if ($_SESSION["user_username"] != "sysadmin") {
            if (!checkUserRight(14, "create")) {echo "<script>can_create = false;</script>";} else {echo "<script>can_create = true;</script>";}
            if (!checkUserRight(14, "edit")) {echo "<script>can_edit = false;</script>";} else {echo "<script>can_edit = true;</script>";}
        } else {
            echo "<script>can_create = true; can_edit = true;</script>";
        }
        ?>
        <div class="content-wrapper">

        
        <!-- Page header -->
        <div class="page-header page-header-light">
            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">

                <div class="d-flex">
                    <i class="icon-file-image mr-2 breadcrumb-item"></i><span style="margin-top: 8px;">Slide home</span>
                </div>

                <div class="header-elements d-none" style="display: flex !important;">
                    <div class="breadcrumb justify-content-center">
                        <?php
                        $show_button_new = true;
                        if ($_SESSION["user_username"] != "sysadmin") {if (!checkUserRight(14, "create")) { $show_button_new = false; }}
                        if ($show_button_new) { ?>
                            <a href="#" class="breadcrumb-elements-item" onclick="new_record();">
                                <i class="icon-plus-squared"></i>
                                New record
                            </a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>


<div class="content unselectable">
    <div class="card card-collapsed" style="background-color: #f9f9f9;">
        <div class="card-header header-elements-inline" onmouseup="$('#searchexpand').click();" style="font-variant: all-petite-caps;">
            <div><i class="icon-search-7"></i> Filtri di ricerca</div>
            <div data-action="collapse" id="searchexpand" style="display:none;"></div>
        </div>

        <div class="card-body">
            <div class="row">
                <div class="col-lg-10 col-md-9 col-sm-12 col-xs-12">
                    <div class="row">

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Img sfondo</label>
                        <input type="text"   class="form-control" autocomplete="off" id="img_sfondo" value="<?php if (isset($_SESSION['slides_filter_img_sfondo'])) { echo $_SESSION['slides_filter_img_sfondo']; } ?>">
                    </div></div>

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Logo</label>
                        <input type="text"   class="form-control" autocomplete="off" id="logo" value="<?php if (isset($_SESSION['slides_filter_logo'])) { echo $_SESSION['slides_filter_logo']; } ?>">
                    </div></div>

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Titolo</label>
                        <input type="text"   class="form-control" autocomplete="off" id="titolo" value="<?php if (isset($_SESSION['slides_filter_titolo'])) { echo $_SESSION['slides_filter_titolo']; } ?>">
                    </div></div>

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">Sottotitolo</label>
                        <input type="text"   class="form-control" autocomplete="off" id="sottotitolo" value="<?php if (isset($_SESSION['slides_filter_sottotitolo'])) { echo $_SESSION['slides_filter_sottotitolo']; } ?>">
                    </div></div>

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">P 1 testo</label>
                        <input type="text"   class="form-control" autocomplete="off" id="p_1_testo" value="<?php if (isset($_SESSION['slides_filter_p_1_testo'])) { echo $_SESSION['slides_filter_p_1_testo']; } ?>">
                    </div></div>

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">P 1 link</label>
                        <input type="text"   class="form-control" autocomplete="off" id="p_1_link" value="<?php if (isset($_SESSION['slides_filter_p_1_link'])) { echo $_SESSION['slides_filter_p_1_link']; } ?>">
                    </div></div>

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">P 2 testo</label>
                        <input type="text"   class="form-control" autocomplete="off" id="p_2_testo" value="<?php if (isset($_SESSION['slides_filter_p_2_testo'])) { echo $_SESSION['slides_filter_p_2_testo']; } ?>">
                    </div></div>

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">P 2 link</label>
                        <input type="text"   class="form-control" autocomplete="off" id="p_2_link" value="<?php if (isset($_SESSION['slides_filter_p_2_link'])) { echo $_SESSION['slides_filter_p_2_link']; } ?>">
                    </div></div>

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold" style="display: inherit;">Attiva</label><br>
                        <input type="checkbox"  class="form-control" id="attiva" value="1" <?php if (isset($_SESSION['slides_filter_attiva'])) { if ($_SESSION['slides_filter_attiva'] == 1) { echo " CHECKED "; } } ?> >
                    </div></div>

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">From ordine</label>
                        <input type="number" class="form-control" autocomplete="off" id="ordine_from" onkeypress="return checkInteger(event)" style="text-align:right;"  step="1"  id="ordine_from"  value="<?php if (isset($_SESSION['slides_filter_ordine_from'])) { echo $_SESSION['slides_filter_ordine_from']; } ?>">
                    </div></div>

                    <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                        <label class="text-bold">To ordine</label>
                        <input type="number" class="form-control" autocomplete="off" id="ordine_to" onkeypress="return checkInteger(event)" style="text-align:right;"  step="1"  id="ordine_to"  value="<?php if (isset($_SESSION['slides_filter_ordine_to'])) { echo $_SESSION['slides_filter_ordine_to']; } ?>">
                    </div></div>


                    </div>
                </div>
                <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">

                    <div class="row">
                        <div class="col-lg-12">
                            <div class="text-center">
                                <button onclick="event.preventDefault(); applyFilters();"
                                        style="width: 150px; padding: 10px; border-radius: 5px;"
                                        class="btn btn-sm btn-outline-info">
                                            <i class="icon-search-1" style="float: left; margin-top: 2px;"></i> Search
                                </button><br>
                                <button onclick="event.preventDefault(); resetFilters();"
                                        style="width: 150px; padding: 10px; border-radius: 5px; margin: 3px 0px 3px 0px;"
                                        class="btn btn-sm btn-outline-warning">
                                            <i class="icon-cancel-alt" style="float: left; margin-top: 2px;"></i> Reset
                                </button><br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="card">
    <table id="myDatatable" class="display table table-striped datatable-responsive" style="width:100%; overflow: hidden;">
        <thead>
            <tr>
                <th><b>Id</b></th>
                <th><b>Titolo</b></th>
                <th><b>Sottotitolo</b></th>
                <th><b>Attiva</b></th>
                <th><b>Ordine</b></th>

                <th><b>Actions</b></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th><b>Id</b></th>
                <th><b>Titolo</b></th>
                <th><b>Sottotitolo</b></th>
                <th><b>Attiva</b></th>
                <th><b>Ordine</b></th>

                <th><b>Actions</b></th>
            </tr>
        </tfoot>
    </table>
</div>

<div id="modal_edit" class="modal fade" style="-webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none;" unselectable="on">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header bg-primary">
				<h6 class="modal-title"><i class="icon-file-image"></i> <span id="modal_mode"></span></h6>
			</div>
            <div class="modal-body">
                <div class="content">
                        
                    <input type="hidden" id="record_id">
            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">Img sfondo</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <div class="row">
                        <div class="col-lg-12 dropzonerow">
                            <form class="dropzone" id="dropzone_img_sfondo"></form>
                        </div>
                        <div class="col-lg-12" id="fileContainer_img_sfondo"></div>    
                    </div>
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">Logo</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <div class="row">
                        <div class="col-lg-12 dropzonerow">
                            <form class="dropzone" id="dropzone_logo"></form>
                        </div>
                        <div class="col-lg-12" id="fileContainer_logo"></div>    
                    </div>
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">Titolo</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <input type="text" id="record_titolo" maxlength="255" autocomplete="off" class="form-control" >
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">Sottotitolo</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <input type="text" id="record_sottotitolo" maxlength="255" autocomplete="off" class="form-control" >
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">P 1 testo</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <input type="text" id="record_p_1_testo" maxlength="255" autocomplete="off" class="form-control" >
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">P 1 link</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <input type="text" id="record_p_1_link" maxlength="255" autocomplete="off" class="form-control" >
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">P 2 testo</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <input type="text" id="record_p_2_testo" maxlength="255" autocomplete="off" class="form-control" >
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">P 2 link</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <input type="text" id="record_p_2_link" maxlength="255" autocomplete="off" class="form-control" >
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 0px 0px 0px 0px;">Attiva</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <input type="checkbox" id="record_attiva" class="form-control" value="1"  style="width: 28px; margin: 0;" >
                </div>
            </div>

            <div class="row form-group">
                <div class="col-lg-4">
                    <label class="text-bold" style="margin: 9px 0px 0px 0px;">Ordine</label>
                </div>
                <div class="col-lg-8" style="margin-top: 3px;">
                    <input type="number" style="text-align:center; width:170px;" step="1" id="record_ordine" onkeypress="return checkInteger(event)" autocomplete="off" class="form-control"  >
                </div>
            </div>

                </div>
            </div>
			<div class="modal-footer">
                <button onclick="save_record();" class="btn btn-lg btn-outline-success savebutton"><i class="icon-ok-3"></i> Save</button>
                <button onclick="$('#modal_edit').modal('hide');" class="btn btn-lg btn-outline-warning"><i class="icon-undo"></i> Cancel</button>
			</div>
		</div>
	</div>
</div>


</div>
</div>
</div>


<script>
    var FILELIST_img_sfondo = new Array;
    var FILELIST_logo = new Array;
    
    function new_record() {
        $('#modal_mode').html('New Slides');
        $("#record_id").val("");

        document.querySelector("#dropzone_img_sfondo").dropzone.removeAllFiles();
        FILELIST_img_sfondo = new Array;
        $("#fileContainer_img_sfondo").html("");


        document.querySelector("#dropzone_logo").dropzone.removeAllFiles();
        FILELIST_logo = new Array;
        $("#fileContainer_logo").html("");

        $("#record_titolo").val("");
        $("#record_sottotitolo").val("");
        $("#record_p_1_testo").val("");
        $("#record_p_1_link").val("");
        $("#record_p_2_testo").val("");
        $("#record_p_2_link").val("");
        if (ischecked("#record_attiva")) {
            $("#record_attiva").click();
        }
        $("#record_ordine").val("");
        setTimeout(function(){ $("#record_titolo").focus(); }, 300);
        if (can_create) { $(".savebutton").show(); $(".dropzone").show(); $(".icon-trash-empty").show(); } else { $(".savebutton").hide();  $(".dropzone").hide(); $(".icon-trash-empty").hide(); }
        $('#modal_edit').modal({backdrop: 'static'});
    }

    function edit_record(id) {
        if (can_edit) { $('#modal_mode').html('Edit Slides'); } else { $('#modal_mode').html('View Slides'); }
        $.ajax({
            async: false,
            type: "POST",
            url: "controllers/slide_home_controller.php",
            data: {
                "action": "read_record",
                "id": id
            },
            success: function (data, stato) {
                var data = $.parseJSON(data);
                if (data == "NOT AUTHORIZED") { return; }

                $("#record_id").val(data["record_id"]);

                $("#fileContainer_img_sfondo").html("");
                document.querySelector("#dropzone_img_sfondo").dropzone.removeAllFiles();
                FILELIST_img_sfondo = new Array;
                if (data.record_img_sfondo !== null) {
                    if (data.record_img_sfondo.length > 0) {
                        FILELIST_img_sfondo = JSON.parse(data.record_img_sfondo);
                        if (FILELIST_img_sfondo === null) {
                            FILELIST_img_sfondo = new Array;
                        } else {
                            drawFileList(FILELIST_img_sfondo, "FILELIST_img_sfondo", "fileContainer_img_sfondo");
                        }
                    }
                }

                $("#fileContainer_logo").html("");
                document.querySelector("#dropzone_logo").dropzone.removeAllFiles();
                FILELIST_logo = new Array;
                if (data.record_logo !== null) {
                    if (data.record_logo.length > 0) {
                        FILELIST_logo = JSON.parse(data.record_logo);
                        if (FILELIST_logo === null) {
                            FILELIST_logo = new Array;
                        } else {
                            drawFileList(FILELIST_logo, "FILELIST_logo", "fileContainer_logo");
                        }
                    }
                }
                $("#record_titolo").val(data["record_titolo"]);
                $("#record_sottotitolo").val(data["record_sottotitolo"]);
                $("#record_p_1_testo").val(data["record_p_1_testo"]);
                $("#record_p_1_link").val(data["record_p_1_link"]);
                $("#record_p_2_testo").val(data["record_p_2_testo"]);
                $("#record_p_2_link").val(data["record_p_2_link"]);

            if (data["record_attiva"] == 1) {
                if (!ischecked("#record_attiva")) {
                    $("#record_attiva").click();
                }
            } else {
                if (ischecked("#record_attiva")) {
                    $("#record_attiva").click();
                }
            }
                $("#record_ordine").val(data["record_ordine"]);
                setTimeout(function(){ $("#record_titolo").focus(); }, 300);
                if (can_edit) { $(".savebutton").show(); $(".dropzone").show(); $(".icon-trash-empty").show(); } else { $(".savebutton").hide(); $(".dropzone").hide(); $(".icon-trash-empty").hide(); }
                $('#modal_edit').modal({backdrop: 'static'});
            }
        });
    }
    
    function save_record() {
        var requiredfields = "";
        $(".modal-body :required").each(function(){
            if (($(this).val() == "") || ($(this).val() === null)) {
                if (requiredfields != "") { requiredfields = requiredfields.concat(", "); }
                var tmp_req_field = $(this).attr("id").replace("record_", "").replace("_", " ");
                if (tmp_req_field.substr(0, 3) == "id ") { tmp_req_field = tmp_req_field.replace("id ", ""); }
                requiredfields = requiredfields.concat(tmp_req_field);
            }
        })
        if (requiredfields != "") { toast("Required fields: ".concat(requiredfields), "error"); return; }

        $.ajax({
            async: false,
            type: "POST",
            url: "controllers/slide_home_controller.php",
            data: {
                "action": "save_record",
                "id": $("#record_id").val(), 
                "img_sfondo": JSON.stringify(FILELIST_img_sfondo),
                "logo": JSON.stringify(FILELIST_logo),
                "titolo": $("#record_titolo").val(), 
                "sottotitolo": $("#record_sottotitolo").val(), 
                "p_1_testo": $("#record_p_1_testo").val(), 
                "p_1_link": $("#record_p_1_link").val(), 
                "p_2_testo": $("#record_p_2_testo").val(), 
                "p_2_link": $("#record_p_2_link").val(), 
                "attiva": ischecked("#record_attiva"), 
                "ordine": $("#record_ordine").val(), 

            },
            success: function (data, stato) {
                if (data != "OK") {
                    swal({
                        title: "Error",
                        text: "An error occurred during Save: " + data.toString(),
                        confirmButtonColor: "#66BB6A",
                        type: "warning"
                    });
                } else {
                    $('#modal_edit').modal('hide');
                    update_table();
                }
            }
        });
    }

    function update_table()  {
        table = $('#myDatatable').DataTable();
        table.destroy();
        $("#myDatatable > tbody").html("");
        load_table();
        table = $('#myDatatable').DataTable();
        table.page(0).draw(false);
    }

    $(document).ready(function () {

        $('.select').select2({ width: '100%' });

        new Dropzone("#dropzone_img_sfondo", {
            init: function() {
                this.on("success", function(file, serverFileName) {
                    FILELIST_img_sfondo.push({"serverFileName" : serverFileName, "fileName" : file.name});
                    drawFileList(FILELIST_img_sfondo, "FILELIST_img_sfondo", "fileContainer_img_sfondo");
                });
                this.on("error", function(file, errorMessage) {
                    message_box("Error", errorMessage, "Ok", "error");
                });
            },
            url: "controllers/sys_file_uploader.php" });

        new Dropzone("#dropzone_logo", {
            init: function() {
                this.on("success", function(file, serverFileName) {
                    FILELIST_logo.push({"serverFileName" : serverFileName, "fileName" : file.name});
                    drawFileList(FILELIST_logo, "FILELIST_logo", "fileContainer_logo");
                });
                this.on("error", function(file, errorMessage) {
                    message_box("Error", errorMessage, "Ok", "error");
                });
            },
            url: "controllers/sys_file_uploader.php" });

        load_table();
        table = $('#myDatatable').DataTable();
        table.on('draw', function () {
            table.rows().every(function (rowIdx, tableLoop, rowLoop) {
                var values_in_row = this.data();
                $.ajax({
                    async: false,
                    type: "POST",
                    url: "controllers/slide_home_controller.php",
                    data: {
                        "action": "button_record",
                        "id": values_in_row[0]
                    },
                    success: function (data, stato) {
                        values_in_row[5] = data;
                    }
                });
                this.data(values_in_row);
            });
        });
    });


    function delete_record(id) {
        swal({
            title: "Sei sicuro?",
            text: "I dati non potranno essere recuperati",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: "Si, elimina!",
            cancelButtonText: "No, annulla!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    async: true,
                    type: "POST",
                    url : "controllers/slide_home_controller.php",
                    data: {
                        "action": "delete_record",
                        "id": id
                    },
                    success: function (data) {
                        if (data === "OK") {
                            swal({
                                title: "Eliminato!",
                                text: "Il record è stato cancellato",
                                confirmButtonColor: "#66BB6A",
                                type: "success"
                            }, function () {
                                document.location.reload();
                            });
                        } else {
                            swal({
                                title: "Operazione annullata",
                                text: "Qualcosa è andato storto",
                                confirmButtonColor: "#2196F3",
                                type: "error"
                            });
                        }
                    },
                    error: function (data) {
                        swal({
                            title: "Operazione annullata",
                            text: "Qualcosa è andato storto",
                            confirmButtonColor: "#2196F3",
                            type: "error"
                        });
                    }
                });
            }
            else {
                swal({
                    title: "Operazione annullata",
                    text: "",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
    }


    function load_table() {

        filterimg_sfondo = "";
        if (parseInt($("#img_sfondo").val().length) > 0) { filterimg_sfondo = $("#img_sfondo").val(); }

        filterlogo = "";
        if (parseInt($("#logo").val().length) > 0) { filterlogo = $("#logo").val(); }

        filtertitolo = "";
        if (parseInt($("#titolo").val().length) > 0) { filtertitolo = $("#titolo").val(); }

        filtersottotitolo = "";
        if (parseInt($("#sottotitolo").val().length) > 0) { filtersottotitolo = $("#sottotitolo").val(); }

        filterp_1_testo = "";
        if (parseInt($("#p_1_testo").val().length) > 0) { filterp_1_testo = $("#p_1_testo").val(); }

        filterp_1_link = "";
        if (parseInt($("#p_1_link").val().length) > 0) { filterp_1_link = $("#p_1_link").val(); }

        filterp_2_testo = "";
        if (parseInt($("#p_2_testo").val().length) > 0) { filterp_2_testo = $("#p_2_testo").val(); }

        filterp_2_link = "";
        if (parseInt($("#p_2_link").val().length) > 0) { filterp_2_link = $("#p_2_link").val(); }

        filterattiva = "";
        if (ischecked("#attiva")) { filterattiva = "1"; }

        filterordine_from = "";
        if (parseInt($("#ordine_from").val().length) > 0) { filterordine_from = $("#ordine_from").val(); }
        filterordine_to = "";
        if (parseInt($("#ordine_to").val().length) > 0) { filterordine_to = $("#ordine_to").val(); }

        $('#myDatatable').DataTable( {
            'iDisplayLength': 25,
            'processing': true,
            'serverSide': true,
            'stateSave': true,
            'searching': false,
            'responsive': true,
            dom: '<"datatable-header"flB><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            buttons: [{
                        extend: 'pdf',
                        orientation: 'landscape',
                        text: '<i class="icon-file-pdf"></i> PDF',
                        title: '',
                        className: 'btn btn-outline-danger',
                        exportOptions: {columns: [1, 2, 3, 4, 5, 6, 7, 8]}
                    },
                    {
                        extend: 'excel',
                        text: '<i class="icon-file-excel"></i> Excel',
                        title: '',
                        className: 'btn btn-outline-success',
                        exportOptions: {columns: [1, 2, 3, 4, 5, 6, 7, 8]}
                    }
            ],
            language: {
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: {'first': '|&larr;', 'last': '&rarr;|', 'next': '&rarr;', 'previous': '&larr;'}
            },
            ajax: {
                url: 'controllers/slide_home_controller.php',
                type: 'POST',
                data: {
                    action: "load_table",
                    filterimg_sfondo: filterimg_sfondo,
                    filterlogo: filterlogo,
                    filtertitolo: filtertitolo,
                    filtersottotitolo: filtersottotitolo,
                    filterp_1_testo: filterp_1_testo,
                    filterp_1_link: filterp_1_link,
                    filterp_2_testo: filterp_2_testo,
                    filterp_2_link: filterp_2_link,
                    filterattiva: filterattiva,
                    filterordine_from: filterordine_from,
                    filterordine_to:   filterordine_to,

                }
            },
            'columnDefs': [
                { 'targets': 0, 'visible': false },
                { "targets": 1,                           "width": "25%" },
                { "targets": 2,                           "width": "25%" },
                { "targets": 3,                           "width": "25%" },
                { "targets": 4,                           "width": "25%" },

                { "targets": 5, "className": "dt-center", "orderable": false, "width": "5%" },
            ]
        } );
    }

    function applyFilters() {
        table = $('#myDatatable').DataTable();
        table.destroy();
        $("#myDatatable > tbody").html("");
        load_table();
        table = $('#myDatatable').DataTable();
        table.page(0).draw(false);
    }

    function resetFilters() {
        $("#img_sfondo").val("");
        $("#logo").val("");
        $("#titolo").val("");
        $("#sottotitolo").val("");
        $("#p_1_testo").val("");
        $("#p_1_link").val("");
        $("#p_2_testo").val("");
        $("#p_2_link").val("");
        if (ischecked("#attiva")) {
            $("#attiva").click();
        }
        $("#ordine_from").val("");
        $("#ordine_to").val("");

        applyFilters();
    }

    
</script>