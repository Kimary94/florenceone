<?php include "sys_header.php"; ?>
<body class="navbar-top<?php if ($settings_menu_closed == 1) { echo ' sidebar-xs'; } ?>">
	<?php include "sys_navbar.php"; ?>
	<!-- Page content -->
	<div class="page-content">
		<?php include "sys_sidebar.php"; ?>
		<!-- Main content -->
		<div class="content-wrapper">
			<!-- Page header -->
			<div class="page-header page-header-light">
				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					<div class="d-flex">
                        <i class="icon-home mr-2 breadcrumb-item"></i><span style="margin-top: 8px;">Dashboard</span>
					</div>
				</div>
			</div>
			<!-- Content area -->
			<div class="content">



			</div>
		</div>	<!-- /Main content -->
	</div>	<!-- /Page content -->
</body>
</html>