<?php
    
    // Generate url from project base url
    function linkto($page){
        return substr($_SERVER['REQUEST_URI'], 0, strrpos($_SERVER['PHP_SELF'], "/") + 1) . $page;
    }

    function getPageName() {
        return substr($_SERVER['PHP_SELF'], strrpos($_SERVER['PHP_SELF'], "/") + 1, strpos($_SERVER['PHP_SELF'], ".php") - strrpos($_SERVER['PHP_SELF'], "/") - 1);
    }
    
    function checkUserRight($id_page, $user_right) {
        if ($_SESSION['user_username'] == "sysadmin") {
            return true;
        } else {
            $retv = false;
            global $db;
            if ($db->status()) { 
                $qry = "SELECT can_".$user_right." FROM sys_rights WHERE id_sys_groups = ".$_SESSION['user_id_group']." AND id_sys_pages = ".$id_page; 
                $rows = fetch_rows($qry); 
                foreach ( $rows as $row ) { 
                    if ( $row[0] == 1 ) { $retv = true; }
                }
            }
            return $retv;
        }
    }
    
?>