
<?php include "sys_header.php"; ?>

<body class="navbar-top<?php if ($settings_menu_closed == 1) { echo ' sidebar-xs'; } ?>">
    <?php include "sys_navbar.php"; ?>
    <style>
    .datatable-header{display:none;}
    </style>
    <div class="page-content">
        <?php include "sys_sidebar.php"; ?>
        <div class="content-wrapper">

            <div class="page-header page-header-light">
                <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">

                    <div class="d-flex">
                        <i class="icon-users-outline mr-2 breadcrumb-item"></i><span style="margin-top: 8px;">User groups</span>
                    </div>

                    <div class="header-elements d-none" style="display: flex !important;">
                        <div class="breadcrumb justify-content-center">
                            <a href="sys_groups_edit.php" class="breadcrumb-elements-item">
                                <i class="icon-plus-squared"></i>
                                New record
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="content unselectable">
                <div class="card">
                    <table id="myDatatable" class="display table table-striped datatable-responsive" style="width:100%; overflow: hidden;">
                        <thead style="background-color: #F9F9F9;">
                            <tr>
                                <th><b>Id&nbsp;</b></th>
                                <th><b>Group name&nbsp;</b></th>
                                <th><b>Actions</b></th>
                            </tr>
                        </thead>
                        <tfoot style="background-color: #F9F9F9;">
                            <tr>
                                <th><b>Id&nbsp;</b></th>
                                <th><b>Group name&nbsp;</b></th>
                                <th><b>Actions</b></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>


        </div>
    </div>


<script>
    $(document).ready(function () {
        load_table();
        table = $('#myDatatable').DataTable();
        table.on('draw', function () {
            table.rows().every(function (rowIdx, tableLoop, rowLoop) {
                var values_in_row = this.data();
                $.ajax({
                    async: false,
                    type: "POST",
                    url: "controllers/sys_groups_controller.php",
                    data: {
                        "action": "button_record",
                        "id": values_in_row[0]
                    },
                    success: function (data, stato) {
                        values_in_row[2] = data;
                    }
                });
                this.data(values_in_row);
            });
        });
    });

    function delete_record(id) {
        swal({
            title: "Sei sicuro?",
            text: "I dati non potranno essere recuperati",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: "Si, elimina!",
            cancelButtonText: "No, annulla!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    async: true,
                    type: "POST",
                    url : "controllers/sys_groups_controller.php",
                    data: {
                        "action": "delete_record",
                        "id": id
                    },
                    success: function (data) {
                        if (data === "OK") {
                            swal({
                                title: "Eliminato!",
                                text: "Il record è stato cancellato",
                                confirmButtonColor: "#66BB6A",
                                type: "success"
                            }, function () {
                                document.location.reload();
                            });
                        } else {
                            swal({
                                title: "Operazione annullata",
                                text: "Qualcosa è andato storto",
                                confirmButtonColor: "#2196F3",
                                type: "error"
                            });
                        }
                    },
                    error: function (data) {
                        swal({
                            title: "Operazione annullata",
                            text: "Qualcosa è andato storto",
                            confirmButtonColor: "#2196F3",
                            type: "error"
                        });
                    }
                });
            }
            else {
                swal({
                    title: "Operazione annullata",
                    text: "",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
    }

    function load_table() {
        $('#myDatatable').DataTable( {
            'iDisplayLength': 25,
            'processing': true,
            'serverSide': true,
            'stateSave': true,
            'searching': false,
            'responsive': true,
            dom: '<"datatable-header"flB><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            buttons: [],
            language: {
                lengthMenu: '',
                paginate: {'first': '|&larr;', 'last': '&rarr;|', 'next': '&rarr;', 'previous': '&larr;'}
            },
            ajax: {
                url: 'controllers/sys_groups_controller.php',
                type: 'POST',
                data: {
                    action: "load_table"
                }
            },
            'columnDefs': [
                { 'targets': 0, 'visible': false },
                { "targets": 1,                           "width": "25%" },
                { "targets": 2, "className": "dt-center", "orderable": false, "width": "5%" },
            ]
        } );
    }

    
</script>