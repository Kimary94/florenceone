
<?php include "sys_header.php"; ?>

<body class="navbar-top<?php if ($settings_menu_closed == 1) { echo ' sidebar-xs'; } ?>">
    <?php include "sys_navbar.php"; ?>
    <div class="page-content">
        <?php include "sys_sidebar.php"; ?>
        <div class="content-wrapper">

            <div class="page-header page-header-light">
                <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">

                    <div class="d-flex">
                        <i class="icon-users-outline mr-2 breadcrumb-item"></i>
                        <span style="margin-top: 8px;">
                        <?php
                            if (isset($_GET['id'])) {
                                echo "Edit user group";
                                $qry = "select group_name from sys_groups where id = ".$_GET['id'];
                                $rows = fetch_rows($qry);
                                foreach ( $rows as $row ) {
                                    $group_name = $row['group_name'];
                                }
                            } else {
                                echo "New user group";
                                $group_name = "";
                            }
                        ?>
                        </span>
                    </div>
                </div>
            </div>

            <div class="content unselectable">
                <input type="hidden" id="id" <?php if(isset($_GET['id'])) echo 'value="'.$_GET['id'].'"'; ?> >
                <div class="card" style="background-color: #f9f9f9;">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-3 col-md-6">
                                <div class="form-group container_fields">
                                    <label class="text-bold">Group name</label>
                                    <input type="text" id="group_name"  maxlength="255"  autocomplete="off" class="form-control" value="<?=$group_name?>" required>
                                </div>
                            </div>
                        </div>

                        <style>
                        .table_groups th, .table_groups td {padding: 12px 6px 12px 6px;}
                        .table_groups td {background-color: #fff;}
                        </style>

                        <div class="row" style="margin: 24px -21px 50px -21px;">
                            <table id="myRowsDatatable" class="table table_groups" style="font-size: 12px; width:100%; border: 1px solid #ccc;">
                                <thead style="background-color: #F9F9F9;">
                                    <tr>
                                        <th></th>
                                        <th><b>Read</b></th>
                                        <th><b>Create</b></th>
                                        <th><b>Edit</b></th>
                                        <th><b>Delete</b></th>
                                    </tr>
                                </thead>
                                <?php
                                    if (isset($_GET['id'])) {
                                        $id_group = $_GET['id'];
                                    } else {
                                        $id_group = "0";
                                    }
                                    $qry = "select sys_pages.id, sys_pages.page_name, sys_pages.icon, IFNULL(sys_rights.id, 0) as id_sys_rights, IFNULL(sys_rights.can_read, 0) as can_read, IFNULL(sys_rights.can_create, 0) as can_create, IFNULL(sys_rights.can_edit, 0) as can_edit, IFNULL(sys_rights.can_delete, 0) as can_delete from sys_pages left join sys_rights on sys_rights.id_sys_pages = sys_pages.id and sys_rights.id_sys_groups = ".$id_group." order by sys_pages.sorting";
                                    $rows = fetch_rows($qry);
                                    foreach ( $rows as $row ) {
                                ?>
                                    <tr>
                                        <td style="text-align: center;"><i class="<?=$row['icon']?>" style="float:left;"></i> <?=$row['page_name']?></td>
                                        <td><input style="width:22px; height:22px;" type="checkbox" group_right="can_read"   id_page="<?=$row['id']?>" id_sys_rights="<?=$row['id_sys_rights']?>" value="1" class="form-control" <?php if ($row['can_read'] == 1)   echo " checked "; ?> ></td>
                                        <td><input style="width:22px; height:22px;" type="checkbox" group_right="can_create" id_page="<?=$row['id']?>" id_sys_rights="<?=$row['id_sys_rights']?>" value="1" class="form-control" <?php if ($row['can_create'] == 1) echo " checked "; ?> ></td>
                                        <td><input style="width:22px; height:22px;" type="checkbox" group_right="can_edit"   id_page="<?=$row['id']?>" id_sys_rights="<?=$row['id_sys_rights']?>" value="1" class="form-control" <?php if ($row['can_edit'] == 1)   echo " checked "; ?> ></td>
                                        <td><input style="width:22px; height:22px;" type="checkbox" group_right="can_delete" id_page="<?=$row['id']?>" id_sys_rights="<?=$row['id_sys_rights']?>" value="1" class="form-control" <?php if ($row['can_delete'] == 1) echo " checked "; ?> ></td>
                                    </tr>
                                <?php } ?>

                                <tfoot style="background-color: #F9F9F9;">
                                    <tr>
                                        <th></th>
                                        <th><b>Read</b></th>
                                        <th><b>Create</b></th>
                                        <th><b>Edit</b></th>
                                        <th><b>Delete</b></th>
                                    </tr>
                                </tfoot>
                            </table>
                        </div>

                        <div class="row">
                            <div class="col-lg-12">
                                <div class="text-right">
                                    <button onclick="event.preventDefault(); save_record();" class="btn btn-lg btn-outline-success"><i class="icon-ok-3"></i> Save</button>
                                    <button onclick="event.preventDefault(); document.location.href='sys_groups.php'" class="btn btn-lg btn-outline-warning"><i class="icon-undo"></i> Cancel</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>


<script type="text/javascript">

    $(document).ready(function () {
        setTimeout(function(){ $("#group_name").focus(); }, 300);
    });

    function save_record() {
        var requiredfields = "";
        $(".card-body :required").each(function(){
            if (($(this).val() == "") || ($(this).val() === null)) {
                if (requiredfields != "") { requiredfields = requiredfields.concat(", "); }
                var tmp_req_field = $(this).attr("id").replace("_", " ");
                if (tmp_req_field.substr(0, 3) == "id ") { tmp_req_field = tmp_req_field.replace("id ", ""); }
                requiredfields = requiredfields.concat(tmp_req_field);
            }
        })
        if (requiredfields != "") { toast("Required fields: ".concat(requiredfields), "error"); return; }


        var group_id = 0;

        $.ajax({
            async: false,
            type: "POST",
            url: "controllers/sys_groups_controller.php",
            data: {
                "action": "save_record",
                "id": $('#id').val(), 
                "group_name": $('#group_name').val(), 
            },
            success: function (data, stato) {
                if (data == "KO") {
                    swal({
                        title: "Error",
                        text: "Qualcosa è andato storto " + data.toString(),
                        confirmButtonColor: "#66BB6A",
                        type: "warning"
                    });
                    return;
                } else {
                    group_id = data;
                }
            }
        });

        if (group_id > 0) {
            $('input[type=checkbox]').not(".check_all").each(function () {
                $.ajax({
                    async: false,
                    type: "POST",
                    url: "controllers/sys_groups_controller.php",
                    data: {
                        "action": "save_right",
                        "group_id": group_id, 
                        "group_right": $(this).attr('group_right'),
                        "id_page": $(this).attr('id_page'),
                        "id_sys_rights": $(this).attr('id_sys_rights'),
                        "checked": this.checked                    
                    },
                    success: function (data, stato) {
                        if (data != "OK") {
                            swal({
                                title: "Error",
                                text: "Qualcosa è andato storto " + data.toString(),
                                confirmButtonColor: "#66BB6A",
                                type: "warning"
                            });
                        }
                    }
                });
            });
        }

        location.href = 'sys_groups.php'; 


    }


</script>