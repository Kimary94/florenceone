<?php
    if (!isset($_SESSION)) { session_start(); }
    include "controllers/sys_database.php";
    include "sys_functions.php";
    $page_name = getPageName();

    $settings_project_name = "";
    $settings_menu_closed = 0;
    $settings_decimal_separator = 0;
    $settings_date_format = 0;
    $settings_smtp_mode = 0;
    $settings_smtp_host = "";
    $settings_smtp_port = "";
    $settings_smtp_secure = "";
    $settings_smtp_auth = 0;
    $settings_smtp_username = "";
    $settings_smtp_password = "";
    $settings_smtp_email = "";
    if (isset($db)) {
        if ($db->status()) {
            $qry = "SELECT * FROM sys_settings WHERE id = 1";
            $rows = fetch_rows($qry);
            foreach ( $rows as $row ) {
                $settings_project_name = $row['project_name'];
                $settings_menu_closed = $row['menu_closed'];
                $settings_decimal_separator = $row['decimal_separator'];
                $settings_date_format = $row['date_format'];
                $settings_smtp_mode = intval($row['smtp_mode']);
                $settings_smtp_host = $row['smtp_host'];
                $settings_smtp_port = intval($row['smtp_port']);
                $settings_smtp_secure = $row['smtp_secure'];
                $settings_smtp_auth = intval($row['smtp_auth']);
                $settings_smtp_username = $row['smtp_username'];
                $settings_smtp_password = $row['smtp_password'];
                $settings_smtp_email = $row['smtp_email'];
            }
        }
    }
    if ($settings_project_name == "") $settings_project_name = "Backoffice";

    // If no user session then redirect to Timeout page
    if ( (strtolower($page_name) != 'sys_login') && (strtolower($page_name) != 'sys_wizard') && (strtolower($page_name) != 'sys_timeout') && (!isset($_SESSION['user_id'])) ) {
        header("Location: ".linkto("sys_timeout"));
        die();
    }

    // Pages allowed only for Sys Admin
    if (isset($_SESSION['user_id'])) {
        $sys_admin_pages_allowed = ['sys_dashboard_admin', 'sys_project_settings', 'sys_project_pages', 'sys_users', 'sys_groups'];

        if (strtolower($_SESSION['user_username']) != 'sysadmin') {
            // USER CAN'T ACCESS RESTRICTED PAGES
            if (in_array(strtolower($page_name),$sys_admin_pages_allowed)) {
                header("Location: ".linkto("sys_dashboard"));
                die();
            }
        }
    }
    
    // Destroy session if Login page or Wizard page
    if ((strtolower($page_name) == 'sys_login') || (strtolower($page_name) == 'sys_wizard')) {
        session_destroy();
    }
?>

<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" type="image/png" href="assets/images/favicon.png"/>
    <title><?=$settings_project_name?></title>
    
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="assets/third-party/fontello/fontello.css" rel="stylesheet" type="text/css">
    <link href="assets/css/layout.css" rel="stylesheet" type="text/css">
    <link href="assets/css/components.css" rel="stylesheet" type="text/css">
    <link href="assets/css/colors.css" rel="stylesheet" type="text/css">
    <script src="assets/third-party/jquery/jquery-3.4.1.min.js"></script>
    <script src="assets/third-party/bootstrap/bootstrap.bundle.min.js"></script>
    <script src="assets/js/functions.js"></script>
    <script src="assets/js/app.js"></script>

    <link href="assets/third-party/sweetalert/sweetalert.css" rel="stylesheet">
    <script src="assets/third-party/sweetalert/sweetalert.min.js"></script>
    <link href="assets/third-party/native-toast/native-toast.css" rel="stylesheet">
    <script src="assets/third-party/native-toast/native-toast.min.js"></script>
	<script src="assets/third-party/switchery/switchery.min.js"></script>
	<script src="assets/third-party/dropzone/dropzone.js"></script>

    <?php if (($page_name == 'sys_users') || ($page_name == 'sys_my_profile')) { ?>
        <script src="assets/js/md5.js"></script>
    <?php } ?>
	
    <?php if (($page_name == 'sys_login') || ($page_name == 'sys_wizard')) { ?>
        <link href="assets/third-party/bootstrap/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="assets/css/floating-labels.css" rel="stylesheet">
    <?php } else { ?>
        <link href="assets/third-party/bootstrap/bootstrap_custom.css" rel="stylesheet" type="text/css">
		<script type="text/javascript" src="assets/third-party/perfect-scrollbar/perfect-scrollbar.min.js"></script>
		<script type="text/javascript" src="assets/js/layout_fixed_sidebar_custom.js"></script>
		<script type="text/javascript" src="assets/third-party/select2/select2.full.min.js"></script>
		<script type="text/javascript" src="assets/third-party/datatables/pdfmake.min.js"></script>
        <script type="text/javascript" src="assets/third-party/datatables/vfs_fonts.js"></script>
		<script type="text/javascript" src="assets/third-party/datatables/datatables.min.js"></script>
        <script> Dropzone.autoDiscover = false; </script>
    <?php } ?>
</head>