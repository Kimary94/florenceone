<?php
include "sys_header.php";

if (!file_exists("sys_config.php")) { 
    header("location: ".linkto("sys_wizard"));
    die();
}

$login_page = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
if (strpos($login_page, "?") > 0) { $login_page = substr($login_page, 0, strpos($login_page, "?")); }
$reset_token = "";
$display_reset = ' style="display:none;" ';
$display_login = ' ';

if (isset($_GET['reset'])) {
    if (strlen($_GET['reset']) == 30) {
        $display_reset = ' ';
        $display_login = ' style="display:none;" ';    
        $reset_token = $_GET['reset'];
        echo "<script>setTimeout(function(){ $('#inputNewPassword').val(''); $('#inputNewPassword').focus(); }, 200);</script>";
    } else {
        echo "<script>window.location.replace('".$login_page."');</script>";
    }
}
?>

<body>
    <div class="page-content">
        <div class="content-wrapper">
            
            <center><img class="mb-4" src="assets/images/logo_dark.png" style="width:300px;"></center>

            <?php if ($reset_token != "") { ?>
            <div class="form-signin" id="form-reset" <?=$display_reset?> >
                <div class="text-center mb-4">
                    <p>Inserisci la nuova password</a></p>
                </div>
                <div class="form-label-group">
                    <input type="password" id="inputNewPassword" class="form-control" placeholder="New password" style="height:46px;">
                    <label for="inputNewPassword" class="unselectable">Nuova password</label>
                </div>
                <button class="btn btn-lg btn-outline-primary btn-block" onclick="save_password();"><i class="icon-key-5 mr-2"></i> Salva nuova password</button>
            </div>
            <?php } ?>

            <div class="form-signin" id="form-signin" <?=$display_login?> >
                <div class="text-center mb-4">
                    
                </div>
                <div class="form-label-group">
                    <input type="text" id="inputUsername" class="form-control" placeholder="Username" style="height:46px;" autofocus>
                    <label for="inputUsername" class="unselectable">Username</label>
                </div>
                <div class="form-label-group">
                    <input type="password" id="inputPassword" class="form-control" placeholder="Password" style="height:46px;">
                    <label for="inputPassword" class="unselectable">Password</label>
                </div>
                <button class="btn btn-lg btn-outline-primary btn-block" onclick="login();"><b><i class="icon-key-5 mr-2"></i></b> Login</button>
                <!--
                <div class="text-right" style="margin-top:14px;">
                    <button class="btn" onclick="show_pwlost_form();"><i class="icon-mail"></i> Recupera Password</button>
                </div>
                -->
            </div>
            
            <div class="form-signin" id="form-recover" style="display:none;">
                <div class="text-center mb-4">
                    <p>Inserisci la mail per ricevere il link di reset</a></p>
                </div>
                <div class="form-label-group">
                    <input type="text" id="inputEmail" class="form-control" placeholder="Email" style="height:46px;">
                    <label for="inputEmail" class="unselectable">Email</label>
                </div>
                
                <button class="btn btn-lg btn-outline-primary btn-block" onclick="send_reset_link();"><i class="icon-mail mr-2"></i> Invia link di ripristino</button>
                <div class="text-right" style="margin-top:14px;">
                    <button class="btn" onclick="show_login_form();"><i class="icon-undo"></i> Torna al login</button>
                </div>
            </div>

            </span></p>
        </div>
    </div>
</body>
</html>

<script>
    function save_password() {
        showOverlay();
        $.ajax({
            url: "controllers/sys_login_controller",
            type: "POST",
            data: {
                'action': "save_password",
                'reset_token': "<?=$reset_token?>",
                'new_password': $('#inputNewPassword').val()
            },
            success: function (data, stato) {
                hideOverlay();
                var data = $.parseJSON(data).toString();
                if (data.substr(0,5).toUpperCase() == "ERROR") {
                    toast(data, "danger");
                } else {
                    message_box("Password reset", data, "Ok", "success", go_to_login);
                }
            }
        });
    }

    function show_pwlost_form() {
        $('#form-signin').hide();
        $('#form-recover').show();
    }

    function show_login_form() {
        $('#form-recover').hide();
        $('#form-signin').show();
    }

    function send_reset_link() {
        showOverlay();
        $.ajax({
            url: "controllers/sys_login_controller",
            type: "POST",
            data: {
                'action': "send_reset_link",
                'email': $('#inputEmail').val()
            },
            success: function (data, stato) {
                hideOverlay();
                var data = $.parseJSON(data).toString();
                if (data.substr(0,5).toUpperCase() == "ERROR") {
                    toast(data, "danger");
                } else {
                    message_box("Password reset", data, "Ok", "success", go_to_login);
                }
            }
        });
    }

    function go_to_login() {
        window.location.replace("<?=$login_page?>");
    }

    function login() {
        showOverlay();
        $.ajax({
            url: "controllers/sys_login_controller",
            type: "POST",
            data: {
                'action': "login",
                'username': $('#inputUsername').val(),
                'password': $('#inputPassword').val()
            },
            success: function (data, stato) {
                hideOverlay();
                var data = $.parseJSON(data).toString();
                if (data.substr(0,5).toUpperCase() == "ERROR") {
                    toast(data, "danger");
                } else {
                    if (data == 1) {
                        window.location.replace("<?=linkto("index")?>");
                    } else {
                        toast("Wrong Username / Password", "danger");
                    }
                }
            }
        });
    }
</script>