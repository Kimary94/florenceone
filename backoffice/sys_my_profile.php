<?php include "sys_header.php"; ?>
<body class="navbar-top<?php if ($settings_menu_closed == 1) { echo ' sidebar-xs'; } ?>">
	<?php include "sys_navbar.php"; ?>
	<!-- Page content -->
	<div class="page-content">
		<?php include "sys_sidebar.php"; ?>
		<!-- Main content -->
		<div class="content-wrapper">
			<!-- Page header -->
			<div class="page-header page-header-light">
				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					<div class="d-flex">
                        <i class="icon-user mr-2 breadcrumb-item"></i><span style="margin-top: 8px;">Mio profilo</span>
					</div>
				</div>
			</div>
			<!-- Content area -->
			<div class="content unselectable">
				<div class="card card-collapsed" style="background-color: #f9f9f9;">
					<div class="card-body">
						<div class="row">
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="row">
									<div class="col-lg-4 col-md-4"><div class="form-group container_fields">
										<label class="text-bold">Nome</label>
										<input type="text"   class="form-control" autocomplete="off" id="firstname" value="<?=$_SESSION['user_firstname']?>">
									</div></div>
									<div class="col-lg-4 col-md-4"><div class="form-group container_fields">
										<label class="text-bold">Cognome</label>
										<input type="text"   class="form-control" autocomplete="off" id="lastname" value="<?=$_SESSION['user_lastname']?>">
									</div></div>
									<div class="col-lg-4 col-md-4"><div class="form-group container_fields">
										<label class="text-bold">Email</label>
										<input type="text"   class="form-control" autocomplete="off" id="email" value="<?=$_SESSION['user_email']?>">
									</div></div>
								</div>
								<div class="row">
									<div class="col-lg-4 col-md-4"><div class="form-group container_fields">
										<label class="text-bold">Username</label>
										<input type="text"   class="form-control" autocomplete="off" id="username" style="background: #fff;" value="<?=$_SESSION['user_username']?>" disabled>
									</div></div>
									<div class="col-lg-4 col-md-4"><div class="form-group container_fields">
										<label class="text-bold">Password</label><br>
										<input type="text" class="form-control" autocomplete="off" id="password" style="width:64%; background: #fff; display:inline-block;" value="********" disabled>
										<button class="btn btn-lg btn-outline-primary" onclick="ask_password();" style="width: 38px; padding: 6px; float:right; margin-right: 5px;"><i class="icon-pencil-4"></i></button>
									</div></div>
								</div>
								<div class="row">
									<div class="col-lg-12">
										<div class="text-center">
											<button onclick="event.preventDefault(); saveMyProfile();"
													style="width: 150px; padding: 10px; border-radius: 5px;"
													class="btn btn-lg btn-outline-success">
														<i class="icon-ok-3" style="float: left; margin-top: 2px;"></i> Salva
											</button>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>	<!-- /Content area -->
		</div>	<!-- /Main content -->
	</div>	<!-- /Page content -->
	<script>
		function ask_password() {
			input_box("Imposta password", "Inserisci la password", "Ok", "Cancel", ask_password_next);
		}
		function ask_password_next(user_input) {
			if (user_input) {
				$('#password').val(md5(user_input));
			}
		}
		function saveMyProfile() {
			showOverlay();
			$.ajax({
				async: false,
				type: "POST",
				url: "controllers/sys_my_profile_controller.php",
				data: {
					"action": "save_my_profile",
					"firstname": $('#firstname').val(), 
					"lastname": $('#lastname').val(), 
					"email": $('#email').val(), 
					"password": $('#password').val()
				},
				success: function (data, stato) {
					console.log(data);
					hideOverlay();
					if (data != "OK") {
						toast("An error occurred during save", "danger");
					} else {
						toast("Settings saved", "success");
					}
				}
			});
		}
	</script>
</body>
</html>