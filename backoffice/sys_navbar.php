<div class="unselectable navbar navbar-expand-md navbar-light fixed-top">

    <!-- Desktop -->
    <div class="navbar-header navbar-dark d-none d-md-flex align-items-md-center">
        <!-- Top left image expanded -->
        <div class="navbar-brand navbar-brand-md">
            <a href="/" target="_blank"><img src="assets/images/logo_light.png" alt=""></a>
        </div>

        <!-- Top left image reduced -->
        <div class="navbar-brand navbar-brand-xs">
            <img src="assets/images/logo_icon_light.png" alt="" style="display: unset;">
        </div>
    </div>
    <!-- /Desktop -->


    <!-- Mobile -->
    <div class="d-flex flex-1 d-md-none">
        <!-- Top left image -->
        <div class="navbar-brand mr-auto">
            <img src="assets/images/logo_dark.png" alt="">
        </div>	

        <!-- Show/Hide navbar -->
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-mobile">
            <i class="icon-flow-tree"></i>
        </button>

        <!-- Show/Hide sidebar -->
        <button class="navbar-toggler sidebar-mobile-main-toggle" type="button">
            <i class="icon-menu-2"></i>
        </button>
    </div>
    <!-- /Mobile -->


    <!-- Navbar -->
    <div class="collapse navbar-collapse" id="navbar-mobile">
        
        <!-- Show/Hide sidebar -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a href="#" class="navbar-nav-link sidebar-control sidebar-main-toggle d-none d-md-block">
                    <i class="icon-menu-2"></i>
                </a>
            </li>
        </ul>

        <!-- placeholder -->
        <span style="margin-right: auto !important;"> </span>

        <!-- User menu -->
        <ul class="navbar-nav">
            <li class="nav-item dropdown dropdown-user">
                <a href="#" class="navbar-nav-link d-flex align-items-center dropdown-toggle" data-toggle="dropdown">
                    <img src="assets/images/user.png" class="rounded-circle mr-2" height="34" alt="">
                    <span><?=$_SESSION['user_firstname']." ".$_SESSION['user_lastname']?></span>
                </a>

                <div class="dropdown-menu dropdown-menu-right">
                    
                    <a href="#" onclick="logout();" class="dropdown-item"><i class="icon-logout-1"></i> Logout</a>
                </div>
            </li>
        </ul>
    </div>
    <!-- /Navbar -->
    
</div>

<script>
function logout(){
    $.ajax({
        url: "controllers/sys_login_controller",
        type: "POST",
        data: {
            'action': "logout",
        },
        success: function (data, stato) {
            window.location.replace('<?=linkto("sys_login")?>');
        }
    });
}
</script>