<?php include "sys_header.php"; ?>
<body class="navbar-top<?php if ($settings_menu_closed == 1) { echo ' sidebar-xs'; } ?>">
	<?php include "sys_navbar.php"; ?>
	<!-- Page content -->
	<div class="page-content">
		<?php include "sys_sidebar.php"; ?>
		<!-- Content wrapper -->
		<div class="content-wrapper">
			<!-- Page header -->
			<div class="page-header page-header-light">
				<div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">
					<div class="d-flex">
                        <i class="icon-newspaper-1 mr-2 breadcrumb-item"></i><span style="margin-top: 8px;">Pages</span>
					</div>
					<div class="header-elements d-none" style="display: flex !important;">
						<div class="breadcrumb justify-content-center">
							<a href="javascript:new_page();" class="breadcrumb-elements-item">
								<i class="icon-plus-squared"></i>
								New page
							</a>
						</div>
					</div>
				</div>
			</div>



			<!-- Content area -->
			<div class="content">

				<?php
					$pages_available = false;
					if ($db->status()) {
						$qry = "SELECT count(*) as conteggio FROM sys_pages";
						$rows = fetch_rows($qry);
						foreach ( $rows as $row ) { 
							$pages_available = true;
						}
					}
				?>

				<?php if ($pages_available) { ?>
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12">
						<div class="card">
							
							<div class="card-body">
								<div class="row" style="border-bottom: #888 1px solid; padding-top: 6px; padding-bottom: 6px; font-variant: small-caps; font-size:12px;">
									<div class="col-lg-2 col-md-4 col-sm-4">Page name</div>
									<div class="col-lg-2 col-md-4 col-sm-4">File name</div>
									<div class="col-lg-2 col-md-4 col-sm-4">Module</div>
								</div>
								<?php
									if ($db->status()) {
										$qry = "SELECT sys_pages.*, sys_modules.name as module_name FROM sys_pages LEFT JOIN sys_modules ON sys_pages.module = sys_modules.id ORDER BY sys_pages.sorting";
										$rows = fetch_rows($qry);
										$count_row = 0;
										foreach ( $rows as $row ) {
											$count_row += 1; ?>
											<div class="row" style="border-bottom: #ddd 1px solid; padding-top: 6px; padding-bottom: 6px; line-height: 40px;">
												<div class="col-lg-2 col-md-4 col-sm-4"><i style="width:16px; min-width:16px;" class="<?=$row["icon"]?> mr-2"></i><?=$row["page_name"]?></div>
												<div class="col-lg-2 col-md-4 col-sm-4">
													<?php if (file_exists($row["file_name"].".php")) { echo "<a href='".linkto($row["file_name"])."' target='_blank'>"; } ?>
														<?=$row["file_name"]?>.php
													<?php if (file_exists($row["file_name"].".php")) { echo "</a>"; } ?>
												</div>
												<div class="col-lg-2 col-md-4 col-sm-4"><?=$row["module_name"]?></div>
												<div class="col-lg-6 col-md-12 col-sm-12" style="text-align: right;">
													<button class="btn btn-outline-success" onclick="edit_page(<?=$row['id']?>);"><i class="icon-edit-1 mr-2"></i>Edit</button>
													<button class="btn btn-outline-primary" onclick="load_module(<?=$row['module']?>, <?=$row['id']?>);"><i class="icon-play-2 mr-2"></i>Module</button>
													<button class="btn btn-outline-danger"  onclick="remove_page(<?=$row['id']?>);"><i class="icon-trash-8 mr-2"></i>Delete</button>
													<i style="margin-left: 18px; <?php if ($count_row == 1) echo 'visibility:hidden;'; ?>"   onclick="move_page(<?=$row['id']?>, '-11');" class="icon-up-open-1" id="move_up_<?=$count_row?>"></i>
													<i onclick="move_page(<?=$row['id']?>, '+11');" class="icon-down-open-1" id="move_down_<?=$count_row?>"></i>
												</div>
											</div><?php
										}
									}
								?>
								<script>
								$('#move_down_<?=$count_row?>').css('visibility', 'hidden');
								</script>
							</div>
						</div>
					</div>
				</div> <!-- Row -->
				<?php } ?>


				<div id="modal_page" class="modal fade" data-keyboard="false" data-backdrop="static">
					<div id="modal-dialog" class="modal-dialog modal-md">
						<div class="modal-content">
							<div class="modal-header bg-primary">
								<h6 class="modal-title">
								<span id="modal_new" style="font-variant: small-caps; font-size: 15px;"><i class="icon-plus-squared mr-2"></i>New page</span>
								<span id="modal_edit" style="font-variant: small-caps; font-size: 15px;"><i class="icon-edit-1 mr-2"></i>Edit page</span>
								</h6>
							</div>
							<div class="modal-body">
								<div id="modal_page_fields" class="content" style="-webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none;" unselectable="on">
										
										<input type="hidden" id="page_id">
										
										<div class="row" style="margin-bottom: 6px;">
											<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12" style="padding: 10px 0px 0px 0px; font-variant: small-caps; font-size: 15px;">Page name</div>
											<div class="col-lg-9 col-md-12 col-sm-12 col-xs-12"><input type="text" id="page_page_name" autocomplete="off" class="form-control" onfocusout="$('#page_page_name').val( $('#page_page_name').val().trim() );" ></div>
										</div>

										<div class="row" style="margin-bottom: 6px;">
											<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12" style="padding: 10px 0px 0px 0px; font-variant: small-caps; font-size: 15px;">File name</div>
											<div class="col-lg-9 col-md-12 col-sm-12 col-xs-12"><input type="text" style="width: 81%; display: inline-block;" id="page_file_name" autocomplete="off" class="form-control" onfocusout="$('#page_file_name').val( $('#page_file_name').val().trim() );" onfocus="generateFileName();" >.php</div>
										</div>

										<div class="row" style="margin-bottom: 6px;">
											<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12" style="padding: 10px 0px 0px 0px; font-variant: small-caps; font-size: 15px;">Icon</div>
											<div class="col-lg-9 col-md-12 col-sm-12 col-xs-12">
												<input type="text" id="page_icon" class="form-control" style="width: 81%; display: inline-block;" readonly>
												<span style="border: 1px solid #0095ff; text-align: center; width: 54px; flex: none; float: right; display: inline-block; height: 37px; padding: 1px; border-radius: 4px; padding-top: 9px;" onclick="show_icons();"><i onclick="show_icons();" id="page_icon_show" style="padding-top: 2px;"></i></span>
											</div>
										</div>

										<div class="row" style="margin-bottom: 6px;">
											<div class="col-lg-3 col-md-12 col-sm-12 col-xs-12" style="padding: 10px 0px 0px 0px; font-variant: small-caps; font-size: 15px;">Module</div>
											<div class="col-lg-9 col-md-12 col-sm-12 col-xs-12">
												<select id="page_module" class="form-control select">
												<?php
													if ($db->status()) {
														$qry = "SELECT * FROM sys_modules ORDER BY id";
														$rows = fetch_rows($qry);
														foreach ( $rows as $row ) { ?>
															<option value="<?=$row['id']?>"><?=$row['name']?></option>
															<?php
														}
													}
												?>
												</select>
											</div>
										</div>
								</div>
								
								<div id="modal_page_icons" class="content" style="-webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none; display:none; margin-left: 10px;" unselectable="on">
										<div class="row">
												<input type="text" id="filter_icon" autocomplete="off" placeholder="Filter" class="form-control" style="width: 200px; margin-bottom: 20px; border: 1px solid #2196f3;" onkeyup="filter_icon();" >
										</div>
										<div class="row" id="div_of_icons">
										<?php
											$last_icon = "";
											$file_icons = file_get_contents ("assets/third-party/fontello/fontello.css");
											$icons = explode(".icon-", $file_icons);
											foreach ($icons as $value) {
												if (strpos($value, "content") > 0) {
													$icon = substr($value, 0, strpos($value, ":"));
													echo "<i onclick='icon_selected(\"icon-".$icon."\");' class='icon icon-".$icon." mr-2' style='font-size: 22px; width: 50px; height: 50px; padding-bottom: 12px; color: #000;'></i>";
												}
											}

											?>
										</div>
								</div>

							</div>
							<div class="modal-footer">
								<button onclick="save_page();" class="btn btn-lg btn-outline-success"><i class="icon-ok-3 mr-2"></i>Save</button>
								<button onclick="$('#modal_page').modal('hide');" class="btn btn-lg btn-outline-warning"><i class="icon-undo mr-2"></i>Cancel</button>
							</div>
						</div>
					</div>
				</div>




				<div id="modal_module" class="modal fade" data-keyboard="false" data-backdrop="static">
					<div id="div_load_module" class="modal-dialog modal-xl">
						
					</div>
				</div>


			</div>	<!-- /Content area -->
		</div>	<!-- /Content wrapper -->
	</div>	<!-- /Page content -->
</body>
</html>

<script>
	function move_page(idpage, step) {
		showOverlay();
		$.ajax({
			url: "controllers/sys_project_pages_controller",
			type: "POST",
			data: {
				'action': 'move',
				'id': idpage,
				'step': step
			},
			success: function (data, stato) {
				hideOverlay();
				var data = $.parseJSON(data);
				if (data[0] == 0) {
					message_box("Error", data[1], "Ok", "error");
				} else {
					reload_page();
				}
			}
		});
	}

	function filter_icon() {
		if ($('#filter_icon').val() == '') {
			$('#div_of_icons').find('.icon').css('display', 'block');
			//$('.icon').css('display', 'block');
		} else {
			filter_input = $('#filter_icon').val();
			$('#div_of_icons').find('.icon').css('display', 'none');
			//$('.icon').css('display', 'none');
			$('#div_of_icons').find('[class*="' + filter_input + '"]').css('display', 'block');
			//$('[class*="' + filter_input + '"]').css('display', 'block');
		}
	}

	var selected_page;

	function remove_page(page_id) {
		selected_page = page_id;
		true_false_box("Confirm", "Are you sure to remove this page?", "Yes, remove!", "No, wait...", "warning",  remove_page_step2);
	}

	function remove_page_step2(user_input) {
		if (user_input) {
			showOverlay();
			$.ajax({
				url: "controllers/sys_project_pages_controller",
				type: "POST",
				data: {
					'action': 'remove',
					'id': selected_page
				},
				success: function (data, stato) {
					reload_page();
				}
			});
		}
	}

	function generateFileName() {
		if (($('#page_file_name').val().length == 0) && (($('#page_page_name').val().length > 0))) {
			$('#page_file_name').val( $('#page_page_name').val().trim().replace(/ /g, "_").toLowerCase() );
		}
	}

	function new_page() {
		$('#modal_new').css('display', 'block');
		$('#modal_edit').css('display', 'none');
		$('#page_id').val('');
		$('#page_page_name').val('');
		$('#page_icon').val('');
		$('#page_icon_show').removeClass();
		$('#page_file_name').val('');
		
		$('#page_module').prop('disabled', false);
		$('#modal_page').modal('show');
	}

	function edit_page(page_id) {
		showOverlay();
		$.ajax({
			url: "controllers/sys_project_pages_controller",
			type: "POST",
			data: {
				'action': 'read',
				'id': page_id
			},
			success: function (data, stato) {
				hideOverlay();
				var data = $.parseJSON(data);
				if (data[0] == 0) {
					message_box("Error", data[1], "Ok", "error");
				} else {
					$('#modal_new').css('display', 'none');
					$('#modal_edit').css('display', 'block');			
					$('#page_id').val(data[1]['id']);
					$('#page_page_name').val(data[1]['page_name']);
					$('#page_icon').val(data[1]['icon']);
					$('#page_icon_show').removeClass();
					$('#page_icon_show').addClass(data[1]['icon']);
					$('#page_file_name').val(data[1]['file_name']);
					$('#page_module').prop('disabled', true);
					$('#page_module').val(data[1]['module']);
					$('#modal_page').modal('show');
				}
			}
		});
	}


	function show_icons() {
		$('.modal-footer').css('display', 'none');
		$('#modal_page_fields').css('display', 'none');
		$('#modal-dialog').removeClass();
		$('#modal-dialog').addClass("modal-dialog modal-xl");
		$('#modal_page_icons').css('display', 'block');
		$('#filter_icon').focus();
	}

	function icon_selected(icon_name) {
		$('#modal_page_icons').css('display', 'none');
		$('#modal-dialog').removeClass();
		$('#modal-dialog').addClass("modal-dialog modal-md");
		$('#modal_page_fields').css('display', 'block');
		$('.modal-footer').css('display', 'flex');
		$('#page_icon').val(icon_name);
		$('#page_icon_show').removeClass();
		$('#page_icon_show').addClass(icon_name);
	}
	
	function save_page() {
		if ($('#page_page_name').val().trim().length == 0) {
			message_box("Error", "Missing value: Page Name", "Ok", "warning");
			return;
		}
		if ($('#page_file_name').val().replace(/ /g, "").length == 0) {
			message_box("Error", "Missing value: File Name", "Ok", "warning");
			return;
		}
		if ($('#page_file_name').val().replace(/ /g, "").toLowerCase() == "index") {
			message_box("Error", "Invalid file name", "Ok", "warning");
			return;
		}
		if ($('#page_file_name').val().replace(/ /g, "").toLowerCase().substr(0,4) == "sys_") {
			message_box("Error", "Invalid file name", "Ok", "warning");
			return;
		}
		if ($('#page_icon').val().length == 0) {
			message_box("Error", "Missing value: Icon", "Ok", "warning");
			return;
		}

		showOverlay();
		$.ajax({
			url: "controllers/sys_project_pages_controller",
			type: "POST",
			data: {
				'action': 'save',
				'id': $('#page_id').val(),
				'page_name': $('#page_page_name').val().trim(),
				'icon': $('#page_icon').val(),
				'file_name': $('#page_file_name').val().replace(/ /g, ""),
				'module': $('#page_module').val()
			},
			success: function (data, stato) {
				hideOverlay();
				var data = $.parseJSON(data);
				if (data[0] == 0) {
					message_box("Error", data[1], "Ok", "error");
				} else {
					message_box("Success", data[1], "Ok", "success", reload_page);
				}
			}
		});
	}


	function load_module(module_id, page_id) {
		linkto = window.location.href.replace("sys_project_pages", 'controllers/sys_module'.concat(module_id).concat('?page_id=').concat(page_id) );
		$('#div_load_module').load(linkto);
		$('#modal_module').modal('show');
	}
</script>