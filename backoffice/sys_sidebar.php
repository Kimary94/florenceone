<div class="sidebar sidebar-dark sidebar-main sidebar-fixed sidebar-expand-md">

    <!-- Hide/Fullscreen mobile sidebar -->
    <div class="sidebar-mobile-toggler text-center">
        <a href="#" class="sidebar-mobile-main-toggle">
            <i class="icon-left-dir"></i>
        </a>
        <a href="#" class="sidebar-mobile-expand">
            <i class="icon-resize-full-3"></i>
            <i class="icon-resize-small-2"></i>
        </a>
    </div>
    

    <!-- Sidebar content -->
    <div class="sidebar-content">
        

        
        <!-- Main navigation -->
        <?php $pagename = getPageName(); ?>
        <div class="card card-sidebar-mobile unselectable">
            <ul class="nav nav-sidebar" data-nav-type="accordion">

                <li class="nav-item-header"><div class="text-uppercase font-size-xs line-height-xs">Menu</div></li>
                
                <?php
                    /* USER PAGES */
                    if ($db->status()) {
                        $qry = "SELECT * FROM sys_pages ORDER BY sorting";
                        $rows = fetch_rows($qry);
                        foreach ( $rows as $row ) {
                            if (checkUserRight($row["id"], "read")) {
                                $target_page = str_replace('.php', '', $row['file_name']);
                                echo '<li class="nav-item"><a href="' . linkto($target_page) .'" class="nav-link ' .  (($pagename == $target_page) ? "active" : "") . '"><i style="width:16px; min-width:16px;" class="' . $row['icon'] . '"></i> <span>' . $row['page_name'] . '</span></a></li>';
                            }
                        }
                    }
                ?>

            </ul>
        </div>	<!-- /Main navigation -->
    </div>	<!-- /Sidebar content -->
</div>

<script>
function logout(){
    $.ajax({
        url: "controllers/sys_login_controller",
        type: "POST",
        data: {
            'action': "logout",
        },
        success: function (data, stato) {
            window.location.replace('<?=linkto("sys_login")?>');
        }
    });
}
</script>