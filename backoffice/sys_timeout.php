<?php
include "sys_header.php";
$login_page = substr($_SERVER['REQUEST_URI'], 0, strrpos($_SERVER['PHP_SELF'], "/"));
?>
<body>
<div class="content d-flex justify-content-center align-items-center">
    <div class="flex-fill">
        <div class="text-center mb-3">
            <img class="mb-4" src="assets/images/logo_dark.png" alt="" height="72">
            <h5>Sessione scaduta, effettua nuovamente il Login</h5>
        </div>
        <div class="row">
            <div class="col-xl-2 offset-xl-5 col-md-4 offset-md-4 col-sm-4 offset-sm-4">
                <a href="<?=$login_page?>/" class="btn btn-primary btn-block"><i class="icon-home mr-2"></i> Login</a>
            </div>
        </div>
    </div>
</div>
</body>
</html>