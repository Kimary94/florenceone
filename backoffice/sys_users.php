
<?php include "sys_header.php"; ?>

<body class="navbar-top<?php if ($settings_menu_closed == 1) { echo ' sidebar-xs'; } ?>">
    <?php include "sys_navbar.php"; ?>
    <style>
    .dataTables_info{display:none;}
    </style>
    <div class="page-content">
        <?php include "sys_sidebar.php"; ?>
        <div class="content-wrapper">

        
        <!-- Page header -->
        <div class="page-header page-header-light">
            <div class="breadcrumb-line breadcrumb-line-light header-elements-md-inline">

                <div class="d-flex">
                    <i class="icon-users-2 mr-2 breadcrumb-item"></i><span style="margin-top: 8px;">Operatori</span>
                </div>

                <div class="header-elements d-none" style="display: flex !important;">
                    <div class="breadcrumb justify-content-center">
                        <a href="#" class="breadcrumb-elements-item" onclick="new_record();">
                            <i class="icon-plus-squared"></i>
                            Nuovo operatore
                        </a>
                    </div>
                </div>
            </div>
        </div>


        <div class="content unselectable">


            <div class="card card-collapsed" style="background-color: #f9f9f9; display:none;">
                <div class="card-header header-elements-inline" onmouseup="$('#searchexpand').click();" style="font-variant: all-petite-caps;">
                    <div><i class="icon-search-7"></i> Filtri di ricerca</div>
                    <div data-action="collapse" id="searchexpand" style="display:none;"></div>
                </div>

                <div class="card-body">


                    <div class="row">
                        <div class="col-lg-10 col-md-9 col-sm-12 col-xs-12">
                            <div class="row">

                                <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                                    <label class="text-bold">Nome</label>
                                    <input type="text"   class="form-control" autocomplete="off" id="firstname" value="<?php if (isset($_SESSION['sys_users_filter_firstname'])) { echo $_SESSION['sys_users_filter_firstname']; } ?>">
                                </div></div>

                                <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                                    <label class="text-bold">Lastname</label>
                                    <input type="text"   class="form-control" autocomplete="off" id="lastname" value="<?php if (isset($_SESSION['sys_users_filter_lastname'])) { echo $_SESSION['sys_users_filter_lastname']; } ?>">
                                </div></div>

                                <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                                    <label class="text-bold">Username</label>
                                    <input type="text"   class="form-control" autocomplete="off" id="username" value="<?php if (isset($_SESSION['sys_users_filter_username'])) { echo $_SESSION['sys_users_filter_username']; } ?>">
                                </div></div>

                                <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                                    <label class="text-bold">Email</label>
                                    <input type="text"   class="form-control" autocomplete="off" id="email" value="<?php if (isset($_SESSION['sys_users_filter_email'])) { echo $_SESSION['sys_users_filter_email']; } ?>">
                                </div></div>

                                <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                                    <label class="text-bold">Gruppo</label>
                                    <select class="form-control select" id="id_sys_groups" >
                                    <option value=""></option>
                                    <?php
                                        $selected_id = '';
                                        if (isset($_SESSION['sys_users_filter_id_sys_groups'])) {
                                            if (intval($_SESSION['sys_users_filter_id_sys_groups']) > 0) {
                                                $selected_id = $_SESSION['sys_users_filter_id_sys_groups'];
                                            }
                                        }
                                        $query = $db->query("SELECT id, group_name FROM sys_groups ORDER BY group_name");
                                        $rows = array();
                                        while($risultati = mysqli_fetch_array($query)) {
                                            echo '<option value="'.$risultati['id'].'" ';
                                            if ($selected_id == $risultati['id']) { echo ' SELECTED '; }
                                            echo '>'.$risultati['group_name'].'</option>';
                                        }
                                    ?>
                                    </select>
                                </div></div>

                                <div class="col-lg-3 col-md-6"><div class="form-group container_fields">
                                    <label class="text-bold" style="display: inherit;">Login disabilitato</label><br>
                                    <input type="checkbox"  class="form-control" id="login_disabled" value="1" <?php if (isset($_SESSION['sys_users_filter_login_disabled'])) { if ($_SESSION['sys_users_filter_login_disabled'] == 1) { echo " CHECKED "; } } ?> >
                                </div></div>

                            </div>
                        </div>
                        <div class="col-lg-2 col-md-3 col-sm-12 col-xs-12">

                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="text-center">
                                        <button onclick="event.preventDefault(); applyFilters();"
                                                style="width: 150px; padding: 10px; border-radius: 5px;"
                                                class="btn btn-sm btn-outline-info">
                                                    <i class="icon-search-1" style="float: left; margin-top: 2px;"></i> Cerca
                                        </button><br>
                                        <button onclick="event.preventDefault(); resetFilters();"
                                                style="width: 150px; padding: 10px; border-radius: 5px; margin: 3px 0px 3px 0px;"
                                                class="btn btn-sm btn-outline-warning">
                                                    <i class="icon-cancel-alt" style="float: left; margin-top: 2px;"></i> Reset
                                        </button><br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>



                </div>
            </div>



            <div class="card">
                <table id="myDatatable" class="display table table-striped datatable-responsive" style="width:100%; overflow: hidden;">
                    <thead style="background-color: #F9F9F9;">
                        <tr>
                            <th><b>Id&nbsp;</b></th>
                            <th><b>Nome&nbsp;</b></th>
                            <th><b>Cognome&nbsp;</b></th>
                            <th><b>Username&nbsp;</b></th>
                            <th><b>Email&nbsp;</b></th>
                            <th><b>Gruppo&nbsp;</b></th>
                            <th><b>Login disabilitato&nbsp;</b></th>

                            <th><b></b></th>
                        </tr>
                    </thead>
                    <tfoot style="background-color: #F9F9F9;">
                        <tr>
                            <th><b>Id&nbsp;</b></th>
                            <th><b>Nome&nbsp;</b></th>
                            <th><b>Cognome&nbsp;</b></th>
                            <th><b>Username&nbsp;</b></th>
                            <th><b>Email&nbsp;</b></th>
                            <th><b>Gruppo&nbsp;</b></th>
                            <th><b>Login disabilitato&nbsp;</b></th>
                            <th><b></b></th>
                        </tr>
                    </tfoot>
                </table>
            </div>


            <div id="modal_edit" class="modal fade" style="-webkit-user-select: none; -moz-user-select: none; -ms-user-select: none; user-select: none;" unselectable="on">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header bg-primary">
                            <h6 class="modal-title"><i class="icon-user-lock"></i> <span id="modal_mode"></span></h6>
                        </div>
                        <div class="modal-body">
                            <div class="content">
                                <input type="hidden" id="record_id">

                                <div class="row form-group">
                                    <div class="col-lg-4">
                                        <label class="text-bold" style="margin: 9px 0px 0px 0px; font-variant: small-caps;">Firstname</label>
                                    </div>
                                    <div class="col-lg-8" style="margin-top: 3px;">
                                        <input type="text" id="record_firstname" maxlength="255" autocomplete="off" class="form-control" >
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-lg-4">
                                        <label class="text-bold" style="margin: 9px 0px 0px 0px; font-variant: small-caps;">Lastname</label>
                                    </div>
                                    <div class="col-lg-8" style="margin-top: 3px;">
                                        <input type="text" id="record_lastname" maxlength="255" autocomplete="off" class="form-control" >
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-lg-4">
                                        <label class="text-bold" style="margin: 9px 0px 0px 0px; font-variant: small-caps;">Username</label>
                                    </div>
                                    <div class="col-lg-8" style="margin-top: 3px;">
                                        <input type="text" id="record_username" maxlength="255" autocomplete="off" class="form-control" >
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-lg-4">
                                        <label class="text-bold" style="margin: 9px 0px 0px 0px; font-variant: small-caps;">Password</label>
                                    </div>
                                    <div class="col-lg-8" style="margin-top: 3px;">
                                        <input type="text" id="record_password" maxlength="255" autocomplete="off" class="form-control" style="font-size: 12px; width: 68%; display: inline-block;" disabled >
                                        <button class="btn btn-outline-primary" style="width: 30%; padding: 7px; font-size: 12px; float: right;" onclick="ask_password();">Edit password</button>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-lg-4">
                                        <label class="text-bold" style="margin: 9px 0px 0px 0px; font-variant: small-caps;">Email</label>
                                    </div>
                                    <div class="col-lg-8" style="margin-top: 3px;">
                                        <input type="text" id="record_email" maxlength="255" autocomplete="off" class="form-control" >
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-lg-4">
                                        <label class="text-bold" style="margin: 9px 0px 0px 0px; font-variant: small-caps;">Gruppo</label>
                                    </div>
                                    <div class="col-lg-8" style="margin-top: 3px;">
                                        <select class="form-control select" id="record_id_sys_groups" >
                                        <option value=""></option>
                                        <?php
                                            $query = $db->query("SELECT id, group_name FROM sys_groups ORDER BY group_name");
                                            $rows = array();
                                            while($risultati = mysqli_fetch_array($query)) {
                                                echo '<option value="'.$risultati['id'].'">'.$risultati['group_name'].'</option>';
                                            }
                                        ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="row form-group">
                                    <div class="col-lg-4">
                                        <label class="text-bold" style="margin: 0px 0px 0px 0px; font-variant: small-caps;">Login disabilitato</label>
                                    </div>
                                    <div class="col-lg-8" style="margin-top: 3px;">
                                        <input type="checkbox" id="record_login_disabled" class="form-control" value="1"  style="width: 28px; margin: 0;" >
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div class="modal-footer">
                            <button onclick="save_record();" class="btn btn-lg btn-outline-success"><i class="icon-ok-3"></i> Salva</button>
                            <button onclick="$('#modal_edit').modal('hide');" class="btn btn-lg btn-outline-warning"><i class="icon-undo"></i> Annulla</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    function ask_password() {
        input_box("Imposta password", "Inserisci la password", "Ok", "Cancel", ask_password_next);
    }
    
    function ask_password_next(user_input) {
        if (user_input) {
            $('#record_password').val(md5(user_input));
        }
    }

    function new_record() {
        $('#modal_mode').html('Nuovo operatore');
        $('#record_id').val('');
        $('#record_firstname').val('');
        $('#record_lastname').val('');
        $('#record_username').val('');
        $('#record_email').val('');
        $('#record_password').val('');
        $('#record_id_sys_groups').val(0).trigger('change');
        if (ischecked('#record_login_disabled')) {
            $('#record_login_disabled').click();
        }
        setTimeout(function(){ $("#record_firstname").focus(); }, 300);
        $('#modal_edit').modal({backdrop: 'static'});
    }

    function edit_record(id) {
        $('#modal_mode').html('Modifica operatore');
        $.ajax({
            async: false,
            type: "POST",
            url: "controllers/sys_users_controller.php",
            data: {
                "action": "read_record",
                "id": id
            },
            success: function (data, stato) {
                var data = $.parseJSON(data);

                $('#record_id').val(data['record_id']);
                $('#record_firstname').val(data['record_firstname']);
                $('#record_lastname').val(data['record_lastname']);
                $('#record_username').val(data['record_username']);
                $('#record_email').val(data['record_email']);
                $('#record_password').val(data['record_password']);
                $('#record_reset_token').val(data['record_reset_token']);
                $('#record_id_sys_groups').val(data['record_id_sys_groups']).trigger('change');

            if (data['record_login_disabled'] == 1) {
                if (!ischecked('#record_login_disabled')) {
                    $('#record_login_disabled').click();
                }
            } else {
                if (ischecked('#record_login_disabled')) {
                    $('#record_login_disabled').click();
                }
            }
                setTimeout(function(){ $("#record_firstname").focus(); }, 300);

                $('#modal_edit').modal({backdrop: 'static'});
            }
        });
    }
    
    function save_record() {
        var requiredfields = "";
        $(".modal-body :required").each(function(){
            if (($(this).val() == "") || ($(this).val() === null)) {
                if (requiredfields != "") { requiredfields = requiredfields.concat(", "); }
                var tmp_req_field = $(this).attr("id").replace("record_", "").replace("_", " ");
                if (tmp_req_field.substr(0, 3) == "id ") { tmp_req_field = tmp_req_field.replace("id ", ""); }
                requiredfields = requiredfields.concat(tmp_req_field);
            }
        })
        if (requiredfields != "") { toast("Required fields: ".concat(requiredfields), "error"); return; }

        $.ajax({
            async: false,
            type: "POST",
            url: "controllers/sys_users_controller.php",
            data: {
                "action": "save_record",
                "id": $('#record_id').val(), 
                "firstname": $('#record_firstname').val(), 
                "lastname": $('#record_lastname').val(), 
                "username": $('#record_username').val(), 
                "email": $('#record_email').val(), 
                "password": $('#record_password').val(), 
                "id_sys_groups": $('#record_id_sys_groups').val(), 
                "login_disabled": ischecked('#record_login_disabled'), 

            },
            success: function (data, stato) {
                if (data != "OK") {
                    swal({
                        title: "Error",
                        text: "An error occurred during Save: " + data.toString(),
                        confirmButtonColor: "#66BB6A",
                        type: "warning"
                    });
                } else {
                    $('#modal_edit').modal('hide');
                    update_table();
                }
            }
        });
    }

    function update_table()  {
        table = $('#myDatatable').DataTable();
        table.destroy();
        $("#myDatatable > tbody").html("");
        load_table();
        table = $('#myDatatable').DataTable();
        table.page(0).draw(false);
    }

    $(document).ready(function () {

        $('.select').select2({ width: '100%' });


        load_table();
        table = $('#myDatatable').DataTable();
        table.on('draw', function () {
            table.rows().every(function (rowIdx, tableLoop, rowLoop) {
                var values_in_row = this.data();
                $.ajax({
                    async: false,
                    type: "POST",
                    url: "controllers/sys_users_controller.php",
                    data: {
                        "action": "button_record",
                        "id": values_in_row[0]
                    },
                    success: function (data, stato) {
                        values_in_row[7] = data;
                    }
                });
                this.data(values_in_row);
            });
        });
    });


    function delete_record(id) {
        swal({
            title: "Sei sicuro?",
            text: "I dati non potranno essere recuperati",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#EF5350",
            confirmButtonText: "Si, elimina!",
            cancelButtonText: "No, annulla!",
            closeOnConfirm: false,
            closeOnCancel: false
        },
        function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    async: true,
                    type: "POST",
                    url : "controllers/sys_users_controller.php",
                    data: {
                        "action": "delete_record",
                        "id": id
                    },
                    success: function (data) {
                        if (data === "OK") {
                            swal({
                                title: "Eliminato!",
                                text: "Il record è stato cancellato",
                                confirmButtonColor: "#66BB6A",
                                type: "success"
                            }, function () {
                                document.location.reload();
                            });
                        } else {
                            swal({
                                title: "Operazione annullata",
                                text: "Qualcosa è andato storto",
                                confirmButtonColor: "#2196F3",
                                type: "error"
                            });
                        }
                    },
                    error: function (data) {
                        swal({
                            title: "Operazione annullata",
                            text: "Qualcosa è andato storto",
                            confirmButtonColor: "#2196F3",
                            type: "error"
                        });
                    }
                });
            }
            else {
                swal({
                    title: "Operazione annullata",
                    text: "",
                    confirmButtonColor: "#2196F3",
                    type: "error"
                });
            }
        });
    }



    function load_table() {

        filterfirstname = '';
        if (parseInt($('#firstname').val().length) > 0) { filterfirstname = $('#firstname').val(); }

        filterlastname = '';
        if (parseInt($('#lastname').val().length) > 0) { filterlastname = $('#lastname').val(); }

        filterusername = '';
        if (parseInt($('#username').val().length) > 0) { filterusername = $('#username').val(); }

        filteremail = '';
        if (parseInt($('#email').val().length) > 0) { filteremail = $('#email').val(); }

        filterid_sys_groups = '';
        if ($('#id_sys_groups').val() !== null) { filterid_sys_groups = $('#id_sys_groups').val(); }

        filterlogin_disabled = '';
        if (ischecked('#login_disabled')) { filterlogin_disabled = '1'; }

        $('#myDatatable').DataTable( {
            'iDisplayLength': 25,
            'processing': true,
            'serverSide': true,
            'stateSave': true,
            'searching': false,
            'responsive': true,
            dom: '<"datatable-header"flB><"datatable-scroll-wrap"t><"datatable-footer"ip>',
            buttons: [],
            language: {
                lengthMenu: '<span>Show:</span> _MENU_',
                paginate: {'first': '|&larr;', 'last': '&rarr;|', 'next': '&rarr;', 'previous': '&larr;'}
            },
            ajax: {
                url: 'controllers/sys_users_controller.php',
                type: 'POST',
                data: {
                    action: "load_table",
                    filterfirstname: filterfirstname,
                    filterlastname: filterlastname,
                    filterusername: filterusername,
                    filteremail: filteremail,
                    filterid_sys_groups: filterid_sys_groups,
                    filterlogin_disabled: filterlogin_disabled,

                }
            },
            'columnDefs': [
                { 'targets': 0, 'visible': false },
                { "targets": 1,                           "width": "25%" },
                { "targets": 2,                           "width": "25%" },
                { "targets": 3,                           "width": "25%" },
                { "targets": 4,                           "width": "25%" },
                { "targets": 5,                           "width": "25%" },
                { "targets": 6,                           "width": "25%" },
                { "targets": 7, "className": "dt-center", "orderable": false, "width": "5%" },
            ]
        } );
    }

    function applyFilters() {
        table = $('#myDatatable').DataTable();
        table.destroy();
        $("#myDatatable > tbody").html("");
        load_table();
        table = $('#myDatatable').DataTable();
        table.page(0).draw(false);
    }

    function resetFilters() {
        $('#firstname').val('');
        $('#lastname').val('');
        $('#username').val('');
        $('#email').val('');
        $('#id_sys_groups').val('').trigger('change');
        if (ischecked('#login_disabled')) {
            $('#login_disabled').click();
        }
        applyFilters();
    }

    
</script>