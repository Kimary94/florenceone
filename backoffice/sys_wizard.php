<?php
    include "sys_header.php";
?>

<style>
.unselectable {
    -webkit-touch-callout: none;
    -webkit-user-select: none;
    -khtml-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
</style>

<body>
    <div class="page-content">
        <div class="content-wrapper">
    

            <div class="form-signin">

                <div class="text-center mb-4">
                    <img class="mb-4 unselectable" src="assets/images/logo_dark.png" alt="" height="72">
                </div>

                <div id="step1">
                    <div class="text-center mb-4">
                        <p class="unselectable">Benvenuto.<br>Configure database settings:</a></p>
                    </div>
                    <div class="form-label-group">
                        <input type="text" value="<?=$db_host?>" id="inputHost" class="form-control" style="height:46px;" placeholder="Hostname or IP" autocomplete="off" required autofocus>
                        <label for="inputHost" class="unselectable">Hostname or IP</label>
                    </div>
                    <div class="form-label-group">
                        <input type="text" value="<?=$db_username?>" id="inputUsername" class="form-control" placeholder="Database username" style="height:46px;" autocomplete="off" required>
                        <label for="inputUsername" class="unselectable">Database username</label>
                    </div>
                    <div class="form-label-group">
                        <input type="password" value="<?=$db_password?>" id="inputPassword" class="form-control" placeholder="Database password" style="height:46px;" autocomplete="off" required>
                        <label for="inputPassword" class="unselectable">Database password</label>
                    </div>
                    <button class="btn btn-lg btn-outline-primary btn-block" id="check_db_conn" onclick="check_db_conn();">Check settings <i class="icon-right-open-mini"></i></button>
                </div>

                <div id="step2" style="display:none;" >
                    <div class="text-center mb-4">
                        <p class="unselectable">Benvenuto.<br>Configure database settings:</a></p>
                    </div>
                    <div class="form-label-group">
                        <input type="text" value="<?=$db_database?>" id="inputDatabase" class="form-control" placeholder="Database name" style="height:46px;" autocomplete="off" required>
                        <label for="inputDatabase" class="unselectable">Database name</label>
                    </div>
                    <div>
                        <input type="checkbox" id="createDatabase">
                        <label for="createDatabase" class="unselectable">Create database</label>
                    </div>
                    <br/>
                    <button class="btn btn-lg btn-outline-primary btn-block" id="check_db_sel" onclick="check_db_sel();">Check settings <i class="icon-right-open-mini"></i></button>
                </div>


                <div id="step3" style="display:none;" >
                    <div class="text-center mb-4">
                        <p class="unselectable">Select tables to be created.</a></p>
                    </div>
                    <div>
                        <input type="checkbox" id="tbl_system" checked>
                        <label for="tbl_system" class="unselectable">System tables</label>
                    </div>
                    <div>
                        <input type="checkbox" id="tbl_samples">
                        <label for="tbl_samples" class="unselectable">Sample tables</label>
                    </div>
                    <br/>
                    <button class="btn btn-lg btn-outline-primary btn-block" id="create_tables" onclick="create_tables();">Create tables <i class="icon-right-open-mini"></i></button>
                </div>



                <div id="step4" style="display:none;" >
                    <div class="text-center mb-4" id="field_finished">
                        <p class="unselectable">Almost finished...</a></p>
                    </div>
                    <div>
                        <span id="login_info" class="unselectable"></span>
                        <textarea id="login_info_for_clipboard" style="width: 1px; height: 1px; top: -50; position: absolute;"></textarea>
                    </div>
                    <br/>
                    <button class="btn btn-lg btn-outline-success btn-block" style="width: 49%; display: inline-block; vertical-align: bottom;" id="print" onclick="print_settings();"><i class="icon-printer"></i> Print</button>
                    <button class="btn btn-lg btn-outline-info btn-block" style="width: 49%; display: inline-block; vertical-align: bottom;" id="clipboard" onclick="copy_clipboard();"><i class="icon-clipboard2"></i> Copy</button>
                    <button class="btn btn-lg btn-outline-primary btn-block" id="go_login" onclick="$('#step4').hide(); $('#step5').show();"></i> Last step <i class="icon-right-open-mini"></i></button>
                </div>



                <div id="step5" style="display:none;" >
                    <div class="text-center mb-4" id="field_finished">
                        <p class="unselectable">Finished !</a></p>
                    </div>
                    <div>
                        <span class="unselectable" style="display: inherit;">Check if <i>uploads</i> folder is writable</span>
                        <span id="result_check_uploads_folder" class="unselectable" style="font-weight: bold;"></span>
                        <button class="btn btn-lg btn-outline-success btn-block" id="check_uploads_folder" onclick="check_uploads_folder();"><i class="icon-loop"></i> Check</button>
                    </div>
                    <br>
                    <div>
                        <span class="unselectable" style="display: inherit;">You can now delete <i>sys_wizard.php</i> and <i>controllers/sys_wizard_controller.php</i></span>
                        <span id="result_delete_sys_wizard" class="unselectable" style="font-weight: bold;"></span>
                        <button class="btn btn-lg btn-outline-success btn-block" id="delete_sys_wizard" onclick="delete_sys_wizard();"><i class="icon-trash-8"></i> Delete</button>
                    </div>
                    <br/>
                    <button class="btn btn-lg btn-outline-primary btn-block" id="go_login" onclick="openLogin();"><i class="icon-key-5"></i> Go to login page</button>
                </div>


                

            </div>  <!-- form-signin -->
        </div>      <!-- content-wrapper -->
    </div>          <!-- page-content -->
</body>
</html>

<script>
    function print_settings(){
        $('#field_finished').hide();
        $('#print').hide();
        $('#clipboard').hide();
        $('#go_login').hide();
        window.print();
        setTimeout(function(){ 
            $('#field_finished').show();
            $('#print').show();
            $('#clipboard').show();
            $('#go_login').show();
        }, 200);
    }
    function copy_clipboard(){
        var copyText = document.getElementById("login_info_for_clipboard");
        copyText.select();
        document.execCommand("copy");
        toast("Saved to clipboard", "success");
    }

    function check_uploads_folder() {
        showOverlay();
        $.ajax({
            url: "controllers/sys_wizard_controller",
            type: "POST",
            data: {
                'action': "check_uploads_folder"
            },
            success: function (data, stato) {
                if (data == "Ok, is writeable") {
                    $('#check_uploads_folder').hide();
                }
                $('#result_check_uploads_folder').html(data);
                hideOverlay();
            }
        });
    }

    function delete_sys_wizard() {
        showOverlay();
        $.ajax({
            url: "controllers/sys_wizard_controller",
            type: "POST",
            data: {
                'action': "delete_sys_wizard"
            },
            success: function (data, stato) {
                $('#result_delete_sys_wizard').html(data);
                if (data == "Ok, files deleted") {
                    $('#check_uploads_folder').hide();
                    $('#delete_sys_wizard').hide();
                }
                hideOverlay();
            }
        });
    }

    function check_db_conn() {
        $('#check_db_conn').attr('disabled', true);
        $.ajax({
            url: "controllers/sys_wizard_controller",
            type: "POST",
            data: {
                'action': "check_db_conn",
                'hostname': $('#inputHost').val(),
                'username': $('#inputUsername').val(),
                'password': $('#inputPassword').val()
            },
            success: function (data, stato) {
                $('#check_db_conn').attr('disabled', false);
                var data = $.parseJSON(data);
                if (data == "OK") {
                    $('#step1').hide();
                    $('#step2').show();
                } else {
                    toast(data, "warning");

                }
            }
        });
    }


    function check_db_sel() {
        $('#check_db_sel').attr('disabled', true);
        database = $('#inputDatabase').val().trim().replace(' ', '_')
        $('#inputDatabase').val(database);
        if ($('#createDatabase').prop('checked')) { createDatabase = 1; } else { createDatabase = 0; }
        $.ajax({
            url: "controllers/sys_wizard_controller",
            type: "POST",
            data: {
                'action': "check_db_sel",
                'hostname': $('#inputHost').val(),
                'username': $('#inputUsername').val(),
                'password': $('#inputPassword').val(),
                'database': database,
                'createDatabase': createDatabase
            },
            success: function (data, stato) {
                $('#check_db_sel').attr('disabled', false);
                var data = $.parseJSON(data);
                if (data == "OK") {
                    $('#step2').hide();
                    $('#step3').show();
                } else {
                    toast(data, "warning");
                }
            }
        });
    }


    function create_tables() {
        $('#create_tables').attr('disabled', true);
        if ($('#tbl_system').prop('checked')) { tbl_system = 1; } else { tbl_system = 0; }
        if ($('#tbl_samples').prop('checked')) { tbl_samples = 1; } else { tbl_samples = 0; }
        $.ajax({
            url: "controllers/sys_wizard_controller",
            type: "POST",
            data: {
                'action': "create_tables",
                'tbl_system': tbl_system,
                'tbl_samples': tbl_samples
            },
            success: function (data, stato) {
                console.log(data);
                var data = $.parseJSON(data);
                if (data[0] == "OK") {
                    $('#step3').hide();
                    $('#step4').show();
                    $('#login_info').html(data[1]);
                    $('#login_info_for_clipboard').html(data[2]);
                } else {
                    toast(data[1], "warning");
                }
            }
        });
    }

    function openLogin() {
        window.location.replace('<?=linkto("sys_login")?>');
    }

</script>