<?php
use Melbahja\Seo\Factory;

// Load Composer's autoloader
require 'vendor/autoload.php';

$metatags = Factory::metaTags();
$title = "Florence One | Rivenditore SAP Italia | Condizioni generali di fornitura ";

$metatags->meta('author', 'Biznes')
		->meta('title', $title)
		->meta('description', 'Florence One, in questa pagina sono specificate condizioni di fornitura della nostra azienda e dei nostri prodotti.')
        ->meta('keywords', 'SAP Business One, SAP HANA, ERP, software gestionale, software contabilità')
		->image('/img/logo/florence-one-logo-scuro.png')
		->url('https://florence-one.it/condizioni-generali-di-fornitura')
?>
<!DOCTYPE html>
<html lang="it">

<head>
	<title><?=$title?></title>
	<?=$metatags?>
    <?php include ("header.html"); ?>
</head>

<body>
    <!-- main wrapper -->
    <div id="wrapper">
        <div class="page-wrapper">
            <!-- header of the page -->
            <?php include ("menu.html"); ?>
            <!--/header of the page -->
            <main>
            	<section class="visual">
                    <div class="visual-inner sap-business-one-banner dark-overlay parallax" data-stellar-background-ratio="0.55">
                        <div class="centered">
                            <div class="container">
                                	
                                    <h1 class="visual-title visual-sub-title">Condizioni generali di fornitura</h1>
                                    <div class="breadcrumb-block">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="/"> Home </a></li>
                                            <li class="breadcrumb-item active"> Condizioni generali di fornitura </li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- main content wrapper -->
                <div class="content-wrapper">
                    <section class="content-block">
                        <div class="container">
                            <div class="row mt-5">
                                <div class="col-lg-12 less-wide">
                                    <div class="blog-holder" style="padding-bottom:50px">
                                        <article class="blog-article">
                                                
                                            <div class="blog-desc pt-5">
                                                <p>Le seguenti condizioni generali di fornitura valgono per tutte le forniture di servizi, merce, progetti ecc., di cui all’oggetto sociale, comprese quelle future, pure se nei singoli casi non si fa esplicito riferimento alle stesse. Condizioni di acquisto del committente diverse, non sono impegnative per Florence One Srl se non accettate per iscritto di caso in caso. Al momento dell’ordine o successivamente alla ricezione della merce o al pagamento anche di parte di una commessa, queste condizioni generali di fornitura si intendono accettate dall'acquirente. La validità di singole condizioni non pregiudica la validità delle rimanenti.</p>
                                                <div class="blockquote-block blockquote-block-v2 pt-3 pb-3">
                                                    <blockquote>
                                                        <p>FATTURAZIONE</p>
                                                    </blockquote>
                                                </div>
                                                <p>Le modalità di fatturazione sono dettagliate nell’offerta di Florence One Srl.</p>
                                                <div class="blockquote-block blockquote-block-v2 pt-3 pb-3">
                                                    <blockquote>
                                                        <p>CARATTERISTICHE TECNICHE</p>
                                                    </blockquote>
                                                </div>
                                                </p>Le caratteristiche tecniche di eventuali prodotti forniti da Florence One Srl in abbinamento ai propri servizi sono quelle comunicate dai singoli produttori. Florence One Srl non assume, quindi, responsabilità alcuna per eventuali inesattezze e/o non corrispondenza delle stesse a quanto comunicato. Il servizio offerto vuole essere esclusivamente un aiuto ai propri Clienti, e non intende sostituirsi in alcun modo a quanto i produttori comunicano in via ufficiale al mercato circa caratteristiche tecniche, funzionali e dimensionali dei loro prodotti hardware e software.</p>
                                                <div class="blockquote-block blockquote-block-v2 pt-3 pb-3">
                                                    <blockquote>
                                                        <p>ORDINI</p>
                                                    </blockquote>
                                                </div>
                                                <p>Gli ordini sono accettati solo ed esclusivamente in forma scritta, eventualmente tramite Posta elettronica certificata.</p>
                                                <div class="blockquote-block blockquote-block-v2 pt-3 pb-3">
                                                    <blockquote>
                                                        <p>PREZZI</p>
                                                    </blockquote>
                                                </div>
                                                <p>I prezzi (IVA esclusa) sono quelli che emergono dall’offerta di di Florence One Srl e si riferiscono alla fornitura di quanto descritto nelle specifiche di progetto. Oltre all’assistenza a chiamata che verrà fatturata a consuntivo, eventuali attività aggiuntive, non previste in sede di offerta, saranno oggetto di una nuova quotazione da parte di Florence One Srl.</p>
                                                 <div class="blockquote-block blockquote-block-v2 pt-3 pb-3">
                                                    <blockquote>
                                                        <p>PAGAMENTI</p>
                                                    </blockquote>
                                                </div>
                                                <p>Le forniture dovranno essere pagate mezzo Ri.ba. a 30 gg data fattura fine mese, salvo diverse condizioni che dovranno essere concordate per scritto con Florence One Srl.. Florence One Srl si riserva, a suo insindacabile giudizio, il diritto di non procedere alla conclusione della fornitura, o di non aprirne di nuove, in caso di mancati o ritardati pagamenti. Resta altresı̀ inteso che, in tali casi, saranno dovuti a Florence One Srl i pagamenti delle prestazioni svolte fino a quel momento. Il cliente non è autorizzato a trattenere o ad addebitare pagamenti ad eccezione di casi verificati o accertati legalmente. Il ritardato pagamento darà luogo alla decorrenza degli interessi passivi sulla base del prime rate maggiorato di cinque punti percentuali, dopo otto giorni dalla data della scadenza. Le spese di insoluto, bancarie e cambiarie, sono a carico del cliente. Sono fatti salvi, in ogni caso, gli eventuali maggiori danni. La fornitura è effettuata con la clausola di "riservato dominio" a favore del venditore (art. 1523 e seguenti del C.C.) fino all'effettivo completo pagamento o all'andata a buon fine di effetti eventualmente accettati. Mancando questo, o venendo ritardato, Florence One Srl si riserva di annullare le forniture in corso o da eseguire, nonché di riprendersi la merce o la proprietà di progetti e di far suoi a titolo di penale gli acconti già incassati. Modifiche del diritto di "riservato dominio" possono avvenire solo per accordo scritto.</p>
                                                <div class="blockquote-block blockquote-block-v2 pt-3 pb-3">
                                                    <blockquote>
                                                        <p>TEMPI DI CONSEGNA ED EVENTUALI RITARDI</p>
                                                    </blockquote>
                                                </div>
                                                <p>I tempi di consegna, se previsti, sono quelli che emergono dall’offerta, o dallo studio di fattibilità, o dalle specifiche di progetto e decorrono esclusivamente dalla data di accettazione dell’ordine comunicata al cliente da parte di Florence One Srl. Tuttavia, il cliente è consapevole che, in forniture di questa tipologia, le dipendenze esterne ed interne sono molteplici e non interamente preventivabili e dunque i suddetti tempi di consegna potranno subire delle variazioni, senza che ciò comporti conseguenza alcuna per Florence One Srl. I termini di consegna indicati da Florence One Srl non potranno, pertanto, dar luogo ad annullamenti di ordini se non concordati per iscritto.</p>
                                                <div class="blockquote-block blockquote-block-v2 pt-3 pb-3">
                                                    <blockquote>
                                                        <p>PENALI</p>
                                                    </blockquote>
                                                </div>
                                                <p>Florence One Srl:</br>
													1- Non accetta nessun tipo di penale se non concordata esplicitamente e di volta in volta sulle singole componenti della fornitura</br>
													2- Non si assume mai responsabilità sulle componenti o i prodotti realizzati da terze parti e, in ogni caso, saranno valide le attenuanti derivanti da caso fortuito e/o forza maggiore.</p>
                                                <div class="blockquote-block blockquote-block-v2 pt-3 pb-3">
                                                    <blockquote>
                                                        <p>TRATTAMENTO DEI DATI PERSONALI</p>
                                                    </blockquote>
                                                </div>
                                                <p>Ai sensi del Decreto Legislativo 30 giugno 2003 n.196, il cliente dà atto che i “dati personali” comunicati e/o scambiati, anche in fase di informative precontrattuali, formeranno oggetto di trattamento ai sensi, per gli effetti e con le finalità di cui all’art. 24, comma 1, lettere b), c), d) del D.Lgs. n.196/2003 e successive modificazioni ed integrazioni. Resta, inoltre, inteso che il cliente espressamente acconsente al trasferimento dei “dati personali” ai sensi e per gli effetti dell’art. 42 e dell’art.43 comma 1 lettera a) del D.Lgs. n.196/2003 e, comunque, alla loro comunicazione e diffusione ai sensi e per gli effetti dell’art. 25, comma 1 del citato decreto.</p>
                                            	<div class="blockquote-block blockquote-block-v2 pt-3 pb-3">
                                                    <blockquote>
                                                        <p>CONTROVERSIE</p>
                                                    </blockquote>
                                                </div>
                                                <p>Ogni controversia relativa al presente contratto sarà di esclusiva competenza del Foro di Firenze. Per tutto quanto non espressamente previsto nel presente contratto, si rinvia alle disposizioni di cui al Codice Civile, nonché alle altre norme giuridiche inderogabili aventi efficacia di legge.</p>
                                            	<div class="blockquote-block blockquote-block-v2 pt-3 pb-3">
                                                    <blockquote>
                                                        <p>VARIAZIONE ALLE CONDIZIONI DI FORNITURA</p>
                                                    </blockquote>
                                                </div>
                                                <p>Le condizioni contenute nel presente documento potranno essere modificate, senza preavviso alcuno e avranno validità dalla data di pubblicazione.</p>
                                            </div>
                                        </article>
                                    </div>
                                    
                                </div>
                            </div>
                    </section>
                    </div>
                    <!--/main content wrapper -->
            </main>
            </div>
            <!-- footer of the pagse -->
        <?php include ("footer.html"); ?>
        <!-- jquery library -->
        <script src="vendors/jquery/jquery-2.1.4.min.js"></script>
        <!-- external scripts -->
        <script src="vendors/tether/dist/js/tether.min.js"></script>
        <script src="vendors/bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/stellar/jquery.stellar.min.js"></script>
        <script src="vendors/isotope/javascripts/isotope.pkgd.min.js"></script>
        <script src="vendors/isotope/javascripts/packery-mode.pkgd.js"></script>
        <script src="vendors/owl-carousel/dist/owl.carousel.js"></script>
        <script src="vendors/waypoint/waypoints.min.js"></script>
        <script src="vendors/counter-up/jquery.counterup.min.js"></script>
        <script src="vendors/fancyBox/source/jquery.fancybox.pack.js"></script>
        <script src="vendors/fancyBox/source/helpers/jquery.fancybox-thumbs.js"></script>
        <script src="vendors/image-stretcher-master/image-stretcher.js"></script>
        <script src="vendors/wow/wow.min.js"></script>
        <script src="vendors/rateyo/jquery.rateyo.js"></script>
        <script src="vendors/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script src="vendors/bootstrap-slider-master/src/js/bootstrap-slider.js"></script>
        <script src="vendors/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
        <script src="js/mega-menu.js"></script>
        <!-- custom jquery script -->
        <script src="js/jquery.main.js"></script>
        <!-- REVOLUTION JS FILES -->
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/jquery.themepunch.tools.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/jquery.themepunch.revolution.min.js"></script>
        <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.actions.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.migration.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.video.min.js"></script>
        <!-- SNOW ADD ON -->
        <script type="text/javascript" src="vendors/rev-slider/revolution-addons/snow/revolution.addon.snow.min.js"></script>
        <!-- revolutions slider script -->
        <script src="js/revolution.js"></script>

</html>
