<?php
use Melbahja\Seo\Factory;

// Load Composer's autoloader
require 'vendor/autoload.php';

$metatags = Factory::metaTags();
$title = "Florence One | Rivenditore SAP Italia | Contatti ";

$metatags->meta('author', 'Biznes')
		->meta('title', $title)
		->meta('description', 'Florence One, rimani in contatto con noi. Scrivici per una demo dei nostri prodotti o per informazioni e richieste.')
        ->meta('keywords', 'SAP Business One, SAP HANA, ERP, software gestionale, software contabilità, Florence One')
		->image('img/florence-one-custom-software.jpg')
		->url('https://florence-one.it/contatti')
?>
<!DOCTYPE html>
<html lang="it">

<head>
	<title><?=$title?></title>
	<?=$metatags?>
    <?php include ("header.html"); ?>
</head>

<body>
    <!-- main wrapper -->
    <div id="wrapper">
        <div class="page-wrapper">
            <!-- header of the page -->
            <?php include ("menu.html"); ?>
            <main>
                <!-- visual/banner of the page -->
                <section class="visual">
                    <div class="visual-inner sap-business-one-banner dark-overlay parallax" data-stellar-background-ratio="0.55">
                        <div class="centered">
                            <div class="container">
                                	
                                    <h1 class="visual-title visual-sub-title">Contatti</h1>
                                    <div class="breadcrumb-block">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="/"> Home </a></li>
                                            <li class="breadcrumb-item active"> Contatti </li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/visual/banner of the page -->
                <!-- main content wrapper -->
                <div class="content-wrapper">
                    <section class="content-block pb-0">
                        <div class="container">
                            <div class="contact-container">
                                <h6 class="content-title contact-title">Rimani in contatto con noi!</h6>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <!-- contact form -->
                                        <form action="email_interna.php" method="post" class="waituk_contact-form">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <input type="text" placeholder="NOME *" id="con_fname" name="nome" class="form-control" required>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <input type="text" placeholder="AZIENDA *" id="con_lname" name="azienda" class="form-control" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <input type="tel" placeholder="TELEFONO *" id="con_phone" name="telefono" class="form-control" required>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <input type="email" placeholder="EMAIL *" id="con_email" name="email" class="form-control" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <textarea class="form-control" placeholder="MESSAGGIO *" id="con_message" name="messaggio" required></textarea>
                                            </div>
                                            
                                            <input type="hidden" id="pagina" name="pagina" value="<?=$_SERVER['REQUEST_URI']?>">
                                            
                                            <div class="btn-container">
                                                <button type="submit" class="btn btn-primary btn-arrow">SCRIVICI</button>
                                                <p id="form" class="successo col-11 alert alert-success" style="display:none">Invio riuscito</p>
                                                <p id="form" class="errore col-11 alert alert-success" style="display:none; color:red">Tutti i campi sono obbligatori.</p>
                                            </div>
                                        </form>
                                    </div>
                                    <div class="col-lg-6 col-xl-5 offset-xl-1">
                                        <div class="info-slot">
                                            <div class="icon"><span class="custom-icon-map-marker"></span></div>
                                            <div class="text">
                                                <address>via Umberto Saba, 42
                                                    <br>51100 Pistoia (PT)
                                                    <br>Italia</address>
                                            </div>
                                        </div>
                                        <div class="info-slot">
                                            <div class="icon"><span class="custom-icon-headset"></span></div>
                                            <div class="text">
                                                <ul class="content-list contact-list">
                                                    <li><span class="label-text">TELEFONO</span> <a href="tel:003905731935507">+39 0573 1935507</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="info-slot">
                                            <div class="icon"><span class="custom-icon-message"></span></div>
                                            <div class="text">
                                                <ul class="content-list contact-list">
                                                    <li><a href="mailto:sales@florence-one.it">sales@florence-one.it</a></li>
                                                    <li><a href="mailto:info@florence-one.it">info@florence-one.it</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <div class="map-holder embed-responsive-21by9 grayscaled-map">
                    	<iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2873.3703496132066!2d10.934436316304932!3d43.93100847911295!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x132a8b64089f44f3%3A0x2ead8c8b261d7b74!2sFlorence%20One!5e0!3m2!1sit!2sit!4v1622188875042!5m2!1sit!2sit" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy" height="450"></iframe>
                    </div>
                    <?php include ("sezione_clienti.html"); ?>
                </div>
                <!--/main content wrapper -->
            </main>
        </div>
        <!-- footer of the pagse -->
        <?php include ("footer.html"); ?>
    <!-- jquery library -->
    <script src="vendors/jquery/jquery-2.1.4.min.js"></script>
    <!-- external scripts -->
    <script src="vendors/tether/dist/js/tether.min.js"></script>
    <script src="vendors/bootstrap/js/bootstrap.min.js"></script>
    <script src="vendors/stellar/jquery.stellar.min.js"></script>
    <script src="vendors/isotope/javascripts/isotope.pkgd.min.js"></script>
    <script src="vendors/isotope/javascripts/packery-mode.pkgd.js"></script>
    <script src="vendors/owl-carousel/dist/owl.carousel.js"></script>
    <script src="vendors/waypoint/waypoints.min.js"></script>
    <script src="vendors/counter-up/jquery.counterup.min.js"></script>
    <script src="vendors/fancyBox/source/jquery.fancybox.pack.js"></script>
    <script src="vendors/fancyBox/source/helpers/jquery.fancybox-thumbs.js"></script>
    <script src="vendors/image-stretcher-master/image-stretcher.js"></script>
    <script src="vendors/wow/wow.min.js"></script>
    <script src="vendors/rateyo/jquery.rateyo.js"></script>
    <script src="vendors/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="vendors/bootstrap-slider-master/src/js/bootstrap-slider.js"></script>
    <script src="vendors/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
    <script src="js/mega-menu.js"></script>
    <!-- custom jquery script -->
    <script src="js/jquery.main.js"></script>
    <script src="js/jquery.validate.js"></script>
    <script src="js/mailchimp.js"></script>
    <script src="js/contact-form.js"></script>
    <!-- REVOLUTION JS FILES -->
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/jquery.themepunch.revolution.min.js"></script>
    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.migration.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.video.min.js"></script>
    <!-- SNOW ADD ON -->
    <script type="text/javascript" src="vendors/rev-slider/revolution-addons/snow/revolution.addon.snow.min.js"></script>
        <!-- revolutions slider script -->
    <script src="js/revolution.js"></script>
</body>

</html>
