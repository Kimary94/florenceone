<?php
ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\OAuth;
use League\OAuth2\Client\Provider\Google;
date_default_timezone_set('Etc/UTC');
require 'vendor/autoload.php';

$email_html = $_POST['nome'].' dell\'azienda '.$_POST['azienda'].'<br> Ha scritto:<br><br>'.$_POST['messaggio'].'<br><br> Può essere ricontattato a<br>Email: '.$_POST['email'].'<br>Tel: '.$_POST['telefono'];

$mail = new PHPMailer(true);

$mail->isSMTP();
$mail->SMTPDebug = 0;
$mail->Host = 'smtp.gmail.com';
$mail->Port = 587;
$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
$mail->SMTPAuth = true;
$mail->AuthType = 'XOAUTH2';
$email = 'dliguori@florence-one.it';
$clientId = '299375598841-jn35gm5c8cd4hpm1v1nsr69onvm72h1r.apps.googleusercontent.com';
$clientSecret = 'LoXUPU76RoqG3BJxRCjKXAQB';
$refreshToken = '1//09ZvB8j8cd5nOCgYIARAAGAkSNwF-L9IrKggvvlDam4tfL41sCwtcU8PLkPy73jb7CZrI_KfR_EYjMJ0s1sF5VsuS7MDUX6uuk8A';
$provider = new Google(
    [
        'clientId' => $clientId,
        'clientSecret' => $clientSecret,
    ]
);
$mail->setOAuth(
    new OAuth(
        [
            'provider' => $provider,
            'clientId' => $clientId,
            'clientSecret' => $clientSecret,
            'refreshToken' => $refreshToken,
            'userName' => $email,
        ]
    )
);
$mail->isHTML(true);
$mail->setFrom('dliguori@florence-one.it', 'Florence One Marketing Assistant ');
$mail->addAddress( 'dliguori@florence-one.it' );
$mail->addAddress( 'sales@florence-one.it' );
$mail->Subject = "Richiesta di informazioni";
$mail->CharSet = PHPMailer::CHARSET_UTF8;
$mail->Body    = $email_html;
$mail->send();

$oggi = date("Y-m-d");
include ("db.php");

$account = $db->query('INSERT INTO contatti (nome,azienda,email,telefono,pagina,data) VALUES (?,?,?,?,?,?)', $_POST['nome'],$_POST['azienda'],$_POST['email'],$_POST['telefono'],$_POST['pagina'],$oggi);

header("location: thanks.php");
?>
