										<form action="email_gmail.php" method="post" class="waituk_contact-form" id="demo-form">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <input type="text" placeholder="NOME *" id="con_fname" name="nome" class="form-control" required>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <input type="text" placeholder="AZIENDA *" id="con_lname" name="azienda" class="form-control" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <input type="tel" placeholder="TELEFONO *" id="con_phone" name="telefono" class="form-control" required>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <input type="email" placeholder="EMAIL *" id="con_email" name="email" class="form-control" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                          <div class="g-recaptcha" data-sitekey="6LfyxhobAAAAAG-DCWumniYIsrpoceHOIQY2JIJN"></div>
                                                </div>
                                            </div>
                                            
                                            <input type="hidden" id="pagina" name="pagina" value="<?=$_SERVER['REQUEST_URI']?>">
                                            
                                            <div class="btn-container">
                                                <button type="submit" class="btn btn-primary btn-arrow" style="margin:15px 0">RICHIEDI UNA DEMO</button>
                                                <?php 
                                                if($_GET['error'] == 1){
                                                    echo '<p id="form" class="errore col-11 alert alert-danger" style="color:red">Tutti i campi sono obbligatori.</p>';
                                                }
                                                ?>
                                            </div>
                                        </form>