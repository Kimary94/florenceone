<?php
function slugify($text)
{
    $text = preg_replace('/[^\\pL\d]+/u', '-', $text); 	
	$text = str_replace("-39-", "-", $text);
    $text = trim($text, '-');
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
    $text = strtolower($text);
    $text = preg_replace('/[^-\w]+/', '', $text);
    return $text;
}
function limit_text($text, $limit) 
{ 
    if (str_word_count($text, 0) > $limit) { 
        $words = str_word_count($text, 2); 
        $pos = array_keys($words); 
        $text = substr($text, 0, $pos[$limit]) . '...'; 
    } 
    return $text; 
}
?>
