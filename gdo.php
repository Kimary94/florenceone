<?php
use Melbahja\Seo\Factory;

// Load Composer's autoloader
require 'vendor/autoload.php';

$metatags = Factory::metaTags();
$title = "Florence One | Rivenditore SAP Italia | GDO ";

$metatags->meta('author', 'Biznes')
		->meta('title', $title)
		->meta('description', 'Scopri il caso di successo. Nel 2016 tre grandi players del retail italiano si fondono dando così vita a un colosso della GDO Il gruppo è cresciuto rapidamente e oggi opera in Europa, Nord America, Medio ed Estremo Oriente.')
        ->meta('keywords', 'SAP Business One, SAP HANA, ERP, software gestionale, software contabilità, Florence One')
		->image('img/clienti/FLORENCE-ONE-gdo.jpg')
		->url('https://florence-one.it/gdo')
?>
<!DOCTYPE html>
<html lang="it">

<head>
	<title><?=$title?></title>
	<?=$metatags?>
    <?php include ("header.html"); ?>
</head>

<body>
    <!-- main wrapper -->
    <div id="wrapper">
        <div class="page-wrapper">
            <!-- header of the page -->
            <?php include ("menu.html"); ?>
            <!--/header of the page -->
            <main>
            	<section class="visual">
                    <div class="visual-inner sap-business-one-banner dark-overlay parallax" data-stellar-background-ratio="0.55">
                        <div class="centered">
                            <div class="container">
                                	
                                    <h1 class="visual-title visual-sub-title">GDO</h1>
                                    <div class="breadcrumb-block">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="/"> Home </a></li>
                                            <li class="breadcrumb-item"><a href="grande-distribuzione-organizzata"> Grande distribuzione organizzata </a></li>
                                            <li class="breadcrumb-item active"> GDO </li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- main content wrapper -->
                <div class="content-wrapper">
                    <section class="content-block">
                        <div class="container">
                            <div class="row mt-5">
                                <div class="col-lg-12 less-wide">
                                    <div class="blog-holder" style="padding-bottom:50px">
                                        <article class="blog-article">
                                                
                                            <div class="blog-desc pt-5">
                                                <div class="blog-img">
                                                    <div class="image-wrap">
                                                        <figure class="">
                                                            <img src="img/clienti/FLORENCE-ONE-gdo.jpg" alt="images description">
                                                        </figure>
                                                    </div>
                                                </div>
                                                <div class="blog-share mt-5">
                                                    <ul class="social-network with-text">
                                                        <li><strong>Info</strong></li>
                                                        <li>Cliente: GDO</li>
                                                    </ul>
                                                </div>
                                                <div class="blockquote-block blockquote-block-v2 pt-3 pb-3">
                                                    <blockquote>
                                                        <p>La storia</p>
                                                    </blockquote>
                                                </div>
                                                <p>Nel 2016 tre grandi players del retail italiano si fondono dando così vita a un colosso della GDO Il gruppo è cresciuto rapidamente e oggi opera in Europa, Nord America, Medio ed Estremo Oriente, in oltre 18 paesi, con sede in Italia, una filiale negli Stati Uniti e una rete di agenti che operano in vari paesi del Medio/Estremo Oriente e Australia La competitività dei prezzi e la grande attenzione alla qualità sono il fulcro della loro strategia e del loro successo Sceglierli come partner significa avere accesso alla più grande selezione di autentici prodotti italiani sul mercato Possiedono inoltre una rete di oltre 500 produttori qualificati secondo le più severe norme e regolamenti nella preparazione dei prodotti</p>
                                                <div class="blockquote-block blockquote-block-v2 pt-3 pb-3">
                                                    <blockquote>
                                                        <p>La sfida</p>
                                                    </blockquote>
                                                </div>
                                                <p>Queste tre grandi realtà, al momento della fusione, avevano sistemi informativi diversi che necessitavano quindi di un'armonizzazione in modo da poter fornire ad utenti e consumatori interfacce e strumenti uniformi Oltre a questo il gruppo richiedeva anche l'integrazione in un unico software dei sistemi legacy in uso, con una particolare attenzione ai processi di fatturazione attiva e controllo fatturazione passiva Essendo un'importante azienda italiana per la Grande Distribuzione Organizzata, chiedeva anche un sistema centralizzato per la gestione degli ordini per utenti interni, per i clienti e i fornitori di tutta la rete distributiva italiana</p>
                                                <div class="blockquote-block blockquote-block-v2 pt-3 pb-3">
                                                    <blockquote>
                                                        <p>La soluzione</p>
                                                    </blockquote>
                                                </div>
                                                <p>Oltre a contabilità e finanza, sono state implementate le funzionalità del modulo di logistica e magazzino, estese a nuove e più performanti procedure, realizzate su misura in base alle specifiche cliente Abbiamo ampliato l'anagrafica articolo con nuovi tab tipici di settore e inerenti sia ad articoli no food che ad articoli  L'applicativo gestisce inoltre il prodotto fresco e il prodotto surgelato dal preventivo all'incasso Grazie al nostro intervento è ora possibile analizzare la performance dei singoli reparti lungo tutta la Supply Chain attraverso dei KPI e delle DASHBORD multilivello (desktop e mobile) che consentono all'utente di analizzare in maniera più o meno aggregata i dati</p>
                                                <div class="blog-share mt-5">
                                                    <ul class="social-network with-text">
                                                        
                                                    </ul>
                                                </div>
                                                
                                            </div>
                                        </article>
                                    </div>
                                    <div class="contact-container">
                                        <form action="#" class="comment-form waituk_contact-form">
                                            <fieldset>
                                            	<div class="btn-container">
                                    				<a href="contatti" class="btn btn-primary">Unisciti ai nostri clienti</a>
                               					</div>
                               					<!--
                                                <h6 class="content-title contact-title"><a href="contatti">Entra a far parte del mondo Florence One</a></h6>
                                                
                                                <div class="row">
                                                    <div class="col-sm-6 form-group">
                                                        <input placeholder="Nome" type="text" class="form-control">
                                                    </div>
                                                    <div class="col-sm-6 form-group">
                                                        <input placeholder="Email" type="email" class="form-control">
                                                    </div>
                                                    <div class="col-sm-12 form-group">
                                                        <input placeholder="Azienda" type="text" class="form-control">
                                                    </div>
                                                    <div class="col-sm-12 form-group">
                                                        <textarea placeholder="Messaggio" class="form-control"></textarea>
                                                    </div>
                                                    <div class="col-sm-12 btn-holder">
                                                        <button type="submit" class="btn btn-black btn-full">Invia richiesta</button>
                                                    </div>
                                            
                                        </form>
                                        </div>
                                        -->
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                    </section>
                    </div>
                    <!--/main content wrapper -->
            </main>
            </div>
            <!-- footer of the pagse -->
        <?php include ("footer.html"); ?>
        <!-- jquery library -->
        <script src="vendors/jquery/jquery-2.1.4.min.js"></script>
        <!-- external scripts -->
        <script src="vendors/tether/dist/js/tether.min.js"></script>
        <script src="vendors/bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/stellar/jquery.stellar.min.js"></script>
        <script src="vendors/isotope/javascripts/isotope.pkgd.min.js"></script>
        <script src="vendors/isotope/javascripts/packery-mode.pkgd.js"></script>
        <script src="vendors/owl-carousel/dist/owl.carousel.js"></script>
        <script src="vendors/waypoint/waypoints.min.js"></script>
        <script src="vendors/counter-up/jquery.counterup.min.js"></script>
        <script src="vendors/fancyBox/source/jquery.fancybox.pack.js"></script>
        <script src="vendors/fancyBox/source/helpers/jquery.fancybox-thumbs.js"></script>
        <script src="vendors/image-stretcher-master/image-stretcher.js"></script>
        <script src="vendors/wow/wow.min.js"></script>
        <script src="vendors/rateyo/jquery.rateyo.js"></script>
        <script src="vendors/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script src="vendors/bootstrap-slider-master/src/js/bootstrap-slider.js"></script>
        <script src="vendors/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
        <script src="js/mega-menu.js"></script>
        <!-- custom jquery script -->
        <script src="js/jquery.main.js"></script>
        <!-- REVOLUTION JS FILES -->
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/jquery.themepunch.tools.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/jquery.themepunch.revolution.min.js"></script>
        <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.actions.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.migration.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.video.min.js"></script>
        <!-- SNOW ADD ON -->
        <script type="text/javascript" src="vendors/rev-slider/revolution-addons/snow/revolution.addon.snow.min.js"></script>
        <!-- revolutions slider script -->
        <script src="js/revolution.js"></script>

</html>
