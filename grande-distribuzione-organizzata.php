<?php
use Melbahja\Seo\Factory;

// Load Composer's autoloader
require 'vendor/autoload.php';

$metatags = Factory::metaTags();
$title = "Florence One | Casi di successo | Grade distribuzione organizzata ";

$metatags->meta('author', 'Biznes')
		->meta('title', $title)
		->meta('description', 'Florence One, tra i nostri clienti la grande distribuzione organizzata. Florence One porta la digitalizzazione nella grande distribuzione organizzata.')
        ->meta('keywords', 'gdo, SAP Business One, soluzioni digitali, software per grande distribuzione organizzata, digitalizzazione azienda')
		->image('img/settori/FLORENCE-ONE_grande-distribuzione-organizzata-1920.jpg')
		->url('https://florence-one.it/grande-distribuzione-organizzata')
?>
<!DOCTYPE html>
<html lang="it">

<head>
	<title><?=$title?></title>
	<?=$metatags?>
    <?php include ("header.html"); ?>
</head>

<body class="white-overlay">
    <div class="preloader" id="pageLoad">
        <div class="holder">
            <!--<div class="coffee_cup"></div>-->
            <div></div>
        </div>
    </div>
    
    <!-- main wrapper -->
    <div id="wrapper" class="no-overflow-x">
        <div class="page-wrapper">
            <?php include ("menu.html"); ?>
            <main>
                <!-- visual/banner of the page -->
                <div class="banner banner-home">
                	
                    <div id="rev_slider_279_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="restaurant-header" style="margin:0px auto;background-color:#fff;padding:0px;margin-top:0px;margin-bottom:0px;">
                    	
                        <div id="rev_slider_70_1" class="rev_slider fullscreenabanner" style="display:none;" data-version="5.1.4">
                            <ul>
                                <li class="slider-color-schema-dark" data-index="rs-2" data-transition="fade" data-slotamount="7" data-easein="default" data-easeout="default" data-masterspeed="1000" data-rotate="0" data-saveperformance="off" data-title="Slide" data-description="">
                                    <img src="img/settori/FLORENCE-ONE_grande-distribuzione-organizzata-1920.jpg" alt="image description" data-bgposition="center center" data-kenburns="on" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg" data-bgfit="cover" data-no-retina>
                                    <div class="tp-caption tp-shape tp-shapewrapper" id="slide-1699-layer-10" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-width="full"
                                    data-height="full" data-whitespace="nowrap" data-type="shape" data-basealign="slide" data-responsive_offset="on" data-responsive="off" data-frames='[{"from":"y:0;sX:1;sY:1;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]"
                                    data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="background-color:rgba(0, 0, 0, 0.57);"> </div>
                                    <div class="slider-sub-title text-white tp-caption tp-resizeme rs-parallaxlevel-0" data-x="['left','left','left','left']" data-hoffset="['0','0','0','0']" data-y="['top','top','middle','middle']" data-voffset="['145','100','10','20']" data-width="['1200','960','720','540']" data-textalign="left" data-fontsize="['30','28','24','20']" data-lineheight="['72','62','50','50']" data-letterspacing="5" data-height="none" data-whitespace="normal" data-transform_idle="o:1;" data-transform_in="z:0;rX:0deg;rY:0;rZ:0;sX:1.5;sY:1.5;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:0px;" data-mask_out="x:inherit;y:inherit;" data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on" data-paddingright="[25,25,25,25]" data-paddingleft="[25,25,25,25]">
                                        TRA I NOSTRI CLIENTI
                                    </div>
                                    <div class="slider-main-title text-white tp-caption tp-resizeme rs-parallaxlevel-1" data-x="['left','left','left','left']" data-hoffset="['0','0','0','0']" data-y="['top','top','middle','middle']" data-voffset="['250','150','50','50']" data-width="['1200','960','720','540']" data-textalign="left" data-fontsize="['80','50','35','30']" data-fontweight="900" data-letterspacing="['25','10','5','0']" data-lineheight="['184','100','72','60']" data-height="none" data-whitespace="normal" data-transform_idle="o:1;" data-transform_in="z:0;rX:0deg;rY:0;rZ:0;sX:1.5;sY:1.5;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:0px;" data-mask_out="x:inherit;y:inherit;" data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on" data-paddingright="[25,25,25,25]" data-paddingleft="[25,25,25,25]">
                                        GDO
                                    </div>
                                    
                                    <div class="slider-text text-white tp-caption tp-resizeme rs-parallaxlevel-2" data-x="['left','left','left','left']" data-hoffset="['0','0','0','0']" data-y="['top','top','middle','middle']" data-voffset="['450','230','110','110']" data-width="['600','555','555','480']" data-textalign="left" data-fontsize="['16','14','14','14']" data-lineheight="['30','30','22','22']" data-fontweight="400" data-height="none" data-whitespace="normal" data-transform_idle="o:1;" data-transform_in="z:0;rX:0deg;rY:0;rZ:0;sX:1.5;sY:1.5;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:0px;" data-mask_out="x:inherit;y:inherit;" data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on" data-paddingright="[25,25,25,25]" data-paddingleft="[25,25,25,25]">
                                        L’ evoluzione digitale della grande distribuzione organizzata viene in soccorso ad un settore dove la concorrenza è spietata e gli errori si pagano a caro prezzo.
                                    </div>
                                    <div class="tp-caption rev-btn  rs-parallaxlevel-10" id="slide-163-layer-1" data-x="['left','left','left','left']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['320','60','240','220']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power3.easeOut;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[175%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_out="x:inherit;y:inherit;" data-start="1250" data-splitin="none" data-splitout="none" data-actions='[{"event":"click","action":"jumptoslide","slide":"rs-164","delay":""}]' data-responsive_offset="on" data-paddingtop="[0,0,0,0]" data-paddingright="[25,25,25,25]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[25,25,25,25]">
                                       
                                    </div>
                                </li>
                            </ul>
                        </div>
                        
                    </div>
                </div>
                <!--/visual/banner of the page -->
                <!-- main content wrapper -->
                <section class="content-block portfolio-block" id="container" style="padding-top: 0rem">
                        <div class="row grid">
                            <div class="gallery-item col-lg-4 col-md-6 ui photography">
                                <figure class="picture-item img-block shine-effect image-zoom port-v2">
                                    <img src="img/clienti/FLORENCE-ONE-gdo.jpg" alt="images description">
                                    <figcaption>
                                        <div class="link-box" style="background-color:#222222; padding-top:500px; padding-bottom:500px">
                                            <h3 style="color:white;">Vedi il</br>SUCCESS CASE</h3>
                                            <a href="gdo">
                                                <span class="icon-link"><span class="sr-only">&amp;</span></span>
                                            </a>
                                        </div>
                                    </figcaption>
                                </figure>
                            </div>
                            
                            <div class="gallery-item col-lg-4 col-md-6 ui photography">
                                <figure class="picture-item img-block shine-effect image-zoom port-v2">
                                    <img src="img/settori/FLORENCE-ONE_logo-settori-800.jpg" alt="images description">
                                    <!--
                                    <figcaption>
                                        <div class="link-box" style="background-color:#222222; padding-top:500px; padding-bottom:500px">
                                            <h3 style="color:white">Scopri</br>tutti i prodotti</h3>
                                            <a href="#">
                                                <span class="icon-link"><span class="sr-only">&amp;</span></span>
                                            </a>
                                        </div>
                                    </figcaption>
                                    -->
                                </figure>
                            </div>
                        </div>
                        
                    </section>
                    <section class="content-block pt-0">
                        <div class="container">
                            <div class="block-heading bottom-space">
                                <h3 class="block-top-heading">PRODOTTI</h3>
                                <h2 class="block-main-heading">GDO</h2>
                                <span class="block-sub-heading">Pieno controllo sull’intera catena produttiva e distributiva.</span>
                                <div class="divider"><img src="img/divider.png" alt="images description"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="bottom-space-small-only">
                                        <p>Un settore come quello della GDO è soggetto a cambiamenti in modo più rapito rispetto a tanti altri e richiede perciò una grande capacità di adattamento e trasformazione in modo da rimanere competitivi e migliorare i livelli di performance.</p>
                                        <p>L’adozione di procedure più performanti garantisce il controllo sull’intera catena produttiva e distributiva e la condivisione di dati grazie a sistemi informativi integrati permette di avere una gestione fluida e trasparente tra varie sedi, agenti e produttori. </p>
                                        <div class="btn-container top-m-space">
                                            <a href="servizi" class="btn btn-black has-radius-small">SCOPRI I SERVIZI</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="bottom-s-space">
                                        <p>Centralizzare le fasi della Supplay Chain significa per la GDO aumentare sicurezza e attenzione alla qualità del prodotto. </p>
                                    </div>
                                    <div class="row">
                                        <?php include ("link-settori.html"); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="content-block p-0">
                        <div class="container-fluid">
                            <div class="content-slot alternate-block">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="bg-stretch img-wrap wow slideInLeft">
                                            <img src="img/florence-one-nostri-clienti.jpg" alt="images">
                                        </div>
                                    </div>
                                    <div class="col col-lg-6">
                                        <div class="text-wrap">
                                            <h3>Vuoi unirti ai clienti<br>di Florence One?</h3>
                                            <p>Inviaci un tuo contatto. Ti chiameremo e cercheremo di capire le necessità della tua azienda</p>
                                            <p></p>
                                        <!-- contact form -->
                                        <form action="email_gmail.php" method="post" class="waituk_contact-form">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <input type="text" placeholder="NOME *" id="con_fname" name="nome" class="form-control" required>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <input type="text" placeholder="AZIENDA *" id="con_lname" name="azienda" class="form-control" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <input type="tel" placeholder="TELEFONO *" id="con_phone" name="telefono" class="form-control" required>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <input type="email" placeholder="EMAIL *" id="con_email" name="email" class="form-control" required>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <input type="hidden" id="pagina" name="pagina" value="<?=$_SERVER['REQUEST_URI']?>">
                                            
                                            <div class="btn-container">
                                                <button type="submit" class="btn btn-primary btn-arrow">RICHIEDI UNA DEMO</button>
                                                <p id="form" class="successo col-11 alert alert-success" style="display:none">Invio riuscito</p>
                                                <p id="form" class="errore col-11 alert alert-success" style="display:none; color:red">Tutti i campi sono obbligatori.</p>
                                            </div>
                                        </form>
                                    
                                            
                                            
                                            
                                            
                                            
                                            
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </section>
                <div class="content-wrapper">
                <?php include ("sezione-settori.html"); ?>
                    
                    
                    
                    
                    
                    
                </div>
                <!--/main content wrapper -->
            </main>
        </div>
        <?php include ("footer.html"); ?>
        
        
        
    <!-- jQuery Library -->
    <script src="vendors/jquery/jquery-2.1.4.min.js"></script>
    <!-- Vendor Scripts -->
    <script src="vendors/tether/dist/js/tether.min.js"></script>
    <script src="vendors/bootstrap/js/bootstrap.min.js"></script>
    <script src="vendors/stellar/jquery.stellar.min.js"></script>
    <script src="vendors/isotope/javascripts/isotope.pkgd.min.js"></script>
    <script src="vendors/isotope/javascripts/packery-mode.pkgd.js"></script>
    <script src="vendors/owl-carousel/dist/owl.carousel.min.js"></script>
    <script src="vendors/waypoint/waypoints.min.js"></script>
    <script src="vendors/counter-up/jquery.counterup.min.js"></script>
    <script src="vendors/fancyBox/source/jquery.fancybox.pack.js"></script>
    <script src="vendors/fancyBox/source/helpers/jquery.fancybox-thumbs.js"></script>
    <script src="vendors/image-stretcher-master/image-stretcher.js"></script>
    <script src="vendors/wow/wow.min.js"></script>
    <script src="vendors/rateyo/jquery.rateyo.min.js"></script>
    <script src="vendors/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script src="vendors/bootstrap-slider-master/src/js/bootstrap-slider.js"></script>
    <script src="vendors/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
    <script src="js/mega-menu.js"></script>
    <script src="vendors/retina/retina.min.js"></script>
    <!-- Custom Script -->
    <script src="js/jquery.main.js"></script>
    <!-- REVOLUTION JS FILES -->
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/jquery.themepunch.revolution.min.js"></script>
    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.migration.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.video.min.js"></script>
    <!-- SNOW ADD ON -->
    <script type="text/javascript" src="vendors/rev-slider/revolution-addons/snow/revolution.addon.snow.min.js"></script>
    <!-- Revolution Slider Script -->
    <script src="js/revolution.js"></script>
</body>

</html>
