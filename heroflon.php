<?php
use Melbahja\Seo\Factory;

// Load Composer's autoloader
require 'vendor/autoload.php';

$metatags = Factory::metaTags();
$title = "Florence One | Rivenditore SAP Italia | Heroflon ";

$metatags->meta('author', 'Biznes')
		->meta('title', $title)
		->meta('description', 'Scopri il caso di successo. Heroflon  opera a livello globale nel mercato dei polimeri tecnici fluorurati La forte attitudine verso innovazione, ricerca, etica, sicurezza e sostenibilità, sono i suoi valori fondamentali.')
        ->meta('keywords', 'SAP Business One, SAP HANA, ERP, software gestionale, software contabilità, Florence One')
		->image('img/clienti/FLORENCE-ONE-heroflon.jpg')
		->url('https://florence-one.it/heroflon')
?>
<!DOCTYPE html>
<html lang="it">

<head>
	<title><?=$title?></title>
	<?=$metatags?>
    <?php include ("header.html"); ?>
</head>

<body>
    <!-- main wrapper -->
    <div id="wrapper">
        <div class="page-wrapper">
            <!-- header of the page -->
            <?php include ("menu.html"); ?>
            <!--/header of the page -->
            <main>
            	<section class="visual">
                    <div class="visual-inner sap-business-one-banner dark-overlay parallax" data-stellar-background-ratio="0.55">
                        <div class="centered">
                            <div class="container">
                                	
                                    <h1 class="visual-title visual-sub-title">HEROFLON</h1>
                                    <div class="breadcrumb-block">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="/"> Home </a></li>
                                            <li class="breadcrumb-item"><a href="gruppi-internazionali"> Gruppi internazionali </a></li>
                                            <li class="breadcrumb-item active"> Heroflon </li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- main content wrapper -->
                <div class="content-wrapper">
                    <section class="content-block">
                        <div class="container">
                            <div class="row mt-5">
                                <div class="col-lg-12 less-wide">
                                    <div class="blog-holder" style="padding-bottom:50px">
                                        <article class="blog-article">
                                                
                                            <div class="blog-desc pt-5">
                                                <div class="blog-img">
                                                    <div class="image-wrap">
                                                        <figure class="">
                                                            <img src="img/clienti/FLORENCE-ONE-heroflon.jpg" alt="images description">
                                                        </figure>
                                                    </div>
                                                </div>
                                                <div class="blog-share mt-5">
                                                    <ul class="social-network with-text">
                                                        <li><strong>Info</strong></li>
                                                        <li>Cliente: Heroflon</li>
                                                        <li><a href="https://heroflon.it/">Sito: https://heroflon.it</a></li>
                                                    </ul>
                                                </div>
                                                <div class="blockquote-block blockquote-block-v2 pt-3 pb-3">
                                                    <blockquote>
                                                        <p>La storia</p>
                                                    </blockquote>
                                                </div>
                                                <p>Heroflon è un'azienda che opera a livello globale nel mercato dei polimeri tecnici fluorurati La forte attitudine all'innovazione e alla ricerca, nonché l'etica, la sicurezza e la sostenibilità, sono i suoi valori fondamentali. Pensare globale e agire locale è la filosofia alla base del nostro approccio. I nostri uffici commerciali e le unità logistiche negli Stati Uniti, in Brasile e nel Regno Unito, sono la chiara evidenza del processo di espansione internazionale a cui lavoriamo da anni La nostra attività, congiuntamente con le solide partnership commerciali stabilite in tutto il mondo, porta ai nostri clienti l'eccellenza e la qualità dei nostri prodotti con tempi di consegna ridotti e il vantaggio di avere sul posto un servizio clienti dedicato</p>
                                                <div class="blockquote-block blockquote-block-v2 pt-3 pb-3">
                                                    <blockquote>
                                                        <p>La sfida</p>
                                                    </blockquote>
                                                </div>
                                                <p>Heroflon aveva come macro obiettivo la realizzazione di un sistema informativo integrato per la raccolta e la gestione di tutte le informazioni aziendali Aveva anche bisogno di connettere tutti i processi e di monitorarli tramite un unico software installato con le opportune specifiche, non solo per le sue due sedi italiane ma anche per quelle estere in Brasile, Stati Uniti e Regno Unito La multi fiscalità e il multilingua utente erano tra le richieste principali</p>
                                                <div class="blockquote-block blockquote-block-v2 pt-3 pb-3">
                                                    <blockquote>
                                                        <p>La soluzione</p>
                                                    </blockquote>
                                                </div>
                                                <p>Il progetto ha innanzitutto richiesto la realizzazione di una model company creata per l'Italia sulla base della quale sono state velocemente replicate le installazioni estere in Brasile, Stati Uniti e Regno Unito L'utilizzo di una piattaforma internazionale nativa multi fiscale e multi lingua ha consentito il go live sulla nuova piattaforma di tutte le filiali italiane ed estere in un unico semestre Per la gestione dell'intero ciclo produttivo, è stato totalmente integrato un' internazionale specifico di settore È stato inoltre integrato il software già presente per la gestione del laboratorio analisi e stress test dei prodotti, con l'aggiunta di una check list software per la gestione della qualità Il tutto completato da cruscotto di business intelligence per l'analisi dei dati globali.</p>
                                                <div class="blog-share mt-5">
                                                    <ul class="social-network with-text">
                                                        
                                                    </ul>
                                                </div>
                                                
                                            </div>
                                        </article>
                                    </div>
                                    <div class="contact-container">
                                        <form action="#" class="comment-form waituk_contact-form">
                                            <fieldset>
                                            	<div class="btn-container">
                                    				<a href="contatti" class="btn btn-primary">Unisciti ai nostri clienti</a>
                               					</div>
                               					<!--
                                                <h6 class="content-title contact-title"><a href="contatti">Entra a far parte del mondo Florence One</a></h6>
                                                
                                                <div class="row">
                                                    <div class="col-sm-6 form-group">
                                                        <input placeholder="Nome" type="text" class="form-control">
                                                    </div>
                                                    <div class="col-sm-6 form-group">
                                                        <input placeholder="Email" type="email" class="form-control">
                                                    </div>
                                                    <div class="col-sm-12 form-group">
                                                        <input placeholder="Azienda" type="text" class="form-control">
                                                    </div>
                                                    <div class="col-sm-12 form-group">
                                                        <textarea placeholder="Messaggio" class="form-control"></textarea>
                                                    </div>
                                                    <div class="col-sm-12 btn-holder">
                                                        <button type="submit" class="btn btn-black btn-full">Invia richiesta</button>
                                                    </div>
                                            
                                        </form>
                                        </div>
                                        -->
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                    </section>
                    </div>
                    <!--/main content wrapper -->
            </main>
            </div>
            <!-- footer of the pagse -->
        <?php include ("footer.html"); ?>
        <!-- jquery library -->
        <script src="vendors/jquery/jquery-2.1.4.min.js"></script>
        <!-- external scripts -->
        <script src="vendors/tether/dist/js/tether.min.js"></script>
        <script src="vendors/bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/stellar/jquery.stellar.min.js"></script>
        <script src="vendors/isotope/javascripts/isotope.pkgd.min.js"></script>
        <script src="vendors/isotope/javascripts/packery-mode.pkgd.js"></script>
        <script src="vendors/owl-carousel/dist/owl.carousel.js"></script>
        <script src="vendors/waypoint/waypoints.min.js"></script>
        <script src="vendors/counter-up/jquery.counterup.min.js"></script>
        <script src="vendors/fancyBox/source/jquery.fancybox.pack.js"></script>
        <script src="vendors/fancyBox/source/helpers/jquery.fancybox-thumbs.js"></script>
        <script src="vendors/image-stretcher-master/image-stretcher.js"></script>
        <script src="vendors/wow/wow.min.js"></script>
        <script src="vendors/rateyo/jquery.rateyo.js"></script>
        <script src="vendors/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script src="vendors/bootstrap-slider-master/src/js/bootstrap-slider.js"></script>
        <script src="vendors/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
        <script src="js/mega-menu.js"></script>
        <!-- custom jquery script -->
        <script src="js/jquery.main.js"></script>
        <!-- REVOLUTION JS FILES -->
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/jquery.themepunch.tools.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/jquery.themepunch.revolution.min.js"></script>
        <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.actions.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.migration.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.video.min.js"></script>
        <!-- SNOW ADD ON -->
        <script type="text/javascript" src="vendors/rev-slider/revolution-addons/snow/revolution.addon.snow.min.js"></script>
        <!-- revolutions slider script -->
        <script src="js/revolution.js"></script>

</html>
