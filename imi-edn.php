<?php
use Melbahja\Seo\Factory;

// Load Composer's autoloader
require 'vendor/autoload.php';

$metatags = Factory::metaTags();
$title = "Florence One | Rivenditore SAP Italia | IMI EDN ";

$metatags->meta('author', 'Biznes')
		->meta('title', $title)
		->meta('description', 'Scopri il caso di successo. Le cliniche Excellence Dental Network offrono cure odontoiatriche ad elevati standard terapeutici e scientifici EDN è una rete di cliniche odontoiatriche che conta 21 sedi in Italia e 5 all estero.')
        ->meta('keywords', 'SAP Business One, SAP HANA, ERP, software gestionale, software contabilità, Florence One')
		->image('img/clienti/FLORENCE-ONE-imi-edn.jpg')
		->url('https://florence-one.it/imi-edn')
?>
<!DOCTYPE html>
<html lang="it">

<head>
	<title><?=$title?></title>
	<?=$metatags?>
    <?php include ("header.html"); ?>
</head>

<body>
    <!-- main wrapper -->
    <div id="wrapper">
        <div class="page-wrapper">
            <!-- header of the page -->
            <?php include ("menu.html"); ?>
            <!--/header of the page -->
            <main>
            	<section class="visual">
                    <div class="visual-inner sap-business-one-banner dark-overlay parallax" data-stellar-background-ratio="0.55">
                        <div class="centered">
                            <div class="container">
                                	
                                    <h1 class="visual-title visual-sub-title">IMI- EDN</h1>
                                    <div class="breadcrumb-block">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="/"> Home </a></li>
                                            <li class="breadcrumb-item"><a href="aziende-commerciali-e-servizi"> Commerciali e servizi </a></li>
                                            <li class="breadcrumb-item active"> IMI- EDN </li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- main content wrapper -->
                <div class="content-wrapper">
                    <section class="content-block">
                        <div class="container">
                            <div class="row mt-5">
                                <div class="col-lg-12 less-wide">
                                    <div class="blog-holder" style="padding-bottom:50px">
                                        <article class="blog-article">
                                                
                                            <div class="blog-desc pt-5">
                                                <div class="blog-img">
                                                    <div class="image-wrap">
                                                        <figure class="">
                                                            <img src="img/clienti/FLORENCE-ONE-imi-edn.jpg" alt="images description">
                                                        </figure>
                                                    </div>
                                                </div>
                                                <div class="blog-share mt-5">
                                                    <ul class="social-network with-text">
                                                        <li><strong>Info</strong></li>
                                                        <li>Cliente: IMI- EDN</li>
                                                        <li><a href="https://www.excellencedentalnetwork.com/">Sito: https://www.excellencedentalnetwork.com</a></li>
                                                    </ul>
                                                </div>
                                                <div class="blockquote-block blockquote-block-v2 pt-3 pb-3">
                                                    <blockquote>
                                                        <p>La storia</p>
                                                    </blockquote>
                                                </div>
                                                <p>Le cliniche Excellence Dental Network offrono cure odontoiatriche ad elevati standard terapeutici e scientifici EDN è una rete di cliniche odontoiatriche che conta 21 sedi in Italia e 5 all'estero, con un fatturato in crescita e un continuo investimento in ricerca scientifica ed Innovazione. La nostra ambizione è di immaginare, precorrere e realizzare il futuro dell'odontoiatria, collegando medicina, chirurgia e altre scienze della vita Utilizziamo la migliore tecnologia disponibile in un ambiente clinico confortevole e igienicamente impeccabile Vogliamo che i nostri pazienti svolgano il loro piano terapeutico con serenità e profonda comprensione di ogni suo passaggio Per questo non accettiamo nessun compromesso su qualità e integrità delle nostre azioni Dott Francesco Martelli, fondatore EDN</p> 
                                                <div class="blockquote-block blockquote-block-v2 pt-3 pb-3">
                                                    <blockquote>
                                                        <p>La sfida</p>
                                                    </blockquote>
                                                </div>
                                                <p> Il sistema informatico di EDN era caratterizzato da molteplici applicativi scarsamente integrati, di conseguenza venivano così generate informazioni poco strutturate Oltre a ciò EDN necessitava anche di una gestione analitica dei magazzini e di assicurare un'esperienza utente fortemente orientata ai processi aziendali Un'altra sua necessità era quella di centralizzare ed ottimizzare le operazioni di acquisto delle singole sedi su un'unica piattaforma tecnologica</p>
                                                <div class="blockquote-block blockquote-block-v2 pt-3 pb-3">
                                                    <blockquote>
                                                        <p>La soluzione</p>
                                                    </blockquote>
                                                </div>
                                                <p>In un'unica piattaforma tecnologica sono stati integrati gli applicativi specifici della clinica, contabili, di magazzino e business intelligence Dopo un'analisi dettagliata dei processi aziendali e della user experience abbiamo fornito al cliente indicatori di performance chiari e aggiornati in tempo reale Grazie a questo e ad un app mobile per la gestione del magazzino, è stata effettuata anche l'ottimizzazione degli approvvigionamenti, la quale ha portato ad un risparmio corrispondente a due full time per anno A completamento della dashbord ogni mese, viene prodotto in modo automatico un bilancio gestionale di gruppo di tutte e 26 le cliniche EDN</p>
                                                <div class="blog-share mt-5">
                                                    <ul class="social-network with-text">
                                                        
                                                    </ul>
                                                </div>
                                                
                                            </div>
                                        </article>
                                    </div>
                                    <div class="contact-container">
                                        <form action="#" class="comment-form waituk_contact-form">
                                            <fieldset>
                                            	<div class="btn-container">
                                    				<a href="contatti" class="btn btn-primary">Unisciti ai nostri clienti</a>
                               					</div>
                               					<!--
                                                <h6 class="content-title contact-title"><a href="contatti">Entra a far parte del mondo Florence One</a></h6>
                                                
                                                <div class="row">
                                                    <div class="col-sm-6 form-group">
                                                        <input placeholder="Nome" type="text" class="form-control">
                                                    </div>
                                                    <div class="col-sm-6 form-group">
                                                        <input placeholder="Email" type="email" class="form-control">
                                                    </div>
                                                    <div class="col-sm-12 form-group">
                                                        <input placeholder="Azienda" type="text" class="form-control">
                                                    </div>
                                                    <div class="col-sm-12 form-group">
                                                        <textarea placeholder="Messaggio" class="form-control"></textarea>
                                                    </div>
                                                    <div class="col-sm-12 btn-holder">
                                                        <button type="submit" class="btn btn-black btn-full">Invia richiesta</button>
                                                    </div>
                                            
                                        </form>
                                        </div>
                                        -->
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                    </section>
                    </div>
                    <!--/main content wrapper -->
            </main>
            </div>
            <!-- footer of the pagse -->
        <?php include ("footer.html"); ?>
        <!-- jquery library -->
        <script src="vendors/jquery/jquery-2.1.4.min.js"></script>
        <!-- external scripts -->
        <script src="vendors/tether/dist/js/tether.min.js"></script>
        <script src="vendors/bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/stellar/jquery.stellar.min.js"></script>
        <script src="vendors/isotope/javascripts/isotope.pkgd.min.js"></script>
        <script src="vendors/isotope/javascripts/packery-mode.pkgd.js"></script>
        <script src="vendors/owl-carousel/dist/owl.carousel.js"></script>
        <script src="vendors/waypoint/waypoints.min.js"></script>
        <script src="vendors/counter-up/jquery.counterup.min.js"></script>
        <script src="vendors/fancyBox/source/jquery.fancybox.pack.js"></script>
        <script src="vendors/fancyBox/source/helpers/jquery.fancybox-thumbs.js"></script>
        <script src="vendors/image-stretcher-master/image-stretcher.js"></script>
        <script src="vendors/wow/wow.min.js"></script>
        <script src="vendors/rateyo/jquery.rateyo.js"></script>
        <script src="vendors/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script src="vendors/bootstrap-slider-master/src/js/bootstrap-slider.js"></script>
        <script src="vendors/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
        <script src="js/mega-menu.js"></script>
        <!-- custom jquery script -->
        <script src="js/jquery.main.js"></script>
        <!-- REVOLUTION JS FILES -->
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/jquery.themepunch.tools.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/jquery.themepunch.revolution.min.js"></script>
        <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.actions.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.migration.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.video.min.js"></script>
        <!-- SNOW ADD ON -->
        <script type="text/javascript" src="vendors/rev-slider/revolution-addons/snow/revolution.addon.snow.min.js"></script>
        <!-- revolutions slider script -->
        <script src="js/revolution.js"></script>

</html>
