<?php
// This function expects the input to be UTF-8 encoded.
function slugify($text)
{
    // Swap out Non "Letters" with a -
    $text = preg_replace('/[^\\pL\d]+/u', '-', $text); 	
	$text = str_replace("-39-", "-", $text);
    // Trim out extra -'s
    $text = trim($text, '-');
    // Convert letters that we have left to the closest ASCII representation
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
    // Make text lowercase
    $text = strtolower($text);
    // Strip out anything we haven't been able to convert
    $text = preg_replace('/[^-\w]+/', '', $text);
    return $text;
}

use Melbahja\Seo\Factory;

// Load Composer's autoloader
require 'vendor/autoload.php';

$metatags = Factory::metaTags();
$title = "Florence One | Rivenditore SAP Italia | Home ";

$metatags->meta('author', 'Biznes')
		->meta('title', $title)
		->meta('description', 'Consulenza IT Enterprise e Digital Transformation al servizio della tua azienda. Studiamo soluzioni personalizzate e su misura per processi di business.')
        ->meta('keywords', 'SAP Business One, SAP Business ByDesign, Kendox Infoshare, OnBase Hyland, WHS, software contabilità')
		->image('img/florence-one-digitalizza-impresa.jpg')
		->url('https://florence-one.it')
?>
<!DOCTYPE html>
<html lang="it">

<head>
	<title><?=$title?></title>
	<?=$metatags?>
    <?php include ("header.html"); ?>
</head>

<body class="white-overlay">
    <div class="preloader" id="pageLoad">
        <div class="holder">
            <!--<div class="coffee_cup"></div>-->
            <div></div>
        </div>
    </div>
    
    <!-- main wrapper -->
    <div id="wrapper" class="no-overflow-x">
        <div class="page-wrapper">
            <?php include ("menu.html"); ?>
            <main>
                <!-- visual/banner of the page -->
                <div class="banner banner-home">
                	
                    <div id="rev_slider_279_1_wrapper" class="rev_slider_wrapper fullscreen-container" data-alias="restaurant-header" style="margin:0px auto;background-color:#fff;padding:0px;margin-top:0px;margin-bottom:0px;">
                    	
                        <div id="rev_slider_70_1" class="rev_slider fullscreenabanner" style="display:none;" data-version="5.1.4">
                            <ul>
                                <li class="slider-color-schema-dark" data-index="rs-2" data-transition="fade" data-slotamount="7" data-easein="default" data-easeout="default" data-masterspeed="1000" data-rotate="0" data-saveperformance="off" data-title="Slide" data-description="">
                                    <img src="img/slide-florence-one-home.jpg" alt="image description" data-bgposition="center center" data-kenburns="on" data-duration="30000" data-ease="Linear.easeNone" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="0 0" data-offsetend="0 0" data-bgparallax="10" class="rev-slidebg" data-bgfit="cover" data-no-retina>
                                    <div class="tp-caption tp-shape tp-shapewrapper" id="slide-1699-layer-10" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-width="full"
                                    data-height="full" data-whitespace="nowrap" data-type="shape" data-basealign="slide" data-responsive_offset="on" data-responsive="off" data-frames='[{"from":"y:0;sX:1;sY:1;opacity:0;","speed":2500,"to":"o:1;","delay":500,"ease":"Power4.easeOut"},{"delay":"wait","speed":300,"to":"opacity:0;","ease":"nothing"}]' data-textAlign="['left','left','left','left']" data-paddingtop="[0,0,0,0]"
                                    data-paddingright="[0,0,0,0]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[0,0,0,0]" style="background-color:rgba(0, 0, 0, 0.57);"> </div>
                                    <div class="slider-sub-title text-white tp-caption tp-resizeme rs-parallaxlevel-0" data-x="['left','left','left','left']" data-hoffset="['0','0','0','0']" data-y="['top','top','middle','middle']" data-voffset="['145','100','10','20']" data-width="['1200','960','720','540']" data-textalign="left" data-fontsize="['30','28','24','20']" data-lineheight="['72','62','50','50']" data-letterspacing="5" data-height="none" data-whitespace="normal" data-transform_idle="o:1;" data-transform_in="z:0;rX:0deg;rY:0;rZ:0;sX:1.5;sY:1.5;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:0px;" data-mask_out="x:inherit;y:inherit;" data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on" data-paddingright="[25,25,25,25]" data-paddingleft="[25,25,25,25]">
                                        INNOVAZIONE E TECNOLOGIA
                                    </div>
                                    <div class="slider-main-title text-white tp-caption tp-resizeme rs-parallaxlevel-1" data-x="['left','left','left','left']" data-hoffset="['0','0','0','0']" data-y="['top','top','middle','middle']" data-voffset="['250','150','50','50']" data-width="['1200','960','720','540']" data-textalign="left" data-fontsize="['80','50','35','30']" data-fontweight="900" data-letterspacing="['25','10','5','0']" data-lineheight="['184','100','72','60']" data-height="none" data-whitespace="normal" data-transform_idle="o:1;" data-transform_in="z:0;rX:0deg;rY:0;rZ:0;sX:1.5;sY:1.5;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:0px;" data-mask_out="x:inherit;y:inherit;" data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on" data-paddingright="[25,25,25,25]" data-paddingleft="[25,25,25,25]">
                                        FLORENCE ONE
                                    </div>
                                    
                                    <!--<div class="slider-main-title text-white tp-caption tp-resizeme rs-parallaxlevel-1" data-x="['left','left','left','left']" data-hoffset="['0','0','0','0']" data-y="['top','top','middle','middle']" data-voffset="['250','150','50','50']" data-width="['1200','960','720','540']" data-textalign="left" data-fontsize="['160','88','64','48']" data-fontweight="900" data-letterspacing="['25','10','5','0']" data-lineheight="['184','100','72','60']" data-height="none" data-whitespace="normal" data-transform_idle="o:1;" data-transform_in="z:0;rX:0deg;rY:0;rZ:0;sX:1.5;sY:1.5;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:0px;" data-mask_out="x:inherit;y:inherit;" data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on" data-paddingright="[25,25,25,25]" data-paddingleft="[25,25,25,25]">
                                        FLORENCE ONE
                                    </div>-->
                                    <div class="slider-text text-white tp-caption tp-resizeme rs-parallaxlevel-2" data-x="['left','left','left','left']" data-hoffset="['0','0','0','0']" data-y="['top','top','middle','middle']" data-voffset="['450','230','110','110']" data-width="['600','555','555','480']" data-textalign="left" data-fontsize="['16','14','14','14']" data-lineheight="['30','30','22','22']" data-fontweight="400" data-height="none" data-whitespace="normal" data-transform_idle="o:1;" data-transform_in="z:0;rX:0deg;rY:0;rZ:0;sX:1.5;sY:1.5;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="y:[100%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:0px;" data-mask_out="x:inherit;y:inherit;" data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on" data-paddingright="[25,25,25,25]" data-paddingleft="[25,25,25,25]">
                                        Consulenza IT Enterprise e Digital Transformation al servizio della tua azienda. Studiamo soluzioni personalizzate e su misura per processi di business.
                                    </div>
                                    <!--
                                    <div class="tp-caption rev-btn  rs-parallaxlevel-10" id="slide-163-layer-1" data-x="['left','left','left','left']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['320','60','240','220']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power3.easeOut;" data-transform_in="y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;s:2000;e:Power4.easeInOut;" data-transform_out="y:[175%];s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_out="x:inherit;y:inherit;" data-start="1250" data-splitin="none" data-splitout="none" data-actions='[{"event":"click","action":"jumptoslide","slide":"rs-164","delay":""}]' data-responsive_offset="on" data-paddingtop="[0,0,0,0]" data-paddingright="[25,25,25,25]" data-paddingbottom="[0,0,0,0]" data-paddingleft="[25,25,25,25]">
                                        <a class="btn btn-primary has-radius-small" href="#">SCOPRI FLORENCE ONE</a>
                                    </div>
                                    -->
                                </li>
                            </ul>
                        </div>
                        
                    </div>
                </div>
                <!--/visual/banner of the page -->
                <!-- main content wrapper -->
                <section class="content-block portfolio-block" id="container" style="padding-top: 0rem">
                        <div class="row grid">
                            <div class="gallery-item col-lg-4 col-md-6 ui photography">
                                <figure class="picture-item img-block shine-effect image-zoom port-v2">
                                    <img src="img/prodotti/FLORENCE-ONE-sap-business-one.jpg" alt="images description">
                                    <figcaption>
                                        <div class="link-box" style="background-color:#222222; padding-top:500px; padding-bottom:500px">
                                            <h3 style="color:white;">Scopri</br>SAP Business One</h3>
                                            <a href="sap-business-one">
                                                <span class="icon-link"><span class="sr-only">&amp;</span></span>
                                            </a>
                                        </div>
                                    </figcaption>
                                </figure>
                            </div>
                            <div class="gallery-item col-lg-4 col-md-6 photography programming">
                                <figure class="picture-item img-block shine-effect image-zoom port-v2">
                                    <img src="img/prodotti/FLORENCE-ONE-sap-business-by-design.jpg" alt="images description">
                                    <figcaption>
                                        <div class="link-box" style="background-color:#222222; padding-top:500px; padding-bottom:500px">
                                            <h3 style="color:white">Scopri</br>SAP Business By Design</h3>
                                            <a href="sap-business-bydesign">
                                                <span class="icon-link"><span class="sr-only">&amp;</span></span>
                                            </a>
                                        </div>
                                    </figcaption>
                                </figure>
                            </div>
                            <div class="gallery-item col-lg-4 col-md-6 ui photography">
                                <figure class="picture-item img-block shine-effect image-zoom port-v2">
                                    <img src="img/prodotti/FLORENCE-ONE-kendox.jpg" alt="images description">
                                    <figcaption>
                                        <div class="link-box" style="background-color:#222222; padding-top:500px; padding-bottom:500px">
                                            <h3 style="color:white">Scopri</br>Kendox ECM</h3>
                                            <a href="kendox-ecm">
                                                <span class="icon-link"><span class="sr-only">&amp;</span></span>
                                            </a>
                                        </div>
                                    </figcaption>
                                </figure>
                            </div>
                        </div>
                        <div class="row grid">
                            <div class="gallery-item col-lg-4 col-md-6 ui photography">
                                <figure class="picture-item img-block shine-effect image-zoom port-v2">
                                    <img src="img/prodotti/FLORENCE-ONE-on-base-hyland.jpg" alt="images description">
                                    <figcaption>
                                        <div class="link-box" style="background-color:#222222; padding-top:500px; padding-bottom:500px">
                                            <h3 style="color:white">Scopri</br>OnBase</h3>
                                            <a href="on-base-hyland">
                                                <span class="icon-link"><span class="sr-only">&amp;</span></span>
                                            </a>
                                        </div>
                                    </figcaption>
                                </figure>
                            </div>
                            <div class="gallery-item col-lg-4 col-md-6 photography programming">
                                <figure class="picture-item img-block shine-effect image-zoom port-v2">
                                    <img src="img/prodotti/FLORENCE-ONE-wms.jpg" alt="images description">
                                    <figcaption>
                                        <div class="link-box" style="background-color:#222222; padding-top:500px; padding-bottom:500px">
                                            <h3 style="color:white">Scopri</br>WHS</h3>
                                            <a href="whs-mobile">
                                                <span class="icon-link"><span class="sr-only">&amp;</span></span>
                                            </a>
                                        </div>
                                    </figcaption>
                                </figure>
                            </div>
                            <div class="gallery-item col-lg-4 col-md-6 ui photography">
                                <figure class="picture-item img-block shine-effect image-zoom port-v2">
                                    <img src="img/settori/FLORENCE-ONE_logo-settori-800.jpg" alt="images description">
                                    <!--
                                    <figcaption>
                                        <div class="link-box" style="background-color:#222222; padding-top:500px; padding-bottom:500px">
                                            <h3 style="color:white">Scopri</br>tutti i prodotti</h3>
                                            <a href="#">
                                                <span class="icon-link"><span class="sr-only">&amp;</span></span>
                                            </a>
                                        </div>
                                    </figcaption>
                                    -->
                                </figure>
                            </div>
                        </div>
                    </section>
                <div class="content-wrapper">
                    <section class="content-block">
                        <div class="container">
                            <div class="row multiple-row v-align-row">
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="block-heading">
                                            <h3 class="block-top-heading">FLORENCE ONE</h3>
                                            <h2 class="block-main-heading">AL FIANCO DELLE PMI</h2>
                                            <span class="block-sub-heading">Le migliori soluzioni tecnologiche Enterprise internazionali, pensate per PMI in crescita che vogliono semplificare e rendere più efficienti tutti i processi di business.</span>
                                            <div class="divider"><img src="img/divider.png" alt="images description"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">CONSULENZA</br>BUSINESS</h4>
                                            <div class="des">
                                                <p>Selezioniamo le migliori soluzioni IT a livello internazionale per far crescere la tua Azienda.</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">TECNOLOGIA E INNOVAZIONE</h4>
                                            <div class="des">
                                                <p>Aiutiamo la tua Azienda nella scelta della migliore soluzione software per le tue esigenze.</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">SOLUZIONI PERSONALIZZATE</h4>
                                            <div class="des">
                                                <p>Soluzioni personalizzate fino al minimo dettaglio, per riflettere i processi unici della tua Azienda.</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">SEMPRE AL</br>TUO FIANCO</h4>
                                            <div class="des">
                                                <p>Florence One non ti lascia mai solo, avrai sempre il massimo supporto.</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">PERSONALE</br>QUALIFICATO</h4>
                                            <div class="des">
                                                <p>Personale specializzato e competenze multisettore per interventi mirati.</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <!--
                    <section class="content-block quotation-block black-overlay-6 parallax" data-stellar-background-ratio="0.55">
                        <div class="container">
                            <div class="inner-wrapper">
                                <h3 class="block-top-heading text-white">BEST EVER DESIGN</h3>
                                <h2 class="text-white">Time to enhance your web presence!</h2>
                                <div class="btn-container">
                                    <a href="#" class="btn btn-primary has-radius-small">GET QUOTATION</a>
                                </div>
                            </div>
                        </div>
                    </section>
                    -->
                    <?php include ("sezione-settori.html"); ?>
                    <section class="content-block pt-0">
                        <div class="container">
                            <div class="block-heading bottom-space">
                                <h3 class="block-top-heading">I NOSTRI</h3>
                                <h2 class="block-main-heading">SERVIZI</h2>
                                <span class="block-sub-heading">Servizi e soluzioni calibrate sulle tue esigenze.</span>
                                <div class="divider"><img src="img/divider.png" alt="images description"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="bottom-space-small-only">
                                        <p>Florence One offre soluzioni software e servizi calibrati sulle tue reali esigenze di business. Aiutiamo tutte quelle aziende che credono nel valore della semplificazione dei processi aziendali e nella digitalizzazione delle informazioni.</p>
                                        <p>Consulenza business e soluzioni software sono pensate per snellire l’organizzazione aziendale e rendere più efficienti i tuoi processi di business, ottenendo benefici in termini di redditività, efficienza e riduzione dei costi.  </p>
                                        <div class="btn-container top-m-space">
                                            <a href="servizi" class="btn btn-black has-radius-small">SCOPRI I SERVIZI</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="bottom-s-space">
                                        <p>Scopri i nostri servizi, tutti fondati su esperienza e conoscenze approfondite per garantirti solo soluzioni di massima qualità.</p>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <ul class="content-links">
                                                <li><a href="servizi">DIGITAL INTEGRATION</a></li>
                                                <li><a href="servizi">ENTERPRISE CONTENT</a></li>
                                                <li><a href="servizi">CYBER SECURITY</a></li>
                                                <li><a href="servizi">SVILUPPO SOFTWARE CUSTOM</a></li>
                                                <li><a href="servizi">IT CONSULTING</a></li>
                                            </ul>
                                        </div>
                                        <div class="col-sm-6">
                                            <ul class="content-links">
                                            	<li><a href="servizi">SISTEMI MES</a></li>
                                            	<li><a href="servizi">NETWORK</a></li>
                                                <li><a href="servizi">DATABASE DESIGN</a></li>
                                                <li><a href="servizi">ECOMMERCE SOLUTIONS</a></li>
                                                <li><a href="servizi">MARKETPLACE SOLUTIONS</a></li>
                                                
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section id="work-section" class="content-block work-block">
                        <div class="bg-stretch">
                            <img src="img/florence-one-digitalizza-impresa.jpg" alt="">
                        </div>
                        <div class="container">
                            <div class="block-heading bottom-space text-center">
                                <h3 class="block-top-heading">DIGITALIZZA</h3>
                                <h2 class="block-main-heading">LA TUA IMPRESA</h2>
                                <span class="block-sub-heading">Le migliori tecnologie per un modo di lavorare nuovo</span>
                                <div class="divider"><img src="img/divider.png" alt="images description"></div>
                            </div>
                            <div class="description text-center container-md">
                                <p>Competenze e strumenti all’avanguardia per un nuovo modo di lavorare con processi fluidi, efficienti e veloci. Porta la tua azienda nell’era digitale adottando strutture e soluzioni capaci di creare un nuovo modo di condividere informazioni e documenti, offrendo una visione totale e trasparente dell’andamento aziendale. 
                                </p>
                            </div>
                        </div>
                    </section>
                    <section class="content-block">
                        <div class="container">
                            <div class="block-heading bottom-space">
                                <h3 class="block-top-heading">SCOPRI LE ULTIME</h3>
                                <h2 class="block-main-heading">NEWS</h2>
                                <span class="block-sub-heading">Florence One aggiunge continuamente nuove soluzioni tecnologiche innovative.</br>Per rimanere sempre aggiornato segui le nostre news.</span>
                                <div class="btn-container top-m-space">
                                    <a href="news" class="btn btn-black has-radius-small">LEGGI TUTTE LE NEWS</a>
                                </div>
                                <div class="divider"><img src="img/divider.png" alt="images description"></div>
                            </div>
                            <div class="team-container">
                                <div class="owl-carousel group-slide bottom-m-space">
                                <?php include ("db.php"); ?>
                                <?php
                                    $articoli = $db->query('SELECT * FROM news WHERE attivo = 1 ORDER BY data DESC LIMIT 6')->fetchAll();

                                    foreach ($articoli as $articolo) {
                                        $img = json_decode($articolo["img"], true);
                                        $i = 'backoffice/'.$img[0]['serverFileName'];
                                ?>
                                   <div class="slide-item">
                                        <a href="/news/<?=$articolo['id']?>-<?=slugify($articolo['titolo'])?>">
                                        <figure class="team-box caption-fade-up">
                                            <div class="img-block rev-gray-scale">
                                                <img src="<?=$i?>" alt="images description">
                                            </div>
                                            <figcaption class="text-right">
                                                <strong class="content-title mb-0"><?=$articolo['titolo']?></strong>
                                            </figcaption>
                                        </figure>
                                        </a>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </section>
                    
                    
                    <?php include ("sezione_clienti.html"); ?>
                </div>
                <!--/main content wrapper -->
            </main>
        </div>
        <?php include ("footer.html"); ?>
        
        
        
    <!-- jQuery Library -->
    <script src="vendors/jquery/jquery-2.1.4.min.js"></script>
    <!-- Vendor Scripts -->
    <script src="vendors/tether/dist/js/tether.min.js"></script>
    <script src="vendors/bootstrap/js/bootstrap.min.js"></script>
    <script src="vendors/stellar/jquery.stellar.min.js"></script>
    <script src="vendors/isotope/javascripts/isotope.pkgd.min.js"></script>
    <script src="vendors/isotope/javascripts/packery-mode.pkgd.js"></script>
    <script src="vendors/owl-carousel/dist/owl.carousel.min.js"></script>
    <script src="vendors/waypoint/waypoints.min.js"></script>
    <script src="vendors/counter-up/jquery.counterup.min.js"></script>
    <script src="vendors/fancyBox/source/jquery.fancybox.pack.js"></script>
    <script src="vendors/fancyBox/source/helpers/jquery.fancybox-thumbs.js"></script>
    <script src="vendors/image-stretcher-master/image-stretcher.js"></script>
    <script src="vendors/wow/wow.min.js"></script>
    <script src="vendors/rateyo/jquery.rateyo.min.js"></script>
    <script src="vendors/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
    <script src="vendors/bootstrap-slider-master/src/js/bootstrap-slider.js"></script>
    <script src="vendors/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
    <script src="js/mega-menu.js"></script>
    <script src="vendors/retina/retina.min.js"></script>
    <!-- Custom Script -->
    <script src="js/jquery.main.js"></script>
    <!-- REVOLUTION JS FILES -->
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/jquery.themepunch.revolution.min.js"></script>
    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.migration.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.video.min.js"></script>
    <!-- SNOW ADD ON -->
    <script type="text/javascript" src="vendors/rev-slider/revolution-addons/snow/revolution.addon.snow.min.js"></script>
    <!-- Revolution Slider Script -->
    <script src="js/revolution.js"></script>
</body>

</html>
