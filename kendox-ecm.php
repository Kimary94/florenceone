<?php
use Melbahja\Seo\Factory;

// Load Composer's autoloader
require 'vendor/autoload.php';

$metatags = Factory::metaTags();
$title = "Florence One | Rivenditore Kendox Italia | Kendox Infoshare ECM ";

$metatags->meta('author', 'Biznes')
		->meta('title', $title)
		->meta('description', 'Kendox Infoshare ECM, software di gestione documentale. Gestisci i documenti in forma digitale e automatizza i processi informativi. Kendox InfoShare è la piattaforma avanzata ed innovativa che offre soluzioni pratiche e alla portata di tutti.')
        ->meta('keywords', 'Kendox Infoshare ECM, software per gestione documentale, ECM, digitalizzazione documenti, documenti aziendali digitali')
		->image('img/florence-one-kendox-infoshare-1.jpg')
		->url('https://florence-one.it/kendox-ecm')
?>
<!DOCTYPE html>
<html lang="it">

<head>
	<title><?=$title?></title>
	<?=$metatags?>
    <?php include ("header.html"); ?>
</head>

<body>
    <!-- main wrapper -->
    <div id="wrapper">
        <div class="page-wrapper">
            <?php include ("menu.html"); ?>
            <main>
                <!-- visual/banner of the page -->
                <section class="visual">
                    <div class="visual-inner sap-business-one-banner dark-overlay parallax" data-stellar-background-ratio="0.55">
                        <div class="centered">
                            <div class="container">
                                	<img src="img/prodotti/logo-kendox-ecm-florence-one.png" alt="">
                                	</br></br>
                                    <div class="breadcrumb-block">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="/"> Home </a></li>
                                            <li class="breadcrumb-item active"> Kendox InfoShare ECM </li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/visual/banner of the page -->
                <!-- main content wrapper -->
                <div class="content-wrapper">
                    <section class="content-block">
                        <div class="container text-center">
                        	
                            <div class="heading bottom-space">
                            	<h1 class="visual-title visual-sub-title" style="color:#55565B" >Kendox Infoshare ECM</h1>
                                <h2>La soluzione ECM completa</h2>
                            </div>
                            <div class="description">
                                <p>Gestisci i documenti in forma digitale e automatizza i processi informativi.</br>
								Kendox InfoShare è la piattaforma avanzata ed innovativa che offre soluzioni pratiche e alla portata di tutti.
								</p>
                            </div>
                        </div>
                    </section>
                    <section class="content-block p-0">
                        <div class="container-fluid">
                            <div class="content-slot alternate-block">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="bg-stretch img-wrap wow slideInLeft">
                                            <img src="img/florence-one-kendox-infoshare-1.jpg" alt="images">
                                        </div>
                                    </div>
                                    <div class="col col-lg-6">
                                        <div class="text-wrap">
                                            <h3>Documenti, archiviazione e workflow con Kendox.</h3>
                                            <p>Kendox InfoShare ti offre soluzioni pratiche per la gestione di documenti, archiviazione e workflow. Facile e flessibile si adatta anche a piccole e medie imprese.</p>
                                            <p>Raccogli e trasferisci dati in modo automatico, sfruttando l’ampia gamma di opzioni di integrazione..</p>
                                            <p>L’archiviazione di documenti, dati e record e la loro conservazione è conforme alle normative vigenti, tracciabile e non alterabile per una sicurezza garantita durante tutto il periodo di conservazione.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="bg-stretch img-wrap wow slideInRight">
                                            <img src="img/florence-one-kendox-infoshare-2.jpg" alt="images">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="text-wrap">
                                            <h3>Ottimizza le informazioni aziendali.</h3>
                                            <p>Completo, scalabile e integrabile in modo facile e veloce con altri applicativi e sistemi leader di mercato. Potrai gestire l’informazione qualunque sia l’origine ottimizzando la sua fruizione nei processi aziendali e decisionali. </p>
                                            <p>L’interfaccia è semplice per un utilizzo intuitivo da parte dell’utente e le soluzioni offerte sono disponibili anche in cloud per un utilizzo anche da mobile grazie al suo design reattivo.</p>
                                            <p>Snellisci la gestione dei documenti all'interno della tua azienda. </p>
                                            <p></p>
                                            <a href="brochure/Brochure_Kendox.pdf" target="_blank" class="btn btn-black has-radius-small">Scarica la brochure</a>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </section>
                    <section class="content-block">
                        <div class="container">
                            <div class="row multiple-row v-align-row">
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="block-heading">
                                            <h3 class="block-top-heading">Kendox Infoshare</h3>
                                            <h2 class="block-main-heading">FUNZIONI PRINCIPALI</h2>
                                            <span class="block-sub-heading">Velocità, trasparenza e precisione sono tutte qualità che Kendox InfoShare porta all’interno dell’azienda con la sua gestione digitale di dati e processi informativi.</span>
                                            <div class="divider"><img src="img/divider.png" alt="images description"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">ACQUISIZIONE</h4>
                                            <div class="des">
                                                <p>Acquisizione veloce di dati e documenti da qualsiasi fonte.</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">ORGANIZZAZIONE</h4>
                                            <div class="des">
                                                <p>Organizza tutti i tuoi dati in modo intelligente, centralizzato e sicuro.</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">CONDIVISIONE</h4>
                                            <div class="des">
                                                <p>Condivisione delle informazioni sicura e veloce tra team e stakeholders.</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">ACCESSIBILITÀ</h4>
                                            <div class="des">
                                                <p>Informazioni disponibili in modo immediato e fruibili da qualsiasi device.</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">INTEGRAZIONE</h4>
                                            <div class="des">
                                                <p>API che consentono l’integrazione con altri applicativi presenti.</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">RICERCA</h4>
                                            <div class="des">
                                                <p>Consultazione veloce e facile di documenti e informazione tramite parole chiave.</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">WORKFLOW</h4>
                                            <div class="des">
                                                <p>Semplificazione dei vari processi aziendali e decisionali.</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">PRECISIONE</h4>
                                            <div class="des">
                                                <p>Gestione dei documenti rapida, efficiente e precisa senza errori o ridondanze.</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="content-block p-0">
                        <div class="container-fluid">
                            <div class="content-slot alternate-block">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="bg-stretch img-wrap wow slideInLeft">
                                            <img src="img/florence-one-kendox-infoshare-3.jpg" alt="images">
                                        </div>
                                    </div>
                                    <div class="col col-lg-6">
                                        <div class="text-wrap">
                                            <h3>Richiedi subito una demo</h3>
                                            <p>Sei interessato ad avere specifiche tecniche approfondite? Compila il form.</p>
                                            <p></p>
                                        <!-- contact form -->
                                        <?php include ("form-prodotti.html"); ?>
                                    
                                            
                                            
                                            
                                            
                                            
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="bg-stretch img-wrap wow slideInRight">
                                            <img src="img/florence-one-kendox-infoshare-4.jpg" alt="images">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="text-wrap">
                                            <h3>Dai valore all’informazione.</h3>
                                            <p>Una corretta ed efficiente gestione documentale e dei processi informativi è determinante per garantire alti standard di qualità e precisione.  </p>
                                            <p>Scegliere di digitalizzare i processi significa dare maggior valore all’informazione ed avere la possibilità di sfruttare le potenzialità di strumenti di Business Intelligente per l’analisi dei dati aziendali con un vantaggi evidenti in termini di risultati, tempestività e sicurezza.</p>
                                            <p>Florence One al tuo fianco nella crescita della tua azienda. </p>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </section>
                    <section class="content-block quotation-block black-overlay-6 parallax" data-stellar-background-ratio="0.55">
                        <div class="container">
                            <div class="inner-wrapper">
                                <h3 class="block-top-heading text-white">LE MIGLIORI TECNOLOGIE</h3>
                                <h2 class="text-white">Kendox Infoshare ECM</br>presente nelle migliori aziende</h2>
                                <div class="btn-container">
                                    <a href="contatti" class="btn btn-primary">Richiedi una valutazione</a>
                                </div>
                            </div>
                        </div>
                    </section>
                    <?php include ("sezione_clienti.html"); ?>
                </div>
                <!--/main content wrapper -->
            </main>
        </div>
        <?php include ("footer.html"); ?>
    <!-- jquery library -->
    <script src="vendors/jquery/jquery-2.1.4.min.js"></script>
    <!-- external scripts -->
    <script src="vendors/tether/dist/js/tether.min.js"></script>
    <script src="vendors/bootstrap/js/bootstrap.min.js"></script>
    <script src="vendors/stellar/jquery.stellar.min.js"></script>
    <script src="vendors/isotope/javascripts/isotope.pkgd.min.js"></script>
    <script src="vendors/isotope/javascripts/packery-mode.pkgd.js"></script>
    <script src="vendors/owl-carousel/dist/owl.carousel.js"></script>
    <script src="vendors/waypoint/waypoints.min.js"></script>
    <script src="vendors/counter-up/jquery.counterup.min.js"></script>
    <script src="vendors/fancyBox/source/jquery.fancybox.pack.js"></script>
    <script src="vendors/fancyBox/source/helpers/jquery.fancybox-thumbs.js"></script>
    <script src="vendors/image-stretcher-master/image-stretcher.js"></script>
    <script src="vendors/wow/wow.min.js"></script>
    <script src="vendors/rateyo/jquery.rateyo.js"></script>
    <script src="vendors/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="vendors/bootstrap-slider-master/src/js/bootstrap-slider.js"></script>
    <script src="vendors/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
    <script src="js/mega-menu.js"></script>
    <!-- custom jquery script -->
    <script src="js/jquery.main.js"></script>
    <!-- REVOLUTION JS FILES -->
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/jquery.themepunch.revolution.min.js"></script>
    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.migration.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.video.min.js"></script>
    <!-- SNOW ADD ON -->
    <script type="text/javascript" src="vendors/rev-slider/revolution-addons/snow/revolution.addon.snow.min.js"></script>
        <!-- revolutions slider script -->
    <script src="js/revolution.js"></script>
</html>
