<div class="modal" id="popupid" tabindex="-1" role="dialog">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
    <h4 class="modal-title">Richiesta inviata</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span CLASS="icon-x-1 mt-0"></span></button>
</div>
<div class="modal-body">
    Grazie per aver contattato Florence One, i nostri addetti prenderanno in carico la tua richiesta e risponderanno non appena disponibilie.<br>
    Ti ricordiamo che puoi contattare direttamente Florence One anche attraverso la mail <a href="mailto:sales@florence-one.it">sales@florence-one.it</a><br> o telefonando al numero <a href="tel:3905731935507">+39 0573 1935507</a>
</div>
<div class="modal-footer">Il team di Florence One</div>
</div>
<!-- /.modal-content --></div>
<!-- /.modal-dialog --></div>
<!-- /.modal -->

<script>
    $( document ).ready(function() {
        let searchParams = new URLSearchParams(window.location.search)
        if(searchParams.has('ok'))
            $('#popupid').modal('show');
    });
</script>