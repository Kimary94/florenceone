<?php
use Melbahja\Seo\Factory;

// Load Composer's autoloader
require 'vendor/autoload.php';

$metatags = Factory::metaTags();
$title = "Florence One | Rivenditore SAP Italia | Most Group ";

$metatags->meta('author', 'Biznes')
		->meta('title', $title)
		->meta('description', 'Scopri il caso di successo. Il GRUPPO MOST si colloca nella lavorazione e nobilitazione di materiali e accessori. Fondata nel 2001, la Società è oggi uno dei principali player italiani attivi nella lavorazione e nobilitazione di una vasta gamma di materiali (specialmente pelle) destinati al settore del lusso e alta moda.')
        ->meta('keywords', 'SAP Business One, SAP HANA, ERP, software gestionale, software contabilità, Florence One')
		->image('img/clienti/FLORENCE-ONE-most-group.jpg')
		->url('https://florence-one.it/most-group')
?>
<!DOCTYPE html>
<html lang="it">

<head>
	<title><?=$title?></title>
	<?=$metatags?>
    <?php include ("header.html"); ?>
</head>

<body>
    <!-- main wrapper -->
    <div id="wrapper">
        <div class="page-wrapper">
            <!-- header of the page -->
            <?php include ("menu.html"); ?>
            <!--/header of the page -->
            <main>
            	<section class="visual">
                    <div class="visual-inner sap-business-one-banner dark-overlay parallax" data-stellar-background-ratio="0.55">
                        <div class="centered">
                            <div class="container">
                                	
                                    <h1 class="visual-title visual-sub-title">MOST GROUP</h1>
                                    <div class="breadcrumb-block">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="/"> Home </a></li>
                                            <li class="breadcrumb-item"><a href="pelletteria-e-tessile"> Pelletteria e tessile </a></li>
                                            <li class="breadcrumb-item active"> Most Group </li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- main content wrapper -->
                <div class="content-wrapper">
                    <section class="content-block">
                        <div class="container">
                            <div class="row mt-5">
                                <div class="col-lg-12 less-wide">
                                    <div class="blog-holder" style="padding-bottom:50px">
                                        <article class="blog-article">
                                                
                                            <div class="blog-desc pt-5">
                                                <div class="blog-img">
                                                    <div class="image-wrap">
                                                        <figure class="">
                                                            <img src="img/clienti/FLORENCE-ONE-most-group.jpg" alt="images description">
                                                        </figure>
                                                    </div>
                                                </div>
                                                <div class="blog-share mt-5">
                                                    <ul class="social-network with-text">
                                                        <li><strong>Info</strong></li>
                                                        <li>Cliente: Most Group</li>
                                                        <li><a href="https://www.most-srl.com/">Sito: https://www.most-srl.com</a></li>
                                                        
                                                    </ul>
                                                </div>
                                                <div class="blockquote-block blockquote-block-v2 pt-3 pb-3">
                                                    <blockquote>
                                                        <p>La storia</p>
                                                    </blockquote>
                                                </div>
                                                <p>Il GRUPPO MOST è un'eccellenza italiana attiva nella lavorazione e nobilitazione di materiali e accessori destinati al settore del lusso e dell'alta moda Fondata nel 2001, la Società è oggi uno dei principali player italiani attivi nella lavorazione e nobilitazione di una vasta gamma di materiali (specialmente pelle) destinati al settore del lusso e dell'alta moda. La Società ha implementato un modello di business di successo grazie all'assistenza fornita ai propri clienti durante l'intero processo produttivo e dalla continua attività di ricerca di nuove tecnologie e materiali. Le recenti acquisizioni di SIRIOMOST S.r.l. e CREATIVE S.r.l. rappresentano il consolidamento del know how del gruppo e la spinta continua all'innovazione a livello tecnologico, di impianti e di competenze.</p>
                                                <div class="blockquote-block blockquote-block-v2 pt-3 pb-3">
                                                    <blockquote>
                                                        <p>La sfida</p>
                                                    </blockquote>
                                                </div>
                                                <p>Le tre aziende avevano sistemi informativi diversi che necessitavano quindi di un'armonizzazione in modo da poter fornire ad utenti interfacce e strumenti uniformi Oltre a questo il gruppo richiedeva urgentemente una pianificazione strutturata della produzione la cui gestione basata sulla sola esperienza non consentiva di ottenere i plus derivanti dalle numerose commesse aggiuntive A livello commerciale era inoltre necessario poter presentare al mercato tutte le possibili opzioni di lavorazione per mezzo di uno strumento che comunicasse modernità ma che fosse estremamente flessibile Il crescente numero di transazioni e dei conseguenti flussi finanziari richiedeva una gestione automatizzata del rapporto con gli istituti di credito</p>
                                                <div class="blockquote-block blockquote-block-v2 pt-3 pb-3">
                                                    <blockquote>
                                                        <p>La soluzione</p>
                                                    </blockquote>
                                                </div>
                                                </p>L'introduzione di SAP Business One nelle sue declinazioni desktop, web e mobile, ha consentito di uniformare l'interfaccia per tutti gli utenti che hanno potuto utilizzare il device più performante e adatto alla loro ruotine quotidiana Il modulo produzione è stato arricchito di un pianificatore a capacità finita che ha ottimizzato la produzione delle tre aziende del gruppo, la gestione logistica è stata inoltre rinforzata da una specifica app mobile fruibile da palmare Al commerciale è stato fornito un catalogo web con l'intera documentazione tecnica e realizzativa del portfolio progetti così che sia possibile presentare da tablet i lavori realizzati ed elaborare nuove proposte Lo stesso portale è stato aperto, con autorizzazioni e sicurezza, ai vari portatori di interessi quali clienti e fornitori realizzando campagne promozionali e condivisione della documentazione commerciale e contabile Il rapporto con gli istituti di credito non viene più gestito manualmente bensì con un modulo dedicato alla tesoreria capace di importare ed esportare i movimenti riconciliando automaticamente le partite e proponendo le migliori condizioni riducendo gli oneri bancari</p>
                                                <div class="blog-share mt-5">
                                                    <ul class="social-network with-text">
                                                        
                                                    </ul>
                                                </div>
                                                
                                            </div>
                                        </article>
                                    </div>
                                    <div class="contact-container">
                                        <form action="#" class="comment-form waituk_contact-form">
                                            <fieldset>
                                            	<div class="btn-container">
                                    				<a href="contatti" class="btn btn-primary">Unisciti ai nostri clienti</a>
                               					</div>
                               					<!--
                                                <h6 class="content-title contact-title"><a href="contatti">Entra a far parte del mondo Florence One</a></h6>
                                                
                                                <div class="row">
                                                    <div class="col-sm-6 form-group">
                                                        <input placeholder="Nome" type="text" class="form-control">
                                                    </div>
                                                    <div class="col-sm-6 form-group">
                                                        <input placeholder="Email" type="email" class="form-control">
                                                    </div>
                                                    <div class="col-sm-12 form-group">
                                                        <input placeholder="Azienda" type="text" class="form-control">
                                                    </div>
                                                    <div class="col-sm-12 form-group">
                                                        <textarea placeholder="Messaggio" class="form-control"></textarea>
                                                    </div>
                                                    <div class="col-sm-12 btn-holder">
                                                        <button type="submit" class="btn btn-black btn-full">Invia richiesta</button>
                                                    </div>
                                            
                                        </form>
                                        </div>
                                        -->
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                    </section>
                    </div>
                    <!--/main content wrapper -->
            </main>
            </div>
            <!-- footer of the pagse -->
        <?php include ("footer.html"); ?>
        <!-- jquery library -->
        <script src="vendors/jquery/jquery-2.1.4.min.js"></script>
        <!-- external scripts -->
        <script src="vendors/tether/dist/js/tether.min.js"></script>
        <script src="vendors/bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/stellar/jquery.stellar.min.js"></script>
        <script src="vendors/isotope/javascripts/isotope.pkgd.min.js"></script>
        <script src="vendors/isotope/javascripts/packery-mode.pkgd.js"></script>
        <script src="vendors/owl-carousel/dist/owl.carousel.js"></script>
        <script src="vendors/waypoint/waypoints.min.js"></script>
        <script src="vendors/counter-up/jquery.counterup.min.js"></script>
        <script src="vendors/fancyBox/source/jquery.fancybox.pack.js"></script>
        <script src="vendors/fancyBox/source/helpers/jquery.fancybox-thumbs.js"></script>
        <script src="vendors/image-stretcher-master/image-stretcher.js"></script>
        <script src="vendors/wow/wow.min.js"></script>
        <script src="vendors/rateyo/jquery.rateyo.js"></script>
        <script src="vendors/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script src="vendors/bootstrap-slider-master/src/js/bootstrap-slider.js"></script>
        <script src="vendors/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
        <script src="js/mega-menu.js"></script>
        <!-- custom jquery script -->
        <script src="js/jquery.main.js"></script>
        <!-- REVOLUTION JS FILES -->
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/jquery.themepunch.tools.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/jquery.themepunch.revolution.min.js"></script>
        <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.actions.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.migration.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.video.min.js"></script>
        <!-- SNOW ADD ON -->
        <script type="text/javascript" src="vendors/rev-slider/revolution-addons/snow/revolution.addon.snow.min.js"></script>
        <!-- revolutions slider script -->
        <script src="js/revolution.js"></script>

</html>
