<?php
// This function expects the input to be UTF-8 encoded.
function slugify($text)
{
    // Swap out Non "Letters" with a -
    $text = preg_replace('/[^\\pL\d]+/u', '-', $text); 	
	$text = str_replace("-39-", "-", $text);
    // Trim out extra -'s
    $text = trim($text, '-');
    // Convert letters that we have left to the closest ASCII representation
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
    // Make text lowercase
    $text = strtolower($text);
    // Strip out anything we haven't been able to convert
    $text = preg_replace('/[^-\w]+/', '', $text);
    return $text;
}
?>
<?php
use Melbahja\Seo\Factory;

// Load Composer's autoloader
require 'vendor/autoload.php';

$metatags = Factory::metaTags();
$title = "Florence One | News ";

$metatags->meta('author', 'Biznes')
		->meta('title', $title)
		->meta('description', 'Florence One, le nostre news. Florence One aggiunge continuamente soluzione tecnologiche innovative. Rimani aggiornato.')
        ->meta('keywords', 'soluzioni tecnologiche, SAP Business One, soluzioni digitali, soluzione innovative, news')
		->image('img/settori/florence-one-kendox-infoshare-4.jpg')
		->url('https://florence-one.it/news')
?>
<!DOCTYPE html>
<html lang="it">

<head>
	<title><?=$title?></title>
	<?=$metatags?>
    <?php include ("header.html"); ?>
</head>

<body>
    <!-- main wrapper -->
    <div id="wrapper">
        <div class="page-wrapper">
            <!-- header of the page -->
            <?php include ("menu.html"); ?>
            <!--/header of the page -->
            <main>
                <!-- visual/banner of the page -->
                <section class="visual">
                    <div class="visual-inner sap-business-one-banner dark-overlay parallax" data-stellar-background-ratio="0.55">
                        <div class="container">
                            <div class="visual-text-large text-left visual-center">
                                <h1 class="visual-title visual-sub-title">News</h1>
                                <p>Florence One aggiunge continuamente nuove soluzioni tecnologiche innovative. Per rimanere sempre aggiornato segui le nostre news.</p>
                                <div class="breadcrumb-block">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="/"> Home </a></li>
                                        <li class="breadcrumb-item active"> News </li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/visual/banner of the page -->
                <!-- main content wrapper -->
                <div class="content-wrapper">
                    <section class="content-block">
                        <div class="container">
                            <div class="row multiple-row">
                                <?php include ("db.php"); ?>
                                <?php
                                    $articoli = $db->query('SELECT * FROM news WHERE attivo = 1 ORDER BY data DESC')->fetchAll();

                                    foreach ($articoli as $articolo) {
                                        $img = json_decode($articolo["img"], true);
                                        $i = '/backoffice/'.$img[0]['serverFileName'];
                                ?>
                                <div class="col-md-6 col-lg-4">
                                    <div class="col-wrap">
                                        <div class="post-grid reverse-grid reverse-grid">
                                            <div class="img-block post-img">
                                                <a href="/news/<?=$articolo['id']?>-<?=slugify($articolo['titolo'])?>"><img src="<?=$i?>" alt="images"></a>
                                                <time class="post-date" datetime="<?=$articolo['data']?>"><?=$articolo['data']?></time>
                                            </div>
                                            <div class="post-text-block bg-gray-light">
                                                <strong class="content-title mb-0">
                                                    <a href="/news/<?=$articolo['id']?>-<?=slugify($articolo['titolo'])?>"><?=$articolo['titolo']?></a>
                                                </strong>
                                                <p><?=$articolo['riassunto']?></p>
                                                <div class="post-meta clearfix">
                                                    <div class="post-link-holder">
                                                        <a href="/news/<?=$articolo['id']?>-<?=slugify($articolo['titolo'])?>">Leggi la news <span class="fa fa-arrow-right"><span class="sr-only">&nbsp;</span></span></a>
                                                    </div>
                                                    <div class="post-social text-right">
                                                        <ul class="social-network social-small">
                                                        	<!--
                                                            <li><a href="#"><span class="icon-facebook"><span class="sr-only">&nbsp;</span></span></a></li>
                                                            <li><a href="#"><span class="icon-linkedin"><span class="sr-only">&nbsp;</span></span></a></li>
                                                            -->
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>


                            </div>

                        </div>
                    </section>
                </div>
                <!--/main content wrapper -->
            </main>
        </div>
        <!-- footer of the pagse -->
        <?php include ("footer.html"); ?>
    <!-- jquery library -->
    <script src="vendors/jquery/jquery-2.1.4.min.js"></script>
    <!-- external scripts -->
    <script src="vendors/tether/dist/js/tether.min.js"></script>
    <script src="vendors/bootstrap/js/bootstrap.min.js"></script>
    <script src="vendors/stellar/jquery.stellar.min.js"></script>
    <script src="vendors/isotope/javascripts/isotope.pkgd.min.js"></script>
    <script src="vendors/isotope/javascripts/packery-mode.pkgd.js"></script>
    <script src="vendors/owl-carousel/dist/owl.carousel.js"></script>
    <script src="vendors/waypoint/waypoints.min.js"></script>
    <script src="vendors/counter-up/jquery.counterup.min.js"></script>
    <script src="vendors/fancyBox/source/jquery.fancybox.pack.js"></script>
    <script src="vendors/fancyBox/source/helpers/jquery.fancybox-thumbs.js"></script>
    <script src="vendors/image-stretcher-master/image-stretcher.js"></script>
    <script src="vendors/wow/wow.min.js"></script>
    <script src="vendors/rateyo/jquery.rateyo.js"></script>
    <script src="vendors/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="vendors/bootstrap-slider-master/src/js/bootstrap-slider.js"></script>
    <script src="vendors/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
    <script src="js/mega-menu.js"></script>
    <!-- custom jquery script -->
    <script src="js/jquery.main.js"></script>
    <!-- REVOLUTION JS FILES -->
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/jquery.themepunch.revolution.min.js"></script>
    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.migration.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.video.min.js"></script>
    <!-- SNOW ADD ON -->
    <script type="text/javascript" src="vendors/rev-slider/revolution-addons/snow/revolution.addon.snow.min.js"></script>
    <!-- revolutions slider script -->
    <script src="js/revolution.js"></script>

</html>
