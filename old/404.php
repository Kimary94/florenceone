<?php
use Melbahja\Seo\Factory;

// Load Composer's autoloader
require 'vendor/autoload.php';

$metatags = Factory::metaTags();

$metatags->meta('author', 'Biznes')
		->meta('title', 'Florence One | Rivenditore SAP Italia - Promo SAP Business One')
		->meta('description', 'SAP Business One, il miglior software gestionale per la gestione dei tuoi processi aziendali dagli ordini clienti alla fatturazione, passando per la produzione. Acquista ora e paghi dal 2021!')
        ->meta('keywords', 'SAP Business One, software gestionale, Rivenditore SAP, partner SAP, gestionale SAP, software contabilità, software gestione produzione, software aziendale, ERP, programma contabilità, programma fatture, SAP, ERP aziendale, Florence One')
		->image('img/slide_servizi.jpg')
		->url('https://florence-one.it/promo')
?>
<!DOCTYPE html>
<html lang="it">
    <head>
        <title>Florence One | Rivenditore SAP Italia - Promo SAP Business One</title>
        <meta name="generator"
        content="HTML Tidy for HTML5 (experimental) for Windows https://github.com/w3c/tidy-html5/tree/c63cc39" />
        <?php include ("head.php"); ?>
        <?=$metatags?>
    </head>
    <body>
        <?php include ("menu.php"); ?>
        
        <!-- banner -->
        <div style="height:100vh" class="container-fluid sfondo-big d-flex justify-content-around align-items-center">
        <div class="container">
            <div class="row text-center testo-banner">
                <div class="col-12">
                    <h4 class="pt-5">Scusaci ma <?php echo $_SERVER['REQUEST_URI']; ?> non esiste.</h4>
                    <p>Prova a ripartire dalla nostra <a href="/">homepage</a></p>
                </div>
            </div>
        </div>
        </div>

        <?php include ("footer.php"); ?>
    </body>
</html> 