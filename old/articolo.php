<?php
use Melbahja\Seo\Factory;
// Load Composer's autoloader
require 'vendor/autoload.php';

if(isset($_GET['id'])){
    include ("conn.php");
    $id = $_GET['id'];
    $sql = "SELECT * FROM news WHERE id = ".$id;
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $img = json_decode($row["img"], true);
            $img = str_replace('\\','/',$img[0]['serverFileName']);

$metatags = Factory::metaTags();
?>
<!DOCTYPE html>
<html lang="it">
    <head>
        <title>Florence One | Rivenditore SAP Italia - <?=htmlentities($row['titolo'], ENT_COMPAT, "iso-8859-1")?></title>
        <meta name="generator"
        content="HTML Tidy for HTML5 (experimental) for Windows https://github.com/w3c/tidy-html5/tree/c63cc39" />
        <?php 
		include ("head.php"); 
		
		$metatags->meta('author', 'Biznes')
				->meta('title', 'Florence One - '.$row['titolo'])
				->meta('description', utf8_encode(substr($row['testo'], 0,155)))
				->image('/backoffice/'.$img)
				->facebook('description', utf8_encode(substr($row['testo'], 0,155)))
				->url('https://florence-one.it/news/'.$id.'/'.slugify($row['titolo']))
		?>
        <?=$metatags?>
    </head>
    <body>
        <?php 
            include ("menu.php"); 
        ?>
        <!-- banner -->
        <div id="banner-top" class="container-fluid sfondo-big d-flex justify-content-around align-items-center" style="background:url('/backoffice/<?=$img?>') no-repeat center center;background-size: cover;">
        <div class="container">
            <div class="row text-center testo-banner">
                <div class="col-12">
                    <h2 class="titolo-pagina"><?=utf8_encode($row['titolo'])?></h2>
                    <p class="pt-3 icona bounce"><i class="fas fa-angle-double-down"></i></p>
                </div>
            </div>
        </div>
        </div>
        
        <!-- sezione bianca -->
        <div class="container py-5">
            <div class="row">
                <?php $data = new DateTime($row['data']); ?>
                <div class="col-12"><h3><?=utf8_encode($row['titolo'])?></h3></div>
                <div class="col-12"><?=utf8_encode($row['testo'])?></div>
                <div class="col-12 py-4"><p><?=$data->format('d-m-Y')?></p></div>
            </div>
        </div>
        
        <!-- Bannerino -->
        <div class="container-fluid white-t py-5 catch sfondo-big" style="background:url('/img/world.png')">
        <div class="container">
            <div class="row">
                <div class="col-12"><h3 class="white-t">LEADER NEL <br>MERCATO ERP</h3></div>
            </div>
        </div>
        </div>

        <?php include ("demo.php"); ?>

        <?php include ("footer.php"); ?>
    </body>
</html> 
<?php
}}}
else{
    header("location: /news.php");
}