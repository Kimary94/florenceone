        <!-- Settori -->
        <div class="container-fluid py-5" style="background:;">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center mb-4">
                <h3 class="py-3 grassetto">Alcuni settori in cui operiamo</h3>
                    <p>Grazie ai progetti sviluppati e alle competenze acquisite in anni di sviluppo e affiancamento,<br>Florence One è in grado di supportare aziende di diversi settori di mercato.</p>
                </div>
                
                 <div class="col-12 col-md-4 lazy" data-src="img/settori/FLORENCE-ONE_aziende-produzione.jpg" class="col-12 col-md-4" style="background-size:cover">
                     <a href="case?sett=4">
                     <div class="row no-gutters">
                         <div class="col-12 position-relative settori">
                            <h5 class="position-absolute white-t grassetto" style="bottom:5%">- Aziende di Produzione</h5>
                         </div>
                     </div>
                     </a>
                 </div>
                 
                 <div class="col-12 col-md-4 lazy" data-src="img/settori/FLORENCE-ONE_aziende-commerciali-servizi.jpg" style="background-size:cover">
                     <a href="case?sett=5">
                     <div class="row no-gutters">
                         <div class="col-12 position-relative settori">
                            <h5 class="position-absolute white-t grassetto" style="bottom:5%">- Aziende Commerciali e di Servizi</h5>
                         </div>
                     </div>
                     </a>
                 </div>
                 
                 
                 <div class="col-12 col-md-4 lazy" data-src="img/settori/FLORENCE-ONE_gruppi-internazionali.jpg" style="background-size:cover">
                 <a href="case?sett=6">
                     <div class="row no-gutters">
                         <div class="col-12 position-relative settori">
                            <h5 class="position-absolute white-t grassetto" style="bottom:5%">- Gruppi Internazionali</h5>
                         </div>
                     </div>
                 </a>
                 </div>
                 
             
                 <div class="col-12 col-md-4 lazy"  data-src="img/settori/FLORENCE-ONE_grande-distribuzione-organizzata.jpg" style="background-size:cover">
                     <a href="case?sett=7">
                     <div class="row no-gutters">
                         <div class="col-12 position-relative settori">
                            <h5 class="position-absolute white-t grassetto" style="bottom:5%">- Grande Distribuzione Organizzata</h5>
                         </div>
                     </div>
                     </a>
                 </div>
                 
                 <div class="col-12 col-md-4 lazy" data-src="img/settori/FLORENCE-ONE_pelletteria-tessile.jpg" style="background-size:cover">
                     <a href="case?sett=8">
                     <div class="row no-gutters">
                         <div class="col-12 position-relative settori">
                            <h5 class="position-absolute white-t grassetto" style="bottom:5%">- Pelletteria e Tessile</h5>
                         </div>
                     </div>
                     </a>
                 </div>
                 
                 <div class="col-12 col-md-4 lazy" data-src="img/settori/settori.jpg" style="background-size:cover;background-position:center;">
                     <a href="/case">
                     <div class="row no-gutters">
                         <div class="col-12 position-relative settori">
                            <h5 class="position-absolute white-t grassetto" style="bottom:5%">- Altro</h5>
                         </div>
                     </div>
                     </a>
                 </div>
                 
            </div>
        </div>
        </div>