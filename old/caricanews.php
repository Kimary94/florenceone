<?php
header ('Content-type: text/html; charset=utf-8');
header ('Content-type: text/html; charset=iso8859-15');
include ("conn.php");

// This function expects the input to be UTF-8 encoded.
function slugify($text)
{
    // Swap out Non "Letters" with a -
    $text = preg_replace('/[^\\pL\d]+/u', '-', $text); 
	$text = str_replace("-39-", "-", $text);
    // Trim out extra -'s
    $text = trim($text, '-');
    // Convert letters that we have left to the closest ASCII representation
    $text = iconv('utf-8', 'us-ascii//TRANSLIT', $text);
    // Make text lowercase
    $text = strtolower($text);
    // Strip out anything we haven't been able to convert
    $text = preg_replace('/[^-\w]+/', '', $text);
    return $text;
}

$sql = "SELECT * FROM news ORDER BY Data DESC LIMIT 6 OFFSET ".(7*$_GET['c']);
$output = "";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    while($row = $result->fetch_assoc()) {
        $img = json_decode($row["img"], true);
    $output .= '<div class="col-12 col-md-4 caricati" style="display:none;background:url(\'backoffice/'.str_replace('\\','/',$img[0]['serverFileName']).'\') no-repeat center center; background-size:cover" data-scroll-watch><a href="/news/'.$row['id'].'/'.slugify($row['titolo']).'"><div class="row no-gutters"><div class="col-12 position-relative case-studies overlay"><h3 class="position-absolute">'.$row['titolo'].'</h3></div></div></a></div>';
    }
}
echo $output;
?>