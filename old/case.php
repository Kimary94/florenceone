<?php
use Melbahja\Seo\Factory;

// Load Composer's autoloader
require 'vendor/autoload.php';

$metatags = Factory::metaTags();

$metatags->meta('author', 'Biznes')
		->meta('title', 'Florence One | Rivenditore SAP Italia - Casi di successo')
		->image('img/slide_casi.jpg')
		->meta('description', 'Florence One ha aiutato piccole e grandi aziende, italiane e globali a migliorare il proprio business con soluzioni SAP e programmi personalizzati. Scopri quali.')
		->url('https://florence-one.it/case')
?>
<!DOCTYPE html>
<html lang="it">
    <head>
        <title>Florence One | Rivenditore SAP Italia - Casi di successo</title>
        <meta name="generator"
        content="HTML Tidy for HTML5 (experimental) for Windows https://github.com/w3c/tidy-html5/tree/c63cc39" />
        <?php include ("head.php"); ?>
        <?php include ("conn.php"); ?>
        <?=$metatags?>
    </head>
    <body>
        <?php include ("menu.php"); ?>
        <!-- banner -->
        <div id="banner-top" class="container-fluid sfondo-big d-flex justify-content-around align-items-center" style="background:url('img/slide_casi.jpg')">
        <div class="container">
            <div class="row text-center testo-banner">
                <div class="col-12">
                    <h1 class="titolo-pagina">Casi di successo</h1>
                    <p>Scopri le aziende che hanno scelto SAP Business One.</p>
                    <p class="pt-3 icona bounce"><i class="fas fa-angle-double-down"></i></p>
                </div>
            </div>
        </div>
        </div>
        <form>
        <div class="container-fluid p-4">
            <div class="row ricerca">   
                <div class="col-12">Filtra per</div>
                <?php include ("settori.php"); ?>
                <div class="col-12 col-md-4"><select name="settore" id="settore" onchange="sett=this.value; showCase(sett,modu,regi)"> <option value="" selected>Settore</option> <?php foreach($settore as $value){ echo '<option value="'.$id_settore[$value].'">'.$value.'</option>';}?></select><i class="fa fa-angle-down"></i></div>
                <!-- 
                <?php include ("moduli.php"); ?>
                <div class="col-12 col-md-4"><select name="moduli"  onchange="modu=this.value; showCase(sett,modu,regi)"> <option value="" selected>Modulo</option> <?php foreach($modulo as $value){ echo '<option value="'.$id_modulo[$value].'">'.$value.'</option>';}?></select><i class="fa fa-angle-down"></i></div>
                <?php include ("regioni.php"); ?>
                <div class="col-12 col-md-4"><select name="regione" onchange="regi=this.value; showCase(sett,modu,regi)"> <option value="" selected>Regione</option> <?php foreach($regione as $value){ echo '<option value="'.$id_regione[$value].'">'.$value.'</option>';}?></select><i class="fa fa-angle-down"></i></div>
                 -->
            </div>
        </div>
        
        
        <!-- sezione bianca -->
        <div class="container-fluid p-0">
            <div id="casi" class="row no-gutters white-t ">
                    <?php
                    $sql = "SELECT * FROM casi_di_successo ORDER BY id DESC";
                    $result = $conn->query($sql);
                    if ($result->num_rows > 0) {
                        while($row = $result->fetch_assoc()) {
                            $img = json_decode($row["foto"], true);
                            echo '<div class="col-12 col-md-4 scroll_block" style="background:url(\'backoffice/'.str_replace('\\','/',$img[0]['serverFileName']).'\') no-repeat center center; background-size:cover"><a href="/case/'.$row['id'].'/'.slugify($row['titolo']).'"><div class="row no-gutters"><div class="col-12 position-relative case-studies overlay"><h3 class="position-absolute">'.$row['titolo'].' - '.htmlentities($row['sottotitolo'], ENT_COMPAT, "iso-8859-1").'</h3></div></div></a></div>';
                        }
                    }
                    ?>
            </div>
        </div>
        
        <!-- Bannerino -->
        <div class="container-fluid py-5 catch sfondo-big" style="background:url('img/2021.jpg')">
        <div class="container">
            <div class="row">
                <div class="col-12"><h3 class="white-t">LEADER NEL <br>MERCATO ERP</h3></div>
            </div>
        </div>
        </div>

        <?php include ("footer.php"); ?>
        <?php
            if(isset($_GET['sett'])){
                echo '<script>
                        setTimeout(function(){ 
                            $("#settore").val('.$_GET['sett'].'); 
                            sett= '.$_GET['sett'].';
                            showCase(sett,modu,regi);
                        }, 500);
                    </script>';
            }
        ?>
    </body>
</html> 