<?php
use Melbahja\Seo\Factory;

// Load Composer's autoloader
require 'vendor/autoload.php';

$metatags = Factory::metaTags();

$metatags->meta('author', 'Biznes')
		->meta('title', 'Florence One - SAP Business One')
		->meta('description', 'Florence One offre consulenza Business e Soluzioni Software per far crescere la tua azienda. Nasce come azienda di Consulenza IT Enterprise e Digital Transformation che fornisce servizi ad alcune delle più grandi aziende italiane e multinazionali. Con oltre 20 anni di esperienza e partnership con multinazionali leader di mercato in tanti settori di riferimento. Scopri di più.')
        ->meta('keywords', 'Rivenditore SAP, partner SAP, gestionale SAP, ERP, SAP, ERP aziendale, Florence One, Rivenditore SAP Toscana, Rivenditore SAP Lucca, Rivenditore SAP Pisa, Rivenditore SAP Livorno, Rivenditore SAP Firenze, Rivenditore SAP Prato, Rivenditore SAP Pistoia, Rivenditore SAP Grosseto, Rivenditore SAP Arezzo')
		->image('img/slide_chi.jpg')
		->url('https://florence-one.it/chi-siamo')
?>
<!DOCTYPE html>
<html lang="it">
    <head>
        <title>Florence One | Rivenditore SAP Italia - Chi siamo</title>
        <meta name="generator"
        content="HTML Tidy for HTML5 (experimental) for Windows https://github.com/w3c/tidy-html5/tree/c63cc39" />
        <?php include ("head.php"); ?>
        <?=$metatags?>
    </head>
    <body>
        <?php include ("menu.php"); ?>
         <!-- banner -->
        <div id="banner-top" class="container-fluid sfondo-big d-flex justify-content-around align-items-center" style="background:url('img/slide_chi.jpg')">
        <div class="container">
            <div class="row text-center testo-banner">
                <div class="col-12">
                    <h1 class="titolo-pagina">Chi siamo</h1>
                    <p>Consulenza Business e Soluzioni Software.</p>
                    <p class="pt-3 icona bounce"><i class="fas fa-angle-double-down"></i></p>
                </div>
            </div>
        </div>
        </div>

        <!-- sezione bianca -->
        <div class="container py-5">
            <div class="row">
                <div class="col-12 col-md-6 pt-2 pb-5">
                    <img src="img/florence1-about.jpg" alt="Florence One gruppo" class="img-fluid"  />
                </div>
                <div class="col-12 col-md-6 py-2">
                    <h4 class="pb-3 grassetto">Consulenza Business e Soluzioni Software<br>per far Crescere la tua Azienda</h5>
                    <p>Florence One nasce come azienda di Consulenza IT Enterprise e Digital Transformation che fornisce servizi ad alcune delle più grandi aziende italiane e multinazionali. Oltre 20 anni di esperienza, partnership con multinazionali leader di mercato in tanti settori di riferimento, conoscenze approfondite per fornire soluzioni e servizi della massima qualità, utilizzati dalle più grandi aziende a livello globale come Intesa San Paolo, Spotify, Nike, Netflix, Tesla, AT&T, Coca-Cola, Lufthansa, Pirelli, Snam e Whirlpool, personalizzate su misura per i processi di business delle Piccole e Medie Imprese italiane. </p>
                </div>
            </div>
        </div>
        
        <!-- Bannerino -->
        <div class="container-fluid py-5 catch sfondo-big" style="background:url('img/world.png')">
        <div class="container">
            <div class="row">
                <div class="col-12"><h3 class="white-t">Presente nelle <br>migliori aziende del mondo</h3></div>
            </div>
        </div>
        </div>
        
                <!-- sezione bianca -->
        <div class="container-fluid py-5" style="background:#EDEDED;">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                <h3 class="py-3 grassetto">Un partner concreto</h3>
                </div>
            </div>
            <div class="row mt-4">
               <div class="col-12 py-2">
                    <p>Un aiuto concreto a raggiungere il tuoi Obiettivi. Non ci fermeremo finché non avremo raggiunto i tuoi Risultati.</p>
                    <p>Le soluzioni di Florence One ti permettono di gestire in modo efficiente e centralizzato tutte le aree, i processi di business e le funzioni aziendali. Sistemi di digitalizzazione dei processi, gestione documentale, soluzioni MES, Business Intelligence, portali web e applicazioni mobile personalizzate, business process engineering. Florence One aiuta un numero sempre crescente di Piccole e Medie Imprese in Italia a crescere attraverso soluzioni tecnologiche innovative e leader di mercato.</p>
                </div>
                <div class="col-12 col-md-6 py-2">
                    <h4>Consulenza business</h4>
                    <p>Selezioniamo le migliori soluzioni IT a livello internazionale per far crescere la tua Azienda.</p>
                </div>
                <div class="col-12 col-md-6 py-2">
                    <h4>Tecnologia & innovazione</h4>
                    <p>Aiutiamo la tua Azienda nella scelta della migliore soluzione software per le tue esigenze.</p>
                </div>
                <div class="col-12 col-md-6 py-2">
                    <h4>Soluzioni personalizzate</h4>
                    <p>Soluzioni personalizzate fino al minimo dettaglio, per riflettere i processi unici della tua Azienda.</p>
                </div>
                <div class="col-12 col-md-6 py-2">
                    <h4>Sempre al tuo fianco</h4>
                    <p>Florence One non ti lascia mai solo, avrai sempre il massimo supporto quando ne avrai bisogno.</p>
                </div>
 
            </div>
            <div class="row mt-4">
                <div class="col-12 text-center">
                    <a href="/servizi"><button type="button" class="btn btn-light mb-5">Esplora</button></a>
                </div>
            </div>
        </div>
        </div>
        
        <?php include ("banner-settori.php"); ?>
        
        <?php include ("demo.php"); ?>
        
        <?php include ("tris.php"); ?>
        

        <?php include ("footer.php"); ?>
        <script>
        $('.lazy').Lazy({
            onError: function(element) {
                console.log('error loading ' + element.data('src'));
            }
        });
        </script>
        
    </body>
</html> 