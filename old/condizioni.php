<?php
use Melbahja\Seo\Factory;

// Load Composer's autoloader
require 'vendor/autoload.php';

$metatags = Factory::metaTags();

$metatags->meta('author', 'Biznes')
		->meta('title', 'Florence One | Rivenditore SAP Italia - Condizioni di fornitura')
		->meta('description', 'Le seguenti condizioni generali di fornitura valgono per tutte le forniture di servizi, merce, progetti ecc.')
        ->meta('keywords', 'SAP Business One, software gestionale, Rivenditore SAP, partner SAP, gestionale SAP, software contabilità, software gestione produzione, software aziendale, ERP, programma contabilità, programma fatture, SAP, ERP aziendale, Florence One')
		->image('img/slide_servizi.jpg')
		->url('https://florence-one.it/condizioni')
?>
<!DOCTYPE html>
<html lang="it">
    <head>
        <title>Florence One | Rivenditore SAP Italia - Promo SAP Business One</title>
        <meta name="generator"
        content="HTML Tidy for HTML5 (experimental) for Windows https://github.com/w3c/tidy-html5/tree/c63cc39" />
        <?php include ("head.php"); ?>
        <?=$metatags?>
    </head>
    <body>
        <?php include ("menu.php"); ?>
        <div class="container">
			<div class="row">
				<div class="col-12 mt-5 py-5">
					<h3>CONDIZIONI GENERALI DI FORNITURA 05-02-2020</h3>
                    <p>Le seguenti condizioni generali di fornitura valgono per tutte le forniture di servizi, merce, progetti ecc., di cui all’oggetto sociale, comprese quelle future, pure se nei singoli casi non si fa esplicito riferimento alle stesse. <br>Condizioni di acquisto del committente diverse, non sono impegnative per Florence One Srl se non accettate per iscritto di caso in caso. Al momento dell’ordine o successivamente alla ricezione della merce o al pagamento anche di parte di una commessa, queste condizioni generali di fornitura si intendono accettate dall'acquirente. La validità di singole condizioni non pregiudica la validità delle rimanenti.</p>
                    <h4>FATTURAZIONE</h4>
                    <p>Le modalità di fatturazione sono dettagliate nell’offerta di Florence One Srl.</p>
                    <h4>CARATTERISTICHE TECNICHE</h4>
                    <p>Le caratteristiche tecniche di eventuali prodotti forniti da Florence One Srl in abbinamento ai propri servizi sono quelle comunicate dai singoli produttori. Florence One Srl non assume, quindi, responsabilità alcuna per eventuali inesattezze e/o non corrispondenza delle stesse a quanto comunicato. Il servizio offerto vuole essere esclusivamente un aiuto ai propri Clienti, e non intende sostituirsi in alcun modo a quanto i produttori comunicano in via ufficiale al mercato circa caratteristiche tecniche, funzionali e dimensionali dei loro prodotti hardware e software.</p>
                    <h4>ORDINI</h4>
                    <p>Gli ordini sono accettati solo ed esclusivamente in forma scritta, eventualmente tramite Posta elettronica certificata.</p>
                    <h4>PREZZI</h4>
                    <p>I prezzi (IVA esclusa) sono quelli che emergono dall’offerta di di Florence One Srl e si riferiscono alla fornitura di quanto descritto nelle specifiche di progetto. Oltre all’assistenza a chiamata che verrà fatturata a consuntivo, eventuali attività aggiuntive, non previste in sede di offerta, saranno oggetto di una nuova quotazione da parte di Florence One Srl.</p>
                    <h4>PAGAMENTI</h4>
                    <p>Le forniture dovranno essere pagate mezzo Ri.ba. a 30 gg data fattura fine mese, salvo diverse condizioni che dovranno essere concordate per scritto con Florence One Srl.. <br>Florence One Srl si riserva, a suo insindacabile giudizio, il diritto di non procedere alla conclusione della fornitura, o di non aprirne di nuove, in caso di mancati o ritardati pagamenti. <br>Resta altresı̀ inteso che, in tali casi, saranno dovuti a Florence One Srl i pagamenti delle prestazioni svolte fino a quel momento. Il cliente non è autorizzato a trattenere o ad addebitare pagamenti ad eccezione di casi verificati o accertati legalmente. Il ritardato pagamento darà luogo alla decorrenza degli interessi passivi sulla base del prime rate maggiorato di cinque punti percentuali, dopo otto giorni dalla data della scadenza. Le spese di insoluto, bancarie e cambiarie, sono a carico del cliente. Sono fatti salvi, in ogni caso, gli eventuali maggiori danni. <br>La fornitura è effettuata con la clausola di "riservato dominio" a favore del venditore (art. 1523 	e seguenti del C.C.) fino all'effettivo completo pagamento o all'andata a buon fine di effetti eventualmente accettati. Mancando questo, o venendo ritardato, Florence One Srl si riserva di annullare le forniture in corso o da eseguire, nonché di riprendersi la merce o la proprietà di progetti e di far suoi a titolo di penale gli acconti già incassati. Modifiche del diritto di "riservato dominio" possono avvenire solo per accordo scritto.</p>
                    <h4>TEMPI DI CONSEGNA ED EVENTUALI RITARDI</h4>
                    <p>I tempi di consegna, se previsti, sono quelli che emergono dall’offerta, o dallo studio di fattibilità, o dalle specifiche di progetto e decorrono esclusivamente dalla data di accettazione dell’ordine comunicata al cliente da parte di Florence One Srl. <br>Tuttavia, il cliente è consapevole che, in forniture di questa tipologia, le dipendenze esterne ed interne sono molteplici e non interamente preventivabili e dunque i suddetti tempi di consegna potranno subire delle variazioni, senza che ciò comporti conseguenza alcuna per Florence One Srl. <br>I termini di consegna indicati da Florence One Srl non potranno, pertanto, dar luogo ad annullamenti di ordini se non concordati per iscritto.</p>
                    <h4>PENALI</h4>
                    <p>Florence One Srl: <br>1- Non accetta nessun tipo di penale se non concordata esplicitamente e di volta in volta sulle singole componenti della fornitura <br>2- Non si assume mai responsabilità sulle componenti o i prodotti realizzati da terze parti e, in ogni caso, saranno valide le attenuanti derivanti da caso fortuito e/o forza maggiore.</p>
                    <h4>TRATTAMENTO DEI DATI PERSONALI</h4>
                    <p>Ai sensi del Decreto Legislativo 30 giugno 2003 n.196, il cliente dà atto che i “dati personali” comunicati e/o scambiati, anche in fase di informative precontrattuali, formeranno oggetto di trattamento ai sensi, per gli effetti e con le finalità di cui all’art. 24, comma 1, lettere b), c), d) del D.Lgs. n.196/2003 e successive modificazioni ed integrazioni. Resta, inoltre, inteso che il cliente espressamente acconsente al trasferimento dei “dati personali” ai sensi e per gli effetti dell’art. 42 e dell’art.43 comma 1 lettera a) del D.Lgs. n.196/2003 e, comunque, alla loro comunicazione e diffusione ai sensi e per gli effetti dell’art. 25, comma 1 del citato decreto.</p>
                    <h4>CONTROVERSIE</h4>
                    <p>Ogni controversia relativa al presente contratto sarà di esclusiva competenza del Foro di Firenze. Per tutto quanto non espressamente previsto nel presente contratto, si rinvia alle disposizioni di cui al Codice Civile, nonché alle altre norme giuridiche inderogabili aventi efficacia di legge.</p>
                    <h4>VARIAZIONE ALLE CONDIZIONI DI FORNITURA</h4>
                    <p>Le condizioni contenute nel presente documento potranno essere modificate, senza preavviso alcuno e avranno validità dalla data di pubblicazione.</p>
				</div>
			</div>
		</div>		

        <?php include ("footer.php"); ?>
    </body>
</html> 