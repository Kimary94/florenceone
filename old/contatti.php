<?php
use Melbahja\Seo\Factory;

// Load Composer's autoloader
require 'vendor/autoload.php';

$metatags = Factory::metaTags();

$metatags->meta('author', 'Biznes')
		->meta('title', 'Florence One | Rivenditore SAP Italia - Contatti')
		->meta('description', 'Contatta Florence One, il nostro personale sarà a tua disposizione per ogni esigenza e per soddisfare le richieste sia di natura commerciale che tecnica. Contattaci!')
		->image('img/slide_contatti.jpg')
		->url('https://florence-one.it/contatti')
?>
<!DOCTYPE html>
<html lang="it">
    <head>
        <title>Florence One | Rivenditore SAP Italia - Contatti</title>
        <meta name="generator"
        content="HTML Tidy for HTML5 (experimental) for Windows https://github.com/w3c/tidy-html5/tree/c63cc39" />
        <?php include ("head.php"); ?>
        <?=$metatags?>
    </head>
    <body>
        <?php include ("menu.php"); ?>
        <!-- banner -->
        <div id="banner-top" class="container-fluid sfondo-big d-flex justify-content-around align-items-center" style="background:url('img/slide_contatti.jpg')">
        <div class="container">
            <div class="row text-center testo-banner">
                <div class="col-12">
                    <h2 class="titolo-pagina">Contatti</h2>
                    <p>Contattaci il nostro personale sarà a tua disposizione per ogni esigenza e<br> per soddisfare le richieste sia di natura commerciale che tecnica.</p>
                    <p class="pt-3 icona bounce"><i class="fas fa-angle-double-down"></i></p>
                </div>
            </div>
        </div>
        </div>

        <!-- sezione bianca -->
        <div class="container py-5">
            <div class="row">
                <div class="col-12 pb-5"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2873.3644453136762!2d10.93487706686674!3d43.93113068955637!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x132a8b754c845ec3%3A0x656faa7335bf69e!2sVia%20Umberto%20Saba%2C%2026%2C%2051100%20Pistoia%20PT!5e0!3m2!1sit!2sit!4v1602766141343!5m2!1sit!2sit" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe></div>
                
                <div class="col-12 col-md-4">
                    <h4 class="pb-3 border-bottom">La nostra sede</h4>
                    <p>
                         via Umberto Saba, 42<br>
                         51100 Pistoia PT
                    </p>
                </div>
                <div class="col-12 col-md-4">
                    <h4 class="pb-3 border-bottom">Contact info</h4>
                    <p>
                        sales@florence-one.it<br>
                        +39 0573 1935507
                    </p>
                </div>
                <div class="col-12 col-md-4">
                    <h4>Scrivici!</h4>
                    <p>Risponderemo ad ogni tua richiesta</p>
                    <form>
                        <div class="row">
                        <input class="col-12 col-md-12 my-1 py-2" id="form_nome" name="nome" placeholder="Nome e cognome">
                        <input class="col-12 col-md-12 my-1 py-2" id="form_email" name="email" placeholder="Email">
                        <input class="col-12 col-md-12 my-1 py-2" id="form_azienda" name="azienda" placeholder="Azienda">
                        <input class="col-12 col-md-12 my-1 py-2" id="form_ruolo" name="ruolo" placeholder="Ruolo">
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input type="checkbox" class="custom-control-input" id="form_privacy">
                            <label class="custom-control-label" for="form_privacy">
                                Dichiaro di aver letto l'<a target="_blank" href="https://www.iubenda.com/privacy-policy/19926816">informativa sulla privacy</a> ai sensi del GDPR e do il consenso al trattamento dei dati personali.</div>
                            </label>
                        </div>
                        <button type="button" class="btn btn-primary my-2" onclick="gmail()">Richiedi una demo</button>
                        <p id="form" class="successo col-12 alert alert-success" style="display:none">Invio riuscito</p>
                        <p id="form" class="errore col-11 alert alert-danger" style="display:none; color:red">Tutti i campi sono obbligatori.</p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
        
        <!-- Bannerino -->
        <div class="container-fluid py-5 catch sfondo-big" style="background:url('img/world.png')">
        <div class="container">
            <div class="row">
                <div class="col-12"><h3 class="white-t">Presente nelle <br>migliori aziende del mondo</h3></div>
            </div>
        </div>
        </div>
        
        
        <?php include ("tris.php"); ?>
        

        <?php include ("footer.php"); ?>
        <script>
    
            function gmail(){
        var sub = "Nuovo contatto da Florence-one.it"
        var email = "sales@florence-one.it";
        var body = "E\' stato richiesto un contatto sul sito da:<br> " + $('#form_nome').val() + "<br>" + $('#form_ruolo').val() + " dell\'azienda " + $('#form_azienda').val() + "<br>Puo\' essere contattato all\'email " + $('#form_email').val(); 
        var nome_c = $('#form_nome').val();
        var email_c = $('#form_email').val();
                
        if(($('#form_nome').val() != '') && ($('#form_email').val() != '') && ($('#form_azienda').val() != '') && ($('#form_ruolo').val() != '') && !$("#form_privacy").is(":not(:checked)")){
            $.ajax({
                async: false,
                type: "POST",
                url: "email_parsing.php",
                data: {
                    "sub": sub,
                    "email": email,
                    "body": body
                },
                success: function () {
                    $.ajax({
                        async: false,
                        type: "POST",
                        url: "email_gmail.php",
                        data: {
                            "nome": nome_c,
                            "email": email_c
                        },
                        success: function () {
                            $('#form.successo').show(200);
                        }
                    });
                }
            });
        }
        else{
            $('#form.errore').show(200);
        }
    }
            
        </script>
    </body>
</html> 