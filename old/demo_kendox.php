<!-- demo -->
<div id="demo" class="container-fluid py-5" style="background:#EDEDED;">
<div class="container">
    <div class="row mb-4">
        <div class="col-12 text-center pb-5">
            <h3 class="py-3 grassetto">Richiedi subito una demo</h3>
                <p>Scopri le caratteristiche del nostro ECM, siamo pronti a valutarlo insieme a te.<br>Vuoi una dimostrazione di tutte le potenzialità offerte da Kendox InfoShare?</p>
        </div>
        <div class="col-12 col-md-6 offset-md-3">
            <h4>Scrivici!</h4>
            <p>Risponderemo ad ogni tua richiesta</p>
            <form>
                <div class="row">
                <input class="col-12 col-md-11 my-1 py-2" id="form_nome" name="nome" placeholder="Nome e cognome">
                <input class="col-12 col-md-11 my-1 py-2" id="form_email" name="email" placeholder="Email">
                <input class="col-12 col-md-11 my-1 py-2" id="form_azienda" name="azienda" placeholder="Azienda">
                <input class="col-12 col-md-11 my-1 py-2" id="form_ruolo" name="ruolo" placeholder="Ruolo">
                <div class="custom-control custom-checkbox custom-control-inline">
                    <input type="checkbox" class="custom-control-input" id="form_privacy">
                    <label class="custom-control-label" for="form_privacy">
                        Dichiaro di aver letto l'<a target="_blank" href="https://www.iubenda.com/privacy-policy/19926816">informativa sulla privacy</a> ai sensi del GDPR e do il consenso al trattamento dei dati personali.</div>
                    </label>
                </div>
                <button type="button" class="btn btn-primary my-2" onclick="gmail()" >Richiedi una demo</button>
                <p id="form" class="successo col-11 alert alert-success" style="display:none">Invio riuscito</p>
                <p id="form" class="errore col-11 alert alert-success" style="display:none; color:red">Tutti i campi sono obbligatori.</p>
                </div>
            </form>
        </div>
    </div>
</div>
</div>

<script>
    
function gmail(){
        var sub = "Nuovo richiesta demo Kendox da Florence-one.it"
        var email = "sales@florence-one.it";
        var body = "E\' stata richiesta una demo Kendox da:<br> " + $('#form_nome').val() + "<br>" + $('#form_ruolo').val() + " dell\'azienda " + $('#form_azienda').val() + "<br>Puo\' essere contattato all\'email " + $('#form_email').val(); 
        var nome_c = $('#form_nome').val();
        var email_c = $('#form_email').val();
                
        if(($('#form_nome').val() != '') && ($('#form_email').val() != '') && ($('#form_azienda').val() != '') && ($('#form_ruolo').val() != '') && !$("#form_privacy").is(":not(:checked)")){
            $.ajax({
                async: false,
                type: "POST",
                url: "email_parsing.php",
                data: {
                    "sub": sub,
                    "email": email,
                    "body": body
                },
                success: function () {
                    $.ajax({
                        async: false,
                        type: "POST",
                        url: "email_gmail.php",
                        data: {
                            "nome": nome_c,
                            "email": email_c
                        },
                        success: function () {
                            $('#form.successo').show(200);
                        }
                    });
                }
            });
        }
        else{
            $('#form.errore').show(200);
        }
    }
    
    
</script>