<head>
<meta name="robots" content="noindex">
</head>
<?php
    
switch ($_POST['dipendenti']) {
    case 1:
        $dipendenti = "1-5";
        break;
    case 2:
        $dipendenti = "5-50";
        break;
    case 3:
        $dipendenti = "50-100";
        break;
    case 4:
        $dipendenti = "100-250";
        break;
    case 5:
        $dipendenti = "più di 250";
        break;
}
switch ($_POST['fatturato']) {
    case 1:
        $fatturato = "<1m";
        break;
    case 2:
        $fatturato = "1m-10m";
        break;
    case 3:
        $fatturato = "10m-100m";
        break;
    case 4:
        $fatturato = "100m-250m";
        break;
    case 5:
        $fatturato = ">250m";
        break;
}
$software1 = "Software finanziario e di contabilita': ";
if($_POST['contabilita'])
    $software1 .= "contabilita', ";
if($_POST['fatturazione'])
    $software1 .= "fatturazione, ";
if($_POST['paghe'])
    $software1 .= "paghe, ";
if($_POST['banche'])
    $software1 .= "banche, ";
if($_POST['pagamenti'])
    $software1 .= "pagamenti, ";
if($_POST['excel'])
    $software1 .= "excel, ";

$software2 = "Altri software: ";
if($_POST['finanza'])
    $software2 .= "finanza, ";
if($_POST['crm'])
    $software2 .= "crm, ";
if($_POST['vendite'])
    $software2 .= "vendite, ";
if($_POST['ecommerce'])
    $software2 .= "ecommerce, ";
if($_POST['software_altro'])
    $software2 .= "software_altro, ";
    
switch ($_POST['n_controllate']) {
    case 1:
        $n_controllate = "0";
        break;
    case 2:
        $n_controllate = "1";
        break;
    case 3:
        $n_controllate = "2";
        break;
    case 4:
        $n_controllate = "3m";
        break;
    case 5:
        $n_controllate = "Piu' di 4";
        break;
}
    
$settore = "In quale settore opera l'azienda: "; 
if($_POST['automotive'])
    $settore .= "automotive, ";
if($_POST['agroalimentare'])
    $settore .= "agroalimentare, ";
if($_POST['consumo'])
    $settore .= "consumo, ";
if($_POST['istruzione'])
    $settore .= "istruzione, ";
if($_POST['food'])
    $settore .= "food, ";
if($_POST['manifattura'])
    $settore .= "manifattura, ";
if($_POST['terzo'])
    $settore .= "terzo, ";
if($_POST['professionali'])
    $settore .= "professionali, ";
if($_POST['commercio'])
    $settore .= "commercio, ";
if($_POST['distribuzione'])
    $settore .= "distribuzione, ";
if($_POST['settore_altro'])
    $settore .= "settore_altro, ";

        
    $email_html = 'E\' stata richiesta valutazione da:<br>
    '.$_POST['nome'].' dell\'azienda '.$_POST['azienda'].'<br>
    Puo\' essere contattato all\'email '.$_POST['email'].'<br><br>
    Numero dipendenti: '.$dipendenti.'<br>
    Fatturato annuo: '.$fatturato.'<br>
    '.substr($software1, 0, -2).'<br>
    '.substr($software2, 0, -2).'<br>
    Team informatico: '.$_POST['team'].'<br>
    Database del cliente separato: '.$_POST['database'].'<br>
    Controllate: '.$_POST['controllate'].'<br>
    Numero di controllate: '.$n_controllate.'<br>
    Nello stesso paese della capogruppo: '.$_POST['paese'].'<br>
    Diponibilita\' di report: '.$_POST['report'].'<br>
    '.substr($settore, 0, -2);
    
    $pars=array(
        'sub' => "E' stata richiesta una valutazione personalizzata su Florence-One.it",
        'body' => $email_html,
        'email' => "sales@florence-one.it",
    );

    //step1
    $curlSES=curl_init(); 
    //step2
    curl_setopt($curlSES,CURLOPT_URL,"http://biznesweb.online/email_function.php");
    curl_setopt($curlSES,CURLOPT_RETURNTRANSFER,true);
    curl_setopt($curlSES,CURLOPT_HEADER, false); 
    curl_setopt($curlSES, CURLOPT_POST, true);
    curl_setopt($curlSES, CURLOPT_POSTFIELDS,$pars);
    curl_setopt($curlSES, CURLOPT_CONNECTTIMEOUT,10);
    curl_setopt($curlSES, CURLOPT_TIMEOUT,30);
    //step3
    $result=curl_exec($curlSES);
    //step4
    curl_close($curlSES);
    //step5
    return $result;
?>