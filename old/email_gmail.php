<?php
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\OAuth;
use League\OAuth2\Client\Provider\Google;
date_default_timezone_set('Etc/UTC');
require 'vendor/autoload.php';
    
$email_html = '
<img src="https://florence-one.it/email_header.png" alt="" class="img-fluid" /><br><br>
Gent.mo Sig. '.$_POST['nome'].',<br><br>
mi chiamo Liguori Debora e faccio parte del team di Florence One.<br>
La contatto in merito alla sua cortese richiesta di informazioni e le comunico che è stata presa in carico dai nostri operatori.<br>
Riceverà un nostro contatto nel più breve tempo possibile. <br><br>
Cordiali saluti,<br>
<strong>Debora Liguori</strong><br>
Marketing Assistant<br>
<img src="https://florence-one.it/email_logo.png" alt="" class="img-fluid" />
<br>
<strong>Phone:</strong> +39 0573 1935507<br>
<strong>Email:</strong> dliguori@florence-one.it<br>
<strong>Web:</strong> www.florence-one.it<br>
<br>
Via Umberto Saba, 42 – 51100 Pistoia (PT)<br><br>
<small>The information contained in this transmittal, including any attachments hereto, are confidential and privileged, and intended solely for the specified addressee(s). This e-mail has a confidential nature which is protected by the Italian law. Moreover, the recipient(s) may not disclose, forward, or copy this e-mail or attachments, or any portion thereof, or permit the use of this information, by anyone not entitled to it, or in a way that may be damaging to the sender. If you are not the intended addressee, or if you receive this message by error, please notify the sender and delete this information from your computer. The statements and opinions expressed in this e-mail message are those of the author of the message and do not necessarily represent those of Florence One Srl. Besides, The contents of this message shall be understood as neither given nor endorsed by Florence One Srl. Florence One Srl does not accept liability for corruption, interception or amendment, if any, or the consequences thereof.<small>
';

$mail = new PHPMailer(true);
    
$mail->isSMTP();
$mail->SMTPDebug = 4;
$mail->Host = 'smtp.gmail.com';
$mail->Port = 587;
$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
$mail->SMTPAuth = true;
$mail->AuthType = 'XOAUTH2';
$email = 'dliguori@florence-one.it';
$clientId = '299375598841-jn35gm5c8cd4hpm1v1nsr69onvm72h1r.apps.googleusercontent.com';
$clientSecret = 'LoXUPU76RoqG3BJxRCjKXAQB';
$refreshToken = '1//09ZvB8j8cd5nOCgYIARAAGAkSNwF-L9IrKggvvlDam4tfL41sCwtcU8PLkPy73jb7CZrI_KfR_EYjMJ0s1sF5VsuS7MDUX6uuk8A';
$provider = new Google(
    [
        'clientId' => $clientId,
        'clientSecret' => $clientSecret,
    ]
);

$mail->setOAuth(
    new OAuth(
        [
            'provider' => $provider,
            'clientId' => $clientId,
            'clientSecret' => $clientSecret,
            'refreshToken' => $refreshToken,
            'userName' => $email,
        ]
    )
);
$mail->isHTML(true);
$mail->setFrom('dliguori@florence-one.it', 'Florence One Marketing Assistant ');
$mail->addAddress( $_POST['email'] );
$mail->Subject = "Richiesta di informazioni Florence One";
$mail->CharSet = PHPMailer::CHARSET_UTF8;
$mail->Body    = $email_html;

$response = array();
if (!$mail->send()) {
    $response['risposta'] = "KO";
    return $response;
} else {
    $response['risposta'] = "OK";
    return $response;
}
?>
