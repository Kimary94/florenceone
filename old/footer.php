        
        <!-- footer -->
        <div id="footer" class="container-fluid white-t" style="background:#2A2E31;">
        <div class="container">
            <div class="row py-5">
                <div class="col-12 col-md-3">
                    <a href="/"><h3 class="logo bianco">Florence One</h3></a><br><br>
                    <p class="white-t">Le migliori soluzioni tecnologiche Enterprise internazionali, pensate per PMI in crescita che vogliono semplificare e rendere più efficienti tutti i processi di business.</p>
                </div>
                <div class="col-12 col-md-3 offset-md-3">
                    <h4 class="pb-3 border-bottom">La nostra sede</h4>
                    <p class="white-t">
                         via Umberto Saba, 42<br>
                         51100 Pistoia PT
                    </p>
                </div>
                <div class="col-12 col-md-3">
                    <h4 class="pb-3 border-bottom">Contact info</h4>
                    <p class="white-t">
                        sales@florence-one.it<br>
                        +39 0573 1935507
                    </p>
                </div>
            </div>
        </div>
        <div class="row white-t pt-2"  style="background:black;">
        <div class="container">
            <div class="row">
                <div class="col"><p class="white-t">© 2020 Florence One S.r.l., via Umberto Saba, 42 51100 Pistoia PT , tel. 0573 1935507, P.Iva e C.f. 06754610480, Capitale sociale: € 10.000,00, Registro Imprese Camera di Commercio di Pistoia numero 06754610480, Rea: PT-197703, PEC: florenceone@pec.net | <a href="/condizioni">Condizioni generali di fornitura</a> | <a href="/eula">Contratto di licenza per utente finale ("EULA")</a> | <a href="https://www.iubenda.com/privacy-policy/19926816" target="_blank">Privacy policy</a> | <a href="https://www.iubenda.com/privacy-policy/19926816/cookie-policy" target="_blank">Cookie Policy</a></p></div>
            </div>
        </div>
        </div>

<script src="https://unpkg.com/vanilla-back-to-top@7.2.1/dist/vanilla-back-to-top.min.js"></script>
<!-- JavaScript -->
<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
<script>
jQuery.event.special.touchstart = {
  setup: function( _, ns, handle ){
    if ( ns.includes("noPreventDefault") ) {
      this.addEventListener("touchstart", handle, { passive: false });
    } else {
      this.addEventListener("touchstart", handle, { passive: true });
    }
  }
}
</script>
<script async src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script async src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script async type="text/javascript" src="/assets/js/jquery.slicknav.js"></script>
<script async type="text/javascript" src="/assets/js/biznes.js"></script>

<!-- Lazy load -->
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery.lazy/1.7.9/jquery.lazy.min.js"></script>
<script>addBackToTop({
  diameter: 40,
  backgroundColor: '#29abe2',
  textColor: '#fff'
})</script>

<script>
$(function(){
$('#menu').slicknav();
});
</script>

<script type="text/javascript">
var _iub = _iub || [];
_iub.csConfiguration = {"countryDetection":true,"consentOnContinuedBrowsing":false,"whitelabel":false,"lang":"it","siteId":2138144,"floatingPreferencesButtonDisplay":false,"cookiePolicyId":19926816, "banner":{ "brandBackgroundColor":"none","brandTextColor":"black","logo":"https://florence-one.it/img/Logo_Florence-One_White.png","acceptButtonDisplay":true,"customizeButtonDisplay":true,"acceptButtonColor":"#0073CE","acceptButtonCaptionColor":"white","customizeButtonColor":"#DADADA","customizeButtonCaptionColor":"#4D4D4D","rejectButtonColor":"#0073CE","rejectButtonCaptionColor":"white","position":"float-bottom-center","textColor":"#ffffff","backgroundColor":"#252424" }};
</script>
<script type="text/javascript" src="//cdn.iubenda.com/cs/iubenda_cs.js" charset="UTF-8" async></script>
