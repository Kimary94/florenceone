<?php
header ('Content-type: text/html; charset=utf-8');
header ('Content-type: text/html; charset=iso8859-15');
function safe_url($str) {
$str = htmlentities($str, ENT_COMPAT, "iso-8859-1");
$str = str_replace(' ', '-', $str);
$str = preg_replace('/&([a-zA-Z])(uml|acute|grave|circ|tilde);/','$1',$str);
return html_entity_decode($str);
}

$s = intval($_GET['s']);
$m = intval($_GET['m']);
$r = intval($_GET['r']);

include ("conn.php");
if($s != "") $s = "AND settore.id=".$s;
else $s ="AND 1=1";
if($m != "") $m = "AND moduli.id=".$m;
else $m ="AND 1=1";
if($r != "") $r = "AND regione.id=".$r;
else $r ="AND 1=1";


$sql="
SELECT casi_di_successo.titolo, casi_di_successo.sottotitolo, casi_di_successo.id, casi_di_successo.foto FROM casi_di_successo 
INNER JOIN settore ON casi_di_successo.id_settore=settore.id 
INNER JOIN moduli ON casi_di_successo.id_moduli=moduli.id OR casi_di_successo.id_moduli1=moduli.id OR casi_di_successo.id_moduli2=moduli.id OR casi_di_successo.id_moduli3=moduli.id OR casi_di_successo.id_moduli4=moduli.id 
INNER JOIN regione ON casi_di_successo.id_regione=regione.id 
WHERE casi_di_successo.id>0 ".$s." ".$m." ".$r." GROUP BY id" ;

$result = mysqli_query($conn,$sql);

while($row = mysqli_fetch_array($result)) {
  $img = json_decode($row["foto"], true);
  /* echo '
  <div class="col-12 col-md-4 position-relative case-studies" style="background:url(\'backoffice/'.str_replace('\\','/',$img[0]['serverFileName']).'\')">
    <a href="#">
        <h3 class="position-absolute">'.$row['titolo'].'</h3>
    </a>
</div>'; */
  echo '
  <div class="col-12 col-md-4" style="background:url(\'backoffice/'.str_replace('\\','/',$img[0]['serverFileName']).'\') no-repeat center center; background-size:cover">
        <a href="/case/'.$row['id'].'/'.safe_url($row['titolo']).'">
            <div class="row no-gutters">
                <div class="col-12 position-relative case-studies overlay">
                    <h3 class="position-absolute">'.$row['titolo'].' - '.$row['sottotitolo'].'</h3>
                </div>
            </div>
        </a>
    </div>';
}
mysqli_close($conn);
?>