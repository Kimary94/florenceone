<?php
use Melbahja\Seo\Factory;

// Load Composer's autoloader
require 'vendor/autoload.php';

$metatags = Factory::metaTags();

$metatags->meta('author', 'Biznes')
		->meta('title', 'Florence One | Rivenditore SAP Italia - Homepage')
		->meta('description', 'Richiedi gratuitamente una demo SAP Business One, il gestionale per la semplificazione dei tuoi processi aziendali. Scopri i casi di successo e tutte le news su come gestire al meglio la tua azienda e i suoi processi produttivi. Florence One, partner SAP Italia e Rivenditore autorizzato. Scopri di più.')
        ->meta('keywords', 'SAP business one, software gestionale, Rivenditore SAP, partner SAP, gestionale SAP, software contabilità, software gestione produzione, software aziendale, ERP, programma contabilità, programma fatture, SAP, ERP aziendale, Florence One')
		->image('img/slide_home.jpg')
		->url('https://florence-one.it')
?>
<!DOCTYPE html>
<html lang="it">
    <head>
        <title>Florence One | Rivenditore SAP Italia - Homepage</title>
        <meta name="generator"
        content="HTML Tidy for HTML5 (experimental) for Windows https://github.com/w3c/tidy-html5/tree/c63cc39" />
        <?php 
        include ("head.php");
        include ("conn.php");              
        ?>
        <?=$metatags?>
    </head>
    <body>
        
        <?php include ("menu.php");?>
        
        <!-- SLIDE -->
        <div class="slides">
        
        <?php
        $sql = "SELECT * FROM slides WHERE attiva = 1 ORDER BY ordine";
        $result = $conn->query($sql);
        if ($result->num_rows > 0) {
            while($row = $result->fetch_assoc()) {
                $sfondo = json_decode($row["img_sfondo"], true);
                $sfondo = '/backoffice/'.$sfondo[0]['serverFileName'];
                $logo = json_decode($row["logo"], true);
				$logo = 'backoffice/'.str_replace('uploads/','uploads/resized/',str_replace('\\','/',$logo[0]['serverFileName'])).'?h=100';
                //$logo = '/backoffice/'.$logo[0]['serverFileName'];
                ?>
<!-- banner -->
<div id="banner-top" class="container-fluid sfondo-big d-flex justify-content-around align-items-center" style="background:url('<?=$sfondo?>')">
<div class="container">
    <div class="row text-center testo-banner">
        <div class="col-12">
            <div class="row pt-4">
            <div class="col-12 col-md-4 offset-md-4">
            <img src="<?=$logo?>" alt="Logo <?=utf8_encode($row['titolo'])?>" class="img-fluid" />
            </div>
            </div>
            <h2 class="pt-5"><?=utf8_encode($row['titolo'])?></h2>
            <p class="spaziatura col-md-6 offset-md-3"><?=utf8_encode($row['sottotitolo'])?></p>
            <div class="row pt-4">
                <div class="col-12 col-md-6 text-md-right "><a href="<?=$row['p_1_link']?>"><button type="button" class="btn btn-outline-dark "><?=$row['p_1_testo']?></button></a></div>
                <div class="col-12 col-md-6 text-md-left mt-3 mt-md-0"><a href="<?=$row['p_2_link']?>"><button type="button" class="btn btn-primary"><?=$row['p_2_testo']?></button></a></div>
            </div>
            <p class="pt-5 icona bounce"><i class="fas fa-angle-double-down"></i></p>
        </div>
    </div>
</div>
</div>  
            <?php
            }
        }
        ?>

        </div>
        <!-- SLIDE -->
        
        <?php include ("tris.php"); ?>
        
        <!-- sezione bianca -->
        <div class="container py-5" style="background:url('img/bg_testa.jpg') no-repeat bottom center; padding-bottom:15rem !important;">
            <div class="row">
                <div class="col-12 text-center">
                <h3 class="py-3 grassetto">Florence One al fianco delle PMI</h3>
                    <p>Le migliori soluzioni tecnologiche Enterprise internazionali,<br>pensate per PMI in crescita
                        che vogliono semplificare e rendere pi&#249; efficienti tutti i processi di business.</p>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-12 col-md-6 py-2">
                    <h4>Consulenza business</h4>
                    <p>Selezioniamo le migliori soluzioni IT a livello internazionale per far crescere la tua Azienda.</p>
                </div>
                <div class="col-12 col-md-6 py-2">
                    <h4>Tecnologia & innovazione</h4>
                    <p>Aiutiamo la tua Azienda nella scelta della migliore soluzione software per le tue esigenze.</p>
                </div>
                <div class="col-12 col-md-6 py-2">
                    <h4>Soluzioni personalizzate</h4>
                    <p>Soluzioni personalizzate fino al minimo dettaglio, per riflettere i processi unici della tua Azienda.</p>
                </div>
                <div class="col-12 col-md-6 py-2">
                    <h4>Sempre al tuo fianco</h4>
                    <p>Florence One non ti lascia mai solo, avrai sempre il massimo supporto quando ne avrai bisogno.</p>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-12 text-center">
                    <a href="/chi-siamo"><button type="button" class="btn btn-light mb-5">Esplora</button></a>
                </div>
            </div>
        </div>
        
        <!-- news home -->
        <div class="container-fluid py-5" style="background:black;">
        <div class="container">
            <div class="row white-t">
                <div class="col-12 text-center">
                <h3 class="py-3 grassetto">News</h3>
                    <p class="white-t">Florence One aggiunge continuamente nuove soluzioni tecnologiche innovative.<br>Per rimanere sempre aggiornato segui le nostre news.</p>
                </div>
                <?php
                $sql = "SELECT * FROM news WHERE attivo = 1 ORDER BY data DESC LIMIT 3";
                $result = $conn->query($sql);
                if ($result->num_rows > 0) {
                    while($row = $result->fetch_assoc()) {
                            $img = json_decode($row["img"], true);
							$titolo = $row['titolo'];
                            echo '<div class="col-12 col-md-4 py-2"><a href="news/'.$row['id'].'/'.slugify($titolo).'"><img alt="'.html_entity_decode(htmlentities($row['titolo'])).'" src="backoffice/'.str_replace('uploads/','uploads/resized/',str_replace('\\','/',$img[0]['serverFileName'])).'?h=270" class="img-fluid lazy" /><h5 class="pt-3 spaziatura white-t">'.html_entity_decode(htmlentities($row['titolo'])).'</h5></a></div>';
                        }
                }
                ?>
                <div class="col-12 text-center mt-4">
                    <a href="/news"><button type="button" class="btn btn-light mb-5">Leggi tutte le news</button></a>
                </div>                
            </div>
            
        </div>
        </div>
        
        <!-- clienti -->
        <div class="container pt-4 pb-3">
            <div class="row">
                <div class="col-12 mb-3 text-center">
                <h3 class="py-3 grassetto">I nostri clienti</h3>
                    <p>Tra i nostri clienti pi&#249; importanti ci sono grandi marchi.<br>Essi hanno scelto l'esperienza e l’efficienza di Florence One per la loro crescita aziendale.</p>
                </div>
                <div class="col-1 d-none d-md-block"></div>
                <div class="col-6 col-md-2 py-2">
                    <img alt="Logo Nee" src="/img/resized/nee.jpg?h=170" class="img-fluid" />
                </div>
                <div class="col-6 col-md-2 py-2">
                    <img alt="Logo Dentalx" src="/img/resized/dentalx.jpg?h=170" class="img-fluid" />
                </div>
                <div class="col-6 col-md-2 py-2">
                    <img alt="Logo Alisea" src="/img/resized/alisea.jpg?h=170" class="img-fluid" />
                </div>
                <div class="col-6 col-md-2 py-2">
                    <img alt="Logo Lt" src="/img/resized/lt.jpg?h=170" class="img-fluid" />
                </div>
                <div class="col-6 col-md-2 py-2">
                    <img alt="Logo Coop Italian Food" src="/img/resized/coop-italian-food.jpg?h=170" class="img-fluid" />
                </div>
                <div class="col-1 d-none d-md-block"></div>
                <div class="col-1 d-none d-md-block"></div>
                <div class="col-6 col-md-2 py-2">
                    <img alt="Logo Panapesca" src="/img/resized/panapesca.jpg?h=170" class="img-fluid" />
                </div>
                <div class="col-6 col-md-2 py-2">
                    <img alt="Logo Sirio" src="/img/resized/sirio.jpg?h=170" class="img-fluid" />
                </div>
                <div class="col-6 col-md-2 py-2">
                    <img alt="Logo Nova" src="/img/resized/nova.jpg?h=170" class="img-fluid" />
                </div>
                <div class="col-6 col-md-2 py-2">
                    <img alt="Logo Heroflon" src="/img/resized/heroflon.jpg?h=170" class="img-fluid" />
                </div>
                <div class="col-6 col-md-2 py-2">
                    <img alt="Logo EDN" src="/img/resized/edn.jpg?h=170" class="img-fluid" />
                </div>
                <div class="col-1 d-none d-md-block"></div>
                <div class="col-12 mt-5 text-center">
                    <a href="/case"><button type="button" class="btn btn-light mb-5">Scopri i casi di successo</button></a>
                </div>
            </div>
        </div>
        
        <?php include ("banner-settori.php"); ?>
        
        <?php include ("demo.php"); ?>
        
        <?php include ("footer.php"); ?>
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
        <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
        <script>jQuery( document ).ready(function() { jQuery('#home').addClass('attivo');} );</script>
        <script>
        $('.slides').slick({
            autoplay: true,
            infinite: false,
            prevArrow: '<i class="fas fa-chevron-left slick-prev position-absolute" style="left:30px;top:50%;z-index:10000"></i>',
            nextArrow: '<i class="fas fa-chevron-right slick-next position-absolute" style="right:30px;top:50%"></i>'
        });
        </script>
        <script>
            
        $('.lazy').Lazy({
            onError: function(element) {
                console.log('error loading ' + element.data('src'));
            }
        });
           
        </script>
    </body>
</html> 