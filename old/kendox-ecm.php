<?php
use Melbahja\Seo\Factory;

// Load Composer's autoloader
require 'vendor/autoload.php';

$metatags = Factory::metaTags();

$metatags->meta('author', 'Biznes')
		->meta('title', 'Florence One | Rivenditore SAP Italia - Kendox ECM')
		->meta('description', 'Kendox ECM è il miglior modo di gestire i documenti, le informazioni, i dossier ed i processi all’interno della tua azienda. Perfettamente integrabile con sistemi ERP e con SAP Business One e SAP Business By Design. Scopri di più.')
        ->meta('keywords', 'ECM, software gestione documentale, software gestione documenti, software gestione dossier, software gestione report, software gestione, Kendox, Kendox Infoshare, acquisizione documenti, condivisione documenti, Kendox Toscana, Kendox Italia, gestione workflow documentale, Florence One')
		->image('img/slide_kendox.jpg')
		->url('https://florence-one.it/kendox-ecm')
?>
<!DOCTYPE html>
<html lang="it">
    <head>
        <title>Florence One | Rivenditore SAP Italia - Kendox ECM</title>
        <meta name="generator"
        content="HTML Tidy for HTML5 (experimental) for Windows https://github.com/w3c/tidy-html5/tree/c63cc39" />
        <?php include ("head.php"); ?>
        <?=$metatags?>
    </head>
    <body>
        <?php include ("menu.php"); ?>
         <!-- banner -->
        <div id="banner-top" class="container-fluid sfondo-big d-flex justify-content-around align-items-center" style="background:url('img/slide_kendox.jpg')">
        <div class="container">
            <div class="row text-center testo-banner">
                <div class="col-12">
                    <div class="row pt-4">
                        <div class="col-12 col-md-6 offset-md-3">
                            <img src="img/logo_kendox.png" alt="Logo Kendox ECM" class="img-fluid" /><br />
                        </div>
                    </div>
                    <h2 class="pt-5">Perfettamente integrabile con sistemi ERP</h2>
                    <p>Ottimizza il modo di gestire i documenti, le informazioni, dossier ed i processi digitali</p>
                    <p class="pt-3 icona bounce"><i class="fas fa-angle-double-down"></i></p>
                </div>
            </div>
        </div>
        </div>
        
        <!-- sezione bianca -->
        <div class="container py-5">
            <div class="row">
                <div class="col-12 col-md-6 py-2">
                    <h4 class="pb-3 grassetto">Kendox InfoShare: il futuro della gestione documentale </h5>
                    <p>Kendox InfoShare è una soluzione di Enterprise Content Management (ECM), ossia un software avanzato per la gestione documentale e l’automazione dei processi, progettata per aziende che intendono ottimizzare il management
                    delle informazioni aziendali: dall’acquisizione, alla condivisione all’archiviazione Kendox InfoShare è completo, scalabile ed integrato alle suite SAP Business One e SAP Business By Design.</p>
                    <p>Qualunque sia la natura dell’informazione da gestire, Kendox InfoShare sarà in grado di gestirla in base alle esigenze richiesta, ottimizzandone la sua fruizione nei processi aziendali, rendendoli solidi ed efficienti.</p>
                <div class="text-center">
                    <a href="#demo"><button type="button" class="btn btn-primary mt-4">Richiedi una demo</button></a>
                </div>
                </div>
                <div class="col-12 col-md-6 pt-2 pb-5">
                    <img src="img/kendox_1.jpg" alt="Logo Kendox ECM e SAP Business One" class="img-fluid"  />
                </div>
            </div>
        </div>

        <!-- sezione bianca -->
        <div class="container pb-5">
            <div class="row">
                <div class="col-12 col-md-6 pt-2 pb-5">
                    <img src="img/kendox_2.png" alt="Kendox ECM schermata d'esempio" class="img-fluid"  />
                </div>
                <div class="col-12 col-md-6 py-2">
                    <h4 class="pb-3 grassetto"> Efficienza ed innovazione per dare valore alle informazioni</h5>
                    <p> La sempre più crescente importanza delle informazioni aziendali rende necessario uno strumento come Kendox InfoShare, poiché consente di ottenere elevati standard di qualità e precisione nella gestione dei processi  di archiviazione, semplifica la condivisione dei documenti con le varie figure coinvolte nei processi aziendali e permette di reperire tutte le informazioni necessarie H24, anche da remoto.</p>
                    <p> Grazie all’interfaccia che permette la valutazione a colpo d’occhio delle informazioni le aziende possono reagire più rapidamente alle mutevoli esigenze. Tutto ciò si traduce in significativi aumenti di efficienza dell’intero business. </p>
                </div>
            </div>
        </div>

        
        <!-- funzioni -->
       <div class="container-fluid py-5" style="background:#ededed">
       <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                <h3 class="py-3 grassetto">Le funzioni principali del software Kendox InfoShare</h3>
                    <p>La gestione delle informazioni è fondamentale indipendentemente dalle dimensioni o dal settore dell’impresa; si necessita sempre più di flessibilità, velocità e trasparenza per questo motivo Kendox InfoShare può fare la differenza.</p>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-12 col-md-4 py-2">
                    <h5> Acquisizione</h5>
                    <p>Potrai acquisire velocemente qualsiasi tipo di documento proveniente da canali differenti con un unico software. </p>
                </div>
                <div class="col-12 col-md-4 py-2">
                    <h5>Organizzazione</h5>
                    <p>Tutte le informazioni saranno organizzate in un’unica piattaforma centralizzata a prova di manomissione e ad eliminazione controllata. </p>
                </div>
                <div class="col-12 col-md-4 py-2">
                    <h5>Gestione</h5>
                    <p>La gestione dei documenti sarà più rapida, efficiente e precisa eliminando così gli errori dovuti a duplicati o perdite di dati. </p>
                </div>
                <div class="col-12 col-md-4 py-2">
                    <h5>Condivisione </h5>
                    <p>Facilita la condivisione delle informazioni in totale sicurezza e agilità migliorando la collaborazione tra i vari stakeholders. </p>
                </div>
                <div class="col-12 col-md-4 py-2">
                    <h5>Accessibilità</h5>
                    <p>La gestione digitale dei documenti rende immediatamente disponibili le informazioni, in qualunque momento e da qualunque device. </p>
                </div>
                <div class="col-12 col-md-4 py-2">
                    <h5>Integrazione </h5>
                    <p>Integrato con le suite SAP Business One e SAP Business By Design, per qualsiasi esigenza richiesta dai vari processi aziendali. </p>
                </div>
                <div class="col-12 col-md-4 py-2">
                    <h5>Ricerca</h5>
                    <p>Ottimizza e velocizza la consultazione di documenti e informazioni mediante strumenti di ricerca per parole chiave o full-text.</p>
                </div>
                <div class="col-12 col-md-4 py-2">
                    <h5>Workflow</h5>
                    <p>Consente di disegnare processi, anche complessi, di varia natura. Ad esempio approvazione di preventivi, fatture o contratti, ottimizzando così anche il rapporto con i vari attori coinvolti, siano essi interni o esterni all’azienda</p>
                </div>
                <div class="col-12 col-md-4 py-2">
                    <h5>Servizi Accessori</h5>
                    <p>Integrabile con servizi accessori quali la fatturazione elettronica e la conservazione a norma dei Documenti.</p>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-12 text-center">
                    <a href="#demo"><button type="button" class="btn btn-primary mt-4">Richiedi una demo</button></a>
                </div>
            </div>
        </div>
        </div>
        
        <!-- Bannerino -->
        <div class="container-fluid py-5 catch sfondo-big" style="background:url('img/world.png')">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h3 class="white-t text-center text-md-left">Integrabile con</h3>
                </div>
                <img alt="Logo SAP Business ONE" src="img/sap_b1.png" class="img-fluid col-12 col-md-6" />
                <img alt="Logo SAB ByDesign" src="img/sap_bydesign.png" class="img-fluid col-12 col-md-6" />
            </div>
        </div>
        </div>
        
        <?php include ("demo_kendox.php"); ?>
        

        <?php include ("footer.php"); ?>
    </body>
</html> 