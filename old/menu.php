<div class="container-fluid py-4 d-none d-xl-block" id="navbar" >
    <div class="row">
        <div class="col-lg-2 pl-5"><a href="/"><h3 class="logo">Florence One</h3></a></div>
        <div class="col-lg-10">
            <ul class="d-flex justify-content-around align-items-center h-100">
                <li id="home" class="menu_top"><a href="/">Home</a></li>
                <li id="chi" class="menu_top"><a href="/chi-siamo">Chi siamo</a></li>
                <li id="SAP" class="menu_top"><a href="/sap-business-one">SAP Business One</a></li>
                <li id="kendox" class="menu_top"><a href="/kendox-ecm">ECM</a></li>
                <li id="servizi" class="menu_top"><a href="/servizi">Servizi</a></li>
                <li id="case" class="menu_top"><a href="/case">Casi di successo</a></li>
                <li id="news" class="menu_top"><a href="/news">News</a></li>
                <li id="contatti" class="menu_top"><a href="/contatti">Contatti</a></li>
                <li id="servizi" class="menu_top grassetto"><a href="/valutazione-personalizzata">Valutazione Personalizzata</a></li>
            </ul>           
        </div>
    </div>
</div>

<div class="container-fluid py-4 d-xl-none" id="navbar" >
            <ul id="menu" class="d-none">
                <li id="home" class="menu_top"><a href="/">Home</a></li>
                <li id="chi" class="menu_top"><a href="/chi-siamo">Chi siamo</a></li>
                <li id="SAP" class="menu_top"><a href="/sap-business-one">SAP Business One</a></li>
                <li id="kendox" class="menu_top"><a href="/kendox-ecm">ECM</a></li>
                <li id="servizi" class="menu_top"><a href="/servizi">Servizi</a></li>
                <li id="case" class="menu_top"><a href="/case">Casi di successo</a></li>
                <li id="news" class="menu_top"><a href="/news">News</a></li>
                <li id="contatti" class="menu_top"><a href="/contatti">Contatti</a></li>
                <li id="servizi" class="menu_top grassetto"><a href="/valutazione-personalizzata">Valutazione Personalizzata</a></li>
            </ul>         
</div>