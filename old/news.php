<?php
use Melbahja\Seo\Factory;

// Load Composer's autoloader
require 'vendor/autoload.php';

$metatags = Factory::metaTags();

$metatags->meta('author', 'Biznes')
		->meta('title', 'Florence One | Rivenditore SAP Italia - News')
		->image('img/slide_news.jpg')
		->meta('description', 'Florence One fornisce le ultime notizie sulla famiglai SAP e sui programmi che possono aiutare la tua azienda e ti aiuta a comprendere le soluzioni fornite e as espandere le potenzialità del tuo business.')
		->url('https://florence-one.it/news')
?>
<!DOCTYPE html>
<html lang="it">
    <head>
        <title>Florence One | Rivenditore SAP Italia - News</title>
        <meta name="generator"
        content="HTML Tidy for HTML5 (experimental) for Windows https://github.com/w3c/tidy-html5/tree/c63cc39" />
        <?php 
        include ("head.php");
        include ("conn.php"); 
        ?>
        <?=$metatags?>
    </head>
    <body>
        <?php include ("menu.php"); ?>
        <!-- banner -->
        <div id="banner-top" class="container-fluid sfondo-big d-flex justify-content-around align-items-center" style="background:url('img/slide_news.jpg')">
        <div class="container">
            <div class="row text-center testo-banner">
                <div class="col-12">
                    <h1 class="titolo-pagina">News</h1>
                    <p>Florence One aggiunge continuamente nuove soluzioni tecnologiche innovative, business cases e progetti.<br>Per rimanere sempre aggiornato consulta le nostre news!</p>
                    <p class="pt-3 icona bounce"><i class="fas fa-angle-double-down"></i></p>
                </div>
            </div>
        </div>
        </div>
        
        <!-- sezione bianca -->
        <div class="container-fluid p-0">
            <div id="contenuto-news" class="row no-gutters white-t">
                    <?php
                    $sql = "SELECT * FROM news WHERE attivo = 1 ORDER BY Data DESC LIMIT 6";
                    $result = $conn->query($sql);
                    if ($result->num_rows > 0) {
                        while($row = $result->fetch_assoc()) {
                            $img = json_decode($row["img"], true);
                        echo '<div class="col-12 col-md-4" style="background:url(\'backoffice/'.str_replace('\\','/',$img[0]['serverFileName']).'\') no-repeat center center; background-size:cover" data-scroll-watch><a href="/news/'.$row['id'].'/'.slugify($row['titolo']).'"><div class="row no-gutters"><div class="col-12 position-relative case-studies overlay"><h3 class="position-absolute">'.$row['titolo'].'</h3></div></div></a></div>';
                        }
                    }
                    ?>
            </div>
        </div>
        
        <div class="container-fluid py-5">
            <div class="row text-center">
                <div class="col-12">
                    <button type="button" class="btn btn-primary" onclick="caricanews();">Carica altri...</button>
                </div>
            </div>
        </div>
        
        <!-- Bannerino -->
        <div class="container-fluid py-5 catch sfondo-big loading" style="background:url('img/2021.jpg')">
        <div class="container">
            <div class="row">
                <div class="col-12"><h3 class="white-t">LEADER NEL <br>MERCATO ERP</h3></div>
            </div>
        </div>
        </div>

        <?php include ("footer.php"); ?>
    </body>
</html> 