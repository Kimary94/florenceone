<?php
use Melbahja\Seo\Factory;

// Load Composer's autoloader
require 'vendor/autoload.php';

$metatags = Factory::metaTags();

$metatags->meta('author', 'Biznes')
		->meta('title', 'Florence One | Rivenditore SAP Italia - SAP Business One')
		->meta('description', 'SAP Business One è leader nel mercato ERP. Con le nostre soluzioni software potrai dedicarti alla crescita dell’azienda e lasciare a SAP Business One il compito di gestire i processi aziendali, riguardanti l’amministrazione dell’azienda e la gestione dei processi produttivi. Riducendo costi ed errori. Scopri di più.')
        ->meta('keywords', 'SAP Business One, software gestionale, Rivenditore SAP, partner SAP, gestionale SAP, software contabilità, software gestione produzione, software aziendale, ERP, programma contabilità, programma fatture, SAP, ERP aziendale, Florence One, semplificazione processi aziendali, contabilità, imposte, banche, controlling, vendite, acquisti, sales, service')
		->image('img/slide_sap.jpg')
		->url('https://florence-one.it/sap-business-one')
?>
<!DOCTYPE html>
<html lang="it">
    <head>
        <title>Florence One | Rivenditore SAP Italia - SAP Business One</title>
        <meta name="generator"
        content="HTML Tidy for HTML5 (experimental) for Windows https://github.com/w3c/tidy-html5/tree/c63cc39" />
        <?php include ("head.php"); ?>
        <?=$metatags?>
    </head>
    <body>
        <?php include ("menu.php"); ?>
        <!-- banner -->
        <div id="banner-top" class="container-fluid sfondo-big d-flex justify-content-around align-items-center" style="background:url('img/slide_sap.jpg')">
        <div class="container">
            <div class="row text-center testo-banner">
                <div class="col-12">
                    <img src="img/logo_sap.png" class="img-fluid" alt="Logo Sap Business One" /><br />
                    <h2 class="pt-5">Leader nel mercato ERP</h2>
                    <p>Potrai finalmente dedicarti alla crescita dell’Azienda mentre SAP<br>elimina gli inserimenti di dati ridondanti, gli errori ed i costi delle inefficienza aziendali.</p>
                    <p class="pt-3 icona bounce"><i class="fas fa-angle-double-down"></i></p>
                </div>
            </div>
        </div>
        </div>

        <!-- sezione bianca -->
        <div class="container py-5">
            <div class="row">
                <div class="col-12 col-md-6 pt-2 pb-5">
                    <img src="img/home-sap-business-one-4.jpg" class="img-fluid" alt="Florence One - Sap Business One - Schermata di esempio"  />
                </div>
                <div class="col-12 col-md-6 py-2">
                    <h4 class="pb-3 grassetto">SAP Business One:<br>controllo e semplificazione</h5>
                    <p>Sap Business One è progettato per adattarsi a te e per trasformarsi insieme alla tua azienda. Avrai una visione completa su tutta la tua attività, prenderai decisioni mirate grazie all’accesso a informazioni in tempo reale e tutto questo grazie a un’interfaccia semplice e intuitiva.</p>
                    <ul>
                        <li class="spaziatura">Controllo in tempo reale su processi aziendali</li>
                        <li class="spaziatura">Semplificazione della gestione delle attività</li>
                    </ul>
                </div>
            </div>
        </div>
        
        <!-- funzioni -->
       <div class="container-fluid py-5" style="background:#ededed">
       <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                <h3 class="py-3 grassetto">Le funzioni principali del software</h3>
                    <p>SAP Business One è il Software Gestionale completo e integrato, progettato per le imprese e per crescere con loro: è flessibile,
                        modulare, potente, semplice da usare e, grazie alle competenze specifiche di Florence One, adatto a numerosi settori industriali.</p>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-12 col-md-4 py-2">
                    <h5>Contabilità generale</h5>
                    <p>Gestione della contabilità della tua azienda, sia analitica che gestionale, tramite registrazione di tutti i conti.</p>
                </div>
                <div class="col-12 col-md-4 py-2">
                    <h5>Imposte</h5>
                    <p>Individua la normativa applicata nel paese e imposta i parametri per la definizione e il controllo delle imposte.</p>
                </div>
                <div class="col-12 col-md-4 py-2">
                    <h5>Banche</h5>
                    <p>Effettua transazioni monetarie legate ai tuoi conti correnti bancari comodamente da un’unica piattaforma.</p>
                </div>
                <div class="col-12 col-md-4 py-2">
                    <h5>Controlling</h5>
                    <p>Tieni monitorata la redditività della tue attività aziendali o di singoli rami d’azienda con analisi dedicate.</p>
                </div>
                <div class="col-12 col-md-4 py-2">
                    <h5>Vendite</h5>
                    <p>Gestione dell’intero processo di vendita con possibilità di creare offerte personalizzate per i tuoi clienti.</p>
                </div>
                <div class="col-12 col-md-4 py-2">
                    <h5>Acquisti</h5>
                    <p>Gestisci l’intero ciclo di acquisto grazie anche alla possibilità di creare report per l’analisi delle informazioni.</p>
                </div>
                <div class="col-12 col-md-4 py-2">
                    <h5>Gestione attività</h5>
                    <p>Coordina le interazioni con gli agenti commerciali e tutte le attività di vendita in modo semplice e intuitivo.</p>
                </div>
                <div class="col-12 col-md-4 py-2">
                    <h5>Sales</h5>
                    <p>App mobile che consente l’accesso all’ERP in modo completo per avere sempre la situazione sotto controllo.</p>
                </div>
                <div class="col-12 col-md-4 py-2">
                    <h5>Service</h5>
                    <p>App mobile di gestione del servizio ticketing e per la risoluzione dei problemi in modo veloce ed efficiente.</p>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-12 text-center">
                    <a href="#demo"><button type="button" class="btn btn-primary mt-4">Richiedi una demo</button></a>
                </div>
            </div>
        </div>
        </div>
        
        <!-- Bannerino -->
        <div class="container-fluid py-5 catch sfondo-big" style="background:url('img/world.png')">
        <div class="container">
            <div class="row">
                <div class="col-12"><h3 class="white-t">Presente nelle <br>migliori aziende del mondo</h3></div>
            </div>
        </div>
        </div>
        
        <?php include ("banner-settori.php"); ?>
        
        <?php include ("demo.php"); ?>
        
        <?php include ("tris.php"); ?>
        

        <?php include ("footer.php"); ?>
                <script>
        $('.lazy').Lazy({
            onError: function(element) {
                console.log('error loading ' + element.data('src'));
            }
        });
        </script>
    </body>
</html> 