<?php
use Melbahja\Seo\Factory;

// Load Composer's autoloader
require 'vendor/autoload.php';

$metatags = Factory::metaTags();

$metatags->meta('author', 'Biznes')
		->meta('title', 'Florence One | Rivenditore SAP Italia - Servizi')
		->meta('description', 'Florence One offre servizi e soluzioni calibrate direttamente sulle tue esigenze per implementare ed espandere le potenzialità del tuo business. Offriamo soluzioni MES, applicazioni mobile e web app e tutte le soluzioni della famiglia SAP. Scopri di più.')
        ->meta('keywords', 'Sistemi MES, Florence One, gestione workflow produzione, software gestione magazzino, progettazione portali web, portali intranet, realizzazione ecommerce, realizzazione app mobile')
		->image('img/slide_servizi.jpg')
		->url('https://florence-one.it/servizi')
?>
<!DOCTYPE html>
<html lang="it">
    <head>
        <title>Florence One | Rivenditore SAP Italia - Servizi</title>
        <meta name="generator"
        content="HTML Tidy for HTML5 (experimental) for Windows https://github.com/w3c/tidy-html5/tree/c63cc39" />
        <?php include ("head.php"); ?>
        <?=$metatags?>
    </head>
    <body>
        <?php include ("menu.php"); ?>
        <!-- banner -->
        <div id="banner-top" class="container-fluid sfondo-big d-flex justify-content-around align-items-center" style="background:url('img/slide_servizi.jpg')">
        <div class="container">
            <div class="row text-center testo-banner">
                <div class="col-12">
                    <h2 class="titolo-pagina">Servizi</h2>
                    <p>Servizi e soluzioni calibrate direttamente sulle tue esigenze<br>per implementare ed espandere le potenzialità del tuo business</p>
                    <p class="pt-3 icona bounce"><i class="fas fa-angle-double-down"></i></p>
                </div>
            </div>
        </div>
        </div>

        <!-- servizio 1 -->
        <div class="container-fluid py-5" style="background:">
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                <h3 class="py-3 grassetto">Il tuo Partner nell'Industria 4.0</h3>
            </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-6 pt-2 pb-5">
                    <img src="img/servizi-mes.jpg" alt="Servizi MES" class="img-fluid"  />
                </div>
                <div class="col-12 col-md-6 py-2">
                    <h4 class="pb-3 grassetto">Leader nei Sistemi MES</h5>
                    <p>Grazie alla collaborazione con Up Solutions, l'azienda italiana leader nelle soluzioni MES - Manufacturing Execution System, Florence One è in grado di offrire soluzioni ERP complete e integrate che consentono di sviluppare nuovi prodotti, pianificare la produzione, raccogliere dati dalle macchine, verificare la qualità dei prodotti.</p>
                    <p>Le aziende di produzione sono al centro di una vera e propria rivoluzione nell'ottimizzazione dei processi. Con le soluzioni MES Florence One è possibile creare un unico centro di controllo integrato per la gestione ed il controllo di tutte le funzioni aziendali e dell'intero ciclo di vita delle attività di produzione.</p>
                </div>
            </div>
        </div>
        <!-- funzioni -->
       <div class="container">
            <div class="row mt-4">
                <div class="col-12 col-md-4 py-2">
                    <h5>Gestisci la Produzione in Modo Integrato</h5>
                    <p>Ordini, Time and Material, Schedulazione a capacità finita, PLM, bordo macchina, Magazzino, dispositivi IoT: i sistemi MES di Florence One ti consentono di gestire tutte le funzioni di produzione in modo efficiente ed integrato.</p>
                </div>
                <div class="col-12 col-md-4 py-2">
                    <h5>Potenti WorkFlow di Produzione</h5>
                    <p>Personale, prodotti, R&D, acquisti, produzione, commerciale, partner: i sistemi MES di Florence One e Up Solutions consentono di raccogliere tutti i dati aziendali in un unico database e creare potenti WorkFlow per rendere tutti i processi semplici da gestire, accessibili ovunque tramite web e mobile.</p>
                </div>
                <div class="col-12 col-md-4 py-2">
                    <h5>Verticalizzato per il tuo Settore</h5>
                    <p>Le soluzioni MES di Florence One sono personalizzati per tutti i settori produttivi, anche quelli più complessi come nel caso delle aziende tessili e di moda.</p>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-12 text-center">
                    <a href="/contatti"><button type="button" class="btn btn-primary mt-4">Richiedi informazioni</button></a>
                </div>
            </div>
        </div>
        </div>
        
        <!-- servizio 2 -->
        <div class="container-fluid py-5"  style="background:#ededed" data-scroll-watch>
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                <h3 class="py-3 grassetto">L’applicazione Mobile per la Gestione del Magazzino</h3>
            </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-6 py-2">
                    <h4 class="pb-3 grassetto">Add-on B1 Warehouse</h5>
                    <p>L’applicazione mobile "B1 Warehouse" è pensata per la gestione di mobile del magazzino ed è nativamente integrata con SAP Business One. L’integrazione con SAP Business One consente gestire gli Ordini Clienti generato da SAP.</p>
                    <p>L’add-on è dotato di un sistema di autocompilazione per facilitare la ricerca di un Cliente/Fornitore censito su SAP Business One. Il processo è gestito tramite il servizio Data Interface API (DI API) di SAP; data l’ampiezza del volume di dati che un’anagrafica articoli potrebbe contenere, la soluzione consente di snellire tempi di risposta e la velocità delle operazioni effettuate all’interno dell’applicazione.</p>
                </div>
                <div class="col-12 col-md-6 pt-2 pb-5">
                    <img src="img/servizi-Add-on-Warehouse.jpg" alt="Servizi Add On Warehouse" class="img-fluid"  />
                </div>
            </div>
        </div>
        <!-- funzioni -->
       <div class="container">
            <div class="row mt-4">
                <div class="col-12 col-md-4 py-2">
                    <h5>Interfaccia Semplice e Intuitiva</h5>
                    <p>L’interfaccia grafica è semplice e intuitiva e permette di gestire rapidamente Ordini Clienti, Entrate Merci, Uscite Merci e Trasferimenti di magazzino dai propri terminali.</p>
                </div>
                <div class="col-12 col-md-4 py-2">
                    <h5>Scanerizzazione e Controllo Barcode</h5>
                    <p>Selezionando l’ordine da consegnare, l’operatore di magazzino può utilizzare il terminale per: scannerizzare il Barcode o inserirlo manualmente. L’add-on controlla che gli articoli scannerizzati siano presenti nell’ordine selezionato e evidenzia graficamente le righe che trovano esatta corrispondenza.</p>
                </div>
                <div class="col-12 col-md-4 py-2">
                    <h5>Gestione Magazzino da terminali Android</h5>
                    <p>Gli operatori possono accedere all’applicazione di magazzino digitando il proprio nome utente e password generato in SAP Business One.</p>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-12 text-center">
                    <a href="/contatti"><button type="button" class="btn btn-primary mt-4">Richiedi informazioni</button></a>
                </div>
            </div>
        </div>
        </div>
        
        
        <!-- servizio 4 -->
        <div class="container-fluid py-5" data-scroll-watch>
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                <h3 class="py-3 grassetto">Applicazioni Mobile per la Trasformazione Digitale</h3>
            </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-6 py-2">
                    <h4 class="pb-3 grassetto">Portali Web per Migliorare il tuo Business</h5>
                    <p>Grazie ai servizi integrati di consulenza di processo, progettazione, formazione e promozione le soluzioni web di Florence One danno alla tua Azienda l’assoluta sicurezza di raggiungere i risultati di Business prefissati e rendere tutti i processi efficienti e flessibili.</p>
                    <p>Florence One lavora con un team di tecnici altamente specializzati nella realizzazione di applicazioni mobile per smartphone e tablet, su piattaforma iOS e Android. Il nostro team segue tutte le fasi del ciclo di vita dell’applicazione: dalla progettazione al design grafico, dalla realizzazione alla pubblicazione su App Store e Android Market.</p>
                </div>
                <div class="col-12 col-md-6 pt-2 pb-5">
                    <img src="img/servizi-portaliweb.jpg"  alt="Servizi Portali Web e Applicaizoni Mobile" class="img-fluid"  />
                </div>
            </div>
        </div>
        <!-- funzioni -->
       <div class="container">
            <div class="row mt-4">
                <div class="col-12 col-md-4 py-2">
                    <h5>Portali Web, Intranet e Clienti / Fornitori</h5>
                    <p>Creazione di Portali Web Collaborativi particolarmente adatta alla gestione e condivisione delle informazioni aziendali, distribuendole con accesso sicuro e dedicato a clienti, fornitori, partner e dipendenti, in modo completamente integrato con le soluzioni di Gestione Documentale aziendali.</p>
                </div>
                <div class="col-12 col-md-4 py-2">
                    <h5>Realizzazione di Portali eCommerce</h5>
                    <p>Grazie a migliaia di estensioni, i portali eCommerce Florence One consentono di gestire l’intero ciclo commerciale online, dalla gestione del magazzino per eCommerce e fornitori alla promozione del Portale per aumentare costantemente ROI e vendite online.</p>
                </div>
                <div class="col-12 col-md-4 py-2">
                    <h5>Applicazioni Mobile iOS e Android</h5>
                    <p>Sviluppo di applicazioni native per iOS e Android, sia pubbliche che riservate al personale interno dell’azienda. Cataloghi mobile online e offline, gestionali, sistemi di pubblicazione di contenuti, piattaforme di streaming audio e video. Florence One sviluppa app mobile per consentire a ogni utente di gestire i processi aziendali direttamente dal proprio dispositivo. Le app sviluppate sono pensate per la gestione di più aree, tra cui logistica, magazzino e CRM. Inoltre, Florence One sviluppa app orientate al management aziendale, basate su principi di Real Time Analytics e Reporting.</p>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-12 text-center">
                    <a href="/contatti"><button type="button" class="btn btn-primary mt-4">Richiedi informazioni</button></a>
                </div>
            </div>
        </div>
        </div>
        
        <!-- servizio 4 -->
        <div class="container-fluid py-5"  style="background:#ededed" >
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                <h3 class="py-3 grassetto">Maggiore Efficienza per Dati e Informazioni</h3>
            </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-6 pt-2 pb-5">
                    <img src="img/servizi-business.jpg" alt="Servizi efficienza dati e informazioni" class="img-fluid"  />
                </div>
                <div class="col-12 col-md-6 py-2">
                    <h4 class="pb-3 grassetto">Migliora le Decisioni Aziendali</h5>
                    <p>Per migliorare le decisioni strategiche basandosi su informazioni affidabili ed aggiornate, Omnia Group utilizza soluzioni di Business Intelligence leader di mercato; una suite completa di strumenti per l’integrazione e l'analisi dei dati utilizzata da migliaia di aziende in tutto il mondo.</p>
                    <p>Le soluzioni di Business Intelligence Florence One ti consentono di raccogliere i dati da tutte le fonti aziendali, accedere alle informazioni e condividerle con semplicità, esplorare ed analizzare i dati in modo interattivo ed in tempo reale, ottenere visibilità immediata sulle metriche ed i KPIs (Key Performance Indicators) più importanti per la tua Azienda, scoprire gli andamenti del mercato e prevedere le performance attuali e future di tutti i processi.</p>
                </div>
            </div>
        </div>
        <!-- funzioni -->
       <div class="container">
            <div class="row mt-4">
                <div class="col-12 col-md-4 py-2">
                    <h5>SAP HANA</h5>
                    <p>Con l’implementazione di SAP HANA, in-memory computing database, la soluzione ERP diviene fino a 3600 volte più veloce nell’analisi, condivisione e pubblicazione di grandi quantità di dati, per poter finalmente gestire il tuo business in real-time. Attraverso SAP HANA diventano quindi possibili applicazioni innovative come sistemi di manutenzione predittiva o la simulazione in tempo reale della performance di tutta l’azienda.</p>
                </div>
                <div class="col-12 col-md-4 py-2">
                    <h5>Oracle Hyperion</h5>
                    <p>Oracle Hyperion Workspace è una piattaforma modulare di Business Intelligence che offre funzionalità di reporting, query e analisi per un'ampia varietà di fonti di dati in un unico ambiente coordinato. Internet of Things, Big Data Integration e Analytics sono solo alcune delle funzionalità di Hyperion.</p>
                </div>
                <div class="col-12 col-md-4 py-2">
                    <h5>SAP Business Objects</h5>
                    <p>SAP Business Objects consente di supporta la crescita della tua Azienda con un'unica piattaforma centralizzata per la visualizzazione e la condivisione delle informazioni. Con un'architettura estremamente flessibile, Business Objects è la soluzione modulare che cresce insieme alle esigenze dell'organizzazione.</p>
                </div>
                <div class="col-12 col-md-4 py-2">
                    <h5>Pentaho</h5>
                    <p>Pentaho Data Integration e Pentaho Business Analytics sono le piattaforme di Business Intelligence Open Source leader di mercato. Pentaho consente di gestire ed elaborare i dati in ambienti ibridi e multicloud e di risolvere problemi di business utilizzando connettori per lo streaming dei dati dell'Azienda.</p>
                </div>
                <div class="col-12 col-md-4 py-2">
                    <h5>MicroStrategy</h5>
                    <p>MicroStrategy offre una serie di soluzioni che consentono alle aziende di sfruttare appieno il potenziale dei propri investimenti. Dal reporting aziendale al data discovery, dalla produttività mobile alla telemetria in tempo reale, MicroStrategy offre una vasta gamma di funzionalità per trasformare i dati in informazioni concrete per ottimizzare le performance aziendali.</p>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-12 text-center">
                    <a href="/contatti"><button type="button" class="btn btn-primary mt-4">Richiedi informazioni</button></a>
                </div>
            </div>
        </div>
        </div>
        
        <!-- servizio 5 -->
        <div class="container-fluid py-5"  data-scroll-watch>
        <div class="container">
            <div class="row">
                <div class="col-12 text-center">
                <h3 class="py-3 grassetto">Massima Sicurezza per il Mondo Digitale</h3>
            </div>
            </div>
            <div class="row">
                <div class="col-12 col-md-6 py-2">
                    <h4 class="pb-3 grassetto">Soluzioni Leader a Livello Globale</h5>
                    <p>Le soluzioni di Cyber Security Florence One sono le più utilizzate a livello globale dalle più grandi organizzazioni internazionali. Consulenti esperti ai massimi livelli sui processi di sicurezza digitale di centinaia di organizzazioni private e governative europee ti guidano alla ricerca e all'implementazione delle migliori soluzioni di Cyber Security per difendere gli asset più importanti della tua Piccola e Media Impresa.</p>
                    <p>Proteggere solo la rete aziendale non è più sufficiente. Il proliferare di device mobile e dispositivi IoT (Internet of Things) costringono a ripensare l'approccio alla sicurezza dell'organizzazione. Florence One offre soluzioni innovative di Endpoint Protection che difendono tutti i dispositivi e le reti aziendali in modo proattivo, grazie a complessi algoritmi di machine learning ed intelligenza artificiale.</p>
                </div>
                <div class="col-12 col-md-6 pt-2 pb-5">
                    <img src="img/servizi-cybersecurity.jpg" alt="Servizi Cyber Security e digital" class="img-fluid"  />
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-12 text-center">
                    <a href="/contatti"><button type="button" class="btn btn-primary mt-4">Richiedi informazioni</button></a>
                </div>
            </div>
        </div>
        </div>
        
        <!-- Bannerino -->
        <div class="container-fluid py-5 catch sfondo-big" style="background:url('img/consulenza.jpg')">
        <div class="container">
            <div class="row">
                <div class="col-12"><h3 class="white-t">La soluzione più adatta alla tua Azienda?<br> Contattaci per una consulenza gratuita!</h3></div>
            </div>
        </div>
        </div>
        
        <?php include ("demo.php"); ?>
        
        <?php include ("tris.php"); ?>
        

        <?php include ("footer.php"); ?>
    </body>
</html> 