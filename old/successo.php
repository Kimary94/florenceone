<!DOCTYPE html>
<?php
use Melbahja\Seo\Factory;
// Load Composer's autoloader
require 'vendor/autoload.php';

if(isset($_GET['id'])){
    include ("conn.php");
    $id = $_GET['id'];
    $sql = "SELECT * FROM casi_di_successo WHERE id = ".$id;
    $result = $conn->query($sql);
    if ($result->num_rows > 0) {
        while($row = $result->fetch_assoc()) {
            $img = json_decode($row["foto"], true);
            $img = str_replace('\\','/',$img[0]['serverFileName']);

$metatags = Factory::metaTags();

$metatags->meta('author', 'Biznes')
		->meta('title', 'Florence One - '.htmlentities($row['titolo'], ENT_COMPAT, "iso-8859-1"))
		->meta('description', htmlentities($row['sottotitolo'], ENT_COMPAT, "iso-8859-1"))
		->image('/backoffice/'.$img)
        ->url('https://florence-one.it/case')
?>
<html lang="it">
    <head>
        <title>Florence One | Rivenditore SAP Italia - <?=htmlentities($row['titolo'], ENT_COMPAT, "iso-8859-1")?></title>
        <meta name="generator"
        content="HTML Tidy for HTML5 (experimental) for Windows https://github.com/w3c/tidy-html5/tree/c63cc39" />
        <?php include ("head.php"); ?>
        <?=$metatags?>
    </head>
    <body>
        <?php 
            include ("menu.php"); 
        ?>
        <!-- banner -->
        <div id="banner-top" class="container-fluid sfondo-big d-flex justify-content-around align-items-center" style="background:url('/backoffice/<?=$img?>') no-repeat center center;background-size: cover;">
        <div class="container">
            <div class="row text-center testo-banner">
                <div class="col-12">
                    <h2 class="titolo-pagina"><?=htmlentities($row['titolo'], ENT_COMPAT, "iso-8859-1")?></h2>
                    <p><?=htmlentities($row['sottotitolo'], ENT_COMPAT, "iso-8859-1")?></p>
                    <p class="pt-3 icona bounce"><i class="fas fa-angle-double-down"></i></p>
                </div>
            </div>
        </div>
        </div>
        
        <!-- sezione bianca -->
        <div class="container py-5">
            <div class="row">
                <?php $data = new DateTime($row['data']); ?>
                <div class="col-12 col-md-4"><p>Data: <?=$data->format('d-m-Y')?></p></div>
                <div class="col-12 col-md-4"><p>Cliente: <?=$row['cliente']?></p></div>
                <div class="col-12 col-md-4"><p>Sito: <a href="<?=$row['sito']?>" target="_blank"><?=$row['sito']?></a></p></div>
                <?php
                    if($row['storia'] != ""){
                        echo '<div class="col-12"><h3>La storia</h3>';
                        echo '<p>'.utf8_encode($row['storia']).'</p></div>';
                    }
                    if($row['sfida'] != ""){
                        echo '<div class="col-12"><h3>La sfida</h3>';
                        echo '<p>'.utf8_encode($row['sfida']).'</p></div>';
                    }
                    if($row['soluzione'] != ""){
                        echo '<div class="col-12"><h3>La soluzione</h3>';
                        echo '<p>'.utf8_encode($row['soluzione']).'</p></div>';
                    }
                    if($row['extra'] != ""){
                        echo '<div class="col-12"><h3>Altro</h3>';
                        echo '<p>'.utf8_encode($row['extra']).'</p></div>';
                    }
                ?>
                
            </div>
        </div>
        
        <!-- Bannerino -->
        <div class="container-fluid white-t py-5 catch sfondo-big" style="background:url('/img/world.png')">
        <div class="container">
            <div class="row">
                <div class="col-12"><h3 class="white-t">LEADER NEL <br>MERCATO ERP</h3></div>
            </div>
        </div>
        </div>
        
        <?php include ("demo.php"); ?>

        <?php include ("footer.php"); ?>
    </body>
</html> 
<?php
}}}
else{
    header("location: /news.php");
}