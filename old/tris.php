<!-- tripletta lato lato -->
        <div class="container-fluid">
            <div class="row text-center">
                <div class="col-12 col-md-4 py-5 sfondo-big" style="background:url('img/tris_1.jpg')">
                    <h3>SAP Business One</h3>
                    <p class="mb-5 spaziatura">Digitalizza la tua azienda</p>
                    <p class="catch">LEADER NEL <br>MERCATO ERP</p>
                    <a href="/sap-business-one"><button type="button" class="btn btn-light mb-5 mt-4">Scopri di pi&#249;</button></a>
                </div>
                <div class="col-12 col-md-4 py-5 sfondo-big white-t" style="background:url('img/tris_2.jpg')">
                    <h3>Casi di successo</h3>
                    <p class="mb-5 spaziatura white-t">Scopri le aziende che ci hanno scelto</p>
                    <p class="catch  white-t">CHI CI <br> HA SCELTO</p>
                    <a href="/case"><button type="button" class="btn btn-light mb-5 mt-4">Scopri di pi&#249;</button></a>
                </div>
                <div class="col-12 col-md-4 py-5 sfondo-big" style="background:url('img/tris_3.jpg')">
                    <h3>Servizi</h3>
                    <p class="mb-5 spaziatura">Tutti i nostri servizi alle imprese</p>
                    <p class="catch">SEMPRE AL <br> TUO FIANCO</p>
                    <a href="/servizi"><button type="button" class="btn btn-light mb-5 mt-4">Scopri di pi&#249;</button></a>
                </div>
            </div>
        </div>