<?php
use Melbahja\Seo\Factory;

// Load Composer's autoloader
require 'vendor/autoload.php';

$metatags = Factory::metaTags();

$metatags->meta('author', 'Biznes')
		->meta('title', 'Florence One | Rivenditore SAP Italia - Richiedi valutazione personalizzata')
		->meta('description', 'Prenota una valutazione personalizzata gratuita per scoprire in che modo SAP Business One può aiutare la tua azienda. Per iniziare è sufficiente completare il questionario.')
		->image('img/slide_contatti.jpg')
		->url('https://florence-one.it/valutazione-personalizzata')
?>
<!DOCTYPE html>
<html lang="it">
    <head>
        <title>Florence One | Rivenditore SAP Italia - Richiedi valutazione personalizzata</title>
        <?php include ("head.php"); ?>
        <?=$metatags?>
    </head>
    <body>
        <?php include ("menu.php"); ?>
        <!-- banner -->
        <div id="banner-top" class="container-fluid sfondo-big d-flex justify-content-around align-items-center" style="background:url('img/slide_valutazione.jpg')">
        <div class="container">
            <div class="row text-center testo-banner">
                <div class="col-12">
                    <h2 class="titolo-pagina">Richiedi valutazione personalizzata</h2>
                    <p>Prenota una valutazione personalizzata gratuita per scoprire in che modo SAP Business One può aiutare la tua azienda.</p>
                    <p class="pt-3 icona bounce"><i class="fas fa-angle-double-down"></i></p>
                </div>
            </div>
        </div>
        </div>

        <!-- sezione bianca -->
        <div class="container py-5">
            <div class="row">
                
                <div class="col-12 pt-4">
                    <h4>Richiedi una valutazione personalizzata.</h4>
                    <p>Per iniziare è sufficiente completare il questionario sotto riportato.</p>
                    <form>
                        <div class="form-row">
                            <div class="col-12 col-md-6 mt-5">
                                <p class="grassetto">Quanti dipendenti ha attualmente la tua azienda?</p>
                                <select id="dipendenti" name="dipendenti" class="custom-select">
                                  <option selected value="1">1-5</option>
                                  <option value="2">5-50</option>
                                  <option value="3">50-100</option>
                                  <option value="4">100-250</option>
                                  <option value="5">Più di 250</option>
                                </select>
                                </div>
                            <div class="col-12 col-md-6 mt-5">
                                <p class="grassetto">Quale è la vostra fascia di fatturato annuo?</p>
                                <select id="fatturato" name="fatturato" class="custom-select">
                                  <option selected value="1"><1m</option>
                                  <option value="2">1m-10m</option>
                                  <option value="3">10m-100m</option>
                                  <option value="4">100m-250m</option>
                                  <option value="5">>250m</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-row">
                            <div class="col-12 mt-5">
                                <p class="grassetto">Quale software finanziario e di contabilità utilizzate attualmente per la gestione della vostra azienda? (spuntare tutte le opzioni pertinenti)</p>
                                <div class="custom-control custom-checkbox custom-control-inline">
                                    <input type="checkbox" class="custom-control-input" id="contabilita">
                                    <label class="custom-control-label" for="contabilita">Contabilità</label>
                                </div> 
                                <div class="custom-control custom-checkbox custom-control-inline">
                                    <input type="checkbox" class="custom-control-input" id="fatturazione">
                                    <label class="custom-control-label" for="fatturazione">Fatturazione</label>
                                </div> 
                                <div class="custom-control custom-checkbox custom-control-inline">
                                    <input type="checkbox" class="custom-control-input" id="paghe">
                                    <label class="custom-control-label" for="paghe">Paghe</label>
                                </div> 
                                <div class="custom-control custom-checkbox custom-control-inline">
                                    <input type="checkbox" class="custom-control-input" id="banche">
                                    <label class="custom-control-label" for="banche">Banche</label>
                                </div> 
                                <div class="custom-control custom-checkbox custom-control-inline">
                                    <input type="checkbox" class="custom-control-input" id="pagamenti">
                                    <label class="custom-control-label" for="pagamenti">Pagamenti</label>
                                </div> 
                                <div class="custom-control custom-checkbox custom-control-inline">
                                    <input type="checkbox" class="custom-control-input" id="excel">
                                    <label class="custom-control-label" for="excel">Fogli MS Excel / Altro</label>
                                </div>                                  
                                </div>
                        </div>
                        
                        <div class="form-row">
                            <div class="col-12 mt-5">
                                <p class="grassetto">Quale altro software utilizzate attualmente per la gestione della vostra azienda? (spuntare tutte le opzioni pertinenti)</p>
                                <div class="custom-control custom-checkbox custom-control-inline">
                                    <input type="checkbox" class="custom-control-input" id="finanza">
                                    <label class="custom-control-label" for="finanza">Finanza</label>
                                </div> 
                                <div class="custom-control custom-checkbox custom-control-inline">
                                    <input type="checkbox" class="custom-control-input" id="crm">
                                    <label class="custom-control-label" for="crm">CRM / Marketing</label>
                                </div> 
                                <div class="custom-control custom-checkbox custom-control-inline">
                                    <input type="checkbox" class="custom-control-input" id="vendite">
                                    <label class="custom-control-label" for="vendite">Vendite</label>
                                </div> 
                                <div class="custom-control custom-checkbox custom-control-inline">
                                    <input type="checkbox" class="custom-control-input" id="ecommerce">
                                    <label class="custom-control-label" for="ecommerce">eCommerce</label>
                                </div> 
                                <div class="custom-control custom-checkbox custom-control-inline">
                                    <input type="checkbox" class="custom-control-input" id="software_altro">
                                    <label class="custom-control-label" for="software_altro">Altro</label>
                                </div>
                            </div>
                        </div>
                        
                         <div class="form-row">
                            <div class="col-12 col-md-6 mt-5">
                                <p class="grassetto">Disponete di un team informatico dedicato?</p>
                                <div class="custom-control custom-radio custom-control-inline">
                                  <input type="radio" id="informaticosi" name="team-informatico" value="si" class="custom-control-input">
                                  <label class="custom-control-label" for="informaticosi">Sì</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                  <input type="radio" id="informaticono" name="team-informatico" value="no" class="custom-control-input">
                                  <label class="custom-control-label" for="informaticono">No</label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mt-5">
                                <p class="grassetto">I dati dei vostro clienti sono in un database separato?</p>
                                <div class="custom-control custom-radio custom-control-inline">
                                  <input type="radio" id="dabasesi" name="database" value="si" class="custom-control-input">
                                  <label class="custom-control-label" for="dabasesi">Sì</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                  <input type="radio" id="dabaseno" name="database" value="no" class="custom-control-input">
                                  <label class="custom-control-label" for="dabaseno">No</label>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-row">
                            <div class="col-12 col-md-6 mt-5">
                                <p class="grassetto">Avete società controllate?</p>
                                <div class="custom-control custom-radio custom-control-inline">
                                  <input type="radio" id="controllatesi" name="controllate" value="si" class="custom-control-input">
                                  <label class="custom-control-label" for="controllatesi">Sì</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                  <input type="radio" id="controllateno" name="controllate" value="no" class="custom-control-input">
                                  <label class="custom-control-label" for="controllateno">No</label>
                                </div>
                            </div>
                            <div class="col-12 col-md-6 mt-5">
                                <p class="grassetto">Se SÌ, quante?</p>
                                <select id="n_controllate" name="n_controllate" class="custom-select">
                                  <option selected value="1">0</option>
                                  <option value="2">1</option>
                                  <option value="3">2</option>
                                  <option value="4">3</option>
                                  <option value="5">più di 4</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-row">
                            <div class="col-12 col-md-6 mt-5">
                                <p class="grassetto">Le controllate hanno sede nello stesso Paese della capogruppo?</p>
                                <div class="custom-control custom-radio custom-control-inline">
                                  <input type="radio" id="paesesi" name="paese" value="si" class="custom-control-input">
                                  <label class="custom-control-label" for="paesesi">Sì</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                  <input type="radio" id="paeseno" name="paese" value="no" class="custom-control-input">
                                  <label class="custom-control-label" for="paeseno">No</label>
                                </div>
                            </div>
                             <div class="col-12 col-md-6 mt-5">
                                <p class="grassetto">Siete in grado di creare rapidamente un report per una riunione?</p>
                                <div class="custom-control custom-radio custom-control-inline">
                                  <input type="radio" id="reportsi" name="report" value="si" class="custom-control-input">
                                  <label class="custom-control-label" for="reportsi">Sì</label>
                                </div>
                                <div class="custom-control custom-radio custom-control-inline">
                                  <input type="radio" id="reportno" name="report" value="no" class="custom-control-input">
                                  <label class="custom-control-label" for="reportno">No</label>
                                </div>
                            </div>
                        </div>
                        
                        <div class="form-row">
                            <div class="col-12 mt-5">
                                <p class="grassetto">In quale settore opera la tua azienda?</p>
                                <div class="custom-control custom-checkbox custom-control-inline">
                                    <input type="checkbox" class="custom-control-input" id="automotive">
                                    <label class="custom-control-label" for="automotive">Automotive</label>
                                </div> 
                                <div class="custom-control custom-checkbox custom-control-inline">
                                    <input type="checkbox" class="custom-control-input" id="agroalimentare">
                                    <label class="custom-control-label" for="agroalimentare">Agroalimentare</label>
                                </div> 
                                <div class="custom-control custom-checkbox custom-control-inline">
                                    <input type="checkbox" class="custom-control-input" id="consumo">
                                    <label class="custom-control-label" for="consumo">Prodotti di consumo</label>
                                </div> 
                                <div class="custom-control custom-checkbox custom-control-inline">
                                    <input type="checkbox" class="custom-control-input" id="istruzione">
                                    <label class="custom-control-label" for="istruzione">Istruzione</label>
                                </div> 
                                <div class="custom-control custom-checkbox custom-control-inline">
                                    <input type="checkbox" class="custom-control-input" id="food">
                                    <label class="custom-control-label" for="food">Food and Beverage</label>
                                </div> 
                                <div class="custom-control custom-checkbox custom-control-inline">
                                    <input type="checkbox" class="custom-control-input" id="manifattura">
                                    <label class="custom-control-label" for="manifattura">Manifattura</label>
                                </div>
                                <div class="custom-control custom-checkbox custom-control-inline">
                                    <input type="checkbox" class="custom-control-input" id="terzo">
                                    <label class="custom-control-label" for="terzo">No profit / Terzo settore</label>
                                </div> 
                                <div class="custom-control custom-checkbox custom-control-inline">
                                    <input type="checkbox" class="custom-control-input" id="professionali">
                                    <label class="custom-control-label" for="professionali">Servizi Professionali</label>
                                </div>
                                <div class="custom-control custom-checkbox custom-control-inline">
                                    <input type="checkbox" class="custom-control-input" id="commercio">
                                    <label class="custom-control-label" for="commercio">Commercio</label>
                                </div>
                                <div class="custom-control custom-checkbox custom-control-inline">
                                    <input type="checkbox" class="custom-control-input" id="distribuzione">
                                    <label class="custom-control-label" for="distribuzione">Grande distribuzione organizzata</label>
                                </div>
                                <div class="custom-control custom-checkbox custom-control-inline">
                                    <input type="checkbox" class="custom-control-input" id="settore_altro">
                                    <label class="custom-control-label" for="settore_altro">Altro</label>
                                </div>
                                </div>
                        </div>
                        
                        <div class="form-row">
                            <div class="col-12 mt-5">
                                <p class="grassetto">Inserisci i dati per essere ricontattato al più presto</p>
                            </div>
                            <div class="col-12 col-md-4">
                                <input class="form-control" id="form_nome" name="nome" placeholder="Nome e cognome">
                            </div>
                            <div class="col-12 col-md-4">
                                <input class="form-control" id="form_email" name="email" placeholder="Email">
                            </div>
                            <div class="col-12 col-md-4">
                                <input class="form-control" id="form_azienda" name="azienda" placeholder="Azienda">
                            </div>
                            <div class="col-12">
                            <div class="custom-control custom-checkbox custom-control-inline">
                                <input type="checkbox" class="custom-control-input" id="form_privacy">
                                <label class="custom-control-label" for="form_privacy">
                                    Dichiaro di aver letto l'<a target="_blank" href="https://www.iubenda.com/privacy-policy/19926816">informativa sulla privacy</a> ai sensi del GDPR e do il consenso al trattamento dei dati personali.</div>
                                </label>
                            </div>
                            </div>
                            <div class="col-12 col-md-3 mt-5">
                                <button type="button" class="btn btn-primary my-2" onclick="gmail()">Richiedi il preventivo</button>
                                <p id="form" class="successo col-12 alert alert-success" style="display:none">Invio riuscito</p>
                                <p id="form" class="errore col-11 alert alert-danger" style="display:none; color:red">Tutti i campi sono obbligatori.</p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
        <!-- Bannerino -->
        <div class="container-fluid py-5 catch sfondo-big" style="background:url('img/world.png')">
        <div class="container">
            <div class="row">
                <div class="col-12"><h3 class="white-t">Presente nelle <br>migliori aziende del mondo</h3></div>
            </div>
        </div>
        </div>
        
        
        <?php include ("tris.php"); ?>
        

        <?php include ("footer.php"); ?>
        <script>
    
            function gmail(){
                dipendenti = $('#dipendenti').val();
                fatturato = $('#fatturato').val();
                //finanziario
                software1 = 0;
                if($('#contabilita').prop('checked')) {contabilita = 1;software1 = 1;} else contabilita = 0; 
                if($('#fatturazione').prop('checked')) {fatturazione = 1;software1 = 1;} else fatturazione = 0; 
                if($('#paghe').prop('checked')) {paghe = 1;software1 = 1;} else paghe = 0; 
                if($('#banche').prop('checked')) {banche = 1;software1 = 1;} else banche = 0; 
                if($('#pagamenti').prop('checked')) {pagamenti = 1;software1 = 1;} else pagamenti = 0; 
                if($('#excel').prop('checked')) {excel = 1;software1 = 1;}else excel = 0;
                //gestionale
                software2 = 0;
                if($('#finanza').prop('checked')) {finanza = 1;software2 = 1;} else finanza = 0; 
                if($('#crm').prop('checked')) {crm = 1;software2 = 1;} else crm = 0; 
                if($('#vendite').prop('checked')) {vendite = 1;software2 = 1;} else vendite = 0; 
                if($('#ecommerce').prop('checked')) {ecommerce = 1;software2 = 1;} else ecommerce = 0; 
                if($('#software_altro').prop('checked')) {software_altro = 1;software2 = 1;} else software_altro = 0; 
                
                team = $('input[name="team-informatico"]:checked').val();
                database = $('input[name="database"]:checked').val();
                controllate = $('input[name="controllate"]:checked').val();
                n_controllate = $('#n_controllate').val();
                paese = $('input[name="paese"]:checked').val();
                report = $('input[name="report"]:checked').val();
                
                //settore
                settori = 0;
                if($('#automotive').prop('checked')) {automotive = 1;settori = 1;} else automotive = 0; 
                if($('#agroalimentare').prop('checked')) {agroalimentare = 1;settori = 1;} else agroalimentare = 0; 
                if($('#consumo').prop('checked')) {consumo = 1;settori = 1;} else consumo = 0; 
                if($('#istruzione').prop('checked')) {istruzione = 1;settori = 1;} else istruzione = 0; 
                if($('#food').prop('checked')) {food = 1;settori = 1;} else food = 0; 
                if($('#manifattura').prop('checked')) {manifattura = 1;settori = 1;} else manifattura = 0; 
                if($('#terzo').prop('checked')) {terzo = 1;settori = 1;} else terzo = 0; 
                if($('#professionali').prop('checked')) {professionali = 1;settori = 1;} else professionali = 0; 
                if($('#commercio').prop('checked')) {commercio = 1;settori = 1;} else commercio = 0; 
                if($('#distribuzione').prop('checked')) {distribuzione = 1;settori = 1;} else distribuzione = 0; 
                if($('#settore_altro').prop('checked')) {settore_altro = 1;settori = 1;} else settore_altro = 0; 
                
                nome = $('#form_nome').val();
                email = $('#form_email').val();
                azienda = $('#form_azienda').val();
                
                console.log(team);
                console.log(database);
                if(!$("#form_privacy").is(":not(:checked)") && (email != '') && (azienda != '') && (software1 != 0) && (software2 != 0) && ($('input[name="team-informatico"]').is(":checked")) && ($('input[name="database"]').is(":checked")) && ($('input[name="controllate"]').is(":checked")) && ($('input[name="paese"]').is(":checked")) && ($('input[name="report"]').is(":checked")) && (settori != 0)){
                    $.ajax({
                        async: false,
                        type: "POST",
                        url: "email_functions_valutazione.php",
                        data: {
                            "nome": nome,
                            "email": email,
                            "azienda": azienda,
                            "dipendenti": dipendenti,
                            "fatturato": fatturato,
                            "contabilita": contabilita,
                            "fatturazione": fatturazione,
                            "paghe": paghe,
                            "banche": banche,
                            "pagamenti": pagamenti,
                            "excel": excel,
                            "finanza": finanza,
                            "crm": crm,
                            "vendite": vendite,
                            "ecommerce": ecommerce,
                            "software_altro": software_altro,
                            "team":team,
                            "database": database,
                            "controllate": controllate,
                            "n_controllate": n_controllate,
                            "paese": paese,
                            "report": report,
                            "automotive": automotive,
                            "agroalimentare": agroalimentare,
                            "consumo": consumo,
                            "istruzione": istruzione,
                            "food": food,
                            "manifattura": manifattura,
                            "terzo": terzo,
                            "professionali": professionali,
                            "commercio": commercio,
                            "distribuzione": distribuzione,
                            "settore_altro": settore_altro
                        },
                        success: function () {
                            $('#form.errore').hide(100);
                            $('#form.successo').show(200);
                        }
                    });
                }
            else{
                $('#form.errore').show(200);
            }
                }
            
            
        </script>
    </body>
</html> 