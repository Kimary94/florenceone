<?php
use Melbahja\Seo\Factory;

// Load Composer's autoloader
require 'vendor/autoload.php';

$metatags = Factory::metaTags();
$title = "Florence One | Rivenditore OnBase Italia | OnBase by Hyland ";

$metatags->meta('author', 'Biznes')
		->meta('title', $title)
		->meta('description', 'OnBase by Hyland, Il più facile e flessibile ECM. Migliora l’organizzazione e la gestione di dati e documenti aziendali grazie all’eliminazione del cartaceo. Per un modo di lavorare nuovo, efficiente e con riduzione dei costi operativi.')
        ->meta('keywords', 'OnBase by Hyland, OnBase, ECM, digitalizzazione documenti, documenti aziendali digitali')
		->image('img/florence-one-onbase-hyland-1.jpg')
		->url('https://florence-one.it/on-base-hyland')
?>
<!DOCTYPE html>
<html lang="it">

<head>
	<title><?=$title?></title>
	<?=$metatags?>
    <?php include ("header.html"); ?>
</head>

<body>
    <!-- main wrapper -->
    <div id="wrapper">
        <div class="page-wrapper">
            <?php include ("menu.html"); ?>
            <main>
                <!-- visual/banner of the page -->
                <section class="visual">
                    <div class="visual-inner sap-business-one-banner dark-overlay parallax" data-stellar-background-ratio="0.55">
                        <div class="centered">
                            <div class="container">
                                	
                                    <img src="img/prodotti/logo-onbase-hyland-florence-one.png" alt="">
                                	</br></br>
                                    <div class="breadcrumb-block">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="/"> Home </a></li>
                                            <li class="breadcrumb-item active"> OnBase by Hyland </li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/visual/banner of the page -->
                <!-- main content wrapper -->
                <div class="content-wrapper">
                    <section class="content-block">
                        <div class="container text-center">
                        	
                            <div class="heading bottom-space">
                            	<h1 class="visual-title visual-sub-title" style="color:#55565B" >OnBase by Hyland</h1>
                                <h2>Il più facile e flessibile ECM</h2>
                            </div>
                            <div class="description">
                                <p>Migliora l’organizzazione e la gestione di dati e documenti aziendali grazie all’eliminazione del cartaceo. </br>
								Per un modo di lavorare nuovo, efficiente e con riduzione dei costi operativi.
 								</p>
                            </div>
                        </div>
                    </section>
                    <section class="content-block p-0">
                        <div class="container-fluid">
                            <div class="content-slot alternate-block">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="bg-stretch img-wrap wow slideInLeft">
                                            <img src="img/florence-one-onbase-hyland-1.jpg" alt="images">
                                        </div>
                                    </div>
                                    <div class="col col-lg-6">
                                        <div class="text-wrap">
                                            <h3>OnBase. Gestione centralizzata dei contenuti aziendali</h3>
                                            <p>In azienda viaggiano in entrata e in uscita informazioni, dati e documenti: la loro consultazione, compilazione e il passaggio tra vari livelli e step operativi richiede spesso molto tempo.</p>
                                            <p>Affidarsi al solo lavoro manuale non assicura l’eliminazione di errori e si traduce in costi operativi aggiuntivi.</p>
                                            <p>OnBase ti permette di gestire il flusso di informazioni da un solo applicativo, in modo semplice e veloce. Accedere a tutte le informazioni in tempo reale significa migliorare l’organizzazione con effetti positivi in termini di efficienza.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="bg-stretch img-wrap wow slideInRight">
                                            <img src="img/florence-one-onbase-hyland-2.jpg" alt="images">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="text-wrap">
                                            <h3>L’ECM che cresce insieme a te</h3>
                                            <p>OnBase è uno tra gli ECM più facili e flessibili. La sua struttura modulare consente di configurarlo seguendo l’organizzazione delle aree interne all’azienda e di crescere insieme a te.</p>
                                            <p>Ampliare le sue funzioni è facile: puoi intervenire in autonomia e in modo rapido con pochi click del mouse, creando nuove soluzioni senza necessità di codice personalizzato.</p>
                                            <p>Con OnBase puoi organizzare il tuo personale in ruoli e consentire accesi su vari livelli monitorando le operazioni eseguite. </p>
                                            <p></p>
                                            <a href="brochure/Brochure_OnBase.pdf" target="_blank" class="btn btn-black has-radius-small">Scarica la brochure</a>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </section>
                    <section class="content-block">
                        <div class="container">
                            <div class="row multiple-row v-align-row">
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="block-heading">
                                            <h3 class="block-top-heading">OnBase</h3>
                                            <h2 class="block-main-heading">FUNZIONI PRINCIPALI</h2>
                                            <span class="block-sub-heading">OnBase è una soluzione facile per la gestione centralizzata di informazioni e documenti che ti permette di accelerare i processi grazie all’automatizzazione delle attività.</span>
                                            <div class="divider"><img src="img/divider.png" alt="images description"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">ACQUISIZIONE</h4>
                                            <div class="des">
                                                <p>Digitalizza, importa ed estrai documenti in un unico sistema di controllo.</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">ELABORAZIONE</h4>
                                            <div class="des">
                                                <p>Elabora il flusso di informazioni creando soluzioni su misura e personalizzate.</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">AUTOMAZIONE</h4>
                                            <div class="des">
                                                <p>Automatizza le operazioni ripetute dei processi e le decisioni prevedibili.</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">ACCESSIBILITÀ</h4>
                                            <div class="des">
                                                <p>Accedi alle informazioni sia online che offline e da qualsiasi dispositivo.</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">INTEGRAZIONE</h4>
                                            <div class="des">
                                                <p>Perfettamente integrabile con oltre 500 applicazioni per un controllo totale.</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">ANALISI</h4>
                                            <div class="des">
                                                <p>Visibilità in tempo reale su processi e misurazione delle metriche chiave.</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">ARCHIVIAZIONE</h4>
                                            <div class="des">
                                                <p>Crea copie elettroniche su più sever e ubicazioni, accedi in modo rapido ai dati.</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">SICUREZZA</h4>
                                            <div class="des">
                                                <p>Decidi chi può eccedere e le operazioni che possono essere eseguite.</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="content-block p-0">
                        <div class="container-fluid">
                            <div class="content-slot alternate-block">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="bg-stretch img-wrap wow slideInLeft">
                                            <img src="img/florence-one-onbase-hyland-3.jpg" alt="images">
                                        </div>
                                    </div>
                                    <div class="col col-lg-6">
                                        <div class="text-wrap">
                                            <h3>Richiedi subito una demo</h3>
                                            <p>Compila il form e scopri nel dettaglio tutte le funzionalità offerte da OnBase!</p>
                                            <p></p>
                                        <!-- contact form -->
                                        <?php include ("form-prodotti.html"); ?>
                                    
                                            
                                            
                                            
                                            
                                            
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="bg-stretch img-wrap wow slideInRight">
                                            <img src="img/florence-one-onbase-hyland-4.jpg" alt="images">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="text-wrap">
                                            <h3>Tutti i dati in un unico sistema</h3>
                                            <p>Utilizzare OnBase significa raccogliere tutti i dati e documenti aziendali in un unico sistema, recuperando spazi fisici e risparmiando il tempo da dedicare alla gestione del cartaceo.  </p>
                                            <p>Con la digitalizzazione puoi offrire un servizio clienti superiore, come la compilazione di documenti e form direttamente online, e migliorare l’organizzazione interna dell’azienda. </p>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </section>
                    <section class="content-block quotation-block black-overlay-6 parallax" data-stellar-background-ratio="0.55">
                        <div class="container">
                            <div class="inner-wrapper">
                                <h3 class="block-top-heading text-white">LE MIGLIORI TECNOLOGIE</h3>
                                <h2 class="text-white">OnBase by Hyland</br>presente nelle migliori aziende</h2>
                                <div class="btn-container">
                                    <a href="contatti" class="btn btn-primary">Richiedi una valutazione</a>
                                </div>
                            </div>
                        </div>
                    </section>
                    <?php include ("sezione_clienti.html"); ?>
                </div>
                <!--/main content wrapper -->
            </main>
        </div>
        <?php include ("footer.html"); ?>
    <!-- jquery library -->
    <script src="vendors/jquery/jquery-2.1.4.min.js"></script>
    <!-- external scripts -->
    <script src="vendors/tether/dist/js/tether.min.js"></script>
    <script src="vendors/bootstrap/js/bootstrap.min.js"></script>
    <script src="vendors/stellar/jquery.stellar.min.js"></script>
    <script src="vendors/isotope/javascripts/isotope.pkgd.min.js"></script>
    <script src="vendors/isotope/javascripts/packery-mode.pkgd.js"></script>
    <script src="vendors/owl-carousel/dist/owl.carousel.js"></script>
    <script src="vendors/waypoint/waypoints.min.js"></script>
    <script src="vendors/counter-up/jquery.counterup.min.js"></script>
    <script src="vendors/fancyBox/source/jquery.fancybox.pack.js"></script>
    <script src="vendors/fancyBox/source/helpers/jquery.fancybox-thumbs.js"></script>
    <script src="vendors/image-stretcher-master/image-stretcher.js"></script>
    <script src="vendors/wow/wow.min.js"></script>
    <script src="vendors/rateyo/jquery.rateyo.js"></script>
    <script src="vendors/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="vendors/bootstrap-slider-master/src/js/bootstrap-slider.js"></script>
    <script src="vendors/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
    <script src="js/mega-menu.js"></script>
    <!-- custom jquery script -->
    <script src="js/jquery.main.js"></script>
    <!-- REVOLUTION JS FILES -->
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/jquery.themepunch.revolution.min.js"></script>
    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.migration.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.video.min.js"></script>
    <!-- SNOW ADD ON -->
    <script type="text/javascript" src="vendors/rev-slider/revolution-addons/snow/revolution.addon.snow.min.js"></script>
        <!-- revolutions slider script -->
    <script src="js/revolution.js"></script>
</html>
