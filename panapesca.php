<?php
use Melbahja\Seo\Factory;

// Load Composer's autoloader
require 'vendor/autoload.php';

$metatags = Factory::metaTags();
$title = "Florence One | Rivenditore SAP Italia | Panapesca ";

$metatags->meta('author', 'Biznes')
		->meta('title', $title)
		->meta('description', 'Scopri il caso di successo. Panapesca è leader in Italia nella lavorazione, commercializzazione e distribuzione di prodotti ittici. Da oltre 40 anni garantisce alta qualità e sicurezza dei suoi prodotti.')
        ->meta('keywords', 'SAP Business One, SAP HANA, ERP, software gestionale, software contabilità, Florence One')
		->image('img/clienti/FLORENCE-ONE-panapesca.jpg')
		->url('https://florence-one.it/panapesca')
?>
<!DOCTYPE html>
<html lang="it">

<head>
	<title><?=$title?></title>
	<?=$metatags?>
    <?php include ("header.html"); ?>
</head>

<body>
    <!-- main wrapper -->
    <div id="wrapper">
        <div class="page-wrapper">
            <!-- header of the page -->
            <?php include ("menu.html"); ?>
            <!--/header of the page -->
            <main>
            	<section class="visual">
                    <div class="visual-inner sap-business-one-banner dark-overlay parallax" data-stellar-background-ratio="0.55">
                        <div class="centered">
                            <div class="container">
                                	
                                    <h1 class="visual-title visual-sub-title">PANAPESCA</h1>
                                    <div class="breadcrumb-block">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="/"> Home </a></li>
                                            <li class="breadcrumb-item"><a href="aziende-commerciali-e-servizi"> Commerciali e servizi </a></li>
                                            <li class="breadcrumb-item active"> Panapesca </li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- main content wrapper -->
                <div class="content-wrapper">
                    <section class="content-block">
                        <div class="container">
                            <div class="row mt-5">
                                <div class="col-lg-12 less-wide">
                                    <div class="blog-holder" style="padding-bottom:50px">
                                        <article class="blog-article">
                                                
                                            <div class="blog-desc pt-5">
                                                <div class="blog-img">
                                                    <div class="image-wrap">
                                                        <figure class="">
                                                            <img src="img/clienti/FLORENCE-ONE-panapesca.jpg" alt="images description">
                                                        </figure>
                                                    </div>
                                                </div>
                                                <div class="blog-share mt-5">
                                                    <ul class="social-network with-text">
                                                        <li><strong>Info</strong></li>
                                                        <li>Cliente: Panapesca</li>
                                                        <li><a href="https://www.panapesca.eu/it">Sito: https://www.panapesca.eu</a></li>
                                                    </ul>
                                                </div>
                                                <div class="blockquote-block blockquote-block-v2 pt-3 pb-3">
                                                    <blockquote>
                                                        <p>La storia</p>
                                                    </blockquote>
                                                </div>
                                                <p>Panapesca è leader in Italia nella lavorazione, commercializzazione e distribuzione di prodotti ittici Da oltre 40 anni garantisce l'alta qualità e sicurezza dei suoi prodotti con il massimo rispetto per l'ambiente marino, in modo da assicurarne uno sviluppo sostenibile Grazie ad uno staff di professionisti operanti in oltre 50 paesi del mondo, Panapesca seleziona il pesce migliore, controllando e garantendo tutte le fasi della filiera La distribuzione è multicanale GDO, industria alimentare, mercato all'ingrosso, ristorazione, catering, dettaglio L'azienda si impegna sempre per garantire la massima sicurezza e qualità Panapesca infatti dispone di un laboratorio interno nel quale effettua più di 2 500 analisi ogni anno, oltre ad affidarsi a laboratori esterni accreditati di primo livello per verificare la qualità e la sicurezza igienica di tutti i prodotti offerti L'obiettivo primario è quello di offrire sempre prodotti ottimi e sani, garantendo i massimi standard in termini di qualità e tracciabilità, assicurate dall'automazione della logistica e dei processi di lavorazione</p>
                                                <div class="blockquote-block blockquote-block-v2 pt-3 pb-3">
                                                    <blockquote>
                                                        <p>La sfida</p>
                                                    </blockquote>
                                                </div>
                                                <p>Panapesca aveva la necessità di gestire in modo centralizzato l'intero ciclo di vita dell'ordine cliente, passando per acquisti, logistica e produzione Aveva inoltre bisogno di ottenere una base dati disponibile per statistiche online Panapesca ricercava standard tecnologici avanzati in grado di lavorare in ambienti desktop, web e mobile con grande facilità di utilizzo e con la capacità di gestire le diverse fiscalità internazionali</p>
                                                <div class="blockquote-block blockquote-block-v2 pt-3 pb-3">
                                                    <blockquote>
                                                        <p>La soluzione</p>
                                                    </blockquote>
                                                </div>
                                                <p>Le attività di Florence One in Italia e Thailandia hanno previsto l'implementazione di una specifica app mobile Android per dispositivi palmari finalizzata alla gestione dei magazzini e alla produzione L'introduzione di SAP Business One ha consentito di automatizzare i processi di acquisto, produzione, logistica e vendita Panapensca ha inoltre incrementato notevolmente la propria capacità statistica grazie agli analytics di SAP HANA</p>
                                                <div class="blog-share mt-5">
                                                    <ul class="social-network with-text">
                                                        
                                                    </ul>
                                                </div>
                                                
                                            </div>
                                        </article>
                                    </div>
                                    <div class="contact-container">
                                        <form action="#" class="comment-form waituk_contact-form">
                                            <fieldset>
                                            	<div class="btn-container">
                                    				<a href="contatti" class="btn btn-primary">Unisciti ai nostri clienti</a>
                               					</div>
                               					<!--
                                                <h6 class="content-title contact-title"><a href="contatti">Entra a far parte del mondo Florence One</a></h6>
                                                
                                                <div class="row">
                                                    <div class="col-sm-6 form-group">
                                                        <input placeholder="Nome" type="text" class="form-control">
                                                    </div>
                                                    <div class="col-sm-6 form-group">
                                                        <input placeholder="Email" type="email" class="form-control">
                                                    </div>
                                                    <div class="col-sm-12 form-group">
                                                        <input placeholder="Azienda" type="text" class="form-control">
                                                    </div>
                                                    <div class="col-sm-12 form-group">
                                                        <textarea placeholder="Messaggio" class="form-control"></textarea>
                                                    </div>
                                                    <div class="col-sm-12 btn-holder">
                                                        <button type="submit" class="btn btn-black btn-full">Invia richiesta</button>
                                                    </div>
                                            
                                        </form>
                                        </div>
                                        -->
                                        </fieldset>
                                    </div>
                                </div>
                            </div>
                    </section>
                    </div>
                    <!--/main content wrapper -->
            </main>
            </div>
            <!-- footer of the pagse -->
        <?php include ("footer.html"); ?>
        <!-- jquery library -->
        <script src="vendors/jquery/jquery-2.1.4.min.js"></script>
        <!-- external scripts -->
        <script src="vendors/tether/dist/js/tether.min.js"></script>
        <script src="vendors/bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/stellar/jquery.stellar.min.js"></script>
        <script src="vendors/isotope/javascripts/isotope.pkgd.min.js"></script>
        <script src="vendors/isotope/javascripts/packery-mode.pkgd.js"></script>
        <script src="vendors/owl-carousel/dist/owl.carousel.js"></script>
        <script src="vendors/waypoint/waypoints.min.js"></script>
        <script src="vendors/counter-up/jquery.counterup.min.js"></script>
        <script src="vendors/fancyBox/source/jquery.fancybox.pack.js"></script>
        <script src="vendors/fancyBox/source/helpers/jquery.fancybox-thumbs.js"></script>
        <script src="vendors/image-stretcher-master/image-stretcher.js"></script>
        <script src="vendors/wow/wow.min.js"></script>
        <script src="vendors/rateyo/jquery.rateyo.js"></script>
        <script src="vendors/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script src="vendors/bootstrap-slider-master/src/js/bootstrap-slider.js"></script>
        <script src="vendors/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
        <script src="js/mega-menu.js"></script>
        <!-- custom jquery script -->
        <script src="js/jquery.main.js"></script>
        <!-- REVOLUTION JS FILES -->
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/jquery.themepunch.tools.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/jquery.themepunch.revolution.min.js"></script>
        <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.actions.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.migration.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.video.min.js"></script>
        <!-- SNOW ADD ON -->
        <script type="text/javascript" src="vendors/rev-slider/revolution-addons/snow/revolution.addon.snow.min.js"></script>
        <!-- revolutions slider script -->
        <script src="js/revolution.js"></script>

</html>
