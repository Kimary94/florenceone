<?php
use Melbahja\Seo\Factory;

// Load Composer's autoloader
require 'vendor/autoload.php';

$metatags = Factory::metaTags();
$title = "Florence One | Rivenditore SAP Italia | SAP Business ByDesign ";

$metatags->meta('author', 'Biznes')
		->meta('title', $title)
		->meta('description', 'SAP Business One, ERP scalabile e full cloud. Traduci il tuo potenziale in risultati raggiunti. Con SAP Business ByDesign il tuo business non avrà limiti né confini, in una crescita costante e rapida.')
        ->meta('keywords', 'SAP Business ByDesign, SAP HANA, ERP, software gestionale, software contabilità, full cloud')
		->image('img/florence-one-sap-business-bydesign-1.jpg')
		->url('https://florence-one.it/sap-business-bydesign')
?>
<!DOCTYPE html>
<html lang="it">

<head>
	<title><?=$title?></title>
	<?=$metatags?>
    <?php include ("header.html"); ?>
</head>

<body>
    <!-- main wrapper -->
    <div id="wrapper">
        <div class="page-wrapper">
            <?php include ("menu.html"); ?>
            <main>
                <!-- visual/banner of the page -->
                <section class="visual">
                    <div class="visual-inner sap-business-one-banner dark-overlay parallax" data-stellar-background-ratio="0.55">
                        <div class="centered">
                            <div class="container">
                                	<img src="img/prodotti/logo-sap-business-bydesign-florence-one.png" alt="">
                                    </br></br>
                                    <div class="breadcrumb-block">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="/"> Home </a></li>
                                            <li class="breadcrumb-item active"> SAP Business ByDesign </li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/visual/banner of the page -->
                <!-- main content wrapper -->
                <div class="content-wrapper">
                    <section class="content-block">
                        <div class="container text-center">
                        	
                            <div class="heading bottom-space">
                            	<h1 class="visual-title visual-sub-title" style="color:#55565B" >SAP Business ByDesign</h1>
                                <h2>L’ERP scalabile e full cloud</h2>
                            </div>
                            <div class="description">
                                <p>Traduci il tuo potenziale in risultati raggiunti.</br>
								Con SAP Business ByDesign il tuo business non avrà limiti né confini, in una crescita costante e rapida. 
								</p>
                            </div>
                        </div>
                    </section>
                    <section class="content-block p-0">
                        <div class="container-fluid">
                            <div class="content-slot alternate-block">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="bg-stretch img-wrap wow slideInLeft">
                                            <img src="img/florence-one-sap-business-bydesign-1.jpg" alt="images">
                                        </div>
                                    </div>
                                    <div class="col col-lg-6">
                                        <div class="text-wrap">
                                            <h3>SAP Business ByDesign, per il business in crescita</h3>
                                            <p>Non incontrare limiti nella crescita del tuo business. SAP Business ByDesign è l’ERP studiato e progettato per Start up ed aziende in continua e rapida crescita.</p>
                                            <p>Una soluzione flessibile e facilmente scalabile che si adatta facilmente alla struttura di aziende dinamiche.</p>
                                            <p>SAP Business ByDesign ti permette di passare da 20 a 10.000 utenti seguendo l’espansione della tua azienda e adattandosi anche a divisioni interne e sedi estere.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="bg-stretch img-wrap wow slideInRight">
                                            <img src="img/florence-one-sap-business-bydesign-2.jpg" alt="images">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="text-wrap">
                                            <h3>La tua suite 100% cloud.</h3>
                                            <p>L’ERP full cloud che ti farà dimenticare la necessità di avere server o infrastrutture IT locali, con vantaggi apprezzabili in termini di complessità e costi. </p>
                                            <p>Potrai accedere alla suite semplicemente tramite il browser Web e la sua soluzione totalmente cloud ti farà conoscere la comodità dell’utilizzo anche da mobile.</p>
                                            <p>Rendi flessibile la tua struttura con la soluzione SAP Business ByDesign. </p>
                                            <p></p>
                                            <a href="brochure/Brochure_SAP_Business_ByDesign.pdf" target="_blank" class="btn btn-black has-radius-small">Scarica la brochure</a>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </section>
                    <section class="content-block">
                        <div class="container">
                            <div class="row multiple-row v-align-row">
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="block-heading">
                                            <h3 class="block-top-heading">SAP Business ByDesign</h3>
                                            <h2 class="block-main-heading">FUNZIONI PRINCIPALI</h2>
                                            <span class="block-sub-heading">SAP Business ByDesign è l’ERP flessibile, scalabile e completamente in cloud che crescerà insieme alla tua azienda in modo facile per un business senza limiti</span>
                                            <div class="divider"><img src="img/divider.png" alt="images description"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">CONTABILITÀ</h4>
                                            <div class="des">
                                                <p>Gestione della contabilità della tua azienda, sia analitica che gestionale.</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">IMPOSTE</h4>
                                            <div class="des">
                                                <p>Imposta i parametri per la definizione e il controllo delle imposte.</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">FINANZA</h4>
                                            <div class="des">
                                                <p>Effettua transazioni monetarie legate ai tuoi conti correnti bancari.</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">CONTROLLING</h4>
                                            <div class="des">
                                                <p>Tieni monitorala la redditività delle tue attività aziendali con analisi dedicate.</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">VENDITE</h4>
                                            <div class="des">
                                                <p>Possibilità di creare offerte personalizzate per i tuoi clienti.</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">ACQUISTI</h4>
                                            <div class="des">
                                                <p>Gestisci l’intero ciclo di acquisto, con report per l’analisi delle informazioni.</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">GESITONE ATTIVITÀ</h4>
                                            <div class="des">
                                                <p>Coordina le interazioni con gli agenti commerciali da un'unica piattaforma.</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">SERVICE</h4>
                                            <div class="des">
                                                <p>App mobile di gestione del servizio ticketing e per la risoluzione dei problemi.</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="content-block p-0">
                        <div class="container-fluid">
                            <div class="content-slot alternate-block">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="bg-stretch img-wrap wow slideInLeft">
                                            <img src="img/florence-one-sap-business-bydesign-3.jpg" alt="images">
                                        </div>
                                    </div>
                                    <div class="col col-lg-6">
                                        <div class="text-wrap">
                                            <h3>Richiedi subito una demo</h3>
                                            <p>Vuoi capire se Sap Business ByDesign è adatto alla tua azienda?</br>Compila il form!</p>
                                            <p></p>
                                        <!-- contact form -->
                                        <?php include ("form-prodotti.html"); ?>
                                    
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="bg-stretch img-wrap wow slideInRight">
                                            <img src="img/florence-one-sap-business-bydesign-4.jpg" alt="images">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="text-wrap">
                                            <h3>Diventa competitivo</h3>
                                            <p>SAP Business ByDesign è il miglior alleato della tua crescita aziendale. Tu e il tuo team avrete a disposizione strumenti giusti ed efficaci per lavorare in modo integrato ed efficiente condividendo dati e informazioni. </p>
                                            <p>Puoi comunicare con diversi programmi per una trasparenza totale dell’organizzazione, una riduzione della complessità e dei costi.</p>
                                            <p>Diventa competitivo! </p>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </section>
                    <section class="content-block quotation-block black-overlay-6 parallax" data-stellar-background-ratio="0.55">
                        <div class="container">
                            <div class="inner-wrapper">
                                <h3 class="block-top-heading text-white">LE MIGLIORI TECNOLOGIE</h3>
                                <h2 class="text-white">SAP Business ByDesign</br>presente nelle migliori aziende</h2>
                                <div class="btn-container">
                                    <a href="contatti" class="btn btn-primary">Richiedi una valutazione</a>
                                </div>
                            </div>
                        </div>
                    </section>
                    <?php include ("sezione_clienti.html"); ?>
                </div>
                <!--/main content wrapper -->
            </main>
        </div>
        <?php include ("footer.html"); ?>
    <!-- jquery library -->
    <script src="vendors/jquery/jquery-2.1.4.min.js"></script>
    <!-- external scripts -->
    <script src="vendors/tether/dist/js/tether.min.js"></script>
    <script src="vendors/bootstrap/js/bootstrap.min.js"></script>
    <script src="vendors/stellar/jquery.stellar.min.js"></script>
    <script src="vendors/isotope/javascripts/isotope.pkgd.min.js"></script>
    <script src="vendors/isotope/javascripts/packery-mode.pkgd.js"></script>
    <script src="vendors/owl-carousel/dist/owl.carousel.js"></script>
    <script src="vendors/waypoint/waypoints.min.js"></script>
    <script src="vendors/counter-up/jquery.counterup.min.js"></script>
    <script src="vendors/fancyBox/source/jquery.fancybox.pack.js"></script>
    <script src="vendors/fancyBox/source/helpers/jquery.fancybox-thumbs.js"></script>
    <script src="vendors/image-stretcher-master/image-stretcher.js"></script>
    <script src="vendors/wow/wow.min.js"></script>
    <script src="vendors/rateyo/jquery.rateyo.js"></script>
    <script src="vendors/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="vendors/bootstrap-slider-master/src/js/bootstrap-slider.js"></script>
    <script src="vendors/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
    <script src="js/mega-menu.js"></script>
    <!-- custom jquery script -->
    <script src="js/jquery.main.js"></script>
    <!-- REVOLUTION JS FILES -->
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/jquery.themepunch.revolution.min.js"></script>
    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.migration.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.video.min.js"></script>
    <!-- SNOW ADD ON -->
    <script type="text/javascript" src="vendors/rev-slider/revolution-addons/snow/revolution.addon.snow.min.js"></script>
        <!-- revolutions slider script -->
    <script src="js/revolution.js"></script>
</html>
