<?php
use Melbahja\Seo\Factory;

// Load Composer's autoloader
require 'vendor/autoload.php';

$metatags = Factory::metaTags();
$title = "Florence One | Rivenditore SAP Italia | SAP Business One ";

$metatags->meta('author', 'Biznes')
		->meta('title', $title)
		->meta('description', 'SAP Business One, leader nel mercato ERP, La soluzione per una gestione veloce, totale e ottimizzata. Con L’ERP di SAP puoi automatizzare processi e controlli e dedicarti ad eventi determinanti per la crescita del tuo business. .')
        ->meta('keywords', 'SAP Business One, SAP HANA, ERP, software gestionale, software contabilità')
		->image('img/florence-one-sap-business-one-1.jpg')
		->url('https://florence-one.it/sap-business-one')
?>
<!DOCTYPE html>
<html lang="it">

<head>
	<title><?=$title?></title>
	<?=$metatags?>
    <?php include ("header.html"); ?>
</head>

<body>
    <!-- main wrapper -->
    <div id="wrapper">
        <div class="page-wrapper">
            <?php include ("menu.html"); ?>
            <main>
                <!-- visual/banner of the page -->
                <section class="visual">
                    <div class="visual-inner sap-business-one-banner dark-overlay parallax" data-stellar-background-ratio="0.55">
                        <div class="centered">
                            <div class="container">
                                	<img src="img/prodotti/logo-sap-business-one-florence-one.png" alt="">
                                    </br></br>
                                    <div class="breadcrumb-block">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="/"> Home </a></li>
                                            <li class="breadcrumb-item active"> SAP Business One </li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/visual/banner of the page -->
                <!-- main content wrapper -->
                <div class="content-wrapper">
                    <section class="content-block">
                        <div class="container text-center">
                        	
                            <div class="heading bottom-space">
                            	<h1 class="visual-title visual-sub-title" style="color:#55565B" >SAP Business One</h1>
                                <h2>Leader nel mercato ERP</h2>
                            </div>
                            <div class="description">
                                <p>La soluzione per una gestione veloce, totale e ottimizzata.</br> 
									Con L’ERP di SAP puoi automatizzare processi e controlli e dedicarti ad eventi determinanti per la crescita del tuo business.
								</p>
                            </div>
                        </div>
                    </section>
                    <section class="content-block p-0">
                        <div class="container-fluid">
                            <div class="content-slot alternate-block">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="bg-stretch img-wrap wow slideInLeft">
                                            <img src="img/florence-one-sap-business-one-1.jpg" alt="images">
                                        </div>
                                    </div>
                                    <div class="col col-lg-6">
                                        <div class="text-wrap">
                                            <h3>SAP Business One: controllo e semplificazione</h3>
                                            <p>Studiato per le piccole e medie imprese, SAP Business One è l’ERP flessibile che si adatta ad ogni settore grazie alla struttura modulare e altamente personalizzabile.</p>
                                            <p>Con oltre 500 add-on, scegli solo ciò che ti serve e crea la soluzione più adatta al tuo business.</p>
                                            <p>SAP Business One è controllo e semplificazione SAP Business One è controllo e semplificazione per la tua azienda. Un’unica interfaccia, progettata per essere user-friendly, ti consente di avere sempre la situazione sotto controllo attraverso analisi real-time.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="bg-stretch img-wrap wow slideInRight">
                                            <img src="img/florence-one-sap-business-one-2.jpg" alt="images">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="text-wrap">
                                            <h3>L’ERP potente e accessibile</h3>
                                            <p>SAP Business One è uno strumento potente capace di aiutarti nella crescita del tuo business ma accessibile in termini di costo e facile da implementare.</p>
                                            <p>Sia che tu scelga la soluzione in cloud o on-premise le funzionalità offerte sono le stesse, con best practice specifiche per ogni settore.</p>
                                            <p>Una soluzione completa anche per chi desidera portare il proprio business oltre i confini perché configurabile per più di 170 paesi, 28 lingue e 50 diverse valute. </p>
                                            <p></p>
                                            <a href="brochure/Brochure_SAP_Business_One.pdf" target="_blank" class="btn btn-black has-radius-small">Scarica la brochure</a>
                                        </div>
                                        
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </section>
                    <section class="content-block">
                        <div class="container">
                            <div class="row multiple-row v-align-row">
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="block-heading">
                                            <h3 class="block-top-heading">SAP Business One</h3>
                                            <h2 class="block-main-heading">FUNZIONI PRINCIPALI</h2>
                                            <span class="block-sub-heading">SAP Business One è il Software Gestionale completo e integrato, progettato per le imprese e per crescere con loro: è flessibile, modulare, potente, semplice da usare.</span>
                                            <div class="divider"><img src="img/divider.png" alt="images description"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">CONTABILITÀ</h4>
                                            <div class="des">
                                                <p>Gestione della contabilità della tua azienda, sia analitica che gestionale.</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">IMPOSTE</h4>
                                            <div class="des">
                                                <p>Imposta i parametri per la definizione e il controllo delle imposte.</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">BANCHE</h4>
                                            <div class="des">
                                                <p>Effettua transazioni monetarie legate ai tuoi conti correnti bancari.</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">CONTROLLING</h4>
                                            <div class="des">
                                                <p>Tieni monitorata la redditività della tue attività aziendali con analisi dedicate.</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">VENDITE</h4>
                                            <div class="des">
                                                <p>Possibilità di creare offerte personalizzate per i tuoi clienti.</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">ACQUISTI</h4>
                                            <div class="des">
                                                <p>Gestisci l’intero ciclo di acquisto, con report per l’analisi delle informazioni.</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">GESITONE ATTIVITÀ</h4>
                                            <div class="des">
                                                <p>Coordina le interazioni con gli agenti commerciali da un'unica piattaforma.</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">MOBILE</h4>
                                            <div class="des">
                                                <p>Diverse app mobile per utilizzare le funzioni di SAP ovunque ti trovi.</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="content-block p-0">
                        <div class="container-fluid">
                            <div class="content-slot alternate-block">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="bg-stretch img-wrap wow slideInLeft">
                                            <img src="img/florence-one-sap-business-one-3.jpg" alt="images">
                                        </div>
                                    </div>
                                    <div class="col col-lg-6">
                                        <div class="text-wrap">
                                            <h3>Richiedi subito una demo</h3>
                                            <p>Vuoi una dimostrazione di tutte le potenzialità offerte da SAP Business One?</p>
                                            <p></p>
                                        <!-- contact form -->
                                        <?php include ("form-prodotti.php"); ?>
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="bg-stretch img-wrap wow slideInRight">
                                            <img src="img/florence-one-sap-business-one-4.jpg" alt="images">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="text-wrap">
                                            <h3>Migliora le tue prestazioni</h3>
                                            <p>Scegliere la soluzione ERP di SAP significa poter velocizzare le operazioni, automatizzare i processi e migliorare le prestazioni per un crescita aziendale in termini di competitività, capacità e redditività. </p>
                                            <p>Grazie alla visione globale e in tempo reale delle aree-chiave puoi adottare strategie mirate e intervenire tempestivamente e in maniera intelligente in caso di criticità e problematiche.</p>
                                            <p>Decidi di migliorare le prestazione della tua azienda con SAP Business One. </p>
                                        </div>
                                    </div>
                                </div>
                                <!--
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="bg-stretch img-wrap wow slideInLeft">
                                            <img src="img/img-11.jpg" alt="images">
                                        </div>
                                    </div>
                                    
                                </div>
                                -->
                            </div>
                        </div>
                    </section>
                    <section class="content-block quotation-block black-overlay-6 parallax" data-stellar-background-ratio="0.55">
                        <div class="container">
                            <div class="inner-wrapper">
                                <h3 class="block-top-heading text-white">LE MIGLIORI TECNOLOGIE</h3>
                                <h2 class="text-white">SAP Business One</br>presente nelle migliori aziende</h2>
                                <div class="btn-container">
                                    <a href="contatti" class="btn btn-primary">Richiedi una valutazione</a>
                                </div>
                            </div>
                        </div>
                    </section>
                    <?php include ("sezione_clienti.html"); ?>
                </div>
                <!--/main content wrapper -->
            </main>
        </div>
        <?php include ("footer.html"); ?>
    <!-- jquery library -->
    <script src="vendors/jquery/jquery-2.1.4.min.js"></script>
    <!-- external scripts -->
    <script src="vendors/tether/dist/js/tether.min.js"></script>
    <script src="vendors/bootstrap/js/bootstrap.min.js"></script>
    <script src="vendors/stellar/jquery.stellar.min.js"></script>
    <script src="vendors/isotope/javascripts/isotope.pkgd.min.js"></script>
    <script src="vendors/isotope/javascripts/packery-mode.pkgd.js"></script>
    <script src="vendors/owl-carousel/dist/owl.carousel.js"></script>
    <script src="vendors/waypoint/waypoints.min.js"></script>
    <script src="vendors/counter-up/jquery.counterup.min.js"></script>
    <script src="vendors/fancyBox/source/jquery.fancybox.pack.js"></script>
    <script src="vendors/fancyBox/source/helpers/jquery.fancybox-thumbs.js"></script>
    <script src="vendors/image-stretcher-master/image-stretcher.js"></script>
    <script src="vendors/wow/wow.min.js"></script>
    <script src="vendors/rateyo/jquery.rateyo.js"></script>
    <script src="vendors/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="vendors/bootstrap-slider-master/src/js/bootstrap-slider.js"></script>
    <script src="vendors/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
    <script src="js/mega-menu.js"></script>
    <!-- custom jquery script -->
    <script src="js/jquery.main.js"></script>
    <!-- REVOLUTION JS FILES -->
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/jquery.themepunch.revolution.min.js"></script>
    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.migration.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.video.min.js"></script>
    <!-- SNOW ADD ON -->
    <script type="text/javascript" src="vendors/rev-slider/revolution-addons/snow/revolution.addon.snow.min.js"></script>
        <!-- revolutions slider script -->
    <script src="js/revolution.js"></script>
</html>
