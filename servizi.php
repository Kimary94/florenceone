<?php
use Melbahja\Seo\Factory;

// Load Composer's autoloader
require 'vendor/autoload.php';

$metatags = Factory::metaTags();
$title = "Florence One | Rivenditore SAP Italia | Servizi ";

$metatags->meta('author', 'Biznes')
		->meta('title', $title)
		->meta('description', 'Florence One, scopri i nostri servizi. Digitalizza la tua azienda con servizi come enteprise content, cyber security, software custom, IT consulting, reti aziendali.')
        ->meta('keywords', 'enteprise content, cyber security, software custom, IT consulting, reti aziendali, database design, e-commerce, marketplace, MES')
		->image('img/florence-one-digital-integration.jpg')
		->url('https://florence-one.it/servizi')
?>
<!DOCTYPE html>
<html lang="it">

<head>
	<title><?=$title?></title>
	<?=$metatags?>
    <?php include ("header.html"); ?>
</head>

<body>
    <!-- main wrapper -->
    <div id="wrapper">
        <div class="page-wrapper">
            <?php include ("menu.html"); ?>
            <main>
                <!-- visual/banner of the page -->
                <section class="visual">
                    <div class="visual-inner sap-business-one-banner dark-overlay parallax" data-stellar-background-ratio="0.55">
                        <div class="centered">
                            <div class="container">
                                	
                                    <h1 class="visual-title visual-sub-title">Servizi</h1>
                                    <div class="breadcrumb-block">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="/"> Home </a></li>
                                            <li class="breadcrumb-item active"> Servizi </li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/visual/banner of the page -->
                <!-- main content wrapper -->
                <div class="content-wrapper">
                    <section class="content-block p-0">
                        <div class="container-fluid">
                            <div class="content-slot alternate-block">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="bg-stretch img-wrap wow slideInLeft">
                                            <img src="img/florence-one-digital-integration.jpg" alt="images">
                                        </div>
                                    </div>
                                    <div class="col col-lg-6">
                                        <div class="text-wrap">
                                            <h3>Digital Integration</h3>
                                            <p>Soluzioni per l’integrazione dei sistemi informativi. Ti aiutiamo ad elaborare collegamenti e interconnessioni tra varie piattaforme esistenti per far viaggiare informazioni e dati in modo coordinato, anche con il supporto di architetture specifiche.</p>
                                            <p>Intervento fondamentale per esplorare nuovi modelli di business in modo altamente efficace.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="bg-stretch img-wrap wow slideInRight">
                                            <img src="img/florence-one-enterprise-content.jpg" alt="images">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="text-wrap">
                                            <h3>Enterprise Content</h3>
                                            <p>Strumenti per la gestione di dati e processi informativi in modo intelligente che permettono di sfruttare a pieno il potenziale dei tuoi documenti.  </p>
                                            <p>Non una semplice digitalizzazione dei documenti aziendali, ma soluzioni personalizzate che seguono la struttura organizzativa della tua azienda e il ruolo che documenti e informazioni hanno all’interno di ogni area.</p>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </section>
                    <section class="content-block p-0">
                        <div class="container-fluid">
                            <div class="content-slot alternate-block">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="bg-stretch img-wrap wow slideInLeft">
                                            <img src="img/florence-one-cyber-security.jpg" alt="images">
                                        </div>
                                    </div>
                                    <div class="col col-lg-6">
                                        <div class="text-wrap">
                                            <h3>Cyber Security</h3>
                                            <p>Con misure di cyber security puoi proteggere sistemi, reti e programmi da attacchi digitali, impedendo l’accesso a contenuti e dati sensibili. </p>
                                            <p>Essenziale per qualsiasi azienda con informazioni digitali, la protezione può essere impostata su diversi livelli. </p>
                                            <p>Impostare certe misure garantisce anche il rispetto delle normative vigenti per la conservazione dei dati. </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="bg-stretch img-wrap wow slideInRight">
                                            <img src="img/florence-one-custom-software.jpg" alt="images">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="text-wrap">
                                            <h3>Software Custom</h3>
                                            <p>Sviluppo di software customizzati per esigenze che non trovano risposta in prodotti già esistenti o sopperire alla mancanza di determinate funzionalità.  </p>
                                            <p>Il servizio di software custom ti permette di ottenere un migliore rendimento di determinate attività aziendali.</p>
                                            <p>Ogni nostro software può essere integrato ad applicativi esistenti.</p>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </section>
                    <section class="content-block p-0">
                        <div class="container-fluid">
                            <div class="content-slot alternate-block">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="bg-stretch img-wrap wow slideInLeft">
                                            <img src="img/florence-one-it-consulting.jpg" alt="images">
                                        </div>
                                    </div>
                                    <div class="col col-lg-6">
                                        <div class="text-wrap">
                                            <h3>IT Consulting</h3>
                                            <p>Per risultati efficienti, software e hardware devono funzionare correttamente e la loro implementazione deve essere ottimizzata.  </p>
                                            <p>Ti offriamo supporto e consulenza specializzata costante per affiancarti realmente nella crescita del tuo business. </p>
                                            <p>Effettuiamo per te analisi periodiche della realtà aziendale per aggiornare le soluzioni adottate.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="bg-stretch img-wrap wow slideInRight">
                                            <img src="img/florence-one-network.jpg" alt="images">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="text-wrap">
                                            <h3>Network</h3>
                                            <p>Soluzioni per la creazione ad hoc di reti aziendali e architettura di infrastrutture hardware innovative che garantiscono alta velocità nel trasferimento dei dati tra le macchine locali nell’ambiente aziendale.  </p>
                                            <p>Il networking aziendale verrà sviluppato rispettando l’organizzazione della tua azienda, del workflow e dei processi di produzione.</p>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </section>
                    <section class="content-block p-0">
                        <div class="container-fluid">
                            <div class="content-slot alternate-block">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="bg-stretch img-wrap wow slideInLeft">
                                            <img src="img/florence-one-database-design.jpg" alt="images">
                                        </div>
                                    </div>
                                    <div class="col col-lg-6">
                                        <div class="text-wrap">
                                            <h3>Database Design</h3>
                                            <p>Progettazione di database per l’organizzazione di tutti i dati gestiti all’interno dell’azienda. </p>
                                            <p>Avere database organizzati significa avere dati facilmente integrabili tra loro, consultabili in modo facile e veloce.</p>
                                            <p>Archiviare e suddividere le informazioni ci permette di utilizzare i dati a supporto di processi decisionali.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="bg-stretch img-wrap wow slideInRight">
                                            <img src="img/florence-one-ecommerce.jpg" alt="images">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="text-wrap">
                                            <h3>E-commerce Solutions</h3>
                                            <p>Creazione di E-commerce per la vendita online B2C e B2B studiate ad hoc per la tua attività commerciale.  </p>
                                            <p>Studiamo per te piattaforme responsive che offrono una navigazione intuitiva e fluida, anche da mobile, per una user Experience piacevole.</p>
                                            <p>Personalizza il tuo E-commerce con funzionalità e organizzazione delle categorie. </p>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </section>
                    <section class="content-block p-0">
                        <div class="container-fluid">
                            <div class="content-slot alternate-block">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="bg-stretch img-wrap wow slideInLeft">
                                            <img src="img/florence-one-marketplace.jpg" alt="images">
                                        </div>
                                    </div>
                                    <div class="col col-lg-6">
                                        <div class="text-wrap">
                                            <h3>Marketplace Solutions</h3>
                                            <p>Se vuoi vendere online ma non hai necessità di avere un E-commerce di proprietà, possiamo offrirti soluzioni Marketplace.  </p>
                                            <p>Seguiremo il tuo posizionamento su piattaforme online di intermediazione commerciale, nate per mettere in contatto rivenditori e acquirenti in modo semplice.</p>
                                            <p>In questo modo potrai gestire solo alcuni aspetti della vendita e lasciare il grosso del lavoro direttamente alla piattaforma. </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="bg-stretch img-wrap wow slideInRight">
                                            <img src="img/florence-one-mes.jpg" alt="images">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="text-wrap">
                                            <h3>Sistemi MES</h3>
                                            <p>Soluzioni e strumenti per la gestione efficiente e ottimizzata del reparto produttivo aziendale. </p>
                                            <p>Pianifica la produzione, analizza i dati raccolti dalle macchine e verifica la qualità dei tuoi prodotti in modo facile e veloce. </p>
                                            <p>Collegato a sistemi ERP, utilizza i dati in tempo reale e ottimizza i processi produttivi con soluzioni personalizzabili per qualsiasi settore. </p>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </section>
                    
                    <section class="content-block p-0">
                        <div class="container-fluid">
                            <div class="content-slot alternate-block">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="bg-stretch img-wrap wow slideInLeft">
                                            <img src="img/florence-one-avvia-progetto.jpg" alt="images">
                                        </div>
                                    </div>
                                    <div class="col col-lg-6">
                                        <div class="text-wrap">
                                            <h3>Vuoi avviare un progetto con noi?</h3>
                                            <p>Compila il form, ti contatteremo per capire tutte le tue esigenze e formuleremo insieme a te un'offerta adeguata.</p>
                                            <p></p>
                                        <!-- contact form -->
                                        <form action="email_gmail.php" method="post" class="waituk_contact-form">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <input type="text" placeholder="NOME *" id="con_fname" name="nome" class="form-control" required>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <input type="text" placeholder="AZIENDA *" id="con_lname" name="azienda" class="form-control" required>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <input type="tel" placeholder="TELEFONO *" id="con_phone" name="telefono" class="form-control" required>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                        <input type="email" placeholder="EMAIL *" id="con_email" name="email" class="form-control" required>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <input type="hidden" id="pagina" name="pagina" value="<?=$_SERVER['REQUEST_URI']?>">
                                            
                                            <div class="btn-container">
                                                <button type="submit" class="btn btn-primary btn-arrow">CONTATTACI</button>
                                                <p id="form" class="successo col-11 alert alert-success" style="display:none">Invio riuscito</p>
                                                <p id="form" class="errore col-11 alert alert-success" style="display:none; color:red">Tutti i campi sono obbligatori.</p>
                                            </div>
                                        </form>
                                    
                                            
                                            
                                            
                                            
                                            
                                            
                                        </div>
                                    </div>
                                </div>
                                
                        </div>
                    </section>
                    <section class="content-block quotation-block black-overlay-6 parallax" data-stellar-background-ratio="0.55">
                        <div class="container">
                            <div class="inner-wrapper">
                                <h3 class="block-top-heading text-white">LE MIGLIORI TECNOLOGIE</h3>
                                <h2 class="text-white">Siamo presenti nelle migliori aziende</h2>
                                <div class="btn-container">
                                    <a href="contatti" class="btn btn-primary">Richiedi una valutazione</a>
                                </div>
                            </div>
                        </div>
                    </section>
                    <?php include ("sezione_clienti.html"); ?>
                </div>
                <!--/main content wrapper -->
            </main>
        </div>
        <?php include ("footer.html"); ?>
    <!-- jquery library -->
    <script src="vendors/jquery/jquery-2.1.4.min.js"></script>
    <!-- external scripts -->
    <script src="vendors/tether/dist/js/tether.min.js"></script>
    <script src="vendors/bootstrap/js/bootstrap.min.js"></script>
    <script src="vendors/stellar/jquery.stellar.min.js"></script>
    <script src="vendors/isotope/javascripts/isotope.pkgd.min.js"></script>
    <script src="vendors/isotope/javascripts/packery-mode.pkgd.js"></script>
    <script src="vendors/owl-carousel/dist/owl.carousel.js"></script>
    <script src="vendors/waypoint/waypoints.min.js"></script>
    <script src="vendors/counter-up/jquery.counterup.min.js"></script>
    <script src="vendors/fancyBox/source/jquery.fancybox.pack.js"></script>
    <script src="vendors/fancyBox/source/helpers/jquery.fancybox-thumbs.js"></script>
    <script src="vendors/image-stretcher-master/image-stretcher.js"></script>
    <script src="vendors/wow/wow.min.js"></script>
    <script src="vendors/rateyo/jquery.rateyo.js"></script>
    <script src="vendors/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="vendors/bootstrap-slider-master/src/js/bootstrap-slider.js"></script>
    <script src="vendors/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
    <script src="js/mega-menu.js"></script>
    <!-- custom jquery script -->
    <script src="js/jquery.main.js"></script>
    <!-- REVOLUTION JS FILES -->
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/jquery.themepunch.revolution.min.js"></script>
    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.migration.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.video.min.js"></script>
    <!-- SNOW ADD ON -->
    <script type="text/javascript" src="vendors/rev-slider/revolution-addons/snow/revolution.addon.snow.min.js"></script>
        <!-- revolutions slider script -->
    <script src="js/revolution.js"></script>
</html>
