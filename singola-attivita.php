<!DOCTYPE html>
<html lang="it">

<head>
     <?php include ("header.html"); ?>
</head>

<body>
    <!-- main wrapper -->
    <div id="wrapper">
        <div class="page-wrapper">
            <!-- header of the page -->
            <?php include ("menu.html"); ?>
            <!--/header of the page -->
            <main>
            	<section class="visual">
                    <div class="visual-inner sap-business-one-banner dark-overlay parallax" data-stellar-background-ratio="0.55">
                        <div class="centered">
                            <div class="container">
                                	
                                    <h1 class="visual-title visual-sub-title">SIDERALE CONVERTING</h1>
                                    <div class="breadcrumb-block">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="/"> Home </a></li>
                                            <li class="breadcrumb-item"><a href="aziende-produzione"> Manufactoring </a></li>
                                            <li class="breadcrumb-item active"> Siderale Converting </li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- main content wrapper -->
                <div class="content-wrapper">
                    <section class="content-block">
                        <div class="container">
                            <div class="row mt-5">
                                <div class="col-lg-12 less-wide">
                                    <div class="blog-holder" style="padding-bottom:50px">
                                        <article class="blog-article">
                                                
                                            <div class="blog-desc pt-5">
                                                <div class="blog-img">
                                                    <div class="image-wrap">
                                                        <figure class="">
                                                            <img src="img/clienti/FLORENCE-ONE-siderale-converting.jpg" alt="images description">
                                                        </figure>
                                                    </div>
                                                </div>
                                                <div class="blog-share mt-5">
                                                    <ul class="social-network with-text">
                                                        <li><strong>Info</strong></li>
                                                        <li>CLiente: Siderale Converting</li>
                                                        <li><a href="https://www.sideraleconverting.com">Sito: https://www.sideraleconverting.com</a></li>
                                                    </ul>
                                                </div>
                                                <div class="blockquote-block blockquote-block-v2 pt-3 pb-3">
                                                    <blockquote>
                                                        <p>La storia</p>
                                                    </blockquote>
                                                </div>
                                                <p>Siderale Converting ricerca, progetta e realizza macchinari innovativi per la trasformazione di prodotti in carta tissue adatti sia al mercato Consumer che Professional. Fondata nel 2019, la Società basa oggi la sua forza su due importanti brevetti internazionali che permettono di proporre macchinari in grado di esprimere performance mai raggiunte prima sul mercato. Il modello di business perseguito da Siderale è quello della progettazione, costruzione e vendita di macchinari e prodotti consumabili in condizione di esclusiva, a clienti specifici del settore cartotecnico. L'efficace protezione intellettuale e la dotazione di avanzatissimi sistemi di progettazione, gestione e realizzazione consentono alla società di proporsi anche ai clienti più strutturati, che richiedono un livello di assistenza e competenza tipico dei grandi.</p> 
                                                <div class="blockquote-block blockquote-block-v2 pt-3 pb-3">
                                                    <blockquote>
                                                        <p>La sfida</p>
                                                    </blockquote>
                                                </div>
                                                <p>A differenza di quanto accadeva un tempo, il dialogo con i potenziali clienti che la Siderale deve affrontare, è oggi governato da richieste sempre più stringenti in fatto di normative di sicurezza, ambientali e sulla affidabilità dei macchinari proposti. Per questo motivo è evidente come solo i costruttori più strutturati abbiano la possibilità di proporre i loro impianti a quei clienti che possano realmente determinare il successo di unazienda. Siderale è una micro impresa che ha deciso di competere strutturando lazienda con assoluta scalabilità. Ci siamo dotati fin da subito dei più moderni sistemi di progettazione e di gestione di tutto il processo produttivo in modo da poter rispondere efficacemente alle più complesse dinamiche interne verso clienti e fornitori. Questo assetto ci consente di abbattere molti dei costi che avevamo mantenendo un livello di governance mai visto prima.</p>
                                                <div class="blockquote-block blockquote-block-v2 pt-3 pb-3">
                                                    <blockquote>
                                                        <p>La soluzione</p>
                                                    </blockquote>
                                                </div>
                                                <p>L'introduzione di SAP Business One nelle sue declinazioni desktop, web e mobile, ci consente di rispondere sia ai fornitori che ai nostri più grandi clienti, con la stessa professionalità dimostrata dai nostri concorrenti più influenti sul mercato. Tutte le procedure ed i task vengono organizzati in una sequenza ordinata e corretta, permettendoci di operare senza rischi per la commessa. Anche gli aspetti che riguardano la proposta al cliente vengono curati nei minimi particolari. I nostri venditori possono disporre dei cataloghi e delle schede tecniche sempre aggiornate con le modifiche frequentemente elaborate in ufficio tecnico. Il rapporto con gli istituti di credito non viene più gestito manualmente bensì con un modulo dedicato alla tesoreria capace di importare ed esportare i movimenti riconciliando automaticamente le partite e proponendo le migliori condizioni riducendo gli oneri bancari.</p>
                                                <div class="blog-share mt-5">
                                                    <ul class="social-network with-text">
                                                        <li><strong>Share :</strong></li>
                                                        <li><a href="#"><span class="icon-facebook"></span> Facebook</a></li>
                                                        <li><a href="#"><span class="icon-linkedin"></span> Linkedin</a></li>
                                                    </ul>
                                                </div>
                                                
                                            </div>
                                        </article>
                                    </div>
                                    <div class="contact-container">
                                        <form action="#" class="comment-form waituk_contact-form">
                                            <fieldset>
                                                <h6 class="content-title contact-title">Entra a far parte del mondo Florence One</h6>
                                                <div class="row">
                                                    <div class="col-sm-6 form-group">
                                                        <input placeholder="Nome" type="text" class="form-control">
                                                    </div>
                                                    <div class="col-sm-6 form-group">
                                                        <input placeholder="Email" type="email" class="form-control">
                                                    </div>
                                                    <div class="col-sm-12 form-group">
                                                        <input placeholder="Azienda" type="text" class="form-control">
                                                    </div>
                                                    <div class="col-sm-12 form-group">
                                                        <textarea placeholder="Messaggio" class="form-control"></textarea>
                                                    </div>
                                                    <div class="col-sm-12 btn-holder">
                                                        <button type="submit" class="btn btn-black btn-full">Invia richiesta</button>
                                                    </div>
                                            </fieldset>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </section>
                    </div>
                    <!--/main content wrapper -->
            </main>
            </div>
            <!-- footer of the pagse -->
        <?php include ("footer.html"); ?>
        <!-- jquery library -->
        <script src="vendors/jquery/jquery-2.1.4.min.js"></script>
        <!-- external scripts -->
        <script src="vendors/tether/dist/js/tether.min.js"></script>
        <script src="vendors/bootstrap/js/bootstrap.min.js"></script>
        <script src="vendors/stellar/jquery.stellar.min.js"></script>
        <script src="vendors/isotope/javascripts/isotope.pkgd.min.js"></script>
        <script src="vendors/isotope/javascripts/packery-mode.pkgd.js"></script>
        <script src="vendors/owl-carousel/dist/owl.carousel.js"></script>
        <script src="vendors/waypoint/waypoints.min.js"></script>
        <script src="vendors/counter-up/jquery.counterup.min.js"></script>
        <script src="vendors/fancyBox/source/jquery.fancybox.pack.js"></script>
        <script src="vendors/fancyBox/source/helpers/jquery.fancybox-thumbs.js"></script>
        <script src="vendors/image-stretcher-master/image-stretcher.js"></script>
        <script src="vendors/wow/wow.min.js"></script>
        <script src="vendors/rateyo/jquery.rateyo.js"></script>
        <script src="vendors/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script src="vendors/bootstrap-slider-master/src/js/bootstrap-slider.js"></script>
        <script src="vendors/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
        <script src="js/mega-menu.js"></script>
        <!-- custom jquery script -->
        <script src="js/jquery.main.js"></script>
        <!-- REVOLUTION JS FILES -->
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/jquery.themepunch.tools.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/jquery.themepunch.revolution.min.js"></script>
        <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.actions.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.migration.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.video.min.js"></script>
        <!-- SNOW ADD ON -->
        <script type="text/javascript" src="vendors/rev-slider/revolution-addons/snow/revolution.addon.snow.min.js"></script>
        <!-- revolutions slider script -->
        <script src="js/revolution.js"></script>

</html>
