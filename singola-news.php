<?php 
include ("functions.php");
include ("db.php");
$articolo = $db->query('SELECT * FROM news WHERE id = ?', $_GET['id'])->fetchArray();
$img = json_decode($articolo["img"], true);
$i = '/backoffice/'.$img[0]['serverFileName'];
?>
<?php
use Melbahja\Seo\Factory;

// Load Composer's autoloader
require 'vendor/autoload.php';

$metatags = Factory::metaTags();
$title = $articolo['titolo'];

$metatags->meta('author', 'Biznes')
		->meta('title', $title)
		->meta('description', limit_text(strip_tags($articolo['testo']), 30))
		->image($i)
		->url('https://florence-one.it/news/'.$articolo['id'].'-'.slugify($articolo['titolo']))
?>
<!DOCTYPE html>
<html lang="it">
<head>
     <?php include ("header.html"); ?>
     <title><?=$title?></title>
     <?=$metatags?>
</head>

<body>
    <!-- main wrapper -->
    <div id="wrapper">
        <div class="page-wrapper">
            <!-- header of the page -->
            <?php include ("menu.html"); ?>
            <!--/header of the page -->            
            <main>
            	<section class="visual">
                    <div class="visual-inner sap-business-one-banner dark-overlay parallax" data-stellar-background-ratio="0.55">
                        <div class="centered">
                            <div class="container">
                                	
                                    <h1 class="visual-title visual-sub-title"><?=$articolo['titolo']?></h1>
                                    <div class="breadcrumb-block">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="/"> Home </a></li>
                                            <li class="breadcrumb-item"><a href="/news"> News </a></li>
                                            <li class="breadcrumb-item active"> <?=$articolo['titolo']?> </li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- main content wrapper -->
                <div class="content-wrapper">
                    <section class="content-block">
                        <div class="container">
                            <div class="row mt-5">
                                <div class="col-lg-12 less-wide">
                                    <div class="blog-holder" style="padding-bottom:50px">
                                        <article class="blog-article">
                                        		
                                                
                                            <div class="blog-desc pt-5">
                                                <div class="blog-img">
                                                    <div class="image-wrap">
                                                        <figure class="">
                                                            <img src="<?=$i?>" alt="<?=$articolo['titolo']?>">
                                                        </figure>
                                                    </div>
                                                </div>
                                                
                                                <p>
                                                    <?=$articolo['testo']?>
                                                </p>
                                                
                                            </div>
                                        </article>
                                    </div>
                                </div>
                            </div>
                    </section>
                    </div>
                    <!--/main content wrapper -->
            </main>
            </div>
            <!-- footer of the pagse -->
        <?php include ("footer.html"); ?>
        <!-- jquery library -->
        <script src="/vendors/jquery/jquery-2.1.4.min.js"></script>
        <!-- external scripts -->
        <script src="/vendors/tether/dist/js/tether.min.js"></script>
        <script src="/vendors/bootstrap/js/bootstrap.min.js"></script>
        <script src="/vendors/stellar/jquery.stellar.min.js"></script>
        <script src="/vendors/isotope/javascripts/isotope.pkgd.min.js"></script>
        <script src="/vendors/isotope/javascripts/packery-mode.pkgd.js"></script>
        <script src="/vendors/owl-carousel/dist/owl.carousel.js"></script>
        <script src="/vendors/waypoint/waypoints.min.js"></script>
        <script src="/vendors/counter-up/jquery.counterup.min.js"></script>
        <script src="/vendors/fancyBox/source/jquery.fancybox.pack.js"></script>
        <script src="/vendors/fancyBox/source/helpers/jquery.fancybox-thumbs.js"></script>
        <script src="/vendors/image-stretcher-master/image-stretcher.js"></script>
        <script src="/vendors/wow/wow.min.js"></script>
        <script src="/vendors/rateyo/jquery.rateyo.js"></script>
        <script src="/vendors/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script src="/vendors/bootstrap-slider-master/src/js/bootstrap-slider.js"></script>
        <script src="/vendors/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
        <script src="/js/mega-menu.js"></script>
        <!-- custom jquery script -->
        <script src="/js/jquery.main.js"></script>
        <!-- REVOLUTION JS FILES -->
        <script type="text/javascript" src="/vendors/rev-slider/revolution/js/jquery.themepunch.tools.min.js"></script>
        <script type="text/javascript" src="/vendors/rev-slider/revolution/js/jquery.themepunch.revolution.min.js"></script>
        <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
        <script type="text/javascript" src="/vendors/rev-slider/revolution/js/extensions/revolution.extension.actions.min.js"></script>
        <script type="text/javascript" src="/vendors/rev-slider/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
        <script type="text/javascript" src="/vendors/rev-slider/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
        <script type="text/javascript" src="/vendors/rev-slider/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
        <script type="text/javascript" src="/vendors/rev-slider/revolution/js/extensions/revolution.extension.migration.min.js"></script>
        <script type="text/javascript" src="/vendors/rev-slider/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
        <script type="text/javascript" src="/vendors/rev-slider/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
        <script type="text/javascript" src="/vendors/rev-slider/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
        <script type="text/javascript" src="/vendors/rev-slider/revolution/js/extensions/revolution.extension.video.min.js"></script>
        <!-- SNOW ADD ON -->
        <script type="text/javascript" src="/vendors/rev-slider/revolution-addons/snow/revolution.addon.snow.min.js"></script>
        <!-- revolutions slider script -->
        <script src="/js/revolution.js"></script>

</html>
