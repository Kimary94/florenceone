<!DOCTYPE html>
<html lang="it">

<head>
    <?php include ("header.html"); ?>
</head>

<body>
    <!-- main wrapper -->
    <div id="wrapper">
        <div class="page-wrapper">
            <!-- header of the page -->
            <?php include ("menu.html"); ?>
            <main>
            <!-- visual/banner of the page -->
                <section class="visual">
                    <div class="visual-inner sap-business-one-banner dark-overlay parallax" data-stellar-background-ratio="0.55">
                        <div class="centered">
                            <div class="container">
                                    <h1 class="visual-title visual-sub-title">Valutazione personalizzata</h1>
                                    <div class="breadcrumb-block">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="/"> Home </a></li>
                                            <li class="breadcrumb-item active"> Valutazione personalizzata </li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/visual/banner of the page -->
                <!-- main content wrapper -->
                <div class="content-wrapper">
                    <div class="row  no-gutters">
                        <div class="col-lg-6 hidden-md-down">
                            <div class="bg-stretch img-wrap">
                                <img src="img/florence-one-valutazione-personalizzata.jpg" alt="images">
                            </div>
                        </div>
                        <div class="col-lg-6 signup-block">
                            <div class="signup-wrap text-center">
                                <div class="inner-wrap">
                                    
                                    <form action="#" method="post" id="contact_form" class="waituk_contact-form signup-form">
                                        <div class="row">
                                        	<div class="col-md-12">
                                                <p>In quale settore operi</p>
                                            </div>
                                        	<div class="col-md-4">
                                                <div class="waituk_select-box" >
                                                    <div class="waituk_select-box-default square-box">
                                                        <input type="checkbox" name="checkbox" id="checkbox11" >
                                                        <label for="checkbox11" class="m-0">Automotive</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="waituk_select-box" >
                                                    <div class="waituk_select-box-default square-box" >
                                                        <input type="checkbox" name="checkbox" id="checkbox11" >
                                                        <label for="checkbox11" class="m-0">Agroalimentare</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="waituk_select-box" >
                                                    <div class="waituk_select-box-default square-box" >
                                                        <input type="checkbox" name="checkbox" id="checkbox11" >
                                                        <label for="checkbox11" class="m-0">Produzione</label>
                                                    </div>
                                                </div>
                                            </div>
                                        	<div class="col-md-4">
                                                <div class="waituk_select-box" >
                                                    <div class="waituk_select-box-default square-box" >
                                                        <input type="checkbox" name="checkbox" id="checkbox11" >
                                                        <label for="checkbox11" class="m-0">Istruzione</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="waituk_select-box">
                                                    <div class="waituk_select-box-default square-box" >
                                                        <input type="checkbox" name="checkbox" id="checkbox11" >
                                                        <label for="checkbox11" class="m-0">Food</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="waituk_select-box" style="align:left">
                                                    <div class="waituk_select-box-default square-box" >
                                                        <input type="checkbox" name="checkbox" id="checkbox11" >
                                                        <label for="checkbox11" class="m-0">Manifattura</label>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        	<div class="col-md-4">
                                                <div class="waituk_select-box">
                                                    <div class="waituk_select-box-default square-box">
                                                        <input type="checkbox" name="checkbox" id="checkbox11">
                                                        <label for="checkbox11" class="m-0">Commercio</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="waituk_select-box">
                                                    <div class="waituk_select-box-default square-box">
                                                        <input type="checkbox" name="checkbox" id="checkbox11">
                                                        <label for="checkbox11" class="m-0">Servizi</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="waituk_select-box">
                                                    <div class="waituk_select-box-default square-box">
                                                        <input type="checkbox" name="checkbox" id="checkbox11">
                                                        <label for="checkbox11" class="m-0">Altro</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <p><br>Quanti dipendenti ha la tua azienda?</p>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="text" placeholder="Numero dipendenti" id="con_uname" name="con_fname" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <p>Quale è il vostro fatturato annuo?</p>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <input type="text" placeholder="Fatturato annuo" id="con_uname" name="con_fname" class="form-control">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <p>Quale altri software usate?</p>
                                            </div>
                                        	<div class="col-md-4">
                                                <div class="waituk_select-box" >
                                                    <div class="waituk_select-box-default square-box">
                                                        <input type="checkbox" name="checkbox" id="checkbox11" >
                                                        <label for="checkbox11" class="m-0">Finanza</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="waituk_select-box" >
                                                    <div class="waituk_select-box-default square-box" >
                                                        <input type="checkbox" name="checkbox" id="checkbox11" >
                                                        <label for="checkbox11" class="m-0">CRM</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="waituk_select-box" >
                                                    <div class="waituk_select-box-default square-box" >
                                                        <input type="checkbox" name="checkbox" id="checkbox11" >
                                                        <label for="checkbox11" class="m-0">Vendite</label>
                                                    </div>
                                                </div>
                                            </div>
                                        	<div class="col-md-4">
                                                <div class="waituk_select-box" >
                                                    <div class="waituk_select-box-default square-box" >
                                                        <input type="checkbox" name="checkbox" id="checkbox11" >
                                                        <label for="checkbox11" class="m-0">eCommerce</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="waituk_select-box">
                                                    <div class="waituk_select-box-default square-box" >
                                                        <input type="checkbox" name="checkbox" id="checkbox11" >
                                                        <label for="checkbox11" class="m-0">Altro</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <p><br>Quale software finanziario e di contabilità utilizzate attualmente?</p>
                                            </div>
                                        	<div class="col-md-4">
                                                <div class="waituk_select-box" >
                                                    <div class="waituk_select-box-default square-box">
                                                        <input type="checkbox" name="checkbox" id="checkbox11" >
                                                        <label for="checkbox11" class="m-0">Contabilità</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="waituk_select-box" >
                                                    <div class="waituk_select-box-default square-box" >
                                                        <input type="checkbox" name="checkbox" id="checkbox11" >
                                                        <label for="checkbox11" class="m-0">Banche</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="waituk_select-box" >
                                                    <div class="waituk_select-box-default square-box" >
                                                        <input type="checkbox" name="checkbox" id="checkbox11" >
                                                        <label for="checkbox11" class="m-0">Fatturazione</label>
                                                    </div>
                                                </div>
                                            </div>
                                        	<div class="col-md-4">
                                                <div class="waituk_select-box" >
                                                    <div class="waituk_select-box-default square-box" >
                                                        <input type="checkbox" name="checkbox" id="checkbox11" >
                                                        <label for="checkbox11" class="m-0">Paghe</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="waituk_select-box">
                                                    <div class="waituk_select-box-default square-box" >
                                                        <input type="checkbox" name="checkbox" id="checkbox11" >
                                                        <label for="checkbox11" class="m-0">Pagamenti</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <p><br>Disponete di un team informatico?</p>
                                            </div>
                                        	<div class="col-md-6">
                                                <div class="waituk_select-box" >
                                                    <div class="waituk_select-box-default square-box">
                                                        <input type="checkbox" name="checkbox" id="checkbox11" >
                                                        <label for="checkbox11" class="m-0">SI</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="waituk_select-box" >
                                                    <div class="waituk_select-box-default square-box" >
                                                        <input type="checkbox" name="checkbox" id="checkbox11" >
                                                        <label for="checkbox11" class="m-0">NO</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <p><br>Avete più sedi?</p>
                                            </div>
                                        	<div class="col-md-6">
                                                <div class="waituk_select-box" >
                                                    <div class="waituk_select-box-default square-box">
                                                        <input type="checkbox" name="checkbox" id="checkbox11" >
                                                        <label for="checkbox11" class="m-0">SI</label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="waituk_select-box" >
                                                    <div class="waituk_select-box-default square-box" >
                                                        <input type="checkbox" name="checkbox" id="checkbox11" >
                                                        <label for="checkbox11" class="m-0">NO</label>
                                                    </div>
                                                </div>
                                            </div>
                                        
                                            
                                        </div>
                                        <div class="btn-container mb-3  mb-xl-3 mt-xl-5 mt-lg-2">
                                            <button id="btn_sent" class="btn btn-primary has-radius-small">Invia richiesta</button>
                                        </div>
                                        
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/main content wrapper -->
            </main>
        </div>
        <!-- footer of the pagse -->
        <?php include ("footer.html"); ?>
    <!-- jquery library -->
    <script src="vendors/jquery/jquery-2.1.4.min.js"></script>
    <!-- external scripts -->
    <script src="vendors/tether/dist/js/tether.min.js"></script>
    <script src="vendors/bootstrap/js/bootstrap.min.js"></script>
    <script src="vendors/stellar/jquery.stellar.min.js"></script>
    <script src="vendors/isotope/javascripts/isotope.pkgd.min.js"></script>
    <script src="vendors/isotope/javascripts/packery-mode.pkgd.js"></script>
    <script src="vendors/owl-carousel/dist/owl.carousel.js"></script>
    <script src="vendors/waypoint/waypoints.min.js"></script>
    <script src="vendors/counter-up/jquery.counterup.min.js"></script>
    <script src="vendors/fancyBox/source/jquery.fancybox.pack.js"></script>
    <script src="vendors/fancyBox/source/helpers/jquery.fancybox-thumbs.js"></script>
    <script src="vendors/image-stretcher-master/image-stretcher.js"></script>
    <script src="vendors/wow/wow.min.js"></script>
    <script src="vendors/rateyo/jquery.rateyo.js"></script>
    <script src="vendors/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="vendors/bootstrap-slider-master/src/js/bootstrap-slider.js"></script>
    <script src="vendors/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
    <script src="js/mega-menu.js"></script>
    <!-- custom jquery script -->
    <script src="js/jquery.main.js"></script>
    <!-- REVOLUTION JS FILES -->
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/jquery.themepunch.revolution.min.js"></script>
    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.migration.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.video.min.js"></script>
    <!-- SNOW ADD ON -->
    <script type="text/javascript" src="vendors/rev-slider/revolution-addons/snow/revolution.addon.snow.min.js"></script>
    <!-- revolutions slider script -->
    <script src="js/revolution.js"></script>
</body>

</html>
