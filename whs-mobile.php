<?php
use Melbahja\Seo\Factory;

// Load Composer's autoloader
require 'vendor/autoload.php';

$metatags = Factory::metaTags();
$title = "Florence One | Software per magazzino | WHS Mobile ";

$metatags->meta('author', 'Biznes')
		->meta('title', $title)
		->meta('description', 'WHS Mobile, L’app mobile per la gestione del magazzino. WHS, l’Add-on integrato con SAP Business One per la gestione facile e veloce del magazzino direttamente da mobile.')
        ->meta('keywords', 'WHS Mobile, SAP HANA, Gestione magazzino, Software per magazzino, applicazione per gestione magazzino, gestionale per magazzino')
		->image('img/florence-one-whs-1.jpg')
		->url('https://florence-one.it/whs-mobile')
?>
<!DOCTYPE html>
<html lang="it">

<head>
	<title><?=$title?></title>
	<?=$metatags?>
    <?php include ("header.html"); ?>
</head>

<body>
    <!-- main wrapper -->
    <div id="wrapper">
        <div class="page-wrapper">
            <?php include ("menu.html"); ?>
            <main>
                <!-- visual/banner of the page -->
                <section class="visual">
                    <div class="visual-inner sap-business-one-banner dark-overlay parallax" data-stellar-background-ratio="0.55">
                        <div class="centered">
                            <div class="container">
                                	
                                    <img src="img/prodotti/logo-whs-florence-one.png" alt="">
                                    </br></br>
                                    <div class="breadcrumb-block">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="/"> Home </a></li>
                                            <li class="breadcrumb-item active"> WHS Mobile </li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--/visual/banner of the page -->
                <!-- main content wrapper -->
                <div class="content-wrapper">
                    <section class="content-block">
                        <div class="container text-center">
                        	
                            <div class="heading bottom-space">
                            	<h1 class="visual-title visual-sub-title" style="color:#55565B" >WHS Mobile</h1>
                                <h2>L’app mobile per la gestione del magazzino</h2>
                            </div>
                            <div class="description">
                                <p>WHS, l’Add-on integrato con SAP Business One </br>
								per la gestione facile e veloce del magazzino direttamente da mobile.
 								</p>
                            </div>
                        </div>
                    </section>
                    <section class="content-block p-0">
                        <div class="container-fluid">
                            <div class="content-slot alternate-block">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="bg-stretch img-wrap wow slideInLeft">
                                            <img src="img/florence-one-whs-1.jpg" alt="images">
                                        </div>
                                    </div>
                                    <div class="col col-lg-6">
                                        <div class="text-wrap">
                                            <h3>Gestisci il magazzino direttamente da mobile</h3>
                                            <p>Semplifica la gestione delle merci e del magazzino con l’App mobile sviluppata da Florence One e integrata al gestionale SAP Business One: avrai accesso a tutte le schede prodotto tramite terminale, con funzionalità dedicate e di facile utilizzo.</p>
                                            <p>Ogni utente potrà accedere con proprie credenziali e visualizzare esclusivamente le proprie Dashboard.</p>
                                            <p>Tutte le operazioni effettuate direttamente dal terminale saranno visibili in tempo reale anche sul gestionale SAP per una gestione trasparente, veloce ed efficiente della logistica.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="bg-stretch img-wrap wow slideInRight">
                                            <img src="img/florence-one-whs-2.jpg" alt="images">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="text-wrap">
                                            <h3>Accessi personalizzati.</h3>
                                            <p>Con WHS il personale dedicato al controllo della logistica e del magazzino può essere organizzato in base ai ruoli occupati.</p>
                                            <p>L’account potrà visualizzare tutte o solamente alcune schede a seconda del ruolo ricoperto all’interno dell’organizzazione aziendale. L’interfaccia semplice e intuitiva consente la navigazione tra varie dashboard e di aggiornare i dati.  </p>
                                            <p>Tieni sotto controllo il tuo personale. </p>
                                            <p></p>
                                            <a href="#" class="btn btn-black has-radius-small">Scarica la brochure</a>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </section>
                    <section class="content-block">
                        <div class="container">
                            <div class="row multiple-row v-align-row">
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="block-heading">
                                            <h3 class="block-top-heading">WHS</h3>
                                            <h2 class="block-main-heading">FUNZIONI PRINCIPALI</h2>
                                            <span class="block-sub-heading">WHS è lo strumento mobile Florence One integrato con SAP Business One che ti permetterà  di implementare le funzionalità di logistica e magazzino.</span>
                                            <div class="divider"><img src="img/divider.png" alt="images description"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">CONSEGNE</h4>
                                            <div class="des">
                                                <p>Gestione delle consegne al cliente e visualizzazione degli ordini aperti.</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">ORDINI</h4>
                                            <div class="des">
                                                <p>Creazione ordini da fornitori e gestione delle consegne previste.</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">TRASFERIMENTI</h4>
                                            <div class="des">
                                                <p>Gestione di tutta la logistica interna al magazzino e tra più ubicazioni.</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">ENTRATA MERCI</h4>
                                            <div class="des">
                                                <p>Controllo di entrata generica di merci con causali dedicate e personalizzabili</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">USCITA MERCI</h4>
                                            <div class="des">
                                                <p>Controllo di uscita generica di merci con causali dedicate e personalizzabili.</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">LETTURA BARCODE</h4>
                                            <div class="des">
                                                <p>Scanner automatico o inserimento manuale di barcode per lettura articoli.</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">NUOVO BARCODE</h4>
                                            <div class="des">
                                                <p>Creazione nuovi barcode e inserimento manuale in scheda anagrafica articolo.</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6">
                                    <div class="col-wrap">
                                        <div class="ico-box bg-gray-light has-radius-medium">
                                            <div class="icon">
                                                <span class="custom-icon-list"><span class="sr-only">&amp;</span></span>
                                            </div>
                                            <h4 class="content-title">MULTI ACCOUNT</h4>
                                            <div class="des">
                                                <p>Possibilità di organizzare gli accessi su livelli in base ai ruoli degli account.</p>
                                            </div>
                                            <div class="link-holder">
                                                <a class="link-more" href="#"></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section class="content-block p-0">
                        <div class="container-fluid">
                            <div class="content-slot alternate-block">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="bg-stretch img-wrap wow slideInLeft">
                                            <img src="img/florence-one-whs-3.jpg" alt="images">
                                        </div>
                                    </div>
                                    <div class="col col-lg-6">
                                        <div class="text-wrap">
                                            <h3>Richiedi subito una demo</h3>
                                            <p>Scopri le potenzialità di WHS! Compila il form e richiedi una demo.</p>
                                            <p></p>
                                        <!-- contact form -->
                                        <?php include ("form-prodotti.html"); ?>
                                    
                                            
                                            
                                            
                                            
                                            
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="bg-stretch img-wrap wow slideInRight">
                                            <img src="img/florence-one-whs-4.jpg" alt="images">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="text-wrap">
                                            <h3>Mai stato così semplice.</h3>
                                            <p>Grazie all’Add-on WHS la gestione del magazzino e dell’archivio merci non è mai stata così semplice. Il suo sistema di autocompilazione e la ricerca facile e veloce di clienti e fornitori censiti sul gestionale SAP, ti permette di organizzare una mole importante di dati e articoli in modo facile e veloce, anche l’ubicazione su più magazzini. </p>
                                            <p>In questo modo potrai snellisce i tempi di risposta, eliminare errori nella creazione di ordini in entrata e in uscita ed effettuare tutte le operazioni con maggior velocità e efficienza, anche su più ubicazioni.</p>
                                        </div>
                                    </div>
                                </div>
                                
                            </div>
                        </div>
                    </section>
                    <section class="content-block quotation-block black-overlay-6 parallax" data-stellar-background-ratio="0.55">
                        <div class="container">
                            <div class="inner-wrapper">
                                <h3 class="block-top-heading text-white">LE MIGLIORI TECNOLOGIE</h3>
                                <h2 class="text-white">WHS Mobile</br>presente nelle migliori aziende</h2>
                                <div class="btn-container">
                                    <a href="contatti" class="btn btn-primary">Richiedi una valutazione</a>
                                </div>
                            </div>
                        </div>
                    </section>
                    <?php include ("sezione_clienti.html"); ?>
                </div>
                <!--/main content wrapper -->
            </main>
        </div>
        <?php include ("footer.html"); ?>
    <!-- jquery library -->
    <script src="vendors/jquery/jquery-2.1.4.min.js"></script>
    <!-- external scripts -->
    <script src="vendors/tether/dist/js/tether.min.js"></script>
    <script src="vendors/bootstrap/js/bootstrap.min.js"></script>
    <script src="vendors/stellar/jquery.stellar.min.js"></script>
    <script src="vendors/isotope/javascripts/isotope.pkgd.min.js"></script>
    <script src="vendors/isotope/javascripts/packery-mode.pkgd.js"></script>
    <script src="vendors/owl-carousel/dist/owl.carousel.js"></script>
    <script src="vendors/waypoint/waypoints.min.js"></script>
    <script src="vendors/counter-up/jquery.counterup.min.js"></script>
    <script src="vendors/fancyBox/source/jquery.fancybox.pack.js"></script>
    <script src="vendors/fancyBox/source/helpers/jquery.fancybox-thumbs.js"></script>
    <script src="vendors/image-stretcher-master/image-stretcher.js"></script>
    <script src="vendors/wow/wow.min.js"></script>
    <script src="vendors/rateyo/jquery.rateyo.js"></script>
    <script src="vendors/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script src="vendors/bootstrap-slider-master/src/js/bootstrap-slider.js"></script>
    <script src="vendors/bootstrap-select/dist/js/bootstrap-select.min.js"></script>
    <script src="js/mega-menu.js"></script>
    <!-- custom jquery script -->
    <script src="js/jquery.main.js"></script>
    <!-- REVOLUTION JS FILES -->
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/jquery.themepunch.tools.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/jquery.themepunch.revolution.min.js"></script>
    <!-- SLIDER REVOLUTION 5.0 EXTENSIONS  (Load Extensions only on Local File Systems !  The following part can be removed on Server for On Demand Loading) -->
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.actions.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.carousel.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.kenburn.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.layeranimation.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.migration.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.navigation.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.parallax.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.slideanims.min.js"></script>
    <script type="text/javascript" src="vendors/rev-slider/revolution/js/extensions/revolution.extension.video.min.js"></script>
    <!-- SNOW ADD ON -->
    <script type="text/javascript" src="vendors/rev-slider/revolution-addons/snow/revolution.addon.snow.min.js"></script>
        <!-- revolutions slider script -->
    <script src="js/revolution.js"></script>
</html>
